<?php
  $seccionActiva=12;
  include_once('cabecera.php');
  
  $empresa='';
  if(isset($_GET['codigoCliente'])){
	$codigoCliente=$_GET['codigoCliente'];
	$consulta=consultaBD("SELECT empresa FROM clientes WHERE codigo='$codigoCliente';",true);
	$datosCliente=mysql_fetch_assoc($consulta);
	$empresa=$datosCliente['empresa'];
  }
  
  $res=false;
  if(isset($_POST['codigo'])){
    $res=actualizaDatos('ofertas');
  }
  elseif(isset($_POST['codigoProducto'])){
    $res=insertaDatos('ofertas');
    $res=$res && insertaCodigoOferta($res);
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('ofertas');
  }

  $where=compruebaPerfilParaWhere('ofertas.codigoUsuario');
  $estadisticas=estadisticasGenericasWhere('ofertas',$where);
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de ofertas</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-tags"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Ofertas registradas</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Ofertas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="creaOferta.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Nueva oferta</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        
        <?php
          mensajeResultado('codigoProducto',$res,'oferta');//Fusionamos el mensaje de inserción y actualización
          mensajeResultado('elimina',$res,'oferta', true);
        ?>
		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Ofertas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Código de Oferta </th>
                    <th> Cliente </th>
                    <th> Producto ofrecido </th>
                    <th> Fecha de emisión </th>
                    <th> Estado </th>
                    <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeOfertas($where);
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">

	<?php
	echo "var marcado='".$empresa."';";
  ?>

    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes un cliente.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('ofertas.php',valoresChecks,'post');
      }

    });
	
	var tabla=$('.datatable').DataTable({
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
      "sPaginationType": "bootstrap",
      "bSort": false,//Añadido para evitar ordenación cuando se pulsa en checkbox (forem)
      "iDisplayLength":10,
      "ordering":true,
      "oLanguage": {
        "sLengthMenu": "_MENU_ registros por página",
        "sSearch":"Búsqueda:",
        "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
        "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
        "sEmptyTable":"Aún no hay datos que mostrar",
        "sInfoEmpty":"",
        'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
        'sZeroRecords':'No se han encontrado coincidencias'
    }});

    tabla.fnFilter(marcado);
	
</script>