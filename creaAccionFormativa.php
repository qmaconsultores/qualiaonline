<?php
  $seccionActiva=5;
  include_once('cabecera.php');
  include_once('matrizAcciones.php');
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nueva acción formativa</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="accionesFormativas.php" method="post">
                  <fieldset>
					
					          <div class="control-group">                     
                      <label class="control-label" for="codigoInterno">Acción:</label>
                      <div class="controls">
                        <input type="text" class="input-mini" id="codigoInterno" name="codigoInterno">
						<?php 
							campoOculto($_SESSION['codigoS'], 'codigoUsuario');
							$consulta=consultaBD("SELECT codigoInterno FROM accionFormativa;",true);
							$datosCodigos=mysql_fetch_assoc($consulta);
							$i=0;
							echo "<div id='div-padre'>";
							while(isset($datosCodigos['codigoInterno'])){
								divOculto($datosCodigos['codigoInterno'],'codigoInternoAccion'.$i);
								$i++;
								$datosCodigos=mysql_fetch_assoc($consulta);
							}
							echo "</div>";
						?>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="denominacion">Denominación:</label>
                      <div class="controls">
                        <input type="text" class="span4" id="denominacion" name="denominacion">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


					<?php 					
						campoTexto('urlAccion','URL');
						campoTexto('usuarioAccion','Usuario');
						campoTexto('claveAccion','Contraseña');
						campoSelect('grupoAcciones','Grupo de acciones',devuelveArrayTextos(),devuelveValores()); 
					?>




                    <div class="control-group">                     
                      <label class="control-label" for="tipoAccion">Tipo de acción:</label>
                      <div class="controls">
                        <select name="tipoAccion" class='selectpicker span4'>
                          <option value="PROPIA">Propia</option>
                          <option value="VINCULADA">Vinculada a la obtención de un Cert. de Profesionalidad</option>
                        </select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="nivelAccion">Nivel de acción:</label>
                      <div class="controls">
                        <select name="nivelAccion" class='selectpicker span4'>
                          <option value="BASICO">Básico</option>
                          <option value="SUPERIOR">Superior</option>
                        </select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="contenidos">Contenidos:</label>
                      <div class="controls">
						            <textarea name="contenidos" class="areaTextoAmplia"></textarea>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="objetivos">Objetivos:</label>
                      <div class="controls">
                        <textarea name="objetivos" class="areaTextoAmplia"></textarea>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="horas">Número de Horas:</label>
                      <div class="controls">
                        <input type="text" class="input-mini pagination-right" id="horas" name="horas">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="modalidad">Modalidad:</label>
                      <div class="controls">
                        
                        <select name="modalidad" class='selectpicker show-tick'>
                          <option value="PRESENCIAL">Presencial</option>
                          <option value="DISTANCIA">A distancia</option>
                          <option value="MIXTA">Mixta</option>
                          <option value="TELE">Teleformación</option>
                          <option value="WEBINAR">Webinar</option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->




                    <div class="form-actions">
                      <button id='registrarAccion' type="button" class="btn btn-primary"><i class="icon-ok"></i> Registrar acción formativa</button> 
                      <a href="accionesFormativas.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
	$('#registrarAccion').click(function(){
		var j=0;
		var booleano=false;
		$("#div-padre div").each(function (){
			if($(this).text()==$('#codigoInterno').val() || $('#codigoInterno').val()==''){
				booleano=true;
			}
		});
		if(booleano==true){
			alert("Ya existe una acción formativa con el código indicado.");
		}else{	
			$('form').submit();
		}
	});
  });
</script>
