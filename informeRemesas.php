<?php
  $seccionActiva=7;
  include_once('cabecera.php');
  $res=false;
    
  //$estadisticasVentas=creaEstadisticasVentasInicio();
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
		
		<div class="span12">
		  <div class="widget widget-nopad" id="target-1">
			<div class="widget-header"> <i class="icon-tasks"></i>
			  <h3>Ventas realizadas por comercial</h3>
			</div>
			<!-- /widget-header -->
			
			<div class="widget-content">
			  <div class="widget big-stats-container">
				<div class="widget-content">
				  <h6 class="bigstats">Ventas realizadas:</h6>


					<span id='resultadoDos'></span>

				</div> <!-- /widget-content -->
				<!-- /widget-content --> 
				
			  </div>
			</div>
		  </div>
		 
		</div>
		
		
	</div><!-- /row -->
	
		<div class="widget widget-table action-table">
		<div class="widget-header"> <i class="icon-th-list"></i>
		  <h3>Remesas realizadas</h3>
		</div>
		<!-- /widget-header -->
		<div class="widget-content">
			<center>
				<div class='seleccionComercial'>
					<strong>Firma: </strong>
					<?php 
						echo "<select name='firma' id='firma' class='input-large'>";
						$consulta=consultaBD('SELECT codigo, firma AS texto FROM firmas',true);
						while($firma=mysql_fetch_assoc($consulta)){
							$firmas[$firma['codigo']]=$firma['texto'];
						}
						echo "<option value='todos'>Todas</option>";
						foreach ($firmas as $key => $value) {
							echo "<option value='".$key."'>".$value."</option>";
						}
						
						echo "</select>";
					?>
					<strong>Tipo de remesa: </strong>
					<?php 
						echo "<select name='tipo' id='tipo' class='input-large'>";
			
						$resoluciones=array('19143'=>'CORE','19445'=>'B2B');
						echo "<option value='todos'>Todos</option>";
						foreach ($resoluciones as $key => $value) {
							echo "<option value='".$key."'>".$value."</option>";
						}
						
						echo "</select>";
					?>
					<strong>Banco receptor: </strong>
					<?php 
						echo "<select name='banco' id='banco' class='input-large'>";
			
						$resoluciones=array('SABADELL','BBVA');
						echo "<option value='todos'>Todos</option>";
						foreach ($resoluciones as $resolucion) {
							echo "<option value='".$resolucion."'>".$resolucion."</option>";
						}
						
						echo "</select>";
					?>
					<br/><strong>Año: </strong>
					<?php 
						echo "<select name='anio' id='anio' class='input-large'>";
						echo "<option value='0'></option>";
						for ($i=2015;$i <=date('Y') ; $i++) { 
							echo "<option value='".$i."'>".$i."</option>";
						}
						
						echo "</select>";
					?>
					<strong>Mes: </strong>
					<?php 
						echo "<select name='mes' id='mes' class='input-large'>";
						echo "<option value='0'></option>";
						echo "<option value='01'>Enero</option>";
						echo "<option value='02'>Febrero</option>";
						echo "<option value='03'>Marzo</option>";
						echo "<option value='04'>Abril</option>";
						echo "<option value='05'>Mayo</option>";
						echo "<option value='06'>Junio</option>";
						echo "<option value='07'>Julio</option>";
						echo "<option value='08'>Agosto</option>";
						echo "<option value='09'>Septiembre</option>";
						echo "<option value='10'>Octubre</option>";
						echo "<option value='11'>Noviembre</option>";
						echo "<option value='12'>Diciembre</option>";
						echo "</select>";
					?>
				</div>
			</center>
			<span id='resultado'></span>
			
		</div>
		<!-- /widget-content-->
	  </div>

    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/guidely/guidely.min.js"></script>
<script type="text/javascript" src="js/bootstrap-progressbar.js"></script>

<script type="text/javascript" src="js/full-calendar/fullcalendar.min.js"></script>

<script type="text/javascript">
   
   	$('#firma').change(function(){
		var tipo=$('#tipo').val();
		var banco=$('#banco').val();
		var mes=$('#mes').val();
		var anio=$('#anio').val();
		var firma=$('#firma').val();
		listadoVentas(tipo,mes,banco,anio,firma)
		muestraCantidades(tipo,mes,banco,anio,firma);
	});

	$('#tipo').change(function(){
		var tipo=$('#tipo').val();
		var banco=$('#banco').val();
		var mes=$('#mes').val();
		var anio=$('#anio').val();
		var firma=$('#firma').val();
		listadoVentas(tipo,mes,banco,anio,firma)
		muestraCantidades(tipo,mes,banco,anio,firma);
	});	

	$('#banco').change(function(){
		var tipo=$('#tipo').val();
		var banco=$('#banco').val();
		var mes=$('#mes').val();
		var anio=$('#anio').val();
		var firma=$('#firma').val();
		listadoVentas(tipo,mes,banco,anio,firma)
		muestraCantidades(tipo,mes,banco,anio,firma);
	});	

	$('#mes').change(function(){
		var tipo=$('#tipo').val();
		var banco=$('#banco').val();
		var mes=$('#mes').val();
		var anio=$('#anio').val();
		var firma=$('#firma').val();
		listadoVentas(tipo,mes,banco,anio,firma)
		muestraCantidades(tipo,mes,banco,anio,firma);
	});	

	$('#anio').change(function(){
		var tipo=$('#tipo').val();
		var banco=$('#banco').val();
		var mes=$('#mes').val();
		var anio=$('#anio').val();
		var firma=$('#firma').val();
		listadoVentas(tipo,mes,banco,anio,firma)
		muestraCantidades(tipo,mes,banco,anio,firma);
	});	
	  
	var tipo=$('#tipo').val();
	var mes=$('#mes').val();
	var banco=$('#banco').val();
	var anio=$('#anio').val();
	var firma=$('#firma').val();
	$(document).ready(listadoVentas(tipo,mes,banco,anio,firma));
	$(document).ready(muestraCantidades(tipo,mes,banco,anio,firma));
	
	function listadoVentas(tipo,mes, banco, anio, firma){
		var parametros = {
                "tipo" : tipo,
                "mes" 		: mes,
                "banco": banco,
                "anio": anio,
                "firma": firma
        };
		 $.ajax({
			 type: "POST",
			 url: "listadosajax/listadoRemesas.php",
			 data: parametros,
			  beforeSend: function () {
				   $("#resultado").html('<center><div class="seleccionComercial"><i class="icon-refresh icon-spin"></i></div></center>');
			  },
			 success: function(response){
				   $("#resultado").html(response);
			 }
		});
	  }
	  
	function muestraCantidades(tipo,mes,banco,anio, firma){
		var parametros = {
                "tipo" : tipo,
                "mes" 		: mes,
                "banco": banco,
                "anio": anio,
                "firma": firma
        };
		$.ajax({
			 type: "POST",
			 url: "listadosajax/muestraCantidadesRemesas.php",
			 data: parametros,
			  beforeSend: function () {
				   $("#resultadoDos").html('<center><i class="icon-refresh icon-spin"></i></center>');
			  },
			 success: function(response){
				   $("#resultadoDos").html(response);
			 }
		});
	}
</script>