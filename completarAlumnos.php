<?php
  $seccionActiva=2;
  include_once('cabecera.php');
  $codigoVenta=registraVentaAuxi();
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
  
  if(isset($_GET['venta'])){
    $destino='ventas.php';
	$destinoCancela='ventas.php';
  }
  else{
	if(isset($_GET['activa'])){
		$destinoCancela='posiblesClientes.php';
	}else{
		$destinoCancela='cuentas.php';
	}
	$destino='cuentas.php';
  }
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">     
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Completar alumnos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="<?php echo $destino; ?>" method="post" enctype="multipart/form-data">
                  
                  <div class="tabbable">
                    <ul class="nav nav-tabs">
					  <?php
						$i=0;
						while(isset($_POST['nombre'.$i])){
							$j=$i+1;
							echo "<li ";if($i==0){ echo "class='active'";} echo"><a href='#pagina$j' data-toggle='tab'>Alumno $j</a></li>";
							$i++;
						}
						campoOculto('SI','completaAlumnos');
					  ?>
                    </ul>
                  
                    <br>

                    <div class="tab-content" id="pestanas">

					<?php 
					$i=0;
					$datosAlumnos=consultaBD("SELECT * FROM alumnos WHERE codigoVenta='$codigoVenta';",true);
					$datosRegistro=mysql_fetch_assoc($datosAlumnos);
					while(isset($_POST['nombre'.$i])){ 
						campoOculto($datosRegistro['codigo'], 'codigo'.$i);
					?>
						<div class="tab-pane active" id="pagina<?php echo $i+1; ?>">
						  <fieldset class="span3">
						  
							<div class="control-group">                     
							  <label class="control-label" for="nombre<?php echo $i; ?>">Nombre:</label>
							  <div class="controls">
								<input type="text" class="input-large" id="nombre<?php echo $i; ?>" name="nombre<?php echo $i; ?>" value="<?php echo $datosRegistro['nombre']; ?>">
							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->

							
							<div class="control-group">                     
							  <label class="control-label" for="apellidos<?php echo $i; ?>">Apellidos:</label>
							  <div class="controls">
								<input type="text" class="input-large" id="apellidos<?php echo $i; ?>" name="apellidos<?php echo $i; ?>" value="<?php echo $datosRegistro['apellidos']; ?>">
							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->


							<div class="control-group">                     
							  <label class="control-label" for="dni<?php echo $i; ?>">DNI:</label>
							  <div class="controls">
								<input type="text" class="input-small" id="dni<?php echo $i; ?>" name="dni<?php echo $i; ?>" value="<?php echo $datosRegistro['dni']; ?>">
							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->



							<div class="control-group">                     
							  <label class="control-label" for="sexo<?php echo $i; ?>">Sexo:</label>
							  <div class="controls">
								
								<label class="radio inline">
								  <input type="radio" name="sexo<?php echo $i; ?>" value="M" checked="checked"> Hombre
								</label>
								
								<label class="radio inline">
								  <input type="radio" name="sexo<?php echo $i; ?>" value="F"> Mujer
								</label>

							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->



							<div class="control-group">                     
							  <label class="control-label" for="fechaNac<?php echo $i; ?>">Fecha de nacimiento:</label>
							  <div class="controls">
								<input type="text" class="input-small datepicker hasDatepicker" id="fechaNac<?php echo $i; ?>" name="fechaNac<?php echo $i; ?>" value="<?php echo imprimeFecha(); ?>">
							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->





							<div class="control-group">                     
							  <label class="control-label" for="telefono<?php echo $i; ?>">Teléfono:</label>
							  <div class="controls">
								<input type="text" class="input-small" id="telefono<?php echo $i; ?>" name="telefono<?php echo $i; ?>" value="<?php echo $datosRegistro['telefono']; ?>">
							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->



							<div class="control-group">                     
							  <label class="control-label" for="movil<?php echo $i; ?>">Móvil:</label>
							  <div class="controls">
								<input type="text" class="input-small" id="movil<?php echo $i; ?>" name="movil<?php echo $i; ?>">
							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->



							<div class="control-group">                     
							  <label class="control-label" for="mail<?php echo $i; ?>">eMail:</label>
							  <div class="controls">
								<input type="text" class="input-large" id="mail<?php echo $i; ?>" name="mail<?php echo $i; ?>" value="<?php echo $datosRegistro['mail']; ?>">
							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->


						  </fieldset>
						  <fieldset class="span3">


							<div class="control-group">                     
							  <label class="control-label" for="numSS1<?php echo $i; ?>">Nº Seg. Social:</label>
							  <div class="controls numSS">
								<div class="input-prepend input-append">
								  <input type="text" class="input-supermini" id="numSS1<?php echo $i; ?>" name="numSS1<?php echo $i; ?>" maxlength="2">
								  <span class="add-on">/</span>
								  <input type="text" class="input-small" id="numSS2<?php echo $i; ?>" name="numSS2<?php echo $i; ?>" maxlength="10">
								</div>
							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->

							<?php
								campoSelect('estudios'.$i,'Nivel de estudios:',array('1. Menos que primaria','2. Educación primaria','3. ESO, EGB, Cert. Prof. (1y2)',
								'4. Bachillerato, BUP, FP (I y II)','5. Cert. Prof. de Nivel 3','6. Técnico Superior','7. -1º Ciclo Uni. (Diplomatura-Grados)',
								'8. -2º Ciclo Uni. (Licenciatura-Master)','9. -3º Ciclo Uni. (Doctorado)','10. Otras Titulaciones'),array('sin','primarios','3',
								'bachiller','5','6','diplomatura','licenciatura','9','10'));
							?>



							<div class="control-group">                     
							  <label class="control-label" for="categoria<?php echo $i; ?>">Categoría:</label>
							  <div class="controls">
								
								<select name="categoria<?php echo $i; ?>" class='selectpicker show-tick'>
								  <option value="directivo">Directivo</option>
								  <option value="intermedio">Mando Intermedio</option>
								  <option value="tecnico">Técnico</option>
								  <option value="cualificado">Trabajador Cualificado</option>
								  <option value="bajaCualificacion">Trabajador no Cualificado</option>
								</select>

							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->



							<div class="control-group">                     
							  <label class="control-label" for="cotizacion<?php echo $i; ?>">Grupo de Cotización:</label>
							  <div class="controls">
								
								<select name="cotizacion<?php echo $i; ?>" class='selectpicker show-tick'>
								  <option value="1">1. Ingenieros y Licenciados</option>
								  <option value="2">2. Ingenieros Técnicos</option>
								  <option value="3">3. Jefes Administrativos y de taller</option>
								  <option value="4">4. Ayudantes no Titulados</option>
								  <option value="5">5. Oficiales Administrativos</option>
								  <option value="6">6. Subalternos</option>
								  <option value="7">7. Auxiliares Administrativos</option>
								  <option value="8">8. Oficiales de Primera y Segunda</option>
								  <option value="9">9. Oficiales de Tercera y Especialistas</option>
								  <option value="10">10. Peones</option>
								  <option value="11">1. Trabajadores menores de 18 años</option>
								</select>

							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->



							<div class="control-group">                     
							  <label class="control-label" for="horario<?php echo $i; ?>">Horario de trabajo:</label>
							  <div class="controls">
								<input type="text" class="input-large" id="horario<?php echo $i; ?>" name="horario<?php echo $i; ?>">
							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->



							<div class="control-group">                     
							  <label class="control-label" for="discapacidad<?php echo $i; ?>">Discapacidad:</label>
							  <div class="controls">
								
								<label class="radio inline">
								  <input type="radio" name="discapacidad<?php echo $i; ?>" value="SI"> Si
								</label>
								
								<label class="radio inline">
								  <input type="radio" name="discapacidad<?php echo $i; ?>" value="NO" checked="checked"> No
								</label>

							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->



							<div class="control-group">                     
							  <label class="control-label" for="discapacidad<?php echo $i; ?>">Víctima de terrorismo:</label>
							  <div class="controls">
								
								<label class="radio inline">
								  <input type="radio" name="vTerrorismo<?php echo $i; ?>" value="SI"> Si
								</label>
								
								<label class="radio inline">
								  <input type="radio" name="vTerrorismo<?php echo $i; ?>" value="NO" checked="checked"> No
								</label>

							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->




							<div class="control-group">                     
							  <label class="control-label" for="vViolencia<?php echo $i; ?>">Víctima de violencia de género:</label>
							  <div class="controls">
								
								<label class="radio inline">
								  <input type="radio" name="vViolencia<?php echo $i; ?>" value="SI"> Si
								</label>
								
								<label class="radio inline">
								  <input type="radio" name="vViolencia<?php echo $i; ?>" value="NO" checked="checked"> No
								</label>

							  </div> <!-- /controls -->       
							</div> <!-- /control-group -->

						  </fieldset>
						
						</div>
					<?php 
							$i++;
							$datosRegistro=mysql_fetch_assoc($datosAlumnos);
						}
					?>
                  </div>
                </div>


                  <fieldset class="sinFlotar">
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Completar alumnos</button> 
                      <a href="<?php echo $destinoCancela; ?>" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});

    $('#botonSubida').click(function(){
      if(!$(this).attr('disabled')){
        $('#fichero').click();
      }
    });

  });

function nuevaFila(){
    $('table').find("tr:last").after("<tr id='fila'><td><input type='text' name='titulo' class='input-large' /></td><td><input type='text' name='fechaSubida' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' onclick='enviaFormulario();' class='btn btn-primary'><i class='icon icon-upload'></i> Subir fichero</a> <a href='javascript:void' onclick='eliminaFila();' class='btn btn-warning'><i class='icon icon-ban-circle'></i> Cancelar</a></td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
    $('#botonSubida').attr('disabled',true);
  }

  function eliminaFila(){
    $('table').find("tr:last").remove();
    $('#botonSubida').attr('disabled',false);
  }

  function enviaFormulario(){
    $('form').submit();
  }
</script>
