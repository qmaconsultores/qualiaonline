<?php	
	require_once("dompdf-master/dompdf_config.inc.php");
	require_once 'src/PhpWord/Autoloader.php';
	\PhpOffice\PhpWord\Autoloader::register();
	
	$PHPWord = new \PhpOffice\PhpWord\PhpWord();
	$document = $PHPWord->loadTemplate('documentos/plantillaFacturaCursos.docx');
	$tiempo=time();
	$document->saveAs('documentos/Factura-'.$tiempo.'.docx');
	
	$PHPWord = \PhpOffice\PhpWord\IOFactory::load('documentos/Factura-'.$tiempo.'.docx'); 
	$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($PHPWord , 'HTML');
	$xmlWriter->save('documentos/result.html');  
	
	$dompdf = new DOMPDF();
	$dompdf->load_html_file('documentos/result.html');
	$dompdf->render();
	$dompdf->stream("sample.pdf");


?>