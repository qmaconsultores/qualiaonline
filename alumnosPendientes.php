<?php
  $seccionActiva=6;
  include_once('cabecera.php');
  
  if(isset($_POST['codigo'])){
    $res=actualizaAlumno();
  }
  elseif(isset($_POST['nombre'])){
    $res=insertaAlumnoSinVenta();
  }
  elseif(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaAlumno();
  }

  $estadisticas=creaEstadisticasAlumnosPendientes();
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de alumnos pendientes de asignar a cursos</h6>

                   <div id="big_stats" class="cf">

                     <div class="stat"> <i class="icon-pencil"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Alumnos pendientes</div>
                      <!-- .stat -->

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de alumnos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="formacion.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Alumnos en curso</span> </a>
				<a href="alumnosFinalizados.php" class="shortcut"><i class="shortcut-icon icon-archive"></i><span class="shortcut-label">Alumnos finalizados</span> </a>
				<br>
				<a href="creaAlumno.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Nuevo alumno</span> </a>
                <a href="#" id='email' class="shortcut"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Enviar eMail</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Datos del alumno actualizados."); 
            }
            else{
              mensajeError("no se ha podido actualizar el alumno. Compruebe los datos introducidos."); 
            }
          }
		  elseif(isset($_POST['nombre'])){
            if($res){
              mensajeOk("Alumno registrado correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar el alumno. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['destinatarios'])){
            if($res){
              mensajeOk("Mensaje enviado correctamente."); 
            }
            else{
              mensajeError("no se ha podido enviar el mensaje. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el posible cliente. Contacte con el webmaster."); 
            }
          }
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Alumnos pendientes</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Nombre </th>
                    <th> Teléfono </th>
                    <th> eMail </th>
                    <th> Cliente </th>
                    <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeAlumnosPendientes();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
    $('#email').click(function(){
      var valoresChecks=recorreChecks();
      creaFormulario('enviarEmail.php?seccion=6',valoresChecks,'post');
    });


    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes un cliente.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('alumnosPendientes.php',valoresChecks,'post');
      }

    });

</script>