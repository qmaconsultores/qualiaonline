ALTER TABLE  `cursos` ADD  `formacionCurso` ENUM(  'NO',  'HECHO',  'PROCESO',  'FINALIZADO' ) NOT NULL AFTER  `medios`;

ALTER TABLE  `clientes` ADD  `cnae` VARCHAR( 200 ) NOT NULL AFTER  `nifResponsable`;

ALTER TABLE  `clientes` ADD  `mail2` VARCHAR( 200 ) NOT NULL AFTER  `cnae` ,
ADD  `web` VARCHAR( 200 ) NOT NULL AFTER  `mail2`;

ALTER TABLE  `clientes` ADD  `trabajadores` ENUM(  'NO',  'SI' ) NOT NULL AFTER  `web`;

ALTER TABLE  `usuarios` ADD  `activoUsuario` ENUM(  'SI',  'NO' ) NOT NULL AFTER  `porcentajeTeleventa`;

ALTER TABLE  `clientes` ADD  `motivoBaja` VARCHAR( 100 ) NOT NULL AFTER  `trabajadores`;

ALTER TABLE  `ventas` ADD  `tipoVentaCarteraNueva` ENUM(  'CARTERA',  'NUEVA' ) NOT NULL AFTER  `comision`;

ALTER TABLE  `clientes` ADD  `esUnGrupo` ENUM(  'NO',  'SI' ) NOT NULL AFTER  `motivoBaja` ,
ADD  `codigoVinculacion` INT( 255 ) NULL AFTER  `esUnGrupo` ,
ADD INDEX (  `codigoVinculacion` );