ALTER TABLE  `usuarios` ADD  `tipoProfesional` ENUM(  'Nomina',  'Profesional' ) NOT NULL AFTER  `colorTareas` ,
ADD  `objetivo` VARCHAR( 100 ) NOT NULL AFTER  `tipoProfesional` ,
ADD  `porcentajeNueva` VARCHAR( 100 ) NOT NULL AFTER  `objetivo` ,
ADD  `porcentajeCartera` VARCHAR( 100 ) NOT NULL AFTER  `porcentajeNueva` ,
ADD  `porcentajeAuditoria` VARCHAR( 100 ) NOT NULL AFTER  `porcentajeCartera` ,
ADD  `porcentajeTeleventa` VARCHAR( 100 ) NOT NULL AFTER  `porcentajeAuditoria`;

ALTER TABLE  `facturacion` ADD  `tipoFacturaComercial` ENUM(  'CARTERA',  'NUEVA',  'AUDITORIA',  'TELEVENTA' ) NOT NULL AFTER  `totalTres`;