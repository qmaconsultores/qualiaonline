<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común


  	//$_GET['codigo'] viene en la URL a través de un botón "Descargar Contrato"
	$datos=datosRegistro('alumnos',$_GET['codigo']);
	conexionBD();
	$datosCurso=consultaBD("SELECT cursos.* FROM cursos INNER JOIN alumnos_registrados_cursos ON cursos.codigo=alumnos_registrados_cursos.codigoCurso WHERE alumnos_registrados_cursos.codigoAlumno='".$_GET['codigo']."';");
	$datosCurso=mysql_fetch_assoc($datosCurso);
	$datosAccionFormativa=consultaBD("SELECT accionFormativa.* FROM accionFormativa INNER JOIN cursos ON cursos.codigoaccionFormativa=accionFormativa.codigoInterno WHERE cursos.codigo='".$datosCurso['codigo']."';");
	$datosAccionFormativa=mysql_fetch_assoc($datosAccionFormativa);
	$datosUsuario=consultaBD("SELECT usuarios.* FROM usuarios INNER JOIN clientes ON clientes.codigoUsuario=usuarios.codigo INNER JOIN ventas ON ventas.codigoCliente=clientes.codigo INNER JOIN alumnos ON alumnos.codigoVenta=ventas.codigo WHERE alumnos.codigo='".$datos['codigo']."';");
	$datosUsuario=mysql_fetch_assoc($datosUsuario);
	$datosEmpresa=consultaBD("SELECT clientes.* FROM clientes INNER JOIN ventas ON ventas.codigoCliente=clientes.codigo INNER JOIN alumnos ON alumnos.codigoVenta=ventas.codigo WHERE alumnos.codigo='".$datos['codigo']."';");
	$datosEmpresa=mysql_fetch_assoc($datosEmpresa);
	cierraBD();
	
	//Carga de la librería PHPWord
	require_once 'phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('documentos/plantillaAlumno.docx');

	/*
	Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
	En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
	*/
	
	
	// PARTE ALUMNO
	$document->setValue("apellidos",utf8_decode($datos['apellidos']));
	$document->setValue("nombre",utf8_decode($datos['nombre']));
	$document->setValue("dni",utf8_decode($datos['dni']));
	$document->setValue("numSS",utf8_decode($datos['numSS']));
	$discapacidad=array('NO'=>'No','SI'=>'Si');
	$document->setValue("discapacidad",utf8_decode($discapacidad[$datos['discapacidad']]));
	$document->setValue("sexo",utf8_decode($datos['sexo']));
	$document->setValue("fechaNac",utf8_decode(formateaFechaWeb($datos['fechaNac'])));
	$document->setValue("tlf",utf8_decode($datos['telefono']));
	$document->setValue("mail",utf8_decode($datos['mail']));
	$estudios=array('sin'=>'Sin estudios','primarios'=>'Estudios Primarios, EGB o equivalente','bachiller'=>'F.P., Bachillerato o equivalente','diplomatura'=>'Diplomatura','licenciatura'=>'Licenciatura');
	$document->setValue("estudios",utf8_decode($estudios[$datos['estudios']]));
	$categoria=array('directivo'=>'Directivo','intermedio'=>'Mando Intermedio','tecnico'=>'Técnico','cualificado'=>'Trabajador Cualificado','bajaCualificacion'=>'Trabajador con Baja Cualificación');
	$document->setValue("categoria",utf8_decode($categoria[$datos['categoria']]));
	$cotizacion=array('1'=>'1. Ingenieros y Licenciados','2'=>'2. Ingenieros Técnicos','3'=>'3. Jefes Administrativos y de taller','4'=>'4. Ayudantes no Titulados','5'=>'5. Oficiales Administrativos','6'=>'6. Subalternos','7'=>'7. Auxiliares Administrativos','8'=>'8. Oficiales de Primera y Segunda','9'=>'9. Oficiales de Tercera y Especialistas','10'=>'10. Peones','11'=>'11. Trabajadores menores de 18 años');
	$document->setValue("grupoCotizacion",utf8_decode($cotizacion[$datos['cotizacion']]));
	/****************************************/
	
	//PARTE ACCIÓN FORMATIVA
	$document->setValue("fecha",utf8_decode(fecha()));
	$accionFormativa=$datosAccionFormativa['codigoInterno'].'/'.$datosCurso['codigoInterno'];
	$document->setValue("accionFormativa",utf8_decode($accionFormativa));
	$document->setValue("comercial",utf8_decode($datosUsuario['nombre'].' '.$datosUsuario['apellidos']));
	$document->setValue("telefonoComercial",utf8_decode($datosUsuario['telefono']));
	/*****************************************/
	
	//PARTE EMPRESA
	$document->setValue("empresa",utf8_decode($datosEmpresa['empresa']));
	$document->setValue("ccc",utf8_decode($datosEmpresa['ccc']));
	$document->setValue("cif",utf8_decode($datosEmpresa['cif']));
	$document->setValue("domicilio",utf8_decode($datosEmpresa['direccion']));
	$document->setValue("localidad",utf8_decode($datosEmpresa['localidad']));
	$document->setValue("provincia",utf8_decode($datosEmpresa['provincia']));
	$document->setValue("cp",utf8_decode($datosEmpresa['cp']));
	$document->setValue("telefono",utf8_decode($datosEmpresa['telefono']));
	$document->setValue("mail",utf8_decode($datosEmpresa['mail']));
	$document->setValue("empresa",utf8_decode($datosEmpresa['empresa']));
	$document->setValue("convenio",utf8_decode($datosEmpresa['convenio']));
	$document->setValue("desConvenio",utf8_decode($datosEmpresa['desConvenio']));
	$document->setValue("cnae",utf8_decode($datosEmpresa['cnae']));
	$document->setValue("desCnae",utf8_decode($datosEmpresa['desCnae']));
	$document->setValue("personaContacto",utf8_decode($datosEmpresa['contacto']));
	$document->setValue("cargo",utf8_decode($datosEmpresa['cargo']));
	$rlt=array('NO'=>'No','SI'=>'Si');
	$document->setValue("rlt",utf8_decode($rlt[$datosEmpresa['representacion']]));
	$document->setValue("pyme",utf8_decode($rlt[$datosEmpresa['pyme']]));
	$document->setValue("nuevaCreacion",utf8_decode($rlt[$datosEmpresa['nuevacreacion']]));
	$document->setValue("fechaCreacion",utf8_decode(formateaFechaWeb($datosEmpresa['fechaCreacion'])));
	/****************************************/
	
	// ACCIÓN FORMATIVA
	$document->setValue("accion",utf8_decode($datosAccionFormativa['denominacion']));
	$document->setValue("duracion",utf8_decode($datosAccionFormativa['horas']));
	$document->setValue("precio",utf8_decode(''));
	$modalidad=array('PRESENCIAL'=>'Presencial', 'DISTANCIA'=>'A distancia', 'MIXTA'=>'Mixta', 'TELE'=>'Teleformación');
	$document->setValue("modalidad",utf8_decode($modalidad[$datosAccionFormativa['modalidad']]));
	/****************************************/

	/*
	Cuando ya has sustituido todas las etiquetas por su valores, el método save
	guarda el fichero resultane en la ruta que le indiques con el nombre que le
	indiques.
	*/
	$tiempo=time();
	$document->save('documentos/Ficha.docx');


	/*
	Definir headers.
	Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
	que lo que va a recibir es un fichero que debe descargar.
	*/
	header("Content-Type: application/vnd.ms-docx");
	header("Content-Disposition: attachment; filename=Ficha.docx");
	header("Content-Transfer-Encoding: binary");

	/* 
	Descargar archivo.
	Por último, le decirmos a PHP que lea y transfiera el documento que
	antes con save.
	*/
	readfile('documentos/Ficha.docx');


?>