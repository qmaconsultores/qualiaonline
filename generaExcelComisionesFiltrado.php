<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos/plantillaExcelColaboradores.xlsx");
	
	conexionBD();
	$tiempo=time();
	$j=7;
	$totalComisionado=0;
	
	//COMIENZO DE TODOS LAS FACTURAS COMPRENDIDAS
	$datosColaborador=datosRegistro('colaboradores',$_GET['colaborador']);
	$objPHPExcel->getActiveSheet()->getCell('B3')->setValue($datosColaborador['empresa']);
	conexionBD();
	
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.referencia, facturacion.concepto, coste, empresa, fechaEmision, fechaVencimiento, numCuenta, clientes.referencia AS refCliente, clientes.cif AS cifCliente, clientes.empresa, clientes.direccion, clientes.cp, clientes.localidad, clientes.empresa, clientes.provincia, clientes.bic, facturacion.firma, usuarios.nombre, usuarios.apellidos, facturacion.codigoCliente, facturacion.cobrada, facturacion.codigoVenta, facturacion.formaPago 
		FROM facturacion LEFT JOIN ventas ON facturacion.codigoVenta=ventas.codigo
		LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo
		LEFT JOIN usuarios ON usuarios.codigo=ventas.codigoUsuario WHERE facturacion.codigoCliente IN(SELECT codigo FROM clientes WHERE tieneColaborador='SI' AND comercial='".$_GET['colaborador']."') AND fechaVencimiento>='".$_GET['fechaUno']."' AND fechaVencimiento<='".$_GET['fechaDos']."';");
		$datos=mysql_fetch_assoc($consulta);
		
	$concepto=array("formacionBonificada"=>"Formación Bonificada", "pif"=>"PIF", "iso"=>"ISO", "lopd"=>"LOPD", "trazabilidad"=>"Trazabilidad", "blanqueo"=>"Blanqueo", "software"=>"Software", "web"=>"Web", "gestoria"=>"Gestoría", "formacion"=>"Formación",'prl'=>'PRL','alergenos'=>'ALÉRGENOS');
	while(isset($datos['codigo'])){		
		
		$objPHPExcel->getActiveSheet()->getCell('B'.$j)->setValue($datos['refCliente']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$j)->setValue($datos['referencia']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$j)->setValue($datos['empresa']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$j)->setValue($concepto[$datos['concepto']]);
		$objPHPExcel->getActiveSheet()->getCell('F'.$j)->setValue(formateaFechaWeb($datos['fechaVencimiento']));
		$objPHPExcel->getActiveSheet()->getCell('G'.$j)->setValue(number_format((float)$datos['coste'], 2, ',', '').' €');
		$objPHPExcel->getActiveSheet()->getCell('H'.$j)->setValue($datosColaborador['comision'].' %');
		$objPHPExcel->getActiveSheet()->getCell('I'.$j)->setValue(number_format((float) $datos['coste']*($datosColaborador['comision']/100), 2, ',', '').' €');
	
		$totalComisionado+=$datos['coste']*($datosColaborador['comision']/100);
		$datos=mysql_fetch_assoc($consulta);
		$j++;
	}
	cierraBD();
	
	$j+=4;
	$objPHPExcel->getActiveSheet()->getCell('E'.$j)->setValue('TOTAL LIQUIDACIÓN TRIMESTRAL');
	$objPHPExcel->getActiveSheet()->getCell('I'.$j)->setValue(number_format((float)$totalComisionado, 2, ',', '').' €');

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	
	$objWriter->save('documentos/comisiones.xlsx');

	/*
	// Definir headers
	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=Facturacion-".$tiempo.".zip");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/Facturacion-'.$tiempo.'.zip');*/

	
	
	// Definir headers
	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=comisiones.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/comisiones.xlsx');
?>