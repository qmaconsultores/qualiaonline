<?php
  $seccionActiva=12;
  include_once('cabecera.php');

  $datos=datosRegistro('ofertas',$_GET['codigo']);
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Detalles de la Oferta</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="ofertas.php" method="post">
                  <fieldset>

                    <?php
                      $consulta="SELECT codigo, empresa AS texto FROM clientes;";
                      campoSelectConsulta('codigoCliente','Cliente',$consulta,$datos,'selectpicker span4 show-tick');
                      
                      $consulta="SELECT codigo, CONCAT(codigoProducto,' - ',nombreProducto) AS texto FROM productos;";
                      campoSelectConsulta('codigoProducto','Producto',$consulta,$datos,'selectpicker span4 show-tick');

                      campoSelect('estado','Estado',array('En curso','Rechazada'),array('CURSO','RECHAZADA'),$datos);

                      campoOculto($datos);
                      campoOculto($datos,'codigoOferta');
                      campoTextoSimbolo('precio','Precio de la oferta','€',$datos);
                      campoFecha('fechaOferta','Fecha de emisión',$datos);
                      areaTexto('observaciones','Observaciones',$datos);
                    ?>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar Oferta</button> 
                      <a href="ofertas.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
  });
</script>