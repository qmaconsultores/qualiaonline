<?php
  $seccionActiva=22;
  include_once('cabecera.php');
  
  if(isset($_POST['codigo'])){
    $res=actualizaDatos('gestorias');
  }
  elseif(isset($_POST['nombreGestoria'])){
    $res=insertaDatos('gestorias');
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('gestorias');
  }
  $estadisticas=estadisticasGenericas('gestorias');
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestorías no colaboradoras</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-briefcase"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Gestorías registradas</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		    <?php
        if($_SESSION['tipoUsuario'] != 'FORMACION'){
        ?>
        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de gestorías</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="creaGestoria.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Agregar gestoría</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
        <?php 
        }
        ?>

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Datos de la gestoría actualizados."); 
            }
            else{
              mensajeError("no se han podido actualizar los datos de la gestoría. Compruebe los datos introducidos."); 
            }
          }
		  elseif(isset($_POST['nombreGestoria'])){
            if($res){
              mensajeOk("Gestoría registrada correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar la gestoría. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar la gestoría. Contacte con el webmaster."); 
            }
          }
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Gestorías registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
				 <table class='table table-striped table-bordered datatable' id='tablaGestorias'>
					<thead>
					  <tr>
						<th> Nombre </th>
						<th> Cliente </th>
						<th> Correo electrónico </th>
						<th> Teléfono </th>
            <?php
              if($_SESSION['tipoUsuario'] != 'FORMACION'){
            ?>
						<th class='td-actions'> </th>
						<th><input type='checkbox' id='todo'></th>
            <?php
            }
            ?>
					  </tr>
					</thead>
					<tbody>
						
					</tbody>
              </table>
                
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<!--script src="js/filtroTabla.js"></script-->
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/excanvas.min.js" type="text/javascript"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#tablaGestorias').dataTable({
      'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoGestorias.php?action=getMembersAjx',
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          $('td:eq(8)', nRow).addClass( "centro" );
       },
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });

  $('#email').click(function(){
    var valoresChecks=recorreChecks();
    creaFormulario('enviarEmail.php?seccion=2',valoresChecks,'post');
  });


  $('#tareas').click(function(){
      var valoresChecks=recorreChecks();
      creaFormulario('creaTarea.php',valoresChecks,'post');
  });


  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes una gestoría.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('gestorias.php',valoresChecks,'post');
    }

  });
</script>
