<?php
  $seccionActiva=2;
  include_once('cabecera.php');
  
  if(isset($_POST['codigo'])){
    $res=actualizaVenta();
  }
  elseif(isset($_POST['completaAlumnos'])){
    $res=completaAlumnos();
  }
  elseif(isset($_POST['codigoC'])){
    $res=registraVenta();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaVenta();
  }
  
  if(isset($_GET['codigo'])){//La primera vez que entra en la sección (desde cuentas.php)
    $_SESSION['codigoClienteVenta']=$_GET['codigo'];//Guarda el código en una variable de sesión, para acceder a él durante la navegación
  }
  $codigo=$_SESSION['codigoClienteVenta'];

  $datosCliente=datosCliente($codigo);
  
  $estadisticas=creaEstadisticasVentas($codigo);

?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de ventas realizadas al cliente <a href='detallesCuenta.php?codigo=<?php echo $codigo;?>'><?php echo $datosCliente['empresa']; ?></a></h6>

                   <div id="big_stats" class="cf">

                    <div class="stat"> <i class="icon-shopping-cart"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Ventas realizadas</div>
                    <!-- .stat -->
					
                    <?php if($_SESSION['tipoUsuario']!='ADMINISTRACION'){ ?>
						<div class="stat"> <i class="icon-eur"></i> <span class="value"><?php echo $estadisticas['precio']?></span> <br>Precio total</div>
						<!-- .stat -->
					<?php } ?>

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de ventas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content prueba">
              <div class="shortcuts">
				<a href="creaVenta.php?codigo=<?php echo $codigo; ?>&amp;venta" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Nueva venta</span> </a>
				<?php if($_SESSION['tipoUsuario']=='ADMIN'){?>
					<a href="generaExcelVentas.php?codigoCliente=<?php echo $codigo; ?>" class="shortcut"><i class="shortcut-icon icon-download-alt"></i><span class="shortcut-label">Generar excel</span> </a>
				<?php } 
				compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>	



        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bookmark"></i>
              <h3>Datos de contacto para el cliente <a href='detallesCuenta.php?codigo=<?php echo $codigo;?>'><?php echo $datosCliente['empresa']; ?></a></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <table class="table table-striped table-bordered sinMargenAb">
                  <tr>
                    <th><i class="icon-user"></i> Contacto</th><td><?php echo $datosCliente['contacto']; ?></td>
                  </tr>
                  <tr>
                    <th><i class="icon-envelope"></i> eMail</th><td><?php echo $datosCliente['mail']; ?></td>
                  </tr>
                  <tr>
                    <th><i class="icon-phone"></i> Teléfono</th><td><?php echo formateaTelefono($datosCliente['telefono']); ?></td>
                  </tr>
                </table>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>  


      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Datos de la venta actualizados."); 
            }
            else{
              mensajeError("no se ha podido actualizar la venta. Compruebe los datos introducidos."); 
            }
          }
		  elseif(isset($_POST['completaAlumnos'])){
            if($res){
              mensajeOk("Venta registrada y alumnos completados."); 
            }
            else{
              mensajeError("no se ha podido completar los datos de los alumnos. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['codigoC'])){
            if($res){
              mensajeOk("Venta registrada correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar la venta. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar la venta. Contacte con el webmaster."); 
            }
          }
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Ventas realizadas al cliente <a href='detallesCuenta.php?codigo=<?php echo $codigo;?>'><?php echo $datosCliente['empresa']; ?></a></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Tipo de venta </th>
					<th> Comercial </th>
                    <th> Concepto </th>
					          <th> Fecha </th>
                    <th> Importe </th>
                    <th> Observaciones </th>
					<th> Cartera / Nueva </th>
                    <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeVentas($codigo);
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
$('#eliminar').click(function(){
  var valoresChecks=recorreChecks();
  if(valoresChecks['codigo0']==undefined){
    alert('Por favor, seleccione antes una venta.');
  }
  else{
    valoresChecks['elimina']='SI';
    creaFormulario('ventas.php',valoresChecks,'post');
  }

});
</script>
