<?php
  $seccionActiva=7;
  include_once('cabecera.php');
  $res=false;
  
  if(isset($_POST['codigo'])){
	$res=actualizaDatos('facturacion');  
  }

  if(isset($_POST['codigoColaborador']) && isset($_POST['periodo'])){
	$_SESSION['codigoColaborador']=$_POST['codigoColaborador'];
	$_SESSION['periodo']=$_POST['periodo'];
  $_SESSION['anio']=$_POST['anio'];
  }
  $datosColaborador=datosRegistro('colaboradores',$_SESSION['codigoColaborador']);
  $trimestre=$_SESSION['periodo'];
  $fechaUno='';
  $fechaDos='';
  $anio=$_SESSION['anio'];
  switch($trimestre){
	case '01':
		$fechaUno=$anio.'-01-01';
		$fechaDos=$anio.'-03-31';
	break;
	case '02':
		$fechaUno=$anio.'-04-01';
		$fechaDos=$anio.'-06-30';
	break;
	case '03':
		$fechaUno=$anio.'-07-01';
		$fechaDos=$anio.'-09-30';
	break;
	case '04':
		$fechaUno=$anio.'-10-01';
		$fechaDos=$anio.'-12-31';
	break;
	case '05':
		$fechaUno=$anio.'-01-01';
		$fechaDos=$anio.'-12-31';
	break;
  }
  $estadisticas=creaEstadisticasFacturasColaborador($_SESSION['codigoColaborador'], 'WHERE fechaVencimiento>="'.$fechaUno.'" AND fechaVencimiento<="'.$fechaDos.'" AND cobrada="SI"');
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-tasks"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Comisiones al colaborador <?php echo $datosColaborador['empresa']; ?> desde el: <?php echo formateaFechaWeb($fechaUno); ?> al: <?php echo formateaFechaWeb($fechaDos); ?></h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo $estadisticas['comision']?></span> <br>Total en comisiones</div>
                    <!-- .stat -->
                    <!-- .stat -->
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
		<div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de comisiones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="generaExcelComisionesFiltrado.php?colaborador=<?php echo $_POST['codigoColaborador']; ?>&fechaUno=<?php echo $fechaUno; ?>&fechaDos=<?php echo $fechaDos; ?>" class="shortcut"><i class="shortcut-icon icon-download-alt"></i><span class="shortcut-label">Descargar excel</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
		<?php 
          mensajeResultado('codigo',$res,'comisiones');
        ?>
        
        <div class="widget widget-table action-table cajaSelect">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Facturas emitidas durante el trimestre seleccionado</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
					<th> Referencia </th>
					<th> Cliente </th>
                    <th> Concepto </th>
                    <th> Fecha de emisión </th>
					<th> Fecha de vencimiento </th>
					<th> Importe </th>
					<th> Comisión </th>
					<th> Pagado </th>
          <th> Pagado </th>
					<th class="centro"> </th>
					<th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeFacturasComisionColaborador($_SESSION['codigoColaborador'], 'WHERE fechaVencimiento>="'.$fechaUno.'" AND fechaVencimiento<="'.$fechaDos.'" AND cobrada="SI"',true);

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">	
	var tabla=$('.datatable').DataTable({
		"aaSorting": [],
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave":false,
		"iDisplayLength":25,
		"oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
		  'sZeroRecords':'No se han encontrado coincidencias'
    }});

</script>