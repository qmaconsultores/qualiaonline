<?php
  $seccionActiva=11;
  include_once('cabecera.php');

  $datos=datosRegistro('productos',$_GET['codigo']);
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Datos del Servicio</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="productos.php" method="post">
                  <fieldset>

                    <?php
                      campoOculto($datos);
                      campoTexto('codigoProducto','Código del servicio',$datos,'input-small');
                      campoTexto('nombreProducto','Nombre',$datos);
                      areaTexto('descripcionProducto','Descripción',$datos);
                    ?>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar Servicio</button> 
                      <a href="productos.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
