<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común


  	//$_GET['codigo'] viene en la URL a través de un botón "Descargar Contrato"
	$datos=datosRegistro('facturacion',$_GET['codigo']);
	$datosCliente=datosRegistro('clientes',$datos['codigoCliente']);
	$datosFirma=datosRegistro('firmas',$datos['firma']);
	if($datos['concepto']=='14'){ 
		if($datos['firma']!='3'){
			$letra="F";
		}else{
			$letra="S";
		}
	}else{
		if($datos['firma']!='3'){
			$letra="C";
		}else{
			$letra="S";
		}
	}
	if($datos['firma']=='4'){
		$letra='E';
	}
	$referencia=str_pad($datos['referencia'], 5, "0", STR_PAD_LEFT);
	$referencia=$letra.'-'.$referencia.'/'.substr($datos['anio'], 2);
	
	//Carga de la librería PHPWord
	require_once 'phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$path='documentos2/facturas';
	
	if($datos['concepto']=='14' && $datos['firma']=='4'){
		$path='documentos2/facturas';
		$plantilla='plantillaFacturaEducativa.docx';//
	} 
	else if($datos['concepto']=='14'){
		$consulta=consultaBD("SELECT COUNT(codigoCurso) AS total FROM facturas_cursos WHERE codigoFactura='".$datos['codigo']."';",true);
		$total=mysql_fetch_assoc($consulta);
		if($total['total']>1){
			$plantilla='plantillaFacturaCursosMultiple.docx';//
		}else{
			$plantilla='plantillaFacturaCursos.docx';//
		}
	}else{
		if($datos['tipoFactura']=='mantenimiento'){
			$plantilla='plantillaFacturaLopd3.docx';//
		}elseif($datos['tipoFactura']=='auditoria'){
			$plantilla='plantillaFacturaLopd.docx';//
		}elseif($datos['tipoFactura']=='consultoria'){
			$plantilla='plantillaFacturaLopd2.docx';//
		}elseif($datos['tipoFactura']=='lssi'){
			$plantilla='plantillaFacturaBlanqueo.docx';//
		}elseif($datos['tipoFactura']=='prl'){
			$plantilla='plantillaFacturaPRL.docx';//
		}elseif($datos['tipoFactura']=='alergenos'){
			$plantilla='plantillaFacturaAlergenos.docx';
		}
		elseif($datos['tipoFactura']=='web'){
			$plantilla='plantillaFacturaWeb.docx';//
		}
		elseif($datos['tipoFactura']=='dominiocorreo'){
			$plantilla='plantillaFacturaCorreo.docx';//
		}
		elseif($datos['tipoFactura']=='dominio'){
			$plantilla='plantillaFacturaDominio.docx';//
		}
		elseif($datos['tipoFactura']=='commerce'){
			$plantilla='plantillaFacturaCommerce.docx';
		}
	}
	
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate($path.'/'.$plantilla);

	/*
	Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
	En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
	*/
	
	
	$datosObservaciones=consultaBD("SELECT observaciones FROM historico_facturas WHERE codigoFactura='".$datos['codigo']."' ORDER BY fecha DESC;",true,true);
	$document->setValue("observaciones",utf8_decode($datosObservaciones['observaciones']));
	
	if($datos['tipoFactura']=='lssi'){																//GENERACIÓN DEL LSSI//
		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$coste=$datos['coste'];
		if(isset($_GET['vencimiento'])){
			$vencimiento=datosRegistro('facturas_vto',$_GET['vencimiento']);
			$fecha=formateaFechaWeb($vencimiento['fecha']);
			$coste=$vencimiento['importe'];		
		}
		$datosCliente['empresa']=str_replace('&','&amp;',$datosCliente['empresa']);
		$formaPago=array('transferencia'=>'Transferencia','efectivo'=>'Efectivo','cheque'=>'Cheque','domiciliacion'=>'Domiciliación','tarjeta'=>'Tarjeta');
		$document->setValue("fecha",utf8_decode($fecha));
		$document->setValue("vencimiento",utf8_decode(formateaFechaWeb($datos['fechaVencimiento'])));
		$document->setValue("numFactura",utf8_decode($referencia));
		$document->setValue("anio",date('Y'));
		$document->setValue("nombreCliente",utf8_decode($datosCliente['empresa']));
		$document->setValue("cif",utf8_decode($datosCliente['cif']));
		$document->setValue("dir",utf8_decode($datosCliente['direccion']));
		$document->setValue("cp",utf8_decode($datosCliente['cp']));
		$document->setValue("localidad",utf8_decode($datosCliente['localidad']));
		$document->setValue("provincia",utf8_decode($datosCliente['provincia']));
		$document->setValue("formaPago",utf8_decode($formaPago[$datos['formaPago']]));
		$document->setValue("numCuenta",utf8_decode(substr($datosCliente['numCuenta'],0,4).'-'.substr($datosCliente['numCuenta'],4,4).'-'.substr($datosCliente['numCuenta'],8,2).'-'.substr($datosCliente['numCuenta'],10,6).'-'.substr($datosCliente['numCuenta'],-8,-4).'-****'));
		$document->setValue("firma",utf8_decode($datosFirma['firma']));
		$document->setValue("direccionFirma",utf8_decode($datosFirma['direccion']));
			$document->setValue("cpFirma",utf8_decode($datosFirma['cp']));
			$document->setValue("localidadFirma",utf8_decode($datosFirma['localidad']));
			$document->setValue("telefonoFirma",utf8_decode($datosFirma['telefono']));
			$document->setValue("cifFirma",utf8_decode($datosFirma['cif']));
		$document->setValue("fecha",utf8_decode($fecha));
		$document->setValue("cifFirma",utf8_decode($datosFirma['cif']));
		$document->setValue("subtotal",utf8_decode(number_format((float)$coste, 2, ',', '')));
		$iva=0.21*$coste;
		$total=$coste+$iva;
		$document->setValue("iva",utf8_decode(number_format((float)$iva, 2, ',', '')));
		$document->setValue("total",utf8_decode(number_format((float)$total, 2, ',', '')));
		
	}else{																							//GENERACIÓN DEL RESTO DE FACTURAS//
	
		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$coste=$datos['coste'];
		if(isset($_GET['vencimiento'])){
			$vencimiento=datosRegistro('facturas_vto',$_GET['vencimiento']);
			$fecha=formateaFechaWeb($vencimiento['fecha']);
			$coste=$vencimiento['importe'];		
		}
		$modalidades=array('PRESENCIAL'=>'Presencial','DISTANCIA'=>'A distancia','MIXTA'=>'Mixta','TELE'=>'Teleformación','WEBINAR'=>'Webinar');
		$datosCliente['empresa']=str_replace('&','&amp;',$datosCliente['empresa']);
		
		$banco=array('BAEMESM1XXX'=>'BANESTO','ABNAESMMXXX'=>'THE ROYAL BANK OF SCOTLAND PLC, SUCURSAL EN ESPAÑA','AHCFESMMXXX'=>'AHORRO CORPORACION FINANCIERA, S.A., SOCIEDAD DE VALORES','ALCLESMMXXX'=>'BANCO ALCALA, S.A.','AREBESMMXXX'=>'ARESBANK, S.A.',
		'BAPUES22XXX'=>'BANCA PUEYO, S.A.','BASKES2BXXX'=>'KUTXABANK, S.A.','BBPIESMMXXX'=>'BANCO BPI, S.A., SUCURSAL EN ESPAÑA','BBRUESMXXXX'=>'ING BELGIUM, S.A., SUCURSAL EN ESPAÑA','BBVAESMMXXX'=>'BANCO BILBAO VIZCAYA ARGENTARIA, S.A.',
		'BCCAESMMXXX'=>'BANCO DE CRÉDITO SOCIAL COOPERATIVO S.A.','BCOEESMM081'=>'CAJA RURAL DE CASTILLA-LA MANCHA, S.C.C.','BCOEESMMXXX'=>'BANCO COOPERATIVO ESPAÑOL, S.A.','BESMESMMXXX'=>'NOVO BANCO, S.A., SUCURSAL EN ESPAÑA',
		'BFIVESBBXXX'=>'BANCO MEDIOLANUM, S.A.','BKBKESMMXXX'=>'BANKINTER, S.A.','BKOAES22XXX'=>'BANKOA, S.A.','BMARES2MXXX'=>'BANCA MARCH, S.A.','BMCEESMMXXX'=>'BANQUE MAROCAINE COMMERCE EXTERIEUR INTERNATIONAL, S.A.',
		'BOTKESMXXXX'=>'THE BANK OF TOKYO-MITSUBISHI UFJ, LTD, SUCURSAL EN ESPAÑA','BRASESMMXXX'=>'BANCO DO BRASIL AG, SUCURSAL EN ESPAÑA','BSABESBBXXX'=>'BANCO DE SABADELL, S.A.',
		'BSCHESMMXXX'=>'BANCO SANTANDER, S.A.','BSUIESMMXXX'=>'CREDIT AGRICOLE CORPORATE AND INVESTMENT BANK, SUCURSAL EN ESPAÑA','BVALESMMXXX'=>'RBC INVESTOR SERVICES ESPAÑA, S.A.',
		'CAGLESMMVIG'=>'ABANCA CORPORACIÓN BANCARIA, S.A.','CAHMESMMXXX'=>'BANKIA, S.A.','CAIXESBBXXX'=>'CAIXABANK, S.A.','CAPIESMMXXX'=>'CM CAPITAL MARKETS BOLSA, SOCIEDAD DE VALORES, S.A.','CASDESBBXXX'=>'CAJA DE ARQUITECTOS, S.C.C.',
		'CAZRES2ZXXX'=>'IBERCAJA BANCO, S.A.','CCOCESMMXXX'=>'BANCO CAMINOS, S.A.','CCRIES2AXXX'=>'CAJAS RURALES UNIDAS, S.C.C.','CDENESBBXXX'=>'CAIXA DE CREDIT DELS ENGINYERS - CAJA DE CREDITO DE LOS INGENIEROS, S.C.C.',
		'CECAESMM045'=>'CAJA DE AHORROS Y M.P. DE ONTINYENT','CECAESMM048'=>'LIBERBANK, S.A.','CECAESMM056'=>'COLONYA - CAIXA DESTALVIS DE POLLENSA','CECAESMM105'=>'BANCO DE CASTILLA-LA MANCHA, S.A. /CAJA DE AHORROS DE CASTILLA-LA MANCHA',
		'CECAESMMXXX'=>'CECABANK, S.A.','CESCESBBXXX'=>'CATALUNYA BANC, S.A.','CGDIESMMXXX'=>'BANCO CAIXA GERAL, S.A.','CITIES2XXXX'=>'CITIBANK ESPAÑA, S.A.','CLPEES2MXXX'=>'CAJA LABORAL POPULAR, C.C.','CRESESMMXXX'=>'CREDIT SUISSE AG, SUCURSAL EN ESPAÑA',
		'CSPAES2L108'=>'BANCO DE CAJA ESPAÑA DE INVERSIONES, SALAMANCA Y SORIA, S.A.','CSURES2CXXX'=>'CAJASUR BANCO, S.A.','DSBLESMMXXX'=>'DEXIA SABADELL, S.A.','ESPBESMMXXX'=>'BANCO DE ESPAÑA','ESSIESMMXXX'=>'BANCO ESPIRITO SANTO DE INVESTIMENTO, S.A., SUCURSAL EN ESPAÑA',
		'EVOBESMMXXX'=>'EVO BANCO S.A.U.','FIOFESM1XXX'=>'BANCO FINANTIA SOFINLOC, S.A.','GBMNESMMXXX'=>'BANCO MARE NOSTRUM, S.A.','GVCBESBBETB'=>'GVC GAESCO VALORES, SOCIEDAD DE VALORES, S.A.',
		'IBRCESMMXXX'=>'SOCIEDAD DE GESTION DE LOS SISTEMAS DE REGISTRO, COMPENSACION Y LIQUIDACION DE VALORES, S.A.U.','ICROESMMXXX'=>'INSTITUTO DE CREDITO OFICIAL',
		'INGDESMMXXX'=>'ING BANK, N.V., SUCURSAL EN ESPAÑA','INSGESMMXXX'=>'INVERSEGUROS, SOCIEDAD DE VALORES, S.A.','INVLESMMXXX'=>'BANCO INVERSIS, S.A.','IPAYESMMXXX'=>'SOCIEDAD ESPAÑOLA DE SISTEMAS DE PAGO, S.A.',
		'IVALESMMXXX'=>'INTERMONEY VALORES, SOCIEDAD DE VALORES, S.A.','LISEESMMXXX'=>'LINK SECURITIES, SOCIEDAD DE VALORES, S.A.','MADRESMMXXX'=>'BANCO DE MADRID, S.A.','MEFFESBBXXX'=>'BME CLEARING, S.A.',
		'MISVESMMXXX'=>'MAPFRE INVERSION, SOCIEDAD DE VALORES, S.A.','MLCEESMMXXX'=>'MERRILL LYNCH CAPITAL MARKETS ESPAÑA, S.A., SOCIEDAD DE VALORES','NACNESMMXXX'=>'BANCO DE LA NACION ARGENTINA, SUCURSAL EN ESPAÑA',
		'NATXESMMXXX'=>'NATIXIS, S.A., SUCURSAL EN ESPAÑA','POHIESMMXXX'=>'TARGOBANK, S.A.','POPIESMMXXX'=>'POPULAR BANCA PRIVADA, S.A.','POPLESMMXXX'=>'BANCOPOPULAR-E, S.A.','POPUESMMXXX'=>'BANCO POPULAR ESPAÑOL, S.A.',
		'PRABESMMXXX'=>'COOPERATIEVE CENTRALE RAIFFEISEN- BOERENLEENBANK B.A. (RABOBANK NEDERLAND), SUCURSAL EN ESPAÑA','PROAESMMXXX'=>'EBN BANCO DE NEGOCIOS, S.A.',
		'PSTRESMMXXX'=>'BANCO PASTOR, S.A.','RENBESMMXXX'=>'RENTA 4 BANCO, S.A.','RENTESMMXXX'=>'RENTA 4 SOCIEDAD DE VALORES, S.A.','UBIBESMMXXX'=>'UBI BANCA INTERNATIONAL, S.A., SUCURSAL EN ESPAÑA','UCJAES2MXXX'=>'UNICAJA BANCO, S.A.',
		'XBCNESBBXXX'=>'SOCIEDAD RECTORA BOLSA VALORES DE BARCELONA, S.A., S.R.B.V.','XRBVES2BXXX'=>'SOCIEDAD RECTORA BOLSA DE VALORES DE BILBAO, S.A., S.R.B.V.','XRVVESVVXXX'=>'SOCIEDAD RECTORA BOLSA VALORES DE VALENCIA, S.A., S.R.B.V.',
		'BPLCESMMXXX'=>'BARCLAYS BANK PLC, SUCURSAL EN ESPAÑA','BARCESMMXXX'=>'BARCLAYS BANK, S.A.','BNPAESMZXXX'=>'BNP PARIBAS ESPAÑA, S.A.','GEBAESMMBIL'=>'BNP PARIBAS FORTIS, S.A., N.V., SUCURSAL EN ESPAÑA','PARBESMHXXX'=>'BNP PARIBAS SECURITIES SERVICES, SUCURSAL EN ESPAÑA',
		'BNPAESMHXXX'=>'BNP PARIBAS, SUCURSAL EN ESPAÑA','CITIESMXSEC'=>'CITIBANK INTERNATIONAL LTD, SUCURSAL EN ESPAÑA','COBAESMXTMA'=>'COMMERZBANK AKTIENGESELLSCHAFT, SUCURSAL EN ESPAÑA','DEUTESBBASS'=>'DEUTSCHE BANK, S.A.E.','CSSOES2SFIN'=>'FINANDUERO, SOCIEDAD DE VALORES, S.A.',
		'MIDLESMXXXX'=>'HSBC BANK PLC, SUCURSAL EN ESPAÑA','EHYPESMXXXX'=>'HYPOTHEKENBANK FRANKFURT AG., SUCURSAL EN ESPAÑA','WELAESMMFUN'=>'PORTIGON AG, SUCURSAL EN ESPAÑA','SOGEESMMAGM'=>'SOCIETE GENERALE, SUCURSAL EN ESPAÑA','UBSWESMMNPB'=>'UBS BANK, S.A.','XXXXESBBXXX'=>'');
		
		if($datos['formaPago']!='transferencia'){
			$document->setValue("banco",utf8_decode($banco[$datosCliente['bic']]));
			$document->setValue("cuenta",utf8_decode(substr($datosCliente['numCuenta'],0,4).'-****-**-******-'.substr($datosCliente['numCuenta'],-8,-4).'-'.substr($datosCliente['numCuenta'],-4)));
		}else{
			if($datosFirma['codigo']==1){
				$document->setValue("banco",'BANCO SABADELL');
				$document->setValue("cuenta",'ES64. 0081.0454.460001193421');
			} else if($datosFirma['codigo']==2){
				$document->setValue("banco",'BANCO SABADELL');
				$document->setValue("cuenta",'ES05. 0081.4199.210001986703');
			} else if($datosFirma['codigo']==3) {
				$document->setValue("banco",'BANCO SABADELL');
				$document->setValue("cuenta",'ES24. 0081.4199.200001986604');
			} else if($datosFirma['codigo']==4) {
				$document->setValue("banco",'BANCO BILBAO VIZCAYA ARGENTARIA, S.A.');
				$document->setValue("cuenta",'ES08. 0182.4775.550200313563');
			}
		}
		if($datos['concepto']=='14'){    //PLANTILLA DE CURSOS
			$consultaCursos=consultaBD("SELECT codigoCurso FROM facturas_cursos WHERE codigoFactura='".$datos['codigo']."';",true);
			if(mysql_num_rows($consultaCursos)>1){
				$cursosReemplazo='';
				while($datosCursos=mysql_fetch_assoc($consultaCursos)){
					$datosCurso=datosRegistro('cursos',$datosCursos['codigoCurso']);
					$datosAccion=datosRegistro('accionFormativa',$datosCurso['codigoaccionFormativa'],'codigoInterno');
					
					$consulta=consultaBD("SELECT COUNT(codigoAlumno) AS total FROM alumnos_registrados_cursos WHERE codigoCurso='".$datosCursos['codigoCurso']."';",true);
					$total=mysql_fetch_assoc($consulta);
					
					if(strlen($datosAccion['codigoInterno'])<3){
						while(strlen($datosAccion['codigoInterno'])<3){
							$datosAccion['codigoInterno']='0'.$datosAccion['codigoInterno'];
						}
					}
					if(strlen($datosCurso['codigoInterno'])<2){
						while(strlen($datosCurso['codigoInterno'])<2){
							$datosCurso['codigoInterno']='0'.$datosCurso['codigoInterno'];
						}
					}
					$cursosReemplazo.='GRUPO: '.$datosAccion['codigoInterno'].'-'.$datosCurso['codigoInterno'].'<w:br/>';
					$cursosReemplazo.='ACCIÓN: '.$datosAccion['codigoInterno'].'<w:br/>';
					$cursosReemplazo.='CURSO: '.$datosAccion['denominacion'].'<w:br/>';
					$cursosReemplazo.='MODALIDAD: '.$modalidades[$datosAccion['modalidad']].'<w:br/>';
					$cursosReemplazo.='EMPRESA: '.$datosCliente['empresa'].'<w:br/>';
					$cursosReemplazo.='HORAS DE CONTROL Y TUTORIZACIÓN: '.$datosAccion['horas'].'<w:br/>';
					$cursosReemplazo.='ALUMNOS: '.$total['total'].'<w:br/>';
					$cursosReemplazo.='FECHA DE INICIO: '.formateaFechaWeb($datosCurso['fechaInicio']).'<w:br/>';
					$cursosReemplazo.='FECHA DE FIN: '.formateaFechaWeb($datosCurso['fechaFin']).'<w:br/><w:br/>';
				}
				
				$document->setValue("cursos",utf8_decode($cursosReemplazo));
			}else{
				$datosCursos=mysql_fetch_assoc($consultaCursos);
				$datosCurso=datosRegistro('cursos',$datosCursos['codigoCurso']);
				$datosAccion=datosRegistro('accionFormativa',$datosCurso['codigoaccionFormativa'],'codigoInterno');
				
				$consulta=consultaBD("SELECT COUNT(codigoAlumno) AS total FROM alumnos_registrados_cursos WHERE codigoCurso='".$datosCursos['codigoCurso']."';",true);
				$total=mysql_fetch_assoc($consulta);
				
				if(strlen($datosAccion['codigoInterno'])<3){
					while(strlen($datosAccion['codigoInterno'])<3){
						$datosAccion['codigoInterno']='0'.$datosAccion['codigoInterno'];
					}
				}
				if(strlen($datosCurso['codigoInterno'])<2){
					while(strlen($datosCurso['codigoInterno'])<2){
						$datosCurso['codigoInterno']='0'.$datosCurso['codigoInterno'];
					}
				}
				$document->setValue("grupo",utf8_decode($datosCurso['codigoInterno']));
				
				$document->setValue("grupo",utf8_decode($datosAccion['codigoInterno'].'-'.$datosCurso['codigoInterno']));
				$document->setValue("accion",utf8_decode($datosAccion['codigoInterno']));
				$document->setValue("curso",utf8_decode($datosAccion['denominacion']));
				$document->setValue("modalidad",utf8_decode($modalidades[$datosAccion['modalidad']]));
				$document->setValue("empresa",utf8_decode($datosCliente['empresa']));
				$document->setValue("horas",utf8_decode($datosAccion['horas']));
				$document->setValue("numAlumnos",utf8_decode($total['total']));
				$document->setValue("fechaInicio",utf8_decode(formateaFechaWeb($datosCurso['fechaInicio'])));
				$document->setValue("fechaFin",utf8_decode(formateaFechaWeb($datosCurso['fechaFin'])));
		
			}

			if($datos['firma']=='4'){

				$datosAccion=datosRegistro('accionFormativa',$datosCurso['codigoaccionFormativa'],'codigoInterno');
				
				$consulta=consultaBD("SELECT COUNT(codigoAlumno) AS total FROM alumnos_registrados_cursos WHERE codigoCurso='".$datosCursos['codigoCurso']."';",true);
				$total=mysql_fetch_assoc($consulta);
				
				if(strlen($datosAccion['codigoInterno'])<3){
					while(strlen($datosAccion['codigoInterno'])<3){
						$datosAccion['codigoInterno']='0'.$datosAccion['codigoInterno'];
					}
				}
				if(strlen($datosCurso['codigoInterno'])<2){
					while(strlen($datosCurso['codigoInterno'])<2){
						$datosCurso['codigoInterno']='0'.$datosCurso['codigoInterno'];
					}
				}
				$document->setValue("grupo",utf8_decode($datosCurso['codigoInterno']));
				
				$document->setValue("grupo",utf8_decode($datosAccion['codigoInterno'].'-'.$datosCurso['codigoInterno']));
				$document->setValue("accion",utf8_decode($datosAccion['codigoInterno']));
				$document->setValue("curso",utf8_decode($datosAccion['denominacion']));
				$document->setValue("modalidad",utf8_decode($modalidades[$datosAccion['modalidad']]));
				$document->setValue("empresa",utf8_decode($datosCliente['empresa']));
				$document->setValue("horas",utf8_decode($datosAccion['horas']));
				$document->setValue("numAlumnos",utf8_decode($total['total']));

				$document->setValue("fechaInicio",utf8_decode(formateaFechaWeb($datosCurso['fechaInicio'])));
				$document->setValue("fechaFin",utf8_decode(formateaFechaWeb($datosCurso['fechaFin'])));
			}
			
		}


		
			$document->setValue("firma",utf8_decode($datosFirma['firma']));
			$document->setValue("direccionFirma",utf8_decode($datosFirma['direccion']));
			$document->setValue("cpFirma",utf8_decode($datosFirma['cp']));
			$document->setValue("localidadFirma",utf8_decode($datosFirma['localidad']));
			$document->setValue("telefonoFirma",utf8_decode($datosFirma['telefono']));
			$document->setValue("cifFirma",utf8_decode($datosFirma['cif']));
			$document->setValue("fecha",utf8_decode($fecha));
			if($datos['concepto']=='14' && $datosFirma['codigo']=='3'){
				$letra='S/';
			}elseif($datos['concepto']=='14' && $datosFirma['codigo']!='3'){
				$letra='F/';
			}else{
				$letra='';
			}

			if($datos['firma']=='4'){
				$letra='E/';
			}
			$document->setValue("referencia",utf8_decode($referencia));
			$document->setValue("fechaVencimiento",utf8_decode(formateaFechaWeb($datos['fechaVencimiento'])));
			$document->setValue("cliente",utf8_decode($datosCliente['empresa']));
			$document->setValue("direccion",utf8_decode($datosCliente['direccion']));
			$document->setValue("cp",utf8_decode($datosCliente['cp']));
			$document->setValue("localidad",utf8_decode($datosCliente['localidad']));
			$document->setValue("provincia",utf8_decode($datosCliente['provincia']));
			$document->setValue("telefono",utf8_decode($datosCliente['telefono']));
			$document->setValue("cif",utf8_decode($datosCliente['cif']));
			$document->setValue("idCliente",utf8_decode($datosCliente['referencia']));
			$document->setValue("coste",utf8_decode(number_format((float)$coste, 2, ',', '')));
			$document->setValue("subtotal",utf8_decode(number_format((float)$coste, 2, ',', '').' &#8364;'));
			
			$iva=0.21*$coste;
			$total=$coste+$iva;
			
			
			if($datos['concepto']!='14'){
				$document->setValue("total",utf8_decode(number_format((float)$total, 2, ',', '')).' &#8364;');
				$document->setValue("totalIva",utf8_decode(number_format((float)$iva, 2, ',', '')).' &#8364;');
			}else{
				$document->setValue("gastosOrga",utf8_decode(number_format((float)$datos['gastosOrganizacion'], 2, ',', '')).' &#8364;');
				$document->setValue("gastosImpar",utf8_decode(number_format((float)$datos['gastosImparticion'], 2, ',', '')).' &#8364;');
				if($datosFirma['codigo']!=3){
					$document->setValue("total",utf8_decode(number_format((float)$total, 2, ',', '')).' &#8364;');
					$document->setValue("totalIva",utf8_decode(number_format((float)$iva, 2, ',', '')).' &#8364;');
					$document->setValue("iva",utf8_decode('21%'));
				}else{
					$document->setValue("total",utf8_decode(number_format((float)$coste, 2, ',', '').' &#8364;'));
					$document->setValue("totalIva",'');
					$document->setValue("iva",'');
				}
			}
	}

	$vtos=consultaBD("SELECT * FROM facturas_vto WHERE codigoFactura=".$datos['codigo'],true);
	$i=1;
	$vencimientos='';
	$costes='';
	if(!isset($_GET['vencimiento'])){
		while ($vto=mysql_fetch_assoc($vtos)) {
			if($vencimientos != ''){
				$vencimientos.='<w:br />';
				$costes.='<w:br />';
			}

			$totalVencimiento=$vto['importe']*1.21;

			$vencimientos.=$i.'&#176; VENCIMIENTO: '.formateaFechaWeb($vto['fecha']).'  '.number_format((float)$totalVencimiento, 2, ',', '');
			$i++;
		}
	}
	if($i==2){
		$vencimientos=str_replace('1&#176; ', '', $vencimientos);
	}
	$document->setValue("vencimientos",utf8_decode($vencimientos));


	/*
	Cuando ya has sustituido todas las etiquetas por su valores, el método save
	guarda el fichero resultane en la ruta que le indiques con el nombre que le
	indiques.
	*/
	$tiempo=time();
	$document->save('documentos2/Factura.docx');

	/*
	Definir headers.
	Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
	que lo que va a recibir es un fichero que debe descargar.
	*/
	header("Content-Type: application/vnd.ms-docx");
	header("Content-Disposition: attachment; filename=Factura.docx");
	header("Content-Transfer-Encoding: binary");

	/* 
	Descargar archivo.
	Por último, le decirmos a PHP que lea y transfiera el documento que
	antes con save.
	*/
	readfile('documentos2/Factura.docx');


?>