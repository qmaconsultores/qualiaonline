<?php
  $seccionActiva=19;
  include_once('cabecera.php');
  $res=false;

  $estadisticas=creaEstadisticasFacturasSupervisor($_SESSION['usuario']);
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-tasks"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-ticket"></i> <span class="value"><?php echo $estadisticas['facturacion']?></span> <br>Nº ventas</div>
                    <!-- .stat -->
                    <div class="stat"> <i class="icon-credit-card"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Cantidad</div>
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
      <?php 
      //echo "<a class='noAjax' href='ventasSupervisorExcel.php?codigo=".$_SESSION['usuario']."'>Descargar</a>";
      ?>
      <div class="span12">
        
        <div class="widget widget-table action-table cajaSelect">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Ventas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable" id="tablaFacturacion">
                <thead>
                  <tr>
          					<th> Referencia </th>
          					<th> Cliente </th>
          					<th> Comercial </th>
                    <th> Concepto </th>
                    <th> Cartera/Nueva </th>
                    <th> Fecha de emisión </th>
          					<th> Fecha de vencimiento </th>
          					<th> Importe </th>
          					<th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimefacturasSupervisor($_SESSION['usuario']);

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<script type="text/javascript">
	
	var tabla=$('.datatable').DataTable({
		"aaSorting": [],
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave":false,
		"iDisplayLength":25,
		"oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
		  'sZeroRecords':'No se han encontrado coincidencias'
    }});
</script>