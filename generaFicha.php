<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común

	//Carga de la librería PHPWord
	require_once 'phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('documentos/plantillados.docx');
	
	conexionBD();
	$consulta=consultaBD("SELECT * FROM alumnos INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno 
	WHERE alumnos_registrados_cursos.codigoCurso=".$_GET['codigo'].";");
	
	$datos=mysql_fetch_assoc($consulta);
	$i=1;
	
	while($datos!=0){

		/*
		Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
		En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
		*/
		$document->setValue("nombre".$i,utf8_decode($datos['nombre']));
		$document->setValue("apellidos".$i,utf8_decode($datos['apellidos']));
		$document->setValue("dni".$i,utf8_decode($datos['dni']));
		$document->setValue("nss".$i,utf8_decode($datos['numSS1'].$datos['numSS2']));
		$document->setValue("fechaNac".$i,utf8_decode($datos['fechaNac']));
		$document->setValue("estudios".$i,utf8_decode($datos['estudios']));
		$document->setValue("cotizacion".$i,utf8_decode($datos['cotizacion']));
		$document->setValue("puesto".$i,utf8_decode($datos['categoria']));
		$document->setValue("area".$i,utf8_decode($datos['categoria']));
		$document->setValue("discapacidad".$i,utf8_decode($datos['discapacidad']));
		
		$datos=mysql_fetch_assoc($consulta);
		$i++;
	}
	
	if($i<25){
		while($i<25){
		$document->setValue("nombre".$i,"");
		$document->setValue("apellidos".$i,"");
		$document->setValue("dni".$i,"");
		$document->setValue("nss".$i,"");
		$document->setValue("fechaNac".$i,"");
		$document->setValue("estudios".$i,"");
		$document->setValue("cotizacion".$i,"");
		$document->setValue("puesto".$i,"");
		$document->setValue("area".$i,"");
		$document->setValue("discapacidad".$i,"");
		$i++;
		}
	}
	
	/*
		Cuando ya has sustituido todas las etiquetas por su valores, el método save
		guarda el fichero resultane en la ruta que le indiques con el nombre que le
		indiques.
		*/
		$document->save('documentos/listadoAlumnosCurso.docx');


		/*
		Definir headers.
		Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
		que lo que va a recibir es un fichero que debe descargar.
		*/
		header("Content-Type: application/vnd.ms-docx");
		header("Content-Disposition: attachment; filename=listadoAlumnosCurso.docx");
		header("Content-Transfer-Encoding: binary");

		/* 
		Descargar archivo.
		Por último, le decirmos a PHP que lea y transfiera el documento que
		antes con save.
		*/
		readfile('documentos/listadoAlumnosCurso.docx');
	
	cierraBD();

?>