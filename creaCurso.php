<?php
  $seccionActiva=5;
  include_once('cabecera.php');
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nuevo curso</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="formacion.php" method="post">
                  <fieldset>


                    
					
					<div class="control-group">                     
                      <label class="control-label" for="accionFormativa">Acción formativa:</label>
                      <div class="controls">
					  
						 <?php 
							echo "<span id='resultado'></span>";
						?>
					  
					  </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('plataforma','Plataforma');
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="responsable">Responsable:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="responsable" name="responsable">
						<input type="hidden" name="listadoAlumnos" id="listadoAlumnos">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					
					<div class="control-group">                     
                      <label class="control-label" for="tlfResponsable">Tlf. Responsable:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="tlfResponsable" name="tlfResponsable">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					
				     <div class="control-group">                     
                      <label class="control-label" for="codigoInterno">Código curso:</label>
                      <div class="controls numSS">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini" id="codigoAccionFormativa" name="codigoAccionFormativa" disabled="disabled">
                          <span class="add-on">/</span>
                          <input type="text" class="input-mini" id="codigoInterno" name="codigoInterno">
						  <input type="hidden" id="tipoFormacion" name="tipoFormacion">
                        </div>
                      </div> <!-- /controls -->         
                    </div> <!-- /control-group -->

                    
                    <div class="control-group">                     
                      <label class="control-label" for="tutor">Tutor:</label>
                      <div class="controls">
                        <?php selectTutor(); ?>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fechaInicio">Fecha de inicio:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaInicio" name="fechaInicio" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="fechaFin">Fecha de finalización:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaFin" name="fechaFin" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <?php
                      $opciones=array('NO', 'SI');
                      campoSelect('llamadaBienvenida', 'LLamada de Bienvenida', $opciones, $opciones, '', 'selectpicker show-tick anchoAuto');
                      campoSelect('llamadaSeguimiento', 'LLamada de Seguimiento', $opciones, $opciones, '', 'selectpicker show-tick anchoAuto');
                      campoSelect('llamadaFinalizacion', 'LLamada de Finalización', $opciones, $opciones, '', 'selectpicker show-tick anchoAuto');
                      campoSelect('bonificado', '¿Bonificado?', $opciones, $opciones, '', 'selectpicker show-tick anchoAuto');
					  campoSelect('finalizado', '¿Finalizado?', $opciones, $opciones, '', 'selectpicker show-tick anchoAuto');
                      $consultaUser="SELECT codigo, CONCAT(apellidos, ', ', nombre) AS texto FROM usuarios WHERE activoUsuario='SI';";
                      campoSelectConsulta('comercial', 'Comercial', $consultaUser);
                    ?>

					<div class="control-group">                     
					  <label class="control-label" for="formacionCurso">Formación:</label>
					  <div class="controls">
						
						<select name="formacionCurso" id="formacionCurso" class='selectpicker show-tick'>
						  <option value="NO" data-content="<span class='label'>No tiene</span>"></option>
						  <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>"></option>
						  <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>"></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>"></option>
						</select>

					  </div> <!-- /controls -->       
					</div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="email">Alumnos:</label>
                      <div class="controls conBorde">
                       
                        <table class="table table-striped table-bordered datatable">
                          <thead>
                            <tr>
                              <th> Nombre </th>
                              <th> DNI </th>
                              <th> eMail </th>
                              <th> Teléfono </th>
							                <th> Empresa </th>
                              <th><input type='checkbox' id="todo"></th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              imprimeAlumnosCreaCurso();
                            ?>
                          
                          </tbody>
                        </table>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				          	<div class="control-group">                     
                      <label class="control-label" for="mediosPropios">Medios para la formación:</label>
                      <div class="controls">
                        <label class="checkbox inline">
                          <input type="checkbox" name="mediosPropios" id="mediosPropios" value="SI"> Propios de la empresa bonificada
                        </label>
                        <br>
                        <label class="checkbox inline">
                          <input type="checkbox" name="mediosEntidad" id="mediosEntidad" value="SI"> De la entidad organizadora
                        </label>
                        <br>
                        <label class="checkbox inline">
                          <input type="checkbox" name="mediosCentro" id="mediosCentro" value="SI"> Del centro de formación o entidad formadora externa
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoSelect('medios','Medios',array('Entidad inscrita','Entidad externa'),array('EntidadInscrita','EntidadOrganizadora'));
					?>

				<h3 class="apartadoFormulario">Descripción</h3>
          					
          		<h4 class="apartadoFormulario" id="textoCambiarUno">Centro de la tutoría y horarios</h4>
				
					<div class="control-group">                     
                      <label class="control-label" for="cifTutoria">CIF:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cifTutoria" name="cifTutoria" value="B65775850">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="centroTutoria">Centro:</label>
                      <div class="controls">
                        <input type="text" class="span4" id="centroTutoria" name="centroTutoria">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="tlfTutoria">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="tlfTutoria" name="tlfTutoria" value="931 79 07 03">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="domicilioTutoria">Domicilio:</label>
                      <div class="controls">
                        <input type="text" class="span4" id="domicilioTutoria" name="domicilioTutoria" value="C/ Pablo Iglesias, 58">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="cpTutoria">CP:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cpTutoria" name="cpTutoria" value="08302">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="poblacionTutoria">Población:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="poblacionTutoria" name="poblacionTutoria" value="Mataró">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						echo "<span id='resultadoDos'></span>";
					?>
					
				<div id="centroPresencial">
					
					<h4 class="apartadoFormulario" id="textoCambiarDos">Centro gestor de la formación y horarios</h4>
					
					<div class="control-group">                     
                      <label class="control-label" for="cif">CIF:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cif" name="cif">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="centro">Centro:</label>
                      <div class="controls">
                        <input type="text" class="span4" id="centro" name="centro">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="tlf">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="tlf" name="tlf">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="domicilio">Domicilio:</label>
                      <div class="controls">
                        <input type="text" class="span4" id="domicilio" name="domicilio">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="cp">CP:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cp" name="cp">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="poblacion">Población:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="poblacion" name="poblacion">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="titularidad">Titularidad:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="titularidad" value="publico" checked="checked"> Público
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="titularidad" value="privado"> Privado
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="horario">Horario mañana:</label>
                      <div class="controls">
                        <input type="text" class="input-small timepicker" id="horaInicioFormacion" name="horaInicioFormacion" value="<?php echo hora(); ?>">
            						 a 
            						<input type="text" class="input-small timepicker" id="horaFinFormacion" name="horaFinFormacion" value="<?php echo hora(); ?>">
            						 (hh:mm) Comprendido entre 00:01h. y 15:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
				  	
				    <div class="control-group">                     
                      <label class="control-label" for="horario">Horario tarde:</label>
                      <div class="controls">
                        <input type="text" class="input-small timepicker" id="horaInicioFormacionTarde" name="horaInicioFormacionTarde" value="<?php echo hora(); ?>">
            						 a 
            						<input type="text" class="input-small timepicker" id="horaFinFormacionTarde" name="horaFinFormacionTarde" value="<?php echo hora(); ?>">
            						 (hh:mm) Comprendido entre 15:01h. y 00:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="horasTutoria">Horas formación:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horasFormacion" name="horasFormacion">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				            <div class="control-group">                     
                      <label class="control-label" for="horas">Días impartición:</label>
                      <div class="controls">
                        <table class="mitadAncho">
                          <tr>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                          </tr>
                          <tr>
                            <th><input type="checkbox" id="lunesFormacion" name="lunesFormacion"></th>
                            <th><input type="checkbox" id="martesFormacion" name="martesFormacion"></th>
                            <th><input type="checkbox" id="miercolesFormacion" name="miercolesFormacion"></th>
                            <th><input type="checkbox" id="juevesFormacion" name="juevesFormacion"></th>
                            <th><input type="checkbox" id="viernesFormacion" name="viernesFormacion"></th>
                            <th><input type="checkbox" id="sabadoFormacion" name="sabadoFormacion"></th>
                            <th><input type="checkbox" id="domingoFormacion" name="domingoFormacion"></th>
                          </tr>
                        </table>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				</div>
				
					<div id="centroDistancia">
						<h4 class="apartadoFormulario" id="textoCambiarDos">Centro gestor de la formación a distancia y horarios</h4>
						
						<div class="control-group">                     
						  <label class="control-label" for="tutor">Tutor:</label>
						  <div class="controls">
							<?php selectTutor(false, "tutorDistancia"); ?>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="cifDistancia">CIF:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="cifDistancia" name="cifDistancia">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="centroGestorDistancia">Centro:</label>
						  <div class="controls">
							<input type="text" class="span4" id="centroGestorDistancia" name="centroGestorDistancia">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="tlfDistancia">Teléfono:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="tlfDistancia" name="tlfDistancia">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="domicilioDistancia">Domicilio:</label>
						  <div class="controls">
							<input type="text" class="span4" id="domicilioDistancia" name="domicilioDistancia">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="cpDistancia">CP:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="cpDistancia" name="cpDistancia">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="poblacionDistancia">Población:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="poblacionDistancia" name="poblacionDistancia">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="titularidadDistancia">Titularidad:</label>
						  <div class="controls">
							<label class="radio inline">
							  <input type="radio" name="titularidadDistancia" value="publico" checked="checked"> Público
							</label>
							
							<label class="radio inline">
							  <input type="radio" name="titularidadDistancia" value="privado"> Privado
							</label>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						
					
					<?php
						echo "<span id='resultadoTres'></span>";
					?>
				            
						
					</div>
					
					<h4 class="apartadoFormulario" id="textoCambiarTres">Representación Legal del Trabajador</h4>
					
					<div class="control-group">                     
                      <label class="control-label" for="informarlt">¿Se ha informado a la RLT?:</label>
                      <div class="controls">
                        <select id="informarlt" name="informarlt" class='selectpicker show-tick anchoAuto'>
							<option value="SI" selected="selected">SI
							<option value="NO">No
						</select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="informerlt">Informe RLT:</label>
                      <div class="controls">
                        <select id="informerlt" name="informerlt" class='selectpicker show-tick anchoAuto'>
							<option value="NOINF" selected="selected">No informa
							<option value="FAV">Favorable
							<option value="DIS">Discrepancias
						</select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group" id="divFecha">                     
                      <label class="control-label" for="fechaDiscrepancia">Fecha discrepancia:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaDiscrepancia" name="fechaDiscrepancia" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="resuelto">Resuelto 15 días:</label>
                      <div class="controls">
                        <select id="resuelto" name="resuelto" class='selectpicker show-tick anchoAuto'>
							<option value="SI" selected="selected">SI
							<option value="NO">No
						</select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Registrar curso</button> 
                      <a href="formacion.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
	  
	  <!--Popup -->
		  <div class="span6 hide ventanasFlotantes" id="ventanaFlotanteDos">
			<div class="widget cajaSelect">
				<div class="widget-header"> <i class="icon-share-alt icon-book"></i><i class="icon-chevron-right"></i><i class="icon-plus-sign"></i>
				  <h3>Nueva acción formativa</h3>
				</div>
				<!-- /widget-header -->
				<div class="widget-content">
				  
				  <div class="tab-pane" id="formcontrols">
					<form id="edit-profile" class="form-horizontal" method="post">
					  <fieldset>
						<div class="control-group">                     
						  <label class="control-label" for="codigoInternoAccion">Acción:</label>
						  <div class="controls">
							<input type="text" class="input-mini" id="codigoInternoAccion" name="codigoInternoAccion">
							<?php 
								campoOculto($_SESSION['codigoS'], 'codigoUsuario');
								$consulta=consultaBD("SELECT codigoInterno FROM accionFormativa;",true);
								$datosCodigos=mysql_fetch_assoc($consulta);
								$i=0;
								echo "<div id='div-padre'>";
								while(isset($datosCodigos['codigoInterno'])){
									divOculto($datosCodigos['codigoInterno'],'codigoInternoAccion'.$i);
									$i++;
									$datosCodigos=mysql_fetch_assoc($consulta);
								}
								echo "</div>";
							?>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->

						<div class="control-group">                     
						  <label class="control-label" for="denominacion">Denominación:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="denominacion" name="denominacion">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->


						<?php campoTexto('grupoAcciones','Grupo','','input-mini'); ?>



						<div class="control-group">                     
						  <label class="control-label" for="tipoAccion">Tipo de acción:</label>
						  <div class="controls">
							<select name="tipoAccion" id='tipoAccion' class='selectpicker input-large'>
							  <option value="PROPIA">Propia</option>
							  <option value="VINCULADA">Vinculada a la obtención de un Cert. de Profesionalidad</option>
							</select>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="nivelAccion">Nivel de acción:</label>
						  <div class="controls">
							<select name="nivelAccion" id='nivelAccion' class='selectpicker input-large'>
							  <option value="BASICO">Básico</option>
							  <option value="SUPERIOR">Superior</option>
							</select>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->


						<div class="control-group">                     
						  <label class="control-label" for="contenidos">Contenidos:</label>
						  <div class="controls">
										<textarea name="contenidos" id='contenidos' class="areaTextoAmplia"></textarea>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->


						<div class="control-group">                     
						  <label class="control-label" for="objetivos">Objetivos:</label>
						  <div class="controls">
							<textarea name="objetivos" id='objetivos' class="areaTextoAmplia"></textarea>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="horas">Número de Horas:</label>
						  <div class="controls">
							<input type="text" class="input-mini pagination-right" id="horas" name="horas">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="modalidad">Modalidad:</label>
						  <div class="controls">
							
							<select name="modalidad" id='modalidad' class='selectpicker show-tick'>
							  <option value="PRESENCIAL">Presencial</option>
							  <option value="DISTANCIA">A distancia</option>
							  <option value="MIXTA">Mixta</option>
							  <option value="TELE">Teleformación</option>
							  <option value="WEBINAR">Webinar</option>
							</select>

						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->




						<div class="form-actions">
						  <button id='registrarAccion' type="button" class="btn btn-primary"><i class="icon-ok"></i> Registrar acción formativa</button> 
						  <button type="button" class="btn" id="cancelar"><i class="icon-remove"></i> Cancelar</button> 
						</div> <!-- /form-actions -->
					  </fieldset>
					</form>
					</div>


				</div>
				<!-- /widget-content --> 
			  </div>
		  </div>
		  <!--Fin Popup -->
	  
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript" src="js/full-calendar/jquery-ui.custom.min.js"></script><!-- Habilita el drag y el resize -->
<script type="text/javascript">
  $(document).ready(function(){
	$('.timepicker').timepicker({showMeridian: false,
                defaultTime: false});
	$('#ventanaFlotanteDos').draggable();
	listadoAcciones();
	
	var codigoTutor=$("select[name=tutor]").val();
	diasTutorias(codigoTutor);
	
	var codigoTutor=$("select[name=tutorDistancia]").val();
	diasTutoriasDistancia(codigoTutor);
	
	/*REGISTRO POR AJAX*/
	$('#registrarAccion').click(function(){
		var codigoInterno=$('#codigoInternoAccion').val();
		var denominacion=$('#denominacion').val();
		var grupoAcciones=$('#grupoAcciones').val();
		var tipoAccion=$('#tipoAccion').val();
		var contenidos=$('#contenidos').val();
		var objetivos=$('#objetivos').val();
		var horas=$('#horas').val();
		var modalidad=$('#modalidad').val();
		var nivelAccion=$('#nivelAccion').val();
		$.ajax({
         type: "POST",
         url: "listadosajax/insercionAccion.php",
         data: "codigoInterno=" + codigoInterno + "&denominacion=" + denominacion + "&grupoAcciones=" + grupoAcciones + "&tipoAccion=" + tipoAccion + "&contenidos=" + contenidos + 
		 "&objetivos=" + objetivos + "&horas=" + horas + "&modalidad=" + modalidad + "&nivelAccion=" + nivelAccion ,
		 beforeSend:function () {
			   $("#resultado").html('<div class="control-group"><div class="controls"><i class="icon-refresh icon-spin"></i></div></div>');
         },
         success: function(response){
				 listadoAcciones();
         }
		});
		cierraVentana('ventanaFlotanteDos');
	});
	
	$('#cancelar').click(function(){
		cierraVentana('ventanaFlotanteDos');
	});
	
	$('#divFecha').css('display','none');
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$('#informerlt').change(function(){
       if($(this).val()=='DIS'){
		$('#divFecha').css('display','block');
	  }else{
		$('#divFecha').css('display','none');
	  }

    });
	
	$("select[name=tutor]").change(function(){
		var codigoTutor=$("select[name=tutor]").val();
		diasTutorias(codigoTutor);
	});
	
	$("select[name=tutorDistancia]").change(function(){
		var codigoTutor=$("select[name=tutorDistancia]").val();
		diasTutoriasDistancia(codigoTutor);
	});
	
	//PARTE NUEVA SOLUCIÓN DE CHECKS
	
	var array=new Array();   //Creo un array que me servirá para transmitir los alumnos que vaya seleccionando
	
	cargaOyente(array);
	
	$('.dataTables_paginate').click(function(){
		$("td input:checkbox").unbind("change");
		cargaOyente(array);
	});
	
	$('.dataTables_filter').keypress(function(){
		$("td input:checkbox").unbind("change");
		cargaOyente(array);
	});
	
	//FIN PARTE NUEVA SOLUCIÓN DE CHECKS
	
  });
  
  function abreVentana(id){
    $('#'+id).removeClass('hide');
  }

  function cierraVentana(id){
    $('#'+id).addClass('hide');
  }
  
  function listadoAcciones(){
	 $.ajax({
		 type: "POST",
         url: "listadosajax/listadoAcciones.php",
		 beforeSend:function () {
			   $("#resultado").html('<div class="control-group"><div class="controls"><i class="icon-refresh icon-spin"></i></div></div>');
         },
         success: function(response){
               $("#resultado").html(response);
         }
    });
  }
  
  function diasTutorias(codigoTutor){
	 $.ajax({
		 type: "POST",
         url: "listadosajax/diasTutorias.php",
		 data: "codigoTutor=" + codigoTutor,
		 beforeSend:function () {
			   $("#resultadoDos").html('<div class="control-group"><div class="controls"><i class="icon-refresh icon-spin"></i></div></div>');
         },
         success: function(response){
               $("#resultadoDos").html(response);
         }
    });
  }
  
  function diasTutoriasDistancia(codigoTutor){
	 $.ajax({
		 type: "POST",
         url: "listadosajax/diasTutoriasDistancia.php",
		 data: "codigoTutor=" + codigoTutor,
		 beforeSend:function () {
			   $("#resultadoTres").html('<div class="control-group"><div class="controls"><i class="icon-refresh icon-spin"></i></div></div>');
         },
         success: function(response){
               $("#resultadoTres").html(response);
         }
    });
  }
  
  //PARTE NUEVA SOLUCIÓN DE CHECKS
  
  function cargaOyente(array){		
	$('td input:checkbox').change(function(){ 		//Cada vez que de a un check se ejecuta
		var codigo=$(this).val();					//Cojo el valor del check
		var salir=false;
		jQuery.each( array, function( i, val ) {
			if(val==codigo){						//Compruebo que no esté ya insertado en el array
				array.splice( i, 1 );
				salir=true;
			}
		});
		if(salir!=true){
			array.push(codigo);						//Si no está lo añado
		}
		$('#listadoAlumnos').val(array);			//Asigno al input creado auxiliar, el valor del array que recorreré en funciones.php
	});
  }
  
  //FIN PARTE NUEVA SOLUCIÓN DE CHECKS
</script>