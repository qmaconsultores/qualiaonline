<?php
  $seccionActiva=17;
  include_once('cabecera.php');
  
  $res=false;
  if(isset($_POST['codigo'])){
	if(isset($_POST['iban'])){
		$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
	}
    $res=actualizaDatos('colaboradores');
	$res=$res && insertaHistoricoColaborador($_POST['codigo']);
	insertaServiciosColaborador($_POST['codigo']);
	if(isset($_FILES['ficheroDocumento']) && $_FILES['ficheroDocumento']['name']!=''){	
		$_POST['codigoCliente']=$_POST['codigo'];
		$_FILES['ficheroFactura']=$_FILES['ficheroDocumento'];
    	$res=$res && insertaDatos('documentos_colaboradores',time(),'documentos');
    }
  }
  
  if(isset($_GET['codigo'])){
	  $codigo=$_GET['codigo'];
  }else{
	  $codigo=$_POST['codigo'];
  }
  
  $datos=datosRegistro('colaboradores', $codigo);
  
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Detalles Colaborador</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="?" method="post" enctype="multipart/form-data">
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#pagina1" data-toggle="tab">Datos generales</a></li>
				  <li><a href="#pagina2" data-toggle="tab">Documentos</a></li>
				</ul>
			  
				<br>

				<div class="tab-content" id="pestanas">

				<div class="tab-pane active" id="pagina1">
                  <fieldset class="span5">
                    
                     <input type="hidden" name="codigo" value="<?php echo $datos['codigo']; ?>" >
					 
					<?php 
						campoTexto('referencia','ID',$datos, 'input-mini pagination-right'); 
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="empresa">Empresa:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="empresa" name="empresa" value="<?php echo $datos['empresa']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="cif">CIF:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cif" name="cif" value="<?php echo $datos['cif']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					          <div class="control-group">                     
                      <label class="control-label" for="ccc">NºSS (C.C.C):</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="ccc" name="ccc" value="<?php echo $datos['ccc']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTextoSimbolo('comision','Comisión','%',$datos);
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="numSS1">Nº Cuenta Bancaria:</label>
                      <div class="controls numSS">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini" id="iban" name="iban" maxlength="4" size="4" value="<?php echo substr($datos['numCuenta'], 0, 4);?>" >
                          <span class="add-on">/</span>
                          <input type="text" class="input-medium" id="numCuenta" name="numCuenta" maxlength="20" size="20" value="<?php echo substr($datos['numCuenta'], 4, 24);?>" >
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="nuevacreacion">Empresa de Nueva Creación:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="nuevacreacion" value="SI" <?php if($datos['nuevacreacion']=='SI'){ echo "checked='checked'";} ?> > Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="nuevacreacion" value="NO" <?php if($datos['nuevacreacion']=='NO'){ echo "checked='checked'";}?> > No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php 
						campoFecha('fechaCreacion','Fecha de creación',$datos,false);
					?>


                    <div class="control-group">                     
                      <label class="control-label" for="direccion">Dirección:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="direccion" name="direccion" value="<?php echo $datos['direccion']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="cp">CP:</label>
                      <div class="controls">
                        <input type="text" class="input-mini" id="cp" name="cp" value="<?php echo $datos['cp']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="localidad">Localidad:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="localidad" name="localidad" value="<?php echo $datos['localidad']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="provincia">Provincia:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="provincia" name="provincia" value="<?php echo $datos['provincia']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono" value="<?php echo $datos['telefono']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="movil">Móvil:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="movil" name="movil" value="<?php echo $datos['movil']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('telefonoDos','Teléfono 2',$datos,'input-small');
						campoTexto('movilDos','Móvil 2',$datos,'input-small');
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="mail">Mail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="mail" name="mail" value="<?php echo $datos['mail']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="fax">Fax:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="fax" name="fax" value="<?php echo $datos['fax']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="pyme">PYME:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="pyme" value="SI"  <?php if($datos['pyme']=='SI'){ echo "checked='checked'";} ?> > Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="pyme" value="NO"  <?php if($datos['pyme']=='NO'){ echo "checked='checked'";} ?> > No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                  </fieldset>
                  <fieldset class="span3">
					
					<?php
						campoSelectConsulta('codigoUsuario','Comercial',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios ORDER BY nombre, apellidos;",$datos);
						selectConsultaMultipleColaboradores($datos['codigo']);
					?>
					
                    <div class="control-group">                     
                      <label class="control-label" for="sector">Sector:</label>
                      <div class="controls">
                        
                        <select name="sector" class='selectpicker show-tick' data-live-search="true" >
                          <option value="HOSTELERIA" <?php if($datos['sector']=='HOSTELERIA'){ echo 'selected="selected"';} ?> >Hostelería</option>
                          <option value="COMERCIO" <?php if($datos['sector']=='COMERCIO'){ echo 'selected="selected"';} ?> >Comercio</option>
                          <option value="TRANSPORTE" <?php if($datos['sector']=='TRANSPORTE'){ echo 'selected="selected"';} ?> >Transporte</option>
                          <option value="ESTETICA" <?php if($datos['sector']=='ESTETICA'){ echo 'selected="selected"';} ?> >Estética</option>
                          <option value="SANIDAD" <?php if($datos['sector']=='SANIDAD'){ echo 'selected="selected"';} ?> >Sanidad</option>
                          <option value="VETERINARIA" <?php if($datos['sector']=='VETERINARIA'){ echo 'selected="selected"';} ?> >Veterinaria</option>
                          <option value="DEPORTES" <?php if($datos['sector']=='DEPORTES'){ echo 'selected="selected"';} ?> >Deportes</option>
                          <option value="ASOCIACIONES" <?php if($datos['sector']=='ASOCIACIONES'){ echo 'selected="selected"';} ?> >Asociaciones</option>
                          <option value="FORMACION" <?php if($datos['sector']=='FORMACION'){ echo 'selected="selected"';} ?> >Formación</option>
                          <option value="INDUSTRIA" <?php if($datos['sector']=='INDUSTRIA'){ echo 'selected="selected"';} ?> >Industria</option>
                          <option value="CONSTRUCCION" <?php if($datos['sector']=='CONSTRUCCION'){ echo 'selected="selected"';} ?> >Construcción</option>
                          <option value="INFORMATICA" <?php if($datos['sector']=='INFORMATICA'){ echo 'selected="selected"';} ?> >Informática</option>
                          <option value="SERVICIOS" <?php if($datos['sector']=='SERVICIOS'){ echo 'selected="selected"';} ?> >Servicios</option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fechaRegistro">Fecha de registro:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaRegistro" name="fechaRegistro"  value="<?php echo formateaFechaWeb($datos['fechaRegistro']); ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('repreLegal','Representante legal',$datos,'input-large');
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="dniRepresentante">DNI Representante:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="dniRepresentante" name="dniRepresentante" value="<?php echo $datos['dniRepresentante']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    
					           <div class="control-group">                     
                      <label class="control-label" for="contacto">Persona de contacto:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="contacto" name="contacto" value="<?php echo $datos['contacto']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					          


                    <div class="control-group">                     
                      <label class="control-label" for="cargo">Cargo:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="cargo" name="cargo" value="<?php echo $datos['cargo']; ?>" >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					
					<div class="control-group">                     
                      <label class="control-label" for="representacion">R. Legal de los Trabajadores:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="representacion" value="SI" <?php if($datos['representacion']=='SI'){ echo "checked='checked'";} ?> > Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="representacion" value="NO" <?php if($datos['representacion']=='NO'){ echo "checked='checked'";} ?> > No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						areaTexto('observaciones','Observaciones');
						$consulta=consultaBD("SELECT * FROM historico_colaboradores WHERE codigoColaborador='$codigo' ORDER BY fecha DESC;",true);
						$datosHistorico=mysql_fetch_assoc($consulta);
						$historico='';
						if(isset($datosHistorico['codigo'])){
							while(isset($datosHistorico['codigo'])){
								$historico=$historico.formateaFechaWeb($datosHistorico['fecha'])." ".$datosHistorico['hora']." ".$datosHistorico['observaciones']." \n\n";
								$datosHistorico=mysql_fetch_assoc($consulta);
							}
						}
						areaTexto('historicoObservaciones','Histórico Observaciones',$historico,'areaTexto',true);
					?>


                  </fieldset>
				  
				  <fieldset class="span10">
				  <h3 class="apartadoFormulario sinFlotar">Convenio colectivo</h3>
				  
				  <?php
          campoFecha('fechaFirma','Fecha firma',$datos);
					campoTexto('convenio','Convenio colectivo',$datos,'input-large');
					areaTexto('desConvenio','Descripción Convenio colectivo',$datos,'areaTexto');
				  ?>
				  
				  </fieldset>

				  <fieldset class="span10">
				  <h3 class="apartadoFormulario sinFlotar">C.N.A.E.</h3>
				  
				  <?php
					campoTexto('cnae','C.N.A.E.',$datos,'input-large');
					areaTexto('desCnae','Descripción C.N.A.E.',$datos,'areaTexto');
				  ?>
				  
				  </fieldset>
				  
				  </div>
				  
				  <div class="tab-pane" id="pagina2">	
					<center>
                        <table class="table table-striped table-bordered datatable mitadAncho" id="tablaDocumentos">
                          <thead>
                            <tr>
                              <th> Documento </th>
                              <th> Fecha de documento </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosColaborador($codigo);

                            ?>
                          
                          </tbody>
                        </table>
                      
                        <a href="javascript:void" id="botonDocumento" class="btn btn-success"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 5 MB como máximo.
                        <input type="file" class="hide" name="ficheroDocumento" id="ficheroDocumento" onchange="nuevaFila();"/>
                      </center>
				  </div>
				  </div>
				  
				  <fieldset class="sinFlotar">

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar Colaborador</button> 
                      <a href="colaboradores.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/iban.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
	$('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$("#numCuenta").focusout(function() {
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
	  }).blur(function(){
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
	});
	$('#botonDocumento').click(function(){
      if($('#ficheroDocumento').val()==''){
        $('#ficheroDocumento').click();
      }
    });
  });
  
  function nuevaFila(){
    $('#botonDocumento').attr('disabled',true);
    $('#tablaDocumentos').find("tr:last").after("<tr><td><input type='text' name='nombreFactura' class='input-large' /><input type='hidden' name='tipo' value='MAIL' /></td><td><input type='text' name='fechaFactura' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('form').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
</script>
