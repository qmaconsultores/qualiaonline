<?php
  $seccionActiva=1;
  include_once('cabecera.php');

  /******************SE ACTUALIZA EN LA MISMA PANTALLA POR PETICIÓN EXPRESA DE ELLOS***************************/
  if(isset($_POST['codigo'])){
	if(isset($_POST['iban'])){
		$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
	}
	if($_POST['tieneColaborador']=='NO'){
		$_POST['comercial']='NULL';
	}
    actualizaCliente();
  }
  /************************************************************************************************************/

  if(isset($_GET['codigo'])){
	  $codigo=$_GET['codigo'];
  }else{
	  $codigo=$_POST['codigo'];
  }

  $datos=datosCliente($codigo);
  
  $consulta=consultaBD("SELECT COUNT(codigo) AS total FROM incidencias WHERE codigoCliente='$codigo';",true);
  $numIncidencias=mysql_fetch_assoc($consulta);
  
  $disabled='';
  $disabledNucleo=false;
  if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){
  	$consulta=consultaBD("SELECT habilitado FROM usuarios WHERE codigo='".$_SESSION['codigoS']."';",true);
  	$datosHabilitados=mysql_fetch_assoc($consulta);
  	if($datosHabilitados['habilitado']=='NO'){
  		$disabled="disabled='disabled'";
  		$disabledNucleo=true;
  	}
  }
  
  $datosUsuario=datosRegistro('usuarios',$_SESSION['codigoS']);

  if($datos['tipoServicio']==NULL){
    $datos['tipoServicio']='NULL';
  }
  
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Datos del posible cliente <?php echo $datos['empresa']; ?></h3> 
			  <?php 
				  echo "
				  <a href='trabajos.php?codigoCliente=$codigo'><span class='label label-warning'>Trabajos</span></a>
				  <a href='formacion.php?codigoCliente=$codigo'><span class='label label-inverse'>Formación</span></a>
				  <a href='creaTareaUnico.php?codigo=".$datos['codigo']."'><span class='label label-success'>Tareas</span></a>
				  <a href='incidencias.php?codigoCliente=$codigo'><span class='label label-danger'>Incidencias</span></a>
				  <a href='facturacion.php?codigoCliente=$codigo'><span class='label label-primary'>Facturación</span></a>";
			  ?>
			  <h3>Tareas pendientes:</h3> 
			  <?php 
				$numTareas=consultaBD("SELECT COUNT(codigo) AS total FROM tareas WHERE codigoCliente='$codigo' AND estado='pendiente' AND codigoUsuario IS NOT NULL;",true,true);
				echo "<span class='label label-danger'>".$numTareas['total']."</span>";
			  ?>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="?" method="post" enctype="multipart/form-data">
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#pagina1" data-toggle="tab">Datos generales</a></li>
				  <li><a href="#pagina2" data-toggle="tab">Contratos</a></li>
				  <li><a href="#pagina3" data-toggle="tab">Facturación</a></li>
				  <li><a href="#pagina4" data-toggle="tab">Consultoría</a></li>
				  <li><a href="#pagina5" data-toggle="tab">Formación</a></li>
				  <li><a href="#pagina6" data-toggle="tab">Mail</a></li>
          <li><a href="#pagina7" data-toggle="tab">Empresas vinculadas</a></li>
				</ul>
			  
				<br>

				<div class="tab-content" id="pestanas">

				<div class="tab-pane active" id="pagina1">
                  <fieldset class="span5">
                    
                     <input type="hidden" name="codigo" id="codigo" value="<?php echo $datos['codigo']; ?>" >
					 
					 <?php 
						 campoOculto($codigo, 'codigoCliente');
						 campoTexto('referencia','ID',$datos, 'input-mini pagination-right'); 
					 ?>

                    <div class="control-group">                     
                      <label class="control-label" for="empresa">Empresa:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="empresa" name="empresa" value="<?php echo $datos['empresa']; ?>" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="cif">CIF:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cif" name="cif" value="<?php echo $datos['cif']; ?>" maxlength="9" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					          <div class="control-group">                     
                      <label class="control-label" for="ccc">NºSS (C.C.C):</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="ccc" name="ccc" value="<?php echo $datos['ccc']; ?>" maxlength="12" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					
					<div class="control-group">                     
                      <label class="control-label" for="numSS1">Nº Cuenta Bancaria:</label>
                      <div class="controls numSS">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini" id="iban" name="iban" maxlength="4" size="4" value="<?php echo substr($datos['numCuenta'], 0, 4);?>" <?php echo $disabled; ?>>
                          <span class="add-on">/</span>
                          <input type="text" class="input-medium" id="numCuenta" name="numCuenta" maxlength="20" size="20" value="<?php echo substr($datos['numCuenta'], 4, 24);?>" <?php echo $disabled; ?>>
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php
						campoSelect('bic','Entidad bancaria',array('BANESTO','THE ROYAL BANK OF SCOTLAND PLC, SUCURSAL EN ESPAÑA','AHORRO CORPORACION FINANCIERA, S.A., SOCIEDAD DE VALORES','BANCO ALCALA, S.A.','ARESBANK, S.A.','BANCA PUEYO, S.A.','KUTXABANK, S.A.','BANCO BPI, S.A., SUCURSAL EN ESPAÑA','ING BELGIUM, S.A., SUCURSAL EN ESPAÑA','BANCO BILBAO VIZCAYA ARGENTARIA, S.A.','BANCO DE CRÉDITO SOCIAL COOPERATIVO S.A.','CAJA RURAL DE CASTILLA-LA MANCHA, S.C.C.','BANCO COOPERATIVO ESPAÑOL, S.A.','NOVO BANCO, S.A., SUCURSAL EN ESPAÑA','BANCO MEDIOLANUM, S.A.','BANKINTER, S.A.','BANKOA, S.A.','BANCA MARCH, S.A.','BANQUE MAROCAINE COMMERCE EXTERIEUR INTERNATIONAL, S.A.','THE BANK OF TOKYO-MITSUBISHI UFJ, LTD, SUCURSAL EN ESPAÑA','BANCO DO BRASIL AG, SUCURSAL EN ESPAÑA','BANCO DE SABADELL, S.A.','BANCO SANTANDER, S.A.','CREDIT AGRICOLE CORPORATE AND INVESTMENT BANK, SUCURSAL EN ESPAÑA','RBC INVESTOR SERVICES ESPAÑA, S.A.','ABANCA CORPORACIÓN BANCARIA, S.A.','BANKIA, S.A.','CAIXABANK, S.A.','CM CAPITAL MARKETS BOLSA, SOCIEDAD DE VALORES, S.A.','CAJA DE ARQUITECTOS, S.C.C.','IBERCAJA BANCO, S.A.','BANCO CAMINOS, S.A.','CAJAS RURALES UNIDAS, S.C.C.','CAIXA DE CREDIT DELS ENGINYERS - CAJA DE CREDITO DE LOS INGENIEROS, S.C.C.','CAJA DE AHORROS Y M.P. DE ONTINYENT','LIBERBANK, S.A.','COLONYA - CAIXA DESTALVIS DE POLLENSA','BANCO DE CASTILLA-LA MANCHA, S.A. /CAJA DE AHORROS DE CASTILLA-LA MANCHA','CECABANK, S.A.','CATALUNYA BANC, S.A.','BANCO CAIXA GERAL, S.A.','CITIBANK ESPAÑA, S.A.','CAJA LABORAL POPULAR, C.C.','CREDIT SUISSE AG, SUCURSAL EN ESPAÑA','BANCO DE CAJA ESPAÑA DE INVERSIONES, SALAMANCA Y SORIA, S.A.','CAJASUR BANCO, S.A.','DEXIA SABADELL, S.A.','BANCO DE ESPAÑA','BANCO ESPIRITO SANTO DE INVESTIMENTO, S.A., SUCURSAL EN ESPAÑA','EVO BANCO S.A.U.','BANCO FINANTIA SOFINLOC, S.A.','BANCO MARE NOSTRUM, S.A.','GVC GAESCO VALORES, SOCIEDAD DE VALORES, S.A.','SOCIEDAD DE GESTION DE LOS SISTEMAS DE REGISTRO, COMPENSACION Y LIQUIDACION DE VALORES, S.A.U.','INSTITUTO DE CREDITO OFICIAL','ING BANK, N.V., SUCURSAL EN ESPAÑA','INVERSEGUROS, SOCIEDAD DE VALORES, S.A.','BANCO INVERSIS, S.A.','SOCIEDAD ESPAÑOLA DE SISTEMAS DE PAGO, S.A.','INTERMONEY VALORES, SOCIEDAD DE VALORES, S.A.','LINK SECURITIES, SOCIEDAD DE VALORES, S.A.','BANCO DE MADRID, S.A.','BME CLEARING, S.A.','MAPFRE INVERSION, SOCIEDAD DE VALORES, S.A.','MERRILL LYNCH CAPITAL MARKETS ESPAÑA, S.A., SOCIEDAD DE VALORES','BANCO DE LA NACION ARGENTINA, SUCURSAL EN ESPAÑA','NATIXIS, S.A., SUCURSAL EN ESPAÑA','TARGOBANK, S.A.','POPULAR BANCA PRIVADA, S.A.','BANCOPOPULAR-E, S.A.','BANCO POPULAR ESPAÑOL, S.A.','COOPERATIEVE CENTRALE RAIFFEISEN- BOERENLEENBANK B.A. (RABOBANK NEDERLAND), SUCURSAL EN ESPAÑA','EBN BANCO DE NEGOCIOS, S.A.','BANCO PASTOR, S.A.','RENTA 4 BANCO, S.A.','RENTA 4 SOCIEDAD DE VALORES, S.A.','UBI BANCA INTERNATIONAL, S.A., SUCURSAL EN ESPAÑA','UNICAJA BANCO, S.A.','SOCIEDAD RECTORA BOLSA VALORES DE BARCELONA, S.A., S.R.B.V.','SOCIEDAD RECTORA BOLSA DE VALORES DE BILBAO, S.A., S.R.B.V.','SOCIEDAD RECTORA BOLSA VALORES DE VALENCIA, S.A., S.R.B.V.','BARCLAYS BANK PLC, SUCURSAL EN ESPAÑA','BARCLAYS BANK, S.A.','BNP PARIBAS ESPAÑA, S.A.','BNP PARIBAS FORTIS, S.A., N.V., SUCURSAL EN ESPAÑA','BNP PARIBAS SECURITIES SERVICES, SUCURSAL EN ESPAÑA','BNP PARIBAS, SUCURSAL EN ESPAÑA','CITIBANK INTERNATIONAL LTD, SUCURSAL EN ESPAÑA','COMMERZBANK AKTIENGESELLSCHAFT, SUCURSAL EN ESPAÑA','DEUTSCHE BANK, S.A.E.','FINANDUERO, SOCIEDAD DE VALORES, S.A.','HSBC BANK PLC, SUCURSAL EN ESPAÑA','HYPOTHEKENBANK FRANKFURT AG., SUCURSAL EN ESPAÑA','PORTIGON AG, SUCURSAL EN ESPAÑA','SOCIETE GENERALE, SUCURSAL EN ESPAÑA','UBS BANK, S.A.'),
						array('BAEMESM1XXX','ABNAESMMXXX','AHCFESMMXXX','ALCLESMMXXX','AREBESMMXXX','BAPUES22XXX','BASKES2BXXX','BBPIESMMXXX','BBRUESMXXXX','BBVAESMMXXX','BCCAESMMXXX','BCOEESMM081','BCOEESMMXXX','BESMESMMXXX','BFIVESBBXXX','BKBKESMMXXX','BKOAES22XXX','BMARES2MXXX','BMCEESMMXXX','BOTKESMXXXX','BRASESMMXXX','BSABESBBXXX','BSCHESMMXXX','BSUIESMMXXX','BVALESMMXXX','CAGLESMMVIG','CAHMESMMXXX','CAIXESBBXXX','CAPIESMMXXX','CASDESBBXXX','CAZRES2ZXXX','CCOCESMMXXX','CCRIES2AXXX','CDENESBBXXX','CECAESMM045','CECAESMM048','CECAESMM056','CECAESMM105','CECAESMMXXX','CESCESBBXXX','CGDIESMMXXX','CITIES2XXXX','CLPEES2MXXX','CRESESMMXXX','CSPAES2L108','CSURES2CXXX','DSBLESMMXXX','ESPBESMMXXX','ESSIESMMXXX','EVOBESMMXXX','FIOFESM1XXX','GBMNESMMXXX','GVCBESBBETB','IBRCESMMXXX','ICROESMMXXX','INGDESMMXXX','INSGESMMXXX','INVLESMMXXX','IPAYESMMXXX','IVALESMMXXX','LISEESMMXXX','MADRESMMXXX','MEFFESBBXXX','MISVESMMXXX','MLCEESMMXXX','NACNESMMXXX','NATXESMMXXX','POHIESMMXXX','POPIESMMXXX','POPLESMMXXX','POPUESMMXXX','PRABESMMXXX','PROAESMMXXX','PSTRESMMXXX','RENBESMMXXX','RENTESMMXXX','UBIBESMMXXX','UCJAES2MXXX','XBCNESBBXXX','XRBVES2BXXX','XRVVESVVXXX','BPLCESMMXXX','BARCESMMXXX','BNPAESMZXXX','GEBAESMMBIL','PARBESMHXXX','BNPAESMHXXX','CITIESMXSEC','COBAESMXTMA','DEUTESBBASS','CSSOES2SFIN','MIDLESMXXXX','EHYPESMXXXX','WELAESMMFUN','SOGEESMMAGM','UBSWESMMNPB'),$datos);
						campoOculto(formateaFechaWeb($datos['fechaUltimaVenta']),'fechaUltimaVenta');
						campoTextoSimbolo('creditoFormativo','Crédito formativo','€',$datos);
						campoSelect('formaPago','Forma de pago',array('Transferencia','Efectivo','Cheque','Domiciliación bancaria','Tarjeta'),array('transferencia','efectivo','cheque','domiciliacion','tarjeta'),$datos);
						
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="nuevacreacion">Empresa de Nueva Creación:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="nuevacreacion" value="SI" <?php if($datos['nuevacreacion']=='SI'){ echo "checked='checked'";} ?> <?php echo $disabled; ?>> Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="nuevacreacion" value="NO" <?php if($datos['nuevacreacion']=='NO'){ echo "checked='checked'";}?> <?php echo $disabled; ?>> No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php 
						campoFecha('fechaCreacion','Fecha de creación',$datos,false,$disabledNucleo);
					?>


                    <div class="control-group">                     
                      <label class="control-label" for="direccion">Dirección:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="direccion" name="direccion" value="<?php echo $datos['direccion']; ?>" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('direccionVisita','Dirección de visita',$datos);
					?>


                    <div class="control-group">                     
                      <label class="control-label" for="cp">CP:</label>
                      <div class="controls">
                        <input type="text" class="input-mini" id="cp" name="cp" value="<?php echo $datos['cp']; ?>" maxlength="5" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="localidad">Localidad:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="localidad" name="localidad" value="<?php echo $datos['localidad']; ?>" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="provincia">Provincia:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="provincia" name="provincia" value="<?php echo $datos['provincia']; ?>" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono" value="<?php echo $datos['telefono']; ?>" maxlength="9" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="movil">Móvil:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="movil" name="movil" value="<?php echo $datos['movil']; ?>" maxlength="9" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="telefonoDos">Teléfono 2:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefonoDos" name="telefonoDos" value="<?php echo $datos['telefonoDos']; ?>" maxlength="9" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="movilDos">Móvil 2:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="movilDos" name="movilDos" value="<?php echo $datos['movilDos']; ?>" maxlength="9" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="mail">Mail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="mail" name="mail" value="<?php echo $datos['mail']; ?>" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('mail2','Mail 2',$datos);
						campoTexto('web','Web',$datos);
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="fax">Fax:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="fax" name="fax" value="<?php echo $datos['fax']; ?>" maxlength="9" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="pyme">PYME:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="pyme" value="SI"  <?php if($datos['pyme']=='SI'){ echo "checked='checked'";} ?> <?php echo $disabled; ?>> Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="pyme" value="NO"  <?php if($datos['pyme']=='NO'){ echo "checked='checked'";} ?> <?php echo $disabled; ?>> No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                  </fieldset>
                  <fieldset class="span3">

                    <?php
						campoTexto('sector','Sector',$datos);
            campoActividades('actividad','Actividad',$datos);
						campoSelect('estado','Estado',array('Sin contactar','En proceso','Reconcertar','Vendido','Pendiente','Baja'),array('Sin contactar','En proceso','Reconcertar','Cerrada','Pendiente','Baja'),$datos);
						echo "<div id='pendienteOculto'>";
							campoRadio('firmado','Firmado',$datos);
							campoRadio('pendiente','Pendiente de',$datos,array('Datos','Formación'),array('Datos','Formacion'));
						echo "</div>";
					?>


                      
                    <div class="control-group">                     
                      <label class="control-label" for="tipo">Tipo de empresa:</label>
                      <div class="controls">
                        
                        <select name="tipo" class='selectpicker show-tick' data-live-search="true" <?php echo $disabled; ?>>
                          <option value="EMPRESA" <?php if($datos['tipoEmpresa']=='EMPRESA'){ echo 'selected="selected"';} ?> >Empresa</option>
                          <option value="PARTICULAR" <?php if($datos['tipoEmpresa']=='PARTICULAR'){ echo 'selected="selected"';} ?> >Particular</option>
                          <option value="AUTONOMO" <?php if($datos['tipoEmpresa']=='AUTONOMO'){ echo 'selected="selected"';} ?> >Autónomo</option>
                          <option value="COLABORADOR" <?php if($datos['tipoEmpresa']=='COLABORADOR'){ echo 'selected="selected"';} ?> >Colaborador</option>
                          <option value="ASOCIACION" <?php if($datos['tipoEmpresa']=='ASOCIACION'){ echo 'selected="selected"';} ?> >Asociación</option>

                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <?php selectTrabajador($datos['codigoUsuario'],$disabledNucleo); ?>


                    <div class="control-group">                     
                      <label class="control-label" for="fechaRegistro">Fecha de registro:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaRegistro" name="fechaRegistro"  value="<?php echo formateaFechaWeb($datos['fechaRegistro']); ?>" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('repreLegal','Representante legal',$datos,'input-large',$disabledNucleo);
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="dniRepresentante">DNI Representante:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="dniRepresentante" name="dniRepresentante" value="<?php echo $datos['dniRepresentante']; ?>" maxlength="9" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    
					           <div class="control-group">                     
                      <label class="control-label" for="contacto">Persona de contacto:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="contacto" name="contacto" value="<?php echo $datos['contacto']; ?>" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					          


                    <div class="control-group">                     
                      <label class="control-label" for="cargo">Cargo:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="cargo" name="cargo" value="<?php echo $datos['cargo']; ?>" <?php echo $disabled; ?>>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <?php
						campoRadio('tieneColaborador','Colaborador',$datos);
					?>

					<div id="colaboradorOculto">
						<?php
							$consulta="SELECT codigo, empresa AS texto FROM colaboradores ORDER BY empresa;";
							campoSelectConsulta('comercial','Colaborador',$consulta,$datos,'selectpicker span3 show-tick',"data-live-search='true'",$disabledNucleo);
						?>
					</div>
					
					
					<?php
					  //$consulta="SELECT codigo, nombreProducto AS texto FROM productos ORDER BY codigoProducto;";
                      //campoSelectConsulta('tipoServicio','Tipo servicio a ofrecer',$consulta,$datos,'selectpicker span3 show-tick',"data-live-search='true'",$disabledNucleo);
					  selectConsultaMultiple($datos['codigo']);
					  campoOculto($datos['tipoServicio'],'tipoServicio');
                    ?>
					
					          <div class="control-group">                     
                      <label class="control-label" for="representacion">R. Legal de los Trabajadores:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="representacion" value="SI" <?php if($datos['representacion']=='SI'){ echo "checked='checked'";} ?> <?php echo $disabled; ?>> Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="representacion" value="NO" <?php if($datos['representacion']=='NO'){ echo "checked='checked'";} ?> <?php echo $disabled; ?>> No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('numTrabajadores','Nº Trabajadores',$datos,'input-mini');
						campoSelect('tipoRemesa','Tipo de remesa',array('Core','B2B'),array('core','b2b'),$datos);
						campoTexto('responsableSeguridad','Responsable de seguridad',$datos);
						campoTexto('nifResponsable','NIF responsable',$datos,'input-small');
						campoTexto('cnae','CNAE',$datos);
						campoRadio('trabajadores','Trabajadores',$datos);
						campoRadio('baja','Dar de baja',$datos);
						echo "<div id='divFechaBaja'>";
							campoFecha('fechaBaja','Fecha de baja',$datos);
							campoSelect('motivoBaja','Motivo de baja',array('Cierre empresa','Descontento','Alta otra empresa','Fin de contrato'),array('Cierre empresa','Descontento','Alta otra empresa','Fin de contrato'),$datos);
						echo "</div>";
						
						$consulta=consultaBD("SELECT DISTINCT(historico_tareas.observaciones) AS observaciones, historico_tareas.fecha, historico_tareas.hora, CONCAT(usuarios.nombre, ' ',usuarios.apellidos) AS anotador  
						FROM historico_tareas INNER JOIN tareas ON historico_tareas.codigoTarea=tareas.codigo 
						INNER JOIN clientes ON clientes.codigo=tareas.codigoCliente LEFT JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo WHERE tareas.codigoCliente='$codigo' AND fecha LIKE'".date('Y')."-%' ORDER BY fecha DESC;",true);
						$datosHistorico=mysql_fetch_assoc($consulta);
						$historicoTareas='';
						if(isset($datosHistorico['fecha'])){
							while(isset($datosHistorico['fecha'])){
								$historicoTareas=$historicoTareas.formateaFechaWeb($datosHistorico['fecha'])." ".$datosHistorico['hora']." ".$datosHistorico['observaciones']." \n\n";
								$datosHistorico=mysql_fetch_assoc($consulta);
							}
						}
						
					?>


                  </fieldset>
				  
				  <fieldset class='span10'>
					<?php
						areaTexto('observaciones','Observaciones','','areaInformeDos');
						$consulta=consultaBD("SELECT historico_clientes.*, CONCAT(usuarios.nombre, ' ',usuarios.apellidos) AS anotador 
						FROM historico_clientes
						LEFT JOIN usuarios ON historico_clientes.codigoUsuario=usuarios.codigo WHERE historico_clientes.codigoCliente='$codigo' AND fecha LIKE'".date('Y')."-%' ORDER BY historico_clientes.fecha DESC;",true);
						$datosHistorico=mysql_fetch_assoc($consulta);
						$historico='';
						if(isset($datosHistorico['codigo'])){
							while(isset($datosHistorico['codigo'])){
								$historico=$historico.formateaFechaWeb($datosHistorico['fecha'])." ".$datosHistorico['hora']." ".$datosHistorico['observaciones'].". ".$datosHistorico['anotador']." \n\n";
								$datosHistorico=mysql_fetch_assoc($consulta);
							}
						}
						areaTextoFiltrar('historicoObservaciones','Histórico Observaciones',$historico,'areaInformeDos',true);
						areaTextoFiltrar('historicoTareas','Histórico Tareas',$historicoTareas,'areaInformeDos',true,'anioFiltrado2','filtrar2');
					?>
				  </fieldset>
				  
				  <h3 class='apartadoFormulario sinFlotar'>Datos Gestoría</h3>
				  
				  <fieldset class="span3">
					<?php
						campoTexto('nombreGestoria','Nombre',$datos);	
						campoTexto('mailGestoria','Correo electrónico',$datos);				
						campoTexto('telefonoGestoria','Teléfono',$datos);	
					?>
				  </fieldset>
				  
				  <h3 class='apartadoFormulario sinFlotar'>Tareas pendientes</h3>
				  
					<div class="widget widget-table action-table">
					<div class="widget-header"> <i class="icon-th-list"></i>
					  <h3>Tareas registradas</h3>
					</div>
					<!-- /widget-header -->
					<div class="widget-content">
					  <table class="table table-striped table-bordered datatable" id="tablaCursos">
						<thead>
						  <tr>
							<th> Empresa </th>
							<th> Usuario </th>
							<th> Localidad </th>
							<th> Contacto </th>
							<th> Teléfono </th>
							<th> Móvil </th>
							<th> Tarea </th>
							<th> Fecha de Inicio </th>
							<th> Hora </th>
							<th> Resolución </th>
							<th class="centro"> </th>
						  </tr>
						</thead>
						<tbody>
							<?php
								imprimeTareasDetallesCuenta($datos['codigo']);
							?>
						</tbody>
					  </table>
					</div>
					<!-- /widget-content-->
				  </div>

				  
				 </div>
				 <div class="tab-pane" id="pagina2">		
				   <center>
                        <table class="table table-striped table-bordered datatable mitadAncho" id="tablaFacturas">
                          <thead>
                            <tr>
                              <th> Documento </th>
                              <th> Fecha de documento </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosCliente($codigo);

                            ?>
                          
                          </tbody>
                        </table>
                      
                        <a href="javascript:void" id="botonFactura" class="btn btn-success"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 5 MB como máximo.
                        <input type="file" class="hide" name="ficheroFactura" id="ficheroFactura" onchange="nuevaFila();"/>
                      </center>
				 </div>
				 
				 <div class="tab-pane" id="pagina3">		
				   <center>
                        <table class="table table-striped table-bordered datatable mitadAncho" id="tablaFacturasFacturacion">
                          <thead>
                            <tr>
                              <th> Documento </th>
                              <th> Fecha de documento </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosCliente($codigo,'FACTURACION');

                            ?>
                          
                          </tbody>
                        </table>
                      
                        <a href="javascript:void" id="botonFacturaFacturacion" class="btn btn-success"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 5 MB como máximo.
                        <input type="file" class="hide" name="ficheroFacturaFacturacion" id="ficheroFacturaFacturacion" onchange="nuevaFilaFacturacion();"/>
                      </center>
				 </div>
				 
				 <div class="tab-pane" id="pagina4">		
				   <center>
                        <table class="table table-striped table-bordered datatable mitadAncho" id="tablaFacturasConsultoria">
                          <thead>
                            <tr>
                              <th> Documento </th>
                              <th> Fecha de documento </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosCliente($codigo,'CONSULTORIA');

                            ?>
                          
                          </tbody>
                        </table>
                      
                        <a href="javascript:void" id="botonFacturaConsultoria" class="btn btn-success"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 20 MB como máximo.
                        <input type="file" class="hide" name="ficheroFacturaConsultoria" id="ficheroFacturaConsultoria" onchange="nuevaFilaConsultoria();"/>
                      </center>
				 </div>
				 
				 <div class="tab-pane" id="pagina5">		
				   <center>
                        <table class="table table-striped table-bordered datatable mitadAncho" id="tablaFacturasFormacion">
                          <thead>
                            <tr>
                              <th> Documento </th>
                              <th> Fecha de documento </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosCliente($codigo,'FORMACION');

                            ?>
                          
                          </tbody>
                        </table>
                      
                        <a href="javascript:void" id="botonFacturaFormacion" class="btn btn-success"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 5 MB como máximo.
                        <input type="file" class="hide" name="ficheroFacturaFormacion" id="ficheroFacturaFormacion" onchange="nuevaFilaFormacion();"/>
                      </center>
				 </div>
				 
				 <div class="tab-pane" id="pagina6">		
				   <center>
                        <table class="table table-striped table-bordered datatable mitadAncho" id="tablaFacturasMail">
                          <thead>
                            <tr>
                              <th> Documento </th>
                              <th> Fecha de documento </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosCliente($codigo,'MAIL');

                            ?>
                          
                          </tbody>
                        </table>
                      
                        <a href="javascript:void" id="botonFacturaMail" class="btn btn-success"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 5 MB como máximo.
                        <input type="file" class="hide" name="ficheroFacturaMail" id="ficheroFacturaMail" onchange="nuevaFilaMail();"/>
                      </center>
				 </div>
         <div class="tab-pane" id="pagina7">   


                            <?php
                              echo '<fieldset class="span5">';
                              selectEmpresasVinculadas($codigo);
                              echo '</fieldset>';
                              echo '<fieldset class="span5">';
                              $listado=obtieneEmpresasVinculadas($codigo,true);
                              echo '<ul class="empresasVinculadas">';
                              foreach ($listado as $empresa) {
                                echo '<li>'.$empresa.'</li>';
                              }
                              echo '</ul>';
                              echo '</fieldset>';
                            ?>

         </div>
				 </div>


                  <fieldset class="sinFlotar">
                    <div class="form-actions">
					  <?php if($_SESSION['tipoUsuario']!='MARKETING' || ($_SESSION['tipoUsuario']=='MARKETING' && $datosUsuario['habilitado']=='SI')){ ?>
						<button id='botonActualizar' type="button" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar datos</button> 
					  <?php } ?>
                      <a href="posiblesClientes.php" class="btn"><i class="icon-arrow-left"></i> Volver</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script src="js/jquery.inputmask.js" type="text/javascript"></script>
<script type="text/javascript" src="js/iban.js"></script>
<script type="text/javascript" src="js/cif.js"></script>
<script type="text/javascript" src="js/nss.js"></script>
<script type="text/javascript" src="js/validaCP.js"></script>
<script type="text/javascript" src="js/bootstrap-filestyle.js"></script>
<script type="text/javascript" src="js/calculaBic.js"></script>
<script type="text/javascript" src="js/selectAjax.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$('.hasDatepicker').inputmask({"mask": "99/99/9999"});
	$("#creditoFormativo").attr('maxlength','10');
	<?php
		if($datos['tieneColaborador']=='SI'){
			echo '$("#colaboradorOculto").css("display","block");';
		}else{
			echo '$("#colaboradorOculto").css("display","none");';
		}
		
	?>
	
	$("#divFechaBaja").css("display","none");
	
	$("#numCuenta").focusout(function() {
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
		if(isAccountComplete()){
			var value = $("#numCuenta").val();
			if(compruebaCCC(value.substr(0,4), value.substr(4,4), value.substr(8,2), value.substr(10,10))){
				updateBic(value);
			}else{
				alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
			}
		}else{
			alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
		}
	  }).blur(function(){
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
		if(isAccountComplete()){
			var value = $("#numCuenta").val();
			if(compruebaCCC(value.substr(0,4), value.substr(4,4), value.substr(8,2), value.substr(10,10))){
				updateBic(value);
			}else{
				alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
			}
		}else{
			alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
		}
	});
	$("input[type=radio][name=tieneColaborador]").change(function(){
		if(this.value=='SI'){
			$("#colaboradorOculto").css('display','block');
		}else{
			$("#colaboradorOculto").css('display','none');
		}
	});
	
	$("input[type=radio][name=esUnGrupo]").change(function(){
		if(this.value=='SI'){
			$("#grupoOculto").css('display','block');
		}else{
			$("#grupoOculto").css('display','none');
		}
	});
	
	/*$("#ccc").focusout(function() {
		validaNss($("#ccc").val());
	});*/
	
	$("#cif").focusout(function() {
		//isValidCif($("#cif").val());
		compruebaDuplicados($('#cif').val());
	});
	
	$("#cp").focusout(function() {
		var provincia=obtieneProvincia($("#cp").val());
		if(provincia!=false){	
			$("#provincia").val(provincia);
		}else{
			alert("Error en la introducción del CP. Introduzca uno válido.");
		}
	});
	$('#botonFactura').click(function(){
      if($('#ficheroFactura').val()==''){
        $('#ficheroFactura').click();
      }
    });
	$('#botonFacturaFacturacion').click(function(){
      if($('#ficheroFacturaFacturacion').val()==''){
        $('#ficheroFacturaFacturacion').click();
      }
    });
	$('#botonFacturaFormacion').click(function(){
      if($('#ficheroFacturaFormacion').val()==''){
        $('#ficheroFacturaFormacion').click();
      }
    });
	$('#botonFacturaConsultoria').click(function(){
      if($('#ficheroFacturaConsultoria').val()==''){
        $('#ficheroFacturaConsultoria').click();
      }
    });
	$('#botonFacturaMail').click(function(){
      if($('#ficheroFacturaMail').val()==''){
        $('#ficheroFacturaMail').click();
      }
    });
	
	$("input[type=radio][name=baja]").change(function(){
		if(this.value=='SI'){
			$("#divFechaBaja").css('display','block');
		}else{
			$("#divFechaBaja").css('display','none');
		}
	});
	
	$('#botonActualizar').click(function(){
		compruebaBaja();
    });
	
	<?php if($datos['estado']!='Pendiente'){ 
			echo "$('#pendienteOculto').css('display','none');";
		}
	?>
	$('#estado').change(function(){
		if($(this).val()=='Pendiente'){
			$('#pendienteOculto').show(300);
		}else{
			$('#pendienteOculto').hide(300);
		}
	});
	
	$('#activarFiltrado').click(function(){
		var anio=$('#anioFiltrado').val();
		var codigo=$('#codigo').val();
		$.ajax({
			data: "anio="+anio+"&codigo="+codigo,
			type: "POST",
			url: "listadosajax/cargaHistoricoObservaciones.php",
		})
		.done(function( data, textStatus, jqXHR ) {
			$('#historicoObservaciones').val(data);
		});
	});
	
	$('#filtrar2').click(function(){
		var anio=$('#anioFiltrado2').val();
		var codigo=$('#codigo').val();
		$.ajax({
			data: "anio="+anio+"&codigo="+codigo,
			type: "POST",
			url: "listadosajax/cargaHistoricoTareas.php",
		})
		.done(function( data, textStatus, jqXHR ) {
			$('#historicoTareas').val(data);
		});
	});
  });
  
   function nuevaFila(){
    $('#botonFactura').attr('disabled',true);
    $('#tablaFacturas').find("tr:last").after("<tr><td><input type='text' name='nombreFactura' class='input-large' /><input type='hidden' name='tipo' value='CONTRATOS' /></td><td><input type='text' name='fechaFactura' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('form').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  
  function nuevaFilaFacturacion(){
    $('#botonFacturaFacturacion').attr('disabled',true);
    $('#tablaFacturasFacturacion').find("tr:last").after("<tr><td><input type='text' name='nombreFactura' class='input-large' /><input type='hidden' name='tipo' value='FACTURACION' /></td><td><input type='text' name='fechaFactura' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('form').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  
  function nuevaFilaConsultoria(){
    $('#botonFacturaConsultoria').attr('disabled',true);
    $('#tablaFacturasConsultoria').find("tr:last").after("<tr><td><input type='text' name='nombreFactura' class='input-large' /><input type='hidden' name='tipo' value='CONSULTORIA' /></td><td><input type='text' name='fechaFactura' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('form').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  
  function nuevaFilaFormacion(){
    $('#botonFacturaFormacion').attr('disabled',true);
    $('#tablaFacturasFormacion').find("tr:last").after("<tr><td><input type='text' name='nombreFactura' class='input-large' /><input type='hidden' name='tipo' value='FORMACION' /></td><td><input type='text' name='fechaFactura' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('form').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  
  function nuevaFilaMail(){
    $('#botonFacturaMail').attr('disabled',true);
    $('#tablaFacturasMail').find("tr:last").after("<tr><td><input type='text' name='nombreFactura' class='input-large' /><input type='hidden' name='tipo' value='MAIL' /></td><td><input type='text' name='fechaFactura' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('form').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  
  function compruebaBaja(){
	var baja=$("input[type=radio][name=baja]:checked").val();
	if(baja=='SI'){
		if(confirm("Este registro se enviará a la cartera de baja, ¿está seguro?")){
			$("#edit-profile").submit();
		}
	}else{
		$("#edit-profile").submit();
	}
  }
  
  function compruebaDuplicados(codigo){
	$.ajax({
        type: "POST",
        url: "listadosajax/compruebaCif.php",
        data: "cif=" + codigo,
        success: function(response){
			if(response!='OK'){
				alert("ERROR:  CIF/NIF duplicado, por favor revise si la empresa ya está registrada");
			}
        }
    });
  }
</script>
