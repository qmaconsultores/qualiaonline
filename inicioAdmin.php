<?php
   /* La sección activa y la cabecera se especifican en el archivo padre inicio.php
  $seccionActiva=0;
  include_once('cabecera.php');
  */
    
  $estadisticas=creaEstadisticasInicio();
  $estadisticasVentas=creaEstadisticasVentasInicio();
  
  $fecha=date('Y-m-d');
  $consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM accesos WHERE fecha LIKE '$fecha%';",true);
  $datosConsulta=mysql_fetch_assoc($consulta);
  if($datosConsulta['codigo']<=1){
	enviaInformeFacturacion();
	enviaFacturas();
  }

  if(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaTarea();
  }
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
	  
		<?php if($_SESSION['codigoS']=='100000000000000'){ ?>
		
					<div class="span6">
					  <div class="widget widget-nopad" id="target-1">
						<div class="widget-header"> <i class="icon-home"></i>
						  <h3>Inicio</h3>
						</div>
						<!-- /widget-header -->
						
						<div class="widget-content">
						  <div class="widget big-stats-container">
							<div class="widget-content">
							  <h6 class="bigstats">Bienvenido <?php echo $usuario; ?>. A continuación se muestran las ventas realizadas:</h6>

							   <div id="big_stats" class="cf">

								 <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$estadisticasVentas['totalHoy'], 2, ',', ''); ?></span> <br>Total hoy</div>
								  <!-- .stat -->
								  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$estadisticasVentas['totalSemana'], 2, ',', '');?></span> <br>Total semana</div>
								  <!-- .stat -->

								  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$estadisticasVentas['totalMes'], 2, ',', '');?></span> <br>Total mes</div>
								  <!-- .stat -->

							   </div>

							</div> <!-- /widget-content -->
							<!-- /widget-content --> 
							
						  </div>
						</div>
					  </div>
					 
					</div>
					
					<div class="span6">
					  <div class="widget" id="target-2">
						<div class="widget-header"> <i class="icon-th"></i>
						  <h3>Opciones</h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
						  <div class="shortcuts">
							<a href="cerrarSesion.php" class="shortcut"><i class="shortcut-icon icon-off"></i><span class="shortcut-label">Cerrar sesión</span> </a>
						  </div>
						  <!-- /shortcuts --> 
						</div>
						<!-- /widget-content --> 
					  </div>
					</div>
					
					<div class="span12">
					  <div class="widget widget-nopad">
						<div class="widget-header"> <i class="icon-calendar"></i>
						   <h3>Ventas realizadas</h3>
						</div>
						  <!-- /widget-header -->
						<div class="widget-content">
						 
						  <div id='calendar'></div>
						  <!-- /widget-content --> 
						</div>
					  </div>
					</div>
					
				</div><!-- /row -->
				
					<div class="widget widget-table action-table">
					<div class="widget-header"> <i class="icon-th-list"></i>
					  <h3>Ventas realizadas</h3>
					</div>
					<!-- /widget-header -->
					<div class="widget-content">
					   <table class='table table-striped table-bordered datatable'>
						<thead>
						  <tr>
							<th> Empresa </th>
							<th> Tipo de venta </th>
							<th> Comercial </th>
							<th> Concepto </th>
							<th> Fecha </th>
							<th> Importe </th>
							<th> Observaciones </th>
						  </tr>
						</thead>
						<tbody>
						
						<?php imprimeVentasGeneralInicio(); ?>
						
						</tbody>
					  </table>
						
					</div>
					<!-- /widget-content-->
				  </div>
				
		<?php }else{ ?>

					<div class="span12">
					  <div class="widget widget-nopad" id="target-1">
						<div class="widget-header"> <i class="icon-home"></i>
						  <h3>Inicio</h3>
						</div>
						<!-- /widget-header -->
						
						<div class="widget-content">
						  <div class="widget big-stats-container">
							<div class="widget-content">
							  <h6 class="bigstats">Bienvenido de nuevo <?php echo $usuario; ?>. A continuación se muestra el estado del sistema:</h6>

							   <div id="big_stats" class="cf">

								 <div class="stat"> <i class="icon-briefcase"></i> <span class="value"><?php echo $estadisticas['cartera']?></span> <br>Total cartera</div>
								  <!-- .stat -->
								  <div class="stat"> <i class="icon-star"></i> <span class="value"><?php echo $estadisticas['nueva']?></span> <br>Total V. Nueva</div>
								  <!-- .stat -->

								  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo $estadisticas['totalcartera']?></span> <br>Total cartera</div>
								  <!-- .stat -->
								  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo $estadisticas['totalnueva']?></span> <br>Total V. Nueva</div>
								  <!-- .stat -->

								  <div class="stat"> <i class="icon-calendar"></i> <span class="value"><?php echo $estadisticas['tareas']?></span> <br>Tareas pendientes</div>
								  <!-- .stat -->

								  
								  <div class="stat"> <i class="icon-exclamation-sign <?php if($estadisticas['incidencias']>0){ echo "rojo"; } ?>"></i> <span class="value"><?php echo $estadisticas['incidencias']?></span> <br>Incidencias pendientes</div>
								  <!-- .stat -->
								  
								  <div class="stat"> <i class="icon-book"></i> <span class="value"><?php echo $estadisticas['auditorias']?></span> <br>Auditorías pendientes</div>
								  <!-- .stat -->

							   </div>

							   <?php compruebaObjetivos();
								compruebaObjetivosImporte();				   
							   ?>

							</div> <!-- /widget-content -->
							<!-- /widget-content --> 
							
						  </div>
						</div>
					  </div>
		<?php } ?>			 
					  <div class="widget" id="target-2">
						<div class="widget-header"> <i class="icon-th"></i>
						  <h3>Filtro</h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
						  <form method="post" action="?">
					  		<?php
					  			campoSelect('mesRanking','Mes',array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'),array('0','01','02','03','04','05','06','07','08','09','10','11','12'));
					  			$anios=array('');
					  			$fecha=date('Y-m-d');
					  			$fecha=explode('-', $fecha);
					  			for($i=2015;$i<=$fecha[0];$i++){
					  				array_push($anios, $i);
					  			}
					  			campoSelect('anioRanking','Año',$anios,$anios);
					  		?>
					  		<input type="submit" value="Filtrar">
					  		</form>
						  <!-- /shortcuts --> 
						</div>
						<!-- /widget-content --> 
					  </div>

					<div class="widget widget-table action-table">
						<div class="widget-header"> <i class="icon-th-list"></i>
							<?php 
								$mes='';
								$anio='';
								if(isset($_POST['mesRanking'])){
									$mes=$_POST['mesRanking'];
									$anio=$_POST['anioRanking'];
								} 
								
								if($mes=='' || $mes=='0'){
									$mes=date('m');
								}
								if($anio=='' || $anio=='0'){
									$anio=date('Y');
								}
								?>
					  		<h3>Ranking de comerciales de <?php echo $mes.'/'.$anio;?></h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
					   		<table class='table table-striped table-bordered'>
								<thead>
						  			<tr>
										<th> Comercial </th>
										<th> Ventas nuevas </th>
										<th> Importe ventas nuevas</th>
										<th> Cartera </th>
										<th> Importe cartera</th>
										<th> Total Ventas </th>
										<th> Total Importe </th>
						  			</tr>
								</thead>
								<tbody>
						
								<?php 

								imprimeRanking($mes,$anio); 

								?>
						
								</tbody>
					  		</table>
						</div>
					<!-- /widget-content-->
				  </div>
					</div>
					<div class="span12">
					  <div class="widget" id="target-2">
						<div class="widget-header"> <i class="icon-th"></i>
						  <h3>Opciones</h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
						  <div class="shortcuts">
							<a href="javascript:void;" id='guia' class="shortcut"><i class="shortcut-icon icon-book"></i><span class="shortcut-label">Guía rápida</span> </a>
							<a href="cerrarSesion.php" class="shortcut"><i class="shortcut-icon icon-off"></i><span class="shortcut-label">Cerrar sesión</span> </a>
							<br>
							<a href="creaTarea.php" class="shortcut"><i class="shortcut-icon icon-list-ol"></i><span class="shortcut-label">Añadir Tarea</span> </a>
							<a href="#" id="email" class="shortcut"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Enviar eMail</span> </a>
							<?php compruebaOpcionEliminar(); ?>
						  </div>
						  <!-- /shortcuts --> 
						</div>
						<!-- /widget-content --> 
					  </div>
					</div>

					<!-- /span6 -->
				</div><!-- /row -->
		

      <?php
          if(isset($_POST['destinatarios'])){
            if($res){
              mensajeOk("Mensaje enviado correctamente."); 
            }
            else{
              mensajeError("no se ha podido enviar el mensaje. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el posible cliente. Contacte con el webmaster."); 
            }
          }
        ?>

      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-th-list"></i>
          <h3>Devoluciones</h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable" id="target-3">
            <thead>
              <tr>
                <th> Referencia </th>
				<th> Cliente </th>
				<th> Concepto </th>
				<th> Fecha de emisión </th>
				<th> Fecha de vencimiento </th>
				<th> Importe </th>
				<th> ¿Cobrada? </th>
				<th> ¿Enviada banco? </th>
				<th> ¿Enviada cliente? </th>
				<th> Devolución y resolución </th>
                <th class="centro"> </th>
                <th><input type='checkbox' id="todo"></th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeFacturas("WHERE devuelta='SI' AND cobrada='NO' AND resolucionFactura=''",false,true);
              ?>
            
            </tbody>
          </table>
        </div>
        <!-- /widget-content-->
      </div>
        
      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-th-list"></i>
          <h3>Tareas pendientes para hoy</h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable" id="target-3">
            <thead>
              <tr>
                <th> Empresa </th>
				<th> Usuario </th>
                <th> Localidad </th>
				<th> Contacto </th>
                <th> Teléfono </th>
				<th> Móvil </th>
                <th> Tarea </th>
                <th> Fecha de Inicio </th>
				<th> Hora </th>
                <th> Resolución </th>
                <th class="centro"> </th>
                <th><input type='checkbox' id="todo"></th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeTareas(true,'FALSO','',true);
              ?>
            
            </tbody>
          </table>
        </div>
        <!-- /widget-content-->
      </div>
	  

    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/guidely/guidely.min.js"></script>
<script type="text/javascript" src="js/bootstrap-progressbar.js"></script>

<script type="text/javascript" src="js/full-calendar/fullcalendar.min.js"></script>

<script src="../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="../api/js/chart.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  
  guidely.add ({
    attachTo: '#target-1'
    , anchor: 'top-left'
    , title: 'Panel de Estadísticas'
    , text: 'En el lado izquierdo de su pantalla encontrará el Panel de Estadísticas, que le servirá para ver de forma rápida y visual el estado del sistema.'
  });
  
  guidely.add ({
    attachTo: '#target-2'
    , anchor: 'top-right'
    , title: 'Panel de Acciones'
    , text: 'En este panel se agrupan las distintas opciones disponibles en cada sección (crear un nuevo elemento, acceder a una subsección, generar un fichero, ...).<br>Para acceder a una opción solo tiene que pulsar el botón correspondiente.'
  });
  
  guidely.add ({
    attachTo: '#target-3'
    , anchor: 'middle-middle'
    , title: 'Listado de Elementos'
    , text: 'Esta tabla muestra un listado en el que podrá consultar los elementos creados en cada sección. Utilice la caja de <b>Búsqueda</b> para filtrar los registros a visualizar.'
  });
  
  guidely.add ({
    attachTo: '#target-4'
    , anchor: 'top-right'
    , title: 'Menú principal'
    , text: 'Una vez que accede al sistema, el Menú Principal está siempre disponible para permitirle navegar entre los distintos módulos de la herramienta. Solo tiene que pulsar en la sección correspondiente para acceder a ella.'
  });

    guidely.add ({
    attachTo: '#target-5'
    , anchor: 'bottom-right'
    , title: 'Menú de Usuario'
    , text: 'Por último, el Menú de Usuario le recuerda en cada momento con que cuenta está trabajando. Además, haciendo click en él se le proporciona un acceso directo para cerrar su sesión.'
  });
  
  $('#guia').click(function(){
    guidely.init ({welcome: true, startTrigger: false, welcomeTitle: 'Guía de uso básico', welcomeText: 'Bienvenido a la Guía de uso del sistema. Pulsando en el botón Iniciar, la plataforma le mostrará unas sencillas explicaciones sobre los distintos elementos que encontrará en la mayoría de las secciones.' });
  });

  $('.progress .bar').progressbar();
    
});

 //Parte de ránking comercial año en curso
  <?php
      //$datos=generaDatosRanking();
    ?>

    //var grafico = new Chart(document.getElementById("pie-chart").getContext("2d")).PolarArea(pieData);

<?php if($_SESSION['codigoS']=='21'){ ?>
$(document).ready(function() {


$('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('inicio.php',valoresChecks,'post');
    }
 });

$('#email').click(function(){
  var valoresChecks=recorreChecks();
  creaFormulario('enviarEmail.php?seccion=0',valoresChecks,'post');
});

  var eventosCreados=0;//Para corregir un BUG que se produce con el callback select de Fullcalendar y el resourceView: se crean en un mismo resource tantos eventos como se han creado anteriormente sin recargar la página.
  var eventoActual=1;

  var colorFondo={'normal':"#7ba9ee",'alta':"#ea807e",'baja':"#7acc76"};
  var colorBorde={'normal':"#3f85f5",'alta':"#b94a48",'baja':"#00a100"};

  var calendario = $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultView: 'month',
    selectable:true,
    selectHelper: true,
    editable:true,

    firstDay:1,//Añadido por mi
    titleFormat: {
      month: 'MMMM yyyy',
      week: "d MMM [ yyyy]{ '&#8212;' d [ MMM] yyyy}",
      day: 'dddd, d MMM, yyyy'
    },
    columnFormat: {
      month: 'ddd',
      week: 'ddd d/M',
      day: 'dddd d/M'
    },
    axisFormat: 'H:mm',
    minTime:9,
    maxTime:19,
    firstHour:7,
    slotMinutes:15,
    timeFormat: 'H:mm { - H:mm}',//Fin añadido por mi
    events:[
      <?php
        generaEventosCalendarioVentas();
      ?>]
  });
 });
 
 <?php } ?>
</script>

