<?php
  $seccionActiva=5;
  include_once('cabecera.php');
  
  $codigo=$_GET['codigo'];
  
  $datos=datosTutor($codigo);
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Detalles tutor</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="tutores.php" method="post">
                  <fieldset>


                    <div class="control-group">                     
                      <label class="control-label" for="nombre">Nombre:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="nombre" name="nombre" value="<?php echo $datos['nombre']; ?>">
						<input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    
                    <div class="control-group">                     
                      <label class="control-label" for="apellidos">Apellidos:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="apellidos" name="apellidos" value="<?php echo $datos['apellidos']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="dni">DNI:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="dni" name="dni" value="<?php echo $datos['dni']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono" value="<?php echo $datos['telefono']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="email">eMail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="email" name="email" value="<?php echo $datos['email']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php
						campoTexto('especialidad','Especialidad',$datos);
					?>
					
					<h3 class='apartadoFormulario'>Datos de tutorías</h3>
					
					<div class="control-group">                     
                      <label class="control-label" for="horario">Horario mañana:</label>
                      <div class="controls">
                        <input type="text" class="input-small timepicker" id="horaInicio" name="horaInicio" value="<?php echo $datos['horaInicio']; ?>">
            						 a 
            						<input type="text" class="input-small timepicker" id="horaFin" name="horaFin" value="<?php echo $datos['horaFin']; ?>">
            						 (hh:mm) Comprendido entre 00:01h. y 15:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				          	<div class="control-group">                     
                      <label class="control-label" for="horario">Horario tarde:</label>
                      <div class="controls">
                        <input type="text" class="input-small timepicker" id="horaInicioTarde" name="horaInicioTarde" value="<?php echo $datos['horaInicioTarde']; ?>">
            						 a 
            						<input type="text" class="input-small timepicker" id="horaFinTarde" name="horaFinTarde" value="<?php echo $datos['horaFinTarde']; ?>">
            						 (hh:mm) Comprendido entre 15:01h. y 00:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					          <div class="control-group">                     
                      <label class="control-label" for="horasTutoria">Horas tutorías:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horasTutoria" name="horasTutoria" value="<?php echo $datos['horasTutoria']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="horas">Días impartición:</label>
                      <div class="controls">
                        <table class="mitadAncho">
                          <tr>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                          </tr>
                          <tr>
                            <th><input type="checkbox" id="lunes" name="lunes" <?php if($datos['lunes']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="martes" name="martes" <?php if($datos['martes']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="miercoles" name="miercoles" <?php if($datos['miercoles']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="jueves" name="jueves" <?php if($datos['jueves']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="viernes" name="viernes" <?php if($datos['viernes']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="sabado" name="sabado" <?php if($datos['sabado']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="domingo" name="domingo" <?php if($datos['domingo']=='SI'){ echo "checked='checked'";} ?>></th>
                          </tr>
                        </table>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar tutor</button> 
                      <a href="tutores.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-timepicker.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.timepicker').timepicker({showMeridian: false,defaultTime: false});
  });
</script>
