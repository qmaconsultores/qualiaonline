<?php
  $seccionActiva=6;
  include_once('cabecera.php');
  
  if(isset($_POST['codigo'])){
    $res=actualizaAlumno();
  }
  elseif(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaAlumno();
  }

  $estadisticas=creaEstadisticasAlumnos('AND fechaFin<CURDATE()');
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de alumnos con cursos finalizados</h6>

                   <div id="big_stats" class="cf">

                     <div class="stat"> <i class="icon-pencil"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Alumnos registrados</div>
                      <!-- .stat -->

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de alumnos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="formacion.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Alumnos en curso</span> </a>
				<a href="alumnosPendientes.php" class="shortcut"><i class="shortcut-icon icon-exclamation"></i><span class="shortcut-label">Alumnos pendientes</span> </a>
				<br>
                <a href="#" id='email' class="shortcut"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Enviar eMail</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Datos del alumno actualizados."); 
            }
            else{
              mensajeError("no se ha podido actualizar el alumno. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['destinatarios'])){
            if($res){
              mensajeOk("Mensaje enviado correctamente."); 
            }
            else{
              mensajeError("no se ha podido enviar el mensaje. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el posible cliente. Contacte con el webmaster."); 
            }
          }
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Alumnos cursos finalizados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable" id="tablaAlumnos">
                <thead>
                  <tr>
                    <th> Acción formativa </th>
                  <th> Curso </th>
          <th> Empresa </th>
          <th> Alumno </th>
          <th> Fecha inicio </th>
          <th> Fecha fin </th>
          <th> Teléfono </th>
          <th> Llamada de inicio </th>
          <th> Resolución </th>
          <th> Plataforma </th>
          <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeAlumnosNuevo('AND fechaFin<CURDATE()');
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<script type="text/javascript" src="js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
    $('.selectpicker').selectpicker();
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaCursos');
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');realizaBusquedaFiltrada('#cajaFiltros','#tablaCursos');});
    $('.resolucion').change(function(){
      var resolucion = $(this).val();
      var codigo = $(this).attr('codigo');
      var codigoCurso = $(this).attr('codigoCurso');
      actualizaResolucion(resolucion, codigo,codigoCurso);
    });

    $('.marcarFecha').click(function(){
      var d = new Date();
      var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getYear()+' '+d.getHours()+':'+d.getMinutes();
      $(this).html(fecha);
      $(this).removeClass();
      $(this).addClass('llamadaHecha');
      var codigo = $(this).attr('codigo');
      var codigoCurso = $(this).attr('codigoCurso');
      actualizaFecha(codigo,codigoCurso);
    });
  });

  function actualizaFecha(codigo, codigoCurso){
    $.post('gestionesAjax.php?funcion=actualizaFechaFormacion();',{'codigo':codigo,'codigoCurso':codigoCurso});
  }
  
  function actualizaResolucion(resolucion, codigo, codigoCurso){
    $.post('gestionesAjax.php?funcion=actualizaResolucion();',{'codigo':codigo,'codigoCurso':codigoCurso,'resolucion':resolucion});
  }
    $('#email').click(function(){
      var valoresChecks=recorreChecks();
      creaFormulario('enviarEmail.php?seccion=6',valoresChecks,'post');
    });


    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes un cliente.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('alumnosFinalizados.php',valoresChecks,'post');
      }

    });

    var tabla=$('#tablaAlumnos').DataTable({
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap",
    "bStateSave":true,
    "iDisplayLength":25,
    "oLanguage": {
      "sLengthMenu": "_MENU_ registros por página",
      "sSearch":"Búsqueda:",
      "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
      "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
      "sEmptyTable":"Aún no hay datos que mostrar",
      "sInfoEmpty":"",
      'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
      'sZeroRecords':'No se han encontrado coincidencias'
    },
    "fnDrawCallback": function (oSettings) {
      $('.selectpicker').selectpicker();
      $('.resolucion').change(function(){
        var resolucion = $(this).val();
        var codigo = $(this).attr('codigo');
        var codigoCurso = $(this).attr('codigoCurso');
        actualizaResolucion(resolucion, codigo,codigoCurso);
      });
      $('.marcarFecha').click(function(){
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getYear()+' '+d.getHours()+':'+d.getMinutes();
        $(this).html(fecha);
        $(this).removeClass();
        $(this).addClass('llamadaHecha');
        var codigo = $(this).attr('codigo');
        var codigoCurso = $(this).attr('codigoCurso');
        actualizaFecha(codigo,codigoCurso);
      });
    }});

    //var tabla=$('#tablaAlumnos').dataTable();
    //tabla.fnFilter(marcado);



</script>