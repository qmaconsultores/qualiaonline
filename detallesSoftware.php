<?php
  $seccionActiva=16;
  include_once("cabecera.php");
  $datos=datosRegistro('software',$_GET['codigo']);
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Datos tarea de Software</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="software.php" method="post">
                  <fieldset>

                    <?php
                      campoOculto($datos);

                      $consulta="SELECT codigo, empresa AS texto FROM clientes";
                      campoSelectConsulta('codigoCliente','Cliente',$consulta,$datos);
                      campoSelect('tipoTarea','Tipo de tarea',array('Creación de Software','Personalización','Modificaciones y/o mejoras','Incidencias'),array('CREACION','DEMO','MODIFICACION','INCIDENCIA'),$datos);
                      campoFecha('fechaSolicitud','Fecha de solicitud',$datos);
                      campoFecha('fechaPrevista','Fecha de prevista de finalización',$datos);
                      $consulta="SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo='PROGRAMADOR';";
                      campoSelectConsulta('codigoUsuario','Programador asignado',$consulta,$datos);
                      campoRadio('realizado','Estado',$datos,array('Pendiente','En desarrollo','Finalizado'),array('1','2','3'));
                      
                      echo "<div id='cajaDesarrollo'>";
                        creaTablaDesarrollo($datos['codigo']);
                      echo "</div><div id='cajaRealizado'>";
                        campoFecha('fechaFinalizacion','Fecha de finalización',$datos);
                        campoTexto('tiempoEmpleado','Tiempo empleado',$datos,'input-small');
                      echo "</div>";

                      areaTexto('observaciones','Observaciones',$datos);

                    ?>
                    
                    <br />                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar Tarea</button> 
                      <a href="software.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    <?php
      if($datos['realizado']=='3'){
        echo "$('#cajaRealizado').css('display','block');
              $('#cajaDesarrollo').css('display','block');";
      }
      elseif($datos['realizado']=='2'){
        echo "$('#cajaRealizado').css('display','none');
              $('#cajaDesarrollo').css('display','block');"; 
      }
      else{
        echo "$('#cajaRealizado').css('display','none');
              $('#cajaDesarrollo').css('display','none');";
      }
    ?>

    $('input[name=realizado]').change(function(){
      if($(this).val()=='3'){
        $('#cajaRealizado').css('display','block');
        $('#cajaDesarrollo').css('display','block');
      }
      else if($(this).val()=='2'){
        $('#cajaDesarrollo').css('display','block');
        $('#cajaRealizado').css('display','none');
      }
      else{
        $('#cajaRealizado').css('display','none');
        $('#cajaDesarrollo').css('display','none');
      }

    })

  });
</script>