<?php

// =========================================================== Funciones comunes ====================================================================================

function login($usuario,$clave){
	if($usuario!='' && $clave!=''){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, usuario, tipo FROM usuarios WHERE usuario='$usuario' AND clave='$clave';");
	cierraBD();
	if(mysql_num_rows($consulta)==1){
		$datos=mysql_fetch_assoc($consulta);
		$_SESSION['codigoS']=$datos['codigo'];
		$_SESSION['usuario']=$datos['usuario'];
		$_SESSION['tipoUsuario']=$datos['tipo'];

		if(isset($_POST['sesion']) && $_POST['sesion']=="si"){//Crea cookie de Sesión
			$valor=md5(rand());
			conexionBD();
			$consulta=consultaBD("UPDATE usuarios SET sesion='$valor' WHERE usuario='$usuario' AND clave='$clave';");
			cierraBD();
			// Caduca en un año
    		setcookie('sesionCRMQualia',$valor, time() + 365 * 24 * 60 * 60);//PARA CONFIG
		}
		registraAcceso();
		header('Location: inicio.php');
	}
	return 1;
	}
}

function conexionBD(){
	/*if($_POST['usuario']=='temporal' || $_SESSION['usuario']=='temporal'){
		$conexion=mysql_connect("localhost","grupqualia-temp","Writemaster7");
		$db='grupqualia-temp';
	} else {
		$conexion=mysql_connect("localhost","grupqualia","Writemaster7");
		$db='grupqualia';
	}*/
	$conexion=mysql_connect("localhost","root","root");//PARA CONFIG
	$db='grupqualia';
	/* PARA VERSIÓN PRUEBAS 
	$conexion=mysql_connect("localhost","qualia_pruebas3","Writemaster7");
	$db='qualia_pruebas3';*/
	if(!$conexion){ 
		echo "Error estableciendo la conexi&oacute;n a la BBDD.<br />";
	}
	else{
		if(!mysql_select_db($db)){
			echo "Error seleccionando base de datos QMA.<br />";
		}
	}

}




function cierraBD(){
	mysql_close();
}


/*
NUEVO: la función consultaBD tiene ahora 2 parámetros opcionales: $conexion, que si es true no es necesario
abrir ni cerrar la BDD antes de la consulta, y $assoc, que si es true devuelve el mysql_fetch_assoc de la consulta
(para consultas que devuelvan solo un registro).
*/
function consultaBD($query,$conexion=false,$assoc=false){
	if($conexion){
		conexionBD();
	}
	insertaEnHistorial($query);
	$consulta=mysql_query($query);
	if($conexion){
		cierraBD();
	}
	if($assoc){
		$consulta=mysql_fetch_assoc($consulta);
	}
	return $consulta;
}

function insertaEnHistorial($query){
	if(strpos($query, 'UPDATE')!==false || strpos($query, 'update')!==false || strpos($query, 'DELETE')!==false || strpos($query, 'delete')!==false || strpos($query, 'INSERT')!==false || strpos($query, 'insert')!==false){
		$query=str_replace("'", "\'", $query);
		$fecha=formateaFechaBD(fecha());
		$hora=hora();
		$codigo=isset($_SESSION['codigoS'])?$_SESSION['codigoS']:NULL;
		$sql="INSERT INTO historial VALUES (NULL,'".$fecha."','".$hora."','".$query."',".$codigo.")";
		//echo $sql.'<br/><br/>';
		mysql_query($sql);
	}
}


//Realiza una consulta que selecciona un único campo por registro y devuelve éstos en un array (para usar con in_array).
function consultaArray($query,$conexion=false){
	$res=array();
	$consulta=consultaBD($query,$conexion);
	
	$datos=mysql_fetch_row($consulta);
	while($datos!=false){
		array_push($res,$datos[0]);
		$datos=mysql_fetch_row($consulta);
	}

	return $res;
}

function compruebaSesion(){
	if(!isset($_SESSION['codigoS']) || !isset($_SESSION['usuario'])){
		header('Location: index.php');
	}
}


function imprimeFecha(){
	echo fecha();
}

function imprimeHora(){
	echo hora();
}

//Nuevas funciones que devuelven la fecha y la hora (sin imprimirlas por pantalla, para operar con ellas)
function fecha(){
	return date('d')."/".date('m')."/".date('Y');
}

function hora(){
	return date('H').":".date('i');
}

function mensajeError($mensaje){
	echo '<div class="errorLogin">
			<div class="alert alert-danger">
				<i class="icon-exclamation-sign"></i> 
				<button type="button" class="close" data-dismiss="alert">&times;</button>
	  			<strong>Error: </strong> '.$mensaje.'
			</div>
		</div>';
}

function mensajeOk($mensaje){
	echo '<div class="errorLogin">
			<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
	  			<i class="icon-info-sign"></i> 
				<strong> '.$mensaje.' </strong>
			</div>
		</div>';
}


function compruebaCookie(){
	if(isset($_COOKIE['sesionCRMQualia'])){//PARA CONFIG
		$valor=$_COOKIE['sesionCRMQualia'];
		conexionBD();
		$consulta=consultaBD("SELECT codigo, usuario, tipo FROM usuarios WHERE sesion='$valor';");
		cierraBD();
		if(mysql_num_rows($consulta)==1){
			$datos=mysql_fetch_assoc($consulta);
			$_SESSION['codigoS']=$datos['codigo'];
			$_SESSION['usuario']=$datos['usuario'];
			$_SESSION['tipoUsuario']=$datos['tipo'];

			registraAcceso();
			header('Location: inicio.php');
		}
	}
}


function formateaFechaBD($fechaV){
	$fecha=$fechaV;
	if(trim($fechaV)!=''){
		$fechaA=explode("/",$fecha);
		$fecha=$fechaA[2]."-".$fechaA[1]."-".$fechaA[0];
	}
	return $fecha;
}

function formateaFechaWeb($fechaV){
	if($fechaV!='0000-00-00' && trim($fechaV)!=''){
		$fechaA=explode("-",$fechaV);
		$fecha=$fechaA[2]."/".$fechaA[1]."/".$fechaA[0];
		return $fecha;
	}
	else{
		return '';
	}
}

function arrayFormulario(){
	$datos=array();
	foreach($_POST as $nombre=>$valor){
		if(substr_count($nombre,'fecha')==1){//Para formatear la fecha a almacenar
			$valor=formateaFechaBD($valor);
		}
		elseif(!is_array($valor)){
			$valor=addslashes($valor);
		}
		$datos[$nombre]=$valor;
	}
	return $datos;
}

function registraAcceso(){
	$fecha=date('Y')."-".date('m')."-".date('d');
	$hora=date("H").":".date("i").":".date("s");
	
	$codigoUsuario=$_SESSION['codigoS'];
	$ip=obtieneIP();
	
	$userAgent = $_SERVER['HTTP_USER_AGENT'];
	$nav=obtieneNav($userAgent);
	$so=obtieneSO($userAgent);
	
	consultaBD("INSERT INTO accesos VALUES(NULL,'$codigoUsuario','$fecha $hora','$ip','$nav','$so');",true);
}

function obtieneIP(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else{
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function obtieneNav($userAgent){
	$resultado="Internet Explorer";
	$navegadores = array(
		'Opera' => '/Opera/',
		'Mozilla Firefox'=> '/(Firebird)|(Firefox)/',
		'Galeon' => '/Galeon/',
		'Mozilla'=>'/Gecko/',
		'MyIE'=>'/MyIE/',
		'Lynx' => '/Lynx/',
		'Konqueror'=>'/Konqueror/',
		'Internet Explorer 7' => '/(MSIE 7\.[0-9]+)/',
		'Internet Explorer 6' => '/(MSIE 6\.[0-9]+)/',
		'Internet Explorer 5' => '/(MSIE 5\.[0-9]+)/',
		'Internet Explorer 4' => '/(MSIE 4\.[0-9]+)/',
		'Safari'=>'/Safari/',
		'Chrome'=>'/Chrome/',
		'Android'=>'/Android/'
	);
	foreach($navegadores as $navegador=>$iden){
		if (preg_match($iden, $userAgent)){
			$resultado=$navegador;
		}
	}
	return $resultado;
}

function obtieneSO($userAgent){
	$so="Windows";
	$userAgent = $_SERVER['HTTP_USER_AGENT'];
	$userAgent = strtolower ($userAgent);
	
	if(strpos($userAgent, "windows")){
		$so="Windows";
	} 
	elseif(strpos($userAgent, "android")) { 
		$so="Android";
	}
	elseif(strpos($userAgent, "linux")) { 
		$so="Linux";
	}
	elseif(strpos($userAgent, "iphone")) { 
		$so="iOS (iPhone)";
	}
	elseif(strpos($userAgent, "ipad")) { 
		$so="iOS (iPad)";
	}
	elseif(strpos($userAgent, "ipod")) { 
		$so="iOS (iPod)";
	}
	elseif(strpos($userAgent, "mac")) { 
		$so="Mac";
	}
	elseif(strpos($userAgent, "unix")) { 
		$so="Unix";
	}
	
	return $so;
}

function imprimeAccesos(){
	conexionBD();

	$consulta=consultaBD("SELECT usuario, fecha, ip, navegador, sistema FROM accesos INNER JOIN usuarios ON accesos.codigoUsuario=usuarios.codigo ORDER BY fecha DESC;");
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		$fechaA=explode(' ',$datos['fecha']);
		$hora=$fechaA[1];
		$fecha=formateaFechaWeb($fechaA[0]);

		echo "
		<tr>
			<td> ".ucfirst($datos['usuario'])." </td>
        	<td> $fecha </td>
        	<td> $hora </td>
        	<td> ".$datos['ip']." </td>
        	<td> ".$datos['navegador']." </td>
        	<td> ".$datos['sistema']." </td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

//Función que elimina los segundos de una hora extraída de la BDD
function formateaHoraWeb($hora){
	return substr_replace($hora, '', strlen($hora)-3, strlen($hora));
}

//Función que añade un espacio cada 3 dígitos de un teléfono (de longitud 9)
function formateaTelefono($telefono){
	$res="";
	if(strlen($telefono)==9){
		$res=substr($telefono,0,3).' '.substr($telefono,3,3).' '.substr($telefono,6,3);
	}
	else{
		$res=$telefono;
	}

	return $res;
}

//Cada herramienta tendrá sus propios perfiles a definir en el array $perfil
function perfilUsuario(){
	$enlace='';
	$perfil=array('ADMIN'=>'Administrador','COMERCIAL'=>'Comercial','FORMACION'=>'Técnico formación','CONSULTORIA'=>'Técnico consultoría','ADMINISTRACION'=>'Administración','ATENCION'=>'Atención al Cliente','TELECONCERTADOR'=>'Teleconcertador','MARKETING'=>'Marketing','SUPERVISOR'=>'Supervisor');//PARA CRM
	//$perfil=array('ADMIN'=>'Administrador','CLIENTE'=>'Cliente','ENTRENADOR'=>'Entrenador');//PARA ONPLAN
	//$perfil=array('ADMIN'=>'Administrador','USUARIO'=>'Usuario');
	if($_SESSION['tipoUsuario']=='ADMIN'){
		$enlace='<li><a href="usuarios.php">';
		//$enlace='<li><a href="javascript:void">';
	}
	else{
		$enlace='<li><a href="javascript:void">';
	}
	$enlace.='<i class="icon-wrench"></i> Perfil: '.$perfil[$_SESSION['tipoUsuario']].'</a></li>';
	echo $enlace;
}


function compruebaPerfilParaWhere($campo=false){
	$where='';
	if($_SESSION['tipoUsuario']=='ADMIN'||$_SESSION['tipoUsuario']=='ADMINISTRACION'||$_SESSION['tipoUsuario']=='ATENCION'||$_SESSION['codigoS']=='57'){
		$where="WHERE 1=1";//Condición irrelevante, pero necesaria para encajar bien la función en las consultas que tengan más de una condición
	}
	else{
		$codigoU=$_SESSION['codigoS'];
		if($campo!=false){
			$where="WHERE ($campo='$codigoU' OR $campo IN (SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU'))";
		}
		else{
			$where="WHERE (codigoUsuario='$codigoU' OR codigoUsuario IN (SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU'))";
		}
	}
	return $where;
}


//INICIO DE OPERACIONES AUTOMÁTICAS CON LA BASE DE DATOS ////////////////////////////////////

//Función que devuelve los campos de una tabla (para usar en las funciones de inserción/actualización)
function camposTabla($tabla){
	$campos=array();
	$consulta=consultaBD("SHOW COLUMNS FROM $tabla");
	$campo=mysql_fetch_row($consulta);
	$campo=mysql_fetch_row($consulta);//Doble avance para no meter en el array de campos el código autoincremental de la tabla
	while($campo!=false){
		array_push($campos,$campo[0]);
		$campo=mysql_fetch_row($consulta);
	}
	return $campos;
}

//Inserción automática de los datos de un formulario en una tabla de la BDD
function insertaDatos($tabla,$nombreFichero='',$ruta='ficheros'){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	$campos=camposTabla($tabla);

	$consulta="INSERT INTO $tabla VALUES(NULL";
	foreach($campos as $campo){
		if(substr_count($campo,'check')==1 && !isset($datos[$campo])){//Comprueba si el campo es un check y si NO se ha marcado
			$datos[$campo]='NO';
		}
		elseif(substr_count($campo,'fichero')==1 && $_FILES[$campo]['name']==''){//Comprueba si el campo es un tipe="file" y no se ha subido ningún fichero
			$datos[$campo]='NO';
		}
		elseif(substr_count($campo,'fichero')==1){//Comprueba si el campo es un tipe="file" y se ha subido un fichero
			$datos[$campo]=subeDocumento($campo,$nombreFichero,$ruta);
		}

		if(!isset($datos[$campo])){
			$res=false;
		}
		elseif($datos[$campo]=='NULL'){//MODIFICACIÓN 23/07/2014 (para pasar a Jose Luis): evita que el valor NULL se inserte como una cadena
			$consulta.=", ".$datos[$campo];
		}
		else{
			$consulta.=",'".$datos[$campo]."'";
		}
	}
	$consulta.=");";
	if(isset($_SESSION["verifica"]) && $_SESSION["verifica"] == 1){   
		unset($_SESSION['verifica']);
		if(isset($_SESSION['sql']) && $_SESSION['sql']==$consulta){

		} else{
			$_SESSION['sql']=$consulta;
			$res=consultaBD($consulta);
			if($res){
				$res=mysql_insert_id();//Si la consulta se ha realizado correctamente devuelve el código de la inserción
			}
			else{
				echo mysql_error();
				echo $consulta;
			}
		}
	}else{
		$res=false;
	}

	cierraBD();

	return $res;
}


//Actualiza los datos de una tabla con los campos de un formulario
function actualizaDatos($tabla,$nombreFichero='',$ruta='ficheros',$where='codigo'){//MODIFICACIÓN 21/07/2014: Incorporación del pamámetro $where para establecer un WHERE
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	$campos=camposTabla($tabla);

	$consulta="UPDATE $tabla SET ";
	foreach($campos as $campo){
		if(substr_count($campo,'check')==1 && !isset($datos[$campo])){//Comprueba si el campo es un check y si NO se ha marcado
			$datos[$campo]='NO';
		}
		elseif(substr_count($campo,'fichero')==1 && $_FILES[$campo]['name']!=''){//MODIFICACIÓN 23/07/2014 (para pasar a Jose Luis): Antes estaba el bloque elseif comentado abajo. Esta modificación hace que los ficheros que no se hayan subido en la actualización no se sobreescriban. TODO: Verificar si en Tecnocertificación funciona correctamente.
			$datos[$campo]=subeDocumento($campo,$nombreFichero,$ruta);
		}
		/*elseif(substr_count($campo,'fichero')==1 && $_FILES[$campo]['name']==''){
			$datos[$campo]='NO';
		}
		elseif(substr_count($campo,'fichero')==1){
			$datos[$campo]=subeDocumento($campo,$nombreFichero,$ruta);
		}*/
		
		if(!isset($datos[$campo])){
			$res=false;
		}
		elseif($datos[$campo]=='NULL'){//MODIFICACIÓN 23/07/2014 (para pasar a Jose Luis): evita que el valor NULL se inserte como una cadena
			$consulta.="$campo=".$datos[$campo]." ,";
		}
		else{
			$consulta.="$campo='".$datos[$campo]."' ,";
		}
		
	}
	$consulta=substr_replace($consulta,'',strlen($consulta)-2,strlen($consulta));//Para quitar última coma
	if($where=='codigo'){
		$consulta.=" WHERE codigo='".$datos['codigo']."';";
	}
	else{
		$consulta.=' '.$where;
	}

	$res=consultaBD($consulta);

	cierraBD();
	return $res;
}



function eliminaDatos($tabla){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM $tabla WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
			echo mysql_error();
		}
	}
	cierraBD();

	return $res;
}

//FIN DE OPERACIONES AUTOMÁTICAS CON LA BASE DE DATOS ////////////////////////////////////


//Comprobación de resultados de operaciones para mostrar mensajes
function mensajeResultado($indice,$res,$campo,$eliminacion=false){
	if(isset($_POST[$indice])){
		if($res && $eliminacion){
			mensajeOk("Eliminación realizada correctamente."); 
		}
		elseif($res && !$eliminacion){
		  mensajeOk("Datos de $campo guardados correctamente."); 
		}
		else{
		  mensajeError("se ha producido un error al gestionar los datos. Compruebe la información introducida."); 
		}
	}
}


function campoTexto($nombreCampo,$texto,$valor='',$clase='input-large',$disabled=false){//MODIFICACIÓN 21/07/2014 DE OFICINA: Añadido parámetro $disabled para desactivar campo
	$des='';
	if($disabled){
		$des='disabled="disabled"';
	}
	$valor=compruebaValorCampo($valor,$nombreCampo);

	echo "
	<div class='control-group'>                     
      <label class='control-label' for='$nombreCampo'>$texto:</label>
      <div class='controls'>
        <input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' value='$valor' $des>
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}



function campoClave($nombreCampo,$texto,$valor='',$claseBoton='btn-primary',$clase='input-large',$disabled=false){
	$des='';
	if($disabled){
		$des='disabled="disabled"';
	}
	$valor=compruebaValorCampo($valor,$nombreCampo);

	echo "
	<div class='control-group'>                     
      <label class='control-label' for='$nombreCampo'>$texto:</label>
      <div class='controls'>
        <input type='password' class='$clase form-control' id='$nombreCampo' name='$nombreCampo' value='$valor' $des>
        <span class='input-group-btn'>
			<button class='btn $claseBoton' type='button' id='mostrarClave' estado='ver'><i class='icon-eye-open'></i></button>
        </span>
   	  </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}


/*function campoFecha($nombreCampo,$texto,$valor=false,$solo=false){
	if(!$valor){
		$valor=fecha();
	}
	else{
		$valor=compruebaValorCampo($valor,$nombreCampo);
		$valor=formateaFechaWeb($valor);
	}

	if($solo){
		campoTextoSolo($nombreCampo,$valor,'input-small datepicker hasDatepicker');
	}
	else{
		campoTexto($nombreCampo,$texto,$valor,'input-small datepicker hasDatepicker');
	}
}*/


function campoFecha($nombreCampo,$texto,$valor=false,$solo=false,$disabled=false){//Modificada por Jose Luis el 15/09/2014 (añadido opción disabled). Pendiente de revisión.
	if(!$valor){
		$valor=fecha();
	}
	else{
		$valor=compruebaValorCampo($valor,$nombreCampo);
		$valor=formateaFechaWeb($valor);
	}

	if($solo){
		campoTextoSolo($nombreCampo,$valor,'input-small datepicker hasDatepicker');
	}
	else{
		campoTexto($nombreCampo,$texto,$valor,'input-small datepicker hasDatepicker',$disabled);
	}
}


function campoFechaTabla($nombreCampo,$valor=false){
	echo "<td>";
	campoFecha($nombreCampo,'',$valor,true);
	echo "</td>";
}

function areaTexto($nombreCampo,$texto,$valor='',$clase='areaTexto',$disabled=''){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	$disabledArea='';
	if($disabled){
		$disabledArea='disabled="disabled"';
	}
	echo "
	<div class='control-group'>                     
      <label class='control-label' for='$nombreCampo'>$texto:</label>
      <div class='controls'>
        <textarea name='$nombreCampo' class='$clase' id='$nombreCampo' $disabledArea>$valor</textarea>
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}


function campoFichero($nombreCampo,$texto){
	echo "
	<div class='control-group'>                     
		<label class='control-label' for='$nombreCampo'>$texto:</label>
		<div class='controls'>
			<input type='file' name='$nombreCampo' id='$nombreCampo'><div class='tip'>Solo se admiten ficheros de 5 MB como máximo</div>
		</div> <!-- /controls -->       
	</div> <!-- /control-group -->";
}


function subeDocumento($campo,$nombreFichero='',$ruta='ficheros'){
	if($nombreFichero==''){
		$nombreFichero=$_FILES[$campo]['name'];
	}
	else{
		$extension=explode('.',$_FILES[$campo]['name']);
		$nombreFichero.='.'.end($extension);
	}

	if (!move_uploaded_file($_FILES[$campo]['tmp_name'], "$ruta/$nombreFichero")){ 
		 $nombreFichero='NO';
	}

	return $nombreFichero;
}


function datosRegistro($tabla,$codigo,$campo='codigo'){
	return consultaBD("SELECT * FROM $tabla WHERE $campo='$codigo';",true,true);
}


//Esta función permite pasarle a un campo el array $datos completo, sin tener que hacer $datos['campo']
function compruebaValorCampo($valor,$nombreCampo){
	if(is_array($valor)){
		$valor=$valor[$nombreCampo];
	}
	return $valor;
}

function campoOculto($valor,$nombreCampo='codigo',$clase='hide'){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	echo "<input type='hidden' class='$clase' name='$nombreCampo' id='$nombreCampo' value='$valor' />";
}


function estadisticasGenericas($tabla){
	return consultaBD("SELECT COUNT(codigo) AS total FROM $tabla;",true,true);
}


function campoSelectConsulta($nombreCampo,$texto,$consulta,$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$disabled='',$tipo=0){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	if($disabled){
		$disabled="disabled='disabled'";
	}

	if($tipo==0){
		echo "
		<div class='control-group'>                     
			<label class='control-label' for='$nombreCampo'>$texto:</label>
			<div class='controls'>";
	}
	elseif($tipo==1){
		echo "<td>";
	}

	echo "<select name='$nombreCampo' id='$nombreCampo' class='$clase' $busqueda $disabled>";
		
		$consulta=consultaBD($consulta,true);
		$datos=mysql_fetch_assoc($consulta);
		while($datos!=false){
			echo "<option value='".$datos['codigo']."'";

			if($valor!=false && $valor==$datos['codigo']){
				echo " selected='selected'";
			}

			echo ">".$datos['texto']."</option>";
			$datos=mysql_fetch_assoc($consulta);
		}
		
	echo "</select>";

	if($tipo==0){
		echo "
			</div> <!-- /controls -->       
		</div> <!-- /control-group -->";
	}
	elseif($tipo==1){
		echo "</td>";
	}
}

function campoSelect($nombreCampo,$texto,$nombres,$valores,$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$tipo=0){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	if($tipo==0){
		echo "
		<div class='control-group'>                     
			<label class='control-label' for='$nombreCampo'>$texto:</label>
			<div class='controls'>";
	}
	elseif($tipo==1){
		echo "<td>";
	}

	echo "<select name='$nombreCampo' id='$nombreCampo' class='$clase' $busqueda>";
		
	for($i=0;$i<count($nombres);$i++){
		echo "<option value='".$valores[$i]."'";

		if($valor!=false && $valor==$valores[$i]){
			echo " selected='selected'";
		}

		echo ">".$nombres[$i]."</option>";
	}
		
	echo "</select>";

	if($tipo==0){
		echo "
			</div> <!-- /controls -->       
		</div> <!-- /control-group -->";
	}
	elseif($tipo==1){
		echo "</td>";
	}
}


function campoRadio($nombreCampo,$texto,$valor='NO', $textosCampos=array('Si','No'),$valoresCampos=array('SI','NO')){//MODIFICACIÓN 21/07/2014 DE OFICINA: $valor antes que textos. $campos, $textosCampos y $valoresCampos arrays con misma longitud
	$valor=compruebaValorCampo($valor,$nombreCampo);

	echo "
	<div class='control-group'>                     
      <label class='control-label'>$texto:</label>
      <div class='controls'>";

    for($i=0;$i<count($textosCampos);$i++){
    	echo "<label class='radio inline'>
    			<input type='radio' name='$nombreCampo' id='$nombreCampo' value='".$valoresCampos[$i]."'";
    	if($valoresCampos[$i]==$valor){
    		echo " checked='checked'";
    	} 
    	echo ">".$textosCampos[$i]."</label>";
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}



function campoCheck($nombreCampo,$texto, $textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false,$valor=array()){
	$nombre=$nombreCampo;

	echo "
	<div class='control-group'>                     
      <label class='control-label'>$texto:</label>
      <div class='controls'>";

    for($i=0;$i<count($textosCampos);$i++){
    	if(!is_array($nombreCampo)){
    		$nombre=$nombreCampo.$i;
    	}

    	echo "<label class='checkbox inline'>
    			<input type='checkbox' name='$nombre' value='".$valoresCampos[$i]."'";
    	if(isset($valor[$i]) && $valoresCampos[$i]==$valor[$i]){//Uso el isset porque si no se pasan valores (por ejemplo, en un alta), el array nunca tendrá el indice $valor[$i]
    		echo " checked='checked'";
    	} 
    	echo ">".$textosCampos[$i]."</label>";
    	if($salto){
    		echo "<br />";
    	}
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}


function obtieneArrayCamposCheck($nombreCampo,$datos){//Para usar esta función, los campos check de la BDD deben llamarse igual y tener un índice que empiece por 0 (p.j. opcion0, opcion1, ...)
	$res=array();
	for($i=0;isset($datos[$nombreCampo.$i]);$i++){
		array_push($res,$datos[$nombreCampo.$i]);
	}
	return $res;
}


function campoTextoSolo($nombreCampo,$valor='',$clase='input-large'){
	echo "<input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' value='$valor'>";
}


function campoTextoTabla($nombreCampo,$valor='',$clase='input-large'){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	echo "<td>";
	campoTextoSolo($nombreCampo,$valor,$clase);
	echo "</td>";
}

function campoCheckSolo($nombreCampo,$valor='',$valorCampo='SI'){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	echo "<input type='checkbox' name='$nombreCampo' value='$valorCampo'";
	if($valor=='SI'){
		echo " checked='checked'";
	}
	echo "/>";
}


function campoTextoSimbolo($nombreCampo,$texto,$simbolo,$valor='',$clase='input-mini pagination-right',$disabled=false){
	$valor=compruebaValorCampo($valor,$nombreCampo);

	$habilitado='';
	if($disabled){
		$habilitado="disabled='disabled'";
	}
	
	echo "
	<div class='control-group'>                     
      <label class='control-label' for='$nombreCampo'>$texto:</label>
      <div class='controls'>
        <div class='input-prepend input-append'>
          <input type='text' class='input-mini pagination-right' id='$nombreCampo' name='$nombreCampo' value='$valor' $habilitado>
          <span class='add-on'> $simbolo</span>
        </div>
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}


function campoTextoSoloSimbolo($nombreCampo,$simbolo,$valor='',$clase='input-mini pagination-right'){
	$valor=compruebaValorCampo($valor,$nombreCampo);

	echo "<div class='input-prepend input-append'>
          <input type='text' class='input-mini pagination-right' id='$nombreCampo' name='$nombreCampo' value='$valor'>
          <span class='add-on'> $simbolo</span>
        </div>";
}


function campoImagen($nombreCampo,$texto,$ruta,$valor){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	if($valor=='NO'){
		//campoFichero($nombreCampo,$texto);
		echo "
		<div class='control-group'>                     
	      <label class='control-label' for='$nombreCampo'>$texto:</label>
	      <div class='controls datoSinInput'>
	        Sin foto
	      </div> <!-- /controls -->       
	    </div> <!-- /control-group -->";
	}
	else{
		echo "
		<div class='control-group'>                     
	      <label class='control-label' for='$nombreCampo'>$texto:</label>
	      <div class='controls'>
	        <img src='$ruta$valor' width='200' />
	      </div> <!-- /controls -->       
	    </div> <!-- /control-group -->";
	}
}


function campoNumero($nombreCampo,$texto,$valor='0',$clase='input-mini pagination-right'){
	$valor=compruebaValorCampo($valor,$nombreCampo);

	echo "
	<div class='control-group'>                     
      <label class='control-label' for='$nombreCampo'>$texto:</label>
      <div class='controls'>
      	<input type='number' name='$nombreCampo' id='$nombreCampo' class='$clase' value='$valor' />
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}



function campoDescarga($nombreCampo,$texto,$ruta,$valor){//NUEVO 23/07/2014 (para pasar a Jose Luis)
	$valor=compruebaValorCampo($valor,$nombreCampo);
	if($valor=='NO'){
		echo "
		<div class='control-group'>                     
	      <label class='control-label' for='$nombreCampo'>$texto:</label>
	      <div class='controls datoSinInput'>
	        Sin documento
	      </div> <!-- /controls -->       
	    </div> <!-- /control-group -->";
	}
	else{
		echo "
		<div class='control-group'>                     
	      <label class='control-label'>$texto:</label>
	      <div class='controls'>
	        <a class='btn btn-inverse' href='$ruta$valor' target='_blank'><i class='icon-download'></i> $valor</a>
	      </div> <!-- /controls -->       
	    </div> <!-- /control-group -->";
	}
}


function campoDato($texto,$valor){//Nuevo 23/07/2014 (para pasar a Jose Luis)
	echo "
	<div class='control-group'>                     
      <label class='control-label'>$texto:</label>
      <div class='controls datoSinInput'>
      	$valor
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}


function campoFechaHoraTabla($nombreCampoFecha,$nombreCampoHora,$valorF=false,$valorH=false){
	$valorF=compruebaValorCampo($valorF,$nombreCampoFecha);
	$valorH=compruebaValorCampo($valorH,$nombreCampoHora);
	if(!$valorH){
		$valorH=hora();
	}
	echo "<td>";
		campoFecha($nombreCampoFecha,'',$valorF,true);
	echo " ";
		campoTextoSolo($nombreCampoHora,formateaHoraWeb($valorH),'input-mini');
	echo "</td>";
}


function campoCalcula($nombreCampo,$texto,$valor='',$simbolo='€', $claseBoton='btn-primary',$clase='input-large',$disabled=false){
	$des='';
	if($disabled){
		$des='disabled="disabled"';
	}
	$valor=compruebaValorCampo($valor,$nombreCampo);

	echo "
	<div class='control-group'>                     
      <label class='control-label' for='$nombreCampo'>$texto:</label>
      <div class='controls'>
        <div class='input-prepend input-append'>
          <input type='text' class='input-mini pagination-right' id='$nombreCampo' name='$nombreCampo' value='$valor'>
          <span class='add-on'> $simbolo</span>
        </div>
        <span class='input-group-btn'>
			<button class='btn $claseBoton' type='button' id='calcular' title='Calcular'><i class='icon-cogs'></i></button>
        </span>
   	  </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function campoHora($nombreCampo,$texto,$valor=false,$solo=false){
	
	if($valor==false){
		$valor=hora();
	}

	if($solo){
		echo "<input type='text' class='input-mini timepicker' id='$nombreCampo' name='$nombreCampo' data-template='modal' data-minute-step='1' value='$valor'>";
	}
	else{
		echo "
			<div class='control-group'>                     
			  <label class='control-label' for='$nombreCampo'>$texto:</label>
			  <div class='controls'>
				<input type='text' class='input-mini timepicker' id='$nombreCampo' name='$nombreCampo' data-minute-step='1' value='$valor'>
			  </div> <!-- /controls -->       
			</div> <!-- /control-group -->";
	}
}

/*
	AÑADIDO EL 08/01/2015
	Función que sustituye a campoOculto en los formularios en los que se trabaje con valores pero éstos no se envíen, o que solo sea 
	necesario enviar un campoOculto de entre varios, evitando así que se superen el número máximo de campos que carga PHP por defecto
	en $_POST (1.000 campos).
*/
function divOculto($valor,$nombreCampo='codigo'){
	echo "<div class='hide' id='$nombreCampo'>$valor</div>";
}

function formateaNumero($dato,$bd=false){
	if($bd){
		$parts=explode(',', $dato);

		if(isset($parts[1])){
			$dato = $parts[0].".".$parts[1];
		}

		$res = round($dato,2);
	}
	else{
		$parts=explode('.', $dato);
		if(isset($parts[1])){
			$res = $parts[0].",".$parts[1];
		}else{
			$res = $parts[0];
		}
	}
	return $res;
}

// =========================================================== Fin funciones comunes ====================================================================================