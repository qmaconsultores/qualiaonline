<?php
  $seccionActiva=14;
  include_once("cabecera.php");
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-euro"></i><i class="icon-chevron-right"></i><i class="icon-download"></i>
              <h3>Descarga de Excel de ventas / facturas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
				<form action='generaExcelComisiones.php' method='post'>
					<div class='control-group'>                     
					  <div class='controls'>
						Firma: 
						<?php 
							echo "<select name='firma' id='firma' class='span3 show-tick' data-live-search='true'>";
		
								$consulta=consultaBD("SELECT codigo, firma AS texto FROM firmas ORDER BY codigo;",true);
								$datos=mysql_fetch_assoc($consulta);
								while($datos!=false){
									echo "<option value='".$datos['codigo']."'";

									echo ">".$datos['texto']."</option>";
									$datos=mysql_fetch_assoc($consulta);
								}
								
							echo "</select>";
						?>
					  </div> <!-- /controls -->       
					</div> <!-- /control-group -->
					<div class='control-group'>                     
					  <div class='controls'>
						Emisión del: <input type='text' class='input-small datepicker hasDatepicker' id='fechaUno' name='fechaUno' value='<?php echo imprimeFecha(); ?>'> 
						Al: <input type='text' class='input-small datepicker hasDatepicker' id='fechaDos' name='fechaDos' value='<?php echo imprimeFecha(); ?>'>
					  </div> <!-- /controls -->       
					</div> <!-- /control-group -->
					
					<button type="submit" class="btn btn-primary">Descargar <i class="icon-download"></i></button>
				</form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();

  });
</script>