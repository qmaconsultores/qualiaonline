<?php
  $seccionActiva=19;
  include_once('cabecera.php');
  
   if(isset($_POST['fechaUno'])){
	$_SESSION['fechaUnoVentas']=$_POST['fechaUno'];
	$_SESSION['fechaDosVentas']=$_POST['fechaDos'];
  }
  $fechaUno=formateaFechaBD($_SESSION['fechaUnoVentas']);
  $fechaDos=formateaFechaBD($_SESSION['fechaDosVentas']);
  
  if(isset($_POST['codigo'])){
    $res=actualizaVenta();
  }
  elseif(isset($_POST['completaAlumnos'])){
    $res=completaAlumnos();
  }
  elseif(isset($_POST['codigoC'])){
    $res=registraVenta();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaVenta();
  }
  
  $and='';
  if($_POST['comercial']!='todos'){
	$and="AND ventas.codigoUsuario='".$_POST['comercial']."'";
  }
  if(isset($_POST['servicio']) && $_POST['servicio']!='todos'){
	$and.=" AND ventas.concepto='".$_POST['servicio']."'";
  }
  
  $estadisticas=creaEstadisticasVentasGeneral("AND fecha>='$fechaUno' AND fecha<='$fechaDos' $and");
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de ventas realizadas</h6>

                   <div id="big_stats" class="cf">

                    <div class="stat"> <i class="icon-shopping-cart"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Ventas realizadas</div>
                    <!-- .stat -->

                    <div class="stat"> <i class="icon-eur"></i> <span class="value"><?php echo $estadisticas['preciototal']?></span> <br>Total ventas</div>

                    
					<?php if($_SESSION['tipoUsuario']!='ADMINISTRACION'){ ?>

            <div class="stat"> <i class="icon-eur"></i> <span class="value"><?php echo $estadisticas['precioNueva']?></span> <br>Total Vtas Nuevas</div>
            <!-- .stat -->
            <div class="stat"> <i class="icon-eur"></i> <span class="value"><?php echo $estadisticas['precioCartera']?></span> <br>Total cartera</div>

					<?php } ?>

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6" style='float:right;'>
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de ventas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content prueba">
              <div class="shortcuts">
				        <a href="ventasGeneral.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>	


      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Datos de la venta actualizados."); 
            }
            else{
              mensajeError("no se ha podido actualizar la venta. Compruebe los datos introducidos."); 
            }
          }
		  elseif(isset($_POST['completaAlumnos'])){
            if($res){
              mensajeOk("Venta registrada y alumnos completados."); 
            }
            else{
              mensajeError("no se ha podido completar los datos de los alumnos. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['codigoC'])){
            if($res){
              mensajeOk("Venta registrada correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar la venta. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar la venta. Contacte con el webmaster."); 
            }
          }
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Ventas realizadas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			   <table class='table table-striped table-bordered datatable'>
                <thead>
                  <tr>
                    <th> ID </th>
					<th> Empresa </th>
                    <th> Tipo de venta </th>
					<th> Comercial </th>
                    <th> Concepto </th>
					<th> Fecha </th>
                    <th> Importe </th>
                    <th> Observaciones </th>
					<th> Cartera / Nueva </th>
                    <th class='centro'></th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>
				
				<?php imprimeVentasGeneral("AND fecha>='$fechaUno' AND fecha<='$fechaDos' $and"); ?>
				
				</tbody>
              </table>
                
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
$('#eliminar').click(function(){
  var valoresChecks=recorreChecks();
  if(valoresChecks['codigo0']==undefined){
    alert('Por favor, seleccione antes una venta.');
  }
  else{
    valoresChecks['elimina']='SI';
    creaFormulario('ventasGeneralFiltrado.php',valoresChecks,'post');
  }

});
</script>
