<?php
  $seccionActiva=5;
  include_once("cabecera.php");
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-file-text"></i>
              <h3>Archivo de Cursos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              <h3>Seleccione el año:</h3><br><br>
				<form action='formacionArchivo.php' method='post'>
					<div class='control-group'>                     
					  <div class='controls'>
						<input type='text' class='input-small' id='anio' name='anio' value='<?php echo date('Y'); ?>'> 
					  </div> <!-- /controls -->       
					</div> <!-- /control-group -->
					<br>
					
					<button type="submit" class="btn btn-primary">Seleccionar <i class="icon-circle-arrow-right"></i></button>
				</form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>