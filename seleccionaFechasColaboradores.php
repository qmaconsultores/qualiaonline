<?php
  $seccionActiva=14;
  include_once("cabecera.php");
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-euro"></i><i class="icon-chevron-right"></i><i class="icon-download-alt"></i>
              <h3>Descarga de excel de colaboradores</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
				<form action='generaResumenColaboradores.php' method='post'>
					<div class='control-group'>                     
					  <div class='controls'>
						Fecha del: <input type='text' class='input-small datepicker hasDatepicker' id='fechaUno' name='fechaUno' value='<?php echo imprimeFecha(); ?>'> 
						Al: <input type='text' class='input-small datepicker hasDatepicker' id='fechaDos' name='fechaDos' value='<?php echo imprimeFecha(); ?>'>
					  </div> <!-- /controls -->       
					</div> <!-- /control-group -->
					
					<button type="submit" class="btn btn-primary">Descargar <i class="icon-download-alt"></i></button>
				</form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();

  });
</script>