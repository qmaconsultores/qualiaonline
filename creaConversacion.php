<?php
  $seccionActiva=20;
  include_once("cabecera.php");
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-comments-o"></i><i class="icon-chevron-right"></i><i class="icon-plus-circle"></i>
              <h3>Nueva Conversación</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="comunicacionInterna.php" method="post" enctype="multipart/form-data">
                  <fieldset>

                    <?php
                      campoDestinatarios();

                      campoTexto('asunto','Asunto','','span4');
                      areaTexto('mensaje','Mensaje');


                      campoFichero('ficheroAdjunto','Adjunto');
                      campoOculto($_SESSION['codigoS'],'codigoUsuario');
                      campoOculto('NO','leido');
                    ?>


                    <br />                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-mail-forward"></i> Enviar Mensaje</button> 
                      <a href="comunicacionInterna.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script src="js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript" src="js/bootstrap-filestyle.js"></script>


<script type="text/javascript">
  $(document).ready(function(){
    $('select').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    $('#mensaje').wysihtml5({locale: "es-ES"});
    $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

    $('input[name=destinatarios0]').click(function(){
      if($(this).attr('checked')){
        $('#cajaDestinatarios input:checkbox').prop('checked',true);
      }
      else{
        $('#cajaDestinatarios input:checkbox').prop('checked',false);
      }
    });

  });
</script>