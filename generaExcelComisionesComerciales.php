<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos/plantillaComisionesComerciales.xlsx");
	
	//Estilos de bordes
	$todos = array(
	  'borders' => array(
		'allborders' => array(
		  'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	  )
	);
	
	$exterior = array(
	  'borders' => array(
		'outline' => array(
		  'style' => PHPExcel_Style_Border::BORDER_THICK
		)
	  )
	);
	
	conexionBD();
	$tiempo=time();
	$j=10;
	$totalComisionado=0;
	$totalFacturado=0;
	
	//COMIENZO DE TODOS LAS FACTURAS COMPRENDIDAS
	$datosComercial=datosRegistro('usuarios',$_GET['comercial']);
	$objPHPExcel->getActiveSheet()->getCell('A4')->setValue($datosComercial['nombre'].' '.$datosComercial['apellidos']);
	$fechaUno=$_GET['fechaUno'];
	$fechaPartida=explode('-',$fechaUno);
	if($fechaPartida[2]=='24'){
		$quincena='1º';
	}else{
		$quincena='2º';
		$fechaPartida[1]=intval($fechaPartida[1]);
		if($fechaPartida[1]==1){
			$fechaPartida[1]='12';
			$fechaPartida[0]--;
		} else {
				$fechaPartida[1]=$fechaPartida[1]-1;
				if($fechaPartida[1]<10){
					$fechaPartida[1]='0'.$fechaPartida[1];
				}
		}
	}
	$anio=$fechaPartida[0];
	$mes=array('01'=>'ENERO','02'=>'FEBRERO','03'=>'MARZO','04'=>'ABRIL','05'=>'MAYO','06'=>'JUNIO','07'=>'JULIO','08'=>'AGOSTO','09'=>'SEPTIEMBRE','10'=>'OCTUBRE','11'=>'NOVIEMBRE','12'=>'DICIEMBRE');
	$fechaPartida[1]=strlen($fechaPartida[1])==1 ? '0'.$fechaPartida[1]:$fechaPartida[1];
	$objPHPExcel->getActiveSheet()->getCell('A6')->setValue('RELACION DE CLIENTES -'.$quincena.' '.$mes[$fechaPartida[1]].' '.$anio);
	conexionBD();
	
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.fechaEmision, facturacion.cobrada, facturacion.fechaVencimiento, facturacion.referencia, facturacion.coste, clientes.empresa, facturacion.enviada, facturacion.concepto, facturacion.enviadaCliente, clientes.codigo AS codigoCliente, insercion, facturacion.firma, facturacion.devuelta, facturacion.tipoFacturaComercial 
		FROM facturacion LEFT JOIN clientes ON clientes.codigo=facturacion.codigoCliente INNER JOIN facturas_vto ON facturacion.codigo=facturas_vto.codigoFactura WHERE facturas_vto.fecha>='".$_GET['fechaUno']."' AND facturas_vto.fecha<='".$_GET['fechaDos']."' AND facturas_vto.cobrada='SI' AND codigoVenta IN(SELECT codigo FROM ventas WHERE codigoUsuario='".$_GET['comercial']."') ORDER BY insercion DESC;");
		$datos=mysql_fetch_assoc($consulta);
		
	$tipoVenta=array('NUEVA'=>'porcentajeNueva','CARTERA'=>'porcentajeCartera','TELEVENTA'=>'porcentajeTeleventa','AUDITORIA'=>'porcentajeAuditoria');
	while(isset($datos['codigo'])){		
		$objPHPExcel->getActiveSheet()->getCell('A'.$j)->setValue($datos['empresa']);
		$objPHPExcel->getActiveSheet()->getCell('B'.$j)->setValue(number_format((float)$datos['coste'], 2, ',', '').' €');
		$objPHPExcel->getActiveSheet()->getCell('C'.$j)->setValue(number_format((float) $datos['coste']*($datosComercial[$tipoVenta[$datos['tipoFacturaComercial']]]/100), 2, ',', '').' €');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':C'.$j)->applyFromArray($todos);
		$totalComisionado+=$datos['coste']*($datosComercial[$tipoVenta[$datos['tipoFacturaComercial']]]/100);
		$totalFacturado+=$datos['coste'];
		$datos=mysql_fetch_assoc($consulta);
		$j++;
	}
	cierraBD();
	
	$j+=5;
	$objPHPExcel->getActiveSheet()->getCell('A'.$j)->setValue('TOTAL FACTURADO:');
	$objPHPExcel->getActiveSheet()->getCell('B'.$j)->setValue(number_format((float)$totalFacturado, 2, ',', '').' €');
	$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':B'.$j)->applyFromArray($todos);
	$j+=2;
	$objPHPExcel->getActiveSheet()->getCell('A'.$j)->setValue('TOTAL COMISIONES:');
	$objPHPExcel->getActiveSheet()->getCell('C'.$j)->setValue(number_format((float)$totalComisionado, 2, ',', '').' €');
	$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':C'.$j)->applyFromArray($exterior);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$j.':C'.$j)->getFont()->setBold(true);

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	
	$objWriter->save('documentos/comisiones.xlsx');

	/*
	// Definir headers
	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=Facturacion-".$tiempo.".zip");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/Facturacion-'.$tiempo.'.zip');*/

	
	
	// Definir headers
	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=comisiones.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/comisiones.xlsx');
?>