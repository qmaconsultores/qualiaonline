<?php
  session_start();
  include_once("funciones.php");
  compruebaSesion();

  $codigoS=$_SESSION['codigoS'];
  $usuario=$_SESSION['usuario'];

  $titulos=array('Inicio','Posibles Clientes','Cuentas','Tareas','Incidencias','Formación','Alumnos','Administración','Usuarios','Correos','Objetivos','Servicios','Ofertas','Consultoría','Facturación','Satisfacción de Clientes','Software','Colaboradores','Incidencias software','Ventas','Comunicación Interna','Documentación','Gestorías','Preventas','Posibles Clientes Propios');//23
?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<title><?php echo $titulos[$seccionActiva]; ?> - GRUPQUALIA</title>
<meta name="viewport" content="width=device-width, initial-scale=0.4, maximum-scale=1.0, user-scalable=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/pages/dashboard.css" rel="stylesheet">
<link href="css/bootstrap-timepicker.min.css" rel="stylesheet">

<link href="css/bootstrap-colorpicker.css" rel="stylesheet">

<link href="css/datepicker.css" rel="stylesheet">
<link href="css/bootstrap-select.css" rel="stylesheet">
<link href="css/bootstrap-wysihtml5.css" rel="stylesheet">
<link href="css/bootstrap-timepicker.min.css" rel="stylesheet">
<meta name="google-translate-customization" content="997ec15ac60d9433-8b2b4c82cae77d8a-g5b72f59f1a187377-1f"></meta>

<?php
if($seccionActiva==0){
  echo '<link href="js/guidely/guidely.css" rel="stylesheet">';
}
?>

<!-- Script HTML5, para el soporte del mismo en IE6-8 -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> 
      
      <span class="logo2">
        <img src="img/logo2.png" alt="GRUPQUALIA"> &nbsp; Software para la administración de la relación con los clientes (CRM)
      </span>

      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown"  id="target-5"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <?php echo ucfirst($usuario); ?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
      			  <li><a href="inicio.php"><i class="icon-home"></i>Inicio </a></li>
      			  <li class="divider"></li>
              <?php perfilUsuario(); ?>
              <li class="divider"></li>
              <li><a href="cerrarSesion.php"><i class="icon-off"></i> Cerrar sesión</a></li>
            </ul>
          </li>
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
	  <?php
		if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
	  ?>
		
		<li <?php if($seccionActiva==1 || $seccionActiva==0){ echo 'class="active"';} ?>><a href="posiblesClientes.php"><i class="icon-shopping-cart"></i><span>Posibles Clientes</span> </a></li>
		<li <?php if($seccionActiva==2){ echo 'class="active"';} ?>><a href="cuentas.php"><i class="icon-ok-circle"></i><span>Cuentas</span> </a></li>
		<li <?php if($seccionActiva==3){ echo 'class="active"';} ?>><a href="tareas.php"><i class="icon-calendar"></i><span>Tareas</span> </a></li>
		<li <?php if($seccionActiva==17){ echo 'class="active"';} ?>><a href="colaboradores.php"><i class="icon-group"></i><span>Colaboradores</span> </a></li>
		<li <?php if($seccionActiva==7){ echo 'class="active"';} ?>><a href="informesTelemarketing.php"><i class="icon-bar-chart"></i><span>Control Telemarketing</span> </a></li>
		<li class="dropdown <?php if($seccionActiva==4||$seccionActiva==18){ echo "active";} ?>"><a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-exclamation-sign"></i><span>Incidencias <?php if($_SESSION['numIncidencias']>0){ echo "<span class='badge' id='aviso'>".$_SESSION['numIncidencias']."</span>";}else{ echo "<span class='' id='aviso'></span>";} ?></span><b class="caret"></b></a>
		  <ul class="dropdown-menu">
			<li><a href="incidencias.php"><i class="icon-exclamation-sign"></i><span>Incidencias</span> </a></li>
			<li class="divider"></li>
			<li><a href="incidenciasSoftware.php"><i class="icon-laptop"></i><span>Incidencias software</span> </a></li>
		  </ul>
		</li>  
	  <?php }elseif($_SESSION['tipoUsuario']=='SUPERVISOR'){ ?>
		<li <?php if($seccionActiva==19){ echo 'class="active"';} ?> ><a href="ventasSupervisor.php"><i class="icon-tags"></i><span>Ventas</span> </a> </li> 
	  <?php
	  }else{
		$datosUsuario=datosRegistro('usuarios',$_SESSION['codigoS']);
		$fecha = date('Y-m-d');
		$nuevafecha = strtotime ( '+3 day' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
		$fecha=$nuevafecha;
		/*$contador=consultaBD("SELECT COUNT(codigo) AS total FROM tareas WHERE codigoUsuario='".$_SESSION['codigoS']."' AND (prioridad='proceso' OR prioridad='reconcertar' OR prioridad='normal') AND fechaFin<'$fecha' AND codigoCliente IS NOT NULL;",true,true);
		if($_SESSION['tipoUsuario']=='COMERCIAL' && $contador['total']>0 && $datosUsuario['director']=='NO'){
	  ?>
		<script type='text/javascript'>
			alert('Debes cerrar todas las tareas atrasadas para seguir utilizando el software');
		</script>
		<li <?php if($seccionActiva==3){ echo 'class="active"';} ?>><a href="tareas.php"><i class="icon-calendar"></i><span>Tareas</span> </a></li-->
	  <?php				
		}else{*/
		  if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ATENCION' || $_SESSION['tipoUsuario']=='COMERCIAL'||$_SESSION['tipoUsuario']=='ADMINISTRACION'||$_SESSION['tipoUsuario']=='CONSULTORIA'||$_SESSION['tipoUsuario']=='FORMACION'||$_SESSION['tipoUsuario']=='MARKETING'){
		  ?>
				<li class="dropdown <?php if($seccionActiva==1||$seccionActiva==2||$seccionActiva==10||$seccionActiva==11||$seccionActiva==12||($seccionActiva==17 && $_SESSION['tipoUsuario']!='ADMIN')||$seccionActiva==22||$seccionActiva==23||$seccionActiva==24){ echo "active";} ?>"><a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-shopping-cart"></i><span>Comercial</span> <b class="caret"></b></a>
				  <ul class="dropdown-menu">
				  		<?php
				  		if($_SESSION['tipoUsuario']!='CONSULTORIA'){
				  			?>
						<li><a href="posiblesClientes.php"><i class="icon-shopping-cart"></i><span>Posibles Clientes Qualia</span> </a></li>
						<li class="divider"></li>
						<li><a href="posiblesClientesPropios.php"><i class="icon-shopping-cart"></i><span>Posibles Clientes Propios</span> </a></li>
						<li class="divider"></li>
						<?php
				  		}
				  			?>
						<li><a href="cuentas.php"><i class="icon-ok-circle"></i><span>Cuentas</span> </a> </li>	
						<?php if($_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='COMERCIAL' || $_SESSION['codigoS']=='120' || $_SESSION['codigoS']=='172'){ ?>
						<li class="divider"></li>	
						<li><a href="preventas.php"><i class="icon-tags"></i><span>Preventas</span> </a> </li>
						<?php } if($_SESSION['tipoUsuario']!='COMERCIAL'){ ?>	
							<li class="divider"></li>	
							<li><a href="clientesBaja.php"><i class="icon-exclamation"></i><span>Clientes dados de baja</span> </a> </li>	
						<?php }if($_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='FORMACION'){ ?>
							<li class="divider"></li>
							<li><a href="colaboradores.php"><i class="icon-group"></i><span>Colaboradores</span> </a></li>
						<?php }if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ATENCION'){ ?>
							<li class="divider"></li>
							<li><a href="objetivos.php"><i class="icon-flag"></i><span>Objetivos</span> </a></li>
							<li class="divider"></li>              
							<li><a href="productos.php"><i class="icon-barcode"></i><span>Servicios</span> </a> </li> 	
						<?php } if($_SESSION['tipoUsuario']!='COMERCIAL'){ ?>	
							<li class="divider"></li> 
							<li><a href="gestorias.php"><i class="icon-briefcase"></i><span>Gestorías no colaboradoras</span> </a> </li>
						<?php } ?>
				  </ul>
				</li>
		  <?php if($_SESSION['tipoUsuario']!='MARKETING'){ ?>
				<li <?php if($seccionActiva==19){ echo 'class="active"';} ?>><a href="ventasGeneral.php"><i class="icon-tags"></i><span>Ventas</span> </a></li>
		  <?php
			}
		  } if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ATENCION' || $_SESSION['tipoUsuario']=='CONSULTORIA' || $_SESSION['tipoUsuario']=='FORMACION'||$_SESSION['tipoUsuario']=='MARKETING'){
		  ?>
				<li <?php if($seccionActiva==13){ echo 'class="active"';} ?>><a href="trabajos.php"><i class="icon-cogs"></i><span>Consultoría</span> </a></li>	
		  <?php } 
		  if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='ATENCION' || $_SESSION['tipoUsuario']=='FORMACION'||$_SESSION['tipoUsuario']=='MARKETING' || $_SESSION['tipoUsuario']=='CONSULTORIA'){
		  ?>
			<li <?php if($seccionActiva==5){ echo 'class="active"';} ?>><a href="formacion.php"><i class="icon-pencil"></i><span>Formación</span> </a></li> 
		  <?php
		  }
		  ?>        
			  <li <?php if($seccionActiva==3){ echo 'class="active"';} ?>><a href="tareas.php"><i class="icon-calendar"></i><span>Tareas</span> </a></li>
			  <li class="dropdown <?php if($seccionActiva==4||$seccionActiva==18){ echo "active";} ?>"><a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-exclamation-sign"></i><span>Incidencias <?php if($_SESSION['numIncidencias']>0){ echo "<span class='badge' id='aviso'>".$_SESSION['numIncidencias']."</span>";}else{ echo "<span class='' id='aviso'></span>";} ?></span><b class="caret"></b></a>
				  <ul class="dropdown-menu">
					<li><a href="incidencias.php"><i class="icon-exclamation-sign"></i><span>Incidencias</span> </a></li>
					<li class="divider"></li>
					<li><a href="incidenciasSoftware.php"><i class="icon-laptop"></i><span>Incidencias software</span> </a></li>
				  </ul>
			  </li>  
			  <li <?php if($seccionActiva==9){ echo 'class="active"';} ?>><a href="correos.php" id="target-4"><i class="icon-envelope"></i><span>Correos</span> </a></li>
		  <?php
		  if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TELECONCERTADOR' || $_SESSION['tipoUsuario']=='MARKETING' || ($_SESSION['tipoUsuario']=='COMERCIAL' && $datosUsuario['director']=='SI')){
		  ?>
			  <li class="dropdown <?php if($seccionActiva==7 || ($seccionActiva==17 && $_SESSION['tipoUsuario']=='ADMIN')){ echo "active";} ?>"><a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-paste"></i><span>Administración</span> <b class="caret"></b></a>
				  <ul class="dropdown-menu">
					<?php if($_SESSION['tipoUsuario']=='ADMIN'){ ?>
					<li><a href="informesFacturacion.php"><i class="icon-euro"></i><span>Informes de facturación</span> </a></li>
					<li class="divider"></li>
					<?php } ?>
					<li><a href="informesVentas.php"><i class="icon-tags"></i><span>Ventas por comercial</span> </a></li>
					<li class="divider"></li>
					<li><a href="informeVentasServicios.php"><i class="icon-tags"></i><span>Ventas por servicios</span> </a></li>
					<li class="divider"></li>
					<li><a href="informesTelemarketing.php"><i class="icon-bar-chart"></i><span>Control Telemarketing</span> </a></li>
					<?php if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='MARKETING' || $_SESSION['tipoUsuario']=='TELECONCERTADOR' || ($_SESSION['tipoUsuario']=='COMERCIAL' && $datosUsuario['director']=='SI')){ ?>
						<li class="divider"></li>
						<li><a href="colaboradores.php"><i class="icon-group"></i><span>Colaboradores</span> </a></li>
					<?php if($_SESSION['tipoUsuario']=='ADMIN'){ ?>
					<li class="divider"></li>
					<li><a href="informesConsultoria.php"><i class="icon-cogs"></i><span>Control Consultoría</span> </a></li>
					<li class="divider"></li>
					<li><a href="informeGestiones.php"><i class="icon-calendar"></i><span>Control de Tareas</span> </a></li>
					<li class="divider"></li>
					<li><a href="informesFormacion.php"><i class="icon-book"></i><span>Control Formación</span> </a></li>
					<li class="divider"></li>
					<li><a href="informesClientes.php"><i class="icon-user"></i><span>Informe Clientes</span> </a></li>
					<?php } 
					if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['codigoS']==172){
					?>
					<li class="divider"></li>
					<li><a href="informesVentaNeta.php"><i class="icon-money"></i><span>Venta neta</span> </a></li>
					<?php } 
					if($_SESSION['tipoUsuario']=='ADMIN'){ ?>
					<li class="divider"></li>
					<li><a href="informePreventas.php"><i class="icon-tags"></i><span>Preventas</span> </a></li>
					<li class="divider"></li>
					<li><a href="seleccionaColaborador.php"><i class="icon">%</i><span>Comisiones colaboradores</span> </a></li>
					<li class="divider"></li>
					<li><a href="seleccionaComercial.php"><i class="icon">%</i><span>Comisiones comerciales</span> </a></li>
					<li class="divider"></li>
					<li><a href="informeDevoluciones.php"><i class="icon-reply"></i><span>Devoluciones</span> </a></li>
					<li class="divider"></li>
					<li><a href="informeRemesas.php"><i class="icon-file"></i><span>Remesas</span> </a></li>
					<?php }
					}?>
				  </ul>
			  </li> 
		  <?php
		  }
		  if($_SESSION['tipoUsuario']=='ADMINISTRACION'){
		  ?>
		  <li class="dropdown <?php if($seccionActiva==7 || ($seccionActiva==17 && $_SESSION['tipoUsuario']=='ADMIN')){ echo "active";} ?>"><a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-paste"></i><span>Administración</span> <b class="caret"></b></a>
				  <ul class="dropdown-menu">
					<li><a href="informesVentaNeta.php"><i class="icon-money"></i><span>Venta neta</span> </a></li>
				  </ul>
			  </li> 
		  <?php
		  }
		  if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ADMINISTRACION'){
		  ?>
			  <li <?php if($seccionActiva==14){ echo 'class="active"';} ?> ><a href="facturacion.php"><i class="icon-euro"></i><span>Facturación</span> </a> </li> 
		  <?php
		  }
		  if($_SESSION['tipoUsuario']=='ADMIN'||$_SESSION['tipoUsuario']=='MARKETING'){
		  ?>
			  <li <?php if($seccionActiva==15){ echo 'class="active"';} ?>><a href="satisfaccion.php"><i class="icon-star"></i><span>Satisfacción</span> </a></li>
		  <?php
		  }if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ATENCION' || $_SESSION['tipoUsuario']=='MARKETING'){
		  ?>
			  <li <?php if($seccionActiva==8){ echo 'class="active"';} ?>><a href="usuarios.php"><i class="icon-group"></i><span>Usuarios</span> </a></li>
		  <?php }
		  //}
		}?>
		<?php if($_SESSION['tipoUsuario']!='SUPERVISOR'){ ?>
			<li class="dropdown <?php if($seccionActiva==20 || $seccionActiva==21){ echo "active";} ?>"><a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-comments"></i><span>Interno</span>  <span class='avisoMensajes'></span><b class="caret"></b></a>
			   <ul class="dropdown-menu">
				 <li><a href="comunicacionInterna.php"><i class="icon-comments"></i><span>Comunicación</span> <span class='avisoMensajes desplegable'></span></a></li>
					 <li class="divider"></li>
					 <li><a href="seleccionaAnio.php"><i class="icon-file-text"></i><span>Documentación</span> </a></li>
					 <li class="divider"></li>
					 <li><a href="seleccionaAnioPapelera.php"><i class="icon-trash"></i><span>Papelera</span> </a></li>
					 <!--li><a href="documentacion.php"><i class="icon-file-text"></i><span>Documentación</span> </a></li-->
			   </ul>
			</li> 
		<?php } ?>		
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->