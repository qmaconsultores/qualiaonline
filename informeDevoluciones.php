<?php
  $seccionActiva=7;
  include_once('cabecera.php');
  $res=false;
    
  //$estadisticasVentas=creaEstadisticasVentasInicio();
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
		
		<div class="span12">
		  <div class="widget widget-nopad" id="target-1">
			<div class="widget-header"> <i class="icon-tasks"></i>
			  <h3>Ventas realizadas por comercial</h3>
			</div>
			<!-- /widget-header -->
			
			<div class="widget-content">
			  <div class="widget big-stats-container">
				<div class="widget-content">
				  <h6 class="bigstats">Ventas realizadas:</h6>


					<span id='resultadoDos'></span>

				</div> <!-- /widget-content -->
				<!-- /widget-content --> 
				
			  </div>
			</div>
		  </div>
		 
		</div>
		
		
	</div><!-- /row -->
	
		<div class="widget widget-table action-table">
		<div class="widget-header"> <i class="icon-th-list"></i>
		  <h3>Devoluciones</h3>
		</div>
		<!-- /widget-header -->
		<div class="widget-content">
			<center>
				<div class='seleccionComercial'>
					<strong>Motivo: </strong>
					<?php 
						echo "<select name='motivo' id='motivo' class='input-large'>";
			
						$consulta=consultaBD("SELECT codigo, motivo AS texto FROM motivos ORDER BY motivo;",true);
						$datos=mysql_fetch_assoc($consulta);
						echo "<option value='todos'>Todos</option>";
						while($datos!=false){
							echo "<option value='".$datos['codigo']."'>".$datos['texto']."</option>";
							$datos=mysql_fetch_assoc($consulta);
						}
						
						echo "</select>";
					?>
					<strong>Resolución: </strong>
					<?php 
						echo "<select name='resolucion' id='resolucion' class='input-large'>";
			
						$resoluciones=array('Remesada','Transferencia','Efectivo','Anulada');
						echo "<option value='todas'>Todas</option>";
						foreach ($resoluciones as $resolucion) {
							echo "<option value='".$resolucion."'>".$resolucion."</option>";
						}
						
						echo "</select>";
					?>
					<strong>Año: </strong>
					<?php 
						echo "<select name='anio' id='anio' class='input-large'>";
						echo "<option value='0'></option>";
						for ($i=2015;$i <=date('Y') ; $i++) { 
							echo "<option value='".$i."'>".$i."</option>";
						}
						
						echo "</select>";
					?>
					<strong>Mes: </strong>
					<?php 
						echo "<select name='mes' id='mes' class='input-large'>";
						echo "<option value='0'></option>";
						echo "<option value='01'>Enero</option>";
						echo "<option value='02'>Febrero</option>";
						echo "<option value='03'>Marzo</option>";
						echo "<option value='04'>Abril</option>";
						echo "<option value='05'>Mayo</option>";
						echo "<option value='06'>Junio</option>";
						echo "<option value='07'>Julio</option>";
						echo "<option value='08'>Agosto</option>";
						echo "<option value='09'>Septiembre</option>";
						echo "<option value='10'>Octubre</option>";
						echo "<option value='11'>Noviembre</option>";
						echo "<option value='12'>Diciembre</option>";
						echo "</select>";
					?>
				</div>
			</center>
			<span id='resultado'></span>
			
		</div>
		<!-- /widget-content-->
	  </div>

    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/guidely/guidely.min.js"></script>
<script type="text/javascript" src="js/bootstrap-progressbar.js"></script>

<script type="text/javascript" src="js/full-calendar/fullcalendar.min.js"></script>

<script type="text/javascript">
   
	$('#motivo').change(function(){
		var motivo=$('#motivo').val();
		var resolucion=$('#resolucion').val();
		var mes=$('#mes').val();
		var anio=$('#anio').val();
		listadoVentas(motivo,mes,resolucion,anio);
		muestraCantidades(motivo,mes,resolucion,anio);
	});	

	$('#resolucion').change(function(){
		var motivo=$('#motivo').val();
		var resolucion=$('#resolucion').val();
		var mes=$('#mes').val();
		var anio=$('#anio').val();
		listadoVentas(motivo,mes,resolucion,anio);
		muestraCantidades(motivo,mes,resolucion,anio);
	});	

	$('#mes').change(function(){
		var motivo=$('#motivo').val();
		var resolucion=$('#resolucion').val();
		var mes=$('#mes').val();
		var anio=$('#anio').val();
		listadoVentas(motivo,mes,resolucion,anio);
		muestraCantidades(motivo,mes,resolucion,anio);
	});	

	$('#anio').change(function(){
		var motivo=$('#motivo').val();
		var resolucion=$('#resolucion').val();
		var mes=$('#mes').val();
		var anio=$('#anio').val();
		listadoVentas(motivo,mes,resolucion,anio);
		muestraCantidades(motivo,mes,resolucion,anio);
	});	
	  
	var motivo=$('#motivo').val();
	var mes=$('#mes').val();
	var resolucion=$('#resolucion').val();
	var anio=$('#anio').val();
	$(document).ready(listadoVentas(motivo,mes,resolucion,anio));
	$(document).ready(muestraCantidades(motivo,mes,resolucion,anio));
	
	function listadoVentas(motivo,mes, resolucion, anio){
		var parametros = {
                "motivo" : motivo,
                "mes" 		: mes,
                "resolucion": resolucion,
                "anio": anio
        };
		 $.ajax({
			 type: "POST",
			 url: "listadosajax/listadoDevoluciones.php",
			 data: parametros,
			  beforeSend: function () {
				   $("#resultado").html('<center><div class="seleccionComercial"><i class="icon-refresh icon-spin"></i></div></center>');
			  },
			 success: function(response){
				   $("#resultado").html(response);
			 }
		});
	  }
	  
	function muestraCantidades(motivo,mes,resolucion,anio){
		var parametros = {
                "motivo" : motivo,
                "mes" 		: mes,
                "resolucion": resolucion,
                "anio": anio
        };
		$.ajax({
			 type: "POST",
			 url: "listadosajax/muestraCantidadesDevoluciones.php",
			 data: parametros,
			  beforeSend: function () {
				   $("#resultadoDos").html('<center><i class="icon-refresh icon-spin"></i></center>');
			  },
			 success: function(response){
				   $("#resultadoDos").html(response);
			 }
		});
	}
</script>