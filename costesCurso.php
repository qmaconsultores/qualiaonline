<?php
  $seccionActiva=5;
  include_once('cabecera.php');

  $codigoEmpresa=$_POST['codigoEmpresa'];
  $codigoCurso=$_POST['codigoCurso'];

  $datosAlumno=datosCliente($codigoEmpresa);
  
  $datos=datosCostes($codigoEmpresa,$codigoCurso);
  $datosPeriodos=datosPeriodos($datos['codigo']);
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-eur"></i>
              <h3>Costes del curso para la empresa <?php echo $datosAlumno['empresa']; ?></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="formacion.php" method="post">
                  <fieldset>

                    <input type="hidden" name="codigoCurso" value="<?php echo $datos['codigo']; ?>">
					

                    <div class="control-group">                     
                      <label class="control-label" for="costesImparticion">Costes directos:</label>
                      <div class="controls">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini pagination-right" id="costesImparticion" name="costesImparticion" value="<?php echo $datos['costesImparticion']; ?>">
                          <span class="add-on">€</span>
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="costesIndirectos">Costes indirectos:</label>
                      <div class="controls">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini pagination-right" id="costesIndirectos" name="costesIndirectos" value="<?php echo $datos['costesIndirectos']; ?>">
                          <span class="add-on">€</span>
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="costesOrganizacion">Costes organización:</label>
                      <div class="controls">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini pagination-right" id="costesOrganizacion" name="costesOrganizacion" value="<?php echo $datos['costesOrganizacion']; ?>">
                          <span class="add-on">€</span>
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="costesHora">Coste medio/hora:</label>
                      <div class="controls">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini pagination-right" id="costesHora" name="costesHora" value="<?php echo $datos['costesHora']; ?>">
                          <span class="add-on">€</span>
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php 
						campoCalcula('costesElegibles', 'Total costes elegibles', $datos['costesElegibles'], '€', 'btn-primary', 'input-mini', true);
					?>
					
					
					 <fieldset>
				  <h3 class="apartadoFormulario">Períodos</h3>
                    <center>
                      <table class="table table-striped table-bordered mitadAncho" id="tabla1">
                        <thead>
                          <tr>
                            <th> Mes </th>
                            <th> Importe </th>
                          </tr>
                        </thead>
                        <tbody>
						  <?php
							$datosPeriodoCoste=mysql_fetch_assoc($datosPeriodos);
							while(isset($datosPeriodoCoste['mes'])){
								echo "
									<tr>
										<td>
											<input type='text' class='input-mini' id='mes0' name='mes0' value='".$datosPeriodoCoste['mes']."'>
										</td>
										<td>
											<input type='text' class='input-small' id='importe0' name='importe0' value='".$datosPeriodoCoste['importe']."'>
										</td>
									  </tr>";
								$datosPeriodoCoste=mysql_fetch_assoc($datosPeriodos);
							}
						?>
                          
                        </tbody>
                      </table>

                      <button type="button" class="btn btn-success" onclick="insertaFila('tabla1');"><i class="icon-plus"></i> Añadir período</button> 
                      <button type="button" class="btn btn-danger" onclick="eliminaFila('tabla1');"><i class="icon-minus"></i> Eliminar período</button> 
                    </center>
				  </fieldset>
					
					

                    
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar costes</button> 
                      <a href="formacion.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$('#calcular').click(function(){
		var costesImparticion=$('#costesImparticion').val().replace(',','.');
		var costesOrganizacion=$('#costesOrganizacion').val().replace(',','.');
		var costesIndirectos=$('#costesIndirectos').val().replace(',','.');
		var total=parseFloat(costesImparticion)+parseFloat(costesOrganizacion)+parseFloat(costesIndirectos);

		$('#costesElegibles').val(total);
	});
  });
</script>