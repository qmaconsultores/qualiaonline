<?php
  $seccionActiva=6;
  include_once('cabecera.php');
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <?php
        if(isset($_GET['codigoFichero'])){
            $res=eliminaFichero($_GET['codigoFichero']);
            if($res){
              mensajeOk("Fichero eliminado correctamente."); 
            }
            else{
              mensajeError("no se han podido eliminar el fichero. Si el problema continua, contacte con <a href='mailto:webmaster@qmaconsultores.com'>webmaster@qmaconsultores.com</a>."); 
            }
          }
        ?>        
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nuevo alumno</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="alumnosPendientes.php" method="post" enctype="multipart/form-data">
                  
                  <div class="tabbable">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#pagina1" data-toggle="tab">Datos generales</a></li>
                      <li><a href="#pagina2" data-toggle="tab">Documentación</a></li>
                      <li><a href="#pagina3" data-toggle="tab">Observaciones</a></li>
                    </ul>
                  
                    <br>

                    <div class="tab-content" id="pestanas">

                    <div class="tab-pane active" id="pagina1">
                      <fieldset class="span3">
					  
                        <div class="control-group">                     
                          <label class="control-label" for="nombre">Nombre:</label>
                          <div class="controls">
                            <input type="text" class="input-large" id="nombre" name="nombre">
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->

                        
                        <div class="control-group">                     
                          <label class="control-label" for="apellidos">Apellidos:</label>
                          <div class="controls">
                            <input type="text" class="input-large" id="apellidos" name="apellidos">
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->


                        <div class="control-group">                     
                          <label class="control-label" for="dni">DNI:</label>
                          <div class="controls">
                            <input type="text" class="input-small" id="dni" name="dni">
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->



                        <div class="control-group">                     
                          <label class="control-label" for="sexo">Sexo:</label>
                          <div class="controls">
                            
                            <label class="radio inline">
                              <input type="radio" name="sexo" value="M" checked="checked"> Hombre
                            </label>
                            
                            <label class="radio inline">
                              <input type="radio" name="sexo" value="F"> Mujer
                            </label>

                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->



                        <div class="control-group">                     
                          <label class="control-label" for="fechaNac">Fecha de nacimiento:</label>
                          <div class="controls">
                            <input type="text" class="input-small datepicker hasDatepicker" id="fechaNac" name="fechaNac" value="<?php echo imprimeFecha(); ?>">
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->





                        <div class="control-group">                     
                          <label class="control-label" for="telefono">Teléfono:</label>
                          <div class="controls">
                            <input type="text" class="input-small" id="telefono" name="telefono">
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->



                        <div class="control-group">                     
                          <label class="control-label" for="movil">Móvil:</label>
                          <div class="controls">
                            <input type="text" class="input-small" id="movil" name="movil">
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->



                        <div class="control-group">                     
                          <label class="control-label" for="mail">eMail:</label>
                          <div class="controls">
                            <input type="text" class="input-large" id="mail" name="mail">
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->


                      </fieldset>
                      <fieldset class="span3">
                        
						
                        <div class="control-group">                     
                          <label class="control-label" for="numSS1">Nº Seg. Social:</label>
                          <div class="controls numSS">
                            <div class="input-prepend input-append">
                              <input type="text" class="input-supermini" id="numSS1" name="numSS1" maxlength="2">
                              <span class="add-on">/</span>
                              <input type="text" class="input-small" id="numSS2" name="numSS2" maxlength="10">
                            </div>
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->

						<?php
							campoSelect('estudios','Nivel de estudios:',array('1. Menos que primaria','2. Educación primaria','3. ESO, EGB, Cert. Prof. (1y2)',
							'4. Bachillerato, BUP, FP (I y II)','5. Cert. Prof. de Nivel 3','6. Técnico Superior','7. -1º Ciclo Uni. (Diplomatura-Grados)',
							'8. -2º Ciclo Uni. (Licenciatura-Master)','9. -3º Ciclo Uni. (Doctorado)','10. Otras Titulaciones'),array('sin','primarios','3',
							'bachiller','5','6','diplomatura','licenciatura','9','10'));
						?>




                        <div class="control-group">                     
                          <label class="control-label" for="categoria">Categoría:</label>
                          <div class="controls">
                            
                            <select name="categoria" class='selectpicker show-tick'>
                              <option value="directivo">Directivo</option>
                              <option value="intermedio">Mando Intermedio</option>
                              <option value="tecnico">Técnico</option>
                              <option value="cualificado">Trabajador Cualificado</option>
                              <option value="bajaCualificacion">Trabajador no Cualificado</option>
                            </select>

                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->



                        <div class="control-group">                     
                          <label class="control-label" for="cotizacion">Grupo de Cotización:</label>
                          <div class="controls">
                            
                            <select name="cotizacion" class='selectpicker show-tick'>
                              <option value="1">1. Ingenieros y Licenciados</option>
                              <option value="2">2. Ingenieros Técnicos</option>
                              <option value="3">3. Jefes Administrativos y de taller</option>
                              <option value="4">4. Ayudantes no Titulados</option>
                              <option value="5">5. Oficiales Administrativos</option>
                              <option value="6">6. Subalternos</option>
                              <option value="7">7. Auxiliares Administrativos</option>
                              <option value="8">8. Oficiales de Primera y Segunda</option>
                              <option value="9">9. Oficiales de Tercera y Especialistas</option>
                              <option value="10">10. Peones</option>
                              <option value="11">11. Trabajadores menores de 18 años</option>
                            </select>

                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->



                        <div class="control-group">                     
                          <label class="control-label" for="horario">Horario de trabajo:</label>
                          <div class="controls">
                            <input type="text" class="input-large" id="horario" name="horario">
                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->



                        <div class="control-group">                     
                          <label class="control-label" for="discapacidad">Discapacidad:</label>
                          <div class="controls">
                            
                            <label class="radio inline">
                              <input type="radio" name="discapacidad" value="SI"> Si
                            </label>
                            
                            <label class="radio inline">
                              <input type="radio" name="discapacidad" value="NO" checked="checked"> No
                            </label>

                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->



                        <div class="control-group">                     
                          <label class="control-label" for="discapacidad">Víctima de terrorismo:</label>
                          <div class="controls">
                            
                            <label class="radio inline">
                              <input type="radio" name="vTerrorismo" value="SI"> Si
                            </label>
                            
                            <label class="radio inline">
                              <input type="radio" name="vTerrorismo" value="NO" checked="checked"> No
                            </label>

                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->




                        <div class="control-group">                     
                          <label class="control-label" for="vViolencia">Víctima de violencia de género:</label>
                          <div class="controls">
                            
                            <label class="radio inline">
                              <input type="radio" name="vViolencia" value="SI"> Si
                            </label>
                            
                            <label class="radio inline">
                              <input type="radio" name="vViolencia" value="NO" checked="checked"> No
                            </label>

                          </div> <!-- /controls -->       
                        </div> <!-- /control-group -->

                      </fieldset>
                    
                    </div>


                    <div class="tab-pane" id="pagina2">

                      <table class="table table-striped table-bordered tablaDocumentos">
                        <thead>
                          <tr>
                            <th> Título del documento </th>
                            <th> Fecha de Subida </th>
                            <th class="centro"> </th>
                          </tr>
                        </thead>
                        <tbody>
						
							
                        
                        </tbody>
                      </table>
                    
                      <a href="javascript:void" class="btn btn-success" id="botonSubida"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 20 MB como máximo.
                      <input type="file" class="hide" name="fichero" id="fichero" onchange="nuevaFila();"/>

                    </div><!-- /pagina2 -->

                    <div class="tab-pane" id="pagina3">

                      <table class="table table-striped table-bordered datatable" id="tablaObservaciones">
                      <thead>
                        <tr>
                          <th> Fecha </th>
                          <th> Observaciones </th>
                        </tr>
                      </thead>
                      <tbody>
                                  
                        <tr>
                          <?php
                            campoFechaTabla('fecha0');
                          ?>
                          <td>
                            <div class='control-group'>                     
                              <div class='controls'>
                                <textarea name='observacion0' class='areaTexto' id='observacion0'></textarea>
                              </div> <!-- /controls -->       
                            </div> <!-- /control-group -->
                          </td>
                        </tr>

                      </tbody>
                    </table>
                      
                    <center>
                      <button type="button" class="btn btn-success" onclick="insertaFila('tablaObservaciones');"><i class="icon-plus"></i> Añadir Observación</button> 
                      <button type="button" class="btn btn-danger" onclick="eliminaFila('tablaObservaciones');"><i class="icon-minus"></i> Eliminar Observación</button> 
                    </center>

                    </div><!-- /pagina2 -->

                  </div>
                </div>


                  <fieldset class="sinFlotar">
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Insertar alumno</button> 
                      <a href="alumnosPendientes.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});

    $('#botonSubida').click(function(){
      if(!$(this).attr('disabled')){
        $('#fichero').click();
      }
    });

  });

function nuevaFila(){
    $('table').find("tr:last").after("<tr id='fila'><td><input type='text' name='titulo' class='input-large' /></td><td><input type='text' name='fechaSubida' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' onclick='enviaFormulario();' class='btn btn-primary'><i class='icon icon-upload'></i> Subir fichero</a> <a href='javascript:void' onclick='eliminaFila();' class='btn btn-warning'><i class='icon icon-ban-circle'></i> Cancelar</a></td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
    $('#botonSubida').attr('disabled',true);
  }

  function eliminaFila(){
    $('table').find("tr:last").remove();
    $('#botonSubida').attr('disabled',false);
  }

  function enviaFormulario(){
    $('form').submit();
  }
</script>
