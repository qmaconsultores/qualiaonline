<?php
  $seccionActiva=15;
  include_once('cabecera.php');
    
  if(isset($_POST['codigo'])){
    $res=actualizaEncuestaSatisfaccion();
  }

  $estadisticas=creaDatosEstadisticasSatisfaccion();
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          
         
        </div>
        <!-- /span6 -->
        <div class="span12">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Número total de Encuestas de Satisfacción realizadas:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-edit"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Encuestas registradas</div>
                  </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>


      <div class="span12">
        
        <?php
        if(isset($_POST['codigo'])){
          if($res){
            mensajeOk("Encuesta actualizada correctamente");
          }
          else{
            mensajeError("no se han podido actualizar la Encuesta. Compruebe los datos introducidos.");
          }
        }
  			elseif(isset($_POST['puntuacion1'])){
  				if($res){
  					mensajeOk("Encuesta registrada correctamente");
  				}
  				else{
  					mensajeError("no se han podido registrar la Encuesta. Compruebe los datos introducidos.");
  				}
  			}
		
        ?>
        
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Encuestas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Cliente </th>
                    <th> Fecha </th>
                    <th class="td-actions"> </th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeEncuestasSatisfaccion();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script src="js/excanvas.min.js" type="text/javascript"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
    <?php
      $datos=generaDatosGraficoEncuestasSatisfaccion();
    ?>
    var pieData = [
        {
            value: <?php echo $datos['1']; ?>,
            color: "#B02B2C"
        },
        {
            value: <?php echo $datos['2']; ?>,
            color: "#f89406"
        },
        {
            value: <?php echo $datos['3']; ?>,
            color: "#FFFF00"
        },
        {
            value: <?php echo $datos['4']; ?>,
            color: "#428bca"
        },
		{
            value: <?php echo $datos['5']; ?>,
            color: "#468847"
        }
      ];

    var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
    
    $("#valor1").text("<?php echo $datos['1']; ?>");
    $("#valor2").text("<?php echo $datos['2']; ?>");
    $("#valor3").text("<?php echo $datos['3']; ?>");
    $("#valor4").text("<?php echo $datos['4']; ?>");
	$("#valor5").text("<?php echo $datos['5']; ?>");
</script>

