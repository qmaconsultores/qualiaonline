<?php
  $seccionActiva=2;
  include_once('cabecera.php');
  $consulta=consultaBD("SELECT referencia FROM clientes WHERE activo='SI' ORDER BY referencia DESC LIMIT 1;",true);
  $referencia=mysql_fetch_assoc($consulta);
  $referenciaNueva=$referencia['referencia']+1;
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Alta de cuenta</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="cuentas.php" method="post">
                  <fieldset class="span5">
				  
					<?php
						campoOculto($referenciaNueva,'referencia');
						campoOculto('NO','baja');
						campoOculto('','fechaBaja');
					?>
                    
                    <div class="control-group">                     
                      <label class="control-label" for="empresa">Empresa:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="empresa" name="empresa">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="cif">CIF:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cif" name="cif" maxlength="9">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					          <div class="control-group">                     
                      <label class="control-label" for="ccc">NºSS (C.C.C):</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="ccc" name="ccc" maxlength="12">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="numSS1">Nº Cuenta Bancaria:</label>
                      <div class="controls numSS">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini" id="iban" name="iban" maxlength="4" size="4">
                          <span class="add-on">/</span>
                          <input type="text" class="input-medium" id="numCuenta" name="numCuenta" maxlength="20" size="20">
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoSelect('bic','Entidad bancaria',array('BANESTO','THE ROYAL BANK OF SCOTLAND PLC, SUCURSAL EN ESPAÑA','AHORRO CORPORACION FINANCIERA, S.A., SOCIEDAD DE VALORES','BANCO ALCALA, S.A.','ARESBANK, S.A.','BANCA PUEYO, S.A.','KUTXABANK, S.A.','BANCO BPI, S.A., SUCURSAL EN ESPAÑA','ING BELGIUM, S.A., SUCURSAL EN ESPAÑA','BANCO BILBAO VIZCAYA ARGENTARIA, S.A.','BANCO DE CRÉDITO SOCIAL COOPERATIVO S.A.','CAJA RURAL DE CASTILLA-LA MANCHA, S.C.C.','BANCO COOPERATIVO ESPAÑOL, S.A.','NOVO BANCO, S.A., SUCURSAL EN ESPAÑA','BANCO MEDIOLANUM, S.A.','BANKINTER, S.A.','BANKOA, S.A.','BANCA MARCH, S.A.','BANQUE MAROCAINE COMMERCE EXTERIEUR INTERNATIONAL, S.A.','THE BANK OF TOKYO-MITSUBISHI UFJ, LTD, SUCURSAL EN ESPAÑA','BANCO DO BRASIL AG, SUCURSAL EN ESPAÑA','BANCO DE SABADELL, S.A.','BANCO SANTANDER, S.A.','CREDIT AGRICOLE CORPORATE AND INVESTMENT BANK, SUCURSAL EN ESPAÑA','RBC INVESTOR SERVICES ESPAÑA, S.A.','ABANCA CORPORACIÓN BANCARIA, S.A.','BANKIA, S.A.','CAIXABANK, S.A.','CM CAPITAL MARKETS BOLSA, SOCIEDAD DE VALORES, S.A.','CAJA DE ARQUITECTOS, S.C.C.','IBERCAJA BANCO, S.A.','BANCO CAMINOS, S.A.','CAJAS RURALES UNIDAS, S.C.C.','CAIXA DE CREDIT DELS ENGINYERS - CAJA DE CREDITO DE LOS INGENIEROS, S.C.C.','CAJA DE AHORROS Y M.P. DE ONTINYENT','LIBERBANK, S.A.','COLONYA - CAIXA DESTALVIS DE POLLENSA','BANCO DE CASTILLA-LA MANCHA, S.A. /CAJA DE AHORROS DE CASTILLA-LA MANCHA','CECABANK, S.A.','CATALUNYA BANC, S.A.','BANCO CAIXA GERAL, S.A.','CITIBANK ESPAÑA, S.A.','CAJA LABORAL POPULAR, C.C.','CREDIT SUISSE AG, SUCURSAL EN ESPAÑA','BANCO DE CAJA ESPAÑA DE INVERSIONES, SALAMANCA Y SORIA, S.A.','CAJASUR BANCO, S.A.','DEXIA SABADELL, S.A.','BANCO DE ESPAÑA','BANCO ESPIRITO SANTO DE INVESTIMENTO, S.A., SUCURSAL EN ESPAÑA','EVO BANCO S.A.U.','BANCO FINANTIA SOFINLOC, S.A.','BANCO MARE NOSTRUM, S.A.','GVC GAESCO VALORES, SOCIEDAD DE VALORES, S.A.','SOCIEDAD DE GESTION DE LOS SISTEMAS DE REGISTRO, COMPENSACION Y LIQUIDACION DE VALORES, S.A.U.','INSTITUTO DE CREDITO OFICIAL','ING BANK, N.V., SUCURSAL EN ESPAÑA','INVERSEGUROS, SOCIEDAD DE VALORES, S.A.','BANCO INVERSIS, S.A.','SOCIEDAD ESPAÑOLA DE SISTEMAS DE PAGO, S.A.','INTERMONEY VALORES, SOCIEDAD DE VALORES, S.A.','LINK SECURITIES, SOCIEDAD DE VALORES, S.A.','BANCO DE MADRID, S.A.','BME CLEARING, S.A.','MAPFRE INVERSION, SOCIEDAD DE VALORES, S.A.','MERRILL LYNCH CAPITAL MARKETS ESPAÑA, S.A., SOCIEDAD DE VALORES','BANCO DE LA NACION ARGENTINA, SUCURSAL EN ESPAÑA','NATIXIS, S.A., SUCURSAL EN ESPAÑA','TARGOBANK, S.A.','POPULAR BANCA PRIVADA, S.A.','BANCOPOPULAR-E, S.A.','BANCO POPULAR ESPAÑOL, S.A.','COOPERATIEVE CENTRALE RAIFFEISEN- BOERENLEENBANK B.A. (RABOBANK NEDERLAND), SUCURSAL EN ESPAÑA','EBN BANCO DE NEGOCIOS, S.A.','BANCO PASTOR, S.A.','RENTA 4 BANCO, S.A.','RENTA 4 SOCIEDAD DE VALORES, S.A.','UBI BANCA INTERNATIONAL, S.A., SUCURSAL EN ESPAÑA','UNICAJA BANCO, S.A.','SOCIEDAD RECTORA BOLSA VALORES DE BARCELONA, S.A., S.R.B.V.','SOCIEDAD RECTORA BOLSA DE VALORES DE BILBAO, S.A., S.R.B.V.','SOCIEDAD RECTORA BOLSA VALORES DE VALENCIA, S.A., S.R.B.V.','BARCLAYS BANK PLC, SUCURSAL EN ESPAÑA','BARCLAYS BANK, S.A.','BNP PARIBAS ESPAÑA, S.A.','BNP PARIBAS FORTIS, S.A., N.V., SUCURSAL EN ESPAÑA','BNP PARIBAS SECURITIES SERVICES, SUCURSAL EN ESPAÑA','BNP PARIBAS, SUCURSAL EN ESPAÑA','CITIBANK INTERNATIONAL LTD, SUCURSAL EN ESPAÑA','COMMERZBANK AKTIENGESELLSCHAFT, SUCURSAL EN ESPAÑA','DEUTSCHE BANK, S.A.E.','FINANDUERO, SOCIEDAD DE VALORES, S.A.','HSBC BANK PLC, SUCURSAL EN ESPAÑA','HYPOTHEKENBANK FRANKFURT AG., SUCURSAL EN ESPAÑA','PORTIGON AG, SUCURSAL EN ESPAÑA','SOCIETE GENERALE, SUCURSAL EN ESPAÑA','UBS BANK, S.A.'),
						array('BAEMESM1XXX','ABNAESMMXXX','AHCFESMMXXX','ALCLESMMXXX','AREBESMMXXX','BAPUES22XXX','BASKES2BXXX','BBPIESMMXXX','BBRUESMXXXX','BBVAESMMXXX','BCCAESMMXXX','BCOEESMM081','BCOEESMMXXX','BESMESMMXXX','BFIVESBBXXX','BKBKESMMXXX','BKOAES22XXX','BMARES2MXXX','BMCEESMMXXX','BOTKESMXXXX','BRASESMMXXX','BSABESBBXXX','BSCHESMMXXX','BSUIESMMXXX','BVALESMMXXX','CAGLESMMVIG','CAHMESMMXXX','CAIXESBBXXX','CAPIESMMXXX','CASDESBBXXX','CAZRES2ZXXX','CCOCESMMXXX','CCRIES2AXXX','CDENESBBXXX','CECAESMM045','CECAESMM048','CECAESMM056','CECAESMM105','CECAESMMXXX','CESCESBBXXX','CGDIESMMXXX','CITIES2XXXX','CLPEES2MXXX','CRESESMMXXX','CSPAES2L108','CSURES2CXXX','DSBLESMMXXX','ESPBESMMXXX','ESSIESMMXXX','EVOBESMMXXX','FIOFESM1XXX','GBMNESMMXXX','GVCBESBBETB','IBRCESMMXXX','ICROESMMXXX','INGDESMMXXX','INSGESMMXXX','INVLESMMXXX','IPAYESMMXXX','IVALESMMXXX','LISEESMMXXX','MADRESMMXXX','MEFFESBBXXX','MISVESMMXXX','MLCEESMMXXX','NACNESMMXXX','NATXESMMXXX','POHIESMMXXX','POPIESMMXXX','POPLESMMXXX','POPUESMMXXX','PRABESMMXXX','PROAESMMXXX','PSTRESMMXXX','RENBESMMXXX','RENTESMMXXX','UBIBESMMXXX','UCJAES2MXXX','XBCNESBBXXX','XRBVES2BXXX','XRVVESVVXXX','BPLCESMMXXX','BARCESMMXXX','BNPAESMZXXX','GEBAESMMBIL','PARBESMHXXX','BNPAESMHXXX','CITIESMXSEC','COBAESMXTMA','DEUTESBBASS','CSSOES2SFIN','MIDLESMXXXX','EHYPESMXXXX','WELAESMMFUN','SOGEESMMAGM','UBSWESMMNPB'));
						campoTextoSimbolo('creditoFormativo','Crédito formativo','€');
						campoSelect('formaPago','Forma de pago',array('Transferencia','Efectivo','Cheque','Domiciliación bancaria','Tarjeta'),array('transferencia','efectivo','cheque','domiciliacion','tarjeta'));
						
					?>


                    <div class="control-group">                     
                      <label class="control-label" for="nuevacreacion">Empresa de Nueva Creación:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="nuevacreacion" value="SI"> Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="nuevacreacion" value="NO" checked="checked"> No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php 
						campoFecha('fechaCreacion','Fecha de creación',' ');
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="direccion">Dirección:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="direccion" name="direccion">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php
						campoTexto('direccionVisita','Dirección de visita');
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="cp">CP:</label>
                      <div class="controls">
                        <input type="text" class="input-mini" id="cp" name="cp" maxlength="5">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="localidad">Localidad:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="localidad" name="localidad">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="provincia">Provincia:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="provincia" name="provincia">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono" maxlength="9">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="movil">Móvil:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="movil" name="movil" maxlength="9">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="telefonoDos">Teléfono 2:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefonoDos" name="telefonoDos" maxlength="9">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="movilDos">Móvil 2:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="movilDos" name="movilDos" maxlength="9">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="mail">Mail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="mail" name="mail">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('mail2','Mail 2');
						campoTexto('web','Web');
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="fax">Fax:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="fax" name="fax" maxlength="9">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="pyme">PYME:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="pyme" value="SI" checked="checked"> Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="pyme" value="NO"> No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                  </fieldset>
                  <fieldset class="span3">

                    <?php
						campoTexto('sector','Sector');
            campoActividades();
						campoSelect('estado','Estado',array('Sin contactar','En proceso','Reconcertar','Vendido','Pendiente','Baja'),array('Sin contactar','En proceso','Reconcertar','Cerrada','Pendiente','Baja'));
						echo "<div id='pendienteOculto'>";
							campoRadio('firmado','Firmado');
							campoRadio('pendiente','Pendiente de','Datos',array('Datos','Formación'),array('Datos','Formacion'));
						echo "</div>";
					?>


                      
                    <div class="control-group">                     
                      <label class="control-label" for="tipo">Tipo de empresa:</label>
                      <div class="controls">
                        
                        <select name="tipo" class='selectpicker show-tick' data-live-search="true">
                          <option value="EMPRESA">Empresa</option>
                          <option value="PARTICULAR">Particular</option>
                          <option value="AUTONOMO">Autónomo</option>
                          <option value="COLABORADOR">Colaborador</option>
                          <option value="ASOCIACION">Asociación</option>

                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <?php selectTrabajador($_SESSION['codigoS']); ?>


                    <div class="control-group">                     
                      <label class="control-label" for="fechaRegistro">Fecha de registro:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaRegistro" name="fechaRegistro" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					
					<?php
						campoTexto('repreLegal','Representante legal');
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="dniRepresentante">DNI Representante:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="dniRepresentante" name="dniRepresentante" maxlength="9">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
                    
					           <div class="control-group">                     
                      <label class="control-label" for="contacto">Persona de contacto:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="contacto" name="contacto">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					


                    <div class="control-group">                     
                      <label class="control-label" for="cargo">Cargo:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="cargo" name="cargo">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php
						campoRadio('tieneColaborador','Colaborador');
					?>

					<div id="colaboradorOculto">
						<?php
							$consulta="SELECT codigo, empresa AS texto FROM colaboradores ORDER BY empresa;";
							campoSelectConsulta('comercial','Colaborador',$consulta);
						?>
					</div>
					
					<?php
						campoRadio('esUnGrupo','Grupo');
					?>
					
					<div id="grupoOculto">
						<?php
							$consulta="SELECT codigo, empresa AS texto FROM clientes WHERE activo='SI' ORDER BY empresa;";
							//campoSelectConsultaAjax('codigoVinculacion','Empresa vinculada',$consulta,false,'detallesCuenta.php?codigo=');
						?>
					</div>
					
					<?php
					  selectConsultaMultiple();
					  campoOculto('NULL','tipoServicio');
                    ?>
					                

                    <div class="control-group">                     
                      <label class="control-label" for="representacion">R. Legal de los Trabajadores:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="representacion" value="SI"> Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="representacion" value="NO" checked="checked"> No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('numTrabajadores','Nº Trabajadores','','input-mini');
						campoSelect('tipoRemesa','Tipo de remesa',array('Core','B2B'),array('core','b2b'));
						campoTexto('responsableSeguridad','Responsable de seguridad');
						campoTexto('nifResponsable','NIF responsable','','input-small');
						campoTexto('cnae','CNAE');
						campoRadio('trabajadores','Trabajadores');
						areaTexto('observaciones','Observaciones');
					?>



                  </fieldset>
				  
				  <h3 class='apartadoFormulario sinFlotar'>Datos Gestoría</h3>
				  
				  <fieldset class="span3">
					<?php
						campoTexto('nombreGestoria','Nombre');	
						campoTexto('mailGestoria','Correo electrónico');				
						campoTexto('telefonoGestoria','Teléfono');	
					?>
				  </fieldset>
				  

                  <fieldset class="sinFlotar">
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Registrar cuenta</button> 
                      <a href="cuentas.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasObjetivos.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script src="js/jquery.inputmask.js" type="text/javascript"></script>
<script type="text/javascript" src="js/iban.js"></script>
<script type="text/javascript" src="js/cif.js"></script>
<script type="text/javascript" src="js/nss.js"></script>
<script type="text/javascript" src="js/validaCP.js"></script>
<script type="text/javascript" src="js/calculaBic.js"></script>
<script type="text/javascript" src="js/selectAjax.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
	$("#colaboradorOculto").css('display','none');
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$('.hasDatepicker').inputmask({"mask": "99/99/9999"});
	$("#creditoFormativo").attr('maxlength','10');
	$("#numCuenta").focusout(function() {
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
		if(isAccountComplete()){
			var value = $("#numCuenta").val();
			if(compruebaCCC(value.substr(0,4), value.substr(4,4), value.substr(8,2), value.substr(10,10))){
				updateBic(value);
			}else{
				alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
			}
		}else{
			alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
		}
	  }).blur(function(){
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
		if(isAccountComplete()){
			var value = $("#numCuenta").val();
			if(compruebaCCC(value.substr(0,4), value.substr(4,4), value.substr(8,2), value.substr(10,10))){
				updateBic(value);
			}else{
				alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
			}
		}else{
			alert('Por favor, introduce un código de cuenta bancaria que sea correcto.');
		}
	});
	$("input[type=radio][name=tieneColaborador]").change(function(){
		if(this.value=='SI'){
			$("#colaboradorOculto").css('display','block');
		}else{
			$("#colaboradorOculto").css('display','none');
		}
	});
	
	$("input[type=radio][name=esUnGrupo]").change(function(){
		if(this.value=='SI'){
			$("#grupoOculto").css('display','block');
		}else{
			$("#grupoOculto").css('display','none');
		}
	});
	
	/*$("#ccc").focusout(function() {
		validaNss($("#ccc").val());
	});*/
	
	$("#cif").focusout(function() {
		//isValidCif($("#cif").val());
		compruebaDuplicados($('#cif').val());
	});
	
	$("#cp").focusout(function() {
		var provincia=obtieneProvincia($("#cp").val());
		if(provincia!=false){	
			$("#provincia").val(provincia);
		}else{
			alert("Error en la introducción del CP. Introduzca uno válido.");
		}
	});
	
	$('#pendienteOculto').css('display','none');
	$('#estado').change(function(){
		if($(this).val()=='Pendiente'){
			$('#pendienteOculto').show(300);
		}else{
			$('#pendienteOculto').hide(300);
		}
	});
  });
  
  function compruebaDuplicados(codigo){
	$.ajax({
        type: "POST",
        url: "listadosajax/compruebaCif.php",
        data: "cif=" + codigo,
        success: function(response){
			if(response!='OK'){
				alert("ERROR:  CIF/NIF duplicado, por favor revise si la empresa ya está registrada");
			}
        }
    });
  }
</script>
