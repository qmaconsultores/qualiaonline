<?php
if ($_GET) {
	session_start();
    require_once('../funciones.php');
    switch ($_GET['action']) {
        case 'getMembersAjx':
            getMembersAjx();
            break;
        case 'updateMemberAjx':
            updateMemberAjx();
            break;
        case 'deleteMember':
            deleteMember();
            break;
    }
    exit;
}

function getMembersAjx() {

    // SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }

    // SQL order
    $aColumns = array('referencia', 'empresa', 'nombreProducto', 'fechaEmision', 'fechaVencimiento', 'coste', 'cobrada', 'enviada', 'enviadaCliente','resolucionFactura','formaPago','firma');
    $sOrder = 'ORDER BY insercion DESC';
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $sOrder .= '`'.$aColumns[ (int)$_GET['iSortCol_'.$i] ].'` '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $sOrder = substr_replace($sOrder, '', -2);
        if ($sOrder == 'ORDER BY') {
            $sOrder = '';
        }
    }

    // SQL where
	$sWhere='HAVING 1=1';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
		$sWhere='HAVING 1=1 AND(';
        for ($i=0; $i<count($aColumns) ; $i++) {
            if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true') {
				if($aColumns[$i]=='referencia' || $aColumns[$i]=='formaPago'){
					$sWhere .='CONVERT(CAST(CONVERT(facturacion.'.$aColumns[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch']."%' USING latin1) AS binary) USING utf8) OR ";
					
				}elseif(strpos($aColumns[$i],'fecha')!==false){
					$sWhere .= "DATE_FORMAT(".$aColumns[$i].",'%d/%m/%Y') LIKE '%".$_GET['sSearch']."%' OR ";
				}else{
					$sWhere .='CONVERT(CAST(CONVERT('.$aColumns[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch']."%' USING latin1) AS binary) USING utf8) OR ";
					
				}
            }
        }
        $sWhere = substr_replace( $sWhere, '', -3 );
        $sWhere .= ')';
    }

	conexionBD();
	$wherePerfil='';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){
		$codigoU=$_SESSION['codigoS'];
		$wherePerfil="AND (clientes.codigoUsuario='$codigoU' OR clientes.codigoUsuario IN(SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU'))";
	}
    $aMembers = consultaBD("SELECT facturacion.codigo, facturacion.fechaEmision, facturacion.cobrada, facturacion.fechaVencimiento, facturacion.referencia, facturacion.coste, clientes.empresa, facturacion.enviada, facturacion.concepto, facturacion.enviadaCliente, clientes.codigo AS codigoCliente, insercion, facturacion.firma, facturacion.devuelta, facturacion.formaPago, productos.nombreProducto, resolucionFactura, clientes.codigoUsuario AS comercial, facturacion.anio 
		FROM facturacion LEFT JOIN clientes ON clientes.codigo=facturacion.codigoCliente LEFT JOIN productos ON facturacion.concepto=productos.codigo {$sWhere} {$wherePerfil} {$sOrder} {$sLimit}");
	
	$datos=mysql_fetch_assoc($aMembers);
	$contador = consultaBD("SELECT facturacion.codigo, facturacion.fechaEmision, facturacion.cobrada, facturacion.fechaVencimiento, facturacion.referencia, facturacion.coste, clientes.empresa, facturacion.enviada, facturacion.concepto, facturacion.enviadaCliente, clientes.codigo AS codigoCliente, insercion, facturacion.firma, facturacion.devuelta, facturacion.formaPago, productos.nombreProducto, resolucionFactura, clientes.codigoUsuario AS comercial, facturacion.anio
		FROM facturacion LEFT JOIN clientes ON clientes.codigo=facturacion.codigoCliente LEFT JOIN productos ON facturacion.concepto=productos.codigo {$sWhere} {$wherePerfil}");
	cierraBD();

    $output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($aMembers),
        'iTotalDisplayRecords' => mysql_num_rows($contador),
        'aaData' => array()
    );
	
	$iconoC=array('SI'=>'<i class="icon-ok-sign iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove-sign iconoFactura icon-danger"></i>');
	$devuelta=array('NO'=>'No devuelta.','SI'=>'Devuelta.');
	
	
	while(isset($datos['codigo'])){
		$devolucion=consultaBD('SELECT * FROM devoluciones_facturas WHERE codigoFactura='.$datos['codigo'].' ORDER BY fecha DESC LIMIT 1',true,true);
		if($devolucion){
			$devolucion='Devuelta. '.$devolucion['resolucionFactura'];
		} else {
			$devolucion='No devuelta.';
		}
		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$fechaVencimiento=formateaFechaWeb($datos['fechaVencimiento']);
		if($datos['concepto']=='14'){ 
			if($datos['firma']!='3'){
				$letra="F";
			}else{
				$letra="S";
			}
		}else{
			if($datos['firma']!='3'){
				$letra="C";
			}else{
				$letra="S";
			}
		}

		if($datos['firma']=='4'){
			$letra='E';
		}
		$datos['referencia']=str_pad($datos['referencia'], 5, "0", STR_PAD_LEFT);
		$botonera="<div class='btn-group'>
                    <button type='button' class='btn btn-warning dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
                    <ul class='dropdown-menu warning' role='menu'>";
					
		if($_SESSION['tipoUsuario']!='ADMINISTRACION' && $_SESSION['tipoUsuario']!='ADMIN'){
		}else{
			$variable='14';
			$botonera.="<li><a href='detallesFactura.php?codigo=".$datos['codigo']."&seccion=$variable' class='pull-left'><span class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a></span></li>";
		}
		$botonera.="<li><a href='enviarEmail.php?codigoFactura=".$datos['codigo']."&seccion=14' class='pull-left'><span class='btn btn-success'><i class='icon-envelope'></i> Enviar</i></a></span></li>
					<li><a href='generaFactura.php?codigo=".$datos['codigo']."' class='pull-left'><span class='btn btn-info'><i class='icon-download-alt'></i> Importe total</i></a></span></li>";
		$vtos=consultaBD("SELECT * FROM facturas_vto WHERE codigoFactura=".$datos['codigo'],true);
		$i=1;
		while ($vto=mysql_fetch_assoc($vtos)) {
				$botonera.="<li><a href='generaFactura.php?codigo=".$datos['codigo']."&vencimiento=".$vto['codigo']."' class='pull-left'><span class='btn btn-info'><i class='icon-calendar'></i> Vencimiento ".$i."</i></a></span></li>";
				$i++;
		}
		$botonera.="</ul>
			</div>";
        $aItem = array(
            $letra."-".$datos['referencia'].'/'.substr($datos['anio'], 2),
			"<a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a>",
        	$datos['nombreProducto'],
        	$fecha ,
			$fechaVencimiento,
			formateaNumero($datos['coste'])." €",
			"<td class='centro'> ".$iconoC[$datos['cobrada']]."</td>",
			"<td class='centro'> ".$iconoC[$datos['enviada']]."</td>",
			"<td class='centro'> ".$iconoC[$datos['enviadaCliente']]."</td>",
			"<td> ".$devolucion."</td>",
        	"<td class='centro'>
				$botonera
        	</td>",
			"<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>",
			'DT_RowId' => $datos['codigo']
        );
		$datos=mysql_fetch_assoc($aMembers);
        $output['aaData'][] = $aItem;
	}
    /*foreach ($aMembers as $iID => $aInfo) {
		echo "ENTRA";
        $aItem = array(
            $aInfo['nombre'], $aInfo['apellidos'], $aInfo['email'], $aInfo['telefono'], $aInfo['usuario'], $aInfo['clave'], 'DT_RowId' => $aInfo['codigo']
        );
        $output['aaData'][] = $aItem;
    }*/
    echo json_encode($output);
}
