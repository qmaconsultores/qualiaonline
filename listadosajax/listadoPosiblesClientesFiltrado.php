<?php
if ($_GET) {
	session_start();
    require_once('../funciones.php');
    switch ($_GET['action']) {
        case 'getMembersAjx':
            getMembersAjx();
            break;
        case 'updateMemberAjx':
            updateMemberAjx();
            break;
        case 'deleteMember':
            deleteMember();
            break;
    }
    exit;
}

function getMembersAjx() {

	$formu=array();
	$formu['comercial']=$_GET['comercial'];
	$formu['servicio']=$_GET['servicio'];
	$formu['poblacion']=$_GET['poblacion'];
	$formu['estado']=$_GET['estado'];

    // SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }

    // SQL order
    $aColumns = array('referencia', 'empresa', 'estado', 'pendiente', 'cif', 'localidad', 'nombre', 'actividad', 'nombreProducto', 'activo', 'codigoUsuario', 'provincia');
    $sOrder = '';
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $sOrder .= '`'.$aColumns[ (int)$_GET['iSortCol_'.$i] ].'` '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $sOrder = substr_replace($sOrder, '', -2);
        if ($sOrder == 'ORDER BY') {
            $sOrder = '';
        }
    }

    // SQL where
	if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
		$sWhere='WHERE (codigoUsuario="'.$_SESSION['codigoS'].'" OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'"))';
	}else{
		$sWhere=compruebaPerfilParaWhere();
	}
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
		if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
			$sWhere='WHERE (codigoUsuario="'.$_SESSION['codigoS'].'" OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'"))';
		}else{
			$sWhere=compruebaPerfilParaWhere();
		}
		$sWhere=$sWhere.' AND(';
        for ($i=0; $i<count($aColumns) ; $i++) {
            if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true') {
				if($aColumns[$i]=='nombre'){
					$sWhere .= "CONCAT(`nombre`, ' ', `apellidos`) LIKE '%".$_GET['sSearch']."%' OR ";
				}else{
					$sWhere .= '`' . $aColumns[$i]."` LIKE '%".$_GET['sSearch']."%' OR ";
				}
            }
        }
        $sWhere = substr_replace( $sWhere, '', -3 );
        $sWhere .= ')';
    }

    if($_GET['compra'] != 'NULL'){
        if($_GET['compra'] == 'SI'){
        	$sWhere .= ' AND clientes.codigo IN (SELECT codigoCliente FROM ventas)';
       	} else {
       		$sWhere .= ' AND clientes.codigo NOT IN (SELECT codigoCliente FROM ventas)';
       	}
    }

    $sWhere .= " AND clientes.baja = '".$_GET['baja']."'";
    if($_GET['baja'] == 'SI' && $_GET['motivoBaja'] != 'NULL'){
    	$sWhere .= " AND clientes.motivoBaja = '".$_GET['motivoBaja']."'";
    }

	conexionBD();
	$filtrado=cargaVariablesFiltrado('GET');
    $aMembers = consultaBD("SELECT clientes.codigo, empresa, nombreProducto, actividad, usuarios.nombre, usuarios.apellidos, referencia, localidad, cif, estado, firmado, pendiente FROM clientes 
	LEFT JOIN productos ON clientes.tipoServicio=productos.codigo 
	LEFT JOIN usuarios ON usuarios.codigo=clientes.codigoUsuario {$sWhere} AND activo='NO' AND baja='NO' $filtrado {$sOrder} {$sLimit}");
	$datos=mysql_fetch_assoc($aMembers);
    $iCnt = consultaBD("SELECT COUNT(clientes.codigo) AS 'Cnt' FROM `clientes` LEFT JOIN productos ON clientes.tipoServicio=productos.codigo 
	LEFT JOIN usuarios ON usuarios.codigo=clientes.codigoUsuario {$sWhere} AND activo='NO' AND baja='NO' $filtrado");
	$iCnt=mysql_fetch_assoc($iCnt);
	cierraBD();

    $output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($aMembers),
        'iTotalDisplayRecords' => $iCnt['Cnt'],
        'aaData' => array()
    );
	$sectores=array("HOSTELERIA"=>"Hostelería", "COMERCIO"=>"Comercio", "TRANSPORTE"=>"Transporte", "ESTETICA"=>"Estética", "SANIDAD"=>"Sanidad", "VETERINARIA"=>"Veterinaria", "DEPORTES"=>"Deportes", "ASOCIACIONES"=>"Asociaciones", "FORMACION"=>"Formación", "INDUSTRIA"=>"Industria", "CONSTRUCCION"=>"Construcción", "INFORMATICA"=>"Informática", "SERVICIOS"=>"Servicios", "OTROS"=>"Otros", ""=>"");

	conexionBD();
	$colores=array('Sin contactar'=>'','En proceso'=>"class='registroAzul'",'Reconcertar'=>"class='registroNegro'",'Cerrada'=>"class='registroVerde'",
	'Pendiente'=>"class='registroAmarillo'",'Baja'=>"class='registroRojo'");
	$coloresTarea=array(''=>'','sincontactar'=>'','proceso'=>"class='registroAzul'",'reconcertar'=>"class='registroNegro'",'alta'=>"class='registroVerde'",
	'normal'=>"class='registroAmarillo'",'baja'=>"class='registroRojo'",'concertada'=>"class='registroVerdeClaro'",'visitado'=>"class='registroAzulClaro'");
	$textoTarea=array(''=>'','sincontactar'=>'Sin contactar','proceso'=>"En proceso",'reconcertar'=>"Reconcertar",'alta'=>"Cerrada",
	'normal'=>"Pendiente",'baja'=>"Baja",'concertada'=>'Concertada','visitado'=>'Visitado');
	$actividades = generaArrayActividades();
	while(isset($datos['codigo'])){
		$textoProductos='  ';
		$productos=consultaBD("SELECT nombreProducto FROM productos LEFT JOIN productos_clientes ON productos.codigo=productos_clientes.codigoProducto WHERE codigoCliente='".$datos['codigo']."'");
		while($datosProductos=mysql_fetch_assoc($productos)){
			$textoProductos.=$datosProductos['nombreProducto'].', ';
		}
		$textoProductos=substr($textoProductos,0,-2);
		$tarea=consultaBD("SELECT prioridad FROM tareas WHERE codigoCliente='".$datos['codigo']."' ORDER BY codigo DESC LIMIT 1",false,true);
		
		$textoConcatenado='';
		if($datos['estado']=='Pendiente'){
			$textoConcatenado=". Firmado: ".$datos['firmado'].". Pendiente de: ".$datos['pendiente'];
		}
		$botones="<div class='btn-group'>
				<button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'><i class='icon-download'></i> Opciones <span class='caret'></span></button>
				<ul class='dropdown-menu inverse' role='menu'>
					<li><a href='detallesPosibleCliente.php?codigo=".$datos['codigo']."' class='pull-left'><span class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a></span></li>
					<li><a href='creaTareaUnico.php?codigo=".$datos['codigo']."' class='pull-left'><span class='btn btn-warning'><i class='icon-plus-sign'></i> Añadir Tarea</i></a></span></li> ";
					if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ADMINISTRACION'){
						$botones.="<li><a href='creaVenta.php?codigo=".$datos['codigo']."&activa' class='pull-left'><span class='btn btn-success'><i class='icon-ok-sign'></i> Confirmar venta</i></a></span></li>";
					}
					$botones.="
					<li><a href='trabajos.php?codigoCliente=".$datos['codigo']."' class='pull-left'><span class='btn btn-warning'><i class='icon-cogs'></i> Trabajos</i></a></span></li>
					<li><a href='tareas.php?codigoCliente=".$datos['codigo']."' class='pull-left'><span class='btn btn-success'><i class='icon-calendar'></i> Tareas</i></a></span></li>
					<li><a href='incidencias.php?codigoCliente=".$datos['codigo']."' class='pull-left'><span class='btn btn-inverse'><i class='icon-exclamation'></i> Incidencias</i></a></span></li>
				</ul>
			</div>";
        $aItem = array(
            $datos['referencia'],
			$datos['empresa'],
			"<div ".$colores[$datos['estado']].">".$datos['estado'].$textoConcatenado."</div>",
			"<div ".$coloresTarea[$tarea['prioridad']].">".$textoTarea[$tarea['prioridad']]."</div>",
			$datos['cif'],
			$datos['localidad'],
			$datos['nombre'].' '.$datos['apellidos'],
			$actividades[$datos['actividad']],
			$textoProductos,
			$botones,
			"<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>",
			'DT_RowId' => $datos['codigo']
        );
		$datos=mysql_fetch_assoc($aMembers);
        $output['aaData'][] = $aItem;
	}
	cierraBD();
    /*foreach ($aMembers as $iID => $aInfo) {
		echo "ENTRA";
        $aItem = array(
            $aInfo['nombre'], $aInfo['apellidos'], $aInfo['email'], $aInfo['telefono'], $aInfo['usuario'], $aInfo['clave'], 'DT_RowId' => $aInfo['codigo']
        );
        $output['aaData'][] = $aItem;
    }*/
    echo json_encode($output);
}
