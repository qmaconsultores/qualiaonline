<?php
session_start();
include_once('../funciones.php');
compruebaSesion();



	$datos=arrayFormulario();
	$where='';
	if($datos['motivo']!='todos'){
		$where='WHERE motivo = "'.$datos['motivo'].'"';
	}
	if($datos['resolucion']!='todas'){
		if($where==''){
			$where='WHERE devoluciones_facturas.resolucionFactura = "'.$datos['resolucion'].'"';
		} else {
			$where.=' AND devoluciones_facturas.resolucionFactura = "'.$datos['resolucion'].'"';
		}	
	}
	
	if($datos['anio']=='0'){
		$fecha=date('Y');
		$whereFecha=' OR fecha LIKE "0000-00-00"';
	}else{
		$fecha=$datos['anio'];
		$whereFecha='';
	}
	
	if($datos['mes']=='0'){
		$fecha=$fecha.'-%';
	}else{
		$fecha=$fecha.'-'.$datos['mes'].'-%';
	}
	

	if($where==''){
		$where.="WHERE fecha LIKE '".$fecha."'".$whereFecha;
	} else {
		$where.=" AND fecha LIKE '".$fecha."'".$whereFecha;
	}	


	$consulta=consultaBD("SELECT SUM(coste) AS total FROM facturacion INNER JOIN devoluciones_facturas ON facturacion.codigo=devoluciones_facturas.codigoFactura $where;",true,true);
	$datos['importe']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(devoluciones_facturas.codigo) AS total FROM facturacion INNER JOIN devoluciones_facturas ON facturacion.codigo=devoluciones_facturas.codigoFactura $where;",true,true);
	$datos['total']=$consulta['total'];
?>
<div id="big_stats" class="cf">
  
  <div class="stat"> <i class="icon-reply"></i> <span class="value"><?php echo $datos['total']; ?></span> <br>Devoluciones</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['importe'], 2, ',', '');?></span> <br>Importe</div>
  <?php
  	$where='motivo='.$datos['motivo'].'&resolucion='.$datos['resolucion'].'&anio='.$datos['anio'].'&mes='.$datos['mes'];
  ?>
  <div class="stat"><a href="generaExcelDevoluciones.php?<?php echo $where;?>" class="shortcut noAjax"><i class="shortcut-icon icon-download-alt"></i><span class="shortcut-label">Descargar</span> </a></div>
  </div>
</div>