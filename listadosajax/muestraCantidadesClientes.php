
<?php
session_start();
include_once('../funciones.php');
compruebaSesion();


	$datosFormulario=arrayFormulario();

	conexionBD();
		$datos=array();
		
		$tipos=array(0=>'Sin contactar',1=>'En proceso',2=>'Pendiente',3=>'Cerrada',4=>'Reconcertar',5=>'Baja');
		for($i=0;$i<=5;$i++){
			$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes WHERE estado='".$tipos[$i]."' AND fechaRegistro LIKE'".date('Y')."-%' AND activo='SI';");
			$consulta=mysql_fetch_assoc($consulta);

			$datos[$tipos[$i].'Cartera']=$consulta['codigo'];
		}
		
		$tipos=array(0=>'Sin contactar',1=>'En proceso',2=>'Pendiente',3=>'Cerrada',4=>'Reconcertar',5=>'Baja');
		for($i=0;$i<=5;$i++){
			$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes WHERE estado='".$tipos[$i]."' AND fechaRegistro<'".date('Y')."-01-01' AND activo='SI';");
			$consulta=mysql_fetch_assoc($consulta);

			$datos[$tipos[$i].'Nuevos']=$consulta['codigo'];
		}
	

	cierraBD();

?>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-phone"></i> <span class="value"><?php echo $datos['Sin contactarCartera']; ?></span> <br>Sin contactar</div>
  <div class="stat"> <i class="icon-refresh"></i> <span class="value"><?php echo $datos['En procesoCartera']; ?></span> <br>En proceso</div>
  <div class="stat"> <i class="icon-refresh icon-spin"></i> <span class="value"><?php echo $datos['ReconcertarCartera'];?></span> <br>Reconcertar</div>
</div>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['CerradaCartera'];?></span> <br>Vendido</div>
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['PendienteCartera'];?></span> <br>Pendiente</div>
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['BajaCartera'];?></span> <br>Baja</div>
</div>
<h6 class="fondoAzul">Clientes de cartera:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-phone"></i> <span class="value"><?php echo $datos['Sin contactarNuevos']; ?></span> <br>Sin contactar</div>
  <div class="stat"> <i class="icon-refresh"></i> <span class="value"><?php echo $datos['En procesoNuevos']; ?></span> <br>En proceso</div>
  <div class="stat"> <i class="icon-refresh icon-spin"></i> <span class="value"><?php echo $datos['ReconcertarNuevos'];?></span> <br>Reconcertar</div>
</div>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['CerradaNuevos'];?></span> <br>Vendido</div>
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['PendienteNuevos'];?></span> <br>Pendiente</div>
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['BajaNuevos'];?></span> <br>Baja</div>
</div>