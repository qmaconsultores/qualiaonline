<?php
session_start();
?>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<?php
include_once('../funciones.php');
compruebaSesion();

	conexionBD();
	$datos=arrayFormulario();
	
	if($datos['comercial']=='todos'){
		$where='';
	}else{
		$where="WHERE ventas.codigoUsuario='".$datos['comercial']."'";
	}

	if($datos['comercial']=='todos'){
		if($datos['mes']=='0'){
			$where.='WHERE fecha LIKE"%'.date('Y').'-%"';
		}else{
			$where.="WHERE fecha LIKE '".date('Y')."-".$datos['mes']."-%'";
		}
	}else{
		if($datos['mes']=='0'){
			$where.='AND fecha LIKE"%'.date('Y').'-%"';
		}else{
			$where.=" AND fecha LIKE '".date('Y')."-".$datos['mes']."-%'";
		}
	}

	$consulta=consultaBD("SELECT ventas.codigo, ventas.tipo, productos.nombreProducto AS concepto, precio, fecha, observaciones, empresa, activo, clientes.codigo AS codigoCliente, CONCAT(nombre, ' ', apellidos) AS comercial, clientes.referencia FROM ventas
	LEFT JOIN clientes ON clientes.codigo=ventas.codigoCliente LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo
	LEFT JOIN productos ON ventas.concepto=productos.codigo $where ORDER BY empresa, fecha DESC;");
	
	$datos=mysql_fetch_assoc($consulta);
	echo "
		<table class='table table-striped table-bordered datatable'>
			<thead>
			  <tr>
				<th> ID Cliente </th>
				<th> Empresa </th>
				<th> Tipo de venta </th>
				<th> Comercial </th>
				<th> Concepto </th>
				<th> Fecha </th>
				<th> Importe </th>
				<th> Observaciones </th>
			  </tr>
			</thead>
			<tbody>";
			
	$destino=array('SI'=>'detallesCuenta.php', 'NO'=>'detallesPosibleCliente.php');

	while($datos!=0){
		$fecha=formateaFechaWeb($datos['fecha']);
		
		echo "
		<tr>
			<td> ".$datos['referencia']." </td>
			<td> <a href='".$destino[$datos['activo']]."?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a> </td>
        	<td> ".ucfirst($datos['tipo'])." </td>
			<td> ".$datos['comercial']." </td>
        	<td> ".$datos['concepto']." </td>
			<td> $fecha </td>
			<td> ".$datos['precio']." € </td>
			<td> ".ucfirst($datos['observaciones'])." </td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	echo "
	</tbody>
              </table>";
	cierraBD();

?>