<?php
session_start();
include_once('../funciones.php');
compruebaSesion();


	$datosFormulario=arrayFormulario();
	
		$where='';
		$whereTareas='';
		$servicios=array('formacion','lssi','alergenos','prl','plataformaWeb','auditoriaLopd','consultoriaLopd','auditoriaPrl','webLegalizada','dominioCorreo','dominio','ecommerce');

	conexionBD();
	if($datosFormulario['mes']=='0'){
		$dia=$datosFormulario['dia'];
		$datos=array();

		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha>='".date('Y')."-01-01' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalNeto']=$consulta['codigo'];
		
		$preventas=consultaBD("SELECT * FROM preventas WHERE fecha >= '".date('Y')."-01-01' $whereTareas;");

		$bruto=0;
        while($preventa=mysql_fetch_assoc($preventas)){
            for($i=0;$i<count($servicios);$i++){
                if($preventa[$servicios[$i]]=='SI'){
                    $bruto=$bruto+$preventa[$servicios[$i].'Precio'];
                }
            }
        }

		$datos['totalBruto']=$bruto;

		$consulta=consultaBD("SELECT SUM(importe) AS codigo FROM tareas WHERE fechaInicio>='".date('Y')."-01-01' AND prioridad='alta' AND codigo NOT IN (SELECT codigoTarea FROM preventas);");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalAntiguo']=$consulta['codigo'];
		
		$datos['diferencia']=($datos['totalBruto']+$datos['totalAntiguo'])-$datos['totalNeto'];
	}else{
		$dia=$datosFormulario['dia']==''?'%':$datosFormulario['dia'].'%';
		$mes=$datosFormulario['mes'];
		$datos=array();

		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha LIKE'".date('Y')."-$mes-$dia' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalNeto']=$consulta['codigo'];

		$preventas=consultaBD("SELECT * FROM preventas WHERE fecha LIKE '".date('Y')."-$mes-$dia' $whereTareas;");
		$bruto=0;
        while($preventa=mysql_fetch_assoc($preventas)){
            for($i=0;$i<count($servicios);$i++){
                if($preventa[$servicios[$i]]=='SI'){
                    $bruto=$bruto+$preventa[$servicios[$i].'Precio'];
                }
            }
        }
		$datos['totalBruto']=$bruto;

		$consulta=consultaBD("SELECT SUM(importe) AS codigo FROM tareas WHERE fechaInicio LIKE'".date('Y')."-$mes-$dia%' AND prioridad='alta' AND codigo NOT IN (SELECT codigoTarea FROM preventas);");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalAntiguo']=$consulta['codigo'];
		
		$datos['diferencia']=($datos['totalBruto']+$datos['totalAntiguo'])-$datos['totalNeto'];
		
	}

	cierraBD();

?>
<div id="big_stats" class="cf">

  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['totalAntiguo'], 2, ',', ''); ?></span> <br>Total Bruto Anterior</div>

  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['totalBruto'], 2, ',', ''); ?></span> <br>Total Bruto</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['totalNeto'], 2, ',', '');?></span> <br>Total Neto</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['diferencia'], 2, ',', '');?></span> <br>Diferencia</div>
  <!-- .stat -->
  
</div>