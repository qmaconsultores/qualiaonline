<?php
session_start();
include_once('../funciones.php');
compruebaSesion();


	$datosFormulario=arrayFormulario();

	$mes=$datosFormulario['mes'] == '0' ? '01' : $datosFormulario['mes'];
	$mesFinal=$datosFormulario['mes'] == '0' ? 12 : $datosFormulario['mes'];
	$where='';
	if($datosFormulario['comercial'] != 'NULL'){
		$where = 'AND ventas.codigoUsuario='.$datosFormulario['comercial'];
	}

	conexionBD();
		$datos=array();
		
		$consulta=consultaBD("SELECT COUNT(ventas.codigo) AS codigo FROM ventas LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo WHERE clientes.baja='NO' AND fecha>='".date('Y-'.$mes.'-01')."' AND fecha<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['numeroVentas']=$consulta['codigo'];

		$consulta=consultaBD("SELECT SUM(ventas.precio) AS codigo FROM ventas LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo WHERE clientes.baja='NO' AND fecha>='".date('Y-'.$mes.'-01')."' AND fecha<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
		$consulta=mysql_fetch_assoc($consulta);
    if($consulta['codigo'] == ''){
        $consulta['codigo'] = 0;
    }

		$datos['totalVentas']=$consulta['codigo'];
		
		
		$productos=consultaBD("SELECT * FROM productos");
		while($producto=mysql_fetch_assoc($productos)){
			$consulta=consultaBD("SELECT COUNT(ventas.codigo) AS codigo FROM ventas LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo WHERE clientes.baja='NO' AND concepto = ".$producto['codigo']." AND fecha>='".date('Y-'.$mes.'-01')."' AND fecha<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
      $consulta=mysql_fetch_assoc($consulta);

      $datos['numeroVentas-'.$producto['codigo']]=$consulta['codigo'];

      $consulta=consultaBD("SELECT SUM(ventas.precio) AS codigo FROM ventas LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo WHERE clientes.baja='NO' AND concepto = ".$producto['codigo']." AND fecha>='".date('Y-'.$mes.'-01')."' AND fecha<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
      $consulta=mysql_fetch_assoc($consulta);
      if($consulta['codigo'] == ''){
        $consulta['codigo'] = 0;
      }

      $datos['totalVentas-'.$producto['codigo']]=$consulta['codigo'];
		}

?>
<div id="big_stats" class="cf">
  
  <div class="stat" style='width:50%;'> <i class="icon-shopping-cart"></i> <span class="value"><?php echo $datos['numeroVentas']; ?></span> <br>Ventas</div>
  <!-- .stat -->
  <div class="stat" style='width:50%;'> <i class="icon-eur"></i> <span class="value"><?php echo $datos['totalVentas'];?></span> <br>Total</div>
  <!-- .stat -->
</div>
<?php
  $productos=consultaBD("SELECT * FROM productos");
  while($producto=mysql_fetch_assoc($productos)){
?>
<h6 class="fondoAzul"><?php echo $producto['nombreProducto']; ?>:</h6>
<div id="big_stats" class="cf">
   <div class="stat" style='width:50%;'> <i class="icon-shopping-cart"></i> <span class="value"><?php echo $datos['numeroVentas-'.$producto['codigo']]; ?></span> <br>Ventas</div>
  <!-- .stat -->
  <div class="stat" style='width:50%;'> <i class="icon-eur"></i> <span class="value"><?php echo $datos['totalVentas-'.$producto['codigo']];?></span> <br>Total</div>
</div>
<?php
}
  cierraBD();
?>