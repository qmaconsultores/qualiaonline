<?php
if ($_GET) {
	session_start();
    require_once('../funciones.php');
    switch ($_GET['action']) {
        case 'getMembersAjx':
            getMembersAjx();
            break;
        case 'updateMemberAjx':
            updateMemberAjx();
            break;
        case 'deleteMember':
            deleteMember();
            break;
    }
    exit;
}

function getMembersAjx() {

    // SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }

    // SQL order
    $aColumns = array('referencia', 'empresa', 'cif', 'localidad', 'nombre', 'sector', 'nombreProducto', 'fechaUltimaVenta', 'activo', 'codigoUsuario');
    $sOrder = '';
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $sOrder .= '`'.$aColumns[ (int)$_GET['iSortCol_'.$i] ].'` '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $sOrder = substr_replace($sOrder, '', -2);
        if ($sOrder == 'ORDER BY') {
            $sOrder = '';
        }
    }

    // SQL where
	$sWhere=compruebaPerfilParaWhere().' AND tieneColaborador="SI" AND comercial="'.$_GET['codigoColaborador'].'"';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
		$sWhere=compruebaPerfilParaWhere().' AND tieneColaborador="SI" AND comercial="'.$_GET['codigoColaborador'].'" AND(';
        for ($i=0; $i<count($aColumns) ; $i++) {
            if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true') {
				if($aColumns[$i]=='nombre'){
					$sWhere .= "CONCAT(`nombre`, ' ', `apellidos`) LIKE '%".$_GET['sSearch']."%' OR ";
				}else{
					$sWhere .= '`' . $aColumns[$i]."` LIKE '%".$_GET['sSearch']."%' OR ";
				}
            }
        }
        $sWhere = substr_replace( $sWhere, '', -3 );
        $sWhere .= ')';
    }

	conexionBD();
    $aMembers = consultaBD("SELECT clientes.codigo, empresa, nombreProducto, sector, usuarios.nombre, usuarios.apellidos, clientes.referencia, localidad, fechaUltimaVenta, cif FROM clientes 
	LEFT JOIN productos ON clientes.tipoServicio=productos.codigo 
	LEFT JOIN usuarios ON usuarios.codigo=clientes.codigoUsuario {$sWhere} AND activo='SI' AND baja='NO' {$sOrder} {$sLimit}");
	$datos=mysql_fetch_assoc($aMembers);
    $iCnt = consultaBD("SELECT COUNT(clientes.codigo) AS 'Cnt' FROM `clientes` LEFT JOIN productos ON clientes.tipoServicio=productos.codigo 
	LEFT JOIN usuarios ON usuarios.codigo=clientes.codigoUsuario {$sWhere} AND activo='SI' AND baja='NO'");
	$iCnt=mysql_fetch_assoc($iCnt);
	cierraBD();

    $output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($aMembers),
        'iTotalDisplayRecords' => $iCnt['Cnt'],
        'aaData' => array()
    );
	$sectores=array("HOSTELERIA"=>"Hostelería", "COMERCIO"=>"Comercio", "TRANSPORTE"=>"Transporte", "ESTETICA"=>"Estética", "SANIDAD"=>"Sanidad", "VETERINARIA"=>"Veterinaria", "DEPORTES"=>"Deportes", "ASOCIACIONES"=>"Asociaciones", "FORMACION"=>"Formación", "INDUSTRIA"=>"Industria", "CONSTRUCCION"=>"Construcción", "INFORMATICA"=>"Informática", "SERVICIOS"=>"Servicios", "OTROS"=>"Otros", ""=>"");

	conexionBD();
	while(isset($datos['codigo'])){
		$textoProductos='  ';
		$productos=consultaBD("SELECT nombreProducto FROM productos LEFT JOIN productos_clientes ON productos.codigo=productos_clientes.codigoProducto WHERE codigoCliente='".$datos['codigo']."'");
		while($datosProductos=mysql_fetch_assoc($productos)){
			$textoProductos.=$datosProductos['nombreProducto'].', ';
		}
		$textoProductos=substr($textoProductos,0,-2);
        $aItem = array(
            $datos['referencia'],
			$datos['empresa'],
			$datos['cif'],
			$datos['localidad'],
			$datos['nombre'].' '.$datos['apellidos'],
			$sectores[$datos['sector']],
			$textoProductos,
			formateaFechaWeb($datos['fechaUltimaVenta']),
			"<div class='btn-group'>
				<button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'><i class='icon-download'></i> Opciones <span class='caret'></span></button>
				<ul class='dropdown-menu inverse' role='menu'>
					<li><a href='detallesCuenta.php?codigo=".$datos['codigo']."' class='pull-left'><span class='label label-primary'><i class='icon-zoom-in'></i> Ver datos</i></a></span></li>
					<li><a href='creaTareaUnico.php?codigo=".$datos['codigo']."' class='pull-left'><span class='label label-warning'><i class='icon-plus-sign'></i> Añadir Tarea</i></a></span></li>
					<li><a href='ventas.php?codigo=".$datos['codigo']."' class='pull-left'><span class='label label-success'><i class='icon-shopping-cart'></i> Ventas realizadas</i></a></span></li>
					<li><a href='generaContrato.php?codigo=".$datos['codigo']."' class='pull-left'><span class='label label-info'><i class='icon-download'></i> Contrato</i></a></span></li>
					<li><a href='trabajos.php?codigoCliente=".$datos['codigo']."' class='pull-left'><span class='label label-warning'><i class='icon-cogs'></i> Trabajos</i></a></span></li>
					<li><a href='tareas.php?codigoCliente=".$datos['codigo']."' class='pull-left'><span class='label label-success'><i class='icon-calendar'></i> Tareas</i></a></span></li>
					<li><a href='incidencias.php?codigoCliente=".$datos['codigo']."' class='pull-left'><span class='label label-inverse'><i class='icon-exclamation'></i> Incidencias</i></a></span></li>
                    <li><a href='posiblesClientes.php?posible=".$datos['codigo']."' class='pull-left'><span class='label label-danger'><i class='icon-shopping-cart'></i> Pasar a posibles clientes</i></a></span></li>
				</ul>
			</div>",
			'DT_RowId' => $datos['codigo']
        );
		$datos=mysql_fetch_assoc($aMembers);
        $output['aaData'][] = $aItem;
	}
	cierraBD();
    /*foreach ($aMembers as $iID => $aInfo) {
		echo "ENTRA";
        $aItem = array(
            $aInfo['nombre'], $aInfo['apellidos'], $aInfo['email'], $aInfo['telefono'], $aInfo['usuario'], $aInfo['clave'], 'DT_RowId' => $aInfo['codigo']
        );
        $output['aaData'][] = $aItem;
    }*/
    echo json_encode($output);
}
