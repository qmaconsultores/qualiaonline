<?php
session_start();
include_once('../funciones.php');
compruebaSesion();


	$datosFormulario=arrayFormulario();

	$mes=$datosFormulario['mes'] == '0' ? '01' : $datosFormulario['mes'];
	$mesFinal=$datosFormulario['mes'] == '0' ? 12 : $datosFormulario['mes'];
	$where='';
	if($datosFormulario['comercial'] != 'NULL'){
		$where = 'AND codigoUsuario='.$datosFormulario['comercial'];
	}

	conexionBD();
		$datos=array();
		
		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE descripcion IN ('PRIMERA','SEGUIMIENTO','LOPD','PRL','FORMACION') AND fechaInicio>='".date('Y-'.$mes.'-01')."' AND fechaFin<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalVisitas']=$consulta['codigo'];

		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE descripcion = 'LLAMADA' AND fechaInicio>='".date('Y-'.$mes.'-01')."' AND fechaFin<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalLlamadas']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE descripcion = 'MAIL' AND fechaInicio>='".date('Y-'.$mes.'-01')."' AND fechaFin<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalMail']=$consulta['codigo'];
		
		$tipos=array(0=>'PRIMERA',1=>'SEGUIMIENTO',2=>'LOPD',3=>'PRL',4=>'FORMACION');
		for($i=0;$i<=4;$i++){
			$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE descripcion = '".$tipos[$i]."' AND fechaInicio>='".date('Y-'.$mes.'-01')."' AND fechaFin<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
      $consulta=mysql_fetch_assoc($consulta);

			$datos[$tipos[$i]]=$consulta['codigo'];
			
			$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE descripcion = '".$tipos[$i]."' AND prioridad='ALTA' AND fechaInicio>='".date('Y-'.$mes.'-01')."' AND fechaFin<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
      $consulta=mysql_fetch_assoc($consulta);

			$datos[$tipos[$i].'Vendidas']=$consulta['codigo'];
			
			$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE descripcion = '".$tipos[$i]."' AND prioridad='BAJA' AND fechaInicio>='".date('Y-'.$mes.'-01')."' AND fechaFin<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
      $consulta=mysql_fetch_assoc($consulta);

      $datos[$tipos[$i].'Bajas']=$consulta['codigo'];
			
			$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE descripcion = '".$tipos[$i]."' AND prioridad<>'ALTA' AND prioridad<>'BAJA' AND fechaInicio>='".date('Y-'.$mes.'-01')."' AND fechaFin<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
      $consulta=mysql_fetch_assoc($consulta);

      $datos[$tipos[$i].'Pendientes']=$consulta['codigo'];
		}


	cierraBD();

?>
<div id="big_stats" class="cf">
  
  <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $datos['totalVisitas']; ?></span> <br>Visitas</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-phone"></i> <span class="value"><?php echo $datos['totalLlamadas'];?></span> <br>Llamadas</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-envelope"></i> <span class="value"><?php echo $datos['totalMail'];?></span> <br>Mails</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">Primera Visita:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $datos['PRIMERA']; ?></span> <br>Totales</div>
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['PRIMERAVendidas']; ?></span> <br>Vendidas</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['PRIMERAPendientes'];?></span> <br>Pendientes</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['PRIMERABajas'];?></span> <br>Bajas</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">Visita seguimiento:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $datos['SEGUIMIENTO']; ?></span> <br>Totales</div>
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['SEGUIMIENTOVendidas']; ?></span> <br>Vendidas</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['SEGUIMIENTOPendientes'];?></span> <br>Pendientes</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['SEGUIMIENTOBajas'];?></span> <br>Bajas</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">Auditoría LOPD:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $datos['LOPD']; ?></span> <br>Totales</div>
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['LOPDVendidas']; ?></span> <br>Vendidas</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['LOPDPendientes'];?></span> <br>Pendientes</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['LOPDBajas'];?></span> <br>Bajas</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">Auditoría PRL:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $datos['PRL']; ?></span> <br>Totales</div>
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['PRLVendidas']; ?></span> <br>Vendidas</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['PRLPendientes'];?></span> <br>Pendientes</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['PRLBajas'];?></span> <br>Bajas</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">Visita formación:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $datos['FORMACION']; ?></span> <br>Totales</div>
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['FORMACIONVendidas']; ?></span> <br>Vendidas</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['FORMACIONPendientes'];?></span> <br>Pendientes</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['FORMACIONBajas'];?></span> <br>Bajas</div>
  <!-- .stat -->
</div>
