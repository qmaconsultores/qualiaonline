<?php
	session_start();
	include_once('../funciones.php');
	compruebaSesion();
	
	$datos=arrayFormulario();

	$consulta=consultaBD("SELECT lunes,martes,miercoles,jueves,viernes,sabado,domingo,horaInicio,horaFin,horaInicioTarde,horaFinTarde,horasTutoria FROM tutores WHERE codigo='".$datos['codigoTutor']."';",true);
	$datosTutor=mysql_fetch_assoc($consulta);
	
?>

	<div class="control-group">                     
	  <label class="control-label" for="horario">Horario mañana:</label>
	  <div class="controls">
		<input type="text" class="input-small" id="horaInicioFormacionDistancia" name="horaInicioFormacionDistancia" value="<?php echo $datosTutor['horaInicio']; ?>">
					 a 
					<input type="text" class="input-small" id="horaFinFormacionDistancia" name="horaFinFormacionDistancia" value="<?php echo $datosTutor['horaFin']; ?>">
					 (hh:mm) Comprendido entre 00:01h. y 15:00h.
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->
	
	<div class="control-group">                     
	  <label class="control-label" for="horario">Horario tarde:</label>
	  <div class="controls">
		<input type="text" class="input-small" id="horaInicioFormacionTardeDistancia" name="horaInicioFormacionTardeDistancia" value="<?php echo $datosTutor['horaInicioTarde']; ?>">
					 a 
					<input type="text" class="input-small" id="horaFinFormacionTardeDistancia" name="horaFinFormacionTardeDistancia" value="<?php echo $datosTutor['horaFinTarde']; ?>">
					 (hh:mm) Comprendido entre 15:01h. y 00:00h.
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->
	
	<div class="control-group">                     
	  <label class="control-label" for="horasTutoria">Horas formación:</label>
	  <div class="controls">
		<input type="text" class="input-small" id="horasFormacionDistancia" name="horasFormacionDistancia" value="<?php echo $datosTutor['horasTutoria']; ?>">
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->

	<div class="control-group">                     
	  <label class="control-label" for="horas">Días impartición:</label>
	  <div class="controls">
		<table class="mitadAncho">
		  <tr>
			<th>L</th>
			<th>M</th>
			<th>X</th>
			<th>J</th>
			<th>V</th>
			<th>S</th>
			<th>D</th>
		  </tr>
		  <tr>
			<th><input type="checkbox" id="lunesFormacionDistancia" name="lunesFormacionDistancia" <?php if($datosTutor['lunes']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="martesFormacionDistancia" name="martesFormacionDistancia" <?php if($datosTutor['martes']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="miercolesFormacionDistancia" name="miercolesFormacionDistancia" <?php if($datosTutor['miercoles']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="juevesFormacionDistancia" name="juevesFormacionDistancia" <?php if($datosTutor['jueves']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="viernesFormacionDistancia" name="viernesFormacionDistancia" <?php if($datosTutor['viernes']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="sabadoFormacionDistancia" name="sabadoFormacionDistancia" <?php if($datosTutor['sabado']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="domingoFormacionDistancia" name="domingoFormacionDistancia" <?php if($datosTutor['domingo']=='SI'){ echo "checked='checked'";} ?>></th>
		  </tr>
		</table>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->