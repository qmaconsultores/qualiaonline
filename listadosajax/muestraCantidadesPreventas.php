<?php
session_start();
include_once('../funciones.php');
compruebaSesion();


	$datos=arrayFormulario();

	if($datos['mes']!=0){
		if($datos['anio']!=0){
			$anio=$datos['anio'];
		} else {
			$anio=date('Y');
		}
		$where='WHERE fecha LIKE "'.$anio.'-'.$datos['mes'].'-%"';
		$where2='AND fecha LIKE "'.$anio.'-'.$datos['mes'].'-%"';
	} else {
		if($datos['anio']!=0){
			$anio=$datos['anio'];
		} else {
			$anio=date('Y');
		}
		$where='WHERE fecha LIKE "'.$anio.'-%-%"';
		$where2='AND fecha LIKE "'.$anio.'-%-%"';
	}
	$servicios=array('formacion','lssi','alergenos','prl','plataformaWeb','auditoriaLopd','consultoriaLopd','auditoriaPrl','webLegalizada','dominioCorreo','dominio','ecommerce');
	conexionBD();
	$res=array();
	$consulta=consultaBD('SELECT COUNT(codigo) AS codigo FROM preventas '.$where,false, true);
	$res['total']=$consulta['codigo'];
	$consulta=consultaBD('SELECT * FROM preventas '.$where);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
        for($i=0;$i<count($servicios);$i++){
           	if($preventa[$servicios[$i]]=='SI'){
                $total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
   	}
   	$res['totalImporte']=number_format((float)$total, 2, ',', '').' €';

   	$consulta=consultaBD('SELECT COUNT(codigo) AS codigo FROM preventas WHERE documentos="NO" AND aceptado="SINEVALUAR" '.$where2,false, true);
	$res['noDoc']=$consulta['codigo'];
	$consulta=consultaBD('SELECT * FROM preventas WHERE documentos="NO" AND aceptado="SINEVALUAR" '.$where2);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
        for($i=0;$i<count($servicios);$i++){
           	if($preventa[$servicios[$i]]=='SI'){
                $total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
   	}
   	$res['noDocImporte']=number_format((float)$total, 2, ',', '').' €';

   	$consulta=consultaBD('SELECT COUNT(codigo) AS codigo FROM preventas WHERE documentos="SI" '.$where2,false, true);
	$res['siDoc']=$consulta['codigo'];
	$consulta=consultaBD('SELECT * FROM preventas WHERE documentos="SI" '.$where2);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
        for($i=0;$i<count($servicios);$i++){
           	if($preventa[$servicios[$i]]=='SI'){
                $total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
   	}
   	$res['siDocImporte']=number_format((float)$total, 2, ',', '').' €';

   	$consulta=consultaBD('SELECT COUNT(codigo) AS codigo FROM preventas WHERE documentos="SI" AND aceptado="NO" '.$where2,false, true);
	$res['noAceptado']=$consulta['codigo'];
	$consulta=consultaBD('SELECT * FROM preventas WHERE documentos="SI" AND aceptado="NO" '.$where2);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
        for($i=0;$i<count($servicios);$i++){
           	if($preventa[$servicios[$i]]=='SI'){
                $total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
   	}
   	$res['noAceptadoImporte']=number_format((float)$total, 2, ',', '').' €';

   	$consulta=consultaBD('SELECT COUNT(codigo) AS codigo FROM preventas WHERE documentos="SI" AND verificada="NO" AND aceptado!="NO" '.$where2,false, true);
	$res['noVerificada']=$consulta['codigo'];
	$consulta=consultaBD('SELECT * FROM preventas WHERE documentos="SI" AND verificada="NO" AND aceptado!="NO" '.$where2);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
        for($i=0;$i<count($servicios);$i++){
           	if($preventa[$servicios[$i]]=='SI'){
                $total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
   	}
   	$res['noVerificadaImporte']=number_format((float)$total, 2, ',', '').' €';

   	$consulta=consultaBD('SELECT COUNT(codigo) AS codigo FROM preventas WHERE documentos="SI" AND verificada="SI" '.$where2,false, true);
	$res['siVerificada']=$consulta['codigo'];
	$consulta=consultaBD('SELECT * FROM preventas WHERE documentos="SI" AND verificada="SI" '.$where2);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
        for($i=0;$i<count($servicios);$i++){
           	if($preventa[$servicios[$i]]=='SI'){
                $total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
   	}
   	$res['siVerificadaImporte']=number_format((float)$total, 2, ',', '').' €';

   	$consulta=consultaBD('SELECT COUNT(codigo) AS codigo FROM preventas WHERE documentos="SI" AND verificada="SI" AND aceptado="SINEVALUAR" '.$where2,false, true);
	$res['sinEvaluar']=$consulta['codigo'];
	$consulta=consultaBD('SELECT * FROM preventas WHERE documentos="SI" AND verificada="SI" AND aceptado="SINEVALUAR" '.$where2);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
        for($i=0;$i<count($servicios);$i++){
           	if($preventa[$servicios[$i]]=='SI'){
                $total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
   	}
   	$res['sinEvaluarImporte']=number_format((float)$total, 2, ',', '').' €';

   	$consulta=consultaBD('SELECT COUNT(codigo) AS codigo FROM preventas WHERE documentos="SI" AND verificada="SI" AND aceptado="SI" '.$where2,false, true);
	$res['aceptado']=$consulta['codigo'];
	$consulta=consultaBD('SELECT * FROM preventas WHERE documentos="SI" AND verificada="SI" AND aceptado="SI" '.$where2);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
        for($i=0;$i<count($servicios);$i++){
           	if($preventa[$servicios[$i]]=='SI'){
                $total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
   	}
   	$res['aceptadoImporte']=number_format((float)$total, 2, ',', '').' €';

?>
<table class='tablaPreventas'>
<tr class='padre'>
	<td><i class='icon-tags'></i></td>
	<td colspan='4'>TOTAL PREVENTA</td>
	<td class='cantidad'><?php echo $res['total']; ?></td>
	<td class='cantidad'><?php echo $res['totalImporte']; ?></td>
</tr>
<tr>
	<td class='sinBorde'></td>
	<td><i class='icon-file-o'></i></td>	
	<td colspan='3'>SIN DOCUMENTOS</td>
	<td class='cantidad'><?php echo $res['noDoc']; ?></td>
	<td class='cantidad'><?php echo $res['noDocImporte']; ?></td>
</tr>
<tr class='padre'>
	<td class='sinBorde'></td>
	<td><i class='icon-file-text'></i></td>
	<td colspan='3'>CON DOCUMENTOS</td>
	<td class='cantidad'><?php echo $res['siDoc']; ?></td>
	<td class='cantidad'><?php echo $res['siDocImporte']; ?></td>
</tr>
<tr>
	<td class='sinBorde'></td>
	<td class='sinBorde'></td>
	<td><i class='icon-remove'></i></td>
	<td colspan='2'>RECHAZADAS</td>
	<td class='cantidad'><?php echo $res['noAceptado']; ?></td>
	<td class='cantidad'><?php echo $res['noAceptadoImporte']; ?></td>
</tr>
<tr>
	<td class='sinBorde'></td>
	<td class='sinBorde'></td>
	<td><i class='icon-check-empty'></i></td>
	<td colspan='2'>NO VERIFICADAS</td>
	<td class='cantidad'><?php echo $res['noVerificada']; ?></td>
	<td class='cantidad'><?php echo $res['noVerificadaImporte']; ?></td>
</tr>
<tr class='padre'>
	<td class='sinBorde'></td>
	<td class='sinBorde'></td>
	<td><i class='icon-check'></i></td>
	<td colspan='2'>VERIFICADAS</td>
	<td class='cantidad'><?php echo $res['siVerificada']; ?></td>
	<td class='cantidad'><?php echo $res['siVerificadaImporte']; ?></td>
</tr>
<tr>
	<td class='sinBorde'></td>
	<td class='sinBorde'></td>
	<td class='sinBorde'></td>
	<td><i class='icon-ban'></i></td>
	<td>NO FACTURADO</td>
	<td class='cantidad'><?php echo $res['sinEvaluar']; ?></td>
	<td class='cantidad'><?php echo $res['sinEvaluarImporte']; ?></td>
</tr>
<tr>
	<td class='sinBorde'></td>
	<td class='sinBorde'></td>
	<td class='sinBorde'></td>
	<td><i class='icon-eur'></i></td>
	<td>FACTURADO</td>
	<td class='cantidad'><?php echo $res['aceptado']; ?></td>
	<td class='cantidad'><?php echo $res['aceptadoImporte']; ?></td>
</tr>
</table>
<?php

  cierraBD();
?>