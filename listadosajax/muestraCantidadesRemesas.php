<?php
session_start();
include_once('../funciones.php');
compruebaSesion();



	$datos=arrayFormulario();
	$where='';
	$where='';
	if($datos['tipo']!='todos'){
		$where='WHERE tipoSepa = "'.$datos['tipo'].'"';
	}

	if($datos['firma']!='todos'){
		if($where==''){
			$where='WHERE codigoFirma = "'.$datos['firma'].'"';
		} else {
			$where.=' AND codigoFirma = "'.$datos['firma'].'"';
		}	
	}

	
	if($datos['banco']!='todos'){
		if($where==''){
			$where='WHERE banco = "'.$datos['banco'].'"';
		} else {
			$where.=' AND banco = "'.$datos['banco'].'"';
		}	
	}
	
	if($datos['anio']=='0'){
		$fecha=date('Y');
	}else{
		$fecha=$datos['anio'];
	}
	
	if($datos['mes']=='0'){
		$fecha=$fecha.'-%';
	}else{
		$fecha=$fecha.'-'.$datos['mes'].'-%';
	}
	

	if($where==''){
		$where.="WHERE fecha LIKE '".$fecha."'";
	} else {
		$where.=" AND fecha LIKE '".$fecha."'";
	}	


	$consulta=consultaBD("SELECT SUM(importe) AS total FROM remesas $where;",true,true);
	$datos['importe']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM remesas $where;",true,true);
	$datos['total']=$consulta['total'];
?>
<div id="big_stats" class="cf">
  
  <div class="stat" style="width: 50%"> <i class="icon-file"></i> <span class="value"><?php echo $datos['total']; ?></span> <br>Remesas</div>
  <!-- .stat -->
  <div class="stat"  style="width: 50%"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['importe'], 2, ',', '');?></span> <br>Importe</div>
  </div>
</div>