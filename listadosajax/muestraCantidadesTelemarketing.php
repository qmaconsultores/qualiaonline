
<?php
session_start();
include_once('../funciones.php');
compruebaSesion();


	$datosFormulario=arrayFormulario();
	
	if($datosFormulario['comercial']=='todos'){	
		$where='';
	}else{
		$where="AND codigoUsuario='".$datosFormulario['comercial']."'";
	}

	conexionBD();
	if($datosFormulario['mes']=='0'){
		$datos=array();

		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE fechaInicio LIKE'%".date('Y')."-%' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalVisitas']=$consulta['codigo'];

		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE prioridad='NORMAL' AND fechaInicio LIKE'%".date('Y')."-%' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalPendientes']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE prioridad='RECONCERTAR' AND fechaInicio LIKE'%".date('Y')."-%' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalReconcertadas']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE 1=1 AND fecha LIKE'%".date('Y')."-%' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalVendido']=$consulta['codigo'];
	}else{
		$datos=array();
		$mes=$datosFormulario['mes'];
		$dia=$datosFormulario['dia'];
		
		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE fechaInicio LIKE'".date('Y-'.$mes.'-'.$dia)."%' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalVisitas']=$consulta['codigo'];

		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE prioridad='NORMAL' AND fechaInicio LIKE'".date('Y-'.$mes.'-'.$dia)."%' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalPendientes']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tareas WHERE prioridad='RECONCERTAR' AND fechaInicio LIKE'".date('Y-'.$mes.'-'.$dia)."%' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalReconcertadas']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha LIKE'".date('Y-'.$mes.'-'.$dia)."%' $where;");
		$consulta=mysql_fetch_assoc($consulta);
		$datos['totalVendido']=$consulta['codigo'];
	}

	cierraBD();

?>
<div id="big_stats" class="cf">
  
  <div class="stat"> <i class="icon-calendar"></i> <span class="value"><?php echo $datos['totalVisitas']; ?></span> <br>Visitas registradas</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['totalPendientes'];?></span> <br>Visitas pendientes</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-refresh icon-spin"></i> <span class="value"><?php echo $datos['totalReconcertadas'];?></span> <br>Visitas reconcertadas</div>
  <!-- .stat -->
  <?php if($_SESSION['codigoS']=='19' || $_SESSION['codigoS']=='21'){ ?>
	  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['totalVendido'], 2, ',', '');?></span> <br>Importe vendido</div>
	  <!-- .stat -->
  <?php } ?>
</div>