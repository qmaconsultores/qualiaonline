<?php
if ($_GET) {
	session_start();
    require_once('../funciones.php');
    switch ($_GET['action']) {
        case 'getMembersAjx':
            getMembersAjx();
            break;
        case 'updateMemberAjx':
            updateMemberAjx();
            break;
        case 'deleteMember':
            deleteMember();
            break;
    }
    exit;
}

function getMembersAjx() {

    // SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }

    // SQL order
    $aColumns = array('referencia', 'proyecto', 'empresa', 'estado', 'fechaValidez', 'formacion', 'prl', 'lopd', 'lssi', 'aler', 'web', 'auditoria1', 'auditoria2');
    $sOrder = '';
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $sOrder .= '`'.$aColumns[ (int)$_GET['iSortCol_'.$i] ].'` '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $sOrder = substr_replace($sOrder, '', -2);
        if ($sOrder == 'ORDER BY') {
            $sOrder = '';
        }
    }

    // SQL where
	$sWhere='HAVING 1=1';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
		$sWhere=$sWhere.' AND(';
        for ($i=0; $i<count($aColumns) ; $i++) {
            if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true') {
				if($aColumns[$i]=='fechaValidez'){
					$sWhere .= "DATE_FORMAT(fechaValidez,'%d/%m/%Y') LIKE '%".$_GET['sSearch']."%' OR ";
				}
				else{
					$sWhere .= '`' . $aColumns[$i]."` LIKE '%".$_GET['sSearch']."%' OR ";
				}
            }
        }
        $sWhere = substr_replace( $sWhere, '', -3 );
		$sWhere .= ')';	
    }
	if(isset($_GET['sSearch_0'])){
		$sWhere=$sWhere.' AND(2=2 AND ';
		for ($i=0; $i<count($aColumns) ; $i++) {
			if(isset($_GET['sSearch_'.$i]) && $_GET['sSearch_'.$i]!=''){//Búsqueda por columnas (nótese el índice _$i)
				if(strpos($aColumns[$i],'fecha')!==false){
					$sWhere .= "DATE_FORMAT(".$aColumns[$i].",'%d/%m/%Y') LIKE '%".$_GET['sSearch_'.$i]."%' AND ";
				}
				else{
					$sWhere.=$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' AND ";//La búsqueda por columnas es con AND, para que los filtros sean acumulativos
				}
			}
		}
		$sWhere = substr_replace( $sWhere, '', -4 );
		$sWhere .= ')';	
	}

	conexionBD();
	$anio=$_GET['anio'];
    $aMembers = consultaBD("SELECT trabajos.codigo, ordenTrabajo, proyecto, trabajos.estado, empresa, clientes.codigo AS codigoCliente, clientes.referencia, trabajos.fechaValidez, formacion, prl, lopd, lssi, aler, trabajos.web, auditoria1, auditoria2 FROM trabajos 
	INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo {$sWhere} AND fechaValidez LIKE'$anio%' {$sOrder} {$sLimit}");
	$datos=mysql_fetch_assoc($aMembers);
    $iCnt = consultaBD("SELECT trabajos.codigo, ordenTrabajo, proyecto, trabajos.estado, empresa, clientes.codigo AS codigoCliente, clientes.referencia, trabajos.fechaValidez, formacion, prl, lopd, lssi, aler, trabajos.web, auditoria1, auditoria2 FROM trabajos 
	INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo {$sWhere} AND fechaValidez LIKE'$anio%'");
	cierraBD();

    $output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($aMembers),
        'iTotalDisplayRecords' => mysql_num_rows($iCnt),
        'aaData' => array()
    );
	conexionBD();
	$estado=array('NO'=>"<span class='label'>No tiene</span>",'HECHO'=>"<span class='label label-danger'>No hecho</span>",'PROCESO'=>"<span class='label label-warning'>En proceso</span>",'FINALIZADO'=>"<span class='label label-success'>Finalizado</span>");
	while(isset($datos['codigo'])){
		$botones='';
		if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){
			$botones="<a href='detallesTrabajo.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i></i></a>";
		}
		$icono='';
		if($datos['fechaValidez']<date('Y-m-d') && $datos['fechaValidez']!='0000-00-00' && $datos['estado']!='ENTREGADO'){
			$icono=' <span class="label label-danger"><i class="icon-flag"></i></span>';
		}
        $aItem = array(
            $datos['referencia'],
        	$datos['proyecto'],
        	"<a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a> </td>",
        	$datos['estado'].'<br/>'.$botones,
			formateaFechaWeb($datos['fechaValidez']).$icono,
			$estado[$datos['formacion']],
			$estado[$datos['prl']],
			$estado[$datos['lopd']],
			$estado[$datos['lssi']],
			$estado[$datos['aler']],
			$estado[$datos['web']],
            $estado[$datos['auditoria1']],
            $estado[$datos['auditoria2']],
			'DT_RowId' => $datos['codigo']
        );
		$datos=mysql_fetch_assoc($aMembers);
        $output['aaData'][] = $aItem;
	}
	cierraBD();
    /*foreach ($aMembers as $iID => $aInfo) {
		echo "ENTRA";
        $aItem = array(
            $aInfo['nombre'], $aInfo['apellidos'], $aInfo['email'], $aInfo['telefono'], $aInfo['usuario'], $aInfo['clave'], 'DT_RowId' => $aInfo['codigo']
        );
        $output['aaData'][] = $aItem;
    }*/
    echo json_encode($output);
}
