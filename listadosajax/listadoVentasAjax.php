<?php
if ($_GET) {
	session_start();
    require_once('../funciones.php');
    switch ($_GET['action']) {
        case 'getMembersAjx':
            getMembersAjx();
            break;
        case 'updateMemberAjx':
            updateMemberAjx();
            break;
        case 'deleteMember':
            deleteMember();
            break;
    }
    exit;
}

function getMembersAjx() {

    // SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }

    // SQL order
    $aColumns = array('empresa', 'tipo', 'comercial', 'nombreProducto', 'fecha', 'precio', 'observaciones', 'tipoVentaCarteraNueva', 'codigoCliente', 'codigoUsuario');
    $sOrder = '';
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $sOrder .= '`'.$aColumns[ (int)$_GET['iSortCol_'.$i] ].'` '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $sOrder = substr_replace($sOrder, '', -2);
        if ($sOrder == 'ORDER BY') {
            $sOrder = '';
        }
    }

    // SQL where
	$sWhere='HAVING 1=1';
	if($_SESSION['tipoUsuario']!='SUPERVISOR'){
		$sWhere=compruebaPerfilParaWhereHaving('ventas.codigoUsuario');
	}else{
		$sWhere="WHERE fecha>='2015-03-01' AND fecha<='2015-12-31'";
	}	
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
		$sWhere="HAVING 1=1 AND(";
		if($_SESSION['tipoUsuario']!='SUPERVISOR'){
			$sWhere=compruebaPerfilParaWhereHAVING('ventas.codigoUsuario').' AND(';		
		}else{
			$sWhere="WHERE fecha>='2015-03-01' AND fecha<='2015-12-31'";
		}
        for ($i=0; $i<count($aColumns) ; $i++) {
            if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true') {
				if($aColumns[$i]=='codigoUsuario' || $aColumns[$i]=='tipo'){
					$sWhere .='CONVERT(CAST(CONVERT(ventas.'.$aColumns[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch']."%' USING latin1) AS binary) USING utf8) OR ";
					//$sWhere .= 'ventas.' . $aColumns[$i]." LIKE '%".$_GET['sSearch']."%' OR ";
				}elseif(strpos($aColumns[$i],'fecha')!==false){
					$sWhere .= "DATE_FORMAT(".$aColumns[$i].",'%d/%m/%Y') LIKE '%".$_GET['sSearch']."%' OR ";					
				}else{
					$sWhere .='CONVERT(CAST(CONVERT('.$aColumns[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch']."%' USING latin1) AS binary) USING utf8) OR ";
					//$sWhere .= '`' . $aColumns[$i]."` LIKE '%".$_GET['sSearch']."%' OR ";
				}
            }
        }
        $sWhere = substr_replace( $sWhere, '', -3 );
        $sWhere .= ')';
    }
	
	$dia=date('d');
	$mes=date('m');
	if($dia>=24 && $mes=='12'){
		$anio=date('Y');
		$fecha='AND fecha>="'.$anio.'-'.$mes.'-'.$dia.'"';
	}else{
		$anio=date('Y')-1;
		$fecha='AND fecha>="'.$anio.'-12-24"';
	}

	conexionBD();
    $aMembers = consultaBD("SELECT ventas.codigo, ventas.tipo, concepto, precio, fecha, observaciones, empresa, activo, clientes.codigo AS codigoCliente, CONCAT(nombre, ' ', apellidos) AS comercial, referencia, ventas.codigoUsuario, clientes.baja, productos.nombreProducto, tipoVentaCarteraNueva FROM ventas
	INNER JOIN clientes ON clientes.codigo=ventas.codigoCliente LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo
	LEFT JOIN productos ON ventas.concepto=productos.codigo {$sWhere} AND clientes.baja='NO' $fecha {$sOrder} {$sLimit}");
	$datos=mysql_fetch_assoc($aMembers);
	$contador = consultaBD("SELECT ventas.codigo, ventas.tipo, concepto, precio, fecha, observaciones, empresa, activo, clientes.codigo AS codigoCliente, CONCAT(nombre, ' ', apellidos) AS comercial, ventas.codigoUsuario, clientes.baja, productos.nombreProducto, tipoVentaCarteraNueva FROM ventas
	INNER JOIN clientes ON clientes.codigo=ventas.codigoCliente LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo 
	LEFT JOIN productos ON ventas.concepto=productos.codigo {$sWhere} AND clientes.baja='NO' $fecha");
	cierraBD();

    $output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($aMembers),
        'iTotalDisplayRecords' => mysql_num_rows($contador),
        'aaData' => array()
    );
	
	$destino=array('SI'=>'detallesCuenta.php', 'NO'=>'detallesPosibleCliente.php');
	
	while(isset($datos['codigo'])){
		$fecha=formateaFechaWeb($datos['fecha']);
        $aItem = array(
            $datos['referencia'],
            "<a href='".$destino[$datos['activo']]."?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a>",
        	ucfirst($datos['tipo']),
			$datos['comercial'],
        	$datos['nombreProducto'],
			$fecha,
			$datos['precio']." €",
			$datos['tipoVentaCarteraNueva'],
        	"<td class='centro'>
        		<a href='detallesVentaGeneral.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
        	</td>",
			"<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>",
			'DT_RowId' => $datos['codigo']
        );
		$datos=mysql_fetch_assoc($aMembers);
        $output['aaData'][] = $aItem;
	}
    /*foreach ($aMembers as $iID => $aInfo) {
		echo "ENTRA";
        $aItem = array(
            $aInfo['nombre'], $aInfo['apellidos'], $aInfo['email'], $aInfo['telefono'], $aInfo['usuario'], $aInfo['clave'], 'DT_RowId' => $aInfo['codigo']
        );
        $output['aaData'][] = $aItem;
    }*/
    echo json_encode($output);
}
