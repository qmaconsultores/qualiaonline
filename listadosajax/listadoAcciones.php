<?php
	session_start();
	include_once('../funciones.php');
	compruebaSesion();
	
	$where=compruebaPerfilParaWhere();

	echo "<select id='accionFormativa' name='accionFormativa' class='selectpicker show-tick anchoAuto' data-live-search='true'>";
	conexionBD();
	$consulta=consultaBD("SELECT codigo, codigoInterno, denominacion, modalidad, horas FROM accionFormativa $where ORDER BY codigo DESC;");
	cierraBD();
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=false){
		echo "<option value='".$datos['codigoInterno']."'";

		$tipo=array("PRESENCIAL"=>"Presencial", "DISTANCIA"=>"A distancia", "MIXTA"=>"Mixta", "TELE"=>"Teleformación", "WEBINAR"=>"Webinar");
		echo ">".$datos['codigoInterno']." - ".$datos['denominacion']." (".$tipo[$datos['modalidad']].")</option>";
		$datos=mysql_fetch_assoc($consulta);
	}
	echo "</select>
	<button type='button' class='add-on btn-primary botones' id='anadirAccion'> + </button>";
	creaInputsOcultos();
?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
	var texto=$("#accionFormativa option:selected").text();
	if(texto.indexOf("Presencial")>=0){
		$('#centroPresencial').css('display','block')
		$('#centroDistancia').css('display','none');
		$('#tipoFormacion').val("PRESENCIAL");
	}else if(texto.indexOf("A distancia")>=0){
		$('#centroPresencial').css('display','none');
		$('#centroDistancia').css('display','block');
		$('#tipoFormacion').val("DISTANCIA");
	}else if(texto.indexOf("Mixta")>=0){
		$('#centroPresencial').css('display','block');
		$('#centroDistancia').css('display','block');
		$('#tipoFormacion').val("MIXTA");
	}else if(texto.indexOf("Teleforma")>=0){
		$('#centroPresencial').css('display','block');
		$('#centroDistancia').css('display','none');
		$('#tipoFormacion').val("TELEFORMA");
	}else if(texto.indexOf("Webinar")>=0){
		$('#centroPresencial').css('display','block');
		$('#centroDistancia').css('display','none');
		$('#tipoFormacion').val("WEBINAR");
	  }
		var codigo=$('#accionFormativa option:selected').val();
		var horas='#horas-'+codigo;
		var grupo='#grupo-'+codigo;
		$('#horasTutoria').val($(horas).val());
		$('#horasFormacion').val($(horas).val());
		$('#horasFormacionDistancia').val($(horas).val());
		$('#codigoInterno').val($(grupo).text());
	$('#accionFormativa').change(function(){	
		$('#codigoAccionFormativa').val($(this).val());
		var codigo=$('#accionFormativa option:selected').val();
		var horas='#horas-'+codigo;
		var grupo='#grupo-'+codigo;
		$('#horasTutoria').val($(horas).val());
		$('#horasFormacion').val($(horas).val());
		$('#horasFormacionDistancia').val($(horas).val());
		$('#codigoInterno').val($(grupo).text());
		var texto=$("#accionFormativa option:selected").text();
       if(texto.indexOf("Presencial")>=0){
			$('#centroPresencial').css('display','block')
			$('#centroDistancia').css('display','none');
			$('#tipoFormacion').val("PRESENCIAL");
		}else if(texto.indexOf("A distancia")>=0){
			$('#centroPresencial').css('display','none');
			$('#centroDistancia').css('display','block');
			$('#tipoFormacion').val("DISTANCIA");
		}else if(texto.indexOf("Mixta")>=0){
			$('#centroPresencial').css('display','block');
			$('#centroDistancia').css('display','block');
			$('#tipoFormacion').val("MIXTA");
		}else if(texto.indexOf("Teleforma")>=0){
			$('#centroPresencial').css('display','block');
			$('#centroDistancia').css('display','none');
			$('#tipoFormacion').val("TELEFORMA");
		}else if(texto.indexOf("Webinar")>=0){
			$('#centroPresencial').css('display','block');
			$('#centroDistancia').css('display','none');
			$('#tipoFormacion').val("WEBINAR");
		  }
    });
	$('#codigoAccionFormativa').val($('#accionFormativa').val());
	$('#anadirAccion').click(function(){
		abreVentana('ventanaFlotanteDos');
	});
   });
</script>