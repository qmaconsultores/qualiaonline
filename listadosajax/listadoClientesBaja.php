<?php
if ($_GET) {
	session_start();
    require_once('../funciones.php');
    switch ($_GET['action']) {
        case 'getMembersAjx':
            getMembersAjx();
            break;
        case 'updateMemberAjx':
            updateMemberAjx();
            break;
        case 'deleteMember':
            deleteMember();
            break;
    }
    exit;
}

function getMembersAjx() {

    // SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }

    // SQL order
    $aColumns = array('referencia', 'empresa', 'cif', 'localidad', 'nombre', 'sector', 'nombreProducto', 'fechaBaja', 'motivoBaja', 'activo', 'codigoUsuario');
    $sOrder = '';
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $sOrder .= '`'.$aColumns[ (int)$_GET['iSortCol_'.$i] ].'` '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $sOrder = substr_replace($sOrder, '', -2);
        if ($sOrder == 'ORDER BY') {
            $sOrder = '';
        }
    }

    // SQL where
	$sWhere="WHERE 1=1";
	if($_SESSION['tipoUsuario']!='FORMACION'){
		$sWhere=compruebaPerfilParaWhere();
	}
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
		$sWhere="WHERE 1=1 AND(";
		if($_SESSION['tipoUsuario']!='FORMACION'){
			$sWhere=compruebaPerfilParaWhere().' AND(';
		}
        for ($i=0; $i<count($aColumns) ; $i++) {
            if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true') {
				if($aColumns[$i]=='nombre'){
					$sWhere .= "CONCAT(`nombre`, ' ', `apellidos`) LIKE '%".$_GET['sSearch']."%' OR ";
				}else{
					$sWhere .= '`' . $aColumns[$i]."` LIKE '%".$_GET['sSearch']."%' OR ";
				}
            }
        }
        $sWhere = substr_replace( $sWhere, '', -3 );
        $sWhere .= ')';
    }

	conexionBD();
    $aMembers = consultaBD("SELECT clientes.codigo, empresa, nombreProducto, sector, usuarios.nombre, usuarios.apellidos, clientes.referencia, localidad, fechaBaja, cif, motivoBaja, provincia FROM clientes 
	LEFT JOIN productos ON clientes.tipoServicio=productos.codigo 
	LEFT JOIN usuarios ON usuarios.codigo=clientes.codigoUsuario {$sWhere} AND baja='SI' {$sOrder} {$sLimit}");
	$datos=mysql_fetch_assoc($aMembers);
    $iCnt = consultaBD("SELECT COUNT(clientes.codigo) AS 'Cnt' FROM `clientes` LEFT JOIN productos ON clientes.tipoServicio=productos.codigo 
	LEFT JOIN usuarios ON usuarios.codigo=clientes.codigoUsuario {$sWhere} AND baja='SI'");
	$iCnt=mysql_fetch_assoc($iCnt);
	cierraBD();

    $output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($aMembers),
        'iTotalDisplayRecords' => $iCnt['Cnt'],
        'aaData' => array()
    );
	$sectores=array("HOSTELERIA"=>"Hostelería", "COMERCIO"=>"Comercio", "TRANSPORTE"=>"Transporte", "ESTETICA"=>"Estética", "SANIDAD"=>"Sanidad", "VETERINARIA"=>"Veterinaria", "DEPORTES"=>"Deportes", "ASOCIACIONES"=>"Asociaciones", "FORMACION"=>"Formación", "INDUSTRIA"=>"Industria", "CONSTRUCCION"=>"Construcción", "INFORMATICA"=>"Informática", "SERVICIOS"=>"Servicios", "OTROS"=>"Otros", ""=>"");

	while(isset($datos['codigo'])){
        $aItem = array(
            $datos['referencia'],
			$datos['empresa'],
			$datos['cif'],
			$datos['localidad'],
            $datos['provincia'],
			$datos['nombre'].' '.$datos['apellidos'],
			$datos['nombreProducto'],
			formateaFechaWeb($datos['fechaBaja']),
			$datos['motivoBaja'],
			"<div class='btn-group'>
				<button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'><i class='icon-download'></i> Opciones <span class='caret'></span></button>
				<ul class='dropdown-menu inverse' role='menu'>
					<li><a href='detallesClienteBaja.php?codigo=".$datos['codigo']."' class='pull-left'><span class='label label-primary'><i class='icon-zoom-in'></i> Ver datos</i></a></span></li>
					<li><a href='creaTarea.php?codigo0=".$datos['codigo']."' class='pull-left'><span class='label label-warning'><i class='icon-plus-sign'></i> Añadir Tarea</i></a></span></li>
					<li><a href='ventas.php?codigo=".$datos['codigo']."' class='pull-left'><span class='label label-success'><i class='icon-shopping-cart'></i> Ventas realizadas</i></a></span></li>
					<li><a href='generaContrato.php?codigo=".$datos['codigo']."' class='pull-left'><span class='label label-info'><i class='icon-download'></i> Contrato</i></a></span></li>
					<li><a href='trabajos.php?codigoCliente=".$datos['codigo']."' class='pull-left'><span class='label label-warning'><i class='icon-cogs'></i> Trabajos</i></a></span></li>
					<li><a href='tareas.php?codigoCliente=".$datos['codigo']."' class='pull-left'><span class='label label-success'><i class='icon-calendar'></i> Tareas</i></a></span></li>
					<li><a href='incidencias.php?codigoCliente=".$datos['codigo']."' class='pull-left'><span class='label label-inverse'><i class='icon-exclamation'></i> Incidencias</i></a></span></li>
				</ul>
			</div>",
			"<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>",
			'DT_RowId' => $datos['codigo']
        );
		$datos=mysql_fetch_assoc($aMembers);
        $output['aaData'][] = $aItem;
	}
    echo json_encode($output);
}
