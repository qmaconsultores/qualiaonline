<?php
if ($_GET) {
	session_start();
    require_once('../funciones.php');
    switch ($_GET['action']) {
        case 'getMembersAjx':
            getMembersAjx();
            break;
        case 'updateMemberAjx':
            updateMemberAjx();
            break;
        case 'deleteMember':
            deleteMember();
            break;
    }
    exit;
}

function getMembersAjx() {

    // SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }

    // SQL order
    $aColumns = array('nombreGestoria', 'empresa', 'mailGestoria', 'telefonoGestoria', 'codigoCliente', 'codigo');
    $sOrder = '';
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $sOrder .= '`'.$aColumns[ (int)$_GET['iSortCol_'.$i] ].'` '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $sOrder = substr_replace($sOrder, '', -2);
        if ($sOrder == 'ORDER BY') {
            $sOrder = '';
        }
    }

    // SQL where
	$sWhere='HAVING 1=1';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
		$sWhere='HAVING 1=1';
		$sWhere=$sWhere.' AND(';
        for ($i=0; $i<count($aColumns) ; $i++) {
            if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true') {
				$sWhere .= '`' . $aColumns[$i]."` LIKE '%".$_GET['sSearch']."%' OR ";
            }
        }
        $sWhere = substr_replace( $sWhere, '', -3 );
        $sWhere .= ')';
    }

	conexionBD();
    $aMembers = consultaBD("SELECT gestorias.codigo, gestorias.nombreGestoria, gestorias.mailGestoria, gestorias.telefonoGestoria, gestorias.codigoCliente, clientes.empresa FROM gestorias 
	LEFT JOIN clientes ON gestorias.codigoCliente=clientes.codigo {$sWhere} {$sOrder} {$sLimit}");
	$datos=mysql_fetch_assoc($aMembers);
    $iCnt = consultaBD("SELECT gestorias.codigo, gestorias.nombreGestoria, gestorias.mailGestoria, gestorias.telefonoGestoria, gestorias.codigoCliente, clientes.empresa FROM gestorias 
	LEFT JOIN clientes ON gestorias.codigoCliente=clientes.codigo {$sWhere}");
	$iCnt=mysql_num_rows($iCnt);
	cierraBD();

    $output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($aMembers),
        'iTotalDisplayRecords' => $iCnt,
        'aaData' => array()
    );
	conexionBD();
	while(isset($datos['codigo'])){
		$datosCliente=consultaBD("SELECT empresa FROM clientes WHERE codigo='".$datos['codigoCliente']."';",false,true);

        if($_SESSION['tipoUsuario'] != 'FORMACION'){
        $aItem = array(
            $datos['nombreGestoria']."</td>",
			$datosCliente['empresa'],
			$datos['mailGestoria'],
			$datos['telefonoGestoria'],
			"<div class='btn-group'>
				<button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'><i class='icon-download'></i> Opciones <span class='caret'></span></button>
				<ul class='dropdown-menu inverse' role='menu'>
					<li><a href='detallesGestoria.php?codigo=".$datos['codigo']."' class='pull-left'><span class='label label-primary'><i class='icon-zoom-in'></i> Ver datos</i></a></span></li>
					<li><a href='creaColaboradorGestoria.php?codigo=".$datos['codigo']."' class='pull-left'><span class='label label-success'><i class='icon-group'></i> Pasar a colaborador</i></a></span></li>
				</ul>
			</div>",
			"<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>",
			'DT_RowId' => $datos['codigo']
        );
        } else {
            $aItem = array(
            $datos['nombreGestoria']."</td>",
            $datosCliente['empresa'],
            $datos['mailGestoria'],
            $datos['telefonoGestoria'],
            'DT_RowId' => $datos['codigo']
        );
        }
		$datos=mysql_fetch_assoc($aMembers);
        $output['aaData'][] = $aItem;
	}
	cierraBD();
    echo json_encode($output);
}
