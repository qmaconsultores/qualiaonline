<script type="text/javascript" src="js/checkTabla.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<?php
session_start();
include_once('../funciones.php');
compruebaSesion();

	conexionBD();
	$where=compruebaPerfilParaWhere('ventas.codigoUsuario');
	$consulta=consultaBD("SELECT ventas.codigo, ventas.tipo, concepto, precio, fecha, observaciones, empresa, activo, clientes.codigo AS codigoCliente, CONCAT(nombre, ' ', apellidos) AS comercial FROM ventas
	INNER JOIN clientes ON clientes.codigo=ventas.codigoCliente LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo $where ORDER BY empresa, fecha DESC;");
	echo mysql_error();
	$datos=mysql_fetch_assoc($consulta);

	$concepto=array("formacionBonificada"=>"Formación Bonificada", "pif"=>"PIF", "iso"=>"ISO", "lopd"=>"LOPD", "trazabilidad"=>"Trazabilidad", "blanqueo"=>"Blanqueo", "software"=>"Software", "web"=>"Web", "gestoria"=>"Gestoría", "formacion"=>"Formación");
	$destino=array('SI'=>'detallesCuenta.php', 'NO'=>'detallesPosibleCliente.php');

	echo "
		 <table class='table table-striped table-bordered datatable'>
                <thead>
                  <tr>
					<th> Empresa </th>
                    <th> Tipo de venta </th>
					<th> Comercial </th>
                    <th> Concepto </th>
					<th> Fecha </th>
                    <th> Importe </th>
                    <th> Observaciones </th>
                    <th class='centro'></th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>"; 
				
	while($datos!=0){
		$fecha=formateaFechaWeb($datos['fecha']);
		
		echo "
		<tr>
			<td> <a href='".$destino[$datos['activo']]."?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a> </td>
        	<td> ".ucfirst($datos['tipo'])." </td>
			<td> ".$datos['comercial']." </td>
        	<td> ".$concepto[$datos['concepto']]." </td>
			<td> $fecha </td>
			<td> ".$datos['precio']." € </td>
			<td> ".ucfirst($datos['observaciones'])." </td>
        	<td class='centro'>
        		<a href='detallesVentaGeneral.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
        	</td>
        	<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	
	echo "
		</tbody>
              </table>";
	cierraBD();

?>