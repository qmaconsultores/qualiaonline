<?php
session_start();
?>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<?php
include_once('../funciones.php');
compruebaSesion();

	$datos=arrayFormulario();
	$where='';
	if($datos['tipo']!='todos'){
		$where='WHERE tipoSepa = "'.$datos['tipo'].'"';
	}
	
	if($datos['firma']!='todos'){
		if($where==''){
			$where='WHERE codigoFirma = "'.$datos['firma'].'"';
		} else {
			$where.=' AND codigoFirma = "'.$datos['firma'].'"';
		}	
	}

	if($datos['banco']!='todos'){
		if($where==''){
			$where='WHERE banco = "'.$datos['banco'].'"';
		} else {
			$where.=' AND banco = "'.$datos['banco'].'"';
		}	
	}
	
	if($datos['anio']=='0'){
		$fecha=date('Y');
	}else{
		$fecha=$datos['anio'];
	}
	
	if($datos['mes']=='0'){
		$fecha=$fecha.'-%';
	}else{
		$fecha=$fecha.'-'.$datos['mes'].'-%';
	}
	

	if($where==''){
		$where.="WHERE fecha LIKE '".$fecha."'";
	} else {
		$where.=" AND fecha LIKE '".$fecha."'";
	}	
	
	
	$consulta=consultaBD("SELECT remesas.*,firmas.firma FROM remesas LEFT JOIN firmas ON remesas.codigoFirma=firmas.codigo $where ORDER BY fecha DESC;",true);
	
	echo "
		<table class='table table-striped table-bordered datatable'>
			<thead>
			  <tr>
			  	<th> Fecha </th>
			  	<th> Firma </th>
				<th> Importe </th>
				<th> Tipo de remesa </th>
				<th> Banco receptor</th>
				<th></th>
			  </tr>
			</thead>
			<tbody>";
			
	$tipo=array('19143'=>'CORE','19445'=>'B2B');
	while($datos=mysql_fetch_assoc($consulta)){
		$fecha=formateaFechaWeb($datos['fecha']);
		echo "
		<tr>
			<td> ".$fecha." </td>
			<td> ".$datos['firma']." </td>
			<td> ".$datos['importe']." €</td>
        	<td> ".$tipo[$datos['tipoSepa']]." </td>
			<td> ".$datos['banco']." </td>
			<td> <a href='detallesRemesa.php?codigo=".$datos['codigo']."' class='btn btn-primary'>Detalles</a></td>
    	</tr>";
	}
	echo "
	</tbody>
              </table>";

?>