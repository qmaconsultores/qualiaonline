<script type="text/javascript" src="js/checkTabla.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<?php
session_start();
include_once('../funciones.php');
compruebaSesion();

//$codigoS=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION'){
		$where=compruebaPerfilParaWhere();
	}

	conexionBD();

	$consulta=consultaBD("SELECT clientes.codigo, empresa, nombreProducto, sector, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS nombreUsuario, referencia, localidad FROM clientes LEFT JOIN productos ON clientes.tipoServicio=productos.codigo LEFT JOIN usuarios ON usuarios.codigo=clientes.codigoUsuario $where AND activo='NO' ORDER BY referencia, empresa, fechaRegistro;");
	$datos=mysql_fetch_assoc($consulta);

	$sectores=array("HOSTELERIA"=>"Hostelería", "COMERCIO"=>"Comercio", "TRANSPORTE"=>"Transporte", "ESTETICA"=>"Estética", "SANIDAD"=>"Sanidad", "VETERINARIA"=>"Veterinaria", "DEPORTES"=>"Deportes", "ASOCIACIONES"=>"Asociaciones", "FORMACION"=>"Formación", "INDUSTRIA"=>"Industria", "CONSTRUCCION"=>"Construcción", "INFORMATICA"=>"Informática", "SERVICIOS"=>"Servicios", "OTROS"=>"Otros", ""=>"");

	echo "
		<table class='table table-striped table-bordered datatable'>
                <thead>
                  <tr>
					<th> ID </th>
                    <th> Nombre del cliente </th>
					<th> Población </th>
					<th> Comercial asignado </th>
                    <th> Sector </th>
                    <th> Servicio ofrecido </th>
                    <th class='td-large'></th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>";
	while($datos!=0){
		echo "
		<tr>
			<td> P-".$datos['referencia']." </td>
        	<td> ".$datos['empresa']." </td>
			<td> ".$datos['localidad']." </td>
			<td> ".$datos['nombreUsuario']." </td>
        	<td> ".$sectores[$datos['sector']]." </td>
        	<td> ".$datos['nombreProducto']." </td>
        	<td class='centro'>
				<div class='margenPeque'>
					<a href='detallesPosibleCliente.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a>
					<a href='creaTarea.php?codigo0=".$datos['codigo']."' class='btn btn-warning'><i class='icon-plus-sign'></i> Añadir Tarea</i></a>
					<a href='creaVenta.php?codigo=".$datos['codigo']."&activa' class='btn btn-success'><i class='icon-ok-sign'></i> Confirmar venta</i></a>
				</div>
				<a href='trabajos.php?codigoCliente=".$datos['codigo']."' class='btn btn-warning'><i class='icon-cogs'></i> Trabajos</i></a>
				<a href='tareas.php?codigoCliente=".$datos['codigo']."' class='btn btn-success'><i class='icon-calendar'></i> Tareas</i></a>
				<a href='incidencias.php?codigoCliente=".$datos['codigo']."' class='btn btn-inverse'><i class='icon-exclamation'></i> Incidencias</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	
	echo "
		</tbody>
              </table>";
	cierraBD();
?>