<?php
session_start();
include_once('../funciones.php');
compruebaSesion();


	$datosFormulario=arrayFormulario();
	
	if($datosFormulario['comercial']=='todos'){	
		$where='';
	}else{
		$where="AND ventas.codigoUsuario='".$datosFormulario['comercial']."'";
	}

	conexionBD();
	if($datosFormulario['mes']=='0'){
		$datos=array();

		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha='".date('Y-m-d')."' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalHoy']=$consulta['codigo'];
		
		# Obtenemos el día de la semana de la fecha dada
		$diaSemana=date("w",mktime(0,0,0,date('m'),date('d'),date('Y')));

		# el 0 equivale al domingo...
		if($diaSemana==0){
			$diaSemana=7;
		}

		# A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
		$primerDia=date("Y-m-d",mktime(0,0,0,date('m'),date('d')-$diaSemana+1,date('Y')));

		# A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
		$ultimoDia=date("Y-m-d",mktime(0,0,0,date('m'),date('d')+(7-$diaSemana),date('Y')));


		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha>='$primerDia' AND fecha<='$ultimoDia' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalSemana']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha>='".date('Y-m-1')."' AND fecha<='".date("d",(mktime(0,0,0,date('m')+1,1,date('Y'))-1))."' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalMes']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM ventas WHERE tipoVentaCarteraNueva='NUEVA' AND fecha>='".date('Y-m-1')."' AND fecha<='".date("d",(mktime(0,0,0,date('m')+1,1,date('Y'))-1))."' $where;");

		$consulta=mysql_fetch_assoc($consulta);

		$datos['nuevas']=$consulta['codigo'];

		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE tipoVentaCarteraNueva='NUEVA' AND fecha>='".date('Y-m-1')."' AND fecha<='".date("d",(mktime(0,0,0,date('m')+1,1,date('Y'))-1))."' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalNuevas']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM ventas WHERE tipoVentaCarteraNueva='CARTERA' AND fecha>='".date('Y-m-1')."' AND fecha<='".date("d",(mktime(0,0,0,date('m')+1,1,date('Y'))-1))."' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['cartera']=$consulta['codigo'];

		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE tipoVentaCarteraNueva='CARTERA' AND fecha>='".date('Y-m-1')."' AND fecha<='".date("d",(mktime(0,0,0,date('m')+1,1,date('Y'))-1))."' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalCartera']=$consulta['codigo'];
	}else{

		$mes=$datosFormulario['mes'];
		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha>='".date('Y-'.$mes.'-1')."' AND fecha<='".date('Y-'.$mes.'-31')."' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalMes']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM ventas WHERE tipoVentaCarteraNueva='NUEVA' AND fecha>='".date('Y-'.$mes.'-1')."' AND fecha<='".date('Y-'.$mes.'-31')."' $where;");

		$consulta=mysql_fetch_assoc($consulta);

		$datos['nuevas']=$consulta['codigo'];

		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE tipoVentaCarteraNueva='NUEVA' AND fecha>='".date('Y-'.$mes.'-1')."' AND fecha<='".date('Y-'.$mes.'-31')."' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalNuevas']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM ventas WHERE tipoVentaCarteraNueva='CARTERA' AND fecha>='".date('Y-'.$mes.'-1')."' AND fecha<='".date('Y-'.$mes.'-31')."' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['cartera']=$consulta['codigo'];

		$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE tipoVentaCarteraNueva='CARTERA' AND fecha>='".date('Y-'.$mes.'-1')."' AND fecha<='".date('Y-'.$mes.'-31')."' $where;");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalCartera']=$consulta['codigo'];
	}

	cierraBD();

?>
<div id="big_stats" class="cf">
  
  <?php
  	if($datosFormulario['mes']=='0'){
  ?>
  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['totalHoy'], 2, ',', ''); ?></span> <br>Total hoy</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['totalSemana'], 2, ',', '');?></span> <br>Total semana</div>
  </div>
  <div id="big_stats" class="cf">
  <!-- .stat -->
  <?php
  	}
  ?>

  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['totalMes'], 2, ',', '');?></span> <br>Total mes</div>
  <!-- .stat -->
</div>
<div id="big_stats" class="cf">

  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['nuevas'];?></span> <br>Ventas nuevas</div>
  <!-- .stat -->

  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['totalNuevas'], 2, ',', '');?></span> <br>Total Nuevas</div>
  
  <div class="stat"> <i class="icon-briefcase"></i> <span class="value"><?php echo $datos['cartera'];?></span> <br>Ventas de cartera</div>
  <!-- .stat -->

  <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo number_format((float)$datos['totalCartera'], 2, ',', '');?></span> <br>Total Cartera</div>
  
</div>