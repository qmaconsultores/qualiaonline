<?php
if ($_GET) {
	session_start();
    require_once('../funciones.php');
    switch ($_GET['action']) {
        case 'getMembersAjx':
            getMembersAjx();
            break;
        case 'updateMemberAjx':
            updateMemberAjx();
            break;
        case 'deleteMember':
            deleteMember();
            break;
    }
    exit;
}

function getMembersAjx() {

    // SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }

    // SQL order
    $aColumns = array('codigoAccion', 'grupo', 'denominacion', 'apellidos', 'fechaInicio', 'fechaFin', 'formacionCurso', 'codigo', 'nombre');
    $sOrder = '';
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $sOrder .= '`'.$aColumns[ (int)$_GET['iSortCol_'.$i] ].'` '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $sOrder = substr_replace($sOrder, '', -2);
        if ($sOrder == 'ORDER BY') {
            $sOrder = '';
        }
    }

    // SQL where
	$sWhere='HAVING 1=1';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION' && $_SESSION['tipoUsuario']!='MARKETING' && $_SESSION['tipoUsuario']!='CONSULTORIA'){
		$sWhere=compruebaPerfilParaWhereHaving(true);
	}
	
	$archivo='';
	if(isset($_GET['archivo'])){
		$archivo="AND cursos.fechaInicio LIKE'".$_GET['archivo']."-%'";
	}
	
	if($_SESSION['tipoUsuario']=='COMERCIAL'){
		$sWhere="HAVING cursos.codigo IN(SELECT codigoCurso FROM alumnos_registrados_cursos WHERE codigoAlumno IN(
		SELECT codigo FROM alumnos WHERE codigoVenta IN(SELECT codigo FROM ventas WHERE codigoCliente IN(
		SELECT codigo FROM clientes WHERE codigoUsuario='$codigoU' OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador='$codigoU')))))";
	}
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
		
		$sWhere=$sWhere.' AND(';
        for ($i=0; $i<count($aColumns) ; $i++) {
            if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true') {
				if($aColumns[$i]=='fechaInicio'){
					$sWhere .= "DATE_FORMAT(fechaInicio,'%d/%m/%Y') LIKE '%".$_GET['sSearch']."%' OR ";
				}
				elseif($aColumns[$i]=='fechaFin'){
					$sWhere .= "DATE_FORMAT(fechaFin,'%d/%m/%Y') LIKE '%".$_GET['sSearch']."%' OR ";
				}
				elseif($aColumns[$i]=='apellidos'){
					$sWhere .= "CONCAT(`apellidos`, ', ', `nombre`) LIKE '%".$_GET['sSearch']."%' OR ";
				}
				else{
					$sWhere .= '`' . $aColumns[$i]."` LIKE '%".$_GET['sSearch']."%' OR ";
				}
            }
        }
        $sWhere = substr_replace( $sWhere, '', -3 );
        $sWhere .= ')';
    }
	if(isset($_GET['sSearch_0'])){
		
		$sWhere=$sWhere.' AND(2=2 AND ';
		for ($i=0; $i<count($aColumns) ; $i++) {
			if(isset($_GET['sSearch_'.$i]) && $_GET['sSearch_'.$i]!=''){//Búsqueda por columnas (nótese el índice _$i)
				if(strpos($aColumns[$i],'fecha')!==false){
					$sWhere .= "DATE_FORMAT(".$aColumns[$i].",'%d/%m/%Y') LIKE '%".$_GET['sSearch_'.$i]."%' AND ";
				}
				elseif($aColumns[$i]=='apellidos'){
					$sWhere .="CONCAT(`apellidos`, ', ', `nombre`) LIKE '%".$_GET['sSearch_'.$i]."%' AND ";
				}
				else{
					$sWhere.=$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' AND ";//La búsqueda por columnas es con AND, para que los filtros sean acumulativos
				}					
			}
		}
		$sWhere = substr_replace( $sWhere, '', -4 );
		$sWhere .= ')';	
	}

	conexionBD();
    $aMembers = consultaBD("SELECT cursos.codigo AS codigo, denominacion, nombre, apellidos, fechaInicio, fechaFin, accionFormativa.codigoInterno AS codigoAccion, 
	cursos.codigoInterno AS grupo, cursos.formacionCurso FROM (accionFormativa INNER JOIN cursos ON accionFormativa.codigoInterno=cursos.codigoAccionFormativa) 
	INNER JOIN tutores ON cursos.codigoTutor=tutores.codigo {$sWhere} $archivo {$sOrder} {$sLimit}");

	$_SESSION['consultaCursos']="SELECT cursos.codigo AS codigo, denominacion, nombre, apellidos, fechaInicio, fechaFin, accionFormativa.codigoInterno AS codigoAccion, 
	cursos.codigoInterno AS grupo, cursos.formacionCurso FROM (accionFormativa INNER JOIN cursos ON accionFormativa.codigoInterno=cursos.codigoAccionFormativa) 
	INNER JOIN tutores ON cursos.codigoTutor=tutores.codigo {$sWhere} $archivo {$sOrder}";

	$datos=mysql_fetch_assoc($aMembers);
    $iCnt = consultaBD("SELECT cursos.codigo AS codigo, denominacion, nombre, apellidos, fechaInicio, fechaFin, accionFormativa.codigoInterno AS codigoAccion, 
	cursos.codigoInterno AS grupo, cursos.formacionCurso FROM (accionFormativa INNER JOIN cursos ON accionFormativa.codigoInterno=cursos.codigoAccionFormativa) 
	INNER JOIN tutores ON cursos.codigoTutor=tutores.codigo {$sWhere} $archivo");
	cierraBD();

    $output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($aMembers),
        'iTotalDisplayRecords' => mysql_num_rows($iCnt),
        'aaData' => array()
    );
	conexionBD();
	$estado=array('NO'=>"<span class='label'>No tiene</span>",'HECHO'=>"<span class='label label-danger'>No hecho</span>",'PROCESO'=>"<span class='label label-warning'>En proceso</span>",'FINALIZADO'=>"<span class='label label-success'>Finalizado</span>");
	while(isset($datos['codigo'])){
		$fechaInicio=formateaFechaWeb($datos['fechaInicio']);
		$fechaFin=formateaFechaWeb($datos['fechaFin']);
		
		$botonera="";
		if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='CONSULTORIA'){
			$botonera.="<a href='detallesCurso.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
			<a href='notificaciones.php?codigo=".$datos['codigo']."' class='btn btn-warning'><i class='icon-bell-alt'></i> Notificaciones</i></a>
			<a href='costes.php?codigo=".$datos['codigo']."' class='btn btn-info'><i class='icon-euro'></i> Costes</i></a>";
		}
		$botonera.="<!--a href='generaXMLParticipantes.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-download'></i> XML Participantes</i></a-->
		<a href='generaDocumentacion.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-download'></i> Documentación</i></a>";
		
        $aItem = array(
            $datos['codigoAccion'],
			$datos['grupo'],
        	$datos['denominacion'],
        	$datos['apellidos'].", ".$datos['nombre'],
        	$fechaInicio,
        	$fechaFin,
			$estado[$datos['formacionCurso']],
			$botonera,			
			"<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>",
			'DT_RowId' => $datos['codigo']
        );
		$datos=mysql_fetch_assoc($aMembers);
        $output['aaData'][] = $aItem;
	}
	cierraBD();
    /*foreach ($aMembers as $iID => $aInfo) {
		echo "ENTRA";
        $aItem = array(
            $aInfo['nombre'], $aInfo['apellidos'], $aInfo['email'], $aInfo['telefono'], $aInfo['usuario'], $aInfo['clave'], 'DT_RowId' => $aInfo['codigo']
        );
        $output['aaData'][] = $aItem;
    }*/
    echo json_encode($output);
}
