<?php
session_start();
include_once('../funciones.php');
compruebaSesion();


	$datosFormulario=arrayFormulario();

	$mes=$datosFormulario['mes'] == '0' ? '01' : $datosFormulario['mes'];
	$mesFinal=$datosFormulario['mes'] == '0' ? 12 : $datosFormulario['mes'];
	$where='';
	if($datosFormulario['comercial'] != 'NULL'){
		$where = 'AND ofertas.codigoUsuario='.$datosFormulario['comercial'];
	}

	conexionBD();
		$datos=array();
		
		$consulta=consultaBD("SELECT COUNT(trabajos.codigo) AS codigo FROM trabajos INNER JOIN ofertas ON trabajos.codigoOferta=ofertas.codigo WHERE fechaValidez>='".date('Y-'.$mes.'-01')."' AND fechaValidez<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalTrabajos']=$consulta['codigo'];

		$consulta=consultaBD("SELECT COUNT(trabajos.codigo) AS codigo FROM trabajos INNER JOIN ofertas ON trabajos.codigoOferta=ofertas.codigo WHERE trabajos.estado='PROCESO' AND fechaValidez>='".date('Y-'.$mes.'-01')."' AND fechaValidez<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalProceso']=$consulta['codigo'];
		
		$consulta=consultaBD("SELECT COUNT(trabajos.codigo) AS codigo FROM trabajos INNER JOIN ofertas ON trabajos.codigoOferta=ofertas.codigo WHERE trabajos.estado='ENTREGADO' AND fechaValidez>='".date('Y-'.$mes.'-01')."' AND fechaValidez<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
		$consulta=mysql_fetch_assoc($consulta);

		$datos['totalFinalizados']=$consulta['codigo'];
		
		$tipos=array(0=>'formacion',1=>'prl',2=>'lopd',3=>'lssi',4=>'aler',5=>'web');
		for($i=0;$i<=5;$i++){
			$consulta=consultaBD("SELECT COUNT(trabajos.codigo) AS codigo FROM trabajos INNER JOIN ofertas ON trabajos.codigoOferta=ofertas.codigo WHERE ".$tipos[$i]."='NO' AND fechaValidez>='".date('Y-'.$mes.'-01')."' AND fechaValidez<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
			$consulta=mysql_fetch_assoc($consulta);

			$datos[$tipos[$i].'NoTiene']=$consulta['codigo'];
			
			$consulta=consultaBD("SELECT COUNT(trabajos.codigo) AS codigo FROM trabajos INNER JOIN ofertas ON trabajos.codigoOferta=ofertas.codigo WHERE ".$tipos[$i]."='HECHO' AND fechaValidez>='".date('Y-'.$mes.'-01')."' AND fechaValidez<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
			$consulta=mysql_fetch_assoc($consulta);

			$datos[$tipos[$i].'No']=$consulta['codigo'];
			
			$consulta=consultaBD("SELECT COUNT(trabajos.codigo) AS codigo FROM trabajos INNER JOIN ofertas ON trabajos.codigoOferta=ofertas.codigo WHERE ".$tipos[$i]."='PROCESO' AND fechaValidez>='".date('Y-'.$mes.'-01')."' AND fechaValidez<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
			$consulta=mysql_fetch_assoc($consulta);

			$datos[$tipos[$i].'Proceso']=$consulta['codigo'];
			
			$consulta=consultaBD("SELECT COUNT(trabajos.codigo) AS codigo FROM trabajos INNER JOIN ofertas ON trabajos.codigoOferta=ofertas.codigo WHERE ".$tipos[$i]."='FINALIZADO' AND fechaValidez>='".date('Y-'.$mes.'-01')."' AND fechaValidez<='".date('Y-'.$mesFinal.'-31')."' ".$where.";");
			$consulta=mysql_fetch_assoc($consulta);

			$datos[$tipos[$i].'Finalizada']=$consulta['codigo'];
		}


	cierraBD();

?>
<div id="big_stats" class="cf">
  
  <div class="stat"> <i class="icon-wrench"></i> <span class="value"><?php echo $datos['totalTrabajos']; ?></span> <br>Trabajos registrados</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-refresh icon-spin"></i> <span class="value"><?php echo $datos['totalProceso'];?></span> <br>En proceso</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['totalFinalizados'];?></span> <br>Finalizados</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">Formación:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['formacionNoTiene']; ?></span> <br>No tiene</div>
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['formacionNo']; ?></span> <br>Formación no hecha</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-refresh icon-spin"></i> <span class="value"><?php echo $datos['formacionProceso'];?></span> <br>Formación en proceso</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['formacionFinalizada'];?></span> <br>Formación finalizada</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">PRL:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['prlNoTiene']; ?></span> <br>No tiene</div>
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['prlNo']; ?></span> <br>Formación no hecha</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-refresh icon-spin"></i> <span class="value"><?php echo $datos['prlProceso'];?></span> <br>Formación en proceso</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['prlFinalizada'];?></span> <br>Formación finalizada</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">LOPD:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['lopdNoTiene']; ?></span> <br>No tiene</div>
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['lopdNo']; ?></span> <br>Formación no hecha</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-refresh icon-spin"></i> <span class="value"><?php echo $datos['lopdProceso'];?></span> <br>Formación en proceso</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['lopdFinalizada'];?></span> <br>Formación finalizada</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">LSSI:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['lssiNoTiene']; ?></span> <br>No tiene</div>
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['lssiNo']; ?></span> <br>Formación no hecha</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-refresh icon-spin"></i> <span class="value"><?php echo $datos['lssiProceso'];?></span> <br>Formación en proceso</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['lssiFinalizada'];?></span> <br>Formación finalizada</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">ALER:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['alerNoTiene']; ?></span> <br>No tiene</div>
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['alerNo']; ?></span> <br>Formación no hecha</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-refresh icon-spin"></i> <span class="value"><?php echo $datos['alerProceso'];?></span> <br>Formación en proceso</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['alerFinalizada'];?></span> <br>Formación finalizada</div>
  <!-- .stat -->
</div>
<h6 class="fondoAzul">WEB:</h6>
<div id="big_stats" class="cf">
  <div class="stat"> <i class="icon-remove"></i> <span class="value"><?php echo $datos['webNoTiene']; ?></span> <br>No tiene</div>
  <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $datos['webNo']; ?></span> <br>Formación no hecha</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-refresh icon-spin"></i> <span class="value"><?php echo $datos['webProceso'];?></span> <br>Formación en proceso</div>
  <!-- .stat -->
  <div class="stat"> <i class="icon-ok"></i> <span class="value"><?php echo $datos['webFinalizada'];?></span> <br>Formación finalizada</div>
  <!-- .stat -->
</div>