<?php
	session_start();
	include_once('../funciones.php');
	compruebaSesion();
	
	$datos=arrayFormulario();

	$consulta=consultaBD("SELECT lunes,martes,miercoles,jueves,viernes,sabado,domingo,horaInicio,horaFin,horaInicioTarde,horaFinTarde,horasTutoria FROM tutores WHERE codigo='".$datos['codigoTutor']."';",true);
	$datosTutor=mysql_fetch_assoc($consulta);
	
?>

	<div class="control-group">                     
	  <label class="control-label" for="horario">Horario mañana:</label>
	  <div class="controls">
		<input type="text" class="input-small" id="horaInicio" name="horaInicio" value="<?php echo $datosTutor['horaInicio']; ?>">
					 a 
					<input type="text" class="input-small" id="horaFin" name="horaFin" value="<?php echo $datosTutor['horaFin']; ?>">
					 (hh:mm) Comprendido entre 00:01h. y 15:00h.
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->
	
			<div class="control-group">                     
	  <label class="control-label" for="horario">Horario tarde:</label>
	  <div class="controls">
		<input type="text" class="input-small" id="horaInicioTarde" name="horaInicioTarde" value="<?php echo $datosTutor['horaInicioTarde']; ?>">
					 a 
					<input type="text" class="input-small" id="horaFinTarde" name="horaFinTarde" value="<?php echo $datosTutor['horaFinTarde']; ?>">
					 (hh:mm) Comprendido entre 15:01h. y 00:00h.
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->
	
			  <div class="control-group">                     
	  <label class="control-label" for="horasTutoria">Horas tutorías:</label>
	  <div class="controls">
		<input type="text" class="input-small" id="horasTutoria" name="horasTutoria" value="<?php echo $datosTutor['horasTutoria']; ?>">
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->

	<div class="control-group">                     
	  <label class="control-label" for="horas">Días impartición:</label>
	  <div class="controls">
		<table class="mitadAncho">
		  <tr>
			<th>L</th>
			<th>M</th>
			<th>X</th>
			<th>J</th>
			<th>V</th>
			<th>S</th>
			<th>D</th>
		  </tr>
		  <tr>
			<th><input type="checkbox" id="lunes" name="lunes" <?php if($datosTutor['lunes']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="martes" name="martes" <?php if($datosTutor['martes']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="miercoles" name="miercoles" <?php if($datosTutor['miercoles']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="jueves" name="jueves" <?php if($datosTutor['jueves']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="viernes" name="viernes" <?php if($datosTutor['viernes']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="sabado" name="sabado" <?php if($datosTutor['sabado']=='SI'){ echo "checked='checked'";} ?>></th>
			<th><input type="checkbox" id="domingo" name="domingo" <?php if($datosTutor['domingo']=='SI'){ echo "checked='checked'";} ?>></th>
		  </tr>
		</table>
	  </div> <!-- /controls -->       
	</div> <!-- /control-group -->