<?php
	//$codigoU=$_SESSION['codigoS'];
	include_once('../funciones.php');
	session_start();
	$inicio=$_GET['start'];
	$fin=$_GET['end'];
	
	$inicioVista=formateaFechaVistaCalendario($inicio,true);
	$finVista=formateaFechaVistaCalendario($fin,true,true);
	
	
	if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
		$where='WHERE 1=1 AND(tareas.codigoUsuario="'.$_SESSION['codigoS'].'" OR tareas.codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'"))';
	}else{
		$where=compruebaPerfilParaWhere('tareas.codigoUsuario');
	}
	

	conexionBD();
	$anio=date('Y');
	$consulta=consultaBD("SELECT tareas.codigo, empresa, tarea, fechaInicio, fechaFin, horaInicio, horaFin, todoDia, prioridad, colorTareas, tareas.firmado, tareas.pendiente FROM tareas 
	INNER JOIN clientes ON tareas.codigoCliente=clientes.codigo 
	LEFT JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo $where AND tareas.estado='pendiente' AND fechaInicio LIKE'$anio-%' AND (fechaInicio>='$inicioVista' OR fechaInicio<='$finVista' OR fechaFin>='$inicioVista' OR fechaFin<='$finVista') ORDER BY prioridad;");
	$datos=mysql_fetch_assoc($consulta);
	$tareas="";
	$clases=array('alta'=>'"backgroundColor": "#098409", "borderColor": "#055D05"','normal'=>'"backgroundColor": "#FACC2E", "borderColor": "#B18904"','baja'=>'"backgroundColor": "#ec5b58", "borderColor": "#b94a48"',
	'sincontactar'=>'"backgroundColor": "#B2B2FF", "borderColor": "#A3A3CA"','proceso'=>'"backgroundColor": "#0000FF", "borderColor": "#0505DC"',
	'reconcertar'=>'"backgroundColor": "#515161", "borderColor": "#000000"', 'concertada'=>'"backgroundColor": "#06E306", "borderColor": "#1DAB1D"', 'visitado'=>'"backgroundColor": "#9595FF", "borderColor": "#8181EC"');
	while($datos!=false){
		$fechaInicio=explode('-',$datos['fechaInicio']);
		$fechaFin=explode('-',$datos['fechaFin']);
		
		$colores=$clases[$datos['prioridad']];

		$tareas.='
			{
				"id": '.$datos['codigo'].',
		    	"title": "-'.$datos['empresa'].': '.$datos['tarea'].'",';
		if($datos['todoDia']=='SI'){
			$tareas.='
			    "start": "'.$fechaInicio[0].'-'.$fechaInicio[1].'-'.$fechaInicio[2].' 00:00:00",
		    	"end": "'.$fechaFin[0].'-'.$fechaFin[1].'-'.$fechaFin[2].' 00:00:00",
		    	"allDay": true,
		    	'.$colores.'		    	
	    	},';
		}
		else{
			$horaInicio=explode(':',$datos['horaInicio']);
			$horaFin=explode(':',$datos['horaFin']);

		    $tareas.='
			    "start": "'.$fechaInicio[0].'-'.$fechaInicio[1].'-'.$fechaInicio[2].' '.$horaInicio[0].':'.$horaInicio[1].':00",
		    	"end": "'.$fechaFin[0].'-'.$fechaFin[1].'-'.$fechaFin[2].' '.$horaFin[0].':'.$horaFin[1].':00",
		    	"allDay": false,
		    	'.$colores.'
	    	},';
		}

		$datos=mysql_fetch_assoc($consulta);
  	}
  	$tareas=substr_replace($tareas, '', strlen($tareas)-1, strlen($tareas));//Para quitar última coma
  	echo "[".$tareas."]";
?>