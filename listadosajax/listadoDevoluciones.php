<?php
session_start();
?>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<?php
include_once('../funciones.php');
compruebaSesion();

	$datos=arrayFormulario();
	$where='';
	if($datos['motivo']!='todos'){
		$where='WHERE motivo = "'.$datos['motivo'].'"';
	}
	if($datos['resolucion']!='todas'){
		if($where==''){
			$where='WHERE devoluciones_facturas.resolucionFactura = "'.$datos['resolucion'].'"';
		} else {
			$where.=' AND devoluciones_facturas.resolucionFactura = "'.$datos['resolucion'].'"';
		}	
	}
	
	if($datos['anio']=='0'){
		$fecha=date('Y');
		$whereFecha=' OR fecha LIKE "0000-00-00"';
	}else{
		$fecha=$datos['anio'];
		$whereFecha='';
	}
	
	if($datos['mes']=='0'){
		$fecha=$fecha.'-%';
	}else{
		$fecha=$fecha.'-'.$datos['mes'].'-%';
	}
	

	if($where==''){
		$where.="WHERE fecha LIKE '".$fecha."'".$whereFecha;
	} else {
		$where.=" AND fecha LIKE '".$fecha."'".$whereFecha;
	}	
	
	
	$consulta=consultaBD("SELECT facturacion.codigo,facturacion.referencia,facturacion.coste,facturacion.codigoCliente, devoluciones_facturas.fecha, devoluciones_facturas.motivo, devoluciones_facturas.resolucionFactura, facturacion.firma, facturacion.concepto, facturacion.anio, facturacion.fechaVencimiento FROM facturacion INNER JOIN devoluciones_facturas ON facturacion.codigo=devoluciones_facturas.codigoFactura $where ORDER BY fecha DESC;",true);
	
	echo "
		<table class='table table-striped table-bordered datatable'>
			<thead>
			  <tr>
			  	<th> Factura </th>
				<th> Cliente </th>
				<th> Fecha vencimiento </th>
				<th> Fecha devolución </th>
				<th> Motivo </th>
				<th> Resolución </th>
				<th> Importe </th>
			  </tr>
			</thead>
			<tbody>";
			
	$destino=array(''=>'detallesCuenta.php','SI'=>'detallesCuenta.php', 'NO'=>'detallesPosibleCliente.php', 'PROPIO'=>'detallesPosibleCliente.php');
	while($datos=mysql_fetch_assoc($consulta)){
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		$fecha=formateaFechaWeb($datos['fecha']);
		$motivo=datosRegistro('motivos',$datos['motivo']);
		if($datos['concepto']=='14'){ 
			if($datos['firma']!='3'){
				$letra="F";
			}else{
				$letra="S";
			}
		}else{
			if($datos['firma']!='3'){
				$letra="C";
			}else{
				$letra="S";
			}
		}
		echo "
		<tr>
			<td> <a href='detallesFactura.php?codigo=".$datos['codigo']."&seccion=14'>".$letra."-".$datos['referencia'].'/'.substr($datos['anio'], 2)."</a></td>
			<td> <a href='".$destino[$cliente['activo']]."?codigo=".$datos['codigoCliente']."'>".$cliente['empresa']."</a> </td>
        	<td> ".formateaFechaWeb($datos['fechaVencimiento'])." </td>
        	<td> ".$fecha." </td>
			<td> ".$motivo['motivo']." </td>
        	<td> ".$datos['resolucionFactura']." </td>
			<td> ".$datos['coste']." € </td>
    	</tr>";
	}
	echo "
	</tbody>
              </table>";

?>