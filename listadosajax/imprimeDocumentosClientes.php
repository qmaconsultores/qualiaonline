<script type="text/javascript" src="js/checkTabla.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<?php
session_start();
include_once('../funciones.php');
compruebaSesion();

	$codigoCliente=$_GET['codigoCliente'];
	$consulta=consultaBD("SELECT * FROM documentosClientes WHERE codigoCliente='$codigoCliente' ORDER BY fechaFactura DESC;",true);

	$datos=mysql_fetch_assoc($consulta);

	echo "
		 <center>
			<table class='table table-striped table-bordered datatable mitadAncho' id='tablaFacturas'>
			  <thead>
				<tr>
				  <th> Documento </th>
				  <th> Fecha de documento </th>
				  <th class='centro'> </th>
				</tr>
			  </thead>
			  <tbody>"; 
				
	while($datos!=0){
		$enlace='documentos/';
		if(stristr($datos['fechaFactura'], '2015-') != FALSE){
			$enlace='http://softwareparapymes.net/grupqualia/documentos/';
		}
		echo "
		<tr>
        	<td> ".$datos['nombreFactura']."</td>
        	<td> ".formateaFechaWeb($datos['fechaFactura'])." </td>
        	<td class='centro'>
        		<a href='$enlace".$datos['ficheroFactura']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a> ";
				if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='MARKETING'){
					echo "<a href='eliminaFactura.php?codigo=".$datos['codigo']."&eliminar=1&codigoCliente=$codigoCliente' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>";
				}
			echo "
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	
	echo "
		</tbody>
      </table>";

?>