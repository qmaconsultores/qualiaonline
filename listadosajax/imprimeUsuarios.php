<script type="text/javascript" src="js/checkTabla.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<?php
session_start();
include_once('../funciones.php');
compruebaSesion();

conexionBD();

	$consulta=consultaBD("SELECT codigo, nombre, apellidos, telefono, email, usuario, clave, tipo, director FROM usuarios ORDER BY apellidos, nombre;");
	$datos=mysql_fetch_assoc($consulta);
	$director=array('SI'=>'Director ','NO'=>'');
	$tipo=array('ADMIN'=>'Administrador','COMERCIAL'=>'Comercial','ADMINISTRACION'=>'Administración','CONSULTORIA'=>'Consultor','FORMACION'=>'Formación','ATENCION'=>'Atención al Cliente');

	$mensaje= "
		<table class='table table-striped table-bordered datatable' id='tablaUsuarios'>
            <thead>
              <tr>
                <th> Nombre </th>
                <th> Teléfono </th>
                <th> eMail </th>
                <th> Usuario </th>
                <th> Clave </th>
                <th> Tipo </th>
                <th class='centro'></th>
                <th><input type='checkbox' id='todo'></th>
              </tr>
            </thead>
            <tbody>";
	while($datos!=0){
		$mensaje.= "
		<tr>
			<td> ".$datos['apellidos'].", ".$datos['nombre']." </td>
			<td> ".formateaTelefono($datos['telefono'])." </td>
			<td> ".$datos['email']." </td>
        	<td> ".$datos['usuario']." </td>
        	<td> ".$datos['clave']." </td>
        	<td> ";
			if($datos['tipo']!='ADMIN' && $datos['tipo']!='ADMINISTRACION'){
				$mensaje.= $director[$datos['director']].$tipo[$datos['tipo']];
			}else{
				$mensaje.= $tipo[$datos['tipo']];
			}			
			$mensaje.= "</td>
        	<td class='centro'>
        		<a href='detallesUsuario.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-edit'></i> Modificar</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	$mensaje.= "
		</tbody>
       </table>
	";
	
	echo $mensaje;
	cierraBD();
?>