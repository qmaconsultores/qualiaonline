<?php
session_start();
if ($_GET) {
    require_once('../funciones.php');
    switch ($_GET['action']) {
        case 'getMembersAjx':
            getMembersAjx();
            break;
        case 'updateMemberAjx':
            updateMemberAjx();
            break;
        case 'deleteMember':
            deleteMember();
            break;
    }
    exit;
}

function getMembersAjx() {

    // SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }

    // SQL order
    $aColumns = array('empresa', 'usuarioAsignado', 'localidad', 'contacto', 'telefono', 'movil', 'tarea', 'fechaInicio', 'horaInicio', 'prioridad');
    $sOrder = 'ORDER BY fechaInicio DESC';
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
				
					$sOrder .= '`'.$aColumns[ (int)$_GET['iSortCol_'.$i] ].'` '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $sOrder = substr_replace($sOrder, '', -2);
        if ($sOrder == 'ORDER BY') {
            $sOrder = '';
        }
    }

    // SQL where
    if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
			$sWhere='HAVING tareas.codigoUsuario="'.$_SESSION['codigoS'].'" OR tareas.codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'")';
	}else{
		$sWhere=compruebaPerfilParaHavingTareas('tareas.codigoUsuario');
	}
	
	$inicio=$_GET['inicio'];
	if($inicio=='SI'){
		$inicio=true;
	}else{
		$inicio=false;
	}
		
	if($inicio){
		$sWhere.=" AND estado!='realizada'";
	}
	else{
		$sWhere.=" AND estado='realizada'";
	}
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
			$sWhere='HAVING 1=1 AND (tareas.codigoUsuario="'.$_SESSION['codigoS'].'" OR tareas.codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'"))';
		}else{
			$sWhere=compruebaPerfilParaHavingTareas('tareas.codigoUsuario');
		}
		if($inicio){
			$sWhere.=" AND estado!='realizada' AND(";
		}
		else{
			$sWhere.=" AND estado='realizada' AND(";
		}
        for ($i=0; $i<count($aColumns) ; $i++) {
            if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == 'true') {
				if(strpos($aColumns[$i],'fecha')!==false){
					$sWhere .= "DATE_FORMAT(".$aColumns[$i].",'%d/%m/%Y') LIKE '%".$_GET['sSearch']."%' OR ";
				}elseif(strpos($aColumns[$i],'prioridad')!==false){
					$sWhere .= '`' . $aColumns[$i]."` LIKE '%".htmlspecialchars(devuelveCadenaCorrecta($_GET['sSearch']))."%' OR ";
				}
				else{
					$sWhere .= '`' . $aColumns[$i]."` LIKE '%".htmlspecialchars($_GET['sSearch'])."%' OR ";
				}				
            }
        }
        $sWhere = substr_replace( $sWhere, '', -3 );
        $sWhere .= ')';
    }

    $sWhere .= " AND fechaInicio > '2016-11-30'";
	conexionBD();
    $aMembers = consultaBD("SELECT tareas.codigo, clientes.empresa, clientes.contacto, clientes.telefono, clientes.localidad, tareas.tarea, tareas.fechaInicio, tareas.estado, tareas.prioridad, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) AS usuarioAsignado, clientes.codigo AS codigoCliente, horaInicio, clientes.movil, tareas.firmado, tareas.pendiente, tareas.codigoUsuario, tareas.descripcion,clientes.activo 
		FROM clientes INNER JOIN tareas ON clientes.codigo=tareas.codigoCliente INNER JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo {$sWhere} {$sOrder} {$sLimit}");
	
	$datos=mysql_fetch_assoc($aMembers);
	
    $iCnt = consultaBD("SELECT tareas.codigo, clientes.empresa, clientes.contacto, clientes.telefono, clientes.localidad, tareas.tarea, tareas.fechaInicio, tareas.estado, tareas.prioridad, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) AS usuarioAsignado, clientes.codigo AS codigoCliente, horaInicio, clientes.movil, tareas.firmado, tareas.pendiente, tareas.codigoUsuario, tareas.descripcion,clientes.activo  
		FROM clientes INNER JOIN tareas ON clientes.codigo=tareas.codigoCliente INNER JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo {$sWhere}");
	
	$iCnt=mysql_num_rows($iCnt);
	cierraBD();

    $output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($aMembers),
        'iTotalDisplayRecords' => $iCnt,
        'aaData' => array()
    );
	$colores=array('sincontactar'=>'','proceso'=>"class='registroAzul'",'reconcertar'=>"class='registroNegro'",'alta'=>"class='registroVerde'",
	'normal'=>"class='registroAmarillo'",'pendiente'=>"class='registroAmarillo'",'baja'=>"class='registroRojo'",'concertada'=>"class='registroVerdeClaro'",'visitado'=>"class='registroAzulClaro'");
	
	$prioridad=array('normal'=>"Pendiente",'pendiente'=>"Pendiente",'alta'=>"Vendido",'baja'=>"Baja",'sincontactar'=>"Sin contactar",
	'proceso'=>"En proceso",'reconcertar'=>"Reconcertar",'concertada'=>'Concertada','visitado'=>'Visitado');
	$estado=array('pendiente'=>"<span class='label label-warning'>Pendiente</span>",'realizada'=>"<span class='label label-success'>Realizada</span>");
	/*echo "SELECT tareas.codigo, clientes.empresa, clientes.contacto, clientes.telefono, clientes.localidad, tareas.tarea, tareas.fechaInicio, tareas.estado, tareas.prioridad, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) AS usuarioAsignado, clientes.codigo AS codigoCliente, horaInicio, clientes.movil, tareas.firmado, tareas.pendiente, tareas.codigoUsuario, tareas.descripcion 
		FROM clientes INNER JOIN tareas ON clientes.codigo=tareas.codigoCliente INNER JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo {$sWhere} {$sOrder} {$sLimit}";*/
	while($datos!=0){
		$fecha=formateaFechaWeb($datos['fechaInicio']);
		
		if($inicio){
			$botonDos="<a href='?realizaTarea=".$datos['codigo']."' class='btn btn-success'><i class='icon-ok-sign'></i></a>";
		}else{
			$botonDos="";
		}
		
		$textoConcatenado='';
		if($datos['prioridad']=='normal' || $datos['prioridad']=='pendiente'){
			$textoConcatenado=". Firmado: ".$datos['firmado'].".<br/>Pendiente de: ".$datos['pendiente'];
		}
		$tipoTareas=array(''=>'','PRIMERA'=>'Primera visita','SEGUIMIENTO'=>'Visita seguimiento','LOPD'=>'Auditoría LOPD','PRL'=>'Auditoría PRL','FORMACION'=>'Visita formación','LLAMADA'=>'Llamada Seguimiento','MAIL'=>'Mail');
		$tarea = $tipoTareas[$datos['descripcion']];
		if($datos['tarea'] != ''){
			$tarea.=' - '.$datos['tarea'];
		}
		if($datos['activo']=='SI'){
			$url="detallesCuenta.php?codigo=".$datos['codigoCliente'];
		} elseif ($datos['activo']=='NO') {
			$url="detallesPosibleCliente.php?codigo=".$datos['codigoCliente'];
		} else {
			$url="detallesPosibleClientePropio.php?codigo=".$datos['codigoCliente'];
		}
        $aItem = array(
            "<a href='".$url."'>".utf8_encode($datos['empresa'])."</a>",
			utf8_encode($datos['usuarioAsignado']),
			utf8_encode($datos['localidad']),
			utf8_encode($datos['contacto']),
			formateaTelefono($datos['telefono']),
			formateaTelefono($datos['movil']),
			utf8_encode($tarea),
			$fecha,
			formateaHoraWeb($datos['horaInicio']),
			"<div ".$colores[utf8_encode($datos['prioridad'])].">".$prioridad[utf8_encode($datos['prioridad'])].'<br/>'.utf8_encode($textoConcatenado)."</div>",
			"<td class='centro'>
        		<a href='detallesTarea.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i></a>
				$botonDos
			</td>",
			"<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>"
        );
		$datos=mysql_fetch_assoc($aMembers);
        $output['aaData'][] = $aItem;
	}
    /*foreach ($aMembers as $iID => $aInfo) {
		echo "ENTRA";
        $aItem = array(
            $aInfo['nombre'], $aInfo['apellidos'], $aInfo['email'], $aInfo['telefono'], $aInfo['usuario'], $aInfo['clave'], 'DT_RowId' => $aInfo['codigo']
        );
        $output['aaData'][] = $aItem;
    }*/
    echo json_encode($output);
}

function devuelveCadenaCorrecta($texto){
	$cadena="";
	switch($texto){
		case 'Pendiente': $cadena='normal'; break;
		case 'pendiente': $cadena='normal'; break;
		case 'Vendido': $cadena='alta'; break;
		case 'vendido': $cadena='alta'; break;
		case 'Baja': $cadena='baja'; break;
		case 'baja': $cadena='baja'; break;
		case 'Sin contactar': $cadena='sincontactar'; break;
		case 'sin contactar': $cadena='sincontactar'; break;
		case 'En proceso': $cadena='proceso'; break;
		case 'en proceso': $cadena='proceso'; break;
		case 'Reconcertar': $cadena='reconcertar'; break;
		case 'reconcertar': $cadena='reconcertar'; break;
		case 'Concertada': $cadena='concertada'; break;
		case 'concertada': $cadena='concertada'; break;
		case 'Visitado': $cadena='visitado'; break;
		case 'visitado': $cadena='visitado'; break;
		default: $cadena=$texto; break;
	}
	return $cadena;
}
