<?php
session_start();
?>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<?php
include_once('../funciones.php');
compruebaSesion();

    conexionBD();
    $datos=arrayFormulario();

    $where='';
    $whereTareas='';
    
    /*if($datos['comercial']=='todos'){
        $where='';
    }else{
        $where="WHERE ventas.codigoUsuario='".$datos['comercial']."'";
    }

    if($datos['comercial']=='todos'){
        if($datos['mes']=='0'){
            $where.='WHERE fecha LIKE"%'.date('Y').'-%"';
        }else{
            $where.="WHERE fecha LIKE '".date('Y')."-".$datos['mes']."-%'";
        }
    }else{
        if($datos['mes']=='0'){
            $where.='AND fecha LIKE"%'.date('Y').'-%"';
        }else{
            $where.=" AND fecha LIKE '".date('Y')."-".$datos['mes']."-%'";
        }
    }*/
    $mes = $datos['mes'] == 0 ? '%' : $datos['mes'];
    $dia = $datos['dia'] == '' ? '%' : $datos['dia'];

    $consulta=consultaBD("SELECT codigo, CONCAT (nombre,' ',apellidos) AS comercial FROM usuarios WHERE (tipo='COMERCIAL' OR codigo=19 OR codigo=21 OR codigo=120) AND activoUsuario = 'SI' ORDER BY nombre;");
    $servicios=array('formacion','lssi','alergenos','prl','plataformaWeb','auditoriaLopd','consultoriaLopd','auditoriaPrl','webLegalizada','dominioCorreo','dominio','ecommerce');
    echo "
        <table class='table table-striped table-bordered datatable'>
            <thead>
              <tr>
                <th> Comercial </th>
                <th> Total bruto anterior </th>
                <th> Total bruto </th>
                <th> Total neto </th>
                <th> Diferencia </th>
              </tr>
            </thead>
            <tbody>";
            
    while($datos=mysql_fetch_assoc($consulta)){
        $neto=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha LIKE '".date('Y')."-".$mes."-".$dia."' AND codigoUsuario='".$datos['codigo']."';",false,true);
        $brutoAnterior=consultaBD("SELECT SUM(importe) AS codigo FROM tareas WHERE fechaInicio LIKE '".date('Y')."-".$mes."-".$dia."' AND prioridad='ALTA' AND codigoUsuario='".$datos['codigo']."' AND codigo NOT IN (SELECT codigoTarea FROM preventas);",false,true);
        $preventas=consultaBD("SELECT * FROM preventas INNER JOIN tareas ON preventas.codigoTarea=tareas.codigo WHERE preventas.fecha LIKE '".date('Y')."-".$mes."-".$dia."' AND codigoUsuario='".$datos['codigo']."';",false);
        $bruto=0;
        while($preventa=mysql_fetch_assoc($preventas)){
            for($i=0;$i<count($servicios);$i++){
                if($preventa[$servicios[$i]]=='SI'){
                    $bruto=$bruto+$preventa[$servicios[$i].'Precio'];
                }
            }
        }
        $neto = $neto['codigo'] == '' ? 0 : $neto['codigo'];
        $brutoAnterior = $brutoAnterior['codigo'] == '' ? 0 : $brutoAnterior['codigo'];
        $diferencia = ($bruto+$brutoAnterior) - $neto;
        echo "
        <tr>
            <td> ".$datos['comercial']." </td>
            <td> ".number_format((float)$brutoAnterior, 2, ',', '')." € </td>
            <td> ".number_format((float)$bruto, 2, ',', '')." € </td>
            <td> ".number_format((float)$neto, 2, ',', '')." € </td>
            <td> ".number_format((float)$diferencia, 2, ',', '')." € </td>
        </tr>";
    }
    echo "
    </tbody>
              </table>";
    cierraBD();

?>