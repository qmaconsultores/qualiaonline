<?php
  $seccionActiva=23;
  include_once("cabecera.php");

  $datos=arrayFormulario();
  if(!isset($datos['codigo']) && isset($datos['fechaInicio'])){
    $_POST['codigo']=creaTarea(true);
  } else {
    $res=actualizaTarea();
  }
  $tarea=datosRegistro('tareas',$_POST['codigo']);
  $cliente=datosRegistro('clientes',$tarea['codigoCliente']);
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Alta de Preventa</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="preventas.php" method="post">
                  <fieldset>

                    <?php
                      abreColumnaCampos();
                        campoOculto($_POST['codigo'],'codigoTarea');
                        campoOculto('SINEVALUAR','aceptado');
                        campoOculto('NO','verificada');
                        campoSelectClientePreventa($tarea['codigoCliente'],$cliente['empresa']);
                        //selectClientes($tarea['codigoCliente']);
                      cierraColumnaCampos();
                      abreColumnaCampos();
                        campoFecha('fecha','Fecha de preventa');
                        if($_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='ADMIN'){
                          campoRadio('documentos','¿Dispone de documentos?');
                        } else {
                          campoOculto('NO','documentos');
                        }
                      cierraColumnaCampos();
                      echo '<br clear="all"><h2 class="apartadoFormulario">SERVICIOS</h2>';
                      abreColumnaCampos('span3 servicioPreventa');
                        campoSelect('formacion','Formación',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="formacion hide">';
                          campoTextoSimbolo('formacionPrecio','Precio','€');
                        echo '</div>';

                        campoSelect('prl','PRL',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="prl hide">';
                          campoTextoSimbolo('prlPrecio','Precio','€');
                        echo '</div>';

                        campoSelect('consultoriaLopd','Consultoría LOPD',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="consultoriaLopd hide">';
                          campoTextoSimbolo('consultoriaLopdPrecio','Precio','€');
                        echo '</div>';

                        campoSelect('dominioCorreo','Dominio y correo electrónico',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="dominioCorreo hide">';
                          campoTextoSimbolo('dominioCorreoPrecio','Precio','€');
                        echo '</div>';
                      cierraColumnaCampos();

                      abreColumnaCampos('span3 servicioPreventa');
                        campoSelect('lssi','LSSI',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="lssi hide">';
                          campoTextoSimbolo('lssiPrecio','Precio','€');
                        echo '</div>';

                        campoSelect('plataformaWeb','Plataforma Web',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="plataformaWeb hide">';
                          campoTextoSimbolo('plataformaWebPrecio','Precio','€');
                        echo '</div>';

                        campoSelect('auditoriaPrl','Auditoría PRL',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="auditoriaPrl hide">';
                          campoTextoSimbolo('auditoriaPrlPrecio','Precio','€');
                        echo '</div>';

                        campoSelect('dominio','Dominio',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="dominio hide">';
                          campoTextoSimbolo('dominioPrecio','Precio','€');
                        echo '</div>';
                      cierraColumnaCampos();

                      abreColumnaCampos('span3 servicioPreventa');
                        campoSelect('alergenos','APPCC-Alérgenos',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="alergenos hide">';
                          campoTextoSimbolo('alergenosPrecio','Precio','€');
                        echo '</div>';

                        campoSelect('auditoriaLopd','Auditoría LOPD',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="auditoriaLopd hide">';
                          campoTextoSimbolo('auditoriaLopdPrecio','Precio','€');
                        echo '</div>';

                        campoSelect('webLegalizada','Web legalizada',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="webLegalizada hide">';
                          campoTextoSimbolo('webLegalizadaPrecio','Precio','€');
                        echo '</div>';

                        campoSelect('ecommerce','E-commerce',array('No','Si'),array('NO','SI'),'','selectpicker span1 show-tick selectPrecio');
                        echo '<div class="ecommerce hide">';
                          campoTextoSimbolo('ecommercePrecio','Precio','€');
                        echo '</div>';
                      cierraColumnaCampos();
                      echo '<br clear="all"><h2 class="apartadoFormulario">OBSERVACIONES</h2>';
                      areaTexto('observaciones','Observaciones o mótivos de rechazo','','areaInforme');
                    ?>

                  </fieldset>
                  <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Crear preventa</button> 
                      <a href="detallesTarea.php?codigo=<?php echo $datos['codigo']; ?>" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
<?php include_once('pie.php'); ?>
<!--script type="text/javascript" src="js/bootstrap-slider.js"></script-->
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectPrecio').change(function(){
      var id = $(this).attr('id');
      var val = $(this).val();
      if(val == 'SI'){
        $('.'+id).removeClass('hide');
      } else {
        $('.'+id).addClass('hide');
      }
    })
  });

  

</script>


