<?php
  $seccionActiva=19;
  include_once('cabecera.php');
  $codigo=$_POST['codigoCliente'];

  $nombreCliente=nombreCliente($codigo);
  $datosCliente=datosRegistro('clientes',$codigo);
  $datosColaborador=datosRegistro('colaboradores',$datosCliente['comercial']);
  
  $activa=0;

  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  

?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nueva venta para el cliente <?php echo $nombreCliente; ?></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal formularioVenta" action="ventasGeneral.php" method="post">
				 <div class="tabbable">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#pagina1" data-toggle="tab">Datos de venta</a></li>
                      <li><a href="#pagina2" data-toggle="tab">Factura</a></li>
                    </ul>
                  
                    <br>

                    <div class="tab-content" id="pestanas">

                    <div class="tab-pane active" id="pagina1">
                  <fieldset>
                    
                    <input type="hidden" name="codigoC" value="<?php echo $codigo; ?>">
                    <input type="hidden" name="activa" value="<?php echo $activa; ?>">
					
					<div class="control-group">                     
                      <label class="control-label" for="empresa">Empresa:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="empresa" name="empresa" value="<?php echo $datosCliente['empresa']; ?>" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="contacto">Persona de contacto:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="contacto" name="contacto" value="<?php echo $datosCliente['contacto']; ?>" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono" value="<?php echo $datosCliente['telefono']; ?>" maxlength="9" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="movil">Teléfono móvil:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="movil" name="movil" value="<?php echo $datosCliente['movil']; ?>" maxlength="9" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="mail">Mail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="mail" name="mail" value="<?php echo $datosCliente['mail']; ?>" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoSelect('tipoVentaCarteraNueva','Tipo de venta',array('Cartera','Nueva'),array('CARTERA','NUEVA'));
						$consulta=consultaBD("SELECT * FROM historico_clientes WHERE codigoCliente='$codigo' ORDER BY fecha;",true);
						$datosHistorico=mysql_fetch_assoc($consulta);
						$historico='';
						if(isset($datosHistorico['codigo'])){
							while(isset($datosHistorico['codigo'])){
								$historico=$historico.formateaFechaWeb($datosHistorico['fecha'])." ".$datosHistorico['hora']." ".$datosHistorico['observaciones']." \n\n";
								$datosHistorico=mysql_fetch_assoc($consulta);
							}
						}
						areaTexto('historicoObservaciones','Histórico Observaciones',$historico,'areaTexto',true);
						$consulta=consultaBD("SELECT historico_tareas.* FROM historico_tareas INNER JOIN tareas ON historico_tareas.codigoTarea=tareas.codigo INNER JOIN clientes ON clientes.codigo=tareas.codigoCliente WHERE tareas.codigoCliente='$codigo' ORDER BY fecha;",true);
						$datosHistorico=mysql_fetch_assoc($consulta);
						$historico='';
						if(isset($datosHistorico['codigo'])){
							while(isset($datosHistorico['codigo'])){
								$historico=$historico.formateaFechaWeb($datosHistorico['fecha'])." ".$datosHistorico['hora']." ".$datosHistorico['observaciones']." \n\n";
								$datosHistorico=mysql_fetch_assoc($consulta);
							}
						}
						areaTexto('historicoTareas','Histórico Tareas',$historico,'areaTexto',true);
						
						campoOculto('servicio','tipo');
						campoOculto($datosColaborador['comision'],'comision');
						
						$consulta="SELECT codigo, CONCAT(nombre, ' ', apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' ORDER BY nombre, apellidos;";
						campoSelectConsulta('codigoUsuario','Comercial',$consulta,$datosCliente['codigoUsuario']);
						if($datosCliente['tieneColaborador']=='SI' && $datosCliente['comercial']!=null){
							campoRadio('participaColaborador','Participa Colaborador');
						}else{
							campoOculto('NO','participaColaborador');
						}
						campoSelectConsulta('concepto','Concepto',"SELECT codigo, nombreProducto AS texto FROM productos ORDER BY codigo;");
					?>


                    <!--div class="control-group" id="servicio">                     
                      <label class="control-label" for="selectServicio">Concepto:</label>
                      <div class="controls">
                      
                        <select name="concepto" id="selectServicio" class='selectpicker show-tick' data-live-search="true">
						  <option value="formacion">Formación</option>
                          <option value="lopd">Protección de datos</option>
                          <option value="blanqueo">Blanqueo de capitales</option>
						  <option value="prl">PRL</option>
						  <option value="alergenos">ALÉRGENOS</option>
						  <option value="web">Plataforma WEB</option>
                        </select>
                      
                      </div--> <!-- /controls -->       
                    <!--/div--> <!-- /control-group -->
                    



                    <div class="control-group">                     
                      <label class="control-label" for="precio">Precio:</label>
                      <div class="controls">
                        
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini pagination-right" id="precio" name="precio">
                          <span class="add-on">€</span>
                        </div>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fechaVenta">Fecha venta:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaVenta" name="fechaVenta" value="<?php echo imprimeFecha();?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="observaciones">Observaciones:</label>
                      <div class="controls">
                        <textarea name="observaciones" class="areaTexto" id="observaciones"></textarea>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					


                    <div id='cajaAlumnos'>
                     <h4 class="apartadoFormulario">Alumnos a inscribir</h4>
                      <table class="table table-striped table-bordered anchoAuto" id="tablaAlumnos">
                        <thead>
                          <tr>
                            <th> Nombre </th>
                            <th> Apellidos </th>
                            <th> DNI </th>
                            <th> eMail </th>
                            <th> Teléfono </th>
                          </tr>
                        </thead>
                        <tbody>
                        
                        <tr>
                          <td><input type="text" class="input-large" id="nombre0" name="nombre0"></td>
                          <td><input type="text" class="input-large" id="apellidos0" name="apellidos0"></td>
                          <td><input type="text" class="input-small" id="dni0" name="dni0"></td>
                          <td><input type="text" class="input-large" id="mail0" name="mail0"></td>
                          <td><input type="text" class="input-small" id="tlf0" name="tlf0"></td>
                        </tr>

                        </tbody>
                      </table>
                      <br>
                      <center>
                        <button type="button" class="btn btn-success" onclick="insertaFila('tablaAlumnos');"><i class="icon-plus"></i> Añadir alumno</button> 
                        <button type="button" class="btn btn-danger" onclick="eliminaFila('tablaAlumnos');"><i class="icon-minus"></i> Eliminar alumno</button> 
                      </center>
					  
					  <!--NUEVA PARTE PARA CREAR CURSO DIRECTO CON LA VENTA -->
					  <h3 class='apartadoFormulario'>Datos del curso</h3>
					  
					  <?php
						$consulta="SELECT alumnos.codigo, CONCAT(alumnos.nombre, ' ',alumnos.apellidos) AS texto FROM alumnos INNER JOIN ventas ON ventas.codigo=alumnos.codigoVenta INNER JOIN clientes ON clientes.codigo=ventas.codigoCliente WHERE clientes.codigo='$codigo' ORDER BY nombre, apellidos;";
						campoSelectConsulta('alumnosPrevios[]','Alumnos previos',$consulta,false,'selectpicker span3 show-tick','data-live-search="true" multiple data-selected-text-format="count" multiple title="Seleccione a los alumnos..."');
					  ?>
					  
					  <div class="control-group">                     
                      <label class="control-label" for="accionFormativa">Acción formativa:</label>
                      <div class="controls">
					  
						 <?php 
							echo "<span id='resultado'></span>";
						?>
					  
					  </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('plataforma','Plataforma');
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="responsable">Responsable:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="responsable" name="responsable">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					
					<div class="control-group">                     
                      <label class="control-label" for="tlfResponsable">Tlf. Responsable:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="tlfResponsable" name="tlfResponsable">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					
				          	<div class="control-group">                     
                      <label class="control-label" for="codigoInterno">Código curso:</label>
                      <div class="controls numSS">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini" id="codigoAccionFormativa" name="codigoAccionFormativa" disabled="disabled">
                          <span class="add-on">/</span>
                          <input type="text" class="input-mini" id="codigoInterno" name="codigoInterno">
						  <input type="hidden" id="tipoFormacion" name="tipoFormacion">
                        </div>
                      </div> <!-- /controls -->         
                    </div> <!-- /control-group -->

                    
                    <div class="control-group">                     
                      <label class="control-label" for="tutor">Tutor:</label>
                      <div class="controls">
                        <?php selectTutor(); ?>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fechaInicio">Fecha de inicio:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaInicio" name="fechaInicio" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="fechaFin">Fecha de finalización:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaFin" name="fechaFin" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <?php
                      $opciones=array('NO', 'SI');
					  campoOculto('NO','llamadaBienvenida');
					  campoOculto('NO','llamadaSeguimiento');
					  campoOculto('NO','llamadaFinalizacion');
                      campoSelect('bonificado', '¿Bonificado?', $opciones, $opciones, '', 'selectpicker show-tick anchoAuto');
					  campoSelect('finalizado', '¿Finalizado?', $opciones, $opciones, '', 'selectpicker show-tick anchoAuto');
					  $consultaUser="SELECT codigo, CONCAT(apellidos, ', ', nombre) AS texto FROM usuarios WHERE activoUsuario='SI';";
                      campoSelectConsulta('comercial', 'Comercial', $consultaUser);
                    ?>
					
					<div class="control-group">                     
					  <label class="control-label" for="formacionCurso">Formación:</label>
					  <div class="controls">
						
						<select name="formacionCurso" id="formacionCurso" class='selectpicker show-tick'>
						  <option value="NO" data-content="<span class='label'>No tiene</span>"></option>
						  <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>"></option>
						  <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>"></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>"></option>
						</select>

					  </div> <!-- /controls -->       
					</div> <!-- /control-group -->
					
				     <div class="control-group">                     
                      <label class="control-label" for="mediosPropios">Medios para la formación:</label>
                      <div class="controls">
                        <label class="checkbox inline">
                          <input type="checkbox" name="mediosPropios" id="mediosPropios" value="SI"> Propios de la empresa bonificada
                        </label>
                        <br>
                        <label class="checkbox inline">
                          <input type="checkbox" name="mediosEntidad" id="mediosEntidad" value="SI"> De la entidad organizadora
                        </label>
                        <br>
                        <label class="checkbox inline">
                          <input type="checkbox" name="mediosCentro" id="mediosCentro" value="SI"> Del centro de formación o entidad formadora externa
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoSelect('medios','Medios',array('Entidad inscrita','Entidad externa'),array('EntidadInscrita','EntidadOrganizadora'));
					?>
				
				<?php
					campoOculto('B65775850','cifTutoria');
					campoOculto('','centroTutoria');
					campoOculto('931 79 07 03','tlfTutoria');
					campoOculto('C/ Pablo Iglesias, 58','domicilioTutoria');
					campoOculto('08302','cpTutoria');
					campoOculto('Mataró','poblacionTutoria');
					campoOculto('','cif');
					campoOculto('','centro');
					campoOculto('','tlf');
					campoOculto('','domicilio');
					campoOculto('','cp');
					campoOculto('','poblacion');
					campoOculto('publico','titularidad');
					campoOculto('','horaInicioFormacion');
					campoOculto('','horaFinFormacion');
					campoOculto('','horaInicioFormacionTarde');
					campoOculto('','horaFinFormacionTarde');
					campoOculto('','horasFormacion');
					campoOculto('NO','lunesFormacion');
					campoOculto('NO','martesFormacion');
					campoOculto('NO','miercolesFormacion');
					campoOculto('NO','juevesFormacion');
					campoOculto('NO','viernesFormacion');
					campoOculto('NO','sabadoFormacion');
					campoOculto('NO','domingoFormacion');
					echo "<span id='resultadoDos'></span>";
					campoOculto(false,'tutorDistancia');
					campoOculto('','cifDistancia');
					campoOculto('','centroGestorDistancia');
					campoOculto('','tlfDistancia');
					campoOculto('','domicilioDistancia');
					campoOculto('','cpDistancia');
					campoOculto('','poblacionDistancia');
					campoOculto('publico','titularidadDistancia');
					echo "<span id='resultadoTres'></span>";
					campoOculto('SI','informarlt');
					campoOculto('NOINF','informerlt');
					campoOculto('00/00/0000','fechaDiscrepancia');
					campoOculto('SI','resuelto');
				?>
					  
                    </div>
					
					<div id='cajaGestoria'>
						<fieldset class='span3'>
							<?php
								campoTexto('proyecto','Denominación del proyecto');
							?>
						</fieldset>
						<fieldset class='span3'>

						<div class="control-group">                     
						  <label class="control-label" for="formacion">Formación:</label>
						  <div class="controls">
							
							<select name="formacion" id="formacion" class='selectpicker show-tick'>
							  <option value="NO" data-content="<span class='label'>No tiene</span>"></option>
							  <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>"></option>
							  <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>"></option>
							  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>"></option>
							</select>

						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="prl">PRL:</label>
						  <div class="controls">
							
							<select name="prl" id="prl" class='selectpicker show-tick'>
							  <option value="NO" data-content="<span class='label'>No tiene</span>"></option>
							  <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>"></option>
							  <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>"></option>
							  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>"></option>
							</select>

						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="lopd">LOPD:</label>
						  <div class="controls">
							
							<select name="lopd" id="lopd" class='selectpicker show-tick'>
							  <option value="NO" data-content="<span class='label'>No tiene</span>"></option>
							  <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>"></option>
							  <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>"></option>
							  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>"></option>
							</select>

						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="lssi">LSSI:</label>
						  <div class="controls">
							
							<select name="lssi" id="lssi" class='selectpicker show-tick'>
							  <option value="NO" data-content="<span class='label'>No tiene</span>"></option>
							  <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>"></option>
							  <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>"></option>
							  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>"></option>
							</select>

						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="aler">ALER:</label>
						  <div class="controls">
							
							<select name="aler" id="aler" class='selectpicker show-tick'>
							  <option value="NO" data-content="<span class='label'>No tiene</span>"></option>
							  <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>"></option>
							  <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>"></option>
							  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>"></option>
							</select>

						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
                      <label class="control-label" for="web">Web:</label>
                      <div class="controls">
                        
                        <select name="web" id="web" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>"></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>"></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>"></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>"></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					
					<div class="control-group">                     
                      <label class="control-label" for="auditoria1">Auditoría LOPD:</label>
                      <div class="controls">
                        
                        <select name="auditoria1" id="auditoria1" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>"></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>"></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>"></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>"></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="auditoria2">Auditoría PRL:</label>
                      <div class="controls">
                        
                        <select name="auditoria2" id="auditoria2" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>"></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>"></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>"></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>"></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
						
					  </fieldset>
                     <h3 class="apartadoFormulario sinFlotar">Planificación de Hitos</h3>
                    <table class="table table-striped table-bordered" id="tablaHitos">
                      <thead>
                        <tr>
                          <th> Actividad </th>
                          <th> Fecha de finalización prevista </th>
                          <th> Fecha de finalización real </th>
                          <th> Observaciones </th>
                        </tr>
                      </thead>
                      <tbody>
							<?php
								$i=0;								
								$textos=array(
									0=>'PROYECTO DE CONSULTORIA',
									1=>'INSCRIPCIÓN DE LOS FICHEROS',
									2=>'REDACCIÓN DOCUMENTO DE SEGURIDAD'
								);
								while($i<3){
							?>
								<tr>
								  <td><?php campoTextoSolo('actividad'.$i,$textos[$i]); ?></td>
								  <td><?php campoFecha('fechaPrevista'.$i,'',false,true); ?></td>
								  <td><?php campoFecha('fechaReal'.$i,'',false,true); ?></td>
								   <?php areaTextoTabla("observaciones".$i);?>
									
								</tr>
							<?php
								$i++;
								}
							?>
                      </tbody>
                    </table>
                    </div>

					</div>
                    
<!--DIVISIÓN DE PÁGINAS PARA CREACIÓN DE FACTURA DIRECTA CON LA VENTA-->					
					

                    <div class="tab-pane" id="pagina2">
					
						<?php
              $anio=date('Y');
              if(date("Y-m-d") >= $anio.'-12-21' && date("Y-m-d") < ($anio+1).'-01-01'){
                $anio++;
              }
						 //F
  $consulta=consultaBD("SELECT referencia FROM facturacion WHERE concepto='14' AND firma!='3' AND YEAR(fechaEmision) = '".$anio."' ORDER BY referencia DESC LIMIT 1;",true);
  $referencia=mysql_fetch_assoc($consulta);

  $referenciaNueva=$referencia['referencia']+1;
  
  $consulta=consultaBD("SELECT referencia FROM facturacion WHERE concepto!='14' AND YEAR(fechaEmision) = '".$anio."' ORDER BY referencia DESC LIMIT 1;",true);
  $referencia=mysql_fetch_assoc($consulta);
  if($referencia['referencia']==''){
  $referencia['referencia']=0;
  }
  $referenciaNuevaConsultoria=$referencia['referencia']+1;
  
  //S
  $consulta=consultaBD("SELECT referencia FROM facturacion WHERE concepto='14' AND firma='3' AND YEAR(fechaEmision) = '".$anio."' ORDER BY referencia DESC LIMIT 1;",true);
  $referencia=mysql_fetch_assoc($consulta);
  if($referencia['referencia']==''){
  $referencia['referencia']=0;
  }
  $referenciaNuevaFirmaTres=$referencia['referencia']+1;
							  
							  if($referenciaNueva<10){
								$referenciaNueva='0000'.$referenciaNueva;
							  }elseif($referenciaNueva<100){	
								$referenciaNueva='000'.$referenciaNueva;
							  }elseif($referenciaNueva<1000){	
								$referenciaNueva='00'.$referenciaNueva;
							  }elseif($referenciaNueva<10000){	
								$referenciaNueva='0'.$referenciaNueva;
							  }
							  
							  if($referenciaNuevaFirmaTres<10){
								$referenciaNuevaFirmaTres='0000'.$referenciaNuevaFirmaTres;
							  }elseif($referenciaNuevaFirmaTres<100){	
								$referenciaNuevaFirmaTres='000'.$referenciaNuevaFirmaTres;
							  }elseif($referenciaNuevaFirmaTres<1000){	
								$referenciaNuevaFirmaTres='00'.$referenciaNuevaFirmaTres;
							  }elseif($referenciaNuevaFirmaTres<10000){	
								$referenciaNuevaFirmaTres='0'.$referenciaNuevaFirmaTres;
							  }
							
							$colaborador=datosRegistro('colaboradores',$datosCliente['comercial']);
						?>
						
						<div class="control-group">                     
						  <label class="control-label" for="empresa">Empresa:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="empresa" name="empresa" value="<?php echo $datosCliente['empresa']; ?>" disabled='disabled'>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<?php if($datosCliente['tieneColaborador']=='SI'){ ?>
						<div class="control-group">                     
						  <label class="control-label" for="empresa">Colaborador:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="empresa" name="empresa" value="<?php echo $colaborador['empresa']; ?>" disabled='disabled'>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						<?php } ?>
						
						<div class="control-group">                     
						  <label class="control-label" for="contacto">Persona de contacto:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="contacto" name="contacto" value="<?php echo $datosCliente['contacto']; ?>" disabled='disabled'>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="telefono">Teléfono:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="telefono" name="telefono" value="<?php echo $datosCliente['telefono']; ?>" maxlength="9" disabled='disabled'>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="mail">Mail:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="mail" name="mail" value="<?php echo $datosCliente['mail']; ?>" disabled='disabled'>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="referencia">Referencia:</label>
						  <div class="controls">
							<input type="text" class="input-medium" id="referencia" name="referencia" value="<?php echo $referenciaNueva; ?>">/<?php echo substr($anio,2); ?>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->

						<?php 
							campoOculto($codigo,'codigoCliente');
							campoOculto('NO','enviada');
							campoOculto('NO','enviadaCliente');
							campoOculto('NO','devuelta');
							campoOculto(date('Y-m-d H:i:s'),'insercion');
							campoOculto('00/00/0000','fechaPago');
							campoOculto('00/00/0000','fechaEnvioDocumentacion');
							campoOculto('00/00/0000','fechaRecepcion');
							campoOculto('NO','pagadoColaborador');
							campoOculto('NULL','motivoDevolucion');
							campoOculto('','resolucionFactura');
              campoOculto($anio,'anio');
						?>
						

						<div class="control-group">                     
						  <label class="control-label" for="fechaEmision">Fecha emisión:</label>
						  <div class="controls">
							<input type="text" class="input-small datepicker hasDatepicker" id="fechaEmision" name="fechaEmision" value="<?php imprimeFecha(); ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->

						<?php
              campoFecha('fechaVencimiento','Fecha vencimiento');
							campoSelect('tipoFacturaComercial','Tipo de factura',array('Cartera','Venta nueva','Auditoría','Televenta'),array('CARTERA','NUEVA','AUDITORIA','TELEVENTA'));				
						?>
						
					<h3 class="apartadoFormulario sinFlotar">Vencimientos</h3>
            Se guardarán aquellos vencimientos con importes definidos
              <center>
              <table class="table table-striped table-bordered datatable" id="tablaVencimientos">
              <thead>
                <tr>
                <th> Fecha </th>
                <th> Importe € </th>
                <th> Enviada </th>
                <th> Cobrada </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                <?php
                  campoFechaTabla('fecha0');
                  campoTextoTabla('importe0','','input-small pagination-right');
                  campoSelect('enviada0','',array('No','Si'),array('NO','SI'),false,'span2','data-live-search="true"',1);
                  campoSelect('cobrada0','',array('No','Si'),array('NO','SI'),false,'span2','data-live-search="true"',1);
                ?>
                </tr>
              </tbody>
              </table>
              <br/>
              </center>
              <center>
              <button type="button" class="btn btn-success" onclick="insertaFila('tablaVencimientos');"><i class="icon-plus"></i> Añadir Vencimiento</button> 
              </center>
              <br/>

						<?php
							$consulta="SELECT codigo, firma AS texto FROM firmas ORDER BY codigo;";
							campoSelectConsulta('firma','Empresa (Firma)',$consulta);
							echo "<div id='tipoFacturaDescarga'>";
								campoSelect('tipoFactura','Tipo de documento',array('Mantenimiento','Auditoría','Consultoría','LSSI','PRL','APPCC-Alérgenos','Web legalizada','Dominio y correo electrónico','Dominio','E-commerce'), array('mantenimiento','auditoria','consultoria','lssi','prl','alergenos','web','dominiocorreo','dominio','commerce'));
							echo "</div>";
							
							campoTextoSimbolo('gastosOrganizacion','Gastos de organización','€');
							campoTextoSimbolo('gastosImparticion','Gastos de impartición','€');
						?>


						<div class="control-group">                     
						  <label class="control-label" for="coste">Coste:</label>
						  <div class="controls">
							<div class="input-prepend input-append">
							  <input type="text" class="input-small pagination-right" id="coste" name="coste">
							  <span class="add-on">€</span>
							</div>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->

						
						<?php
							campoSelect('formaPago','Forma de pago',array('Transferencia','Efectivo','Cheque','Domiciliación bancaria','Tarjeta'),array('transferencia','efectivo','cheque','domiciliacion','tarjeta'),$datosCliente);
						?>





						<div class="control-group">                     
						  <label class="control-label">¿Cobrada?</label>  
							<div class="controls">
							<label class="radio inline">
							  <input type="radio" name="cobrada" value="SI"> Si
							</label>

							<label class="radio inline">
							  <input type="radio" name="cobrada" value="NO" checked="checked"> No
							</label>

						  </div>  
						</div> <!-- /control-group -->
						
						<?php
							areaTexto('observaciones','Observaciones','','areaInforme');
						?>
						


                     <br>
                    
                      
                    
					</div>
				
					<div class="form-actions">
                      <button type="button" class="btn btn-primary" id="registrar"><i class="icon-ok"></i> Registrar venta</button> 
					  <button type="button" class="btn btn-success" id="completar"><i class="icon-edit"></i> Completar alumnos</button>
                      <a href="ventasGeneral.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
				
				</div>
					
			  </div>
					
			</div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
	  
	  
<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<!-----------------------------------------------------------------FIN PAGINACIÓN PARA CREACIÓN DE FACTURAS----------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->

	  
	  
	  
	  <!--Popup -->
		  <div class="span6 hide ventanasFlotantes" id="ventanaFlotanteDos">
			<div class="widget cajaSelect">
				<div class="widget-header"> <i class="icon-share-alt icon-book"></i><i class="icon-chevron-right"></i><i class="icon-plus-sign"></i>
				  <h3>Nueva acción formativa</h3>
				</div>
				<!-- /widget-header -->
				<div class="widget-content">
				  
				  <div class="tab-pane" id="formcontrols">
					<form id="edit-profile" class="form-horizontal" method="post">
					  <fieldset>
						<div class="control-group">                     
						  <label class="control-label" for="codigoInternoAccion">Acción:</label>
						  <div class="controls">
							<input type="text" class="input-mini" id="codigoInternoAccion" name="codigoInternoAccion">
							<?php 
								$consulta=consultaBD("SELECT codigoInterno FROM accionFormativa;",true);
								$datosCodigos=mysql_fetch_assoc($consulta);
								$i=0;
								echo "<div id='div-padre'>";
								while(isset($datosCodigos['codigoInterno'])){
									divOculto($datosCodigos['codigoInterno'],'codigoInternoAccion'.$i);
									$i++;
									$datosCodigos=mysql_fetch_assoc($consulta);
								}
								echo "</div>";
							?>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->

						<div class="control-group">                     
						  <label class="control-label" for="denominacion">Denominación:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="denominacion" name="denominacion">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->


						<?php campoTexto('grupoAcciones','Grupo','','input-mini'); ?>



						<div class="control-group">                     
						  <label class="control-label" for="tipoAccion">Tipo de acción:</label>
						  <div class="controls">
							<select name="tipoAccion" id='tipoAccion' class='selectpicker input-large'>
							  <option value="PROPIA">Propia</option>
							  <option value="VINCULADA">Vinculada a la obtención de un Cert. de Profesionalidad</option>
							</select>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="nivelAccion">Nivel de acción:</label>
						  <div class="controls">
							<select name="nivelAccion" id='nivelAccion' class='selectpicker input-large'>
							  <option value="BASICO">Básico</option>
							  <option value="SUPERIOR">Superior</option>
							</select>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->


						<div class="control-group">                     
						  <label class="control-label" for="contenidos">Contenidos:</label>
						  <div class="controls">
										<textarea name="contenidos" id='contenidos' class="areaTextoAmplia"></textarea>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->


						<div class="control-group">                     
						  <label class="control-label" for="objetivos">Objetivos:</label>
						  <div class="controls">
							<textarea name="objetivos" id='objetivos' class="areaTextoAmplia"></textarea>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="horas">Número de Horas:</label>
						  <div class="controls">
							<input type="text" class="input-mini pagination-right" id="horas" name="horas">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="modalidad">Modalidad:</label>
						  <div class="controls">
							
							<select name="modalidad" id='modalidad' class='selectpicker show-tick'>
							  <option value="PRESENCIAL">Presencial</option>
							  <option value="DISTANCIA">A distancia</option>
							  <option value="MIXTA">Mixta</option>
							  <option value="TELE">Teleformación</option>
							  <option value="WEBINAR">Webinar</option>
							</select>

						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->




						<div class="form-actions">
						  <button id='registrarAccion' type="button" class="btn btn-primary"><i class="icon-ok"></i> Registrar acción formativa</button> 
						  <button type="button" class="btn" id="cancelar"><i class="icon-remove"></i> Cancelar</button> 
						</div> <!-- /form-actions -->
					  </fieldset>
					</form>
					</div>


				</div>
				<!-- /widget-content --> 
			  </div>
		  </div>
		  <!--Fin Popup -->
	  
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/filasVentas.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/bootstrap-timepicker.js"></script>

<script type="text/javascript" src="js/full-calendar/jquery-ui.custom.min.js"></script><!-- Habilita el drag y el resize -->
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('#fechaVencimiento').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $('#fecha0').val($(this).val());
    });
    $('#producto').css('display','none');
    $('#cajaAlumnos').css('display','block');
	$('#cajaGestoria').css('display','none');
	$('#ocultoComision').css('display','none');


    //Para tabla alumnos
    $('select[name=concepto]').change(function(){//Lo mismo pero para el select de servicios
      if($(this).val()=='14'){
        $('#cajaAlumnos').css('display','block');
		$('#cajaGestoria').css('display','none');
      }
      else{
        $('#cajaAlumnos').css('display','none'); 
		$('#cajaGestoria').css('display','block');
      }
    });
    //Fin tabla alumnos
	
	$('#selectServicio').change(function(){
		if($(this).val()!='14'){
			$('#completar').css('display','none');
		}else{
			$('#completar').css('display','inline');
		}
	});

	$('#registrar').click(function(){
		registra();
    });
	$('#completar').click(function(){
		completa();
    });
	
	//PARTE NUEVA PARA CURSOS
		$('.timepicker').timepicker({showMeridian: false,defaultTime: false});
		$('#ventanaFlotanteDos').draggable();
		listadoAcciones();
		
		/*REGISTRO POR AJAX*/
		$('#registrarAccion').click(function(){
			var codigoInterno=$('#codigoInternoAccion').val();
			var denominacion=$('#denominacion').val();
			var grupoAcciones=$('#grupoAcciones').val();
			var tipoAccion=$('#tipoAccion').val();
			var contenidos=$('#contenidos').val();
			var objetivos=$('#objetivos').val();
			var horas=$('#horas').val();
			var modalidad=$('#modalidad').val();
			var nivelAccion=$('#nivelAccion').val();
			$.ajax({
			 type: "POST",
			 url: "listadosajax/insercionAccion.php",
			 data: "codigoInterno=" + codigoInterno + "&denominacion=" + denominacion + "&grupoAcciones=" + grupoAcciones + "&tipoAccion=" + tipoAccion + "&contenidos=" + contenidos + 
			 "&objetivos=" + objetivos + "&horas=" + horas + "&modalidad=" + modalidad + "&nivelAccion=" + nivelAccion ,
			 beforeSend:function () {
				   $("#resultado").html('<div class="control-group"><div class="controls"><i class="icon-refresh icon-spin"></i></div></div>');
			 },
			 success: function(response){
					 listadoAcciones();
			 }
			});
			cierraVentana('ventanaFlotanteDos');
		});
		
		$('#cancelar').click(function(){
			cierraVentana('ventanaFlotanteDos');
		});
    $('#coste').focusout(function(){
    $('#importe0').val($(this).val());
  });
		
		$('#divFecha').css('display','none');
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
		$('#informerlt').change(function(){
		   if($(this).val()=='DIS'){
			$('#divFecha').css('display','block');
		  }else{
			$('#divFecha').css('display','none');
		  }

		});
	//FIN PARTE NUEVA PARA CURSOS

	//PARTE NUEVA TUTORES
		var codigoTutor=$("select[name=tutor]").val();
		diasTutorias(codigoTutor);
		
		var codigoTutor=$("select[name=tutorDistancia]").val();
		diasTutoriasDistancia(codigoTutor);
		
		$("select[name=tutor]").change(function(){
			var codigoTutor=$("select[name=tutor]").val();
			diasTutorias(codigoTutor);
		});
		
		$("select[name=tutorDistancia]").change(function(){
			var codigoTutor=$("select[name=tutorDistancia]").val();
			diasTutoriasDistancia(codigoTutor);
		});
	
	//FIN PARTE NUEVA TUTORES
	
	//PARTE NUEVA FACTURAS
	$('#precio').blur(function(){
     var precio=$('#precio').val();
     $('#coste').val(precio);
     $('#importe0').val(precio);
  });

  $('#coste').focusout(function(){
    $('#importe0').val($(this).val());
  });
	$("#tipoFacturaDescarga").css('display','none');
	$('select[name=concepto]').change(function() {
        if (this.value == '14') {
			$("#tipoFacturaDescarga").css('display','none');
			<?php 
				echo "$('#referencia').val('$referenciaNueva');";
			?>
        }
        else {
			$("#tipoFacturaDescarga").css('display','block');
			<?php 
				echo "$('#referencia').val('$referenciaNuevaConsultoria');";
			?>
        }
		if(this.value == 'prl'){
			$('#tipoFactura').val('prl');
			$('.selectpicker').selectpicker('refresh');
		}
		if(this.value == 'alergenos'){
			$('#tipoFactura').val('alergenos');
			$('.selectpicker').selectpicker('refresh');
		}
		if(this.value == 'web'){
				$('#tipoFactura').val('web');
				$('.selectpicker').selectpicker('refresh');
		}
		if(this.value == 'lopd'){
			$('#tipoFactura').val('commerce');
			$('.selectpicker').selectpicker('refresh');
		}
    });
	
	$('select[name=firma]').change(function() {
		if (this.value == '3' && $('select[name=concepto]').val()=='14') {
			<?php 
				echo "$('#referencia').val('$referenciaNuevaFirmaTres');";
			?>
        }
        else {
			if($('select[name=concepto]').val()=='14'){
				<?php 
					echo "$('#referencia').val('$referenciaNueva');";
				?>
			}
        }
	});
	//FIN PARTE NUEVA FACTURAS
  });
  
	function registra(){	
		$('.formularioVenta').attr('action', "ventasGeneral.php").submit();
	}
	
	function completa(){		
		$('.formularioVenta').attr('action', "completarAlumnosGeneral.php").submit();
	}
	
	function abreVentana(id){
		$('#'+id).removeClass('hide');
	  }

	  function cierraVentana(id){
		$('#'+id).addClass('hide');
	  }
  
	
	function listadoAcciones(){
	 $.ajax({
         url: "listadosajax/listadoAcciones.php",
		 beforeSend:function () {
			   $("#resultado").html('<div class="control-group"><div class="controls"><i class="icon-refresh icon-spin"></i></div></div>');
         },
         success: function(response){
               $("#resultado").html(response);
         }
		});
	}
	
	 function diasTutorias(codigoTutor){
	 $.ajax({
		 type: "POST",
         url: "listadosajax/diasTutoriasOculto.php",
		 data: "codigoTutor=" + codigoTutor,
		 beforeSend:function () {
			   $("#resultadoDos").html('<div class="control-group"><div class="controls"><i class="icon-refresh icon-spin"></i></div></div>');
         },
         success: function(response){
               $("#resultadoDos").html(response);
         }
    });
  }
  
  function diasTutoriasDistancia(codigoTutor){
	 $.ajax({
		 type: "POST",
         url: "listadosajax/diasTutoriasDistanciaOculto.php",
		 data: "codigoTutor=" + codigoTutor,
		 beforeSend:function () {
			   $("#resultadoTres").html('<div class="control-group"><div class="controls"><i class="icon-refresh icon-spin"></i></div></div>');
         },
         success: function(response){
               $("#resultadoTres").html(response);
         }
    });
  }
</script>
