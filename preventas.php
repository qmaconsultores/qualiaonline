<?php
  $seccionActiva=23;
  include_once('cabecera.php');

  if(isset($_POST['formacionPrecio'])){
    $servicios=array('formacionPrecio','lssiPrecio','alergenosPrecio','prlPrecio','plataformaWebPrecio','auditoriaLopdPrecio','consultoriaLopdPrecio','auditoriaPrlPrecio','webLegalizadaPrecio','dominioCorreoPrecio','dominioPrecio','ecommercePrecio');
    foreach ($servicios as $servicio) {
      $_POST[$servicio]=str_replace(',', '.', $_POST[$servicio]);
    }
  }
  
  $res=false;
  if(isset($_POST['codigo'])){
    $res=actualizaDatos('preventas');
    $res=$res && actualizaTareaPreventa();
    if(isset($_POST['desbloqueado'])){
      $res=modificarVentas();
    }
  }
  elseif(isset($_POST['codigoTarea'])){
    $res=insertaDatos('preventas');
    $res=$res && actualizaTareaPreventa();
  }
  elseif(isset($_GET['codigoEvaluado'])){
    $res=evaluaPreventa($_GET['codigoEvaluado'],$_GET['evaluacion']);
  }
  elseif(isset($_GET['codigoEliminar'])){
    $res=eliminaPreventa($_GET['codigoEliminar']);
  }


  if(!isset($_GET['estado']) && $_SESSION['codigoS']=='14'){
    if(!isset($_GET['esta'])){
      $_GET['estado']=6;
    }
  }

  $where='WHERE 1=1';
  if($_SESSION['tipoUsuario']=='COMERCIAL' && $_SESSION['codigoS']!='120' && $_SESSION['codigoS']!='172'){
      $where='INNER JOIN tareas ON preventas.codigoTarea=tareas.codigo '.$where.' AND tareas.codigoUsuario='.$_SESSION['codigoS'];
  }

  $filtro=false;
  $get='';
  $mes=0;
  $anio='';
  if(isset($_POST['mes']) || isset($_GET['mes'])){
    $mes=isset($_POST['mes'])?$_POST['mes']:$_GET['mes'];
    $anio=isset($_POST['anio'])?$_POST['anio']:$_GET['anio'];
    if($mes>0){
      $filtro='MONTH(fecha)="'.$mes.'"';
      $get.='&mes='.$mes;
    } else {
      $get.='&mes='.date('m');
    }
    if($anio!=''){
      if($filtro==''){
        $filtro='YEAR(fecha)="'.$anio.'"';
      } else {
        $filtro.=' AND YEAR(fecha)="'.$anio.'"';
      }
      $get.='&anio='.$anio;
    } else {
      if($filtro==''){
        $filtro='YEAR(fecha)="'.date('Y').'"';
      } else {
        $filtro.=' AND YEAR(fecha)="'.date('Y').'"';
      }
      $get.='&anio='.date('Y');
    }
  }
  $estadisticas=estadisticasPreventas2($where,$filtro);

  /*$preventas=consultaBD("SELECT preventas.*, tareas.codigoCliente FROM preventas INNER JOIN tareas ON preventas.codigoTarea=tareas.codigo",true);
  while($preventa=mysql_fetch_assoc($preventas)){
    echo "UPDATE preventas SET codigoCliente=".$preventa['codigoCliente']." WHERE codigo=".$preventa['codigo'].";<br/>";
  }*/
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content estadisticasPreventas">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de preventas</h6>
                   <?php
                   if(!isset($_GET['estado'])){
                      echo '<div id="big_stats" class="cf">';
                        echo '<div class="stat"> <i class="icon-tags"></i> <span class="value">'.$estadisticas['total'].'</span> <br>Total preventa</div>';
                        echo '<div class="stat"> <i class="icon-money"></i> <span class="value">'.$estadisticas['totalImporte'].'</span> <br>Importe</div><hr></hr>';
                      echo '</div>';
                   }

                   if(!isset($_GET['estado']) || $_GET['estado']==1){
                      echo '<div id="big_stats" class="cf">';
                        echo '<div class="stat"> <i class="icon-file-o"></i> <span class="value">'.$estadisticas['noDocumentos'].'</span> <br>Sin documentos</div>';
                       echo '<div class="stat"> <i class="icon-money"></i> <span class="value">'.$estadisticas['noDocumentosImporte'].'</span> <br>Importe</div>';
                      echo '</div>';
                   }

                   if(!isset($_GET['estado']) || $_GET['estado']==2){
                      echo '<div id="big_stats" class="cf">';                    
                        echo '<div class="stat"> <i class="icon-file-text"></i> <span class="value">'.$estadisticas['siDocumentos'].'</span> <br>Con documentos</div>';
                        echo '<div class="stat"> <i class="icon-money"></i> <span class="value">'.$estadisticas['siDocumentosImporte'].'</span> <br>Importe</div>';
                      echo '</div>';
                   }

                   if(isset($_GET['estado']) && ($_GET['estado']==2 || $_GET['estado']==3)){
                      echo '<div id="big_stats" class="cf">';                    
                        echo '<div class="stat"> <i class="icon-remove"></i> <span class="value">'.$estadisticas['rechazadas'].'</span> <br>Rechazadas</div>';
                        echo '<div class="stat"> <i class="icon-money"></i> <span class="value">'.$estadisticas['rechazadasImporte'].'</span> <br>Importe</div>';
                      echo '</div>';
                   }

                   if(isset($_GET['estado']) && ($_GET['estado']==2 || $_GET['estado']==4)){
                      echo '<div id="big_stats" class="cf">';                    
                        echo '<div class="stat"> <i class="icon-check-empty"></i> <span class="value">'.$estadisticas['noVerificadas'].'</span> <br>No verificadas</div>';
                        echo '<div class="stat"> <i class="icon-money"></i> <span class="value">'.$estadisticas['noVerificadasImporte'].'</span> <br>Importe</div>';
                      echo '</div>';
                   }

                   if(isset($_GET['estado']) && ($_GET['estado']==2 || $_GET['estado']==5)){
                      echo '<div id="big_stats" class="cf">';                    
                        echo '<div class="stat"> <i class="icon-check"></i> <span class="value">'.$estadisticas['siVerificadas'].'</span> <br>Verificadas</div>';
                        echo '<div class="stat"> <i class="icon-money"></i> <span class="value">'.$estadisticas['siVerificadasImporte'].'</span> <br>Importe</div>';
                      echo '</div>';
                   }

                   if(isset($_GET['estado']) && ($_GET['estado']==5 || $_GET['estado']==6)){
                      echo '<div id="big_stats" class="cf">';                    
                        echo '<div class="stat"> <i class="icon-ban"></i> <span class="value">'.$estadisticas['noFacturado'].'</span> <br>No facturado</div>';
                        echo '<div class="stat"> <i class="icon-money"></i> <span class="value">'.$estadisticas['noFacturadoImporte'].'</span> <br>Importe</div>';
                      echo '</div>';
                   }

                   if(isset($_GET['estado']) && ($_GET['estado']==5 || $_GET['estado']==7)){
                      echo '<div id="big_stats" class="cf">';                    
                        echo '<div class="stat"> <i class="icon-eur"></i> <span class="value">'.$estadisticas['facturado'].'</span> <br>Facturado</div>';
                        echo '<div class="stat"> <i class="icon-money"></i> <span class="value">'.$estadisticas['facturadoImporte'].'</span> <br>Importe</div>';
                      echo '</div>';
                   }
                   ?>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Preventas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <?php
                  $action='preventas.php';
                    if(isset($_GET['estado'])){
                      $action.='?estado='.$_GET['estado'];
                    }
                    echo '<form id="filtro" action="'.$action.'" method="post">';
                    campoSelect('mes','Mes',array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'),array(0,01,02,03,04,05,06,07,08,09,10,11,12),$mes,'selectpicker span2 show-tick');
                    $anios=array('');
                    for($i=2017;$i<=date('Y');$i++){
                      array_push($anios, $i);
                    }
                    campoSelect('anio','Año',$anios,$anios,$anio,'selectpicker span1 show-tick');
                    echo '</form>';
                    if(!!$filtro){
                      echo '<a href="preventas.php" class="btn btn-propio"><i class="icon icon-remove"></i> Quitar filtros</a><br/><br/>';
                    }
                    if(isset($_GET['estado'])){
                      echo '<a href="preventas.php?esta=0'.$get.'" class="shortcut"><i class="shortcut-icon icon-tags"></i><span class="shortcut-label">Total</span> </a>';
                    }
                    if(!isset($_GET['estado']) || (isset($_GET['estado']) && $_GET['estado']!=1)){
                      echo '<a href="preventas.php?estado=1'.$get.'" class="shortcut"><i class="shortcut-icon icon-file-o"></i><span class="shortcut-label">Sin documentos</span> </a>';
                    }
                    if(!isset($_GET['estado']) || (isset($_GET['estado']) && $_GET['estado']!=2)){
                      echo '<a href="preventas.php?estado=2'.$get.'" class="shortcut"><i class="shortcut-icon icon-file-text"></i><span class="shortcut-label">Con documentos</span> </a>';
                    }
                    if(!isset($_GET['estado']) || (isset($_GET['estado']) && $_GET['estado']!=3)){
                      echo '<a href="preventas.php?estado=3'.$get.'" class="shortcut"><i class="shortcut-icon icon-remove"></i><span class="shortcut-label">Rechazadas</span> </a>';
                    }
                    if(!isset($_GET['estado']) || (isset($_GET['estado']) && $_GET['estado']!=4)){
                      echo '<a href="preventas.php?estado=4'.$get.'" class="shortcut"><i class="shortcut-icon icon-check-empty"></i><span class="shortcut-label">No verificadas</span> </a>';
                    }
                    if(!isset($_GET['estado']) || (isset($_GET['estado']) && $_GET['estado']!=5)){
                      echo '<a href="preventas.php?estado=5'.$get.'" class="shortcut"><i class="shortcut-icon icon-check"></i><span class="shortcut-label">Verificadas</span> </a>';
                    }
                    if(!isset($_GET['estado']) || (isset($_GET['estado']) && $_GET['estado']!=6)){
                      echo '<a href="preventas.php?estado=6'.$get.'" class="shortcut"><i class="shortcut-icon icon-ban"></i><span class="shortcut-label">No facturado</span> </a>';
                    }
                    if(!isset($_GET['estado']) || (isset($_GET['estado']) && $_GET['estado']!=7)){
                      echo '<a href="preventas.php?estado=7'.$get.'" class="shortcut"><i class="shortcut-icon icon-eur"></i><span class="shortcut-label">Facturado</span> </a>';
                    }
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        
        <?php
          mensajeResultado('codigoTarea',$res,'Preventa');//Fusionamos el mensaje de inserción y actualización
          mensajeResultado('elimina',$res,'Preventa', true);
        ?>
		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Preventas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>

                    <th> Cliente </th>
                    <th> Comercial </th>
                    <th> Fecha </th>
                    <th> Servicios </th>
                    <th> Total </th>
                    <?php
                    if(!isset($_GET['estado'])){ ?>
                      <th> Documentos </th>
                    <?php
                    }
                    if(!isset($_GET['estado']) || (isset($_GET['estado']) && $_GET['estado'] < 3)){ ?>
                      <th> Verificada </th>
                    <?php } ?>
                    <th class="centro"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
                    $where='';
                    if(isset($_GET['estado']) && $_GET['estado']==1){
                      $where='WHERE documentos="NO"';
                    } else if(isset($_GET['estado']) && $_GET['estado']==2){
                      $where='WHERE documentos="SI"';
                    } else if(isset($_GET['estado']) && $_GET['estado']==3){
                      $where='WHERE aceptado="NO"';
                    } else if(isset($_GET['estado']) && $_GET['estado']==4){
                      $where='WHERE documentos="SI" AND verificada="NO" AND aceptado!="NO"';
                    } else if(isset($_GET['estado']) && $_GET['estado']==5){
                      $where='WHERE documentos="SI" AND verificada="SI"';
                    } else if(isset($_GET['estado']) && $_GET['estado']==6){
                      $where='WHERE documentos="SI" AND verificada="SI" AND aceptado="SINEVALUAR"';
                    } else if(isset($_GET['estado']) && $_GET['estado']==7){
                      $where='WHERE documentos="SI" AND verificada="SI" AND aceptado="SI"';
                    }

                    if($_SESSION['tipoUsuario']=='COMERCIAL' && $_SESSION['codigoS']!='120' && $_SESSION['codigoS']!='172'){
                      $where.='AND tareas.codigoUsuario='.$_SESSION['codigoS'];
                    }

                    /*if(!isset($_GET['aceptado']) || (isset($_GET['aceptado']) && $_GET['aceptado']=='SINEVALUAR')){
                      if($_SESSION['codigoS']=='29'){
                        $where.=' AND verificada="NO"';
                      }

                      if($_SESSION['codigoS']=='14'){
                        $where.=' AND verificada="SI"';
                      }
                    }*/
          					imprimePreventas($where,$filtro);
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript">
    $('select').change(function(){
      $('form#filtro').submit();
    });

    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes un servicio.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('preventas.php',valoresChecks,'post');
      }

    

    });
</script>