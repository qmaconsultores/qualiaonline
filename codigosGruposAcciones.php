<option value='001-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='001-00'){ echo 'selected="selected"';} ?> >001-00 Actualización en docencia en general</option>
<option value='002-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='002-00'){ echo 'selected="selected"';} ?> >002-00 Administración de personal en general</option>
<option value='003-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='003-00'){ echo 'selected="selected"';} ?> >003-00 Secretariado y otros trabajos auxiliares de oficina en general</option>
<option value='003-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='003-01'){ echo 'selected="selected"';} ?> >003-01 Trabajos auxiliares de Oficina</option>
<option value='003-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='003-02'){ echo 'selected="selected"';} ?> >003-02 Atención telefónica y recepción</option>
<option value='003-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='003-03'){ echo 'selected="selected"';} ?> >003-03 Secretariado</option>
<option value='004-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='004-00'){ echo 'selected="selected"';} ?> >004-00 Almacenaje, Stocks y Envíos en general</option>
<option value='004-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='004-01'){ echo 'selected="selected"';} ?> >004-01 Operación de carretillas</option>
<option value='005-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='005-00'){ echo 'selected="selected"';} ?> >005-00 Análisis de riesgos-banca en general</option>
<option value='006-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='006-00'){ echo 'selected="selected"';} ?> >006-00 Análisis y Control de Costes en general</option>
<option value='007-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='007-00'){ echo 'selected="selected"';} ?> >007-00 Análisis y Control Financiero en general</option>
<option value='008-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='008-00'){ echo 'selected="selected"';} ?> >008-00 Análisis y ensayos de laboratorio en general</option>
<option value='008-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='008-01'){ echo 'selected="selected"';} ?> >008-01 Trabajos auxiliares en  laboratorios de química industrial</option>
<option value='008-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='008-02'){ echo 'selected="selected"';} ?> >008-02 Análisis en laboratorios de química industrial</option>
<option value='008-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='008-03'){ echo 'selected="selected"';} ?> >008-03 Trabajos auxiliares en laboratorios de industrias alimentarias</option>
<option value='009-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='009-00'){ echo 'selected="selected"';} ?> >009-00 Atención al Cliente/Calidad Servicio en general</option>
<option value='009-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='009-01'){ echo 'selected="selected"';} ?> >009-01 Información al cliente</option>
<option value='009-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='009-02'){ echo 'selected="selected"';} ?> >009-02 Calidad de servicio</option>
<option value='009-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='009-03'){ echo 'selected="selected"';} ?> >009-03 Atención al cliente mediante TICs</option>
<option value='010-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='010-00'){ echo 'selected="selected"';} ?> >010-00 Atención al paciente o usuario de servicios sanitarios en general</option>
<option value='010-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='010-01'){ echo 'selected="selected"';} ?> >010-01 Atención al paciente o usuario hospitalario</option>
<option value='010-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='010-02'){ echo 'selected="selected"';} ?> >010-02 Atención extrahospitalaria: farmacia, ortopedia, etc.</option>
<option value='011-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='011-00'){ echo 'selected="selected"';} ?> >011-00 Auditoría ambiental en general</option>
<option value='013-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='013-00'){ echo 'selected="selected"';} ?> >013-00 Auditoría económico financiera en general</option>
<option value='014-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='014-00'){ echo 'selected="selected"';} ?> >014-00 Auditoría informática en general</option>
<option value='015-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='015-00'){ echo 'selected="selected"';} ?> >015-00 Autómatas programables y robótica en general</option>
<option value='016-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='016-00'){ echo 'selected="selected"';} ?> >016-00 Automatismos industriales en general</option>
<option value='016-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='016-01'){ echo 'selected="selected"';} ?> >016-01 Instalación de automatismos</option>
<option value='017-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='017-00'){ echo 'selected="selected"';} ?> >017-00 Cuidados sanitarios auxiliares en general</option>
<option value='017-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='017-01'){ echo 'selected="selected"';} ?> >017-01 Movilización y traslado de pacientes</option>
<option value='017-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='017-02'){ echo 'selected="selected"';} ?> >017-02 Cuidados auxiliares de transporte sanitario</option>
<option value='017-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='017-03'){ echo 'selected="selected"';} ?> >017-03 Cuidados auxiliares de enfermería hospitalaria</option>
<option value='017-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='017-04'){ echo 'selected="selected"';} ?> >017-04 Cuidados auxiliares en geriatría</option>
<option value='017-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='017-05'){ echo 'selected="selected"';} ?> >017-05 Cuidados auxiliares en salud mental y toxicomanías</option>
<option value='017-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='017-06'){ echo 'selected="selected"';} ?> >017-06 Cuidados auxiliares en rehabilitación</option>
<option value='017-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='017-07'){ echo 'selected="selected"';} ?> >017-07 Higiene dental</option>
<option value='017-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='017-08'){ echo 'selected="selected"';} ?> >017-08 Prótesis dental</option>
<option value='018-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='018-00'){ echo 'selected="selected"';} ?> >018-00 Bibliotecas, Archivos y Documentación en general</option>
<option value='018-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='018-01'){ echo 'selected="selected"';} ?> >018-01 Documentación en medios de comunicación</option>
<option value='019-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='019-00'){ echo 'selected="selected"';} ?> >019-00 Calderería industrial en general</option>
<option value='019-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='019-01'){ echo 'selected="selected"';} ?> >019-01 Tubería industrial</option>
<option value='019-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='019-02'){ echo 'selected="selected"';} ?> >019-02 Trabajos auxiliares de calderería</option>
<option value='019-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='019-03'){ echo 'selected="selected"';} ?> >019-03 Calderería- Tubería</option>
<option value='019-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='019-04'){ echo 'selected="selected"';} ?> >019-04 Técnicas de calderería</option>
<option value='024-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='024-00'){ echo 'selected="selected"';} ?> >024-00 Hostelería-Servicio de comidas y bebidas en general</option>
<option value='024-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='024-01'){ echo 'selected="selected"';} ?> >024-01 Jefatura de salas de restauración</option>
<option value='024-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='024-02'){ echo 'selected="selected"';} ?> >024-02 Servicio en restaurantes y bares</option>
<option value='024-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='024-03'){ echo 'selected="selected"';} ?> >024-03 Presentación de vinos y bebidas</option>
<option value='024-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='024-04'){ echo 'selected="selected"';} ?> >024-04 Presentación de Buffetes</option>
<option value='024-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='024-05'){ echo 'selected="selected"';} ?> >024-05 Coctelería</option>
<option value='025-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='025-00'){ echo 'selected="selected"';} ?> >025-00 Servicios de Juegos de Azar en general</option>
<option value='025-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='025-01'){ echo 'selected="selected"';} ?> >025-01 Gestión de apuestas en casinos y mesas de juego</option>
<option value='025-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='025-02'){ echo 'selected="selected"';} ?> >025-02 Operación de apuestas</option>
<option value='026-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-01'){ echo 'selected="selected"';} ?> >026-01 Estadística</option>
<option value='026-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-02'){ echo 'selected="selected"';} ?> >026-02 Bioquímica</option>
<option value='026-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-03'){ echo 'selected="selected"';} ?> >026-03 Edafología</option>
<option value='026-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-04'){ echo 'selected="selected"';} ?> >026-04 Botánica</option>
<option value='026-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-05'){ echo 'selected="selected"';} ?> >026-05 Economía</option>
<option value='026-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-06'){ echo 'selected="selected"';} ?> >026-06 Geología</option>
<option value='026-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-07'){ echo 'selected="selected"';} ?> >026-07 Matemáticas</option>
<option value='026-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-08'){ echo 'selected="selected"';} ?> >026-08 Química</option>
<option value='026-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-09'){ echo 'selected="selected"';} ?> >026-09 Física</option>
<option value='026-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-10'){ echo 'selected="selected"';} ?> >026-10 Psicología</option>
<option value='026-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='026-99'){ echo 'selected="selected"';} ?> >026-99 Otras Ciencias aplicadas</option>
<option value='027-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='027-00'){ echo 'selected="selected"';} ?> >027-00 Cobros e Impagos en general</option>
<option value='027-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='027-01'){ echo 'selected="selected"';} ?> >027-01 Análisis de riesgos y gestión crediticia</option>
<option value='027-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='027-02'){ echo 'selected="selected"';} ?> >027-02 Gestión de cobros y reclamaciones</option>
<option value='028-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='028-00'){ echo 'selected="selected"';} ?> >028-00 Hostelería- cocina en general</option>
<option value='028-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='028-01'){ echo 'selected="selected"';} ?> >028-01 Jefatura de cocinas</option>
<option value='028-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='028-02'){ echo 'selected="selected"';} ?> >028-02 Repostería-pastelería</option>
<option value='029-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='029-00'){ echo 'selected="selected"';} ?> >029-00 Comercio Exterior en general</option>
<option value='030-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='030-00'){ echo 'selected="selected"';} ?> >030-00 Compras y Aprovisionamientos en general</option>
<option value='030-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='030-01'){ echo 'selected="selected"';} ?> >030-01 Negociación y otras técnicas de relación con proveedores</option>
<option value='031-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='031-00'){ echo 'selected="selected"';} ?> >031-00 Comunicaciones Informáticas en general</option>
<option value='032-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='032-00'){ echo 'selected="selected"';} ?> >032-00 Conducción y pilotaje de vehículos, aeronaves y trenes en general</option>
<option value='032-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='032-01'){ echo 'selected="selected"';} ?> >032-01 Conducción de vehículos ligeros</option>
<option value='032-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='032-02'){ echo 'selected="selected"';} ?> >032-02 Conducción de camiones pesados</option>
<option value='032-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='032-03'){ echo 'selected="selected"';} ?> >032-03 Conducción de autobuses</option>
<option value='032-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='032-04'){ echo 'selected="selected"';} ?> >032-04 Pilotaje de aeronaves</option>
<option value='032-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='032-05'){ echo 'selected="selected"';} ?> >032-05 Conducción de ferrocarriles y trenes</option>
<option value='034-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-01'){ echo 'selected="selected"';} ?> >034-01 Conocimiento del producto: Agricultura, ganadería, caza y actividades de los servicios relacionados con las mismas</option>
<option value='034-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-02'){ echo 'selected="selected"';} ?> >034-02 Conocimiento del producto: Selvicultura, explotación forestal y actividades de los servicios relacionados con las mismas</option>
<option value='034-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-03'){ echo 'selected="selected"';} ?> >034-03 Conocimiento del producto: Pesca, acuicultura y actividades de los servicios relacionados con las mismas</option>
<option value='034-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-04'){ echo 'selected="selected"';} ?> >034-04 Conocimiento del producto: Extracción y aglomeración de antracita, hulla, lignito y turba</option>
<option value='034-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-05'){ echo 'selected="selected"';} ?> >034-05 Conocimiento del producto: Extracción de crudos de petróleo y gas natural; actividades de los servicios relacionados con las explotaciones petrolíferas y de gas, excepto actividades de prospección</option>
<option value='034-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-06'){ echo 'selected="selected"';} ?> >034-06 Conocimiento del producto: Extracción de minerales de uranio y torio</option>
<option value='034-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-07'){ echo 'selected="selected"';} ?> >034-07 Conocimiento del producto: Extracción de minerales metálicos</option>
<option value='034-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-08'){ echo 'selected="selected"';} ?> >034-08 Conocimiento del producto: Extracción de minerales no metálicos ni energéticos</option>
<option value='034-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-09'){ echo 'selected="selected"';} ?> >034-09 Conocimiento del producto: Industria de productos alimenticios y bebidas</option>
<option value='034-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-10'){ echo 'selected="selected"';} ?> >034-10 Conocimiento del producto: Industria del tabaco</option>
<option value='034-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-11'){ echo 'selected="selected"';} ?> >034-11 Conocimiento del producto: Industria textil</option>
<option value='034-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-12'){ echo 'selected="selected"';} ?> >034-12 Conocimiento del producto: Industria de la confección y de la peletería</option>
<option value='034-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-13'){ echo 'selected="selected"';} ?> >034-13 Conocimiento del producto: Preparación, curtido y acabado del cuero; fabricación de artículos de marroquinería y viaje; artículos de guarnicionería, talabartería y zapatería</option>
<option value='034-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-14'){ echo 'selected="selected"';} ?> >034-14 Conocimiento del producto: Industria de la madera y del corcho, excepto muebles; cestería y espartería</option>
<option value='034-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-15'){ echo 'selected="selected"';} ?> >034-15 Conocimiento del producto: Industria del papel</option>
<option value='034-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-16'){ echo 'selected="selected"';} ?> >034-16 Conocimiento del producto: Edición, artes gráficas y reproducción de soportes grabados</option>
<option value='034-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-17'){ echo 'selected="selected"';} ?> >034-17 Conocimiento del producto: Industria química</option>
<option value='034-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-18'){ echo 'selected="selected"';} ?> >034-18 Conocimiento del producto: Fabricación de productos de caucho y materias plásticas</option>
<option value='034-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-19'){ echo 'selected="selected"';} ?> >034-19 Conocimiento del producto: Metalurgia</option>
<option value='034-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-20'){ echo 'selected="selected"';} ?> >034-20 Conocimiento del producto: Fabricación de productos metálicos, excepto maquinaria y equipo</option>
<option value='034-21' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-21'){ echo 'selected="selected"';} ?> >034-21 Conocimiento del producto: Industria de la construcción de maquinaria y equipo mecánico</option>
<option value='034-22' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-22'){ echo 'selected="selected"';} ?> >034-22 Conocimiento del producto: Fabricación de máquinas de oficina y equipos informáticos</option>
<option value='034-23' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-23'){ echo 'selected="selected"';} ?> >034-23 Conocimiento del producto: Fabricación de maquinaria y material eléctrico</option>
<option value='034-24' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-24'){ echo 'selected="selected"';} ?> >034-24 Conocimiento del producto: Fabricación de material electrónico; fabricación de equipo y aparatos de radio, televisión y comunicaciones</option>
<option value='034-25' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-25'){ echo 'selected="selected"';} ?> >034-25 Conocimiento del producto: Fabricación de equipo e instrumentos médico-quirúrgicos, de precisión, óptica y relojería</option>
<option value='034-26' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-26'){ echo 'selected="selected"';} ?> >034-26 Conocimiento del producto: Fabricación de vehículos de motor, remolques y semirremolques</option>
<option value='034-27' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-27'){ echo 'selected="selected"';} ?> >034-27 Conocimiento del producto: Fabricación de otro material de transporte</option>
<option value='034-28' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-28'){ echo 'selected="selected"';} ?> >034-28 Conocimiento del producto: Coquerías, refino de petróleo y tratamiento de combustibles nucleares</option>
<option value='034-29' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-29'){ echo 'selected="selected"';} ?> >034-29 Conocimiento del producto: Fabricación de otros productos minerales no metálicos</option>
<option value='034-30' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-30'){ echo 'selected="selected"';} ?> >034-30 Conocimiento del producto: Fabricación de muebles</option>
<option value='034-31' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-31'){ echo 'selected="selected"';} ?> >034-31 Conocimiento del producto: Reciclaje</option>
<option value='034-32' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-32'){ echo 'selected="selected"';} ?> >034-32 Conocimiento del producto: Producción y distribución de energía eléctrica, gas, vapor y agua caliente</option>
<option value='034-33' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-33'){ echo 'selected="selected"';} ?> >034-33 Conocimiento del producto: Captación, depuración y distribución de agua</option>
<option value='034-34' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-34'){ echo 'selected="selected"';} ?> >034-34 Conocimiento del producto: Construcción</option>
<option value='034-35' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-35'){ echo 'selected="selected"';} ?> >034-35 Conocimiento del producto: Venta, mantenimiento y reparación de vehículos de motor, motocicletas y ciclomotores; venta al por menor de combustible para vehículos de motor</option>
<option value='034-36' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-36'){ echo 'selected="selected"';} ?> >034-36 Conocimiento del producto: Comercio al por mayor e intermediarios del comercio, excepto vehículos de motor y motocicletas</option>
<option value='034-37' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-37'){ echo 'selected="selected"';} ?> >034-37 Conocimiento del producto: Comercio al por menor, excepto el comercio de vehículos de motor, motocicletas y ciclomotores; reparación de efectos personales y enseres domésticos</option>
<option value='034-38' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-38'){ echo 'selected="selected"';} ?> >034-38 Conocimiento del producto: Hostelería</option>
<option value='034-39' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-39'){ echo 'selected="selected"';} ?> >034-39 Conocimiento del producto: Transporte terrestre; transporte por tuberías</option>
<option value='034-40' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-40'){ echo 'selected="selected"';} ?> >034-40 Conocimiento del producto: Transporte marítimo, de cabotaje y por vías de navegación interiores</option>
<option value='034-41' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-41'){ echo 'selected="selected"';} ?> >034-41 Conocimiento del producto: Transporte aéreo y espacial</option>
<option value='034-42' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-42'){ echo 'selected="selected"';} ?> >034-42 Conocimiento del producto: Actividades anexas a los transportes; actividades de agencias de viaje</option>
<option value='034-43' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-43'){ echo 'selected="selected"';} ?> >034-43 Conocimiento del producto: Correos y telecomunicaciones</option>
<option value='034-44' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-44'){ echo 'selected="selected"';} ?> >034-44 Conocimiento del producto: Intermediación financiera, excepto seguros y planes de pensiones</option>
<option value='034-45' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-45'){ echo 'selected="selected"';} ?> >034-45 Conocimiento del producto: Actividades auxiliares ala intermediación financiera</option>
<option value='034-46' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-46'){ echo 'selected="selected"';} ?> >034-46 Conocimiento del producto: Seguros y planes de pensiones, excepto seguridad social</option>
<option value='034-47' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-47'){ echo 'selected="selected"';} ?> >034-47 Conocimiento del producto: Actividades inmobiliarias</option>
<option value='034-48' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-48'){ echo 'selected="selected"';} ?> >034-48 Conocimiento del producto: Alquiler de maquinaria y equipo sin operario, de efectos personales y enseres domésticos</option>
<option value='034-49' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-49'){ echo 'selected="selected"';} ?> >034-49 Conocimiento del producto: Actividades informáticas</option>
<option value='034-50' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-50'){ echo 'selected="selected"';} ?> >034-50 Conocimiento del producto: Investigación y desarrollo</option>
<option value='034-51' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-51'){ echo 'selected="selected"';} ?> >034-51 Conocimiento del producto: Otras actividades empresariales</option>
<option value='034-52' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-52'){ echo 'selected="selected"';} ?> >034-52 Conocimiento del producto: Educación</option>
<option value='034-53' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-53'){ echo 'selected="selected"';} ?> >034-53 Conocimiento del producto: Actividades sanitarias y veterinarias, servicios sociales</option>
<option value='034-54' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-54'){ echo 'selected="selected"';} ?> >034-54 Conocimiento del producto: Actividades de saneamiento público</option>
<option value='034-55' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-55'){ echo 'selected="selected"';} ?> >034-55 Conocimiento del producto: Actividades asociativas</option>
<option value='034-56' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-56'){ echo 'selected="selected"';} ?> >034-56 Conocimiento del producto: Actividades recreativas, culturales y deportivas</option>
<option value='034-57' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='034-57'){ echo 'selected="selected"';} ?> >034-57 Conocimiento del producto: Actividades diversas de servicios personales</option>
<option value='035-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-01'){ echo 'selected="selected"';} ?> >035-01 Conocimiento del sector: Agricultura, ganadería, caza</option>
<option value='035-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-02'){ echo 'selected="selected"';} ?> >035-02 Conocimiento del sector: Selvicultura, explotación forestal y actividades de los servicios relacionados con las mismas</option>
<option value='035-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-03'){ echo 'selected="selected"';} ?> >035-03 Conocimiento del sector: Pesca, acuicultura y actividades de los servicios relacionados con las mismas</option>
<option value='035-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-04'){ echo 'selected="selected"';} ?> >035-04 Conocimiento del sector: Extracción y aglomeración de antracita, hulla, lignito y turba</option>
<option value='035-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-05'){ echo 'selected="selected"';} ?> >035-05 Conocimiento del sector: Extracción de crudos de petróleo y gas natural; actividades de los servicios relacionados con las explotaciones petrolíferas y de gas, excepto actividades de prospección</option>
<option value='035-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-06'){ echo 'selected="selected"';} ?> >035-06 Conocimiento del sector: Extracción de minerales de uranio y torio</option>
<option value='035-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-07'){ echo 'selected="selected"';} ?> >035-07 Conocimiento del sector: Extracción de minerales metálicos</option>
<option value='035-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-08'){ echo 'selected="selected"';} ?> >035-08 Conocimiento del sector: Extracción de minerales no metálicos ni energéticos</option>
<option value='035-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-09'){ echo 'selected="selected"';} ?> >035-09 Conocimiento del sector: Industria de productos alimenticios y bebidas</option>
<option value='035-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-10'){ echo 'selected="selected"';} ?> >035-10 Conocimiento del sector: Industria del tabaco</option>
<option value='035-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-11'){ echo 'selected="selected"';} ?> >035-11 Conocimiento del sector: Industria textil</option>
<option value='035-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-12'){ echo 'selected="selected"';} ?> >035-12 Conocimiento del sector: Industria de la confección y de la peletería</option>
<option value='035-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-13'){ echo 'selected="selected"';} ?> >035-13 Conocimiento del sector: Preparación, curtido y acabado del cuero; fabricación de artículos de marroquinería y viaje; artículos de guarnicionería, talabartería y zapatería</option>
<option value='035-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-14'){ echo 'selected="selected"';} ?> >035-14 Conocimiento del sector: Industria de la madera y del corcho, excepto muebles; cestería y espartería</option>
<option value='035-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-15'){ echo 'selected="selected"';} ?> >035-15 Conocimiento del sector: Industria del papel</option>
<option value='035-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-16'){ echo 'selected="selected"';} ?> >035-16 Conocimiento del sector: Edición, artes gráficas y reproducción de soportes grabados</option>
<option value='035-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-17'){ echo 'selected="selected"';} ?> >035-17 Conocimiento del sector: Industria química</option>
<option value='035-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-18'){ echo 'selected="selected"';} ?> >035-18 Conocimiento del sector: Fabricación de productos de caucho y materias plásticas</option>
<option value='035-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-19'){ echo 'selected="selected"';} ?> >035-19 Conocimiento del sector: Metalurgia</option>
<option value='035-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-20'){ echo 'selected="selected"';} ?> >035-20 Conocimiento del sector: Fabricación de productos metálicos, excepto maquinaria y equipo</option>
<option value='035-21' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-21'){ echo 'selected="selected"';} ?> >035-21 Conocimiento del sector: Industria de la construcción de maquinaria y equipo mecánico</option>
<option value='035-22' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-22'){ echo 'selected="selected"';} ?> >035-22 Conocimiento del sector: Fabricación de máquinas de oficina y equipos informáticos</option>
<option value='035-23' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-23'){ echo 'selected="selected"';} ?> >035-23 Conocimiento del sector: Fabricación de maquinaria y material eléctrico</option>
<option value='035-24' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-24'){ echo 'selected="selected"';} ?> >035-24 Conocimiento del sector: Fabricación de material electrónico; fabricación de equipo y aparatos de radio, televisión y comunicaciones</option>
<option value='035-25' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-25'){ echo 'selected="selected"';} ?> >035-25 Conocimiento del sector: Fabricación de equipo e instrumentos médico-quirúrgicos, de precisión, óptica y relojería</option>
<option value='035-26' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-26'){ echo 'selected="selected"';} ?> >035-26 Conocimiento del sector: Fabricación de vehículos de motor, remolques y semirremolques</option>
<option value='035-27' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-27'){ echo 'selected="selected"';} ?> >035-27 Conocimiento del sector: Fabricación de otro material de transporte</option>
<option value='035-28' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-28'){ echo 'selected="selected"';} ?> >035-28 Conocimiento del sector: Coquerías, refino de petróleo y tratamiento de combustibles nucleares</option>
<option value='035-29' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-29'){ echo 'selected="selected"';} ?> >035-29 Conocimiento del sector: Fabricación de otros productos minerales no metálicos</option>
<option value='035-30' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-30'){ echo 'selected="selected"';} ?> >035-30 Conocimiento del sector: Fabricación de muebles</option>
<option value='035-31' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-31'){ echo 'selected="selected"';} ?> >035-31 Conocimiento del sector: Reciclaje</option>
<option value='035-32' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-32'){ echo 'selected="selected"';} ?> >035-32 Conocimiento del sector: Producción y distribución de energía eléctrica, gas, vapor y agua caliente</option>
<option value='035-33' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-33'){ echo 'selected="selected"';} ?> >035-33 Conocimiento del sector: Captación, depuración y distribución de agua</option>
<option value='035-34' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-34'){ echo 'selected="selected"';} ?> >035-34 Conocimiento del sector: Construcción</option>
<option value='035-35' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-35'){ echo 'selected="selected"';} ?> >035-35 Conocimiento del sector: Venta, mantenimiento y reparación de vehículos de motor, motocicletas y ciclomotores; venta al por menor de combustible para vehículos de motor</option>
<option value='035-36' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-36'){ echo 'selected="selected"';} ?> >035-36 Conocimiento del sector: Comercio al por mayor e intermediarios del comercio, excepto vehículos de motor y motocicletas</option>
<option value='035-37' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-37'){ echo 'selected="selected"';} ?> >035-37 Conocimiento del sector: Comercio al por menor, excepto el comercio de vehículos de motor, motocicletas y ciclomotores; reparación de efectos personales y enseres domésticos</option>
<option value='035-38' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-38'){ echo 'selected="selected"';} ?> >035-38 Conocimiento del sector: Hostelería</option>
<option value='035-39' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-39'){ echo 'selected="selected"';} ?> >035-39 Conocimiento del sector: Transporte terrestre; transporte por tuberías</option>
<option value='035-40' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-40'){ echo 'selected="selected"';} ?> >035-40 Conocimiento del sector: Transporte marítimo, de cabotaje y por vías de navegación interiores</option>
<option value='035-41' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-41'){ echo 'selected="selected"';} ?> >035-41 Conocimiento del sector: Transporte aéreo y espacial</option>
<option value='035-42' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-42'){ echo 'selected="selected"';} ?> >035-42 Conocimiento del sector: Actividades anexas a los transportes; actividades de agencias de viaje</option>
<option value='035-43' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-43'){ echo 'selected="selected"';} ?> >035-43 Conocimiento del sector: Correos y telecomunicaciones</option>
<option value='035-44' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-44'){ echo 'selected="selected"';} ?> >035-44 Conocimiento del sector: Intermediación financiera, excepto seguros y planes de pensiones</option>
<option value='035-45' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-45'){ echo 'selected="selected"';} ?> >035-45 Conocimiento del sector: Actividades auxiliares ala intermediación financiera</option>
<option value='035-46' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-46'){ echo 'selected="selected"';} ?> >035-46 Conocimiento del sector: Seguros y planes de pensiones, excepto seguridad social</option>
<option value='035-47' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-47'){ echo 'selected="selected"';} ?> >035-47 Conocimiento del sector: Actividades inmobiliarias</option>
<option value='035-48' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-48'){ echo 'selected="selected"';} ?> >035-48 Conocimiento del sector: Alquiler de maquinaria y equipo sin operario, de efectos personales y enseres domésticos</option>
<option value='035-49' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-49'){ echo 'selected="selected"';} ?> >035-49 Conocimiento del sector: Actividades informáticas</option>
<option value='035-50' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-50'){ echo 'selected="selected"';} ?> >035-50 Conocimiento del sector: Investigación y desarrollo</option>
<option value='035-51' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-51'){ echo 'selected="selected"';} ?> >035-51 Conocimiento del sector: Otras actividades empresariales</option>
<option value='035-52' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-52'){ echo 'selected="selected"';} ?> >035-52 Conocimiento del sector: Educación</option>
<option value='035-53' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-53'){ echo 'selected="selected"';} ?> >035-53 Conocimiento del sector: Actividades sanitarias y veterinarias, servicios sociales</option>
<option value='035-54' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-54'){ echo 'selected="selected"';} ?> >035-54 Conocimiento del sector: Actividades de saneamiento público</option>
<option value='035-55' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-55'){ echo 'selected="selected"';} ?> >035-55 Conocimiento del sector: Actividades asociativas</option>
<option value='035-56' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-56'){ echo 'selected="selected"';} ?> >035-56 Conocimiento del sector: Actividades recreativas, culturales y deportivas</option>
<option value='035-57' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='035-57'){ echo 'selected="selected"';} ?> >035-57 Conocimiento del sector: Actividades diversas de servicios personales</option>
<option value='036-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='036-00'){ echo 'selected="selected"';} ?> >036-00 Contabilidad en general</option>
<option value='037-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='037-00'){ echo 'selected="selected"';} ?> >037-00 Control de Gestión en general</option>
<option value='038-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='038-00'){ echo 'selected="selected"';} ?> >038-00 Control numérico en general</option>
<option value='038-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='038-01'){ echo 'selected="selected"';} ?> >038-01 Preparación de máquinas herramienta y Control Numérico (CNC)</option>
<option value='038-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='038-02'){ echo 'selected="selected"';} ?> >038-02 Programación de máquinas herramienta con Control Numérico (CNC)</option>
<option value='039-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='039-00'){ echo 'selected="selected"';} ?> >039-00 Diseño asistido por ordenador en general</option>
<option value='040-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='040-00'){ echo 'selected="selected"';} ?> >040-00 Diseño Gráfico Informatizado en general</option>
<option value='041-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='041-00'){ echo 'selected="selected"';} ?> >041-00 Diseño industrial en general</option>
<option value='042-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='042-00'){ echo 'selected="selected"';} ?> >042-00 Atención y venta en establecimientos comerciales en general</option>
<option value='042-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='042-01'){ echo 'selected="selected"';} ?> >042-01 Cobro y atención en Caja</option>
<option value='043-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='043-00'){ echo 'selected="selected"';} ?> >043-00 Enfermería en general</option>
<option value='043-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='043-01'){ echo 'selected="selected"';} ?> >043-01 Enfermería de atención primaria</option>
<option value='043-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='043-02'){ echo 'selected="selected"';} ?> >043-02 Enfermería de hospitales</option>
<option value='043-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='043-03'){ echo 'selected="selected"';} ?> >043-03 Enfermería de salud mental y toxicomanías</option>
<option value='043-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='043-99'){ echo 'selected="selected"';} ?> >043-99 Enfermería- otros ámbitos</option>
<option value='044-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='044-00'){ echo 'selected="selected"';} ?> >044-00 Equipos técnicos- Información y manifestaciones artísticas en general</option>
<option value='044-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='044-01'){ echo 'selected="selected"';} ?> >044-01 Operación de cámaras</option>
<option value='044-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='044-02'){ echo 'selected="selected"';} ?> >044-02 Edición y montaje de imágenes</option>
<option value='044-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='044-03'){ echo 'selected="selected"';} ?> >044-03 Técnicas de sonido</option>
<option value='044-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='044-04'){ echo 'selected="selected"';} ?> >044-04 Operación de equipos en estación de radio y televisión</option>
<option value='044-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='044-05'){ echo 'selected="selected"';} ?> >044-05 Proyecciones cinematográficas</option>
<option value='044-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='044-06'){ echo 'selected="selected"';} ?> >044-06 Fotografía</option>
<option value='044-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='044-07'){ echo 'selected="selected"';} ?> >044-07 Laboratorio de imagen</option>
<option value='044-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='044-08'){ echo 'selected="selected"';} ?> >044-08 Iluminación</option>
<option value='045-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='045-00'){ echo 'selected="selected"';} ?> >045-00 Escaparatismo en general</option>
<option value='046-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='046-00'){ echo 'selected="selected"';} ?> >046-00 Escenografía y ambientación artística en general</option>
<option value='046-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='046-01'){ echo 'selected="selected"';} ?> >046-01 Decoración de escenarios</option>
<option value='046-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='046-02'){ echo 'selected="selected"';} ?> >046-02 Vestuario</option>
<option value='046-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='046-03'){ echo 'selected="selected"';} ?> >046-03 Caracterización</option>
<option value='046-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='046-04'){ echo 'selected="selected"';} ?> >046-04 Regiduría de escena</option>
<option value='047-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='047-01'){ echo 'selected="selected"';} ?> >047-01 Traumatología</option>
<option value='047-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='047-02'){ echo 'selected="selected"';} ?> >047-02 Ginecología</option>
<option value='047-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='047-03'){ echo 'selected="selected"';} ?> >047-03 Cirugía</option>
<option value='047-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='047-04'){ echo 'selected="selected"';} ?> >047-04 Neurología</option>
<option value='047-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='047-99'){ echo 'selected="selected"';} ?> >047-99 Otras especialidades médicas</option>
<option value='048-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='048-00'){ echo 'selected="selected"';} ?> >048-00 Procesos productivos industria pesada: Fabricación de estructuras metálicas en general</option>
<option value='048-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='048-01'){ echo 'selected="selected"';} ?> >048-01 Carpintería metálica y de PVC</option>
<option value='048-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='048-02'){ echo 'selected="selected"';} ?> >048-02 Montaje de estructuras metálicas</option>
<option value='049-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='049-00'){ echo 'selected="selected"';} ?> >049-00 Estudios de mercado en general</option>
<option value='050-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='050-00'){ echo 'selected="selected"';} ?> >050-00 Finanzas para no Financieros en general</option>
<option value='051-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='051-00'){ echo 'selected="selected"';} ?> >051-00 Explotación forestal en general</option>
<option value='051-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='051-01'){ echo 'selected="selected"';} ?> >051-01 Trabajos forestales</option>
<option value='051-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='051-02'){ echo 'selected="selected"';} ?> >051-02 Manipulación de motosierras</option>
<option value='051-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='051-03'){ echo 'selected="selected"';} ?> >051-03 Explotación del alcornoque</option>
<option value='051-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='051-04'){ echo 'selected="selected"';} ?> >051-04 Explotación cinegética</option>
<option value='052-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='052-00'){ echo 'selected="selected"';} ?> >052-00 Formación de formadores en general</option>
<option value='052-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='052-01'){ echo 'selected="selected"';} ?> >052-01 Aplicación de TICs a la formación</option>
<option value='054-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='054-00'){ echo 'selected="selected"';} ?> >054-00 Montaje e instalación de frío industrial en general</option>
<option value='054-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='054-01'){ echo 'selected="selected"';} ?> >054-01 Montaje e instalación de cámaras frigoríficas</option>
<option value='054-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='054-02'){ echo 'selected="selected"';} ?> >054-02 Instalación y mantenimiento de aire acondicionado</option>
<option value='055-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='055-00'){ echo 'selected="selected"';} ?> >055-00 Gestión administrativa en general</option>
<option value='056-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='056-00'){ echo 'selected="selected"';} ?> >056-00 Gestión ambiental en general</option>
<option value='056-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='056-01'){ echo 'selected="selected"';} ?> >056-01 Sensibilización hacia el medio ambiente</option>
<option value='057-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='057-00'){ echo 'selected="selected"';} ?> >057-00 Dirección y gestión bancaria en general</option>
<option value='058-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='058-00'){ echo 'selected="selected"';} ?> >058-00 Gestión comercial en general</option>
<option value='058-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='058-01'){ echo 'selected="selected"';} ?> >058-01 Gestión en Agencia Comercial</option>
<option value='059-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='059-00'){ echo 'selected="selected"';} ?> >059-00 Gestión de almacén y/o distribución en general</option>
<option value='060-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='060-00'){ echo 'selected="selected"';} ?> >060-00 Gestión de empresas de Ec. Social en general</option>
<option value='061-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='061-00'){ echo 'selected="selected"';} ?> >061-00 Gestión de flotas en general</option>
<option value='061-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='061-01'){ echo 'selected="selected"';} ?> >061-01 Gestión de flotas- transporte por carretera</option>
<option value='061-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='061-02'){ echo 'selected="selected"';} ?> >061-02 Gestión de flotas- transporte marítimo</option>
<option value='061-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='061-03'){ echo 'selected="selected"';} ?> >061-03 Gestión de flotas-Transporte aéreo</option>
<option value='062-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='062-00'){ echo 'selected="selected"';} ?> >062-00 Gestión de la formación en general</option>
<option value='063-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='063-00'){ echo 'selected="selected"';} ?> >063-00 Gestión de obra en general</option>
<option value='063-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='063-01'){ echo 'selected="selected"';} ?> >063-01 Supervisión de ejecución de obra de edificación</option>
<option value='063-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='063-02'){ echo 'selected="selected"';} ?> >063-02 Supervisión de ejecución de obra civil</option>
<option value='064-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='064-00'){ echo 'selected="selected"';} ?> >064-00 Gestión de la producción en general</option>
<option value='065-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='065-00'){ echo 'selected="selected"';} ?> >065-00 Gestión de proyectos (no de proyectos informáticos) en general</option>
<option value='066-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='066-00'){ echo 'selected="selected"';} ?> >066-00 Gestión de proyectos informáticos en general</option>
<option value='067-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='067-00'){ echo 'selected="selected"';} ?> >067-00 Gestión de Pymes en general</option>
<option value='067-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='067-01'){ echo 'selected="selected"';} ?> >067-01 Gerencia de Pequeño Comercio</option>
<option value='068-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='068-00'){ echo 'selected="selected"';} ?> >068-00 Gestión de recursos humanos en general</option>
<option value='068-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='068-01'){ echo 'selected="selected"';} ?> >068-01 Selección y desarrollo de Recursos Humanos</option>
<option value='068-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='068-02'){ echo 'selected="selected"';} ?> >068-02 Habilidades directivas</option>
<option value='068-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='068-03'){ echo 'selected="selected"';} ?> >068-03 Habilidades personales e interpersonales en el entorno laboral</option>
<option value='068-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='068-04'){ echo 'selected="selected"';} ?> >068-04 Comunicación Interna</option>
<option value='068-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='068-05'){ echo 'selected="selected"';} ?> >068-05 Recursos humanos: Negociación colectiva</option>
<option value='068-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='068-06'){ echo 'selected="selected"';} ?> >068-06 Conocimiento de la empresa (acogida), cultura de empresa, cambio de cultura</option>
<option value='069-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='069-00'){ echo 'selected="selected"';} ?> >069-00 Gestión del mantenimiento en general</option>
<option value='069-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='069-01'){ echo 'selected="selected"';} ?> >069-01 Técnicas de organización de instalaciones y mantenimiento de edificios y equipamientos urbanos</option>
<option value='069-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='069-02'){ echo 'selected="selected"';} ?> >069-02 Técnicas de organización del mantenimiento de equipos electromecánicos de uso no industrial</option>
<option value='069-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='069-03'){ echo 'selected="selected"';} ?> >069-03 Técnicas de organización del mantenimiento industrial</option>
<option value='070-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='070-00'){ echo 'selected="selected"';} ?> >070-00 Gestión económico financiera en general</option>
<option value='071-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='071-00'){ echo 'selected="selected"';} ?> >071-00 Gestión de grandes empresas y redes empresariales en general</option>
<option value='072-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='072-00'){ echo 'selected="selected"';} ?> >072-00 Gestión fiscal en general</option>
<option value='073-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='073-00'){ echo 'selected="selected"';} ?> >073-00 Gestión hospitalaria en general</option>
<option value='074-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='074-00'){ echo 'selected="selected"';} ?> >074-00 Gestión hotelera en general</option>
<option value='074-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='074-01'){ echo 'selected="selected"';} ?> >074-01 Gestión de hoteles y otros alojamientos</option>
<option value='074-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='074-02'){ echo 'selected="selected"';} ?> >074-02 Gestión de establecimientos de restauración</option>
<option value='074-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='074-03'){ echo 'selected="selected"';} ?> >074-03 Jefatura de economatos y bodegas</option>
<option value='075-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='075-00'){ echo 'selected="selected"';} ?> >075-00 Gestión inmobiliaria en general</option>
<option value='078-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='078-00'){ echo 'selected="selected"';} ?> >078-00 Habilitación y especialización en docencia en general</option>
<option value='079-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='079-00'){ echo 'selected="selected"';} ?> >079-00 Seguridad alimentaria: manipulación y control de alimentos en general</option>
<option value='079-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='079-01'){ echo 'selected="selected"';} ?> >079-01 Higiene alimentaria / Manipulación de alimentos</option>
<option value='079-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='079-02'){ echo 'selected="selected"';} ?> >079-02 Control de puntos críticos (Ind. Alimentaria)</option>
<option value='080-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='080-00'){ echo 'selected="selected"';} ?> >080-00 Hostelería-Atención en pisos en general</option>
<option value='080-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='080-01'){ echo 'selected="selected"';} ?> >080-01 Gobernanza de pisos</option>
<option value='080-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='080-02'){ echo 'selected="selected"';} ?> >080-02 Servicio de pisos</option>
<option value='080-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='080-03'){ echo 'selected="selected"';} ?> >080-03 Lencería, lavandería,  planchado</option>
<option value='087-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-00'){ echo 'selected="selected"';} ?> >087-00 Informática de Usuario / Ofimática en general</option>
<option value='087-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-01'){ echo 'selected="selected"';} ?> >087-01 Ofimática: Procesadores de texto</option>
<option value='087-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-02'){ echo 'selected="selected"';} ?> >087-02 Ofimática: Bases de datos</option>
<option value='087-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-03'){ echo 'selected="selected"';} ?> >087-03 Ofimática: Hojas de Cálculo</option>
<option value='087-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-04'){ echo 'selected="selected"';} ?> >087-04 Ofimática: Aplicaciones para Presentaciones en público</option>
<option value='087-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-05'){ echo 'selected="selected"';} ?> >087-05 Ofimática: Internet-intranet y navegadores</option>
<option value='087-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-06'){ echo 'selected="selected"';} ?> >087-06 Ofimática: Diseño de páginas web</option>
<option value='087-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-07'){ echo 'selected="selected"';} ?> >087-07 Informática de usuario: Aplicaciones para el tratamiento de imágenes, sonido y vídeo</option>
<option value='087-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-08'){ echo 'selected="selected"';} ?> >087-08 Informática de usuario: Tratamiento e informes de grandes almacenes de datos</option>
<option value='087-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-09'){ echo 'selected="selected"';} ?> >087-09 Informática de usuario: Lenguajes de programación</option>
<option value='087-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-10'){ echo 'selected="selected"';} ?> >087-10 Informática de usuario: E-bussines & e-commerce</option>
<option value='087-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-11'){ echo 'selected="selected"';} ?> >087-11 Informática de usuario: Aplicaciones estándar de gestión de recursos, compras y ventas</option>
<option value='087-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-12'){ echo 'selected="selected"';} ?> >087-12 Informática de usuario: Aplicaciones para tratamiento estadístico</option>
<option value='087-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-13'){ echo 'selected="selected"';} ?> >087-13 Informática de usuario: Sistemas operativos</option>
<option value='087-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-14'){ echo 'selected="selected"';} ?> >087-14 Introducción a la informática y/o Nuevas Tecnologías de la información y comunicación</option>
<option value='087-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-15'){ echo 'selected="selected"';} ?> >087-15 Informática de usuario: Utilidades y herramientas de apoyo para el tratamiento de ficheros</option>
<option value='087-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='087-16'){ echo 'selected="selected"';} ?> >087-16 Informática de usuario: Aplicaciones estándar para estudios y oficinas técnicas</option>
<option value='088-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='088-00'){ echo 'selected="selected"';} ?> >088-00 Informática  de desarrollo en general</option>
<option value='089-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='089-00'){ echo 'selected="selected"';} ?> >089-00 Ingeniería y nuevas tecnologías i+d+i en general</option>
<option value='090-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='090-00'){ echo 'selected="selected"';} ?> >090-00 Instalaciones de viviendas y edificios en general</option>
<option value='090-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='090-01'){ echo 'selected="selected"';} ?> >090-01 Fontanería</option>
<option value='090-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='090-02'){ echo 'selected="selected"';} ?> >090-02 Calefacción y climatización</option>
<option value='090-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='090-03'){ echo 'selected="selected"';} ?> >090-03 Instalación de gas</option>
<option value='090-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='090-04'){ echo 'selected="selected"';} ?> >090-04 Instalación de aislamientos</option>
<option value='090-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='090-05'){ echo 'selected="selected"';} ?> >090-05 Impermeabilización</option>
<option value='090-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='090-06'){ echo 'selected="selected"';} ?> >090-06 Instalaciones eléctricas en edificios</option>
<option value='091-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='091-00'){ echo 'selected="selected"';} ?> >091-00 Montaje e instalación de equipos industriales en general</option>
<option value='091-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='091-01'){ echo 'selected="selected"';} ?> >091-01 Instalación de equipos y sistemas electrónicos</option>
<option value='091-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='091-02'){ echo 'selected="selected"';} ?> >091-02 Instalación de máquinas y equipos industriales</option>
<option value='091-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='091-03'){ echo 'selected="selected"';} ?> >091-03 Instalación de equipos y sistemas de comunicación electrónicos</option>
<option value='091-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='091-04'){ echo 'selected="selected"';} ?> >091-04 Instalación y mantenimiento  de conducciones de fluidos</option>
<option value='091-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='091-05'){ echo 'selected="selected"';} ?> >091-05 Montaje electromecánico en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='091-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='091-06'){ echo 'selected="selected"';} ?> >091-06 Trabajos auxiliares de montajes electrónicos en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='091-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='091-07'){ echo 'selected="selected"';} ?> >091-07 Montaje de dispositivos y cuadros electrónicos en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='091-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='091-08'){ echo 'selected="selected"';} ?> >091-08 Montaje y ajuste de equipos electrónicos en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='092-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='092-00'){ echo 'selected="selected"';} ?> >092-00 Instrumentación y control en general</option>
<option value='093-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='093-00'){ echo 'selected="selected"';} ?> >093-00 Interiorismo y decoración en general</option>
<option value='094-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='094-00'){ echo 'selected="selected"';} ?> >094-00 Interpretación de planos y delineación en general</option>
<option value='095-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='095-00'){ echo 'selected="selected"';} ?> >095-00 Inversiones-banca en general</option>
<option value='096-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='096-00'){ echo 'selected="selected"';} ?> >096-00 Jardinería, floricultura y arte floral en general</option>
<option value='096-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='096-01'){ echo 'selected="selected"';} ?> >096-01 Cultivo de flores</option>
<option value='096-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='096-02'){ echo 'selected="selected"';} ?> >096-02 Manipulación de flores</option>
<option value='096-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='096-03'){ echo 'selected="selected"';} ?> >096-03 Trabajos de centros de jardinería</option>
<option value='096-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='096-04'){ echo 'selected="selected"';} ?> >096-04 Trabajos en viveros</option>
<option value='096-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='096-05'){ echo 'selected="selected"';} ?> >096-05 Jardinería</option>
<option value='097-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='097-00'){ echo 'selected="selected"';} ?> >097-00 Laboratorio clínico en general</option>
<option value='097-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='097-01'){ echo 'selected="selected"';} ?> >097-01 Técnicas de microbiología</option>
<option value='097-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='097-02'){ echo 'selected="selected"';} ?> >097-02 Análisis de hematología</option>
<option value='097-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='097-03'){ echo 'selected="selected"';} ?> >097-03 Técnicas de inmunología-bioquímica</option>
<option value='097-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='097-04'){ echo 'selected="selected"';} ?> >097-04 Técnicas de anatomía patológica</option>
<option value='097-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='097-05'){ echo 'selected="selected"';} ?> >097-05 Técnicas auxiliares de laboratorio clínico</option>
<option value='101-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='101-00'){ echo 'selected="selected"';} ?> >101-00 Logística Integral en general</option>
<option value='102-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='102-00'){ echo 'selected="selected"';} ?> >102-00 Manipulación de mercancías en general</option>
<option value='102-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='102-01'){ echo 'selected="selected"';} ?> >102-01 Manipulación de mercancías-Transporte por carretera</option>
<option value='102-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='102-02'){ echo 'selected="selected"';} ?> >102-02 Manipulación de mercancías en puerto</option>
<option value='102-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='102-03'){ echo 'selected="selected"';} ?> >102-03 Manipulación de mercancías- Estiba y desestiba de buques</option>
<option value='102-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='102-04'){ echo 'selected="selected"';} ?> >102-04 Manipulación de mercancías con grúas portuarias</option>
<option value='102-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='102-05'){ echo 'selected="selected"';} ?> >102-05 Manipulación de mercancías en cubierta de buques</option>
<option value='102-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='102-06'){ echo 'selected="selected"';} ?> >102-06 Manipulación de mercancías-Transporte aéreo</option>
<option value='103-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='103-00'){ echo 'selected="selected"';} ?> >103-00 Mantenimiento de Edificios y Otro Equipamiento Urbano en general</option>
<option value='103-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='103-01'){ echo 'selected="selected"';} ?> >103-01 Análisis y diagnóstico de edificios</option>
<option value='103-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='103-02'){ echo 'selected="selected"';} ?> >103-02 Domótica</option>
<option value='103-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='103-03'){ echo 'selected="selected"';} ?> >103-03 Mantenimiento de instalaciones de climatización de edificios</option>
<option value='104-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='104-00'){ echo 'selected="selected"';} ?> >104-00 Mantenimiento de Equipos Electromecánicos de uso no industrial en general</option>
<option value='104-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='104-01'){ echo 'selected="selected"';} ?> >104-01 Mantenimiento de Equipos de climatización</option>
<option value='104-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='104-02'){ echo 'selected="selected"';} ?> >104-02 Mantenimiento de Equipos Electrodomésticos y de limpieza de edificios</option>
<option value='104-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='104-03'){ echo 'selected="selected"';} ?> >104-03 Mantenimiento de Equipos Informáticos y periféricos</option>
<option value='104-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='104-04'){ echo 'selected="selected"';} ?> >104-04 Mantenimiento de Equipos de Telefonía y Comunicaciones</option>
<option value='104-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='104-05'){ echo 'selected="selected"';} ?> >104-05 Mantenimiento de Equipos de electromedicina</option>
<option value='105-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-00'){ echo 'selected="selected"';} ?> >105-00 Mantenimiento Industrial en general</option>
<option value='105-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-01'){ echo 'selected="selected"';} ?> >105-01 Mantenimiento mecánico</option>
<option value='105-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-02'){ echo 'selected="selected"';} ?> >105-02 Mantenimiento electromecánico</option>
<option value='105-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-03'){ echo 'selected="selected"';} ?> >105-03 Mantenimiento eléctrico</option>
<option value='105-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-04'){ echo 'selected="selected"';} ?> >105-04 Mantenimiento electrónico</option>
<option value='105-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-05'){ echo 'selected="selected"';} ?> >105-05 Trabajos auxiliares en mantenimiento mecánico en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='105-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-06'){ echo 'selected="selected"';} ?> >105-06 Mantenimiento de instalaciones mecánicas en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='105-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-07'){ echo 'selected="selected"';} ?> >105-07 Mantenimiento mecánico en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='105-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-08'){ echo 'selected="selected"';} ?> >105-08 Trabajos auxiliares de mantenimiento electromecánico en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='105-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-09'){ echo 'selected="selected"';} ?> >105-09 Técnicas de mantenimiento electromecánico en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='105-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-10'){ echo 'selected="selected"';} ?> >105-10 Técnicas de mantenimiento de equipos eléctricos en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='105-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-11'){ echo 'selected="selected"';} ?> >105-11 Técnicas de mantenimiento electrónico en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='105-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='105-12'){ echo 'selected="selected"';} ?> >105-12 Mantenimiento de equipos electromecánicos en Industrias de Fabricación de Equipos Electromecánicos</option>
<option value='106-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='106-00'){ echo 'selected="selected"';} ?> >106-00 Mantenimiento y Reparación de Equipos de Transporte en general</option>
<option value='106-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='106-01'){ echo 'selected="selected"';} ?> >106-01 Reparación de maquinaria agrícola autopropulsada</option>
<option value='106-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='106-02'){ echo 'selected="selected"';} ?> >106-02 Reparación de maquinaria industrial autopropulsada</option>
<option value='106-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='106-03'){ echo 'selected="selected"';} ?> >106-03 Reparación de motores de aviación</option>
<option value='106-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='106-04'){ echo 'selected="selected"';} ?> >106-04 Reparación de motores náuticos y componentes mecánicos navales</option>
<option value='106-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='106-05'){ echo 'selected="selected"';} ?> >106-05 Mantenimiento de vehículos ferroviarios de tracción</option>
<option value='106-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='106-06'){ echo 'selected="selected"';} ?> >106-06 Reparación de vehículos pesados</option>
<option value='107-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='107-00'){ echo 'selected="selected"';} ?> >107-00 Mantenimiento y Reparación de Vehículos Ligeros en general</option>
<option value='107-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='107-01'){ echo 'selected="selected"';} ?> >107-01 Reparación de carrocerías de vehículos</option>
<option value='107-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='107-02'){ echo 'selected="selected"';} ?> >107-02 Reparación de motores y equipos de inyección</option>
<option value='107-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='107-03'){ echo 'selected="selected"';} ?> >107-03 Reparación de sistemas eléctricos y electrónicos de vehículos</option>
<option value='107-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='107-04'){ echo 'selected="selected"';} ?> >107-04 Diagnosis de vehículos</option>
<option value='107-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='107-05'){ echo 'selected="selected"';} ?> >107-05 Reparación de vehículos de dos o tres ruedas</option>
<option value='108-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='108-00'){ echo 'selected="selected"';} ?> >108-00 Marketing en general</option>
<option value='109-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-01'){ echo 'selected="selected"';} ?> >109-01 Materiales y materias primas: Agricultura, ganadería, caza y actividades de los servicios relacionados con las mismas</option>
<option value='109-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-02'){ echo 'selected="selected"';} ?> >109-02 Materiales y materias primas: Selvicultura, explotación forestal y actividades de los servicios relacionados con las mismas</option>
<option value='109-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-03'){ echo 'selected="selected"';} ?> >109-03 Materiales y materias primas: Pesca, acuicultura y actividades de los servicios relacionados con las mismas</option>
<option value='109-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-04'){ echo 'selected="selected"';} ?> >109-04 Materiales y materias primas: Extracción y aglomeración de antracita, hulla, lignito y turba</option>
<option value='109-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-05'){ echo 'selected="selected"';} ?> >109-05 Materiales y materias primas: Extracción de crudos de petróleo y gas natural; actividades de los servicios relacionados con las explotaciones petrolíferas y de gas, excepto actividades de prospección</option>
<option value='109-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-06'){ echo 'selected="selected"';} ?> >109-06 Materiales y materias primas: Extracción de minerales de uranio y torio</option>
<option value='109-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-07'){ echo 'selected="selected"';} ?> >109-07 Materiales y materias primas: Extracción de minerales metálicos</option>
<option value='109-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-08'){ echo 'selected="selected"';} ?> >109-08 Materiales y materias primas: Extracción de minerales no metálicos ni energéticos</option>
<option value='109-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-09'){ echo 'selected="selected"';} ?> >109-09 Materiales y materias primas: Industria de productos alimenticios y bebidas</option>
<option value='109-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-10'){ echo 'selected="selected"';} ?> >109-10 Materiales y materias primas: Industria del tabaco</option>
<option value='109-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-11'){ echo 'selected="selected"';} ?> >109-11 Materiales y materias primas: Industria textil</option>
<option value='109-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-12'){ echo 'selected="selected"';} ?> >109-12 Materiales y materias primas: Industria de la confección y de la peletería</option>
<option value='109-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-13'){ echo 'selected="selected"';} ?> >109-13 Materiales y materias primas: Preparación, curtido y acabado del cuero; fabricación de artículos de marroquinería y viaje; artículos de guarnicionería, talabartería y zapatería</option>
<option value='109-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-14'){ echo 'selected="selected"';} ?> >109-14 Materiales y materias primas: Industria de la madera y del corcho, excepto muebles; cestería y espartería</option>
<option value='109-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-15'){ echo 'selected="selected"';} ?> >109-15 Materiales y materias primas: Industria del papel</option>
<option value='109-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-16'){ echo 'selected="selected"';} ?> >109-16 Materiales y materias primas: Edición, artes gráficas y reproducción de soportes grabados</option>
<option value='109-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-17'){ echo 'selected="selected"';} ?> >109-17 Materiales y materias primas: Industria química</option>
<option value='109-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-18'){ echo 'selected="selected"';} ?> >109-18 Materiales y materias primas: Fabricación de productos de caucho y materias plásticas</option>
<option value='109-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-19'){ echo 'selected="selected"';} ?> >109-19 Materiales y materias primas: Metalurgia</option>
<option value='109-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-20'){ echo 'selected="selected"';} ?> >109-20 Materiales y materias primas: Fabricación de productos metálicos, excepto maquinaria y equipo</option>
<option value='109-21' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-21'){ echo 'selected="selected"';} ?> >109-21 Materiales y materias primas: Industria de la construcción de maquinaria y equipo mecánico</option>
<option value='109-22' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-22'){ echo 'selected="selected"';} ?> >109-22 Materiales y materias primas: Fabricación de máquinas de oficina y equipos informáticos</option>
<option value='109-23' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-23'){ echo 'selected="selected"';} ?> >109-23 Materiales y materias primas: Fabricación de maquinaria y material eléctrico</option>
<option value='109-24' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-24'){ echo 'selected="selected"';} ?> >109-24 Materiales y materias primas: Fabricación de material electrónico; fabricación de equipo y aparatos de radio, televisión y comunicaciones</option>
<option value='109-25' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-25'){ echo 'selected="selected"';} ?> >109-25 Materiales y materias primas: Fabricación de equipo e instrumentos médico-quirúrgicos, de precisión, óptica y relojería</option>
<option value='109-26' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-26'){ echo 'selected="selected"';} ?> >109-26 Materiales y materias primas: Fabricación de vehículos de motor, remolques y semirremolques</option>
<option value='109-27' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-27'){ echo 'selected="selected"';} ?> >109-27 Materiales y materias primas: Fabricación de otro material de transporte</option>
<option value='109-28' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-28'){ echo 'selected="selected"';} ?> >109-28 Materiales y materias primas: Coquerías, refino de petróleo y tratamiento de combustibles nucleares</option>
<option value='109-29' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-29'){ echo 'selected="selected"';} ?> >109-29 Materiales y materias primas: Fabricación de otros productos minerales no metálicos</option>
<option value='109-30' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-30'){ echo 'selected="selected"';} ?> >109-30 Materiales y materias primas: Fabricación de muebles; otras industrias manufactureras</option>
<option value='109-31' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-31'){ echo 'selected="selected"';} ?> >109-31 Materiales y materias primas: Reciclaje</option>
<option value='109-32' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-32'){ echo 'selected="selected"';} ?> >109-32 Materiales y materias primas: Producción y distribución de energía eléctrica, gas, vapor y agua caliente</option>
<option value='109-33' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-33'){ echo 'selected="selected"';} ?> >109-33 Materiales y materias primas: Captación, depuración y distribución de agua</option>
<option value='109-34' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-34'){ echo 'selected="selected"';} ?> >109-34 Materiales y materias primas: Construcción</option>
<option value='109-35' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='109-35'){ echo 'selected="selected"';} ?> >109-35 Materiales y materias primas: Hostelería</option>
<option value='110-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='110-00'){ echo 'selected="selected"';} ?> >110-00 Mercados financieros en general</option>
<option value='111-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='111-00'){ echo 'selected="selected"';} ?> >111-00 Merchandising en general</option>
<option value='112-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='112-00'){ echo 'selected="selected"';} ?> >112-00 Metodologías-Didácticas específicas en general</option>
<option value='114-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='114-00'){ echo 'selected="selected"';} ?> >114-00 Calidad en general</option>
<option value='114-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='114-01'){ echo 'selected="selected"';} ?> >114-01 Auditoría de Calidad</option>
<option value='114-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='114-02'){ echo 'selected="selected"';} ?> >114-02 Introducción a la calidad</option>
<option value='114-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='114-03'){ echo 'selected="selected"';} ?> >114-03 Herramientas de calidad</option>
<option value='114-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='114-04'){ echo 'selected="selected"';} ?> >114-04 Gestión de calidad total/Modelos de excelencia empresarial</option>
<option value='114-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='114-05'){ echo 'selected="selected"';} ?> >114-05 Normalización/Homologación/ certificación de calidad</option>
<option value='115-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='115-00'){ echo 'selected="selected"';} ?> >115-00 Nuevas tecnologías e investigación aplicadas a la docencia</option>
<option value='116-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='116-00'){ echo 'selected="selected"';} ?> >116-00 Nutrición y dietética en general</option>
<option value='117-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='117-00'){ echo 'selected="selected"';} ?> >117-00 Oficina técnica-Construcción en general</option>
<option value='117-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='117-01'){ echo 'selected="selected"';} ?> >117-01 Delineación  en edificación y obras civiles</option>
<option value='117-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='117-02'){ echo 'selected="selected"';} ?> >117-02 Técnicas auxiliares de obra</option>
<option value='117-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='117-03'){ echo 'selected="selected"';} ?> >117-03 Técnicas auxiliares de topografía</option>
<option value='117-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='117-04'){ echo 'selected="selected"';} ?> >117-04 Técnicas auxiliares de laboratorio de obra</option>
<option value='117-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='117-05'){ echo 'selected="selected"';} ?> >117-05 Técnicas auxiliares de control y vigilancia de obras</option>
<option value='117-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='117-06'){ echo 'selected="selected"';} ?> >117-06 Planificación y control de obras</option>
<option value='117-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='117-07'){ echo 'selected="selected"';} ?> >117-07 Cálculo de estructuras y cimentaciones</option>
<option value='118-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-01'){ echo 'selected="selected"';} ?> >118-01 Encofrado</option>
<option value='118-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-02'){ echo 'selected="selected"';} ?> >118-02 Ferralla</option>
<option value='118-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-03'){ echo 'selected="selected"';} ?> >118-03 Hormigonado</option>
<option value='118-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-04'){ echo 'selected="selected"';} ?> >118-04 Entibado</option>
<option value='118-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-05'){ echo 'selected="selected"';} ?> >118-05 Montaje de estructuras tubulares</option>
<option value='118-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-06'){ echo 'selected="selected"';} ?> >118-06 Albañilería</option>
<option value='118-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-07'){ echo 'selected="selected"';} ?> >118-07 Revoco</option>
<option value='118-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-08'){ echo 'selected="selected"';} ?> >118-08 Tendido de yesos</option>
<option value='118-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-09'){ echo 'selected="selected"';} ?> >118-09 Estucado</option>
<option value='118-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-10'){ echo 'selected="selected"';} ?> >118-10 Colocación de escayola</option>
<option value='118-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-11'){ echo 'selected="selected"';} ?> >118-11 Colocación de prefabricados ligeros</option>
<option value='118-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-12'){ echo 'selected="selected"';} ?> >118-12 Instalación de redes de saneamiento</option>
<option value='118-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-13'){ echo 'selected="selected"';} ?> >118-13 Cantería</option>
<option value='118-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-14'){ echo 'selected="selected"';} ?> >118-14 Colocación de mármol</option>
<option value='118-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-15'){ echo 'selected="selected"';} ?> >118-15 Mampostería</option>
<option value='118-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-16'){ echo 'selected="selected"';} ?> >118-16 Pavimentación</option>
<option value='118-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-17'){ echo 'selected="selected"';} ?> >118-17 Trabajos en portland</option>
<option value='118-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-18'){ echo 'selected="selected"';} ?> >118-18 Pintura de edificios</option>
<option value='118-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-19'){ echo 'selected="selected"';} ?> >118-19 Entarimado</option>
<option value='118-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-20'){ echo 'selected="selected"';} ?> >118-20 Enmoquetado-entelado</option>
<option value='118-21' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-21'){ echo 'selected="selected"';} ?> >118-21 Colocación de pavimentos ligeros</option>
<option value='118-22' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-22'){ echo 'selected="selected"';} ?> >118-22 Acristalado de edificios</option>
<option value='118-23' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-23'){ echo 'selected="selected"';} ?> >118-23 Colocación de pizarra</option>
<option value='118-24' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-24'){ echo 'selected="selected"';} ?> >118-24 Solado-alicatado</option>
<option value='118-25' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-25'){ echo 'selected="selected"';} ?> >118-25 Techado en chapas y placas</option>
<option value='118-26' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-26'){ echo 'selected="selected"';} ?> >118-26 Operación de plantas de áridos</option>
<option value='118-27' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-27'){ echo 'selected="selected"';} ?> >118-27 Operación de plantas de hormigón</option>
<option value='118-28' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-28'){ echo 'selected="selected"';} ?> >118-28 Operación de plantas de aglomerados asfálticos</option>
<option value='118-29' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-29'){ echo 'selected="selected"';} ?> >118-29 Prefabricado de hormigón</option>
<option value='118-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='118-99'){ echo 'selected="selected"';} ?> >118-99 Otros competencias específicas de oficios de construcción</option>
<option value='119-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-00'){ echo 'selected="selected"';} ?> >119-00 Operación de maquinaria de construcción en general</option>
<option value='119-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-01'){ echo 'selected="selected"';} ?> >119-01 Operaciones con maquinas de perforación</option>
<option value='119-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-02'){ echo 'selected="selected"';} ?> >119-02 Operaciones de voladura con explosivos</option>
<option value='119-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-03'){ echo 'selected="selected"';} ?> >119-03 Operaciones con maquinaria excavadora</option>
<option value='119-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-04'){ echo 'selected="selected"';} ?> >119-04 Operaciones con maquinaria explanadora</option>
<option value='119-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-05'){ echo 'selected="selected"';} ?> >119-05 Operaciones con maquinaria de transporte de tierras</option>
<option value='119-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-06'){ echo 'selected="selected"';} ?> >119-06 Operaciones con maquinaria compactadoras</option>
<option value='119-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-07'){ echo 'selected="selected"';} ?> >119-07 Operaciones con maquinaria de firmes y pavimentos</option>
<option value='119-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-08'){ echo 'selected="selected"';} ?> >119-08 Operaciones con maquinaria de dragado</option>
<option value='119-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-09'){ echo 'selected="selected"';} ?> >119-09 Operaciones con maquinaria de vías</option>
<option value='119-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-10'){ echo 'selected="selected"';} ?> >119-10 Operación de grúas</option>
<option value='119-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='119-99'){ echo 'selected="selected"';} ?> >119-99 Operación de otra maquinaria de construcción</option>
<option value='120-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='120-00'){ echo 'selected="selected"';} ?> >120-00 Operativa bancaria en general</option>
<option value='120-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='120-01'){ echo 'selected="selected"';} ?> >120-01 Trabajos administrativos en entidades financieras</option>
<option value='120-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='120-02'){ echo 'selected="selected"';} ?> >120-02 Operaciones financieras de activo</option>
<option value='120-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='120-03'){ echo 'selected="selected"';} ?> >120-03 Operaciones financieras internacionales</option>
<option value='120-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='120-04'){ echo 'selected="selected"';} ?> >120-04 Operaciones financieras internas</option>
<option value='120-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='120-05'){ echo 'selected="selected"';} ?> >120-05 Gestión comercial de servicios financieros</option>
<option value='121-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='121-00'){ echo 'selected="selected"';} ?> >121-00 Operativa de agencias de viajes en general</option>
<option value='122-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-00'){ echo 'selected="selected"';} ?> >122-00 Procesos de producción, transformación y distribución de energía y agua en general</option>
<option value='122-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-01'){ echo 'selected="selected"';} ?> >122-01 Operación y mantenimiento de centrales hidroeléctricas</option>
<option value='122-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-02'){ echo 'selected="selected"';} ?> >122-02 Supervisión de centrales hidroeléctricas</option>
<option value='122-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-03'){ echo 'selected="selected"';} ?> >122-03 Operación de centrales termoeléctrica</option>
<option value='122-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-04'){ echo 'selected="selected"';} ?> >122-04 Control de centrales termoeléctricas</option>
<option value='122-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-05'){ echo 'selected="selected"';} ?> >122-05 Montaje eléctrico de centrales eléctricas</option>
<option value='122-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-06'){ echo 'selected="selected"';} ?> >122-06 Montaje mecánico de centrales eléctricas</option>
<option value='122-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-07'){ echo 'selected="selected"';} ?> >122-07 Montaje de equipos de instrumentación y control  de central eléctrica</option>
<option value='122-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-08'){ echo 'selected="selected"';} ?> >122-08 Operaciones de instrumentación y control de centrales eléctricas</option>
<option value='122-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-09'){ echo 'selected="selected"';} ?> >122-09 Operaciones en líneas eléctricas de alta tensión</option>
<option value='122-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-10'){ echo 'selected="selected"';} ?> >122-10 Montaje y mantenimiento de subestaciones eléctricas</option>
<option value='122-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-11'){ echo 'selected="selected"';} ?> >122-11 Operaciones en  subestaciones eléctricas de alta tensión</option>
<option value='122-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-12'){ echo 'selected="selected"';} ?> >122-12 Operaciones en redes y centros de distribución de energía eléctrica</option>
<option value='122-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-13'){ echo 'selected="selected"';} ?> >122-13 Montaje y mantenimiento de instalaciones de distribución de energía eléctrica</option>
<option value='122-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-14'){ echo 'selected="selected"';} ?> >122-14 Operación de centros de maniobra de distribución de energía eléctrica</option>
<option value='122-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-15'){ echo 'selected="selected"';} ?> >122-15 Instalación de sistemas de energía solar térmica</option>
<option value='122-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-16'){ echo 'selected="selected"';} ?> >122-16 Instalación de sistemas fotovoltaicos y eólicos</option>
<option value='122-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-17'){ echo 'selected="selected"';} ?> >122-17 Técnicas de sistemas de energías renovables</option>
<option value='122-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-18'){ echo 'selected="selected"';} ?> >122-18 Operaciones de Calderas Industriales</option>
<option value='122-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-19'){ echo 'selected="selected"';} ?> >122-19 Operaciones en gasoductos</option>
<option value='122-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-20'){ echo 'selected="selected"';} ?> >122-20 Operaciones en sistemas de distribución de gas</option>
<option value='122-21' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-21'){ echo 'selected="selected"';} ?> >122-21 Supervisión de sistemas de distribución de gas</option>
<option value='122-22' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-22'){ echo 'selected="selected"';} ?> >122-22 Mantenimiento de plantas de captación y tratamiento de agua</option>
<option value='122-23' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-23'){ echo 'selected="selected"';} ?> >122-23 Operaciones en plantas de tratamiento de agua</option>
<option value='122-24' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-24'){ echo 'selected="selected"';} ?> >122-24 Operaciones en sistemas de distribución de agua</option>
<option value='122-25' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='122-25'){ echo 'selected="selected"';} ?> >122-25 Técnicas de sistemas de distribución de agua</option>
<option value='123-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='123-00'){ echo 'selected="selected"';} ?> >123-00 Operativa de seguros en general</option>
<option value='123-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='123-01'){ echo 'selected="selected"';} ?> >123-01 Técnicas administrativas de seguros</option>
<option value='123-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='123-02'){ echo 'selected="selected"';} ?> >123-02 Comercialización de seguros</option>
<option value='124-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='124-00'){ echo 'selected="selected"';} ?> >124-00 Operativa de transportes en general</option>
<option value='124-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='124-01'){ echo 'selected="selected"';} ?> >124-01 Operativa de transportes por carretera</option>
<option value='124-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='124-02'){ echo 'selected="selected"';} ?> >124-02 Operativa de transporte marítimo</option>
<option value='124-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='124-03'){ echo 'selected="selected"';} ?> >124-03 Operativa de transporte por ferrocarril. Control de circulación</option>
<option value='124-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='124-04'){ echo 'selected="selected"';} ?> >124-04 Atención a pasajeros en aeronaves</option>
<option value='124-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='124-05'){ echo 'selected="selected"';} ?> >124-05 Operación de centros de facilitación aeroportuaria</option>
<option value='125-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='125-00'){ echo 'selected="selected"';} ?> >125-00 Organización de centros educativos en general</option>
<option value='127-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='127-01'){ echo 'selected="selected"';} ?> >127-01 Legislación y normativa comunitaria</option>
<option value='127-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='127-02'){ echo 'selected="selected"';} ?> >127-02 Legislación y normativa internacional no comunitaria</option>
<option value='127-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='127-03'){ echo 'selected="selected"';} ?> >127-03 Legislación y normativa financiera y tributaria</option>
<option value='127-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='127-04'){ echo 'selected="selected"';} ?> >127-04 Legislación y normativa sociolaboral</option>
<option value='127-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='127-05'){ echo 'selected="selected"';} ?> >127-05 Legislación y normativa mercantil</option>
<option value='127-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='127-06'){ echo 'selected="selected"';} ?> >127-06 Legislación y normativa medioambiental</option>
<option value='127-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='127-07'){ echo 'selected="selected"';} ?> >127-07 Legislación y normativa administrativa (excepto la medioambiental)</option>
<option value='127-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='127-99'){ echo 'selected="selected"';} ?> >127-99 Otra legislación y normativa</option>
<option value='129-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='129-00'){ echo 'selected="selected"';} ?> >129-00 Montaje, instalación de equipos de uso no industrial en general</option>
<option value='133-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='133-00'){ echo 'selected="selected"';} ?> >133-00 Planificación y organización empresarial en general</option>
<option value='135-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-01'){ echo 'selected="selected"';} ?> >135-01 Carpintería-ebanistería artesana</option>
<option value='135-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-02'){ echo 'selected="selected"';} ?> >135-02 Restauración en madera</option>
<option value='135-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-03'){ echo 'selected="selected"';} ?> >135-03 Elaboración de objetos de fibra vegetal</option>
<option value='135-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-04'){ echo 'selected="selected"';} ?> >135-04 Cerámica</option>
<option value='135-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-05'){ echo 'selected="selected"';} ?> >135-05 Talla en piedra</option>
<option value='135-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-06'){ echo 'selected="selected"';} ?> >135-06 Soplado de vidrio</option>
<option value='135-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-07'){ echo 'selected="selected"';} ?> >135-07 Vidriería artística</option>
<option value='135-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-08'){ echo 'selected="selected"';} ?> >135-08 Decoración de objetos de vidrio</option>
<option value='135-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-09'){ echo 'selected="selected"';} ?> >135-09 Fundición artesana</option>
<option value='135-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-10'){ echo 'selected="selected"';} ?> >135-10 Cerrajería artística</option>
<option value='135-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-11'){ echo 'selected="selected"';} ?> >135-11 Calderería artística</option>
<option value='135-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-12'){ echo 'selected="selected"';} ?> >135-12 Marroquinería</option>
<option value='135-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-13'){ echo 'selected="selected"';} ?> >135-13 Guarnicionería</option>
<option value='135-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-14'){ echo 'selected="selected"';} ?> >135-14 Zapatería</option>
<option value='135-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-15'){ echo 'selected="selected"';} ?> >135-15 Tejeduría manual</option>
<option value='135-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-16'){ echo 'selected="selected"';} ?> >135-16 Adornos textiles</option>
<option value='135-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-17'){ echo 'selected="selected"';} ?> >135-17 Modistería</option>
<option value='135-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-18'){ echo 'selected="selected"';} ?> >135-18 Sastrería</option>
<option value='135-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-19'){ echo 'selected="selected"';} ?> >135-19 Costura</option>
<option value='135-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-20'){ echo 'selected="selected"';} ?> >135-20 Joyería</option>
<option value='135-21' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-21'){ echo 'selected="selected"';} ?> >135-21 Platería</option>
<option value='135-22' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-22'){ echo 'selected="selected"';} ?> >135-22 Luthería</option>
<option value='135-23' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-23'){ echo 'selected="selected"';} ?> >135-23 Encuadernación-restauración de libros</option>
<option value='135-24' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-24'){ echo 'selected="selected"';} ?> >135-24 Muñequería</option>
<option value='135-25' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-25'){ echo 'selected="selected"';} ?> >135-25 Elaboración de figuras plásticas</option>
<option value='135-26' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-26'){ echo 'selected="selected"';} ?> >135-26 Reparación de relojes</option>
<option value='135-27' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-27'){ echo 'selected="selected"';} ?> >135-27 Construcción de maquetas</option>
<option value='135-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='135-99'){ echo 'selected="selected"';} ?> >135-99 Otros Procesos productivos de Artesanía</option>
<option value='136-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='136-00'){ echo 'selected="selected"';} ?> >136-00 Procesos productivos Automoción en general</option>
<option value='137-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-01'){ echo 'selected="selected"';} ?> >137-01 Sacrificio de ganado</option>
<option value='137-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-02'){ echo 'selected="selected"';} ?> >137-02 Carnicería</option>
<option value='137-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-03'){ echo 'selected="selected"';} ?> >137-03 Elaboración de productos cárnicos</option>
<option value='137-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-04'){ echo 'selected="selected"';} ?> >137-04 Procesado de leche</option>
<option value='137-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-05'){ echo 'selected="selected"';} ?> >137-05 Elaboración de quesos</option>
<option value='137-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-06'){ echo 'selected="selected"';} ?> >137-06 Elaboración de helados</option>
<option value='137-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-07'){ echo 'selected="selected"';} ?> >137-07 Elaboración de productos lácteos y ovoproductos</option>
<option value='137-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-08'){ echo 'selected="selected"';} ?> >137-08 Elaboración de aceites y grasas</option>
<option value='137-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-09'){ echo 'selected="selected"';} ?> >137-09 Panadería</option>
<option value='137-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-10'){ echo 'selected="selected"';} ?> >137-10 Pastelería</option>
<option value='137-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-11'){ echo 'selected="selected"';} ?> >137-11 Elaboración de galletas</option>
<option value='137-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-12'){ echo 'selected="selected"';} ?> >137-12 Elaboración de cacao y chocolate</option>
<option value='137-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-13'){ echo 'selected="selected"';} ?> >137-13 Elaboración de turrones y mazapanes</option>
<option value='137-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-14'){ echo 'selected="selected"';} ?> >137-14 Elaboración de caramelos y dulces</option>
<option value='137-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-15'){ echo 'selected="selected"';} ?> >137-15 Elaboración de conservas de productos de la pesca</option>
<option value='137-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-16'){ echo 'selected="selected"';} ?> >137-16 Elaboración de conservas vegetales</option>
<option value='137-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-17'){ echo 'selected="selected"';} ?> >137-17 Procesado de productos congelados</option>
<option value='137-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-18'){ echo 'selected="selected"';} ?> >137-18 Molinería</option>
<option value='137-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-19'){ echo 'selected="selected"';} ?> >137-19 Elaboración de piensos compuestos</option>
<option value='137-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-20'){ echo 'selected="selected"';} ?> >137-20 Elaboración de azúcar</option>
<option value='137-21' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-21'){ echo 'selected="selected"';} ?> >137-21 Elaboración de precocinados y cocinados</option>
<option value='137-22' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-22'){ echo 'selected="selected"';} ?> >137-22 Procesado de catering</option>
<option value='137-23' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-23'){ echo 'selected="selected"';} ?> >137-23 Elaboración de café</option>
<option value='137-24' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-24'){ echo 'selected="selected"';} ?> >137-24 Elaboración de té e infusiones</option>
<option value='137-25' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-25'){ echo 'selected="selected"';} ?> >137-25 Elaboración de frutos secos y aperitivos snacks</option>
<option value='137-26' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-26'){ echo 'selected="selected"';} ?> >137-26 Elaboración de sopas, salsas, caldos y postres deshidratados</option>
<option value='137-27' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-27'){ echo 'selected="selected"';} ?> >137-27 Elaboración de alimentos infantiles</option>
<option value='137-28' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-28'){ echo 'selected="selected"';} ?> >137-28 Pescadería</option>
<option value='137-29' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-29'){ echo 'selected="selected"';} ?> >137-29 Elaboración de vinos</option>
<option value='137-30' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-30'){ echo 'selected="selected"';} ?> >137-30 Elaboración de sidras</option>
<option value='137-31' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-31'){ echo 'selected="selected"';} ?> >137-31 Elaboración de cerveza</option>
<option value='137-32' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-32'){ echo 'selected="selected"';} ?> >137-32 Elaboración de alcoholes y licores</option>
<option value='137-33' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-33'){ echo 'selected="selected"';} ?> >137-33 Elaboración de bebidas analcoholicas</option>
<option value='137-34' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-34'){ echo 'selected="selected"';} ?> >137-34 Elaboración de tabaco</option>
<option value='137-35' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-35'){ echo 'selected="selected"';} ?> >137-35 Trabajos auxiliares en industrias alimentarias</option>
<option value='137-36' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-36'){ echo 'selected="selected"';} ?> >137-36 Almacenaje en industrias alimentarias</option>
<option value='137-37' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-37'){ echo 'selected="selected"';} ?> >137-37 Envasado de productos alimentarios</option>
<option value='137-38' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-38'){ echo 'selected="selected"';} ?> >137-38 Supervisión de operaciones de industrias alimentarias</option>
<option value='137-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='137-99'){ echo 'selected="selected"';} ?> >137-99 Otros Procesos productivos-Ind-Alimentarias</option>
<option value='138-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-01'){ echo 'selected="selected"';} ?> >138-01 Operación de montaje y ajuste mecánico</option>
<option value='138-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-02'){ echo 'selected="selected"';} ?> >138-02 Técnicas de ajuste mecánico</option>
<option value='138-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-03'){ echo 'selected="selected"';} ?> >138-03 Operación de máquinas-herramienta</option>
<option value='138-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-04'){ echo 'selected="selected"';} ?> >138-04 Operación de torno y fresadura</option>
<option value='138-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-05'){ echo 'selected="selected"';} ?> >138-05 Construcción de prototipos mecánicos</option>
<option value='138-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-06'){ echo 'selected="selected"';} ?> >138-06 Desarrollo de productos mecánicos</option>
<option value='138-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-07'){ echo 'selected="selected"';} ?> >138-07 Trabajos auxiliares de electricidad</option>
<option value='138-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-08'){ echo 'selected="selected"';} ?> >138-08 Electricidad industrial</option>
<option value='138-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-09'){ echo 'selected="selected"';} ?> >138-09 Diseño de sistemas de control eléctrico</option>
<option value='138-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-10'){ echo 'selected="selected"';} ?> >138-10 Operaciones de matricería y moldes</option>
<option value='138-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='138-99'){ echo 'selected="selected"';} ?> >138-99 Otros Procesos productivos- Industrias de fabricación de equipos mecánicos, eléctricos y electrónicos</option>
<option value='139-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='139-01'){ echo 'selected="selected"';} ?> >139-01 Procesos de fundición</option>
<option value='139-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='139-02'){ echo 'selected="selected"';} ?> >139-02 Procesos de forja</option>
<option value='139-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='139-03'){ echo 'selected="selected"';} ?> >139-03 Procesos de laminación</option>
<option value='139-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='139-04'){ echo 'selected="selected"';} ?> >139-04 Procesos de estirado</option>
<option value='139-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='139-05'){ echo 'selected="selected"';} ?> >139-05 Procesos de tratamientos térmicos</option>
<option value='139-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='139-06'){ echo 'selected="selected"';} ?> >139-06 Recubrimiento de superficies metálicas</option>
<option value='139-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='139-99'){ echo 'selected="selected"';} ?> >139-99 Otros procesos productivos industria pesada: Construcciones metálicas y metalurgia en general</option>
<option value='140-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-01'){ echo 'selected="selected"';} ?> >140-01 Técnicas de producción editorial</option>
<option value='140-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-02'){ echo 'selected="selected"';} ?> >140-02 Técnicas de edición</option>
<option value='140-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-03'){ echo 'selected="selected"';} ?> >140-03 Realización de proyectos gráficos y maquetas</option>
<option value='140-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-04'){ echo 'selected="selected"';} ?> >140-04 Redacción y corrección de textos</option>
<option value='140-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-05'){ echo 'selected="selected"';} ?> >140-05 Trabajos de pre-impresión</option>
<option value='140-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-06'){ echo 'selected="selected"';} ?> >140-06 Preparación de textos</option>
<option value='140-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-07'){ echo 'selected="selected"';} ?> >140-07 Preparación de imágenes</option>
<option value='140-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-08'){ echo 'selected="selected"';} ?> >140-08 Integración de fotorreproducción</option>
<option value='140-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-09'){ echo 'selected="selected"';} ?> >140-09 Elaboración de pruebas</option>
<option value='140-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-10'){ echo 'selected="selected"';} ?> >140-10 Montaje y preparación de la forma impresora</option>
<option value='140-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-11'){ echo 'selected="selected"';} ?> >140-11 Impresión en huecograbado</option>
<option value='140-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-12'){ echo 'selected="selected"';} ?> >140-12 Impresión en offset-bobina</option>
<option value='140-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-13'){ echo 'selected="selected"';} ?> >140-13 Impresión en offset-hoja</option>
<option value='140-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-14'){ echo 'selected="selected"';} ?> >140-14 Impresión de flexografía</option>
<option value='140-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-15'){ echo 'selected="selected"';} ?> >140-15 Impresión de serigrafía</option>
<option value='140-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-16'){ echo 'selected="selected"';} ?> >140-16 Impresión de pequeños formatos</option>
<option value='140-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-17'){ echo 'selected="selected"';} ?> >140-17 Confección de complejos</option>
<option value='140-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-18'){ echo 'selected="selected"';} ?> >140-18 Confección de bolsas y sobres</option>
<option value='140-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-19'){ echo 'selected="selected"';} ?> >140-19 Extrusionado</option>
<option value='140-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-20'){ echo 'selected="selected"';} ?> >140-20 Encuadernación industrial</option>
<option value='140-21' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-21'){ echo 'selected="selected"';} ?> >140-21 Contracolado-engomado</option>
<option value='140-22' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-22'){ echo 'selected="selected"';} ?> >140-22 Troquelado</option>
<option value='140-23' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-23'){ echo 'selected="selected"';} ?> >140-23 Confección de productos de cartón</option>
<option value='140-24' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-24'){ echo 'selected="selected"';} ?> >140-24 Confección de etiquetas</option>
<option value='140-25' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-25'){ echo 'selected="selected"';} ?> >140-25 Maquetación de prensa</option>
<option value='140-26' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-26'){ echo 'selected="selected"';} ?> >140-26 Infografía de prensa</option>
<option value='140-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='140-99'){ echo 'selected="selected"';} ?> >140-99 Otros Procesos productivos- Industrias gráficas</option>
<option value='141-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-01'){ echo 'selected="selected"';} ?> >141-01 Fabricación de abrasivos rígidos</option>
<option value='141-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-02'){ echo 'selected="selected"';} ?> >141-02 Fabricación de abrasivos flexibles</option>
<option value='141-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-03'){ echo 'selected="selected"';} ?> >141-03 Fabricación de cerámicas especiales</option>
<option value='141-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-04'){ echo 'selected="selected"';} ?> >141-04 Fabricación de ladrillos y revestimientos cerámicos</option>
<option value='141-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-05'){ echo 'selected="selected"';} ?> >141-05 Fabricación de artículos del hogar y porcelana sanitaria</option>
<option value='141-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-06'){ echo 'selected="selected"';} ?> >141-06 Fabricación de vidrio y transformado en vidrio</option>
<option value='141-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-07'){ echo 'selected="selected"';} ?> >141-07 Elaboración de artículos de vidrio y transformados en vidrio</option>
<option value='141-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-08'){ echo 'selected="selected"';} ?> >141-08 Fabricación de fibra de vidrio</option>
<option value='141-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-09'){ echo 'selected="selected"';} ?> >141-09 Fabricación de vidrio óptico</option>
<option value='141-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-10'){ echo 'selected="selected"';} ?> >141-10 Fabricación de cadenas de joyería y bisutería</option>
<option value='141-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-11'){ echo 'selected="selected"';} ?> >141-11 Fabricación de artículos de joyería y bisutería por colada</option>
<option value='141-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-12'){ echo 'selected="selected"';} ?> >141-12 Fabricación de joyería y bisutería por prensado</option>
<option value='141-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-13'){ echo 'selected="selected"';} ?> >141-13 Fabricación de artículos de joyería por conformado electrolítico</option>
<option value='141-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-14'){ echo 'selected="selected"';} ?> >141-14 Acabado y verificado de artículos de joyería y bisutería</option>
<option value='141-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-15'){ echo 'selected="selected"';} ?> >141-15 Transformado de plástico para juguetes</option>
<option value='141-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-16'){ echo 'selected="selected"';} ?> >141-16 Manufacturado de juguetes de plástico y textil</option>
<option value='141-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-17'){ echo 'selected="selected"';} ?> >141-17 Fabricación de juguetes metálicos</option>
<option value='141-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-18'){ echo 'selected="selected"';} ?> >141-18 Fabricación de juguetes educativos y de sociedad</option>
<option value='141-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-19'){ echo 'selected="selected"';} ?> >141-19 Elaboración de lápices</option>
<option value='141-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-20'){ echo 'selected="selected"';} ?> >141-20 Fabricación de máquinas de escritorio</option>
<option value='141-21' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-21'){ echo 'selected="selected"';} ?> >141-21 Fabricación de cremalleras</option>
<option value='141-22' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-22'){ echo 'selected="selected"';} ?> >141-22 Teñido de artículos diversos</option>
<option value='141-23' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-23'){ echo 'selected="selected"';} ?> >141-23 Fabricación de cursores y remaches</option>
<option value='141-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='141-99'){ echo 'selected="selected"';} ?> >141-99 Otros procesos productivos-Industrias manufactureras diversas</option>
<option value='142-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-01'){ echo 'selected="selected"';} ?> >142-01 Operación de plantas químicas</option>
<option value='142-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-02'){ echo 'selected="selected"';} ?> >142-02 Técnicas de plantas químicas</option>
<option value='142-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-03'){ echo 'selected="selected"';} ?> >142-03 Operación en la fabricación en industrias farmacéuticas</option>
<option value='142-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-04'){ echo 'selected="selected"';} ?> >142-04 Técnicas de fabricación en industrias farmacéuticas</option>
<option value='142-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-05'){ echo 'selected="selected"';} ?> >142-05 Operación en la fabricación en industrias petroquímicas</option>
<option value='142-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-06'){ echo 'selected="selected"';} ?> >142-06 Técnicas de fabricación en industrias petroquímicas</option>
<option value='142-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-07'){ echo 'selected="selected"';} ?> >142-07 Operación en la fabricación de productos en otras industrias químicas</option>
<option value='142-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-08'){ echo 'selected="selected"';} ?> >142-08 Técnicas de fabricación en otras industrias químicas</option>
<option value='142-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-09'){ echo 'selected="selected"';} ?> >142-09 Operación en la transformación de plástico y caucho</option>
<option value='142-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-10'){ echo 'selected="selected"';} ?> >142-10 Técnicas de transformación de plástico y caucho</option>
<option value='142-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-11'){ echo 'selected="selected"';} ?> >142-11 Operación de industrias papeleras</option>
<option value='142-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-12'){ echo 'selected="selected"';} ?> >142-12 Técnicas de industrias papeleras</option>
<option value='142-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='142-99'){ echo 'selected="selected"';} ?> >142-99 Otros procesos productivos-Industrias químicas</option>
<option value='143-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-01'){ echo 'selected="selected"';} ?> >143-01 Procesado de fibras</option>
<option value='143-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-02'){ echo 'selected="selected"';} ?> >143-02 Procesado de fibras para la hilatura</option>
<option value='143-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-03'){ echo 'selected="selected"';} ?> >143-03 Hilado y acabado de hilados</option>
<option value='143-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-04'){ echo 'selected="selected"';} ?> >143-04 Coordinación técnica de producción de hilatura</option>
<option value='143-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-05'){ echo 'selected="selected"';} ?> >143-05 Control de calidad de hilatura</option>
<option value='143-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-06'){ echo 'selected="selected"';} ?> >143-06 Urdido y encolado</option>
<option value='143-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-07'){ echo 'selected="selected"';} ?> >143-07 Preparación de monturas y C:A:D:Jacquard</option>
<option value='143-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-08'){ echo 'selected="selected"';} ?> >143-08 Tejeduría en telar de calada</option>
<option value='143-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-09'){ echo 'selected="selected"';} ?> >143-09 Revisado y reparado de productos textiles. Tejeduría de calada</option>
<option value='143-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-10'){ echo 'selected="selected"';} ?> >143-10 Coordinación técnica de producción de tejeduría de calada</option>
<option value='143-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-11'){ echo 'selected="selected"';} ?> >143-11 Control de calidad de tejeduría de calada</option>
<option value='143-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-12'){ echo 'selected="selected"';} ?> >143-12 Procesado de telas no tejidas</option>
<option value='143-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-13'){ echo 'selected="selected"';} ?> >143-13 Coordinación técnica de producción de telas no tejidas</option>
<option value='143-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-14'){ echo 'selected="selected"';} ?> >143-14 Control de calidad de telas no tejidas</option>
<option value='143-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-15'){ echo 'selected="selected"';} ?> >143-15 Tejeduría de género de punto en máquinas de recogida</option>
<option value='143-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-16'){ echo 'selected="selected"';} ?> >143-16 Tejeduría de género de punto en máquinas de urdimbre</option>
<option value='143-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-17'){ echo 'selected="selected"';} ?> >143-17 Revisado y reparado de productos textiles. Géneros de punto</option>
<option value='143-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-18'){ echo 'selected="selected"';} ?> >143-18 Coordinación técnica de producción de géneros de punto</option>
<option value='143-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-19'){ echo 'selected="selected"';} ?> >143-19 Control de calidad de género de punto</option>
<option value='143-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-20'){ echo 'selected="selected"';} ?> >143-20 Preparado y blanqueado textil</option>
<option value='143-21' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-21'){ echo 'selected="selected"';} ?> >143-21 Estampado textil</option>
<option value='143-22' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-22'){ echo 'selected="selected"';} ?> >143-22 Teñido textil</option>
<option value='143-23' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-23'){ echo 'selected="selected"';} ?> >143-23 Acabados y aprestos</option>
<option value='143-24' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-24'){ echo 'selected="selected"';} ?> >143-24 Preparación de disoluciones</option>
<option value='143-25' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-25'){ echo 'selected="selected"';} ?> >143-25 Coordinación técnica de producción de ennoblecimiento textil</option>
<option value='143-26' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-26'){ echo 'selected="selected"';} ?> >143-26 Control de calidad de ennoblecimiento textil</option>
<option value='143-27' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-27'){ echo 'selected="selected"';} ?> >143-27 Confección de patrones y escalado</option>
<option value='143-28' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-28'){ echo 'selected="selected"';} ?> >143-28 Operación de máquinas de confección industrial</option>
<option value='143-29' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-29'){ echo 'selected="selected"';} ?> >143-29 Planchado</option>
<option value='143-30' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-30'){ echo 'selected="selected"';} ?> >143-30 Bordado y acolchado</option>
<option value='143-31' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-31'){ echo 'selected="selected"';} ?> >143-31 Revisión de productos textiles de confección</option>
<option value='143-32' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-32'){ echo 'selected="selected"';} ?> >143-32 Coordinación técnica de producción de confección</option>
<option value='143-33' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-33'){ echo 'selected="selected"';} ?> >143-33 Control de calidad de confección</option>
<option value='143-34' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-34'){ echo 'selected="selected"';} ?> >143-34 Curtición de pieles y cueros</option>
<option value='143-35' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-35'){ echo 'selected="selected"';} ?> >143-35 Acabado de cueros</option>
<option value='143-36' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-36'){ echo 'selected="selected"';} ?> >143-36 Control de procesos de curtición</option>
<option value='143-37' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-37'){ echo 'selected="selected"';} ?> >143-37 Confección de patrones de calzado</option>
<option value='143-38' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-38'){ echo 'selected="selected"';} ?> >143-38 Cortado de cuero, ante y napa</option>
<option value='143-39' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-39'){ echo 'selected="selected"';} ?> >143-39 Preparación y cosido de cuero, ante y napa</option>
<option value='143-40' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-40'){ echo 'selected="selected"';} ?> >143-40 Montado de calzado</option>
<option value='143-41' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-41'){ echo 'selected="selected"';} ?> >143-41 Terminado de calzado</option>
<option value='143-42' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-42'){ echo 'selected="selected"';} ?> >143-42 Confección de patrones de maroquinería y guantería</option>
<option value='143-43' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-43'){ echo 'selected="selected"';} ?> >143-43 Marroquinería industrial</option>
<option value='143-44' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-44'){ echo 'selected="selected"';} ?> >143-44 Confección de patrones de peletería, ante y napa</option>
<option value='143-45' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-45'){ echo 'selected="selected"';} ?> >143-45 Cortado, clavado y clasificado de peletería</option>
<option value='143-46' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-46'){ echo 'selected="selected"';} ?> >143-46 Operación de máquinas de peletería</option>
<option value='143-47' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-47'){ echo 'selected="selected"';} ?> >143-47 Forrado y terminado de peletería, ante y napa</option>
<option value='143-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='143-99'){ echo 'selected="selected"';} ?> >143-99 Otros procesos productivos- Industrias textiles, de la piel y el cuero</option>
<option value='144-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-01'){ echo 'selected="selected"';} ?> >144-01 Prospecciones y sondeos</option>
<option value='144-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-02'){ echo 'selected="selected"';} ?> >144-02 Preparación y conservación de galerías</option>
<option value='144-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-03'){ echo 'selected="selected"';} ?> >144-03 Arranque de carbón</option>
<option value='144-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-04'){ echo 'selected="selected"';} ?> >144-04 Transporte y extracción de mineral</option>
<option value='144-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-05'){ echo 'selected="selected"';} ?> >144-05 Electromecánica minera</option>
<option value='144-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-06'){ echo 'selected="selected"';} ?> >144-06 Extracción por sutiraje</option>
<option value='144-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-07'){ echo 'selected="selected"';} ?> >144-07 Aplicación de explosivos y arranque de minerales</option>
<option value='144-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-08'){ echo 'selected="selected"';} ?> >144-08 Minería de rocas para usos ornamentales</option>
<option value='144-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-09'){ echo 'selected="selected"';} ?> >144-09 Obtención y tratamiento de sal común</option>
<option value='144-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-10'){ echo 'selected="selected"';} ?> >144-10 Tratamiento y clasificación de rocas y minerales</option>
<option value='144-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-11'){ echo 'selected="selected"';} ?> >144-11 Beneficio de rocas para ornamentación</option>
<option value='144-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-12'){ echo 'selected="selected"';} ?> >144-12 Operación de coquización</option>
<option value='144-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-13'){ echo 'selected="selected"';} ?> >144-13 Sinterización de minerales de hierro</option>
<option value='144-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-14'){ echo 'selected="selected"';} ?> >144-14 Operación de horno alto</option>
<option value='144-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-15'){ echo 'selected="selected"';} ?> >144-15 Procesos de lixiviación y electrolisis</option>
<option value='144-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-16'){ echo 'selected="selected"';} ?> >144-16 Procesos de fusión y colada</option>
<option value='144-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-17'){ echo 'selected="selected"';} ?> >144-17 Obtención de alúmina y aluminio</option>
<option value='144-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-18'){ echo 'selected="selected"';} ?> >144-18 Operación de hornos de tostación, calcinación y sinterización</option>
<option value='144-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='144-99'){ echo 'selected="selected"';} ?> >144-99 Otros procesos productivos- Minería y primeras transformaciones</option>
<option value='145-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-01'){ echo 'selected="selected"';} ?> >145-01 Piscicultura de criadero</option>
<option value='145-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-02'){ echo 'selected="selected"';} ?> >145-02 Piscicultura de engorde en aguas marinas</option>
<option value='145-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-03'){ echo 'selected="selected"';} ?> >145-03 Piscicultura en aguas continentales</option>
<option value='145-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-04'){ echo 'selected="selected"';} ?> >145-04 Reproducción y cultivo de larvas</option>
<option value='145-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-05'){ echo 'selected="selected"';} ?> >145-05 Estabulación de crustáceos (Cetareas)</option>
<option value='145-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-06'){ echo 'selected="selected"';} ?> >145-06 Cultivo de crustáceos (Criadero y engorde)</option>
<option value='145-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-07'){ echo 'selected="selected"';} ?> >145-07 Cultivo de moluscos en criadero</option>
<option value='145-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-08'){ echo 'selected="selected"';} ?> >145-08 Cultivo de moluscos en medio natural</option>
<option value='145-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-09'){ echo 'selected="selected"';} ?> >145-09 Reproducción de moluscos</option>
<option value='145-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-10'){ echo 'selected="selected"';} ?> >145-10 Marisqueo</option>
<option value='145-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-11'){ echo 'selected="selected"';} ?> >145-11 Cultivo de zooplancton</option>
<option value='145-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-12'){ echo 'selected="selected"';} ?> >145-12 Cultivo de fitoplancton</option>
<option value='145-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-13'){ echo 'selected="selected"';} ?> >145-13 Cultivo de macroalgas en medio natural</option>
<option value='145-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-14'){ echo 'selected="selected"';} ?> >145-14 Recolección de macroalgas</option>
<option value='145-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-15'){ echo 'selected="selected"';} ?> >145-15 Pesca de bajura</option>
<option value='145-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-16'){ echo 'selected="selected"';} ?> >145-16 Pesca de litoral</option>
<option value='145-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-17'){ echo 'selected="selected"';} ?> >145-17 Mecánica de pesca en litoral</option>
<option value='145-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-18'){ echo 'selected="selected"';} ?> >145-18 Pesca de altura y gran altura</option>
<option value='145-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-19'){ echo 'selected="selected"';} ?> >145-19 Mecánica de pesca en altura y gran altura</option>
<option value='145-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-20'){ echo 'selected="selected"';} ?> >145-20 Buceo profesional</option>
<option value='145-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='145-99'){ echo 'selected="selected"';} ?> >145-99 Otros procesos productivos-Pesca y acuicultura</option>
<option value='146-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-01'){ echo 'selected="selected"';} ?> >146-01 Aserrado  de madera</option>
<option value='146-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-02'){ echo 'selected="selected"';} ?> >146-02 Secado y tratamiento de madera</option>
<option value='146-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-03'){ echo 'selected="selected"';} ?> >146-03 Fabricación de chapa y tablero contrachapado</option>
<option value='146-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-04'){ echo 'selected="selected"';} ?> >146-04 Fabricación de laminados de madera</option>
<option value='146-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-05'){ echo 'selected="selected"';} ?> >146-05 Fabricación de tableros de partículas y de fibras</option>
<option value='146-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-06'){ echo 'selected="selected"';} ?> >146-06 Elaboración de proyectos de carpintería y mueble</option>
<option value='146-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-07'){ echo 'selected="selected"';} ?> >146-07 Despiece de madera y tableros</option>
<option value='146-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-08'){ echo 'selected="selected"';} ?> >146-08 Mecanizado de madera y tableros</option>
<option value='146-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-09'){ echo 'selected="selected"';} ?> >146-09 Armado y montaje de carpintería y mueble</option>
<option value='146-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-10'){ echo 'selected="selected"';} ?> >146-10 Barnizado-lacado</option>
<option value='146-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-11'){ echo 'selected="selected"';} ?> >146-11 Tapicería de muebles</option>
<option value='146-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-12'){ echo 'selected="selected"';} ?> >146-12 Confección de muebles de caña, junco y mimbre</option>
<option value='146-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-13'){ echo 'selected="selected"';} ?> >146-13 Carpintería semi industrializada</option>
<option value='146-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-14'){ echo 'selected="selected"';} ?> >146-14 Carpintería de armar</option>
<option value='146-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-15'){ echo 'selected="selected"';} ?> >146-15 Carpintería de ribera</option>
<option value='146-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-16'){ echo 'selected="selected"';} ?> >146-16 Ebanistería semi industrializada</option>
<option value='146-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-17'){ echo 'selected="selected"';} ?> >146-17 Fabricación de toneles</option>
<option value='146-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-18'){ echo 'selected="selected"';} ?> >146-18 Preparación de corcho</option>
<option value='146-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-19'){ echo 'selected="selected"';} ?> >146-19 Fabricación de tapones</option>
<option value='146-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-20'){ echo 'selected="selected"';} ?> >146-20 Fabricación de artículos de corcho aglomerado</option>
<option value='146-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='146-99'){ echo 'selected="selected"';} ?> >146-99 Otros procesos productivos-Industrias de la madera y el corcho</option>
<option value='147-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-01'){ echo 'selected="selected"';} ?> >147-01 Cultivo de cereales y leguminosas de grano</option>
<option value='147-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-02'){ echo 'selected="selected"';} ?> >147-02 Cultivo de raíces y tubérculos</option>
<option value='147-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-03'){ echo 'selected="selected"';} ?> >147-03 Cultivo de plantas forrajeras y pratenses</option>
<option value='147-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-04'){ echo 'selected="selected"';} ?> >147-04 Cultivo de plantas industriales</option>
<option value='147-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-05'){ echo 'selected="selected"';} ?> >147-05 Fruticultura</option>
<option value='147-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-06'){ echo 'selected="selected"';} ?> >147-06 Viticultura</option>
<option value='147-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-07'){ echo 'selected="selected"';} ?> >147-07 Olivicultura</option>
<option value='147-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-08'){ echo 'selected="selected"';} ?> >147-08 Horticultura</option>
<option value='147-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-09'){ echo 'selected="selected"';} ?> >147-09 Producción de setas</option>
<option value='147-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-10'){ echo 'selected="selected"';} ?> >147-10 Producción de plantas hortícolas</option>
<option value='147-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-11'){ echo 'selected="selected"';} ?> >147-11 Manipulación de frutas y hortalizas</option>
<option value='147-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-12'){ echo 'selected="selected"';} ?> >147-12 Manipulación de tractores</option>
<option value='147-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-13'){ echo 'selected="selected"';} ?> >147-13 Manipulación de cosechadoras</option>
<option value='147-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='147-99'){ echo 'selected="selected"';} ?> >147-99 Producción agrícola- otros cultivos</option>
<option value='148-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='148-01'){ echo 'selected="selected"';} ?> >148-01 Explotación de ganado vacuno</option>
<option value='148-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='148-02'){ echo 'selected="selected"';} ?> >148-02 Explotación de ganado ovino-caprino</option>
<option value='148-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='148-03'){ echo 'selected="selected"';} ?> >148-03 Explotación de ganado equino</option>
<option value='148-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='148-04'){ echo 'selected="selected"';} ?> >148-04 Explotación extensiva de ganado porcino</option>
<option value='148-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='148-05'){ echo 'selected="selected"';} ?> >148-05 Explotación intensiva de ganado porcino</option>
<option value='148-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='148-06'){ echo 'selected="selected"';} ?> >148-06 Cunicultura</option>
<option value='148-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='148-07'){ echo 'selected="selected"';} ?> >148-07 Avicultura</option>
<option value='148-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='148-08'){ echo 'selected="selected"';} ?> >148-08 Apicultura</option>
<option value='148-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='148-99'){ echo 'selected="selected"';} ?> >148-99 Otros tipos de producción ganadera</option>
<option value='149-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='149-00'){ echo 'selected="selected"';} ?> >149-00 Producción y realización de audiovisuales, radio y espectáculos en general</option>
<option value='149-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='149-01'){ echo 'selected="selected"';} ?> >149-01 Producción de radio, cine, televisión, teatro y espectáculos</option>
<option value='149-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='149-02'){ echo 'selected="selected"';} ?> >149-02 Realización de radio, cine, televisión, teatro y espectáculos</option>
<option value='149-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='149-03'){ echo 'selected="selected"';} ?> >149-03 Técnicas audiovisuales</option>
<option value='149-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='149-04'){ echo 'selected="selected"';} ?> >149-04 Infografía en medios audiovisuales</option>
<option value='150-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='150-00'){ echo 'selected="selected"';} ?> >150-00 Productividad en general</option>
<option value='150-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='150-01'){ echo 'selected="selected"';} ?> >150-01 Herramientas de medida de la productividad</option>
<option value='151-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='151-00'){ echo 'selected="selected"';} ?> >151-00 Diagnóstico clínico y radioterapia en general</option>
<option value='151-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='151-01'){ echo 'selected="selected"';} ?> >151-01 Técnicas de radiodiagnóstico</option>
<option value='151-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='151-02'){ echo 'selected="selected"';} ?> >151-02 Técnicas de medicina nuclear</option>
<option value='151-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='151-03'){ echo 'selected="selected"';} ?> >151-03 Técnicas de radioterapia</option>
<option value='151-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='151-04'){ echo 'selected="selected"';} ?> >151-04 Técnicas no radiológicas de diagnóstico clínico</option>
<option value='151-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='151-99'){ echo 'selected="selected"';} ?> >151-99 Otras técnicas de diagnóstico clínico</option>
<option value='152-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='152-00'){ echo 'selected="selected"';} ?> >152-00 Publicidad y comunicación externa de la empresa en general</option>
<option value='152-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='152-01'){ echo 'selected="selected"';} ?> >152-01 Relaciones Públicas y protocolo empresarial</option>
<option value='153-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='153-00'){ echo 'selected="selected"';} ?> >153-00 Recepción hotelera en general</option>
<option value='154-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='154-00'){ echo 'selected="selected"';} ?> >154-00 Redacción/Guión/locución-Información y manifestaciones artísticas en general</option>
<option value='154-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='154-01'){ echo 'selected="selected"';} ?> >154-01 Técnicas de redacción, guión y periodismo</option>
<option value='154-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='154-02'){ echo 'selected="selected"';} ?> >154-02 Técnicas de locución y doblaje</option>
<option value='155-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='155-00'){ echo 'selected="selected"';} ?> >155-00 Reforma educativa de la LOGSE en general</option>
<option value='157-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='157-00'){ echo 'selected="selected"';} ?> >157-00 Salud laboral y enfermedades profesionales en general</option>
<option value='158-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='158-00'){ echo 'selected="selected"';} ?> >158-00 Seguridad de Instalaciones y dispositivos de alto riesgo en general</option>
<option value='159-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='159-00'){ echo 'selected="selected"';} ?> >159-00 Prevención de riesgos laborales en general</option>
<option value='159-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='159-01'){ echo 'selected="selected"';} ?> >159-01 Prevención de riesgos en la construcción</option>
<option value='160-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='160-00'){ echo 'selected="selected"';} ?> >160-00 Seguridad y vigilancia en general</option>
<option value='160-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='160-01'){ echo 'selected="selected"';} ?> >160-01 Seguridad privada</option>
<option value='160-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='160-02'){ echo 'selected="selected"';} ?> >160-02 Prevención de incendios</option>
<option value='160-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='160-03'){ echo 'selected="selected"';} ?> >160-03 Protección Civil</option>
<option value='161-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='161-00'){ echo 'selected="selected"';} ?> >161-00 Servicios Asistenciales en general</option>
<option value='161-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='161-01'){ echo 'selected="selected"';} ?> >161-01 Prevención de la marginación escolar y social</option>
<option value='161-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='161-02'){ echo 'selected="selected"';} ?> >161-02 Ayuda domiciliaria</option>
<option value='161-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='161-03'){ echo 'selected="selected"';} ?> >161-03 Servicios asistenciales a mayores: geriatría y gerocultura</option>
<option value='162-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-01'){ echo 'selected="selected"';} ?> >162-01 Peluquería</option>
<option value='162-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-02'){ echo 'selected="selected"';} ?> >162-02 Estilismo</option>
<option value='162-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-03'){ echo 'selected="selected"';} ?> >162-03 Manicura</option>
<option value='162-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-04'){ echo 'selected="selected"';} ?> >162-04 Estética</option>
<option value='162-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-05'){ echo 'selected="selected"';} ?> >162-05 Maquillaje</option>
<option value='162-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-06'){ echo 'selected="selected"';} ?> >162-06 Operación de cementerios</option>
<option value='162-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-07'){ echo 'selected="selected"';} ?> >162-07 Servicios funerarios</option>
<option value='162-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-08'){ echo 'selected="selected"';} ?> >162-08 Tanatología</option>
<option value='162-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-09'){ echo 'selected="selected"';} ?> >162-09 Atención al consumidor</option>
<option value='162-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-10'){ echo 'selected="selected"';} ?> >162-10 Servicio doméstico</option>
<option value='162-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='162-99'){ echo 'selected="selected"';} ?> >162-99 Otros Servicios Personales</option>
<option value='163-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='163-00'){ echo 'selected="selected"';} ?> >163-00 Servicios Recreativos, Culturales y Deportivos en general</option>
<option value='163-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='163-01'){ echo 'selected="selected"';} ?> >163-01 Mantenimiento de bienes culturales</option>
<option value='163-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='163-02'){ echo 'selected="selected"';} ?> >163-02 Animación deportiva</option>
<option value='163-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='163-03'){ echo 'selected="selected"';} ?> >163-03 Masaje deportivo</option>
<option value='163-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='163-04'){ echo 'selected="selected"';} ?> >163-04 Monitoraje deportivo</option>
<option value='163-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='163-05'){ echo 'selected="selected"';} ?> >163-05 Animación socio- cultural</option>
<option value='163-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='163-06'){ echo 'selected="selected"';} ?> >163-06 Conservación de parques y jardines</option>
<option value='163-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='163-07'){ echo 'selected="selected"';} ?> >163-07 Operación de parques zoológicos</option>
<option value='163-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='163-08'){ echo 'selected="selected"';} ?> >163-08 Operación de centros de recreo</option>
<option value='163-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='163-99'){ echo 'selected="selected"';} ?> >163-99 Otros servicios recreativos, culturales y deportivos</option>
<option value='164-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='164-00'){ echo 'selected="selected"';} ?> >164-00 Socorrismo y Primeros Auxilios en general</option>
<option value='164-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='164-01'){ echo 'selected="selected"';} ?> >164-01 Socorrismo acuático</option>
<option value='164-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='164-02'){ echo 'selected="selected"';} ?> >164-02 Primeros auxilios</option>
<option value='165-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='165-00'){ echo 'selected="selected"';} ?> >165-00 Soldadura en general</option>
<option value='165-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='165-01'){ echo 'selected="selected"';} ?> >165-01 Mantenimiento de estructuras metálicas</option>
<option value='165-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='165-02'){ echo 'selected="selected"';} ?> >165-02 Técnicas de soldadura de estructuras metálicas ligeras</option>
<option value='165-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='165-03'){ echo 'selected="selected"';} ?> >165-03 Técnicas de soldadura de estructuras metálicas pesadas</option>
<option value='165-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='165-04'){ echo 'selected="selected"';} ?> >165-04 Técnicas de soldadura de tuberías y recipientes de alta presión</option>
<option value='165-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='165-05'){ echo 'selected="selected"';} ?> >165-05 Homologación en Soldadura</option>
<option value='165-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='165-06'){ echo 'selected="selected"';} ?> >165-06 Soldadura especializada</option>
<option value='165-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='165-07'){ echo 'selected="selected"';} ?> >165-07 Soldadura eléctrica y oxigasística</option>
<option value='165-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='165-08'){ echo 'selected="selected"';} ?> >165-08 Supervisión de obra soldada</option>
<option value='165-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='165-99'){ echo 'selected="selected"';} ?> >165-99 Otras técnicas de soldadura</option>
<option value='166-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-01'){ echo 'selected="selected"';} ?> >166-01 Limpieza viaria</option>
<option value='166-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-02'){ echo 'selected="selected"';} ?> >166-02 Tratamiento y eliminación de residuos urbanos</option>
<option value='166-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-03'){ echo 'selected="selected"';} ?> >166-03 Depuración de aguas residuales</option>
<option value='166-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-04'){ echo 'selected="selected"';} ?> >166-04 Operación del mantenimiento  alcantarillado</option>
<option value='166-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-05'){ echo 'selected="selected"';} ?> >166-05 Supervisión del mantenimiento de alcantarillado</option>
<option value='166-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-06'){ echo 'selected="selected"';} ?> >166-06 Control de plagas</option>
<option value='166-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-07'){ echo 'selected="selected"';} ?> >166-07 Lavandería</option>
<option value='166-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-08'){ echo 'selected="selected"';} ?> >166-08 Tintorería</option>
<option value='166-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-09'){ echo 'selected="selected"';} ?> >166-09 Plancha</option>
<option value='166-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-10'){ echo 'selected="selected"';} ?> >166-10 Recepción de prendas para teñido y lavado</option>
<option value='166-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-11'){ echo 'selected="selected"';} ?> >166-11 Limpieza de inmuebles</option>
<option value='166-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='166-99'){ echo 'selected="selected"';} ?> >166-99 Otros servicios de limpieza y tratamiento de residuos urbanos</option>
<option value='167-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='167-00'){ echo 'selected="selected"';} ?> >167-00 Técnicas de venta y formación de vendedores en general</option>
<option value='168-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='168-00'){ echo 'selected="selected"';} ?> >168-00 Telecomunicaciones no informáticas en general</option>
<option value='169-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='169-00'){ echo 'selected="selected"';} ?> >169-00 Telemárketing / márketing telefónico</option>
<option value='170-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='170-00'){ echo 'selected="selected"';} ?> >170-00 Tratamiento de residuos (excepto residuos urbanos) en general</option>
<option value='171-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='171-01'){ echo 'selected="selected"';} ?> >171-01 Atención en ruta</option>
<option value='171-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='171-02'){ echo 'selected="selected"';} ?> >171-02 Animación turística</option>
<option value='171-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='171-03'){ echo 'selected="selected"';} ?> >171-03 Atención en congresos</option>
<option value='171-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='171-04'){ echo 'selected="selected"';} ?> >171-04 Información turística</option>
<option value='171-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='171-05'){ echo 'selected="selected"';} ?> >171-05 Desarrollo turístico</option>
<option value='171-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='171-06'){ echo 'selected="selected"';} ?> >171-06 Turismo rural</option>
<option value='171-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='171-99'){ echo 'selected="selected"';} ?> >171-99 Otros servicios turísticos especializados</option>
<option value='172-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='172-00'){ echo 'selected="selected"';} ?> >172-00 Tutorías y orientación en general</option>
<option value='173-00' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='173-00'){ echo 'selected="selected"';} ?> >173-00 Conocimiento y promoción de productos para farmacias en general</option>
<option value='174-01' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-01'){ echo 'selected="selected"';} ?> >174-01 Alemán</option>
<option value='174-02' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-02'){ echo 'selected="selected"';} ?> >174-02 Árabe</option>
<option value='174-03' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-03'){ echo 'selected="selected"';} ?> >174-03 Asturiano/bable</option>
<option value='174-04' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-04'){ echo 'selected="selected"';} ?> >174-04 Braille</option>
<option value='174-05' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-05'){ echo 'selected="selected"';} ?> >174-05 Castellano</option>
<option value='174-06' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-06'){ echo 'selected="selected"';} ?> >174-06 Catalán</option>
<option value='174-07' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-07'){ echo 'selected="selected"';} ?> >174-07 Chino</option>
<option value='174-08' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-08'){ echo 'selected="selected"';} ?> >174-08 Euskera</option>
<option value='174-09' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-09'){ echo 'selected="selected"';} ?> >174-09 Francés</option>
<option value='174-10' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-10'){ echo 'selected="selected"';} ?> >174-10 Gallego</option>
<option value='174-11' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-11'){ echo 'selected="selected"';} ?> >174-11 Griego</option>
<option value='174-12' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-12'){ echo 'selected="selected"';} ?> >174-12 Holandés</option>
<option value='174-13' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-13'){ echo 'selected="selected"';} ?> >174-13 Inglés</option>
<option value='174-14' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-14'){ echo 'selected="selected"';} ?> >174-14 Italiano</option>
<option value='174-15' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-15'){ echo 'selected="selected"';} ?> >174-15 Japonés</option>
<option value='174-16' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-16'){ echo 'selected="selected"';} ?> >174-16 Lenguaje de signos</option>
<option value='174-17' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-17'){ echo 'selected="selected"';} ?> >174-17 Portugués</option>
<option value='174-18' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-18'){ echo 'selected="selected"';} ?> >174-18 Ruso</option>
<option value='174-19' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-19'){ echo 'selected="selected"';} ?> >174-19 Sueco</option>
<option value='174-20' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-20'){ echo 'selected="selected"';} ?> >174-20 Valenciano</option>
<option value='174-99' <?php if(isset($datos['grupoAcciones']) && $datos['grupoAcciones']=='174-99'){ echo 'selected="selected"';} ?> >174-99 Otros Idiomas no clasificables anteriormente</option>