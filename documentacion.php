<?php
  $seccionActiva=21;
  include_once('cabecera.php');
  
  if(isset($_POST['anio'])){
	$_SESSION['anioSeleccionado']=$_POST['anio'];
  }
  $anio=$_SESSION['anioSeleccionado'];

  if(isset($_FILES['ficheroInterno'])){
    $res=insertaDatos('documentosInternos',$_POST['nombreDocumento'],'documentos/internos');
  }
  else if(isset($_FILES['ficheroExterno'])){
    $res=insertaDatos('documentosExternos',$_POST['nombreDocumento'],'documentos/externos');
  }
  else if(isset($_FILES['ficheroRegistro'])){
    $res=insertaDatos('documentosRegistros',$_POST['nombreDocumento'],'documentos/registros');
  }
  else if(isset($_FILES['ficheroDistribucion'])){
    $res=insertaDatos('documentosDistribucion',$_POST['nombreDocumento'],'documentos/distribucion');
  }
  elseif(isset($_GET['codigo']) && isset($_GET['eliminarInterno'])){
    $res=eliminaDocumento('documentosInternos',$_GET['codigo']);
  }
  elseif(isset($_GET['codigo']) && isset($_GET['eliminarExterno'])){
    $res=eliminaDocumento('documentosExternos',$_GET['codigo']);
  }
  elseif(isset($_GET['codigo']) && isset($_GET['eliminarRegistro'])){
    $res=eliminaDocumento('documentosRegistros',$_GET['codigo']);
  }
  elseif(isset($_GET['codigo']) && isset($_GET['eliminarDistribucion'])){
    $res=eliminaDocumento('documentosDistribucion',$_GET['codigo']);
  }
  
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?> 
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">


        <!-- /span6 -->
        <div class="span12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Documentos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">

              <?php
                if(isset($_FILES['ficheroInterno'])||isset($_FILES['ficheroExterno'])||isset($_FILES['ficheroRegistro'])||
                  isset($_FILES['ficheroDistribucion'])){
                  if($res){
                    mensajeOk("Documento registrado correctamente.");
                  }
                  else{
                    mensajeError("no se han podido registrar el Documento. Compruebe su tamaño.");
                  }
                }
                elseif(isset($_GET['codigo']) && (isset($_GET['eliminarInterno']) || isset($_GET['eliminarExterno']) || 
                  isset($_GET['eliminarRegistro']) || isset($_GET['eliminarDistribucion']))){
                  if($res){
                    mensajeOk("Documento eliminado correctamente.");
                  }
                  else{
                    mensajeError("no se han podido eliminar el Documento. Contacte con el webmaster.");
                  }
                }
              ?>

              <h6>A continuación se muestra la documentación subida al sistema:</h6><br>

                <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tabbable">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#pagina1" data-toggle="tab">Internos</a></li>
                  <li><a href="#pagina2" data-toggle="tab">Externos</a></li>
				  <?php if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='MARKETING'){ ?>
					<li><a href="#pagina3" data-toggle="tab">Empresas</a></li>
				  <?php } ?>
				  <li><a href="#pagina4" data-toggle="tab">Colaboradores</a></li>
                  <!--li><a href="#pagina4" data-toggle="tab">Distribución</a></li-->
                </ul>

                <br>
                
                <div class="tab-content">
                  <div class="tab-pane active" id="pagina1">
                    <fieldset>
                      <form id="formSubidaInterno" action="?" method="post" enctype="multipart/form-data">
                        <table class="table table-striped table-bordered datatable" id="internos">
                          <thead>
                            <tr>
                              <th> Código </th>
                              <th> Documento </th>
                              <th> Revisión </th>
                              <th> Fecha </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosInternos($anio);

                            ?>
                          
                          </tbody>
                        </table>
                        <br/>
						<?php if($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19' || $_SESSION['tipoUsuario']=='MARKETING'){ ?>
							<a href="javascript:void" class="btn btn-success" onclick="$('#ficheroInterno').click();"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 20 MB como máximo.
							<input type="file" class="hide" name="ficheroInterno" id="ficheroInterno" onchange="nuevaFilaInternos();"/>
						<?php } ?>
                      </form>
                    </fieldset>
                  </div>

                  <div class="tab-pane" id="pagina2">
                    <fieldset>
                      <form id="formSubidaExterno" action="?" method="post" enctype="multipart/form-data">
                        <table class="table table-striped table-bordered datatable" id="externos">
                          <thead>
                            <tr>
                              <th> Código </th>
                              <th> Documento </th>
                              <th> Ubicación </th>
							  <th> Fecha subida </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosExternos($anio);

                            ?>
                          
                          </tbody>
                        </table>
                        <br/>
						<?php if($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19' || $_SESSION['tipoUsuario']=='MARKETING'){ ?>
							<a href="javascript:void" class="btn btn-success" onclick="$('#ficheroExterno').click();"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 20 MB como máximo.
							<input type="file" class="hide" name="ficheroExterno" id="ficheroExterno" onchange="nuevaFilaExternos();"/>
						<?php } ?>
                      </form>
                    </fieldset>
                  </div>

                  <div class="tab-pane" id="pagina3">
                    <fieldset>
                      <form id="formSubidaRegistro" action="?" method="post" enctype="multipart/form-data">
                        <table class="table table-striped table-bordered datatable" id="registros">
                          <thead>
                            <tr>
                              <th> Código </th>
                              <th> Empresa </th>
                              <th> Responsable </th>
                              <th> Ubicación </th>
							  <th> Fecha subida </th>
                              <th> Tiempo Conservación </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosRegistros($anio);

                            ?>
                          
                          </tbody>
                        </table>
                        <br/>
						<?php if($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19' || $_SESSION['tipoUsuario']=='MARKETING'){ ?>
							<a href="javascript:void" class="btn btn-success" onclick="$('#ficheroRegistro').click();"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 20 MB como máximo.
							<input type="file" class="hide" name="ficheroRegistro" id="ficheroRegistro" onchange="nuevaFilaRegistros();"/>
						<?php } ?>
                      </form>
                    </fieldset>
                  </div>
          
                  <div class="tab-pane" id="pagina4">
                    <fieldset>
                      <form id="formSubidaDistribucion" action="?" method="post" enctype="multipart/form-data">
                        <table class="table table-striped table-bordered datatable" id="distribucion">
                          <thead>
                            <tr>
                              <th> Código </th>
                              <th> Colaborador </th>
                              <th> Responsable </th>
                              <th> Ubicación </th>
							  <th> Fecha subida </th>
                              <th> Tiempo Conservación </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosColaboradores($anio);

                            ?>
                          
                          </tbody>
                        </table>
                        <br/>
						<?php if($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19' || $_SESSION['tipoUsuario']=='MARKETING'){ ?>
							<a href="javascript:void" class="btn btn-success" onclick="$('#ficheroDistribucion').click();"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 20 MB como máximo.
							<input type="file" class="hide" name="ficheroDistribucion" id="ficheroDistribucion" onchange="nuevaFilaDistribucion();"/>
						<?php } ?>
                      </form>
                    </fieldset>
                  </div>
                </div>
              </div>

          </div>  

          <br/>
          <a href="generaExcelDocumentos.php" class="btn btn-inverse pull-right"><i class="icon-download"></i> Descargar Excel</a>            
            
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>


<div class="margenAb"></div>


<!-- /main -->
</div>
<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/jquery.form.min.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript"> 
  function nuevaFilaInternos(){
    $('#internos').find("tr:last").after("<tr><td><input type='text' name='codigoDocumento' class='input-mini' /><input type='hidden' name='papelera' value='NO' /></td><td><input type='text' name='nombreDocumento' class='input-large' /></td><td><input type='text' name='fechaRevision' class='input-small datepicker hasDatepicker' /></td><td><input type='text' name='fecha' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('#formSubidaInterno').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  function nuevaFilaExternos(){
    $('#externos').find("tr:last").after("<tr><td><input type='text' name='codigoDocumento' class='input-mini' /><input type='hidden' name='papelera' value='NO' /></td><td><input type='text' name='nombreDocumento' class='input-large' /></td><td><input type='text' name='ubicacion' class='input-large' /></td><td><input type='text' name='fechaSubida' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('#formSubidaExterno').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  function nuevaFilaRegistros(){
    $('#registros').find("tr:last").after("<tr><td><input type='text' name='codigoDocumento' class='input-mini' /><input type='hidden' name='papelera' value='NO' /></td><td><input type='text' name='nombreDocumento' class='input-small' /></td><td><input type='text' name='responsable' class='input-small' /></td><td><input type='text' name='ubicacion' class='input-small' /></td><td><input type='text' name='fechaSubida' class='input-small datepicker hasDatepicker' /></td><td><input type='text' name='tiempoConservacion' class='input-large' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('#formSubidaRegistro').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
  function nuevaFilaDistribucion(){
    $('#distribucion').find("tr:last").after("<tr><td><input type='text' name='codigoDocumento' class='input-mini' /><input type='hidden' name='papelera' value='NO' /></td><td><input type='text' name='nombreDocumento' class='input-small' /></td><td><input type='text' name='responsable' class='input-small' /></td><td><input type='text' name='ubicacion' class='input-small' /></td><td><input type='text' name='fechaSubida' class='input-small datepicker hasDatepicker' /></td><td><input type='text' name='tiempoConservacion' class='input-large' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('#formSubidaDistribucion').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
</script>
