<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos/plantillaColaboradores.xlsx");
	
	$i=0;
	$j=6; 
	
	$fecha=formateaFechaBD($_POST['fechaUno']);
	$fechaDos=formateaFechaBD($_POST['fechaDos']);
	
	$objPHPExcel->getActiveSheet()->getCell('B3')->setValue("Comisiones a pagar a colaboradores desde: ".formateaFechaWeb($fecha)." hasta: ".formateaFechaWeb($fechaDos));
	
	conexionBD();
	$consultaAux=consultaBD("SELECT colaboradores.codigo, clientes.codigo AS codigoCliente FROM colaboradores INNER JOIN clientes ON colaboradores.codigo=clientes.comercial WHERE clientes.comercial IS NOT NULL;");
		$datosColaboradores=mysql_fetch_assoc($consultaAux);
		while(isset($datosColaboradores['codigo'])){
			$consulta=consultaBD("SELECT SUM( precio*(colaboradores.comision/100) ) AS total , colaboradores.empresa, clientes.empresa AS nombreCliente, colaboradores.numCuenta FROM ventas 
				INNER JOIN clientes ON ventas.codigoCliente = clientes.codigo
				INNER JOIN colaboradores ON clientes.comercial = colaboradores.codigo
				WHERE fecha>='$fecha' AND fecha<='$fechaDos' AND clientes.comercial='".$datosColaboradores['codigo']."' AND clientes.codigo='".$datosColaboradores['codigoCliente']."'");
				$datos=mysql_fetch_assoc($consulta);
				
			while(isset($datos['empresa'])){
				if($datos['total']!=NULL){
					$objPHPExcel->getActiveSheet()->getCell('B'.$j)->setValue($datos['nombreCliente']);
					$objPHPExcel->getActiveSheet()->getCell('C'.$j)->setValue($datos['empresa']);
					$objPHPExcel->getActiveSheet()->getCell('D'.$j)->setValue($datos['total']);
					$objPHPExcel->getActiveSheet()->getCell('E'.$j)->setValue($datos['numCuenta']);
				}else{
					$i--;
					$j--;
				}
				$i++;
				$j++;
				$datos=mysql_fetch_assoc($consulta);
			}
			$datosColaboradores=mysql_fetch_assoc($consultaAux);
		}
	cierraBD();

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$tiempo=time();
	$objWriter->save('documentos/pagoColaboradores.xlsx');


	// Definir headers
	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=pagoColaboradores.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/pagoColaboradores.xlsx');
?>