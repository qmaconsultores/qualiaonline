<?php
  $seccionActiva=5;
  include_once('cabecera.php');

  $datos=datosCurso($_GET['codigo']);
  $codigosAlumnos=datosAlumnosCurso($_GET['codigo']);
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Detalles</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="formacion.php" method="post">
                  <fieldset>

                    <input type="hidden" name="codigo" value="<?php echo $datos['codigo']; ?>">

                    <div class="control-group">                     
                      <label class="control-label" for="accionFormativa">Acción formativa:</label>
                      <div class="controls">
                       <input type="text" class="span5" id="accionFormativa" name="accionFormativa" disabled="disabled" value="<?php echo $datos['denominacion']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('plataforma','Plataforma',$datos);
						campoOculto($datos['codigoaccionFormativa'],'accionFormativa');
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="codigoInterno">Código curso:</label>
                      <div class="controls numSS">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini" id="codigoAccionFormativa" name="codigoAccionFormativa" disabled="disabled" value="<?php echo $datos['codigoAccionFormativa']; ?>">
                          <span class="add-on">/</span>
                          <input type="text" class="input-mini" id="codigoInterno" name="codigoInterno" value="<?php echo $datos['codigoInterno']; ?>">
						  <input type="hidden" id="tipoFormacion" name="tipoFormacion">
                        </div>
                      </div> <!-- /controls -->         
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="horas">Número de horas:</label>
                      <div class="controls">
                       <input type="text" class="input-mini pagination-right" id="horas" name="horas" disabled="disabled" value="<?php echo $datos['horas']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="modalidad">Modalidad:</label>
                      <div class="controls">
                       <input type="text" class="input-medium" id="modalidad" name="modalidad" disabled="disabled" value="<?php echo $datos['modalidad']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    
					<?php 
						$consulta="SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM tutores;";
						campoSelectConsulta('codigoTutor','Tutor',$consulta,$datos);
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="responsable">Responsable:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="responsable" name="responsable" value="<?php echo $datos['responsable']; ?>">
						<input type="hidden" name="listadoAlumnos" id="listadoAlumnos">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					
					<div class="control-group">                     
                      <label class="control-label" for="tlfResponsable">Tlf. Responsable:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="tlfResponsable" name="tlfResponsable" value="<?php echo $datos['tlfResponsable']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fechaInicio">Fecha de inicio:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaInicio" name="fechaInicio" value="<?php echo formateaFechaWeb($datos['fechaInicio']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="fechaFin">Fecha de finalización:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaFin" name="fechaFin" value="<?php echo formateaFechaWeb($datos['fechaFin']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <?php
                      $opciones=array('NO', 'SI');
                      campoSelect('llamadaBienvenida', 'LLamada de Bienvenida', $opciones, $opciones, $datos, 'selectpicker show-tick anchoAuto');
                      campoSelect('llamadaSeguimiento', 'LLamada de Seguimiento', $opciones, $opciones, $datos, 'selectpicker show-tick anchoAuto');
                      campoSelect('llamadaFinalizacion', 'LLamada de Finalización', $opciones, $opciones, $datos, 'selectpicker show-tick anchoAuto');
                      campoSelect('bonificado', '¿Bonificado?', $opciones, $opciones, $datos, 'selectpicker show-tick anchoAuto');
					  campoSelect('finalizado', '¿Finalizado?', $opciones, $opciones, $datos, 'selectpicker show-tick anchoAuto');
                      $consultaUser="SELECT codigo, CONCAT(apellidos, ', ', nombre) AS texto FROM usuarios WHERE activoUsuario='SI';";
                      campoSelectConsulta('comercial', 'Comercial', $consultaUser, $datos);
                    ?>
					
					<div class="control-group">                     
					  <label class="control-label" for="formacionCurso">Formación:</label>
					  <div class="controls">
						
						<select name="formacionCurso" id="formacionCurso" class='selectpicker show-tick'>
						  <option value="NO" data-content="<span class='label'>No tiene</span>" <?php if($datos['formacionCurso']=='NO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>" <?php if($datos['formacionCurso']=='HECHO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>" <?php if($datos['formacionCurso']=='PROCESO'){ echo 'selected="selected"'; } ?>></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>" <?php if($datos['formacionCurso']=='FINALIZADO'){ echo 'selected="selected"'; } ?>></option>
						</select>

					  </div> <!-- /controls -->       
					</div> <!-- /control-group -->

					<div class="control-group">                     
                      <label class="control-label" for="email">Alumnos ya inscritos:</label>
                      <div class="controls conBorde">
                       
                        <table class="table table-striped table-bordered datatable">
                          <thead>
                            <tr>
                              <th> Nombre </th>
                              <th> DNI </th>
                              <th> eMail </th>
                              <th> Teléfono </th>
							  <th> Empresa </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              imprimeAlumnosDetallesCurso($datos['codigo']);
                            ?>
                          
                          </tbody>
                        </table>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="email">Alumnos:</label>
                      <div class="controls conBorde">
                       
                        <table class="table table-striped table-bordered datatable">
                          <thead>
                            <tr>
                              <th> Nombre </th>
                              <th> DNI </th>
                              <th> eMail </th>
                              <th> Teléfono </th>
							                <th> Empresa </th>
                              <th><input type='checkbox' id="todo"></th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              imprimeAlumnosCreaCurso($codigosAlumnos);
                            ?>
                          
                          </tbody>
                        </table>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					           <div class="control-group">                     
                      <label class="control-label" for="mediosPropios">Medios para la formación:</label>
                      <div class="controls">
                        <label class="checkbox inline">
                          <input type="checkbox" name="mediosPropios" id="mediosPropios" value="SI" <?php if($datos['mediosPropios']=='SI'){ echo 'checked="checked"';} ?> > Propios de la empresa bonificada
                        </label>
                        <br>
                        <label class="checkbox inline">
                          <input type="checkbox" name="mediosEntidad" id="mediosEntidad" value="SI" <?php if($datos['mediosEntidad']=='SI'){ echo 'checked="checked"';} ?> > De la entidad organizadora
                        </label>
                        <br>
                        <label class="checkbox inline">
                          <input type="checkbox" name="mediosCentro" id="mediosCentro" value="SI" <?php if($datos['mediosCentro']=='SI'){ echo 'checked="checked"';} ?> > Del centro de formación o entidad formadora externa
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoSelect('medios','Medios',array('Entidad inscrita','Entidad externa'),array('EntidadInscrita','EntidadOrganizadora'),$datos);
					?>

          					<h3 class="apartadoFormulario">Descripción</h3>
          					
          					<h4 class="apartadoFormulario" id="textoCambiarUno">Tutoría</h4>
							
							<div class="control-group">                     
                      <label class="control-label" for="cifTutoria">CIF:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cifTutoria" name="cifTutoria" value="<?php echo $datos['cifTutoria']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="centroTutoria">Centro:</label>
                      <div class="controls">
                        <input type="text" class="span4" id="centroTutoria" name="centroTutoria" value="<?php echo $datos['centroTutoria']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="tlfTutoria">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="tlfTutoria" name="tlfTutoria" value="<?php echo $datos['tlfTutoria']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="domicilioTutoria">Domicilio:</label>
                      <div class="controls">
                        <input type="text" class="span4" id="domicilioTutoria" name="domicilioTutoria" value="<?php echo $datos['domicilioTutoria']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="cpTutoria">CP:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cpTutoria" name="cpTutoria" value="<?php echo $datos['cpTutoria']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="poblacionTutoria">Población:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="poblacionTutoria" name="poblacionTutoria" value="<?php echo $datos['poblacionTutoria']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
          					
          					<div class="control-group">                     
                      <label class="control-label" for="horario">Horario mañana:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horaInicio" name="horaInicio" value="<?php echo $datos['horaInicio']; ?>">
            						 a 
            						<input type="text" class="input-small" id="horaFin" name="horaFin" value="<?php echo $datos['horaFin']; ?>">
            						 (hh:mm) Comprendido entre 00:01h. y 15:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				          	<div class="control-group">                     
                      <label class="control-label" for="horario">Horario tarde:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horaInicioTarde" name="horaInicioTarde" value="<?php echo $datos['horaInicioTarde']; ?>">
            						 a 
            						<input type="text" class="input-small" id="horaFinTarde" name="horaFinTarde" value="<?php echo $datos['horaFinTarde']; ?>">
            						 (hh:mm) Comprendido entre 15:01h. y 00:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					          <div class="control-group">                     
                      <label class="control-label" for="horasTutoria">Horas tutorías:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horasTutoria" name="horasTutoria" value="<?php echo $datos['horasTutoria']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					          <div class="control-group">                     
                      <label class="control-label" for="horas">Días impartición:</label>
                      <div class="controls">
                        <table class="mitadAncho">
                          <tr>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                          </tr>
                          <tr>
                            <th><input type="checkbox" id="lunes" name="lunes" <?php if($datos['lunes']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="martes" name="martes" <?php if($datos['martes']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="miercoles" name="miercoles" <?php if($datos['miercoles']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="jueves" name="jueves" <?php if($datos['jueves']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="viernes" name="viernes" <?php if($datos['viernes']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="sabado" name="sabado" <?php if($datos['sabado']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="domingo" name="domingo" <?php if($datos['domingo']=='SI'){ echo "checked='checked'";} ?>></th>
                          </tr>
                        </table>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				<div id="centroPresencial">
					
					<h4 class="apartadoFormulario" id="textoCambiarDos">Centro gestor de la formación</h4>
					
					<div class="control-group">                     
                      <label class="control-label" for="cif">CIF:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cif" name="cif" value="<?php echo $datos['cif']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="centro">Centro:</label>
                      <div class="controls">
                        <input type="text" class="span4" id="centro" name="centro" value="<?php echo $datos['centro']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="tlf">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="tlf" name="tlf" value="<?php echo $datos['tlf']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="domicilio">Domicilio:</label>
                      <div class="controls">
                        <input type="text" class="span4" id="domicilio" name="domicilio" value="<?php echo $datos['domicilio']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="cp">CP:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cp" name="cp" value="<?php echo $datos['cp']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="poblacion">Población:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="poblacion" name="poblacion" value="<?php echo $datos['poblacion']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="titularidad">Titularidad:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="titularidad" value="publico" <?php if($datos['titularidad']=='publico'){ echo "checked='checked'";} ?>> Público
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="titularidad" value="privado" <?php if($datos['titularidad']=='privado'){ echo "checked='checked'";} ?>> Privado
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="horario">Horario mañana:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horaInicioFormacion" name="horaInicioFormacion" value="<?php echo $datos['horaInicioFormacion']; ?>">
            						 a 
            						<input type="text" class="input-small" id="horaFinFormacion" name="horaFinFormacion" value="<?php echo $datos['horaFinFormacion']; ?>">
            						 (hh:mm) Comprendido entre 00:01h. y 15:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
				  	
				    <div class="control-group">                     
                      <label class="control-label" for="horario">Horario tarde:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horaInicioFormacionTarde" name="horaInicioFormacionTarde" value="<?php echo $datos['horaInicioFormacionTarde']; ?>">
            						 a 
            						<input type="text" class="input-small" id="horaFinFormacionTarde" name="horaFinFormacionTarde" value="<?php echo $datos['horaFinFormacionTarde']; ?>">
            						 (hh:mm) Comprendido entre 15:01h. y 00:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="horasTutoria">Horas formación:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horasFormacion" name="horasFormacion" value="<?php echo $datos['horasFormacion']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				            <div class="control-group">                     
                      <label class="control-label" for="horas">Días impartición:</label>
                      <div class="controls">
                        <table class="mitadAncho">
                          <tr>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                          </tr>
                          <tr>
                            <th><input type="checkbox" id="lunesFormacion" name="lunesFormacion" <?php if($datos['lunesFormacion']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="martesFormacion" name="martesFormacion" <?php if($datos['martesFormacion']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="miercolesFormacion" name="miercolesFormacion" <?php if($datos['miercolesFormacion']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="juevesFormacion" name="juevesFormacion" <?php if($datos['juevesFormacion']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="viernesFormacion" name="viernesFormacion" <?php if($datos['viernesFormacion']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="sabadoFormacion" name="sabadoFormacion" <?php if($datos['sabadoFormacion']=='SI'){ echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="domingoFormacion" name="domingoFormacion" <?php if($datos['domingoFormacion']=='SI'){ echo "checked='checked'";} ?>></th>
                          </tr>
                        </table>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				</div>
					
					<div id="centroDistancia">
						<h4 class="apartadoFormulario" id="textoCambiarDos">Centro gestor de la formación a distancia y horarios</h4>
						
						<div class="control-group">                     
						  <label class="control-label" for="tutor">Tutor:</label>
						  <div class="controls">
							<?php selectTutor($datos['tutorDistancia'], "tutorDistancia"); ?>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="cifDistancia">CIF:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="cifDistancia" name="cifDistancia" value="<?php echo $datos['cifDistancia']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="centroGestorDistancia">Centro:</label>
						  <div class="controls">
							<input type="text" class="span4" id="centroGestorDistancia" name="centroGestorDistancia" value="<?php echo $datos['centroGestorDistancia']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="tlfDistancia">Teléfono:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="tlfDistancia" name="tlfDistancia" value="<?php echo $datos['tlfDistancia']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="domicilioDistancia">Domicilio:</label>
						  <div class="controls">
							<input type="text" class="span4" id="domicilioDistancia" name="domicilioDistancia" value="<?php echo $datos['domicilioDistancia']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="cpDistancia">CP:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="cpDistancia" name="cpDistancia" value="<?php echo $datos['cpDistancia']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="poblacionDistancia">Población:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="poblacionDistancia" name="poblacionDistancia" value="<?php echo $datos['poblacionDistancia']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
						  <label class="control-label" for="titularidadDistancia">Titularidad:</label>
						  <div class="controls">
							<label class="radio inline">
							  <input type="radio" name="titularidadDistancia" value="publico" <?php if($datos['titularidadDistancia']=="publico"){echo "checked='checked'";} ?>> Público
							</label>
							
							<label class="radio inline">
							  <input type="radio" name="titularidadDistancia" value="privado" <?php if($datos['titularidadDistancia']=="privado"){echo "checked='checked'";} ?>> Privado
							</label>
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->
						
						<div class="control-group">                     
                      <label class="control-label" for="horario">Horario mañana:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horaInicioFormacionDistancia" name="horaInicioFormacionDistancia" value="<?php echo $datos['horaInicioFormacionDistancia']; ?>">
            						 a 
            						<input type="text" class="input-small" id="horaFinFormacionDistancia" name="horaFinFormacionDistancia" value="<?php echo $datos['horaFinFormacionDistancia']; ?>">
            						 (hh:mm) Comprendido entre 00:01h. y 15:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
				  	
				    <div class="control-group">                     
                      <label class="control-label" for="horario">Horario tarde:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horaInicioFormacionTardeDistancia" name="horaInicioFormacionTardeDistancia" value="<?php echo $datos['horaInicioFormacionTardeDistancia']; ?>">
            						 a 
            						<input type="text" class="input-small" id="horaFinFormacionTardeDistancia" name="horaFinFormacionTardeDistancia" value="<?php echo $datos['horaFinFormacionTardeDistancia']; ?>">
            						 (hh:mm) Comprendido entre 15:01h. y 00:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="horasTutoria">Horas formación:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horasFormacionDistancia" name="horasFormacionDistancia" value="<?php echo $datos['horasFormacionDistancia']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				            <div class="control-group">                     
                      <label class="control-label" for="horas">Días impartición:</label>
                      <div class="controls">
                        <table class="mitadAncho">
                          <tr>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                          </tr>
                          <tr>
                            <th><input type="checkbox" id="lunesFormacionDistancia" name="lunesFormacionDistancia" <?php if($datos['lunesFormacionDistancia']=="SI"){echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="martesFormacionDistancia" name="martesFormacionDistancia" <?php if($datos['martesFormacionDistancia']=="SI"){echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="miercolesFormacionDistancia" name="miercolesFormacionDistancia" <?php if($datos['miercolesFormacionDistancia']=="SI"){echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="juevesFormacionDistancia" name="juevesFormacionDistancia" <?php if($datos['juevesFormacionDistancia']=="SI"){echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="viernesFormacionDistancia" name="viernesFormacionDistancia" <?php if($datos['viernesFormacionDistancia']=="SI"){echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="sabadoFormacionDistancia" name="sabadoFormacionDistancia" <?php if($datos['sabadoFormacionDistancia']=="SI"){echo "checked='checked'";} ?>></th>
                            <th><input type="checkbox" id="domingoFormacionDistancia" name="domingoFormacionDistancia" <?php if($datos['domingoFormacionDistancia']=="SI"){echo "checked='checked'";} ?>></th>
                          </tr>
                        </table>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				</div>
					
					<h4 class="apartadoFormulario" id="textoCambiarTres">Representación Legal del Trabajador</h4>
					
					<div class="control-group">                     
                      <label class="control-label" for="informarlt">¿Se ha informado a la RLT?:</label>
                      <div class="controls">
                        <select id="informarlt" name="informarlt" class='selectpicker show-tick anchoAuto'>
							<option value="SI" <?php if($datos['informarlt']=='SI'){ echo "selected='selected'"; } ?>>SI
							<option value="NO" <?php if($datos['informarlt']=='NO'){ echo "selected='selected'"; } ?>>No
						</select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="informerlt">Informe RLT:</label>
                      <div class="controls">
                        <select id="informerlt" name="informerlt" class='selectpicker show-tick anchoAuto'>
							<option value="NOINF" <?php if($datos['informerlt']=='NOINF'){ echo "selected='selected'"; } ?>>No informa
							<option value="FAV" <?php if($datos['informerlt']=='FAV'){ echo "selected='selected'"; } ?>>Favorable
							<option value="DIS" <?php if($datos['informerlt']=='DIS'){ echo "selected='selected'"; } ?>>Discrepancias
						</select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group" id="divFecha">                     
                      <label class="control-label" for="fechaDiscrepancia">Fecha discrepancia:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaDiscrepancia" name="fechaDiscrepancia" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="resuelto">Resuelto 15 días:</label>
                      <div class="controls">
                        <select id="resuelto" name="resuelto" class='selectpicker show-tick anchoAuto'>
							<option value="SI" <?php if($datos['resuelto']=='SI'){ echo "selected='selected'"; } ?>>SI
							<option value="NO" <?php if($datos['resuelto']=='NO'){ echo "selected='selected'"; } ?>>No
						</select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar curso</button> 
                      <a href="formacion.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	<?php if ($datos['modalidad']=='PRESENCIAL'){
		echo "
			$('#centroPresencial').css('display','block')
			$('#centroDistancia').css('display','none');
			$('#tipoFormacion').val('PRESENCIAL');
		";
	}elseif($datos['modalidad']=='DISTANCIA'){
		echo "
			$('#centroPresencial').css('display','none');
			$('#centroDistancia').css('display','block');
			$('#tipoFormacion').val('DISTANCIA');
		";
	}elseif($datos['modalidad']=='MIXTA'){
		echo "
			$('#centroPresencial').css('display','block');
			$('#centroDistancia').css('display','block');
			$('#tipoFormacion').val('MIXTA');
		";
	}elseif($datos['modalidad']=='TELE'){
		echo "
			$('#centroPresencial').css('display','block');
			$('#centroDistancia').css('display','none');
			$('#tipoFormacion').val('TELEFORMA');
		";
	}elseif($datos['modalidad']=='WEBINAR'){
    echo "
      $('#centroPresencial').css('display','block');
      $('#centroDistancia').css('display','none');
      $('#tipoFormacion').val('WEBINAR');
    ";
  }
	if($datos['informerlt']!='DIS'){ 
			echo "$('#divFecha').css('display','none');";
		}
	?>
	$('#informerlt').change(function(){
       if($(this).val()=='DIS'){
		$('#divFecha').css('display','block');
	  }else{
		$('#divFecha').css('display','none');
	  }

    });
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	
	//PARTE NUEVA SOLUCIÓN DE CHECKS
	  
		var array=new Array();   //Creo un array que me servirá para transmitir los alumnos que vaya seleccionando
		<?php 
			$consulta=consultaBD("SELECT codigoAlumno FROM alumnos_registrados_cursos WHERE codigoCurso='".$datos['codigo']."';",true);
			$datosAlumnos=mysql_fetch_assoc($consulta);
			$alumnos='';
			while(isset($datosAlumnos['codigoAlumno'])){
				echo "array.push(".$datosAlumnos['codigoAlumno'].");";
				$datosAlumnos=mysql_fetch_assoc($consulta);
			}
		?>
		
		$('#listadoAlumnos').val(array);			//Asigno al input creado auxiliar, el valor del array que recorreré en funciones.php
		cargaOyente(array);
		
		$('.dataTables_paginate').click(function(){
			$("td input:checkbox").unbind("change");
			cargaOyente(array);
		});
		
		$('.dataTables_filter').keypress(function(){
			$("td input:checkbox").unbind("change");
			cargaOyente(array);
		});
	
  });
  
  function cargaOyente(array){		
	$('td input:checkbox').change(function(){ 		//Cada vez que de a un check se ejecuta
		var codigo=$(this).val();					//Cojo el valor del check
		var salir=false;
		jQuery.each( array, function( i, val ) {
			if(val==codigo){						//Compruebo que no esté ya insertado en el array
				array.splice( i, 1 );
				salir=true;
			}
		});
		if(salir!=true){
			array.push(codigo);						//Si no está lo añado
		}
		$('#listadoAlumnos').val(array);			//Asigno al input creado auxiliar, el valor del array que recorreré en funciones.php
	});
  }
  
  //FIN PARTE NUEVA SOLUCIÓN DE CHECKS
</script>