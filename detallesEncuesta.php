<?php
  $seccionActiva=15;
  include_once("cabecera.php");
  
  $codigo=$_GET['codigo'];
  
  $datos=datosEncuesta($codigo);
  $datosCliente=datosRegistro('clientes',$datos['cliente']);
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Encuenta de Satisfacción de Clientes</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="satisfaccion.php" method="post">
                  <fieldset>
				  
					           <div class="control-group" id='cliente'>                     
                      <label class="control-label" for="cliente">Cliente:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="cliente" name="cliente" value="<?php echo $datosCliente['empresa'] ?>" disabled="disabled">
						            <input type="hidden" name="codigo" value="<?php echo $codigo ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('email','Email de cliente',$datosCliente['mail'],'input-large',true);
					?>
					
                    <div class="control-group">                     
                      <label class="control-label" for="fecha">Fecha:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fecha" name="fecha" value="<?php echo formateaFechaWeb($datos['fecha']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group --> <br />
					
					        <center>
          					<?php 
          						creaTablaSatisfaccion(); 
          						creaPreguntaTablaEncuesta("Atención comercial:",1,$datos['pregunta1']);
          						creaPreguntaTablaEncuesta("La atención del Formador:",2,$datos['pregunta2']);
          						creaPreguntaTablaEncuesta("Profesionalidad del Formador:",3,$datos['pregunta3']);
								cierraTablaSatisfaccion();
          					?>
			      </center>
				  
				  <br><br>
				  
					<strong>Valore su satisfaccion de los siguientes apartados:</strong><br><br>
					
					De los servicios contratados<br><br>

                  <center>
          					<?php 
          						creaTablaSatisfaccion(); 
          						creaPreguntaTablaEncuesta("Información y atención antes de ser cliente:",4,$datos['pregunta4']);
          						creaPreguntaTablaEncuesta("Información sobre el servicio contratado:",5,$datos['pregunta5']);
          						creaPreguntaTablaEncuesta("Información sobre el procedimiento de cobro:",6,$datos['pregunta6']);
								creaPreguntaTablaEncuesta("Tiempo de entrega del servicio contratado:",7,$datos['pregunta7']);
								creaPreguntaTablaEncuesta("Implantación del servicio en su empresa:",8,$datos['pregunta8']);
								creaPreguntaTablaEncuesta("Resolución de incidencias desde que es cliente:",9,$datos['pregunta9']);
								cierraTablaSatisfaccion();
          					?>
			      </center>
				  
				  <br>
				    De los cursos de formación realizados<br><br>

                  <center>
          					<?php 
          						creaTablaSatisfaccion(); 
          						creaPreguntaTablaEncuesta("Ha obtenido la formación que usted esperaba:",10,$datos['pregunta10']);
          						creaPreguntaTablaEncuesta("Funcionalidad de la plataforma de formación:",11,$datos['pregunta11']);
          						creaPreguntaTablaEncuesta("Pedagogía y contenidos del curso:",12,$datos['pregunta12']);
								creaPreguntaTablaEncuesta("Tiempo asignado para su realización:",13,$datos['pregunta13']);
								creaPreguntaTablaEncuesta("Tiempo de espera del diploma y del informe de bonificación:",14,$datos['pregunta14']);
          					?>
          						<td colspan="2">A continuación, incluya cuantos comentarios o sugerencias estime oportunos:</td>	
          						<tr>
          							<td colspan="2"><textarea rows="10" class="textarea-amplia" name="comentarios" id="comentarios"><?php echo $datos['comentarios']; ?></textarea></td>	
          						</tr>
          					<?php 
          						cierraTablaSatisfaccion();
          					?>
                  </center>
					

                     <br />
                    
                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar encuesta</button> 
                      <a href="satisfaccion.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>


<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('#fecha').datepicker({format:'dd/mm/yyyy',weekStart:1});
  });

</script>


