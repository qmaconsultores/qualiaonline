<?php
  $seccionActiva=23;
  include_once("cabecera.php");

  if(isset($_POST['codigoPreventa'])){
    $res=actualizaTarea();
    $datos=datosRegistro('preventas',$_POST['codigoPreventa']);
  } else {
    $datos=datosRegistro('preventas',$_GET['codigo']);
  }
  $tarea=datosRegistro('tareas',$datos['codigoTarea']);
  $cliente=datosRegistro('clientes',$tarea['codigoCliente']);
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  

  
  if($datos['verificada']=='SI'){
    $edicion=false;
  } else {
    if($_SESSION['codigoS']=='29' || $_SESSION['codigoS']=='138' || $_SESSION['codigoS']=='251' || $_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='COMERCIAL' || isset($_POST['observaciones'])){
      $edicion=true;
    } else {
      $edicion=false;
    }
  }

  
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Modificar Preventa</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="preventas.php" method="post">
                  <fieldset>

                    <?php
                      if(isset($_GET['desbloquear'])){
                        $edicion=true;
                        campoOculto('SI','desbloqueado');
                      }
                      if(isset($_GET['desbloquear']) == false && $edicion == false && ($_SESSION['codigoS']==11 || $_SESSION['codigoS']==29 || $_SESSION['codigoS']==138 || $_SESSION['codigoS']==120 || $_SESSION['codigoS']==251)){
                        echo '<a style="margin-left: 10px;" href="detallesPreventa.php?codigo='.$_GET['codigo'].'&desbloquear" class="btn btn-primary" id="bloqueoReferencia"><i class="icon-unlock"></i></a>';
                      } 
                      abreColumnaCampos('span5');
                        if(isset($_POST['codigoPreventa'])){
                          campoOculto('SINEVALUAR','aceptado');
                        }
                        campoOculto($datos['codigo'],'codigo');
                        //selectClientes($datos['codigoCliente']);
                        if($edicion){
                          campoSelectClientePreventa($datos['codigoCliente'],$cliente['empresa']);
                        } else {
                          campoDato('Cliente',$cliente['empresa']);
                        }
                        campoOculto($datos['codigoTarea'],'codigoTarea');

                      cierraColumnaCampos();
                      abreColumnaCampos('span5');
                      $class='';
                        if(!$edicion){
                            campoDato('Fecha de preventa',formateaFechaWeb($datos['fecha']));
                        } else {
                          campoFecha('fecha','Fecha de preventa',$datos);
                        }
                        if($edicion ){
                          if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['codigoS']=='29' || $_SESSION['codigoS']=='138' || $_SESSION['codigoS']==120 || $_SESSION['codigoS']==251){
                            campoRadio('documentos','¿Dispone de documentos?',$datos);
                          }
                        } else {
                          campoDato('¿Dispone de documentos?',$datos['documentos']);
                        }
                        
                        if($datos['documentos']=='NO'){
                          $class='hide';  
                        }
            
                        
                        echo "<div id='divVerificada' class='".$class."'>";
                        //if($edicion || $_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['codigoS']==29){
                          if($edicion){
                            if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['codigoS']=='29' || $_SESSION['codigoS']=='138' || $_SESSION['codigoS']==120 || $_SESSION['codigoS']==251){
                              campoRadio('verificada','¿Verificada?',$datos);
                            }
                          } else {
                            campoDato('¿Verificada?',$datos['verificada']);
                          }
                        echo "</div>";
                      cierraColumnaCampos();
                      echo '<br clear="all"><h2 class="apartadoFormulario">SERVICIOS</h2>';
                      if($edicion){
                      abreColumnaCampos('span3 servicioPreventa');
                        campoSelect('formacion','Formación',array('No','Si'),array('NO','SI'),$datos['formacion'],'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="formacion hide">';
                          campoTextoSimbolo('formacionPrecio','Precio','€',number_format((float)$datos['formacionPrecio'], 2, ',', ''));
                        echo '</div>';

                        campoSelect('prl','PRL',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="prl hide">';
                          campoTextoSimbolo('prlPrecio','Precio','€',number_format((float)$datos['prlPrecio'], 2, ',', ''));
                        echo '</div>';

                        campoSelect('consultoriaLopd','Consultoría LOPD',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="consultoriaLopd hide">';
                          campoTextoSimbolo('consultoriaLopdPrecio','Precio','€',number_format((float)$datos['consultoriaLopdPrecio'], 2, ',', ''));
                        echo '</div>';

                        campoSelect('dominioCorreo','Dominio y correo electrónico',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="dominioCorreo hide">';
                          campoTextoSimbolo('dominioCorreoPrecio','Precio','€',number_format((float)$datos['dominioCorreoPrecio'], 2, ',', ''));
                        echo '</div>';
                      cierraColumnaCampos();

                      abreColumnaCampos('span3 servicioPreventa');
                        campoSelect('lssi','LSSI',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="lssi hide">';
                          campoTextoSimbolo('lssiPrecio','Precio','€',number_format((float)$datos['lssiPrecio'], 2, ',', ''));
                        echo '</div>';

                        campoSelect('plataformaWeb','Plataforma Web',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="plataformaWeb hide">';
                          campoTextoSimbolo('plataformaWebPrecio','Precio','€',number_format((float)$datos['plataformaWebPrecio'], 2, ',', ''));
                        echo '</div>';

                        campoSelect('auditoriaPrl','Auditoría PRL',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="auditoriaPrl hide">';
                          campoTextoSimbolo('auditoriaPrlPrecio','Precio','€',number_format((float)$datos['auditoriaPrlPrecio'], 2, ',', ''));
                        echo '</div>';

                        campoSelect('dominio','Dominio',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="dominio hide">';
                          campoTextoSimbolo('dominioPrecio','Precio','€',number_format((float)$datos['dominioPrecio'], 2, ',', ''));
                        echo '</div>';
                      cierraColumnaCampos();

                      abreColumnaCampos('span3 servicioPreventa');
                        campoSelect('alergenos','APPCC-Alérgenos',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="alergenos hide">';
                          campoTextoSimbolo('alergenosPrecio','Precio','€',number_format((float)$datos['alergenosPrecio'], 2, ',', ''));
                        echo '</div>';

                        campoSelect('auditoriaLopd','Auditoría LOPD',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="auditoriaLopd hide">';
                          campoTextoSimbolo('auditoriaLopdPrecio','Precio','€',number_format((float)$datos['auditoriaLopdPrecio'], 2, ',', ''));
                        echo '</div>';

                        campoSelect('webLegalizada','Web legalizada',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="webLegalizada hide">';
                          campoTextoSimbolo('webLegalizadaPrecio','Precio','€',number_format((float)$datos['webLegalizadaPrecio'], 2, ',', ''));
                        echo '</div>';

                        campoSelect('ecommerce','E-commerce',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick selectPrecio');
                        echo '<div class="ecommerce hide">';
                          campoTextoSimbolo('ecommercePrecio','Precio','€',number_format((float)$datos['ecommercePrecio'], 2, ',', ''));
                        echo '</div>';
                      cierraColumnaCampos();
                      } else {
                        $servicios=array('formacion','lssi','alergenos','prl','plataformaWeb','auditoriaLopd','consultoriaLopd','auditoriaPrl','webLegalizada','dominioCorreo','dominio','ecommerce');
                        $nombres=array('formacion'=>'Formación','lssi'=>'LSSI','alergenos'=>'APPCC-Alérgenos','prl'=>'PRL','plataformaWeb'=>'Plataforma Web','auditoriaLopd'=>'Auditoría LOPD','consultoriaLopd'=>'Consultoría LOPD','auditoriaPrl'=>'Auditoría PRL','webLegalizada'=>'Web legalizada','dominioCorreo'=>'Dominio y correo electrónico','dominio'=>'Dominio','ecommerce'=>'E-commerce');
                        echo '<table style="width:50%">';
                        $total=0;
                        foreach ($servicios as $servicio) {
                          if($datos[$servicio]=='SI'){

                            echo '<tr><td style="border-bottom:1px solid #000">'.$nombres[$servicio].'</td><td style="border-bottom:1px solid #000">'.number_format((float)$datos[$servicio.'Precio'], 2, ',', '').' €</td></tr>';
                            $total=$total+$datos[$servicio.'Precio'];
                          }
                        }
                        echo '<tr><td style="border-bottom:1px solid #000">Total</td><td style="border-bottom:1px solid #000">'.number_format((float)$total, 2, ',', '').' €</td></tr></table>';

                      }
                      echo '<br clear="all"><h2 class="apartadoFormulario">OBSERVACIONES</h2>';
                      if($edicion){
                        areaTexto('observaciones','Observaciones o mótivos de rechazo',$datos,'areaInforme');
                      } else {
                        campoDato('Observaciones o mótivo de rechazo',$datos['observaciones']);
                      }
                    ?>

                  </fieldset>
                  <div class="form-actions">
                    <?php /*if($edicion || $_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['codigoS']==29 || isset($_POST['observaciones'])){*/?>
                    <?php if($edicion){?>
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Actualiza preventa</button> 
                    <?php } ?>
                      <a href="preventas.php" class="btn"><i class="icon-remove"></i> Volver</a>
                    </div> <!-- /form-actions -->
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
<?php include_once('pie.php'); ?>
<!--script type="text/javascript" src="js/bootstrap-slider.js"></script-->
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectPrecio').change(function(){
      var id = $(this).attr('id');
      var val = $(this).val();
      if(val == 'SI'){
        $('.'+id).removeClass('hide');
      } else {
        $('.'+id).addClass('hide');
      }
    });
    $('input[name=documentos]').change(function(){
      var val = $(this).val();
      if(val=='SI'){
        $('#divVerificada').removeClass('hide');
      } else {
        $('#divVerificada').addClass('hide');
      }
    });
    mostrarDiv();

  });

function mostrarDiv(){
    $('.selectPrecio').each(function(){
      var id = $(this).attr('id');
      var val = $(this).val();
      if(val == 'SI'){
        $('.'+id).removeClass('hide');
      } else {
        $('.'+id).addClass('hide');
      }
    })
  }
</script>


