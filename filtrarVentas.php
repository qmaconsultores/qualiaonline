<?php
  $seccionActiva=19;
  include_once("cabecera.php");
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-tags"></i><i class="icon-chevron-right"></i><i class="icon-filter"></i>
              <h3>Filtrado de ventas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              Seleccione el intervalo de fechas de venta:<br><br>
				<form action='ventasGeneralFiltrado.php' method='post'>
					<?php if($_SESSION['tipoUsuario']!='COMERCIAL'){ ?>
						Comercial: 
						<?php 
							echo "<select name='comercial' id='comercial' class='span3 show-tick'>
								<option value='todos'>Todos</option>";
		
								$consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' AND tipo='COMERCIAL' ORDER BY nombre, apellidos;",true);
								while($datos=mysql_fetch_assoc($consulta)){
									echo "<option value='".$datos['codigo']."'";

									echo ">".$datos['texto']."</option>";
								}
								
							echo "</select>";
						?>
						<br><br>
						Servicio: 
						<?php 
							echo "<select name='servicio' id='servicio' class='span3 show-tick'>
								<option value='todos'>Todos</option>";
		
								$consulta=consultaBD("SELECT codigo, nombreProducto AS texto FROM productos ORDER BY codigo;",true);
								while($datos=mysql_fetch_assoc($consulta)){
									echo "<option value='".$datos['codigo']."'";

									echo ">".$datos['texto']."</option>";
								}
								
							echo "</select>";
					}else{	
						campoOculto($_SESSION['codigoS'],'comercial');
					}
					?>
					<br><br>
					<div class='control-group'>                     
					  <div class='controls'>
						Del: <input type='text' class='input-small datepicker hasDatepicker' id='fechaUno' name='fechaUno' value='<?php echo imprimeFecha(); ?>'> 
						Al: <input type='text' class='input-small datepicker hasDatepicker' id='fechaDos' name='fechaDos' value='<?php echo imprimeFecha(); ?>'>
					  </div> <!-- /controls -->       
					</div> <!-- /control-group -->
					
					<button type="submit" class="btn btn-primary">Seleccionar <i class="icon-circle-arrow-right"></i></button>
				</form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();

  });
</script>