<?php
session_start();
include_once("funciones.php");

  $consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM incidencias WHERE estado='PENDIENTE';",true);
  $datosConsulta=mysql_fetch_assoc($consulta);
  $_SESSION['numIncidencias']=0;
  if($datosConsulta['codigo']>0){
	$_SESSION['numIncidencias']=$datosConsulta['codigo'];
  }

compruebaCookie();
if(isset($_POST['usuario']) && isset($_POST['clave'])){
	$error=login($_POST['usuario'],$_POST['clave']);
}
?>

<!DOCTYPE html>
<html lang="es">
  
<head>
    <meta charset="utf-8">
    <title>Acceso CRM - GRUPQUALIA</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

	<link href="css/font-awesome.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/pages/signin.css" rel="stylesheet" type="text/css">

</head>

<body onload="document.getElementById('username').focus();">

<div class="logo">
	<img src="img/logo.png" alt="GRUPQUALIA">	
</div>

<?php
if(isset($error) && $error==1){
	mensajeError('nombre de usuario y/o contraseña incorrectos.');
}
?>

<div class="account-container">
	
	<div class="content clearfix">
		
		<form action="?" method="post">

			<h1>Acceso a CRM</h1>
			
			<div class="login-fields">
				
				<p>Por favor, especifique sus credenciales:</p>
				
				<div class="field">
					<label for="username">Usuario</label>
					<input type="text" id="username" name="usuario" value="" placeholder="Usuario" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Contraseña:</label>
					<input type="password" id="password" name="clave" value="" placeholder="Contraseña" class="login password-field"/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<span class="login-checkbox">
					<input id="sesion" name="sesion" type="checkbox" class="field login-checkbox" value="si" tabindex="4" />
					<label class="choice" for="sesion">Mantener sesión iniciada</label>
				</span>
									
				<button class="button btn btn-large btn-primary"><i class="icon-signin"></i> Acceder</button>
				
			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->


<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/signin.js"></script>

</body>

</html>