<?php
  $seccionActiva=20;
  include_once("cabecera.php");
  $datos=datosRegistro('conversaciones',$_GET['codigo']);

  if(isset($_POST['mensaje'])){
    creaMensajeConversacion($datos['codigo']);
  }
  elseif(isset($_GET['elimina'])){
    eliminaMensajeConversacion($_GET['elimina']);
  }
  
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12 margenAb">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-comments-o"></i><i class="icon-chevron-right"></i><i class="icon-search-plus"></i>
              <h3>Ver conversación</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <fieldset class="form-horizontal">

                  <?php
                    echo "<h3 class='asuntoMensaje'>".$datos['asunto']."</h3>";
                    imprimeMensajesConversacion($datos['codigo']);
                  ?>

                  <br /><br />
                  <div class="form-actions sinFlotar">
                    <button type="button" class="btn btn-primary" id='nuevoMensaje'><i class="icon-plus-sign"></i> Nuevo mensaje</button> 
                    <a href="comunicacionInterna.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                  </div> <!-- /form-actions -->
                </fieldset>
              </div>

            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


<!-- Caja de respuesta -->

<div id='cajaRespuesta' class='modal hide fade cajaSelect'> 
  <div class='modal-header'> 
    <a class='close' data-dismiss='modal'>&times;</a>
    <h3> <i class="icon-comments-o"></i><i class="icon-chevron-right"></i><i class="icon-plus-circle"></i> Nuevo mensaje </h3>
  </div> 
  <form class="form-horizontal sinMargenAb" method="post" action="?codigo=<?php echo $datos['codigo']; ?>" enctype="multipart/form-data">
    <div class='modal-body cajaSelect'>
        <fieldset>
          <?php
            areaTexto('mensaje','Mensaje');
            campoFichero('ficheroAdjunto','Adjunto');
          ?>
        </fieldset>
    </div> 
    <div class='modal-footer sinFlotar'> 
      <button type="submit" class="btn btn-primary" id="enviaFormu"><i class="icon-mail-forward"></i> Enviar mensaje</button> 
      <button type="button" class="btn" data-dismiss='modal'><i class="icon-remove"></i> Cancelar</button> 
    </div> 
  </form>
</div>

<!-- Fin caja de respuesta -->

</div>
<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script src="js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript" src="js/bootstrap-filestyle.js"></script>


<script type="text/javascript">
  $(document).ready(function(){
    $('select').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    $('#mensaje').wysihtml5({locale: "es-ES"});
    $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

    $('#nuevoMensaje').click(function(){
      $('#cajaRespuesta').modal({'show':true,'backdrop':'static','keyboard':false});
    });

  });
</script>