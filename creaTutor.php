<?php
  $seccionActiva=5;
  include_once('cabecera.php');
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Tutor nuevo</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="tutores.php" method="post">
                  <fieldset>


                    <div class="control-group">                     
                      <label class="control-label" for="nombre">Nombre:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="nombre" name="nombre">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    
                    <div class="control-group">                     
                      <label class="control-label" for="apellidos">Apellidos:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="apellidos" name="apellidos">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="dni">DNI:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="dni" name="dni">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="email">eMail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="email" name="email">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('especialidad','Especialidad');
					?>
					
					<h3 class='apartadoFormulario'>Datos de tutorías</h3>
					
					<div class="control-group">                     
                      <label class="control-label" for="horario">Horario mañana:</label>
                      <div class="controls">
                        <input type="text" class="input-small timepicker" id="horaInicio" name="horaInicio" value="<?php echo hora(); ?>">
            						 a 
            						<input type="text" class="input-small timepicker" id="horaFin" name="horaFin" value="<?php echo hora(); ?>">
            						 (hh:mm) Comprendido entre 00:01h. y 15:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
				  	
				    <div class="control-group">                     
                      <label class="control-label" for="horario">Horario tarde:</label>
                      <div class="controls">
                        <input type="text" class="input-small timepicker" id="horaInicioTarde" name="horaInicioTarde" value="<?php echo hora(); ?>">
            						 a 
            						<input type="text" class="input-small timepicker" id="horaFinTarde" name="horaFinTarde" value="<?php echo hora(); ?>">
            						 (hh:mm) Comprendido entre 15:01h. y 00:00h.
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="horasTutoria">Horas tutorías:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="horasTutoria" name="horasTutoria">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="horas">Días de tutoría:</label>
                      <div class="controls">
                        <table class="mitadAncho">
                          <tr>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                          </tr>
                          <tr>
                            <th><input type="checkbox" id="lunes" name="lunes"></th>
                            <th><input type="checkbox" id="martes" name="martes"></th>
                            <th><input type="checkbox" id="miercoles" name="miercoles"></th>
                            <th><input type="checkbox" id="jueves" name="jueves"></th>
                            <th><input type="checkbox" id="viernes" name="viernes"></th>
                            <th><input type="checkbox" id="sabado" name="sabado"></th>
                            <th><input type="checkbox" id="domingo" name="domingo"></th>
                          </tr>
                        </table>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Registrar tutor</button> 
                      <a href="tutores.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-timepicker.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.timepicker').timepicker({showMeridian: false,defaultTime: false});
  });
</script>
