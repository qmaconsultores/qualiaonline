<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común

	//Carga de la librería PHPWord
	require_once 'phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	
	$datosFormulario=arrayFormulario();
	$acciones="";
	for($i=0;isset($datosFormulario['codigo'.$i]);$i++){	
		if($datosFormulario['codigo'.$i]!=0 && $datosFormulario['codigo'.$i]>0){
			$datos=datosRegistro('accionFormativa',$datosFormulario['codigo'.$i]);
			
			$nodoAccion=file_get_contents('documentos/nodoAccion.xml');
				
			$nodoAccion=str_replace('${codigoAccion}', $datos['codigoInterno'], $nodoAccion);
			$nodoAccion=str_replace('${nombre}', $datos['denominacion'], $nodoAccion);
			$nodoAccion=str_replace('${codigoGrupo}', $datos['grupoAcciones'], $nodoAccion);
			$nodoAccion=str_replace('${horas}', $datos['horas'], $nodoAccion);
			$modalidad=array("PRESENCIAL"=>"7", "DISTANCIA"=>"8", "MIXTA"=>"9", "TELE"=>"10" , "WEBINAR"=>"");
			$nodoAccion=str_replace('${modalidad}', $modalidad[$datos['modalidad']], $nodoAccion);
			$tipo=array("PROPIA"=>"0", "VINCULADA"=>"1");
			$nodoAccion=str_replace('${tipoAccion}', $tipo[$datos['tipoAccion']], $nodoAccion);
			$nivel=array("BASICO"=>"0", "SUPERIOR"=>"1");
			$nodoAccion=str_replace('${nivelFormacion}', $nivel[$datos['nivelAccion']], $nodoAccion);
			$nodoAccion=str_replace('${objetivos}', $datos['objetivos'], $nodoAccion);
			$nodoAccion=str_replace('${contenidos}', $datos['contenidos'], $nodoAccion);
			$nodoAccion=str_replace('${url}', $datos['urlAccion'], $nodoAccion);
			$nodoAccion=str_replace('${usuario}', $datos['usuarioAccion'], $nodoAccion);
			$nodoAccion=str_replace('${clave}', $datos['claveAccion'], $nodoAccion);

			$acciones.=$nodoAccion;
		}
	
	}
		
	
	unset($nodoAccion);
	$xml=file_get_contents('documentos/plantillaAccion.xml');
	$xml=str_replace('${accionFormativa}', $acciones, $xml);

	file_put_contents("documentos/AccionesFormativas.xml",$xml);

	unset($acciones);
	unset($xml);//Borro las variables para liberar memoria.
	
	/*
	Definir headers.
	Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
	que lo que va a recibir es un fichero que debe descargar.
	*/
	header("Content-Type: application/vnd.ms-xml");
	header('Content-Disposition: attachment; filename=AccionesFormativas.xml');
	header("Content-Transfer-Encoding: binary");

	/* 
	Descargar archivo.
	Por último, le decirmos a PHP que lea y transfiera el documento que
	antes con save.
	*/
	readfile('documentos/AccionesFormativas.xml');

?>