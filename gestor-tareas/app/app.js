var app=angular.module('tareas',[]);

app.controller('controladorTareas',function($scope, $http){
	getTareas();
	function getTareas(){
		$http.get('ajax/getTareas.php').success(function(datos){
			$scope.tareas=datos;
		});
	};
	
	$scope.addTarea=function(tarea){
		$http.get('ajax/addTarea.php?tarea='+tarea).success(function(datos){
			getTareas();
			$scope.inputTarea='';
		});
	};

	$scope.eliminaTarea=function(tarea){
		if(confirm('\u00bfEst\u00e1s seguro de que deseas eliminar esta tarea?')){
			$http.get('ajax/eliminaTarea.php?tarea='+tarea).success(function(datos){
				getTareas();
			});
		}
	};

	$scope.actualizaEstado=function(codigo, estado, tarea) {
		if(estado=='2'){
			estado='0';
		}
		else{
			estado='2';
		}
	 	$http.get("ajax/actualizaTarea.php?codigo="+codigo+"&estado="+estado).success(function(datos){
	 		getTareas();
	 	});
	};
});