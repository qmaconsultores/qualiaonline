<?php
  $seccionActiva=7;
  include_once('cabecera.php');
  $res=false;
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container margenAb">
      <div class="row">
		
		<div class="span12">
		  <div class="widget widget-nopad" id="target-1">
			<div class="widget-header"> <i class="icon-tasks"></i>
			  <h3>Totales ventas</h3>
			</div>
			<!-- /widget-header -->
			
			<div class="widget-content">
			  <div class="widget big-stats-container">
				<div class="widget-content">
					<center>
						<div class='seleccionComercial'>
							<strong>Mes: </strong>
							<?php 
								echo "<select name='mes' id='mes' class='input-large'>";
								echo "<option value='0'></option>";
								echo "<option value='01'>Enero</option>";
								echo "<option value='02'>Febrero</option>";
								echo "<option value='03'>Marzo</option>";
								echo "<option value='04'>Abril</option>";
								echo "<option value='05'>Mayo</option>";
								echo "<option value='06'>Junio</option>";
								echo "<option value='07'>Julio</option>";
								echo "<option value='08'>Agosto</option>";
								echo "<option value='09'>Septiembre</option>";
								echo "<option value='10'>Octubre</option>";
								echo "<option value='11'>Noviembre</option>";
								echo "<option value='12'>Diciembre</option>";
								echo "</select>";
							?>
							<strong>Día: </strong>
							<?php
								echo "<select name='dia' id='dia' class='input-large'>";
								echo "<option value=''></option>";
								echo "<option value='01'>1</option>";
								echo "<option value='02'>2</option>";
								echo "<option value='03'>3</option>";
								echo "<option value='04'>4</option>";
								echo "<option value='05'>5</option>";
								echo "<option value='06'>6</option>";
								echo "<option value='07'>7</option>";
								echo "<option value='08'>8</option>";
								echo "<option value='09'>9</option>";
								echo "<option value='10'>10</option>";
								echo "<option value='11'>11</option>";
								echo "<option value='12'>12</option>";
								echo "<option value='13'>13</option>";
								echo "<option value='14'>14</option>";
								echo "<option value='15'>15</option>";
								echo "<option value='16'>16</option>";
								echo "<option value='17'>17</option>";
								echo "<option value='18'>18</option>";
								echo "<option value='19'>19</option>";
								echo "<option value='20'>20</option>";
								echo "<option value='21'>21</option>";
								echo "<option value='22'>22</option>";
								echo "<option value='23'>23</option>";
								echo "<option value='24'>24</option>";
								echo "<option value='25'>25</option>";
								echo "<option value='26'>26</option>";
								echo "<option value='27'>27</option>";
								echo "<option value='28'>28</option>";
								echo "<option value='29'>29</option>";
								echo "<option value='30'>30</option>";
								echo "<option value='31'>31</option>";
								echo "</select>";
							?>	
						</div>
					</center>
				  <h6 class="bigstats">Importes:</h6>


					<span id='resultadoDos'></span>

				</div> <!-- /widget-content -->
				<!-- /widget-content --> 
				
			  </div>
			</div>
		  </div>
		  <div class="widget widget-table action-table">
          <span id="resultado"></span>
		 
		</div>
		
		
	</div><!-- /row -->

    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/guidely/guidely.min.js"></script>
<script type="text/javascript" src="js/bootstrap-progressbar.js"></script>

<script type="text/javascript" src="js/full-calendar/fullcalendar.min.js"></script>

<script type="text/javascript">
   

	$('#mes').change(function(){
		var mes=$('#mes').val();
		var dia=$('#dia').val();
		muestraCantidades(mes,dia);
		listadoVentas(mes,dia);
	});	
	
	$('#dia').change(function(){
		var mes=$('#mes').val();
		var dia=$('#dia').val();
		muestraCantidades(mes,dia);
		listadoVentas(mes,dia);
	});	
	  
	var mes=$('#mes').val();
	var dia=$('#dia').val();
	$(document).ready(muestraCantidades(mes,dia));
	$(document).ready(listadoVentas(mes,dia));
	
	  
	function muestraCantidades(mes,dia){
		var parametros = {
                "mes": mes,
				"dia": dia
        };
		$.ajax({
			 type: "POST",
			 url: "listadosajax/muestraCantidadesVentaNeta.php",
			 data: parametros,
			  beforeSend: function () {
				   $("#resultadoDos").html('<center><i class="icon-refresh icon-spin"></i></center>');
			  },
			 success: function(response){
				   $("#resultadoDos").html(response);
			 }
		});
	}

	function listadoVentas(mes,dia){
		var parametros = {
                "mes" : mes,
                "dia" : dia
        };
		 $.ajax({
			 type: "POST",
			 url: "listadosajax/listadoVentasComerciales.php",
			 data: parametros,
			  beforeSend: function () {
				   $("#resultado").html('<center><div class="seleccionComercial"><i class="icon-refresh icon-spin"></i></div></center>');
			  },
			 success: function(response){
				   $("#resultado").html(response);
			 }
		});
	 }
</script>