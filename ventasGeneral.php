<?php
  $seccionActiva=19;
  include_once('cabecera.php');
  
  if(isset($_POST['codigo'])){
    $res=actualizaVenta();
  }
  elseif(isset($_POST['completaAlumnos'])){
    $res=completaAlumnos();
  }
  elseif(isset($_POST['codigoC'])){
    $res=registraVenta();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaVenta();
  }
  

	$anio=date('Y');
	$fecha="AND fecha>='".$anio."-1-1'";
	$fecha.=" AND fecha<='".$anio."-12-31'";
  $estadisticas=creaEstadisticasVentasGeneral($fecha);

?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de ventas realizadas</h6>

                   <div id="big_stats" class="cf">

                    <div class="stat"> <i class="icon-shopping-cart"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Ventas realizadas</div>
                    <!-- .stat -->

                    <div class="stat"> <i class="icon-eur"></i> <span class="value"><?php echo $estadisticas['preciototal']?></span> <br>Total ventas</div>

					
					<?php if($_SESSION['tipoUsuario']!='ADMINISTRACION'){ ?>
						<div class="stat"> <i class="icon-eur"></i> <span class="value"><?php echo $estadisticas['precioNueva']?></span> <br>Total Vtas Nuevas</div>
						<!-- .stat -->
						<div class="stat"> <i class="icon-eur"></i> <span class="value"><?php echo $estadisticas['precioCartera']?></span> <br>Total cartera</div>
   
					<?php } ?>

                  </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6" style='float:right;'>
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de ventas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content prueba">
              <div class="shortcuts">
				<?php if($_SESSION['tipoUsuario']!='SUPERVISOR'){?>
				        <a href="seleccionaCliente.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Nueva venta</span> </a>
                <?php }compruebaOpcionEliminar();
					if($_SESSION['tipoUsuario']!='SUPERVISOR'){
				?>
					<a href="filtrarVentas.php" class="shortcut"><i class="shortcut-icon icon-filter"></i><span class="shortcut-label">Filtrar</span> </a>
				<?php } 
					if($_SESSION['tipoUsuario']=='ADMIN'){?>
						<a href="generaExcelVentas.php" class="shortcut"><i class="shortcut-icon icon-download-alt"></i><span class="shortcut-label">Generar excel</span> </a>
				<?php } ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>	


      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Datos de la venta actualizados."); 
            }
            else{
              mensajeError("no se ha podido actualizar la venta. Compruebe los datos introducidos."); 
            }
          }
		  elseif(isset($_POST['completaAlumnos'])){
            if($res){
              mensajeOk("Venta registrada y alumnos completados."); 
            }
            else{
              mensajeError("no se ha podido completar los datos de los alumnos. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['codigoC'])){
            if($res){
              mensajeOk("Venta registrada correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar la venta. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar la venta. Contacte con el webmaster."); 
            }
          }
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Ventas realizadas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			   <table class='table table-striped table-bordered datatable' id='tablaVentas'>
                <thead>
                  <tr>
                    <th> ID </th>
					          <th> Empresa </th>
                    <th> Tipo de venta </th>
					          <th> Comercial </th>
                    <th> Concepto </th>
					          <th> Fecha </th>
                    <th> Importe </th>
					          <th> Cartera / Nueva </th>
                    <th class='centro'></th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>
				
				<?php //imprimeVentasGeneral(); ?>
				
				</tbody>
              </table>
                
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
$('#tablaVentas').dataTable({
  'bProcessing': true, 
  'bServerSide': true, 
  'sAjaxSource': 'listadosajax/listadoVentasAjax.php?action=getMembersAjx',
  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	  $('td:eq(8)', nRow).addClass( "centro" );
   },
   "iDisplayLength":25,
  "oLanguage": {
	  "sLengthMenu": "_MENU_ registros por página",
	  "sSearch":"Búsqueda:",
	  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
	  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
	  "sEmptyTable":"Aún no hay datos que mostrar",
	  "sInfoEmpty":"",
	  'sInfoFiltered':"",
	  'sZeroRecords':'No se han encontrado coincidencias',
	  'sProcessing':'Procesando...'
	}
});
$('#eliminar').click(function(){
  var valoresChecks=recorreChecks();
  if(valoresChecks['codigo0']==undefined){
    alert('Por favor, seleccione antes una venta.');
  }
  else{
    valoresChecks['elimina']='SI';
    creaFormulario('ventasGeneral.php',valoresChecks,'post');
  }

});
</script>
