<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos2/plantillaDevoluciones.xlsx");
	

	exportarExcelDevoluciones($objPHPExcel);//Llamada a la función que rellena el Excel

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('documentos2/Listado_Devoluciones.xlsx');


	// Definir headers
	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=Listado_Devoluciones.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos2/Listado_Devoluciones.xlsx');
?>