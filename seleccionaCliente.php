<?php
  $seccionActiva=19;
  include_once("cabecera.php");
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nueva Factura</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              Seleccione el cliente para el que va a crear la venta:<br><br>
              <?php
				echo'<form action="creaVentaGeneral.php" method="post">';
					selectClientes(false, 'AND activo="SI"');
					echo '<br>
					<button type="submit" class="btn btn-primary">Seleccionar <i class="icon-circle-arrow-right"></i></button>
				</form>';
              ?>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#fechaEmision').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('#fechaVencimiento').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();
    

  });
</script>