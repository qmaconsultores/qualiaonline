<?php
  $seccionActiva=10;
  include_once('cabecera.php');

  if(isset($_POST['codigo'])){
    $res=actualizaObjetivo();
  }
  elseif(isset($_POST['fechaInicio'])){
    $res=registraObjetivo();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaObjetivo();
  }
  
  compruebaFechasObjetivos();
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas globales del sistema para los objetivos vigentes</h6>

                   <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
              
                   <div class="leyenda">
                    <span class="grafico grafico1"></span>Ventas: <span id="valor1"></span><br>
                    <span class="grafico grafico2"></span>Por alcanzar: <span id="valor2"></span><br>
                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de objetivos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="creaObjetivo.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Crear Objetivo</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Objetivo actualizado correctamente."); 
            }
            else{
              mensajeError("no se han podido actualizar el objetivo. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['fechaInicio'])){
  		      if($res){
              mensajeOk("Objetivo establecido correctamente."); 
            }
            else{
              mensajeError("no se ha podido establecer el objetivo. Compruebe los datos introducidos."); 
            }
		      }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el posible cliente. Contacte con el webmaster."); 
            }
          }
        ?>
		
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Objetivos registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Objetivo </th>
                    <th> Fecha de inicio </th>
                    <th> Fecha de fin </th>
                    <th class="centro"> </th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeObjetivos();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/excanvas.min.js" type="text/javascript"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
  <?php
    $datos=generaDatosGraficoObjetivos();
  ?>
  var pieData = [
      {
          value: <?php echo $datos['ventas']; ?>,
          color: "#428bca"
      },
      {
          value: <?php echo $datos['objetivo']; ?>,
          color: "#B02B2C"
      }
    ];

  var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
  
  $("#valor1").text("<?php echo $datos['ventas']; ?>");
  $("#valor2").text("<?php echo $datos['objetivo']; ?>");


  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('objetivos.php',valoresChecks,'post');
    }

  });

</script>
