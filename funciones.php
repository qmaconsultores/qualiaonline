<?php

include_once('nucleo.php');


function generaDatosGraficoPosiblesClientes($activo='NO'){
	$datos=array();
	//$codigoS=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
		$where='WHERE codigoUsuario="'.$_SESSION['codigoS'].'" OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'")';
	}
	elseif($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION' && $_SESSION['tipoUsuario']!='MARKETING'){
		$where=compruebaPerfilParaWhere();
	}
	
	conexionBD();

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes $where AND baja='NO';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totales']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes $where AND activo='".$activo."' AND baja='NO';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['pendientes']=$consulta['codigo'];

	cierraBD();

	return $datos;
}


function generaDatosGraficoCuentas(){
	$datos=array();
	//$codigoS=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
		$where='WHERE (codigoUsuario="'.$_SESSION['codigoS'].'" OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'"))';
	}
	elseif($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION' && $_SESSION['tipoUsuario']!='MARKETING'){
		$where=compruebaPerfilParaWhere();
	}

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes $where AND baja='NO';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totales']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes $where AND activo='SI' AND baja='NO';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['confirmados']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function creaEstadisticasVentas($codigoCliente){
	$datos=array();

	conexionBD();	

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM ventas WHERE codigoCliente='".$codigoCliente."';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];


	$consulta=consultaBD("SELECT SUM(precio) AS precio FROM ventas WHERE codigoCliente='".$codigoCliente."';");
	$consulta=mysql_fetch_assoc($consulta);

	if($consulta['precio']==''){
		$datos['precio']='0';
	}
	else{
		$datos['precio']=$consulta['precio'];
	}

	cierraBD();

	return $datos;
}

function creaEstadisticasInicio(){
	$datos=array();

	conexionBD();

	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION'){
		$where=compruebaPerfilParaWhere();
	}
	
	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes $where AND activo='NO';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['clientes']=$consulta['codigo'];


	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes $where AND activo='SI';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['cuentas']=$consulta['codigo'];
	
	
	//Para mostrar totales cartera y venta nueva
	$whereVentas=compruebaPerfilParaWhere('ventas.codigoUsuario');
	$consulta=consultaBD("SELECT COUNT(ventas.codigo) AS codigo FROM ventas INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo $whereVentas AND tipoVentaCarteraNueva='NUEVA' AND clientes.baja='NO' AND ventas.fecha >= '".date('Y')."-".date('m')."-1' AND ventas.fecha <= '".date('Y')."-".date('m')."-31';"); 
	$consulta=mysql_fetch_assoc($consulta);

	$datos['nueva']=$consulta['codigo'];


	$consulta=consultaBD("SELECT COUNT(ventas.codigo) AS codigo FROM ventas INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo $whereVentas AND tipoVentaCarteraNueva='CARTERA' AND clientes.baja='NO' AND ventas.fecha >= '".date('Y')."-".date('m')."-1' AND ventas.fecha <= '".date('Y')."-".date('m')."-31';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['cartera']=$consulta['codigo'];

	$consulta=consultaBD("SELECT SUM(ventas.precio) AS codigo FROM ventas INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo $whereVentas AND tipoVentaCarteraNueva='NUEVA' AND clientes.baja='NO' AND ventas.fecha >= '".date('Y')."-".date('m')."-1' AND ventas.fecha <= '".date('Y')."-".date('m')."-31';"); 
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totalnueva']=$consulta['codigo'];


	$consulta=consultaBD("SELECT SUM(ventas.precio) AS codigo FROM ventas INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo $whereVentas AND tipoVentaCarteraNueva='CARTERA' AND clientes.baja='NO' AND ventas.fecha >= '".date('Y')."-".date('m')."-1' AND ventas.fecha <= '".date('Y')."-".date('m')."-31';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totalcartera']=$consulta['codigo'];


	$whereTareas=compruebaPerfilParaWhere('tareas.codigoUsuario');
	$consulta=consultaBD("SELECT COUNT(tareas.codigo) AS codigo	FROM clientes INNER JOIN tareas ON clientes.codigo=tareas.codigoCliente $whereTareas AND tareas.estado='PENDIENTE';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['tareas']=$consulta['codigo'];


	$whereIncidencias=compruebaPerfilParaWhere('incidencias.codigoUsuario');
	$consulta=consultaBD("SELECT COUNT(incidencias.codigo) AS codigo FROM clientes INNER JOIN incidencias ON clientes.codigo=incidencias.codigoCliente $whereIncidencias AND incidencias.estado='PENDIENTE';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['incidencias']=$consulta['codigo'];
	
	//Para mostrar total de auditorías pendientes
	$consulta=consultaBD("SELECT COUNT(trabajos.codigo) AS codigo FROM trabajos WHERE fechaValidez>='".date('Y-m-d')."';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['auditorias']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function altaCliente($activo='NO'){
	$res=true;

	$datos=arrayFormulario();

	if(isset($datos['codigoUsuario'])){
		$codigoU=$datos['codigoUsuario'];
	}
	else{
		$codigoU=$_SESSION['codigoS'];
	}

	conexionBD();
	$verifica = $_SESSION["verifica"];  
	if(isset($verifica) && $verifica == 1){
		unset($_SESSION['verifica']);
		$consulta=consultaBD("INSERT INTO clientes VALUES(NULL,'".$datos['empresa']."', '".$datos['cif']."', '".$datos['direccion']."', 
			'".$datos['cp']."', '".$datos['localidad']."', '".$datos['provincia']."', '".$datos['telefono']."', '".$datos['movil']."', '".$datos['mail']."', 
			'".$datos['fax']."', '".$datos['contacto']."', '".$datos['cargo']."', ".$datos['comercial'].",
			'".$datos['actividad']."', '".$datos['sector']."', '".$datos['tipo']."', '".$datos['fechaRegistro']."', ".$datos['tipoServicio'].", '0000-00-00', '$activo', '".$datos['ccc']."', '".$datos['dniRepresentante']."', 
			'".$datos['representacion']."', '".$datos['nuevacreacion']."', '".$datos['fechaCreacion']."', '".$datos['pyme']."',
			'".$datos['repreLegal']."', '".$datos['numCuenta']."', '".$datos['telefonoDos']."', '".$datos['movilDos']."', '".$datos['tieneColaborador']."', '".$datos['creditoFormativo']."', '".$datos['formaPago']."', '', '".$datos['bic']."', '".$datos['referencia']."', '".$datos['baja']."', '".$datos['fechaBaja']."',
			'".$datos['numTrabajadores']."', '".$datos['tipoRemesa']."', '".$datos['nombreGestoria']."', '".$datos['mailGestoria']."', '".$datos['telefonoGestoria']."', '".$datos['estado']."', '".$datos['direccionVisita']."', '".$datos['firmado']."', '".$datos['pendiente']."',
			'".$datos['responsableSeguridad']."', '".$datos['nifResponsable']."', '".$datos['cnae']."', '".$datos['mail2']."', '".$datos['web']."', '".$datos['trabajadores']."', '', '$codigoU');");
		$codigoCliente=mysql_insert_id();

		if(!$consulta){
			$res=false;
		}else{		
			$res=$res && insertaHistorico($codigoCliente);
			$res=$res && insertaServicios($codigoCliente);
			//$res=$res && insertaEmpresasVinculadas($_POST['codigo']);
			if($datos['nombreGestoria']!='' && $datos['mailGestoria']!='' && $datos['telefonoGestoria']!=''){
				$_POST['codigoCliente']=$codigoCliente;
				$res=$res && insertaDatos('gestorias');
			}
		}	
		cierraBD();
		
		if($res){
			pasaLOPD($codigoCliente);
		}
	}else{
		$res=false;
	}

	return $res;

}


function imprimeClientes(){
	//$codigoS=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
		$where='WHERE codigoUsuario="'.$_SESSION['codigoS'].'" OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'"';
	}
	elseif($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION'){
		$where=compruebaPerfilParaWhere();
	}

	conexionBD();

	$consulta=consultaBD("SELECT clientes.codigo, empresa, nombreProducto, sector, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS nombreUsuario, referencia, localidad FROM clientes LEFT JOIN productos ON clientes.tipoServicio=productos.codigo LEFT JOIN usuarios ON usuarios.codigo=clientes.codigoUsuario $where AND activo='NO' ORDER BY referencia, empresa, fechaRegistro;");
	$datos=mysql_fetch_assoc($consulta);

	$sectores=array("HOSTELERIA"=>"Hostelería", "COMERCIO"=>"Comercio", "TRANSPORTE"=>"Transporte", "ESTETICA"=>"Estética", "SANIDAD"=>"Sanidad", "VETERINARIA"=>"Veterinaria", "DEPORTES"=>"Deportes", "ASOCIACIONES"=>"Asociaciones", "FORMACION"=>"Formación", "INDUSTRIA"=>"Industria", "CONSTRUCCION"=>"Construcción", "INFORMATICA"=>"Informática", "SERVICIOS"=>"Servicios", "OTROS"=>"Otros", ""=>"");

	$mensaje="";
	while($datos!=0){
		$mensaje.= "
		<tr>
			<td> P-".$datos['referencia']." </td>
        	<td> ".$datos['empresa']." </td>
			<td> ".$datos['localidad']." </td>
			<td> ".$datos['nombreUsuario']." </td>
        	<td> ".$sectores[$datos['sector']]." </td>
        	<td> ".$datos['nombreProducto']." </td>
        	<td class='centro'>
				<div class='margenPeque'>
					<a href='detallesPosibleCliente.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a>
					<a href='creaTarea.php?codigo0=".$datos['codigo']."' class='btn btn-warning'><i class='icon-plus-sign'></i> Añadir Tarea</i></a>
					<a href='creaVenta.php?codigo=".$datos['codigo']."&activa' class='btn btn-success'><i class='icon-ok-sign'></i> Confirmar venta</i></a>
				</div>
				<a href='trabajos.php?codigoCliente=".$datos['codigo']."' class='btn btn-warning'><i class='icon-cogs'></i> Trabajos</i></a>
				<a href='tareas.php?codigoCliente=".$datos['codigo']."' class='btn btn-success'><i class='icon-calendar'></i> Tareas</i></a>
				<a href='incidencias.php?codigoCliente=".$datos['codigo']."' class='btn btn-inverse'><i class='icon-exclamation'></i> Incidencias</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	echo $mensaje;
	cierraBD();
}

function imprimeCuentas(){
	//$codigoS=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION'){
		$where=compruebaPerfilParaWhere();
	}

	conexionBD();

	$consulta=consultaBD("SELECT clientes.codigo, empresa, nombreProducto, sector, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS nombreUsuario, clientes.referencia, localidad, fechaUltimaVenta FROM clientes 
	LEFT JOIN productos ON clientes.tipoServicio=productos.codigo 
	LEFT JOIN usuarios ON usuarios.codigo=clientes.codigoUsuario $where AND activo='SI' ORDER BY referencia, empresa, fechaVenta;");
	
	$datos=mysql_fetch_assoc($consulta);
	
	$sectores=array("HOSTELERIA"=>"Hostelería", "COMERCIO"=>"Comercio", "TRANSPORTE"=>"Transporte", "ESTETICA"=>"Estética", "SANIDAD"=>"Sanidad", "VETERINARIA"=>"Veterinaria", "DEPORTES"=>"Deportes", "ASOCIACIONES"=>"Asociaciones", "FORMACION"=>"Formación", "INDUSTRIA"=>"Industria", "CONSTRUCCION"=>"Construcción", "INFORMATICA"=>"Informática", "SERVICIOS"=>"Servicios", "OTROS"=>"Otros", ""=>"");

	while($datos!=0){
		echo "
		<tr>
			<td> ".$datos['referencia']." </td>
        	<td> ".$datos['empresa']." </td>
			<td> ".$datos['localidad']." </td>
			<td> ".$datos['nombreUsuario']." </td>
        	<td> ".$sectores[$datos['sector']]." </td>
        	<td> ".$datos['nombreProducto']." </td>
			<td> ".formateaFechaWeb($datos['fechaUltimaVenta'])." </td>
        	<td class='centro'>
				<div class='margenPeque'>
        		<a href='detallesCuenta.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a>
				<a href='creaTarea.php?codigo0=".$datos['codigo']."' class='btn btn-warning'><i class='icon-plus-sign'></i> Añadir Tarea</i></a>
				<a href='ventas.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-shopping-cart'></i> Ventas realizadas</i></a>
				<a href='generaContrato.php?codigo=".$datos['codigo']."' class='btn btn-info'><i class='icon-download'></i> Contrato</i></a>
				</div>
				<a href='trabajos.php?codigoCliente=".$datos['codigo']."' class='btn btn-warning'><i class='icon-cogs'></i> Trabajos</i></a>
				<a href='tareas.php?codigoCliente=".$datos['codigo']."' class='btn btn-success'><i class='icon-calendar'></i> Tareas</i></a>
				<a href='incidencias.php?codigoCliente=".$datos['codigo']."' class='btn btn-inverse'><i class='icon-exclamation'></i> Incidencias</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function imprimeVentas($codigoCliente){
	conexionBD();
	$consulta=consultaBD("SELECT ventas.codigo, ventas.tipo, productos.nombreProducto, precio, fecha, observaciones, CONCAT(nombre, ' ', apellidos) AS comercial, productos.nombreProducto, tipoVentaCarteraNueva FROM ventas
	LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo
	LEFT JOIN productos ON ventas.concepto=productos.codigo WHERE codigoCliente='".$codigoCliente."' ORDER BY fecha DESC;");
	echo mysql_error();
	$datos=mysql_fetch_assoc($consulta);

	

	while($datos!=0){
		$fecha=formateaFechaWeb($datos['fecha']);

		echo "
		<tr>
        	<td> ".ucfirst($datos['tipo'])." </td>
			<td> ".$datos['comercial']." </td>
        	<td> ".$datos['nombreProducto']." </td>
			<td> $fecha </td>
			<td> ".$datos['precio']." € </td>
			<td> ".ucfirst($datos['observaciones'])." </td>
			<td> ".$datos['tipoVentaCarteraNueva']." </td>
        	<td class='centro'>
        		<a href='detallesVenta.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
        	</td>
        	<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function datosCliente($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM clientes WHERE codigo='$codigo';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}


function registraVenta(){
	$res=true;

	$datos=arrayFormulario();
	$referenciaFactura=$datos['referencia'];
	conexionBD();
	$verifica = 1;  
  	$_SESSION["verifica"] = $verifica; 
	if(isset($_SESSION["verifica"]) && $_SESSION["verifica"] == 1){
		if($datos['activa']==1){
			$cliente=datosRegistro('clientes',$datos['codigoC']);
			conexionBD();
			if($cliente['activo']!='SI'){
				$anio=date('Y')+1;
				$fecha=$anio.'-'.date('m').'-'.date('d');
				$consultaAux=consultaBD("SELECT referencia FROM clientes WHERE activo='SI' ORDER BY referencia DESC LIMIT 1;");
				$referencia=mysql_fetch_assoc($consultaAux);
				$referenciaNueva=$referencia['referencia']+1;
			} else {
				$referenciaNueva=$cliente['referencia'];
			}
			$consulta=consultaBD("UPDATE clientes SET activo='SI', fechaVenta='".$datos['fechaVenta']."', referencia='$referenciaNueva'
				WHERE codigo='".$datos['codigoC']."';");
		}
		$consulta=consultaBD("UPDATE clientes SET fechaUltimaVenta='".$datos['fechaVenta']."' WHERE codigo='".$datos['codigoC']."';");
		$consulta=consultaBD("INSERT INTO ventas VALUES(NULL, '".$datos['tipo']."', '".$datos['concepto']."', '".$datos['precio']."', '".$datos['fechaVenta']."',
			'".$datos['observaciones']."','".$datos['participaColaborador']."','".$datos['comision']."','".$datos['tipoVentaCarteraNueva']."','".$datos['codigoC']."','".$datos['codigoUsuario']."');");
			$codigoVenta=mysql_insert_id();
		$consulta=consultaBD("SELECT tipoServicio FROM clientes WHERE codigo='".$datos['codigoC']."';");
		$tipoServicio=mysql_fetch_assoc($consulta);
		if(!isset($tipoServicio['tipoServicio'])){
			$tipoServicio['tipoServicio']='NULL';
		}
		$consulta=consultaBD("INSERT INTO ofertas VALUES(NULL, '".$codigoVenta."', '".$datos['precio']."', '".formateaFechaBD(fecha())."', '', 'ACEPTADA', ".$tipoServicio['tipoServicio'].", '".$datos['codigoC']."', '".$datos['codigoUsuario']."');");
		$codigoOferta=mysql_insert_id();
		$fecha = date_create(date('Y-m-d'));
		$intervalo=compruebaIntervalo($datos['concepto']);
		date_add($fecha, date_interval_create_from_date_string($intervalo));
		$fechaInsertar=date_format($fecha, 'Y-m-d');
		if($datos['concepto']=='14'){
			$formacion=$datos['formacionCurso'];
		} else {
			if($datos['codigoVentaRelacionada'] == 'NULL'){
				$formacion='NO';
			} else {
				cierraBD();
				$ventaRelacionada=datosRegistro('ventas',$datos['codigoVentaRelacionada']);
				$ofertaRelacionada=datosRegistro('ofertas',$ventaRelacionada['codigo'],'codigoOferta');
				$trabajo=datosRegistro('trabajos',$ofertaRelacionada['codigo'],'codigoOferta');
				$consulta=consultaBD("INSERT INTO ventas_relacionadas VALUES(NULL,".$ventaRelacionada['codigo'].",".$codigoVenta.")",true);
				$alumno=datosRegistro('alumnos',$ventaRelacionada['codigo'],'codigoVenta');
				$alumno=datosRegistro('alumnos_registrados_cursos',$alumno['codigo'],'codigoAlumno');
				if($alumno['resolucion']=='SI'){
					$formacion='FINALIZADO';
				} else {
					$formacion='HECHO';
				}
				conexionBD();
			}
		}
		$consulta=consultaBD("INSERT INTO trabajos VALUES(NULL, '$codigoOferta', '".$datos['codigoC']."', ".$tipoServicio['tipoServicio'].", '', '".$datos['proyecto']."', '', 'PROCESO',
		'$fechaInsertar','".$formacion."','".$datos['prl']."','".$datos['lopd']."','".$datos['lssi']."','".$datos['aler']."','".$datos['web']."','".$datos['auditoria1']."','".$datos['auditoria2']."',NULL);");
		$codigoTrabajo=mysql_insert_id();
		$consulta=consultaBD("INSERT INTO hitos VALUES(NULL, '$codigoTrabajo', '".$datos['actividad0']."', '".$datos['fechaPrevista0']."', '', '".$datos['observaciones0']."');");
		$consulta=consultaBD("INSERT INTO hitos VALUES(NULL, '$codigoTrabajo', '".$datos['actividad1']."', '".$datos['fechaPrevista1']."', '', '".$datos['observaciones1']."');");
		$consulta=consultaBD("INSERT INTO hitos VALUES(NULL, '$codigoTrabajo', '".$datos['actividad2']."', '".$datos['fechaPrevista2']."', '', '".$datos['observaciones2']."');");
		if($datos['concepto']!='14'){
			$_POST['codigoCurso']='NULL';
			$codigoCurso='NULL';
			$_POST['codigoCurso']=$codigoCurso;
		}elseif($datos['concepto']=='14'){
			$consulta=consultaBD("INSERT INTO historico_clientes VALUES(NULL, '".$datos['observaciones']."', '".date('Y-m-d')."', '".date('H:i:s')."', '".$datos['codigoC']."', '".$_SESSION['codigoS']."');");
			$i=0;
			//NUEVO PARA VENTA DIRECTA DE CURSO
			$arrayAlumnos=array();
			while(isset($datos['nombre'.$i])){
				if($datos['nombre'.$i]=='' && $datos['apellidos'.$i]=='' && $datos['dni'.$i]=='' && $datos['mail'.$i]=='' && $datos['tlf'.$i]==''){
				}else{
					$consulta=consultaBD("INSERT INTO alumnos(codigo,nombre,apellidos,dni,mail,telefono,codigoVenta,codigoUsuario) VALUES(NULL,'".$datos['nombre'.$i]."','".$datos['apellidos'.$i]."','".$datos['dni'.$i]."','".$datos['mail'.$i]."','".$datos['tlf'.$i]."','".$codigoVenta."','".$_SESSION['codigoS']."');");
					$codigoAlumno=mysql_insert_id();
					array_push($arrayAlumnos, $codigoAlumno);
				}	
				$i++;
			}
			if(isset($datos['alumnosPrevios'])){
				foreach($datos['alumnosPrevios'] as $alumno){ 
					array_push($arrayAlumnos, $alumno);
				}
			}
			$codigoCurso=registraCursoNuevo($arrayAlumnos,$datos);
			$_POST['codigoCurso']=$codigoCurso;
		}
		
		$_POST['codigoVenta']=$codigoVenta;
		if(isset($_POST['codigoPreventa'])){
			$res=consultaBD('UPDATE preventas SET aceptado="SI" WHERE codigo='.$_POST['codigoPreventa'],true);
			$res=consultaBD('INSERT INTO ventas_preventas VALUES(NULL,'.$codigoVenta.','.$_POST['codigoPreventa'].');',true);
		}
		$codigoFactura=insertaDatos('facturacion');
		$_POST['cursos']=array();
		if(!is_null($codigoCurso)){
			array_push($_POST['cursos'],$codigoCurso);
			insertaCursosFactura($codigoFactura);
		}
		insertaVtoFacturas($codigoFactura);
		//unset($_SESSION['verifica']);
	}else{
		$res=false;
	}

	return $res;
}

function compruebaIntervalo($concepto){
	$intervalo='';
	switch($concepto){
		case '14':
			$intervalo='50 days';
		break;
		case '15':
			$intervalo='9 days';
		break;
		case '18':
			$intervalo='9 days';
		break;
		case '19':
			$intervalo='9 days';
		break;
		case '20':
			$intervalo='60 days';
		break;
		case '21':
			$intervalo='9 days';
		break;
		case '22':
			$intervalo='9 days';
		break;
		case '23':
			$intervalo='9 days';
		break;
		case '24':
			$intervalo='9 days';
		break;
		case '25':
			$intervalo='9 days';
		break;
		case '26':
			$intervalo='9 days';
		break;
		case '27':
			$intervalo='9 days';
		break;
		case '28':
			$intervalo='15 days';
		break;
		default:
			$intervalo='15 days';
	}
	return $intervalo;
}

function actualizaCliente(){
	$res=true;

	$datos=arrayFormulario();
	if(isset($datos['codigoUsuario'])){
		$comercial=", codigoUsuario='".$datos['codigoUsuario']."'";
	}
	else{
		$comercial="";
	}
	
	if(isset($datos['empresa'])){
		$consulta=actualizaDatos('clientes');
		
		if(!$consulta){
			$res=false;
			echo mysql_error();
		}			

		conexionBD();
	}else{
		conexionBD();

		if($_SESSION['tipoUsuario']=='FORMACION'){
			$consulta=consultaBD("UPDATE clientes SET creditoFormativo='".$datos['creditoFormativo']."', numTrabajadores='".$datos['numTrabajadores']."', 
			nombreGestoria='".$datos['nombreGestoria']."', mailGestoria='".$datos['mailGestoria']."', telefonoGestoria='".$datos['telefonoGestoria']."'
			WHERE codigo='".$datos['codigo']."';");
		}else{
			if(isset($datos['telefono'])){
				$consulta=consultaBD("UPDATE clientes SET telefono='".$datos['telefono']."', mail='".$datos['mail']."', 
				contacto='".$datos['contacto']."',
				sector='".$datos['sector']."'
				WHERE codigo='".$datos['codigo']."';");
			}
		}
	}

	$res=$res && insertaHistorico($_POST['codigo']);
	$res=$res && insertaServicios($_POST['codigo']);
	$res=$res && insertaEmpresasVinculadas($_POST['codigo']);

	cierraBD();	
	if(isset($_FILES['ficheroFactura']) && $_FILES['ficheroFactura']['name']!=''){
    	$res=$res && insertaDatos('documentosClientes',time(),'documentos');
    }
	if(isset($_FILES['ficheroFacturaFacturacion']) && $_FILES['ficheroFacturaFacturacion']['name']!=''){
		$_FILES['ficheroFactura']=$_FILES['ficheroFacturaFacturacion'];
    	$res=$res && insertaDatos('documentosClientes',time(),'documentos');
    }
	if(isset($_FILES['ficheroFacturaConsultoria']) && $_FILES['ficheroFacturaConsultoria']['name']!=''){
		$_FILES['ficheroFactura']=$_FILES['ficheroFacturaConsultoria'];
    	$res=$res && insertaDatos('documentosClientes',time(),'documentos');
    }
	if(isset($_FILES['ficheroFacturaFormacion']) && $_FILES['ficheroFacturaFormacion']['name']!=''){
		$_FILES['ficheroFactura']=$_FILES['ficheroFacturaFormacion'];
    	$res=$res && insertaDatos('documentosClientes',time(),'documentos');
    }
	if(isset($_FILES['ficheroFacturaMail']) && $_FILES['ficheroFacturaMail']['name']!=''){	
		$_FILES['ficheroFactura']=$_FILES['ficheroFacturaMail'];
    	$res=$res && insertaDatos('documentosClientes',time(),'documentos');
    }

	return $res;
}


function obtieneMailsClientes(){
	$res='';
	$mails=array();
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("SELECT mail FROM clientes WHERE codigo='".$datos['codigo'.$i]."';");
		$consulta=mysql_fetch_assoc($consulta);
		if(!in_array($consulta['mail'],$mails)){
			array_push($mails,$consulta['mail']);
			$res.=$consulta['mail'].', ';
		}
	}

	$res= substr_replace($res, '', strlen($res)-2, strlen($res));//Para quitar última coma

	cierraBD();

	return $res;
}

function enviaCorreos(){
	$res=true;

	$datos=arrayFormulario();
	$datos['mensajeCorreo']=stripcslashes($datos['mensajeCorreo']);
	
	$codigoU=$_SESSION['codigoS'];
	conexionBD();
	$consulta=consultaBD("SELECT email FROM usuarios WHERE codigo='$codigoU';");
	cierraBD();
	$consulta=mysql_fetch_assoc($consulta);

	$headers="From: ".$consulta['email']."\r\n";
	$headers.= "Bcc: ".$datos['ocultos']."\r\n";
	$headers.= "MIME-Version: 1.0\r\n";

	if(isset($_FILES['adjunto']) && $_FILES['adjunto']['name']!=""){
		$aleatorio=md5(time());
		$limiteMime="==TecniBoundary_x{$aleatorio}x";
		$headers.="Content-Type: multipart/mixed;" . "boundary=\"{$limiteMime}\"";

		$mensaje="--{$limiteMime}\r\n"."Content-Type: text/html; charset=\"utf-8\"\r\n"."Content-Transfer-Encoding: 7bit\n\n".$datos['mensajeCorreo']."\n\n";

		foreach($_FILES as $adjunto){//Hecho con foreach para incluir en una futura actualización varios adjuntos
			$fp=fopen($adjunto["tmp_name"], "r");
			$tam=filesize($adjunto["tmp_name"]);
			$fichero=fread($fp,$tam);
			$ficheroAdjunto=chunk_split(base64_encode($fichero));
			fclose($fp);
			
			$mensaje.="--{$limiteMime}\r\n";
			$mensaje.="Content-Type: application/octet-stream; name=\"".$adjunto['name']."\"\r\n"."Content-Description:".$adjunto['name']."\r\n"."Content-Disposition: attachment;filename=\"".$adjunto["name"]."\";size=".$tam."\r\n"."Content-Transfer-Encoding:base64\r\n\r\n".$ficheroAdjunto."\r\n\r\n";
		}

		$mensaje .= "--{$limiteMime}--";
	}
	else{
		$headers.= "Content-Type: text/html; charset=UTF-8";
		$mensaje=$datos['mensajeCorreo'];
	}
	
	if($datos['destinatarios']==''){
		$datos['destinatarios']='info@grupqualia.es';
	}

	if (!mail($datos['destinatarios'], $datos['asunto'], $mensaje ,$headers)){
		$res=false;
	}
	else{
		$fecha=date('Y')."-".date('m')."-".date('d');
		$hora=date('H').":".date('i').":00";

		conexionBD();
		consultaBD("INSERT INTO correos VALUES(NULL,'".$datos['destinatarios']."', '$fecha', '$hora', '".$datos['asunto']."', '".$datos['mensajeCorreo']."', '', '".$datos['ocultos']."', '$codigoU');");
		
		$id=mysql_insert_id();
		$nombreFichero=nombreFicheroAdjunto($_FILES['adjunto']['name'],$id);
		if (!move_uploaded_file($_FILES['adjunto']['tmp_name'], "adjuntosCorreos/$nombreFichero")){ 
			 $nombreFichero='';
		}

		consultaBD("UPDATE correos SET adjunto='$nombreFichero' WHERE codigo='$id';");
		cierraBD();
	}
	
	return $res;
}

function nombreFicheroAdjunto($nombreTemporal,$id){
	$arrayNombre=explode('.',$nombreTemporal);//Creo un array con el nombre y la extensión en un índice distinto
	$nombre=reset($arrayNombre);//Extrae el nombre del fichero.
	$nombre=str_replace(' ','_',$nombre);//Quita los espacios
	$extension=end($arrayNombre);//Extrae la extensión del fichero.
	$nombreFichero=$nombre.'_'.$id.'.'.$extension;//El fichero pasa a igual, solo que entre el nombre y la extensión está el id (para evitar sobrescrituras)

	return $nombreFichero;
}


function eliminaCliente(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM clientes WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
		}
	}

	cierraBD();

	return $res;
}


function nombreCliente($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT empresa FROM clientes WHERE codigo='$codigo';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);

	return $consulta['empresa'];
}


function eliminaVenta(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM ventas WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
		}
	}

	cierraBD();

	return $res;
}


function datosVenta($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM ventas WHERE codigo='$codigo';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}


function datosAlumnos($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre, apellidos, dni, mail, telefono FROM alumnos WHERE codigoVenta='$codigo';");
	cierraBD();

	return $consulta;
}


function actualizaVenta(){
	$res=true;

	$datos=arrayFormulario();
	
	

	$consulta=actualizaDatos('ventas');

	conexionBD();
	if(!$consulta){
		$res=false;
	}
	elseif($datos['concepto']=='14'){
		
		for($i=0;isset($datos["nombre$i"]) || isset($datos["codigoAlumno$i"]);$i++){
			if(isset($datos['codigoAlumno'.$i]) && isset($datos['nombre'.$i])){//Actualización
				$res=$res && consultaBD("UPDATE alumnos SET nombre='".$datos['nombre'.$i]."', apellidos='".$datos['apellidos'.$i]."',dni='".$datos['dni'.$i]."', mail='".$datos['mail'.$i]."', telefono='".$datos['tlf'.$i]."' WHERE codigo='".$datos['codigoAlumno'.$i]."';");
			}
			elseif(!isset($datos['codigoAlumno'.$i]) && isset($datos['nombre'.$i])){//Inserción
				$res= $res && consultaBD("INSERT INTO alumnos(codigo,nombre,apellidos,dni,mail,telefono,codigoVenta,codigoUsuario) VALUES(NULL,'".$datos['nombre'.$i]."','".$datos['apellidos'.$i]."','".$datos['dni'.$i]."','".$datos['mail'.$i]."','".$datos['tlf'.$i]."','".$datos['codigo']."','".$_SESSION['codigoS']."');");
			}
			else{//Eliminación
				$res=$res && consultaBD("DELETE FROM alumnos WHERE codigo='".$datos['codigoAlumno'.$i]."';");
			}
		}
	}
	elseif($datos['concepto']!='14'){
		$consulta=consultaBD('DELETE FROM ventas_relacionadas WHERE codigoVenta='.$datos['codigo'],true);
		$oferta=datosRegistro('ofertas',$datos['codigo'],'codigoOferta');
		$trabajo=datosRegistro('trabajos',$oferta['codigo'],'codigoOferta');
		if($datos['codigoVentaRelacionada'] == 'NULL'){
			$consulta=consultaBD('UPDATE trabajos SET formacion="NO" WHERE codigo='.$trabajo['codigo'],true);
		} else {
			$consulta=consultaBD("INSERT INTO ventas_relacionadas VALUES(NULL,".$datos['codigoVentaRelacionada'].",".$datos['codigo'].")",true);
			$alumno=datosRegistro('alumnos',$datos['codigoVentaRelacionada'],'codigoVenta');
			$alumno=datosRegistro('alumnos_registrados_cursos',$alumno['codigo'],'codigoAlumno');
			if($alumno['resolucion']=='SI'){
				$formacion='FINALIZADO';
			} else {
				$formacion='HECHO';
			}
			$consulta=consultaBD('UPDATE trabajos SET formacion="'.$formacion.'" WHERE codigo='.$trabajo['codigo'],true);
			conexionBD();
		}
	}
	
	cierraBD();

	return $res;
}

function destinoFormulario($codigoS){
	$seccion=array('inicio.php','posiblesClientes.php','cuentas.php','tareas.php','incidencias.php','formacion.php','alumnos.php','','usuarios.php','correos.php','','','','','facturacion.php');

	return $seccion[$codigoS];
}



function imprimeClientesTareas($datosCliente=false){
	//$codigoS=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhereTareas();
	if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
		$where='WHERE codigoUsuario="'.$_SESSION['codigoS'].'" OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'")';
	}

	conexionBD();

	$consulta=consultaBD("SELECT codigo, empresa, contacto, telefono, activo FROM clientes $where ORDER BY empresa, fechaVenta, fechaRegistro;");
	$datos=mysql_fetch_assoc($consulta);

	$sectores=array("HOSTELERIA"=>"Hostelería", "COMERCIO"=>"Comercio", "TRANSPORTE"=>"Transporte", "ESTETICA"=>"Estética", "SANIDAD"=>"Sanidad", "VETERINARIA"=>"Veterinaria", "DEPORTES"=>"Deportes", "ASOCIACIONES"=>"Asociaciones", "FORMACION"=>"Formación", "INDUSTRIA"=>"Industria", "CONSTRUCCION"=>"Construcción", "INFORMATICA"=>"Informática", "SERVICIOS"=>"Servicios");
	$tipo=array('SI'=>'Cliente','NO'=>'Posible cliente');
	while($datos!=0){
		echo "
		<tr>
        	<td> ".$datos['empresa']." </td>
        	<td> ".$tipo[$datos['activo']]." </td>
        	<td> ".$datos['contacto']." </td>
        	<td> ".formateaTelefono($datos['telefono'])." </td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'";
		if($datosCliente!=false && in_array($datos['codigo'],$datosCliente)){
			echo "checked='checked'";
		}

		echo "
				>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}



function registraTarea(){
	$res=true;

	$datos=arrayFormulario();
	if(!isset($datos['todoDia'])){
		$datos['todoDia']='NO';
	}
	else{
		$datos['horaInicio']='';
		$datos['horaFin']='';
	}

	conexionBD();
	foreach ($datos['codigoLista'] as $codigoCliente){ 
   		$consulta=consultaBD("INSERT INTO tareas VALUES(NULL,'".$datos['tarea']."', '".$datos['fechaInicio']."', '".$datos['fechaFin']."', '".$datos['horaInicio']."',
   			 '".$datos['horaFin']."', '".$datos['todoDia']."', '".$datos['estado']."', '".$datos['prioridad']."', '".$datos['observaciones']."', '$codigoCliente','".$_SESSION['codigoS']."');");
   		if(!$consulta){
   			$res=false;
   		}
	}
	cierraBD();

	return $res;
}


function imprimeTareas($inicio=false,$anio='FALSO',$condicionante='',$diaActual=false){
	if($anio=='FALSO'){
		$anio=date('Y');
	}
	//$codigoS=$_SESSION['codigoS'];
	if($condicionante!=''){
		$where="WHERE tareas.codigoUsuario='$condicionante'";
	}else{
		if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
			$where='WHERE 1=1 AND (tareas.codigoUsuario="'.$_SESSION['codigoS'].'" OR tareas.codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'"))';
		}else{
			$where=compruebaPerfilParaWhereTareas('tareas.codigoUsuario');
		}
	}

	conexionBD();
	($inicio) ? $where.=" AND tareas.estado!='REALIZADA'" : $where.=" AND tareas.estado='REALIZADA'";
	
	if($diaActual){
		$fecha=date('Y-m-d');
		$where.=" AND tareas.fechaInicio='$fecha'";
	}

	$condicionanteDos='';
	/*if($_SESSION['tipoUsuario']=='COMERCIAL'){
		$datosUsuario=consultaBD("SELECT * FROM usuarios WHERE codigo='".$_SESSION['codigoS']."'",false,true);
		$fecha=date('Y-m-d');
		$contador=consultaBD("SELECT COUNT(codigo) AS total FROM tareas WHERE codigoUsuario='".$_SESSION['codigoS']."' AND prioridad='proceso' AND estado!='realizada' AND fechaFin<'$fecha';",false,true);
		if($contador['total']>0 && $datosUsuario['director']=='NO'){
			$condicionanteDos="AND fechaInicio<='".date('Y-m-d')."'";
		}
	}*/
	$consulta=consultaBD("SELECT tareas.codigo, clientes.empresa, clientes.contacto, clientes.telefono, clientes.provincia, tareas.tarea, tareas.fechaInicio, tareas.estado, tareas.prioridad, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) AS usuarioAsignado, clientes.codigo AS codigoCliente, horaInicio, clientes.movil, tareas.pendiente, tareas.firmado, clientes.localidad 
		FROM clientes INNER JOIN tareas ON clientes.codigo=tareas.codigoCliente INNER JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo $where AND fechaInicio LIKE'%$anio-%' $condicionanteDos ORDER BY tareas.fechaInicio ASC;");
	$estado=array('pendiente'=>"<span class='label label-warning'>Pendiente</span>",'realizada'=>"<span class='label label-success'>Realizada</span>");
	
	$colores=array('pendiente'=>'','sincontactar'=>'','proceso'=>"class='registroAzul'",'reconcertar'=>"class='registroNegro'",'alta'=>"class='registroVerde'",
	'normal'=>"class='registroAmarillo'",'baja'=>"class='registroRojo'",'concertada'=>"class='registroVerdeClaro'",'visitado'=>"class='registroAzulClaro'");
	
	$prioridad=array('pendiente'=>"Pendiente",'normal'=>"Pendiente",'alta'=>"Vendido",'baja'=>"Baja",'sincontactar'=>"Sin contactar",
	'proceso'=>"En proceso",'reconcertar'=>"Reconcertar",'concertada'=>'Concertada','visitado'=>'Visitado');
	
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		$fecha=formateaFechaWeb($datos['fechaInicio']);
		$textoConcatenado='';
		if($datos['prioridad']=='normal'){
			$textoConcatenado=". Firmado: ".$datos['firmado'].". Pendiente de: ".$datos['pendiente'];
		}
		echo "
		<tr>
        	<td> <a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a>  </td>
			<td> ".$datos['usuarioAsignado']." </td>
        	<td> ".$datos['localidad']." </td>
        	<td> ".$datos['contacto']." </td>
        	<td class='nowrap'> ".formateaTelefono($datos['telefono'])." </td>
			<td class='nowrap'> ".formateaTelefono($datos['movil'])." </td>
        	<td> ".$datos['tarea']." </td>
        	<td> $fecha </td>
			<td> ".formateaHoraWeb($datos['horaInicio'])." </td>
        	<td> <div ".$colores[$datos['prioridad']].">".$prioridad[$datos['prioridad']].$textoConcatenado."</div> </td>";
        
        if(!$inicio){
        	echo "
        	<td class='centro'>
        		<a href='detallesTarea.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a>
			</td>";
		}
		else{
 			echo "
	        <td class='centro'>
	        	<a href='detallesTarea.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a>
	        	<a href='?realizaTarea=".$datos['codigo']."' class='btn btn-success'><i class='icon-ok-sign'></i> Realizada</i></a>
			</td>";
		}

		echo "
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}


function generaEventosCalendarioTareas($condicionante=''){
	//$codigoU=$_SESSION['codigoS'];
	
	if($condicionante!=''){
		$where="WHERE tareas.codigoUsuario='$condicionante'";
	}else{
		if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
			$where='WHERE 1=1 AND(tareas.codigoUsuario="'.$_SESSION['codigoS'].'" OR tareas.codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'"))';
		}else{
			$where=compruebaPerfilParaWhere('tareas.codigoUsuario');
		}
	}
	
	$condicionanteDos='';
	/*if($_SESSION['tipoUsuario']=='COMERCIAL'){
		$datosUsuario=datosRegistro('usuarios',$_SESSION['codigoS']);
		$fecha=date('Y-m-d');
		$contador=consultaBD("SELECT COUNT(codigo) AS total FROM tareas WHERE codigoUsuario='".$_SESSION['codigoS']."' AND prioridad='proceso' AND estado!='realizada' AND fechaFin<'$fecha';",true,true);
		if($contador['total']>0 && $datosUsuario['director']=='NO'){
			$condicionanteDos="AND fechaInicio<='".date('Y-m-d')."'";
		}
	}*/

	conexionBD();
	$anio=date('Y');
	$consulta=consultaBD("SELECT tareas.codigo, empresa, tarea, fechaInicio, fechaFin, horaInicio, horaFin, todoDia, prioridad, colorTareas, tareas.firmado, tareas.pendiente FROM tareas 
	INNER JOIN clientes ON tareas.codigoCliente=clientes.codigo 
	LEFT JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo $where AND tareas.estado='pendiente' AND fechaInicio > '2016-11-30' $condicionanteDos ORDER BY prioridad;");

	$datos=mysql_fetch_assoc($consulta);
	$tareas="";
	$clases=array('alta'=>"backgroundColor: '#098409', borderColor: '#055D05'",'normal'=>"backgroundColor: '#FACC2E', borderColor: '#B18904'",'baja'=>"backgroundColor: '#ec5b58', borderColor: '#b94a48'",
	'sincontactar'=>"backgroundColor: '#B2B2FF', borderColor: '#A3A3CA'",'proceso'=>"backgroundColor: '#0000FF', borderColor: '#0505DC'",
	'reconcertar'=>"backgroundColor: '#515161', borderColor: '#000000'", 'concertada'=>"backgroundColor: '#06E306', borderColor: '#1DAB1D'", 'visitado'=>"backgroundColor: '#9595FF', borderColor: '#8181EC'",'pendiente'=>"backgroundColor: '#B2B2FF', borderColor: '#A3A3CA'");
	while($datos!=false){
		$fechaInicio=explode('-',$datos['fechaInicio']);
		$fechaFin=explode('-',$datos['fechaFin']);
		$fechaInicio[1]--;
		$fechaFin[1]--;
		
		if($condicionante==''){
			$colores="backgroundColor: '".$datos['colorTareas']."', borderColor: '".$datos['colorTareas']."'";
		}else{
			$colores=$clases[$datos['prioridad']];
		}

		$tareas.="
			{
				id: ".$datos['codigo'].",
		    	title: '-".addslashes($datos['empresa']).": ".addslashes($datos['tarea'])."',";
		if($datos['todoDia']=='SI'){
			$tareas.="
			    start: new Date(".$fechaInicio[0].", ".$fechaInicio[1].", ".$fechaInicio[2]."),
		    	end: new Date(".$fechaFin[0].", ".$fechaFin[1].", ".$fechaFin[2]."),
		    	allDay: true,
		    	url: 'detallesTarea.php?codigo=".$datos['codigo']."',
		    	$colores
		    	
	    	},";
		}
		else{
			$horaInicio=explode(':',$datos['horaInicio']);
			$horaFin=explode(':',$datos['horaFin']);

		    $tareas.="
			    start: new Date(".$fechaInicio[0].", ".$fechaInicio[1].", ".$fechaInicio[2].", ".$horaInicio[0].", ".$horaInicio[1]."),
		    	end: new Date(".$fechaFin[0].", ".$fechaFin[1].", ".$fechaFin[2].", ".$horaFin[0].", ".$horaFin[1]."),
		    	allDay: false,
		    	url: 'detallesTarea.php?codigo=".$datos['codigo']."',
		    	$colores
	    	},";
		}

		$datos=mysql_fetch_assoc($consulta);
  	}
  	$tareas=substr_replace($tareas, '', strlen($tareas)-1, strlen($tareas));//Para quitar última coma
  	echo $tareas;
}

function formateaFechaVistaCalendario($timestamp,$bdd=false,$fechaFin=false){
	$fecha=new DateTime();
	$fecha=date_timestamp_set($fecha,$timestamp);
	
	if($bdd){
		$fecha=date_format($fecha,'Y-m-d');
		if($fechaFin){//Porque la fecha de fin transmitida por el calendario es 1 día más que el final
			$array=explode('-',$fecha);
			$array[2]--;
			$fecha=$array[0].'-'.$array[1].'-'.$array[2];
		}
	}

	return $fecha;
}


function generaDatosGraficoTareas($anio='',$condicionante=''){
	$datos=array();
	if($anio==''){
		$anio=date('Y');
	}
	//$codigoS=$_SESSION['codigoS'];
	if($condicionante!=''){
		$where="WHERE tareas.codigoUsuario='$condicionante'";
	}else{
		if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
			$where='WHERE 1=1 AND(tareas.codigoUsuario="'.$_SESSION['codigoS'].'" OR tareas.codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'"))';
		}else{
			$where=compruebaPerfilParaWhere('tareas.codigoUsuario');
		}
	}

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(tareas.codigo) AS codigo FROM tareas INNER JOIN clientes ON clientes.codigo=tareas.codigoCliente $where AND tareas.estado='pendiente' AND fechaInicio LIKE'$anio-%';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['pendientes']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(tareas.codigo) AS codigo FROM tareas INNER JOIN clientes ON clientes.codigo=tareas.codigoCliente $where AND tareas.estado='realizada' AND fechaInicio LIKE'$anio-%';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['realizadas']=$consulta['codigo'];

	cierraBD();

	return $datos;
}


function datosTarea($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT tareas.codigo AS codigo, tareas.descripcion, tareas.tarea, clientes.empresa, clientes.contacto, clientes.telefono, clientes.mail,
	 tareas.fechaInicio, tareas.fechaFin, tareas.horaInicio, tareas.horaFin, tareas.todoDia, tareas.estado, tareas.prioridad, tareas.horaRealizacion,
	 tareas.codigoUsuario, clientes.movil, clientes.direccion, colaboradores.empresa AS colaborador, tareas.codigoCliente, clientes.direccionVisita, tareas.firmado, tareas.direccionVisita AS donde, clientes.sector, tareas.importe, clientes.localidad
	 FROM tareas LEFT JOIN clientes ON tareas.codigoCliente=clientes.codigo LEFT JOIN colaboradores ON clientes.comercial=colaboradores.codigo WHERE tareas.codigo='$codigo';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}







function realizaTarea($codigo){
	$res=true;
	
	$hora=date('H:i:s');
	conexionBD();
	$consulta=consultaBD("UPDATE tareas SET estado='realizada', horaRealizacion='$hora' WHERE codigo='$codigo';");
	
	if(!$consulta){
		$res=false;
	}

	cierraBD();

	return $res;
}

function eliminaTarea(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM tareas WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
		}
	}

	cierraBD();

	return $res;
}


function selectClientes($codigo=false,$activo=''){
	//$codigoU=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	echo "<select name='codigoCliente' class='selectpicker show-tick' data-live-search='true'>";
	conexionBD();
	$consulta=consultaBD("SELECT codigo, empresa FROM clientes $where $activo;");
	cierraBD();
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=false){
		echo "<option value='".$datos['codigo']."'";

		if($codigo!=false && $codigo==$datos['codigo']){
			echo " selected='selected'";
		}

		echo ">".$datos['empresa']."</option>";
		$datos=mysql_fetch_assoc($consulta);
	}
	echo "</select>";
}



function registraIncidencia(){
	$res=true;

	$datos=arrayFormulario();
	
	if(isset($_SESSION["verifica"]) && $_SESSION["verifica"] == 1){
		conexionBD();

		$consulta=consultaBD("INSERT INTO incidencias VALUES(NULL, '".$datos['descripcion']."', '".$datos['departamento']."', '".$datos['prioridad']."',
			'".$datos['estado']."', '".$datos['tipoIncidencia']."', '".$datos['codigoCliente']."','".$_SESSION['codigoS']."');");

		if(!$consulta){
			$res=false;
		}
		
		$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM incidencias WHERE estado='PENDIENTE';");
	  $datosConsulta=mysql_fetch_assoc($consulta);
	  if($datosConsulta['codigo']>0){
		$_SESSION['numIncidencias']=$datosConsulta['codigo'];
	  }else{
		$_SESSION['numIncidencias']=0;
	  }
		
		cierraBD();
		unset($_SESSION['verifica']);
	}else{
		$res=false;
	}

	return $res;	
}


function imprimeIncidencias($inicio=false){
	//$codigoS=$_SESSION['codigoS'];
	$where="WHERE 1=1";
	if($_SESSION['tipoUsuario']!='MARKETING'){
		$where=compruebaPerfilParaWhere('incidencias.codigoUsuario');
	}

	conexionBD();
	if($inicio){
		$where.=" AND incidencias.estado!='RESUELTA'";
	}
	else{
		$where.="";
	}

	$consulta=consultaBD("SELECT incidencias.codigo, clientes.empresa, clientes.contacto, clientes.telefono, incidencias.departamento,
	 incidencias.estado, incidencias.prioridad, clientes.codigo AS codigoCliente, incidencias.tipoIncidencia FROM clientes INNER JOIN incidencias ON clientes.codigo=incidencias.codigoCliente $where ORDER BY incidencias.prioridad;");
	$prioridad=array('NORMAL'=>"<span class='label label-info'>Normal</span>",'ALTA'=>"<span class='label label-danger'>Alta</span>",'BAJA'=>"<span class='label'>Baja</span>");
	$estado=array('PENDIENTE'=>"<span class='label label-danger'>Pendiente</span>",'RESUELTA'=>"<span class='label label-success'>Resuelta</span>",'ENPROCESO'=>"<span class='label label-warning'>En proceso</span>");
	$departamento=array('ADMINISTRACION'=>'Administración','TUTORES'=>'Tutores','COMERCIAL'=>'Comercial','IMPLANTACION'=>'Implantación');
	$tipo=array('Incidencia'=>'Incidencia','Falta'=>'Falta de datos');
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		echo "
		<tr>
        	<td> <a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a>  </td>
			<td> ".$tipo[$datos['tipoIncidencia']]." </td>
        	<td> ".$datos['contacto']." </td>
        	<td> ".formateaTelefono($datos['telefono'])." </td>
        	<td> ".$departamento[$datos['departamento']]." </td>
        	<td> ".$prioridad[$datos['prioridad']]." </td>
        	<td> ".$estado[$datos['estado']]." </td>
        	<td class='centro'>
        		<a href='detallesIncidencia.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}


function generaDatosGraficoIndicencias(){
	$datos=array();
	//$codigoS=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='MARKETING'){
		$where=compruebaPerfilParaWhere('incidencias.codigoUsuario');
	}

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(incidencias.codigo) AS codigo FROM incidencias INNER JOIN clientes ON incidencias.codigoCliente=clientes.codigo $where AND incidencias.estado='PENDIENTE';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['pendientes']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(incidencias.codigo) AS codigo FROM incidencias INNER JOIN clientes ON incidencias.codigoCliente=clientes.codigo $where AND incidencias.estado='RESUELTA';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['resueltas']=$consulta['codigo'];


	$consulta=consultaBD("SELECT COUNT(incidencias.codigo) AS codigo FROM incidencias INNER JOIN clientes ON incidencias.codigoCliente=clientes.codigo $where AND incidencias.estado='ENPROCESO';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['enproceso']=$consulta['codigo'];

	cierraBD();

	return $datos;
}



function datosIncidencias($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM incidencias WHERE codigo='$codigo';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}


function actualizaIncidencia(){
	$res=true;

	$datos=arrayFormulario();
	
	$consulta=actualizaDatos('incidencias');
	conexionBD();
	if(!$consulta){
		$res=false;
	}
	
  $consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM incidencias WHERE estado='PENDIENTE';");
  $datosConsulta=mysql_fetch_assoc($consulta);
  if($datosConsulta['codigo']>0){
	$_SESSION['numIncidencias']=$datosConsulta['codigo'];
  }else{
	$_SESSION['numIncidencias']=0;
  }
  
  if($datos['estado']=='RESUELTA'){
	avisaIncidencia($datos);
  }
	
	cierraBD();

	return $res;
}

function avisaIncidencia($datos){
	$headers="From: programacion@qmaconsultores.com\r\n";
	$headers.= "MIME-Version: 1.0\r\n";
	$headers.= "Content-Type: text/html; charset=UTF-8";

	$mail_destinatario=$datos['emailResponsable'];

	$tipos=array('nc'=>'No Conformidad','incidencia'=>'Incidencia');
	$tipo=$tipos[$datos['tipo']];
	
	$cliente=datosRegistro('clientes',$datos['codigoCliente']);

	$mensaje="Se ha cerrado la incidencia con la siguiente descripción: <br><br><i><b>".$datos['descripcion']."</b></i><br><br> Cliente: <i><b>".$cliente['empresa']."</b></i>";
	
	mail($mail_destinatario, "Grupo Qualia - Resolución de incidencia",$mensaje,$headers);
}

function eliminaIncidencia(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM incidencias WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
		}
	}
	
	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM incidencias WHERE estado='PENDIENTE';");
  $datosConsulta=mysql_fetch_assoc($consulta);
  if($datosConsulta['codigo']>0){
	$_SESSION['numIncidencias']=$datosConsulta['codigo'];
  }else{
	$_SESSION['numIncidencias']=0;
  }

	cierraBD();

	return $res;
}

function obtieneMailsClientesTareas(){
	$res='';
	$mails=array();
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("SELECT clientes.mail FROM clientes INNER JOIN tareas ON clientes.codigo=tareas.codigoCliente WHERE tareas.codigo='".$datos['codigo'.$i]."';");
		$consulta=mysql_fetch_assoc($consulta);
		if(!in_array($consulta['mail'],$mails)){
			array_push($mails,$consulta['mail']);
			$res.=$consulta['mail'].', ';
		}
	}

	$res= substr_replace($res, '', strlen($res)-2, strlen($res));//Para quitar última coma

	cierraBD();

	return $res;
}


function obtieneMailsClientesIncidencias(){
	$res='';
	$mails=array();
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("SELECT clientes.mail FROM clientes INNER JOIN incidencias ON clientes.codigo=incidencias.codigoCliente WHERE incidencias.codigo='".$datos['codigo'.$i]."';");
		$consulta=mysql_fetch_assoc($consulta);
		if(!in_array($consulta['mail'],$mails)){
			array_push($mails,$consulta['mail']);
			$res.=$consulta['mail'].', ';
		}
	}

	$res= substr_replace($res, '', strlen($res)-2, strlen($res));//Para quitar última coma

	cierraBD();

	return $res;
}


function imprimeUsuarios(){
	conexionBD();

	$consulta=consultaBD("SELECT codigo, nombre, apellidos, telefono, email, usuario, clave, tipo, director FROM usuarios ORDER BY apellidos, nombre;");
	$datos=mysql_fetch_assoc($consulta);
	$director=array('SI'=>'Director ','NO'=>'');
	$tipo=array('ADMIN'=>'Administrador','COMERCIAL'=>'Comercial','ADMINISTRACION'=>'Administración','CONSULTORIA'=>'Consultor','FORMACION'=>'Formación','ATENCION'=>'Atención al Cliente','TELECONCERTADOR'=>'Teleconcertador','MARKETING'=>'Marketing','SUPERVISOR'=>'Supervisor');

	while($datos!=0){
		$usuario=$datos['usuario'];
		$clave=$datos['clave'];
		if($datos['tipo']=='ADMIN' || $datos['tipo']=='ATENCION' || $datos['tipo']=='MARKETING'){
			if($_SESSION['codigoS']!=19 && $_SESSION['codigoS']!=21){
				$usuario='**********';
				$clave='**********';
			}
		} 
		echo "
		<tr>
			<td> ".$datos['apellidos'].", ".$datos['nombre']." </td>
			<td> ".formateaTelefono($datos['telefono'])." </td>
			<td> ".$datos['email']." </td>
        	<td> ".$usuario." </td>
        	<td> ".$clave." </td>
        	<td> ";
			if($datos['tipo']!='ADMIN' && $datos['tipo']!='ADMINISTRACION'){
				echo $director[$datos['director']].$tipo[$datos['tipo']];
			}else{
				echo $tipo[$datos['tipo']];
			}			
			echo "</td>
        	<td class='centro'>
        		<a href='detallesUsuario.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-edit'></i> Modificar</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}


function registraUsuario(){
	$res=true;

	$datos=arrayFormulario();
	
	if($datos['director']=='SI'){
		$datos['directorAsociado']='NULL';
	}

	if(isset($_SESSION["verifica"]) && $_SESSION["verifica"] == 1){
		conexionBD();
		$consulta=consultaBD("INSERT INTO usuarios VALUES(NULL,'".$datos['nombre']."', '".$datos['apellidos']."', '".$datos['dni']."', '".$datos['email']."', 
			'".$datos['telefono']."', '".$datos['usuario']."', '".$datos['clave']."', '".$datos['tipo']."', '','".$datos['director']."','".$datos['habilitado']."','".$datos['colorTareas']."',
			'".$datos['tipoProfesional']."', '".$datos['objetivo']."', '".$datos['porcentajeNueva']."', '".$datos['porcentajeCartera']."', '".$datos['porcentajeAuditoria']."', '".$datos['porcentajeTeleventa']."', '".$datos['activoUsuario']."', ".$datos['directorAsociado'].",".$datos['teleconcertador'].",'');");
		$codigoUsuario=mysql_insert_id();
		
		foreach($datos['teleconcertadores'] as $teleconcertador){ 
			$consulta=consultaBD("INSERT INTO usuarios_teleconcertadores VALUES('$codigoUsuario', '$teleconcertador');");
			if(!$consulta){
				$res=false;
			}
		}
		
		cierraBD();

		if(!$consulta){
			$res=false;
		}
		unset($_SESSION['verifica']);
	}else{
		$res=false;
	}

	return $res;
}


function datosUsuario($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM usuarios WHERE codigo='$codigo';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}


function actualizaUsuario(){
	$res=true;

	$datos=arrayFormulario();

	if($datos['director']=='SI'){
		$datos['directorAsociado']='NULL';
	}
	
	conexionBD();
	$consulta=actualizaDatos('usuarios');
	
	$codigoUsuario=$datos['codigo'];
	$consulta=consultaBD("DELETE FROM usuarios_teleconcertadores WHERE codigoUsuario='$codigoUsuario';");
	foreach($datos['teleconcertadores'] as $teleconcertador){ 
		$consulta=consultaBD("INSERT INTO usuarios_teleconcertadores VALUES('$codigoUsuario', '$teleconcertador');");
		if(!$consulta){
			$res=false;
		}
	}	
		
	cierraBD();

	if(!$consulta){
		$res=false;
	}

	return $res;
}



function eliminaUsuario(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM usuarios WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
			echo mysql_error();
		}
	}

	cierraBD();

	return $res;
}


function generaDatosGraficoAccesos(){
	$datos=array();

	conexionBD();
	//La siguiente consulta extrae la hora del campo fecha (que es de tipo DATETIME) junto con el número de ocurrencias de esa hora (por el GROUP BY) para los registros que tienen como fecha hoy.
	$consulta=consultaBD("SELECT HOUR(fecha) AS etiqueta, COUNT(HOUR(fecha)) AS accesos FROM accesos WHERE DATE(fecha)=CURDATE() GROUP BY HOUR(fecha) ORDER BY HOUR(fecha);");
	cierraBD();

	$reg=mysql_fetch_assoc($consulta);
	$datos['etiquetas']='["'.($reg['etiqueta']-1).':00", ';//Creo una primera etiqueta con la hora anterior a la de la primera visita, así si solo ha habido una visita el gráfico se muestra bien
	if($datos['etiquetas']=='[-1:00, '){//FIX 14/05/2014: si la primera hora de acceso es las 00:00, al quitarle -1 marcan las -1:00.
		$datos['etiquetas']='[23:00, ';
	}//Fin FIX 14/05/2014

	$datos['accesos']='[0,';//Valor de primera hora
	$datos['max']=0;

	while($reg!=false){
		$datos['etiquetas'].='"'.$reg['etiqueta'].':00", ';
		$datos['accesos'].=$reg['accesos'].', ';
		if($datos['max']<$reg['accesos']){
			$datos['max']=$reg['accesos'];
		}
		$reg=mysql_fetch_assoc($consulta);
	}

	$datos['etiquetas']= substr_replace($datos['etiquetas'], '', strlen($datos['etiquetas'])-2, strlen($datos['etiquetas']));//Para quitar última coma
	$datos['accesos']= substr_replace($datos['accesos'], '', strlen($datos['accesos'])-2, strlen($datos['accesos']));//Para quitar última coma
	$datos['etiquetas'].=']';
	$datos['accesos'].=']';

	return $datos;
}


function registraAccionFormativa(){
	$res=true;

	$codigoU=$_SESSION['codigoS'];
	$datos=arrayFormulario();
	
	conexionBD();

	if(isset($_SESSION["verifica"]) && $_SESSION["verifica"] == 1){ 
		unset($_SESSION['verifica']);
		$consulta=consultaBD("INSERT INTO accionFormativa VALUES(NULL, '".$datos['denominacion']."', '".$datos['accion']."', '".$datos['grupo']."',
			'".$datos['horas']."', '".$datos['modalidad']."', '$codigoU');");

		if(!$consulta){
			$res=false;
		}
	}else{
		$res=false;
	}
	
	cierraBD();

	return $res;
}


function registraTutor(){
	$res=true;

	$codigoU=$_SESSION['codigoS'];
	$datos=arrayFormulario();
	
	$lunes=compruebaExistencia($datos, 'lunes');
	$martes=compruebaExistencia($datos, 'martes');
	$miercoles=compruebaExistencia($datos, 'miercoles');
	$jueves=compruebaExistencia($datos, 'jueves');
	$viernes=compruebaExistencia($datos, 'viernes');
	$sabado=compruebaExistencia($datos, 'sabado');
	$domingo=compruebaExistencia($datos, 'domingo');
	
	conexionBD();

	if(isset($_SESSION["verifica"]) && $_SESSION["verifica"] == 1){
		unset($_SESSION['verifica']);
		$consulta=consultaBD("INSERT INTO tutores VALUES(NULL, '".$datos['nombre']."', '".$datos['apellidos']."', '".$datos['dni']."',
			'".$datos['telefono']."', '".$datos['email']."', '".$datos['especialidad']."', '$lunes', '$martes', '$miercoles', '$jueves', '$viernes', '$sabado', '$domingo',
			'".$datos['horaInicio']."', '".$datos['horaFin']."', '".$datos['horaInicioTarde']."', '".$datos['horaFinTarde']."', '".$datos['horasTutoria']."', '$codigoU');");

		if(!$consulta){
			$res=false;
		}
	}else{
		$res=false;
	}
	
	cierraBD();

	return $res;
}


function selectaccionFormativa($codigo=false){
	//$codigoU=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	echo "<select id='accionFormativa' name='accionFormativa' class='selectpicker show-tick anchoAuto' data-live-search='true'>";
	conexionBD();
	$consulta=consultaBD("SELECT codigo, codigoInterno, denominacion, modalidad, horas FROM accionFormativa $where ORDER BY denominacion;");
	cierraBD();
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=false){
		echo "<option value='".$datos['codigoInterno']."'";

		if($codigo!=false && $codigo==$datos['codigo']){
			echo " selected='selected'";
		}

		$tipo=array("PRESENCIAL"=>"Presencial", "DISTANCIA"=>"A distancia", "MIXTA"=>"Mixta", "TELE"=>"Teleformación", "WEBINAR"=>"Webinar");
		echo ">".$datos['codigoInterno']." - ".$datos['denominacion']." (".$tipo[$datos['modalidad']].")</option>";
		$datos=mysql_fetch_assoc($consulta);
	}
	echo "</select>";
}


function selectTutor($codigo=false,$nombre='tutor'){
	//$codigoU=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	echo "<select name='$nombre' class='selectpicker show-tick anchoAuto' data-live-search='true'>";
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre, apellidos FROM tutores ORDER BY apellidos, nombre;");
	cierraBD();
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=false){
		echo "<option value='".$datos['codigo']."'";

		if($codigo!=false && $codigo==$datos['codigo']){
			echo " selected='selected'";
		}

		echo ">".$datos['apellidos'].", ".$datos['nombre']."</option>";
		$datos=mysql_fetch_assoc($consulta);
	}
	echo "</select>";
}



function imprimeAlumnosCreaCurso($datosAlumnos=false){
	//$codigoS=$_SESSION['codigoS'];

	conexionBD();

	$consulta=consultaBD("SELECT codigo, nombre, apellidos, dni, mail, telefono, codigoVenta FROM alumnos ORDER BY nombre;");
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		$consulta2=consultaBD("SELECT ventas.codigoCliente, clientes.empresa FROM (alumnos INNER JOIN ventas ON alumnos.codigoVenta=ventas.codigo) INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo WHERE alumnos.codigoVenta='".$datos['codigoVenta']."';");
		$datos2=mysql_fetch_assoc($consulta2);
		echo "
		<tr>
        	<td> ".$datos['apellidos'].", ".$datos['nombre']." </td>
        	<td> ".$datos['dni']." </td>
        	<td> ".$datos['mail']." </td>
			<td> ".$datos['telefono']." </td>
			<td> ".$datos2['empresa']." </td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'";
			if($datosAlumnos!=false && in_array($datos['codigo'],$datosAlumnos)){
				echo "checked='checked'";
			}

			echo ">
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function compruebaExistencia($datos, $texto){
	$res="NO";
	if(isset($datos[$texto])){
		$res="SI";
	}else{
		$res="NO";
	}
	return $res;
}

function registraCurso(){
	$res=true;

	$codigoU=$_SESSION['codigoS'];
	$datos=arrayFormulario();
	
	$mediosPropios=compruebaExistencia($datos, 'mediosPropios');
	$mediosEntidad=compruebaExistencia($datos, 'mediosEntidad');
	$mediosCentro=compruebaExistencia($datos, 'mediosCentro');

	$lunes=compruebaExistencia($datos, 'lunes');
	$martes=compruebaExistencia($datos, 'martes');
	$miercoles=compruebaExistencia($datos, 'miercoles');
	$jueves=compruebaExistencia($datos, 'jueves');
	$viernes=compruebaExistencia($datos, 'viernes');
	$sabado=compruebaExistencia($datos, 'sabado');
	$domingo=compruebaExistencia($datos, 'domingo');
	
	$lunesFormacion=compruebaExistencia($datos, 'lunesFormacion');
	$martesFormacion=compruebaExistencia($datos, 'martesFormacion');
	$miercolesFormacion=compruebaExistencia($datos, 'miercolesFormacion');
	$juevesFormacion=compruebaExistencia($datos, 'juevesFormacion');
	$viernesFormacion=compruebaExistencia($datos, 'viernesFormacion');
	$sabadoFormacion=compruebaExistencia($datos, 'sabadoFormacion');
	$domingoFormacion=compruebaExistencia($datos, 'domingoFormacion');
	
	$lunesFormacionDistancia=compruebaExistencia($datos, 'lunesFormacionDistancia');
	$martesFormacionDistancia=compruebaExistencia($datos, 'martesFormacionDistancia');
	$miercolesFormacionDistancia=compruebaExistencia($datos, 'miercolesFormacionDistancia');
	$juevesFormacionDistancia=compruebaExistencia($datos, 'juevesFormacionDistancia');
	$viernesFormacionDistancia=compruebaExistencia($datos, 'viernesFormacionDistancia');
	$sabadoFormacionDistancia=compruebaExistencia($datos, 'sabadoFormacionDistancia');
	$domingoFormacionDistancia=compruebaExistencia($datos, 'domingoFormacionDistancia');
	
	conexionBD();

	if(isset($_SESSION["verifica"]) && $_SESSION["verifica"] == 1){
		unset($_SESSION['verifica']);
		if($datos['tipoFormacion']=='PRESENCIAL'||$datos['tipoFormacion']=='TELEFORMA'||$datos['tipoFormacion']=='WEBINAR'){
				$consulta=consultaBD("INSERT INTO cursos VALUES(NULL, '".$datos['accionFormativa']."', '".$datos['codigoInterno']."', '".$datos['tutor']."', '".$datos['fechaInicio']."',
			'".$datos['fechaFin']."', '$mediosPropios', '$mediosCentro', '$mediosEntidad', '".$datos['horaInicio']."', '".$datos['horaFin']."', '".$datos['horaInicioTarde']."', 
			'".$datos['horaFinTarde']."', '".$datos['horasTutoria']."', '".$lunes."', '".$martes."', '".$miercoles."', '".$jueves."', '".$viernes."', 
			'".$sabado."', '".$domingo."', '".$datos['cif']."', '".$datos['centro']."', '".$datos['tlf']."', '".$datos['domicilio']."', '".$datos['cp']."', 
			'".$datos['poblacion']."', '".$datos['titularidad']."', '".$datos['informarlt']."', '".$datos['informerlt']."', '".$datos['fechaDiscrepancia']."',
			'".$datos['resuelto']."', '".$datos['cifTutoria']."', '".$datos['centroTutoria']."', '".$datos['tlfTutoria']."', '".$datos['domicilioTutoria']."', '".$datos['cpTutoria']."',
			'".$datos['poblacionTutoria']."', '".$datos['horaInicioFormacion']."', '".$datos['horaFinFormacion']."', '".$datos['horaInicioFormacionTarde']."', '".$datos['horaFinFormacionTarde']."',
			'".$datos['horasFormacion']."', '$lunesFormacion', '$martesFormacion', '$miercolesFormacion', '$juevesFormacion', 
			'$viernesFormacion', '$sabadoFormacion', '$domingoFormacion', null, '', '', '', '', '', '', '', '', '', '', '', '', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO',
			'".$datos['responsable']."', '".$datos['tlfResponsable']."','".$datos['llamadaBienvenida']."','".$datos['llamadaSeguimiento']."','".$datos['llamadaFinalizacion']."',
			'".$datos['bonificado']."','".$datos['plataforma']."','".$datos['finalizado']."','".$datos['medios']."', '".$datos['formacionCurso']."', '".$datos['comercial']."');");
		}elseif($datos['tipoFormacion']=='DISTANCIA'){
				$consulta=consultaBD("INSERT INTO cursos VALUES(NULL, '".$datos['accionFormativa']."', '".$datos['codigoInterno']."', '".$datos['tutor']."', '".$datos['fechaInicio']."',
			'".$datos['fechaFin']."','$mediosPropios', '$mediosCentro', '$mediosEntidad', '".$datos['horaInicio']."', '".$datos['horaFin']."', '".$datos['horaInicioTarde']."', 
			'".$datos['horaFinTarde']."', '".$datos['horasTutoria']."', '".$lunes."', '".$martes."', '".$miercoles."', '".$jueves."', '".$viernes."', 
			'".$sabado."', '".$domingo."', '', '', '', '', '', 
			'', '', '".$datos['informarlt']."', '".$datos['informerlt']."', '".$datos['fechaDiscrepancia']."', 
			'".$datos['resuelto']."', '".$datos['cifTutoria']."', '".$datos['centroTutoria']."', '".$datos['tlfTutoria']."', '".$datos['domicilioTutoria']."', '".$datos['cpTutoria']."',
			'".$datos['poblacionTutoria']."', '', '', '', '',
			'', 'NO', 'NO', 'NO', 'NO', 
			'NO', 'NO', 'NO', '".$datos['tutorDistancia']."', '".$datos['cifDistancia']."', '".$datos['centroGestorDistancia']."', 
			'".$datos['tlfDistancia']."', '".$datos['domicilioDistancia']."', '".$datos['cpDistancia']."', '".$datos['poblacionDistancia']."', '".$datos['titularidadDistancia']."',
			'".$datos['horaInicioFormacionDistancia']."', '".$datos['horaFinFormacionDistancia']."', '".$datos['horaInicioFormacionTardeDistancia']."', '".$datos['horaFinFormacionTardeDistancia']."',
			'".$datos['horasFormacionDistancia']."', '$lunesFormacionDistancia', '$martesFormacionDistancia', '$miercolesFormacionDistancia', '$juevesFormacionDistancia',
			'$viernesFormacionDistancia','$sabadoFormacionDistancia', '$domingoFormacionDistancia', '".$datos['responsable']."', '".$datos['tlfResponsable']."', '".$datos['llamadaBienvenida']."',
			'".$datos['llamadaSeguimiento']."','".$datos['llamadaFinalizacion']."','".$datos['bonificado']."','".$datos['plataforma']."', '".$datos['finalizado']."', '".$datos['medios']."', '".$datos['formacionCurso']."', '".$datos['comercial']."');");
		}elseif($datos['tipoFormacion']=='MIXTA'){
				$consulta=consultaBD("INSERT INTO cursos VALUES(NULL, '".$datos['accionFormativa']."', '".$datos['codigoInterno']."', '".$datos['tutor']."', '".$datos['fechaInicio']."',
			'".$datos['fechaFin']."','$mediosPropios', '$mediosCentro', '$mediosEntidad', '".$datos['horaInicio']."', '".$datos['horaFin']."', '".$datos['horaInicioTarde']."', 
			'".$datos['horaFinTarde']."', '".$datos['horasTutoria']."', '".$lunes."', '".$martes."', '".$miercoles."', '".$jueves."', '".$viernes."', 
			'".$sabado."', '".$domingo."', '".$datos['cif']."', '".$datos['centro']."', '".$datos['tlf']."', '".$datos['domicilio']."', '".$datos['cp']."', 
			'".$datos['poblacion']."', '".$datos['titularidad']."', '".$datos['informarlt']."', '".$datos['informerlt']."', '".$datos['fechaDiscrepancia']."',
			'".$datos['resuelto']."', '".$datos['cifTutoria']."', '".$datos['centroTutoria']."', '".$datos['tlfTutoria']."', '".$datos['domicilioTutoria']."', '".$datos['cpTutoria']."',
			'".$datos['poblacionTutoria']."', '".$datos['horaInicioFormacion']."', '".$datos['horaFinFormacion']."', '".$datos['horaInicioFormacionTarde']."', '".$datos['horaFinFormacionTarde']."',
			'".$datos['horasFormacion']."', '$lunesFormacion', '$martesFormacion', '$miercolesFormacion', '$juevesFormacion', 
			'$viernesFormacion', '$sabadoFormacion', '$domingoFormacion', '".$datos['tutorDistancia']."', '".$datos['cifDistancia']."', '".$datos['centroGestorDistancia']."', 
			'".$datos['tlfDistancia']."', '".$datos['domicilioDistancia']."', '".$datos['cpDistancia']."', '".$datos['poblacionDistancia']."', '".$datos['titularidadDistancia']."',
			'".$datos['horaInicioFormacionDistancia']."', '".$datos['horaFinFormacionDistancia']."', '".$datos['horaInicioFormacionTardeDistancia']."', '".$datos['horaFinFormacionTardeDistancia']."',
			'".$datos['horasFormacionDistancia']."', '$lunesFormacionDistancia', '$martesFormacionDistancia', '$miercolesFormacionDistancia', '$juevesFormacionDistancia',
			'$viernesFormacionDistancia','$sabadoFormacionDistancia', '$domingoFormacionDistancia', '".$datos['responsable']."', '".$datos['tlfResponsable']."','".$datos['llamadaBienvenida']."',
			'".$datos['llamadaSeguimiento']."','".$datos['llamadaFinalizacion']."','".$datos['bonificado']."', '".$datos['plataforma']."', '".$datos['finalizado']."', '".$datos['medios']."', '".$datos['formacionCurso']."', '".$datos['comercial']."');");
		}
	}

	
	$codigoCurso=mysql_insert_id();
	
	if(!$consulta){
		$res=false;
		echo mysql_error();
	}
	else{
		$alumnos=explode(',',$datos['listadoAlumnos']);
		foreach ($alumnos as $codigoAlumno){ 
	   		$consulta=consultaBD("INSERT INTO alumnos_registrados_cursos VALUES('$codigoCurso', '$codigoAlumno', 'NO','','', 'NO', 'NO','NO');");		
			$consulta=consultaBD("SELECT clientes.codigo FROM alumnos INNER JOIN ventas ON ventas.codigo=alumnos.codigoVenta INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo WHERE alumnos.codigo='$codigoAlumno';");
			$datosConsulta=mysql_fetch_assoc($consulta);
			if(isset($datosConsulta['codigo'])){
				$anterior=consultaBD("SELECT * FROM costes WHERE codigoAlumno='".$datosConsulta['codigo']."' AND codigoCurso='$codigoCurso';",false,true);
				if(!isset($anterior['codigo'])){
					$consulta=consultaBD("INSERT INTO accionformativa_cliente VALUES('".$datos['accionFormativa']."', '".$datosConsulta['codigo']."');");			
					$consulta=consultaBD("INSERT INTO costes VALUES(NULL, '".$datosConsulta['codigo']."', '$codigoCurso', '0','0','0','0','0');");
					$codigoCoste=mysql_insert_id();
					$consulta=consultaBD("INSERT INTO periodos_costes VALUES(NULL, '', '', '$codigoCoste');");
				}
			}
	   		if(!$consulta){
	   			$res=false;
	   		}
		}
	}
	
	/*$consulta2=consultaBD("SELECT clientes.empresa, clientes.codigo FROM ((clientes INNER JOIN ventas ON ventas.codigoCliente=clientes.codigo) 
	INNER JOIN alumnos ON alumnos.codigoVenta=ventas.codigo) 
	INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno 
	WHERE alumnos_registrados_cursos.codigoCurso=".$codigoCurso." GROUP BY clientes.codigo;");
	$datos2=mysql_fetch_assoc($consulta2);
	
	while($datos2!=false){
		
		$consulta3=consultaBD("INSERT INTO costes VALUES(null,".$datos2['codigo'].", $codigoCurso, 0, 0, 0);");
		
		$datos2=mysql_fetch_assoc($consulta2);
	}*/
	
	cierraBD();

	return $res;
}


function imprimeCursos(){
	$codigoU=$_SESSION['codigoS'];
	
	$where='';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION'){
		$where=compruebaPerfilParaWhere(true);
	}
	
	if($_SESSION['tipoUsuario']=='COMERCIAL'){
		$where="WHERE cursos.codigo IN(SELECT codigoCurso FROM alumnos_registrados_cursos WHERE codigoAlumno IN(
		SELECT codigo FROM alumnos WHERE codigoVenta IN(SELECT codigo FROM ventas WHERE codigoCliente IN(
		SELECT codigo FROM clientes WHERE codigoUsuario='$codigoU' OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador='$codigoU')))))";
	}

	conexionBD();

	$consulta=consultaBD("SELECT cursos.codigo AS codigo, denominacion, nombre, apellidos, fechaInicio, fechaFin, accionFormativa.codigoInterno AS codigoAccion, 
	cursos.codigoInterno AS grupo FROM (accionFormativa INNER JOIN cursos ON accionFormativa.codigoInterno=cursos.codigoAccionFormativa) 
	INNER JOIN tutores ON cursos.codigoTutor=tutores.codigo $where ORDER BY denominacion, apellidos;");

	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		$fechaInicio=formateaFechaWeb($datos['fechaInicio']);
		$fechaFin=formateaFechaWeb($datos['fechaFin']);

		echo "
		<tr>
			<td> ".$datos['codigoAccion']." </td>
			<td> ".$datos['grupo']." </td>
        	<td> ".$datos['denominacion']." </td>
        	<td> ".$datos['apellidos'].", ".$datos['nombre']."</td>
        	<td> $fechaInicio </td>
        	<td> $fechaFin </td>
        	<td class='centro'>";
				if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='CONSULTORIA'){
					echo "<a href='detallesCurso.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
					<a href='notificaciones.php?codigo=".$datos['codigo']."' class='btn btn-warning'><i class='icon-bell-alt'></i> Notificaciones</i></a>
					<a href='costes.php?codigo=".$datos['codigo']."' class='btn btn-info'><i class='icon-euro'></i> Costes</i></a>";
				}
        		echo "
				<!--a href='generaXMLParticipantes.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-download'></i> XML Participantes</i></a-->
				<a href='generaDocumentacion.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-download'></i> Documentación</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}



function datosCurso($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT cursos.*, accionFormativa.codigoInterno as codigoAccionFormativa, denominacion, accionFormativa.horas, modalidad, nombre, apellidos, email, telefono, tutores.codigo AS codigoTutor
	FROM (accionFormativa INNER JOIN cursos ON accionFormativa.codigoInterno=cursos.codigoaccionFormativa) INNER JOIN tutores ON cursos.codigoTutor=tutores.codigo WHERE cursos.codigo='$codigo';");
	cierraBD();
	$consulta=mysql_fetch_assoc($consulta);
	
	return $consulta;
}

function datosAlumnosCurso($codigo){
	$datos=array();

	conexionBD();
	$consulta=consultaBD("SELECT codigoAlumno FROM alumnos_registrados_cursos INNER JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo WHERE codigoCurso='$codigo';");
	cierraBD();

	$reg=mysql_fetch_assoc($consulta);
	while($reg!=false){//Meto los códigos en un array para usarlo como tal en la función imprimeAlumnosCreaCurso
		array_push($datos,$reg['codigoAlumno']);
		$reg=mysql_fetch_assoc($consulta);
	}

	return $datos;
}


function actualizaCurso(){
	$res=true;

	$datos=arrayFormulario();
	
	conexionBD();
	
	$mediosPropios=compruebaExistencia($datos, 'mediosPropios');
	$mediosEntidad=compruebaExistencia($datos, 'mediosEntidad');
	$mediosCentro=compruebaExistencia($datos, 'mediosCentro');

	$lunes=compruebaExistencia($datos, 'lunes');
	$martes=compruebaExistencia($datos, 'martes');
	$miercoles=compruebaExistencia($datos, 'miercoles');
	$jueves=compruebaExistencia($datos, 'jueves');
	$viernes=compruebaExistencia($datos, 'viernes');
	$sabado=compruebaExistencia($datos, 'sabado');
	$domingo=compruebaExistencia($datos, 'domingo');
	
	$lunesFormacion=compruebaExistencia($datos, 'lunesFormacion');
	$martesFormacion=compruebaExistencia($datos, 'martesFormacion');
	$miercolesFormacion=compruebaExistencia($datos, 'miercolesFormacion');
	$juevesFormacion=compruebaExistencia($datos, 'juevesFormacion');
	$viernesFormacion=compruebaExistencia($datos, 'viernesFormacion');
	$sabadoFormacion=compruebaExistencia($datos, 'sabadoFormacion');
	$domingoFormacion=compruebaExistencia($datos, 'domingoFormacion');
	
	$lunesFormacionDistancia=compruebaExistencia($datos, 'lunesFormacionDistancia');
	$martesFormacionDistancia=compruebaExistencia($datos, 'martesFormacionDistancia');
	$miercolesFormacionDistancia=compruebaExistencia($datos, 'miercolesFormacionDistancia');
	$juevesFormacionDistancia=compruebaExistencia($datos, 'juevesFormacionDistancia');
	$viernesFormacionDistancia=compruebaExistencia($datos, 'viernesFormacionDistancia');
	$sabadoFormacionDistancia=compruebaExistencia($datos, 'sabadoFormacionDistancia');
	$domingoFormacionDistancia=compruebaExistencia($datos, 'domingoFormacionDistancia');
	

	$consulta=consultaBD("UPDATE cursos SET fechaInicio='".$datos['fechaInicio']."', fechaFin='".$datos['fechaFin']."', mediosPropios='$mediosPropios', mediosCentro='$mediosCentro', mediosEntidad='$mediosEntidad'
	, horaInicio='".$datos['horaInicio']."', horaFin='".$datos['horaFin']."',llamadaBienvenida='".$datos['llamadaBienvenida']."',llamadaSeguimiento='".$datos['llamadaSeguimiento']."',llamadaFinalizacion='".$datos['llamadaFinalizacion']."',bonificado='".$datos['bonificado']."', horaInicioTarde='".$datos['horaInicioTarde']."', horaFinTarde='".$datos['horaFinTarde']."', horasTutoria='".$datos['horasTutoria']."'
	, lunes='".$lunes."', martes='".$martes."', miercoles='".$miercoles."', jueves='".$jueves."', viernes='".$viernes."', sabado='".$sabado."', domingo='".$domingo."'
	, cif='".$datos['cif']."', centro='".$datos['centro']."', tlf='".$datos['tlf']."', domicilio='".$datos['domicilio']."', cp='".$datos['cp']."', 
	poblacion='".$datos['poblacion']."', titularidad='".$datos['titularidad']."', codigoInterno='".$datos['codigoInterno']."', informarlt='".$datos['informarlt']."', informerlt='".$datos['informerlt']."', fechaDiscrepancia='".$datos['fechaDiscrepancia']."', resuelto='".$datos['resuelto']."',
	cifTutoria='".$datos['cifTutoria']."', centroTutoria='".$datos['centroTutoria']."', tlfTutoria='".$datos['tlfTutoria']."', domicilioTutoria='".$datos['domicilioTutoria']."', cpTutoria='".$datos['cpTutoria']."',
	poblacionTutoria='".$datos['poblacionTutoria']."', horaInicioFormacion='".$datos['horaInicioFormacion']."', horaFinFormacion='".$datos['horaFinFormacion']."', horaInicioFormacionTarde='".$datos['horaInicioFormacionTarde']."', horaFinFormacionTarde='".$datos['horaFinFormacionTarde']."',
	horasFormacion='".$datos['horasFormacion']."', lunesFormacion='$lunesFormacion', martesFormacion='$martesFormacion', miercolesFormacion='$miercolesFormacion', juevesFormacion='$juevesFormacion', 
	viernesFormacion='$viernesFormacion', sabadoFormacion='$sabadoFormacion', domingoFormacion='$domingoFormacion', tutorDistancia=".$datos['tutorDistancia'].", cifDistancia='".$datos['cifDistancia']."', centroGestorDistancia='".$datos['centroGestorDistancia']."', 
	tlfDistancia='".$datos['tlfDistancia']."', domicilioDistancia='".$datos['domicilioDistancia']."', cpDistancia='".$datos['cpDistancia']."', poblacionDistancia='".$datos['poblacionDistancia']."', titularidadDistancia='".$datos['titularidadDistancia']."',
	horaInicioFormacionDistancia='".$datos['horaInicioFormacionDistancia']."', horaFinFormacionDistancia='".$datos['horaFinFormacionDistancia']."', horaInicioFormacionTardeDistancia='".$datos['horaInicioFormacionTardeDistancia']."', horaFinFormacionTardeDistancia='".$datos['horaFinFormacionTardeDistancia']."',
	horasFormacionDistancia='".$datos['horasFormacionDistancia']."', lunesFormacionDistancia='$lunesFormacionDistancia', martesFormacionDistancia='$martesFormacionDistancia', miercolesFormacionDistancia='$miercolesFormacionDistancia', juevesFormacionDistancia='$juevesFormacionDistancia', viernesFormacionDistancia='$viernesFormacionDistancia', sabadoFormacionDistancia='$sabadoFormacionDistancia', domingoFormacionDistancia='$domingoFormacionDistancia', 
	responsable='".$datos['responsable']."', tlfResponsable='".$datos['tlfResponsable']."', comercial='".$datos['comercial']."', codigoTutor='".$datos['codigoTutor']."', plataforma='".$datos['plataforma']."', finalizado='".$datos['finalizado']."', medios='".$datos['medios']."', formacionCurso='".$datos['formacionCurso']."'
	WHERE codigo='".$datos['codigo']."';");
	echo mysql_error();

	if(!$consulta){
		$res=false;
	}
	else{
		consultaBD("DELETE FROM alumnos_registrados_cursos WHERE codigoCurso='".$datos['codigo']."';");
		$alumnos=explode(',',$datos['listadoAlumnos']);
		foreach ($alumnos as $codigoAlumno){ 
	   		$consulta=consultaBD("INSERT INTO alumnos_registrados_cursos VALUES('".$datos['codigo']."','$codigoAlumno', 'NO','','', 'NO', 'NO','NO');");
			$consulta=consultaBD("SELECT clientes.codigo FROM alumnos INNER JOIN ventas ON ventas.codigo=alumnos.codigoVenta INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo WHERE alumnos.codigo='$codigoAlumno';");
			$datosConsulta=mysql_fetch_assoc($consulta);
			if(isset($datosConsulta['codigo'])){
				$anterior=consultaBD("SELECT * FROM costes WHERE codigoAlumno='".$datosConsulta['codigo']."' AND codigoCurso='".$datos['codigo']."';",false,true);
				if(!isset($anterior['codigo'])){
					$consulta=consultaBD("INSERT INTO accionformativa_cliente VALUES('".$datos['accionFormativa']."', '".$datosConsulta['codigo']."');");			
					$consulta=consultaBD("INSERT INTO costes VALUES(NULL, '".$datosConsulta['codigo']."', '".$datos['codigo']."', '0','0','0','0','0');");
					$codigoCoste=mysql_insert_id();
					$consulta=consultaBD("INSERT INTO periodos_costes VALUES(NULL, '', '', '$codigoCoste');");
				}
			}
	   		if(!$consulta){
	   			$res=false;
	   		}
		}
	}
	
	cierraBD();

	return $res;
}



function eliminaCurso(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM cursos WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
		}
	}

	cierraBD();

	return $res;
}



function obtieneMailsUsuarios(){
	$res='';
	$mails=array();
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("SELECT email FROM usuarios WHERE codigo='".$datos['codigo'.$i]."';");
		$consulta=mysql_fetch_assoc($consulta);
		if(!in_array($consulta['email'],$mails)){
			array_push($mails,$consulta['email']);
			$res.=$consulta['email'].', ';
		}
	}

	$res= substr_replace($res, '', strlen($res)-2, strlen($res));//Para quitar última coma

	cierraBD();

	return $res;
}



function imprimeCorreos(){
	//$codigoU=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='MARKETING'){
		$where=compruebaPerfilParaWhere();
	}

	conexionBD();

	$consulta=consultaBD("SELECT correos.codigo AS codigo, nombre, apellidos, destinatarios, fecha, hora, asunto, SUBSTRING(mensaje,1,10) AS mensaje FROM correos INNER JOIN usuarios ON correos.codigoUsuario=usuarios.codigo $where ORDER BY fecha, hora DESC;");
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		$fecha=formateaFechaWeb($datos['fecha']);
		$hora=formateaHoraWeb($datos['hora']);
		echo "
		<tr>
			<td> ".$datos['apellidos'].", ".$datos['nombre']." </td>
        	<td> ".$datos['destinatarios']." </td>
        	<td class='centro'> $fecha<br>a las $hora </td>
        	<td> ".$datos['asunto']." </td>
        	<td> ".$datos['mensaje']."[...] </td>
        	<td class='centro'>
        		<a href='detallesCorreo.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}


function datosCorreo($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT nombre, apellidos, usuario, email AS remitente, destinatarios, cco, fecha, hora, asunto, mensaje, adjunto FROM correos INNER JOIN usuarios ON correos.codigoUsuario=usuarios.codigo WHERE correos.codigo='$codigo';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}



function eliminaCorreo(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM correos WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
		}
	}

	cierraBD();

	return $res;
}



function firmaUsuario(){
	$codigoU=$_SESSION['codigoS'];

	conexionBD();
	$consulta=consultaBD("SELECT firmaCorreo FROM usuarios WHERE codigo='$codigoU';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta['firmaCorreo'];
}


function actualizaFirma(){
	$res=true;

	$firma=$_POST['firma'];
	$codigoU=$_SESSION['codigoS'];

	conexionBD();
	$consulta=consultaBD("UPDATE usuarios SET firmaCorreo='$firma' WHERE codigo='$codigoU';");
	cierraBD();

	if(!$consulta){
		$res=false;
	}

	return $res;
}




function creaEstadisticasCorreos(){
	$datos=array();

	conexionBD();	
	//$codigoU=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='MARKETING'){
		$where=compruebaPerfilParaWhere();
	}

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM correos $where;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];

	cierraBD();

	return $datos;
}


function compruebaOpcionEliminar(){
	if($_SESSION['usuario']=='ylopez' || $_SESSION['usuario']=='mlopez'){
		echo '<a href="#" id="eliminar" class="shortcut"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>';
	}
}




function creaEstadisticasCursos($archivo=''){
	$datos=array();

	conexionBD();	
	$codigoU=$_SESSION['codigoS'];
	$where='';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION' && $_SESSION['tipoUsuario']!='MARKETING' && $_SESSION['tipoUsuario']!='CONSULTORIA'){
		$where=compruebaPerfilParaWhere();
	}
	
	if($_SESSION['tipoUsuario']=='COMERCIAL'){
		$where="WHERE cursos.codigo IN(SELECT codigoCurso FROM alumnos_registrados_cursos WHERE codigoAlumno IN(
		SELECT codigo FROM alumnos WHERE codigoVenta IN(SELECT codigo FROM ventas WHERE codigoCliente IN(
		SELECT codigo FROM clientes WHERE codigoUsuario='$codigoU' OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador='$codigoU')))))";
	}

	$consulta=consultaBD("SELECT COUNT(cursos.codigo) AS codigo FROM cursos INNER JOIN accionFormativa ON cursos.codigoAccionFormativa=accionFormativa.codigoInterno $where $archivo;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];

	cierraBD();

	return $datos;
}



/*
function compruebaPerfilSelectUsuariosIncidencia(){
	if($_SESSION['tipoUsuario']=='ADMIN'){
		echo '
		<div class="control-group">                     
	      <label class="control-label" for="usuario">Usuario:</label>
	      <div class="controls">';
	    	selectUsuarios();
	    echo '    
	      </div> <!-- /controls -->       
	    </div> <!-- /control-group -->';
	}
}

function selectUsuarios($codigo=false){
	echo "<select name='usuario' id='usuario' class='selectpicker show-tick' data-live-search='true'>";
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre, apellidos FROM usuarios ORDER BY apellidos, nombre;");
	cierraBD();
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=false){
		echo "<option value='".$datos['codigo']."'";

		if($codigo!=false && $codigo==$datos['codigo']){
			echo " selected='selected'";
		}

		echo ">".$datos['apellidos'].", ".$datos['nombre']."</option>";
		$datos=mysql_fetch_assoc($consulta);
	}
	echo "</select>";
}*/


function imprimeAlumnos($condicion="AND fechaFin>=CURDATE()"){
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION'){
		$where=compruebaPerfilParaWhere('alumnos.codigoUsuario');
	}

	conexionBD();

	$consulta=consultaBD("SELECT alumnos.codigo, nombre, apellidos, alumnos.telefono, alumnos.mail, empresa, fechaFin, clientes.codigo AS codigoCliente FROM 
	(alumnos LEFT JOIN ventas ON alumnos.codigoVenta=ventas.codigo) 
	LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo 
	INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno
	INNER JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo
	$where $condicion ORDER BY apellidos, nombre;");
	$datos=mysql_fetch_assoc($consulta);


	while($datos!=0){
		$fecha = date('Y-m-d');
		$nuevafecha = date('Y-m-d',strtotime ( '+3 days' , strtotime ( $fecha ) ) );
		if($datos['fechaFin']<=$nuevafecha){
			$icono=' <span class="label label-danger"><i class="icon-flag"></i></span>';
		}
		else{
			$icono='';
		}
	
		echo "
		<tr>
        	<td> ".$datos['apellidos'].", ".$datos['nombre']." $icono</td>
        	<td> ".formateaTelefono($datos['telefono'])." </td>
        	<td> ".$datos['mail']." </td>
        	<td> <a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a> </td>
        	<td class='centro'>";
				if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='CONSULTORIA' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){
					echo"<a href='detallesAlumno.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-edit'></i> Datos</i></a>";
				}
				echo "<a href='generaDocAlumno.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-download'></i> Descargar</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}



function eliminaAlumno(){
	$res=true;
	$datos=arrayFormulario();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		eliminaObservaciones($datos['codigo'.$i]);
		$consulta=consultaBD("DELETE FROM alumnos WHERE codigo='".$datos['codigo'.$i]."';", true);
		if(!$consulta){
			$res=false;
		}
	}

	return $res;
}



function datosCompletosAlumno($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM alumnos WHERE codigo='$codigo';");
	cierraBD();
	$consulta=mysql_fetch_assoc($consulta);
	$consulta['numSS1']=substr($consulta['numSS'],0,2);
	$consulta['numSS2']=substr($consulta['numSS'],2);

	return $consulta;
}


function actualizaAlumno($curso=false){
	$res=true;

	$datos=arrayFormulario();
	$datos['numSS']=$datos['numSS1'].$datos['numSS2'];

	conexionBD();
	$consulta=actualizaDatos('alumnos');
		
	if($curso){
		$consulta=consultaBD("UPDATE alumnos_registrados_cursos SET llamadaBienvenida='".$datos['llamadaBienvenida']."',fechaBienvenida='".$datos['fechaBienvenida']."',horaBienvenida='".$datos['horaBienvenida']."', llamadaSeguimiento='".$datos['llamadaSeguimiento']."', llamadaFinalizacion='".$datos['llamadaFinalizacion']."', resolucion='".$datos['resolucion']."' WHERE codigoAlumno='".$datos['codigo']."' AND codigoCurso='".$datos['codigoCurso']."';");
	}
	
	cierraBD();

	insertaObservaciones($datos['codigo']);

	return $res;
}

function actualizaCostes(){
	$res=true;

	$datos=arrayFormulario();
	conexionBD();
	$consulta=consultaBD("UPDATE costes SET costesImparticion='".$datos['costesImparticion']."', costesOrganizacion='".$datos['costesOrganizacion']."', costesHora='".$datos['costesHora']."', 
	costesElegibles='".$datos['costesElegibles']."', costesIndirectos='".$datos['costesIndirectos']."' WHERE codigo='".$datos['codigoCurso']."';");
	$consulta=consultaBD("DELETE FROM periodos_costes WHERE codigoCoste='".$datos['codigoCurso']."';");
	$i=0;
	while(isset($datos['mes'.$i])){
		$consulta=consultaBD("INSERT INTO periodos_costes VALUES(NULL, '".$datos['mes'.$i]."', '".$datos['importe'.$i]."', '".$datos['codigoCurso']."');");
		$i++;
	}
	cierraBD();

	return $res;
}


function obtieneMailsAlumnos(){
	$res='';
	$mails=array();
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("SELECT mail FROM alumnos WHERE codigo='".$datos['codigo'.$i]."';");
		$consulta=mysql_fetch_assoc($consulta);
		if(!in_array($consulta['mail'],$mails)){
			array_push($mails,$consulta['mail']);
			$res.=$consulta['mail'].', ';
		}
	}

	$res= substr_replace($res, '', strlen($res)-2, strlen($res));//Para quitar última coma

	cierraBD();

	return $res;
}



function creaEstadisticasAlumnos($condicion="AND fechaFin>=CURDATE()"){
	$datos=array();
	$codigoU=$_SESSION['codigoS'];
	
	conexionBD();	
	$where='';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION' && $_SESSION['tipoUsuario']!='CONSULTORIA'){
		$where=compruebaPerfilParaWhere('alumnos.codigoUsuario');
	}
	
	if($_SESSION['tipoUsuario']=='COMERCIAL'){
		$where="WHERE alumnos.codigoVenta IN(SELECT codigo FROM ventas WHERE codigoCliente IN(
		SELECT codigo FROM clientes WHERE codigoUsuario='$codigoU' OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador='$codigoU')))";
	}

	$consulta=consultaBD("SELECT COUNT(alumnos.codigo) AS codigo, fechaFin FROM (alumnos LEFT JOIN ventas ON alumnos.codigoVenta=ventas.codigo) 
	LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo 
	INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno
	INNER JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo
	$where $condicion;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];

	cierraBD();

	return $datos;
}


function generaXMLParticipantes($codigoCurso){
	$participantes='';

	conexionBD();
	$consulta=consultaBD("SELECT dni, nombre, apellidos, numSS, sexo, fechaNac, denominacion, cursos.codigo AS grupo FROM ((alumnos INNER JOIN alumnos_registrados_cursos ON 
		alumnos.codigo=alumnos_registrados_cursos.codigoAlumno)	INNER JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo) INNER JOIN 
		accionFormativa ON cursos.codigoaccionFormativa=accionFormativa.codigoInterno WHERE cursos.codigo='$codigoCurso';");

	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	$fichero=$datos['denominacion'].'-'.time().'.xml';

	while($datos!=false){
		$nodoParticipante=file_get_contents('documentos/nodoParticipante.xml');

		$espacio=strpos($datos['apellidos'],' ');
		$datos['apellido1']=substr($datos['apellidos'],0,$espacio);
		$datos['apellido2']=substr($datos['apellidos'],$espacio+1);

		$nodoParticipante=str_replace('${dni}', $datos['dni'], $nodoParticipante);
		$nodoParticipante=str_replace('${apellido1}', $datos['apellido1'], $nodoParticipante);
		$nodoParticipante=str_replace('${apellido2}', $datos['apellido2'], $nodoParticipante);
		$nodoParticipante=str_replace('${nombre}', $datos['nombre'], $nodoParticipante);
		$nodoParticipante=str_replace('${numSS}', $datos['numSS'], $nodoParticipante);
		$nodoParticipante=str_replace('${sexo}', $datos['sexo'], $nodoParticipante);
		$nodoParticipante=str_replace('${fechaNac}', formateaFechaWeb($datos['fechaNac']), $nodoParticipante);

		$participantes.=$nodoParticipante;
		$datos=mysql_fetch_assoc($consulta);
	}

	unset($nodoParticipante);
	$xml=file_get_contents('documentos/plantillaParticipante.xml');
	$xml=str_replace('${participante}', $participantes, $xml);

	file_put_contents("documentos/$fichero",$xml);

	unset($participantes);
	unset($xml);//Borro las variables para liberar memoria.

	return $fichero;
}




function imprimeAlumnosNotificaciones($datosAlumnos){
	conexionBD();

	$consulta=consultaBD("SELECT codigo, nombre, dni, mail, telefono FROM alumnos ORDER BY nombre;");
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		if(in_array($datos['codigo'],$datosAlumnos)){
			echo "
			<tr>
	        	<td> ".$datos['nombre']." </td>
	        	<td> ".$datos['dni']." </td>
	        	<td> ".$datos['mail']." </td>
				<td> ".$datos['telefono']." </td>
	    	</tr>";
	    }
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}


function datosNotificacion($codigoCurso){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM notificaciones WHERE codigoCurso='$codigoCurso';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}


function registraNotificacion(){
	$res=true;

	$datos=arrayFormulario();
	
	conexionBD();
	consultaBD("DELETE FROM notificaciones WHERE codigoCurso='".$datos['codigoCursoNotificacion']."';");

	$consulta=consultaBD("INSERT INTO notificaciones VALUES(NULL, '".$datos['fechaInicio']."', '".$datos['fechaFin']."', '".$datos['hora']."',
		'".$datos['repetir']."', '".$datos['mensajeCorreo']."', '".$datos['codigoCursoNotificacion']."');");
	$codigoNotificacion=mysql_insert_id();
	cierraBD();

	if(!$consulta || !enviaAvisoNotificacion($codigoNotificacion)){
		$res=false;
	}

	return $res;
}


function enviaAvisoNotificacion($codigoNotificacion){
	$res=true;

	$headers="From: webmaster@qmaconsultores.com\r\n";
	$headers.= "MIME-Version: 1.0\r\n";
	$headers.= "Content-Type: text/html; charset=UTF-8";

	$mensaje="Código de notificación: $codigoNotificacion";

	if (!mail('webmaster@qmaconsultores.com, programacion@qmaconsultores.com', 'Nueva notificación programada de Grupqualia', $mensaje ,$headers)){
		$res=false;
	}

	return $res;
}


function selectComerciales($codigo=false){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre, apellidos FROM usuarios WHERE activoUsuario='SI' AND (tipo='COMERCIAL' OR tipo='ADMIN') ORDER BY apellidos, nombre;");//Añadido el administrador en la actualización del 20/10/2014
	cierraBD();
	if($codigo!=false){
		$comerciales=comercialesObjetivo($codigo);
	}

	$datos=mysql_fetch_assoc($consulta);
	echo '<select name="comerciales[]" class="selectpicker span4 show-tick" data-live-search="true" multiple data-selected-text-format="count" multiple title="Seleccione a los que se les asignará el objetivo...">';
	while($datos!=false){
		echo '<option value="'.$datos['codigo'].'"';
		
		if($codigo!=false && in_array($datos['codigo'],$comerciales)){
			echo ' selected="selected"';
			$porcentaje=compruebaObjetivoComercial($codigo,$datos['codigo']);
		}

		echo '>'.$datos['apellidos'].', '.$datos['nombre'].'</option>';
		$datos=mysql_fetch_assoc($consulta);
	}
	echo '</select>';
}


function compruebaObjetivoComercial($objetivo,$comercial){
	$consultaO=consultaBD("SELECT COUNT(ventas.codigo) AS codigo, objetivos.valor FROM (((ventas INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo) INNER JOIN usuarios ON clientes.codigoUsuario=usuarios.codigo) INNER JOIN objetivos_asignados_comerciales ON usuarios.codigo=objetivos_asignados_comerciales.codigoUsuario) INNER JOIN objetivos ON objetivos_asignados_comerciales.codigoObjetivo=objetivos.codigo WHERE objetivos_asignados_comerciales.codigoUsuario='$comercial' AND objetivos_asignados_comerciales.codigoObjetivo='$objetivo';");
}


function registraObjetivo(){
	$res=true;

	$codigoU=$_SESSION['codigoS'];
	$datos=arrayFormulario();
	
	conexionBD();

	if(isset($_SESSION["verifica"]) && $_SESSION["verifica"] == 1){
		unset($_SESSION['verifica']);
		$consulta=consultaBD("INSERT INTO objetivos VALUES(NULL, '".$datos['fechaInicio']."', '".$datos['fechaFin']."', '".$datos['valor']."', '".$datos['importe']."');");

		if(!$consulta){
			$res=false;
		}
		else{
			$codigoCurso=mysql_insert_id();
			foreach($datos['comerciales'] as $comercial){ 
				$consulta=consultaBD("INSERT INTO objetivos_asignados_comerciales VALUES('$codigoCurso', '$comercial');");
				if(!$consulta){
					$res=false;
				}
			}
		}
	}else{
		$res=false;
	}
	
	cierraBD();

	return $res;
}

function imprimeObjetivos(){
	conexionBD();

	$consulta=consultaBD("SELECT codigo, fechaInicio, fechaFin, valor, importe FROM objetivos ORDER BY fechaInicio, fechaFin;");
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		echo "
		<tr>
        	<td> Conseguir <strong>".$datos['valor']."</strong> ventas e ingresar <strong>".$datos['importe']."</strong> € </td>
        	<td> ".formateaFechaWeb($datos['fechaInicio'])." </td>
        	<td> ".formateaFechaWeb($datos['fechaFin'])." </td>
        	<td class='centro'>
        		<a href='detallesObjetivo.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}



function eliminaObjetivo(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM objetivos WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
		}
	}

	cierraBD();

	return $res;
}


function datosObjetivo($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM objetivos WHERE codigo='$codigo';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}


function comercialesObjetivo($codigo){
	$datos=array();

	conexionBD();
	$consulta=consultaBD("SELECT codigoUsuario FROM objetivos_asignados_comerciales WHERE codigoObjetivo='$codigo';");
	cierraBD();

	$reg=mysql_fetch_assoc($consulta);
	while($reg!=false){
		array_push($datos,$reg['codigoUsuario']);
		$reg=mysql_fetch_assoc($consulta);
	}
	return $datos;
}


function actualizaObjetivo(){
	$res=true;

	$codigoU=$_SESSION['codigoS'];
	$datos=arrayFormulario();
	
	

	$consulta=actualizaDatos('objetivos');
	conexionBD();
	if(!$consulta){
		$res=false;
	}
	else{
		consultaBD("DELETE FROM objetivos_asignados_comerciales WHERE codigoObjetivo='".$datos['codigo']."';");

		foreach($datos['comerciales'] as $comercial){ 
	   		$consulta=consultaBD("INSERT INTO objetivos_asignados_comerciales VALUES('".$datos['codigo']."', '$comercial');");
	   		if(!$consulta){
	   			echo 'aqui4';
	   			$res=false;
	   		}
		}
	}
	
	cierraBD();

	return $res;
}


function generaDatosGraficoObjetivos(){
	$datos=array('ventas'=>0,'objetivo'=>0);

	conexionBD();
	$consulta=consultaBD("SELECT COUNT(ventas.codigo) AS codigo, objetivos.valor FROM (((ventas INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo) INNER JOIN usuarios ON clientes.codigoUsuario=usuarios.codigo) INNER JOIN objetivos_asignados_comerciales ON usuarios.codigo=objetivos_asignados_comerciales.codigoUsuario) INNER JOIN objetivos ON objetivos_asignados_comerciales.codigoObjetivo=objetivos.codigo WHERE ventas.fecha>=objetivos.fechaInicio AND ventas.fecha<=objetivos.fechaFin AND objetivos.fechaInicio<=CURDATE() AND objetivos.fechaFin>=CURDATE();");
	cierraBD();

	$reg=mysql_fetch_assoc($consulta);
	while($reg!=false){
		$datos['ventas']+=$reg['codigo'];
		$datos['objetivo']+=$reg['valor'];
		$reg=mysql_fetch_assoc($consulta);
	}

	return $datos;	
}

function compruebaObjetivos(){
	if($_SESSION['tipoUsuario']=='COMERCIAL' || $_SESSION['tipoUsuario']=='ADMIN'){//Añadido ADMIN en la actualización del 20/10/2014
		$codigo=$_SESSION['codigoS'];

		conexionBD();
		//POR HACER: mejorar la consulta para que solo haga falta ejecutar 1
		$consulta=consultaBD("SELECT objetivos.valor, objetivos.fechaFin FROM objetivos INNER JOIN objetivos_asignados_comerciales ON objetivos.codigo=objetivos_asignados_comerciales.codigoObjetivo WHERE objetivos.fechaInicio<=CURDATE() AND objetivos.fechaFin>=CURDATE() AND objetivos_asignados_comerciales.codigoUsuario='$codigo';");

		if(mysql_num_rows($consulta)==0){
			echo '<h6 class="bigstats marginAbPeque">Sin objetivo asignado</h6>';
		}
		else{
			$consulta=mysql_fetch_assoc($consulta);
			$valorObjetivo=$consulta['valor'];
			$fechaObjetivo=$consulta['fechaFin'];

			$consulta=consultaBD("SELECT COUNT(ventas.codigo) AS codigo, objetivos.valor, objetivos.fechaFin FROM (((ventas INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo) INNER JOIN usuarios ON clientes.codigoUsuario=usuarios.codigo) INNER JOIN objetivos_asignados_comerciales ON usuarios.codigo=objetivos_asignados_comerciales.codigoUsuario) INNER JOIN objetivos ON objetivos_asignados_comerciales.codigoObjetivo=objetivos.codigo WHERE ventas.fecha>=objetivos.fechaInicio AND ventas.fecha<=objetivos.fechaFin AND objetivos.fechaInicio<=CURDATE() AND objetivos.fechaFin>=CURDATE() AND objetivos_asignados_comerciales.codigoUsuario='$codigo';");
			$reg=mysql_fetch_assoc($consulta);

			$tipoBarra='';
			while($reg!=false){
				if($reg['valor']!=NULL){
					$porcen=floor(($reg['codigo']*100)/$reg['valor']);//Calculo el porcentaje de ventas realizadas con respecto al total, y se lo paso a la función floor, que reondea los decimales a la baja.
					
					if($porcen<16){
						$tipoBarra='progress-danger';
					}
					elseif($porcen<51){
						$tipoBarra='progress-warning';
					}
					elseif($porcen<76){
						$tipoBarra='progress-primary';
					}
					elseif($porcen>=76){
						$tipoBarra='progress-success';
					}
					$valorObjetivo=$reg['valor'];
					$fechaObjetivo=$reg['fechaFin'];
				}
				else{
					$porcen=0;
					$tipoBarra='';
				}
				$reg=mysql_fetch_assoc($consulta);
				echo '
				<div class="cajaObjetivo">
			 		<h6><i class="icon icon-flag"></i> Objetivo vigente: conseguir '.$valorObjetivo.' ventas antes del '.formateaFechaWeb($fechaObjetivo).' (<strong>'.$porcen.'% completado</strong>)</h6>
					<div class="progress '.$tipoBarra.' progress-striped active">
	                    <div class="bar" aria-valuetransitiongoal="'.$porcen.'"></div>
					</div>
				</div>';
			}
		}
		cierraBD();
	}

}


function compruebaInicio(){
	$tipoUsuario=$_SESSION['tipoUsuario'];
	$inicio=array('ADMIN'=>'inicioAdmin.php','COMERCIAL'=>'inicioComercial.php','ADMINISTRACION'=>'inicioAdmin.php','CONSULTORIA'=>'inicioComercial.php','FORMACION'=>'inicioComercial.php','ATENCION'=>'inicioAdmin.php','TELECONCERTADOR'=>'posiblesClientes.php','MARKETING'=>'comunicacionInterna.php','SUPERVISOR'=>'ventasSupervisor.php');

	return $inicio[$tipoUsuario];
}


function creaEstadisticasInicioTutor(){
	$datos=array();

	conexionBD();
	$codigoU=$_SESSION['codigoS'];
	
	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM alumnos;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['alumnos']=$consulta['codigo'];


	$consulta=consultaBD("SELECT COUNT(cursos.codigo) AS codigo FROM cursos INNER JOIN accionFormativa ON cursos.codigoAccionFormativa=accionFormativa.codigo WHERE codigoUsuario='$codigoU';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['cursos']=$consulta['codigo'];


	$consulta=consultaBD("SELECT COUNT(tareas.codigo) AS codigo	FROM clientes INNER JOIN tareas ON clientes.codigo=tareas.codigoCliente WHERE codigoUsuario='$codigoU' AND tareas.estado='PENDIENTE' AND fechaInicio > '2016-11-30';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['tareas']=$consulta['codigo'];


	$consulta=consultaBD("SELECT COUNT(incidencias.codigo) AS codigo FROM clientes INNER JOIN incidencias ON clientes.codigo=incidencias.codigoCliente WHERE codigoUsuario='$codigoU';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['incidencias']=$consulta['codigo'];

	cierraBD();

	return $datos;
}


function compruebaAdjuntos($fichero){
	if(trim($fichero)!=''){
		echo '
		<div class="control-group">                     
	      <label class="control-label" for="adjunto">Archivo adjunto:</label>
	      <div class="controls enlaceForm">
	        <a href="adjuntosCorreos/'.$fichero.'" id="adjunto">'.$fichero.'</a>
	      </div>
	    </div>';
	}
}

function creaEstadisticasFacturas($where='WHERE 1=1'){
	$datos=array();

	conexionBD();
	
	$wherePerfil='';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){
		$codigoU=$_SESSION['codigoS'];
		$wherePerfil="AND codigoCliente IN (SELECT codigo FROM clientes WHERE 1=1 AND (codigoUsuario='$codigoU' OR codigoUsuario IN(SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU')))";
	}

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM facturacion $where $wherePerfil;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['facturacion']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM facturacion $where $wherePerfil AND cobrada='SI';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['cobradas']=$consulta['codigo'];
	
	$consulta=consultaBD("SELECT SUM(coste) AS total FROM facturacion $where $wherePerfil;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=number_format((float)$consulta['total'], 2, ',', '');

	cierraBD();

	return $datos;
}

function creaEstadisticasFacturasDevoluciones($where='WHERE 1=1',$where2=''){
	$datos=array();

	conexionBD();
	
	$wherePerfil='';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){
		$codigoU=$_SESSION['codigoS'];
		$wherePerfil="AND codigoCliente IN (SELECT codigo FROM clientes WHERE 1=1 AND (codigoUsuario='$codigoU' OR codigoUsuario IN(SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU')))";
	}

	$consulta=consultaBD("SELECT * FROM facturacion $where $wherePerfil;");
	$facturacion=0;
	$cobradas=0;
	$total=0;
	while($factura=mysql_fetch_assoc($consulta)){
		$where3=$where2.' AND codigoFactura='.$factura['codigo'];
		$vencimiento=consultaBD('SELECT * FROM facturas_vto '.$where3,false,true);
		if($vencimiento){
			$facturacion++;
			$total=floatval($total)+floatval($vencimiento['importe']);
			if($vencimiento['cobrada']=='SI'){
				$cobradas++;
			}
		}
	}
	$consulta=mysql_fetch_assoc($consulta);

	$datos['facturacion']=$facturacion;
	$datos['cobradas']=$cobradas;
	$datos['total']=number_format((float)$total, 2, ',', '');

	cierraBD();

	return $datos;
}

function imprimeProductos(){
	$consulta=consultaBD("SELECT codigo, codigoProducto, nombreProducto FROM productos ORDER BY codigo DESC;",true);
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		echo "
		<tr>
        	<td> ".$datos['codigoProducto']." </td>
        	<td> ".$datos['nombreProducto']." </td>
			<td class='centro'>
        		<a href='detallesProducto.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-edit'></i> Editar</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function imprimePreventas($where='WHERE 1=1',$filtro=false){
	$fecha=date('Y-m-d');
	$fecha=explode('-', $fecha);
	if(!$filtro){
		$where.=" AND (fecha >= '".$fecha[0]."-".$fecha[1]."-01' OR aceptado='SINEVALUAR')";
	} else {
		$where.=" AND ".$filtro;
	}
	$consulta=consultaBD("SELECT preventas.* FROM preventas INNER JOIN tareas ON preventas.codigoTarea=tareas.codigo ".$where." ORDER BY fecha DESC;",true);
	$servicios=array('formacion','lssi','alergenos','prl','plataformaWeb','auditoriaLopd','consultoriaLopd','auditoriaPrl','webLegalizada','dominioCorreo','dominio','ecommerce');
	while($datos=mysql_fetch_assoc($consulta)){
		$tarea=datosRegistro('tareas',$datos['codigoTarea']);
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		$usuario=datosRegistro('usuarios',$tarea['codigoUsuario']);
		$total=0;
		foreach ($servicios as $value) {
			if($datos[$value]=='SI'){
				$total=$total+$datos[$value.'Precio'];
			}
		}
		if($cliente['activo']=='SI'){
			$cuenta="<a href='detallesCuenta.php?codigo=".$cliente['codigo']."'>".$cliente['empresa']."</a>";
		} else if($cliente['activo']=='NO'){
			$cuenta="<a href='detallesPosibleCliente.php?codigo=".$cliente['codigo']."'>".$cliente['empresa']."</a>";
		} else if($cliente['activo']=='PROPIO'){
			$cuenta="<a href='detallesPosibleClientePropio.php?codigo=".$cliente['codigo']."'>".$cliente['empresa']."</a>";
		}
		$iconoC=array('SI'=>'<i class="icon-ok-sign iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove-sign iconoFactura icon-danger"></i>');
		if($datos['aceptado']=='SI'){
			$style="class='preventaAceptada'";
		} elseif($datos['aceptado']=='NO'){
			$style="class='preventaRechazada'";
		} else {
			$style='';
		}
		echo "
		<tr ".$style.">
        	<td> ".$cuenta."</td>
        	<td> ".$usuario['nombre']." ".$usuario['apellidos']."</td>
        	<td> ".formateaFechaWeb($datos['fecha'])." </td>
        	<td class='centro'> ".devolverServicios($datos)." </td>
        	<td> ".number_format((float)$total, 2, ',', '')." € </td>";
        if(!isset($_GET['estado'])){
        	echo "<td class='centro'> ".$iconoC[$datos['documentos']]."<span class='hide'>".$datos['documentos']."</span> </td>";
        }
        if(!isset($_GET['estado']) || (isset($_GET['estado']) && $_GET['estado'] < 3)){ 
        	echo "<td class='centro'> ".$iconoC[$datos['verificada']]."<span class='hide'>".$datos['verificada']."</span> </td>";
        }
		echo "<td class='centro'>";
				if ($datos['aceptado']!='NO'){
        			echo "<a href='detallesPreventa.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-edit'></i></i></a> ";
        		}

        		if(($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='ATENCION') && $datos['aceptado']=='SINEVALUAR'){
        			echo "<a href='preventas.php?codigoEvaluado=".$datos['codigo']."&evaluacion=SI' class='btn btn-primary'><i class='icon-check'></i></i></a>";
        			if($_SESSION['codigoS']!=14){ 
        			echo " <a href='preventas.php?codigoEvaluado=".$datos['codigo']."&evaluacion=NO' class='btn btn-primary'><i class='icon-remove'></i></i></a>";
        			}
        		}
        		elseif($datos['aceptado']=='SINEVALUAR' && $_SESSION['tipoUsuario']!='COMERCIAL'){
        			echo "<a href='preventas.php?codigoEvaluado=".$datos['codigo']."&evaluacion=NO' class='btn btn-primary'><i class='icon-remove'></i></i></a>";
        		}
        		else if ($datos['aceptado']=='NO' && $_SESSION['tipoUsuario']!='COMERCIAL'){
        			echo " <a href='preventas.php?codigoEvaluado=".$datos['codigo']."&evaluacion=SINEVALUAR' class='btn btn-primary'><i class='icon-check'></i></i></a>";
        			if($_SESSION['tipoUsuario']=='ADMIN'){
        				echo " <a href='preventas.php?codigoEliminar=".$datos['codigo']."&aceptado=NO' class='btn btn-primary'><i class='icon-trash'></i></i></a>";
        			}
        		}

		echo"	</td>
    	</tr>";
	}
}

function evaluaPreventa($codigo,$evaluacion){
	$res=true;	
	if($evaluacion=='SI'){
		$preventa=datosRegistro('preventas',$codigo);
		$tarea=datosRegistro('tareas',$preventa['codigoTarea']);
		$servicios=array('formacion','lssi','alergenos','prl','plataformaWeb','auditoriaLopd','consultoriaLopd','auditoriaPrl','webLegalizada','dominioCorreo','dominio','ecommerce');
		$codigosServicios=array('formacion'=>'14','lssi'=>'18','alergenos'=>'19','prl'=>'20','plataformaWeb'=>'21','auditoriaLopd'=>'22','consultoriaLopd'=>'23','auditoriaPrl'=>'24','webLegalizada'=>'25','dominioCorreo'=>'26','dominio'=>'27','ecommerce'=>'28');
		$i=count($servicios)-1;
		for($i;$i>=0;$i--) {
			if($preventa[$servicios[$i]] == 'SI'){
				$servicio=$servicios[$i];
				if($i==0){
				echo '<script type="text/javascript">
      				window.open("creaVenta.php?codigo='.$preventa['codigoCliente'].'&preventaComercial='.$tarea['codigoUsuario'].'&preventaServicio='.$codigosServicios[$servicio].'&preventaPrecio='.$preventa[$servicio.'Precio'].'&preventaCodigo='.$preventa['codigo'].'&venta&activa","_self");
    			</script>';
    			} else {
    			echo '<script type="text/javascript">
      				window.open("creaVenta.php?codigo='.$preventa['codigoCliente'].'&preventaComercial='.$tarea['codigoUsuario'].'&preventaServicio='.$codigosServicios[$servicio].'&preventaPrecio='.$preventa[$servicio.'Precio'].'&preventaCodigo='.$preventa['codigo'].'&venta&activa","_blank");
    			</script>';	
    			}	
			}
		}
	} else {
		$res=consultaBD('UPDATE preventas SET aceptado="'.$evaluacion.'" WHERE codigo='.$codigo,true);
		$preventa=datosRegistro('preventas',$codigo);
		if($evaluacion=='NO'){
			$res=consultaBD('UPDATE tareas SET estado="pendiente", prioridad="concertada" WHERE codigo='.$preventa['codigoTarea'],true);
		} else {
			$res=consultaBD('UPDATE tareas SET estado="realizada", prioridad="alta" WHERE codigo='.$preventa['codigoTarea'],true);
		}
	}
	return $res;
}

function eliminaPreventa($codigo){
	$_POST['elimina']='SI';
	$res=true;
	$res=consultaBD('DELETE FROM preventas WHERE codigo='.$codigo,true);
	return $res;
}

function devolverServicios($datos){
	$servicios='';
	if($datos['formacion'] == 'SI'){
		$servicios.='Formación';
	}
	if($datos['lssi'] == 'SI'){
		$servicios.= $servicios == '' ? 'LSSI':', LSSI';
	}
	if($datos['alergenos'] == 'SI'){
		$servicios.= $servicios == '' ? 'APPCC-Alérgenos':', APPCC-Alérgenos';
	}
	if($datos['prl'] == 'SI'){
		$servicios.= $servicios == '' ? 'PRL':', PRL';
	}
	if($datos['plataformaWeb'] == 'SI'){
		$servicios.= $servicios == '' ? 'Plataforma Web':', Plataforma Web';
	}
	if($datos['auditoriaLopd'] == 'SI'){
		$servicios.= $servicios == '' ? 'Auditoría LOPD':', Auditoría LOPD';
	}
	if($datos['consultoriaLopd'] == 'SI'){
		$servicios.= $servicios == '' ? 'Consultoría LOPD':', Consultoría LOPD';
	}
	if($datos['auditoriaPrl'] == 'SI'){
		$servicios.= $servicios == '' ? 'Auditoría PRL':', Auditoría PRL';
	}
	if($datos['webLegalizada'] == 'SI'){
		$servicios.= $servicios == '' ? 'Web legalizada':', Web legalizada';
	}
	if($datos['dominioCorreo'] == 'SI'){
		$servicios.= $servicios == '' ? 'Dominio y correo electrónico':', Dominio correo electrónico';
	}
	if($datos['dominio'] == 'SI'){
		$servicios.= $servicios == '' ? 'Dominio':', Dominio';
	}
	if($datos['ecommerce'] == 'SI'){
		$servicios.= $servicios == '' ? 'E-commerce':', E-commerce';
	}
	return $servicios;
}

function imprimeOfertas($where=''){
	$consulta=consultaBD("SELECT ofertas.codigo AS codigo, codigoOferta, fechaOferta, empresa, nombreProducto, ofertas.estado FROM (ofertas INNER JOIN productos ON ofertas.codigoProducto=productos.codigo) INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo $where ORDER BY ofertas.codigo DESC;",true);
	$estados=array('CURSO'=>'<span class="label label-warning">En curso</span>','ACEPTADA'=>'<span class="label label-success">Aceptada</span>','RECHAZADA'=>'<span class="label label-danger">Rechazada</span>');

	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		echo "
		<tr>
        	<td> ".$datos['codigoOferta']." </td>
        	<td> ".$datos['empresa']." </td>
        	<td> ".$datos['nombreProducto']." </td>
        	<td> ".formateaFechaWeb($datos['fechaOferta'])." </td>
        	<td> ".$estados[$datos['estado']]." </td>
			<td class='nowrap'>
        		<a href='detallesOferta.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
        	";
    	
    	if($datos['estado']=='ACEPTADA'){
		conexionBD();
			$consultaDos=consultaBD("SELECT codigo FROM trabajos WHERE codigoOferta='".$datos['codigo']."';");
			cierraBD();
			echo mysql_error();
			$consultaDos=mysql_fetch_assoc($consultaDos);
    		echo "<a href='detallesTrabajo.php?codigo=".$consultaDos['codigo']."'  class='btn btn-success'><i class='icon-circle-arrow-right'></i> Ver Trabajo</i></a>";
    	}
    	else{
    		echo "<a href='creaTrabajo.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-ok-sign'></i> Confirmar Oferta</i></a>";
    	}

        echo "
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function imprimeTrabajos($where=''){
	$consulta=consultaBD("SELECT trabajos.codigo, ordenTrabajo, proyecto, trabajos.estado, empresa, clientes.codigo AS codigoCliente FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo $where ORDER BY trabajos.codigo DESC;",true);
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		echo "
		<tr>
        	<td> ".$datos['ordenTrabajo']." </td>
        	<td> ".$datos['proyecto']." </td>
        	<td> <a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a> </td>
        	<td> ".$datos['estado']." </td>";
			if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){
			echo "<td class='centro'>
					<a href='detallesTrabajo.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
				</td>";
			}
			echo"<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function imprimefacturas($where='',$informe=false,$filtrado=false){
	conexionBD();
	$wherePerfil='';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){
		$codigoU=$_SESSION['codigoS'];
		$wherePerfil="AND (clientes.codigoUsuario='$codigoU' OR clientes.codigoUsuario IN(SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU'))";
	}
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.fechaEmision, facturacion.cobrada, facturacion.fechaVencimiento, facturacion.referencia, facturacion.coste, clientes.empresa, facturacion.enviada, facturacion.concepto, facturacion.enviadaCliente, clientes.codigo AS codigoCliente, insercion, facturacion.firma, facturacion.devuelta, productos.nombreProducto, resolucionFactura 
		FROM facturacion LEFT JOIN clientes ON clientes.codigo=facturacion.codigoCliente 
		LEFT JOIN productos ON facturacion.concepto=productos.codigo $where $wherePerfil
		ORDER BY insercion DESC;");
	
	$datos=mysql_fetch_assoc($consulta);
	$iconoC=array('SI'=>'<i class="icon-ok-sign iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove-sign iconoFactura icon-danger"></i>');
	$devuelta=array('NO'=>'No devuelta.','SI'=>'Devuelta.');
	while($datos!=0){

		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$fechaVencimiento=formateaFechaWeb($datos['fechaVencimiento']);
		echo "
		<tr>
			<td> ";
				if($datos['concepto']=='14'){ 
					if($datos['firma']!='3'){
						echo"F";
					}else{
						echo"S";
					}
				}else{
					if($datos['firma']!='3'){
						echo"C";
					}else{
						echo"S";
					}
				}
			echo "-".$datos['referencia']." </td>
			<td> <a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".utf8_encode($datos['empresa'])."</a>  </td>
        	<td> ".utf8_encode($datos['nombreProducto'])." </td>
        	<td> $fecha </td>
			<td> $fechaVencimiento </td>
			<td> ".formateaNumero($datos['coste'])." €</td>
			<td class='centro'> ".$iconoC[$datos['cobrada']]." </td>
			<td class='centro'> ".$iconoC[$datos['enviada']]." </td>
			<td class='centro'> ".$iconoC[$datos['enviadaCliente']]." </td>";
			if($filtrado){
				
				echo "<td class='centro'> ".$devuelta[$datos['devuelta']].' '.$datos['resolucionFactura']." </td>";
			}	
			echo"
        	<td class='centro'>
				";
				if($_SESSION['tipoUsuario']!='ADMINISTRACION' && $_SESSION['tipoUsuario']!='ADMIN'){
				}else{
					$variable='14';
					if($informe){
						$variable='7';
					}
					echo "<a href='detallesFactura.php?codigo=".$datos['codigo']."&seccion=$variable' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a> ";
				}
				if(!$informe){
					echo "<a href='enviarEmail.php?codigoFactura=".$datos['codigo']."&seccion=14' class='btn btn-success'><i class='icon-envelope'></i> Enviar</i></a> ";
				}
        		echo "<a href='generaFactura.php?codigo=".$datos['codigo']."' class='btn btn-warning'><i class='icon-download-alt'></i> Descargar Factura</i></a>
        	</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function imprimeFacturasDevolucion($where='',$where2='',$informe,$filtrado){
	conexionBD();
	$wherePerfil='';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){
		$codigoU=$_SESSION['codigoS'];
		$wherePerfil="AND (clientes.codigoUsuario='$codigoU' OR clientes.codigoUsuario IN(SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU'))";
	}
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.fechaEmision, facturacion.cobrada, facturacion.fechaVencimiento, facturacion.referencia, facturacion.coste, clientes.empresa, facturacion.enviada, facturacion.concepto, facturacion.enviadaCliente, clientes.codigo AS codigoCliente, insercion, facturacion.firma, facturacion.devuelta, productos.nombreProducto, resolucionFactura 
		FROM facturacion LEFT JOIN clientes ON clientes.codigo=facturacion.codigoCliente 
		LEFT JOIN productos ON facturacion.concepto=productos.codigo $where $wherePerfil
		ORDER BY insercion DESC;");
	
	$datos=mysql_fetch_assoc($consulta);
	$iconoC=array('SI'=>'<i class="icon-ok-sign iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove-sign iconoFactura icon-danger"></i>');
	$devuelta=array('NO'=>'No devuelta.','SI'=>'Devuelta.');
	while($datos=mysql_fetch_assoc($consulta)){
		$where3=$where2.' AND codigoFactura='.$datos['codigo'];
		$vencimiento=consultaBD('SELECT * FROM facturas_vto '.$where3,false,true);
		if($vencimiento){
		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$fechaVencimiento=formateaFechaWeb($datos['fechaVencimiento']);
		echo "
		<tr>
			<td> ";
				if($datos['concepto']=='14'){ 
					if($datos['firma']!='3'){
						echo"F";
					}else{
						echo"S";
					}
				}else{
					if($datos['firma']!='3'){
						echo"C";
					}else{
						echo"S";
					}
				}
			echo "-".$datos['referencia']." </td>
			<td> <a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".utf8_encode($datos['empresa'])."</a>  </td>
        	<td> ".utf8_encode($datos['nombreProducto'])." </td>
        	<td> $fecha </td>
			<td> $fechaVencimiento </td>
			<td> ".formateaNumero($datos['coste'])." €</td>
			<td class='centro'> ".$iconoC[$datos['cobrada']]." </td>
			<td class='centro'> ".$iconoC[$datos['enviada']]." </td>
			<td class='centro'> ".$iconoC[$datos['enviadaCliente']]." </td>";
			if($filtrado){
				
				echo "<td class='centro'> ".$devuelta[$datos['devuelta']].' '.$datos['resolucionFactura']." </td>";
			}	
			echo"
        	<td class='centro'>
				";
				if($_SESSION['tipoUsuario']!='ADMINISTRACION' && $_SESSION['tipoUsuario']!='ADMIN'){
				}else{
					$variable='14';
					if($informe){
						$variable='7';
					}
					echo "<a href='detallesFactura.php?codigo=".$datos['codigo']."&seccion=$variable' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a> ";
				}
				if(!$informe){
					echo "<a href='enviarEmail.php?codigoFactura=".$datos['codigo']."&seccion=14' class='btn btn-success'><i class='icon-envelope'></i> Enviar</i></a> ";
				}
        		echo "<a href='generaFactura.php?codigo=".$datos['codigo']."' class='btn btn-warning'><i class='icon-download-alt'></i> Descargar Factura</i></a>
        	</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	}
	}
	cierraBD();
}

function datosHitos($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM hitos WHERE codigoTrabajo='$codigo';");
	cierraBD();
	return $consulta;
}

function selectTrabajos($codigo=false){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, proyecto FROM trabajos ORDER BY proyecto;");
	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	echo '            
			<form action="?" method="post">
			<select name="codigoTrabajo" id="codigoTrabajo" class="selectpicker show-tick" data-live-search="true">';
		while($datos!=false){
			echo '<option value="'.$datos['codigo'].'"';
		
			if($codigo==$datos['codigo']){
				echo ' selected="selected"';
			}

			echo '>'.$datos['proyecto'].'</option>';
			$datos=mysql_fetch_assoc($consulta);
		}
	echo '</select>
			<br>
		<button type="submit" class="btn btn-primary">Seleccionar <i class="icon-circle-arrow-right"></i></button>
	</form>';
}

function hito($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT actividad FROM hitos WHERE codigo='$codigo';");
	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}

function trabajo($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT proyecto FROM trabajos WHERE codigo='$codigo';");
	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}

function selectHitos($trabajo,$codigo=false){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, actividad FROM hitos WHERE codigoTrabajo='$trabajo' ORDER BY actividad;");
	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	echo '                  
			<select name="hito" id="hito" class="selectpicker show-tick" data-live-search="true">';
			$i=0;
		while($datos!=false){
			echo '<option value="'.$datos['codigo'].'"';
		
			if($codigo==$datos['codigo']){
				echo ' selected="selected"';
			}

			echo '>'.$datos['actividad'].'</option>';
			$datos=mysql_fetch_assoc($consulta);
			$i++;
		}
	echo '</select>';
	
	echo'<input type="hidden" id="coste'.$i.'" name="coste'.$i.'" value="'.$datos['importeReal'].'">';
}

function insertaCodigoOferta($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT fechaOferta, productos.codigoProducto AS codigoProducto FROM ofertas INNER JOIN productos ON ofertas.codigoProducto=productos.codigo WHERE ofertas.codigo='$codigo';",false,true);
	$arrayFecha=explode('-',$consulta['fechaOferta']);
	$codigoOferta=$arrayFecha[0].'-'.$consulta['codigoProducto'].'-'.$codigo;//Código de oferta de la forma 2014-002-1
	$res=consultaBD("UPDATE ofertas SET codigoOferta='$codigoOferta' WHERE codigo='$codigo';");
	cierraBD();

	return $res;
}

function insertaHitos($codigoTrabajo){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	consultaBD("DELETE FROM hitos WHERE codigoTrabajo='$codigoTrabajo';");
	for($i=0;isset($datos['actividad'.$i]);$i++){
		consultaBD("INSERT INTO hitos VALUES(NULL,'$codigoTrabajo','".$datos['actividad'.$i]."','".$datos['fechaPrevista'.$i]."','".$datos['fechaReal'.$i]."',
			'".$datos['observaciones'.$i]."');");
			echo mysql_error();
	}
	cierraBD();

	return $res;
}

function confirmaOferta($codigoOferta){
	return consultaBD("UPDATE ofertas SET estado='ACEPTADA' WHERE codigo='$codigoOferta';",true);
}

function activaCliente($codigoCliente){						//MOD JOSE LUIS
	$consulta=consultaBD("UPDATE clientes SET activo='SI' WHERE codigo='$codigoCliente';",true);
}

function desactivaClientes(){   									//MOD JOSE LUIS
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("SELECT codigoCliente,codigoOferta FROM trabajos WHERE codigo='".$datos['codigo'.$i]."';");
		$consulta=mysql_fetch_assoc($consulta);
		$consultaDos=consultaBD("UPDATE clientes SET activo='NO' WHERE codigo='".$consulta['codigoCliente']."';");
		$consultaDos="DELETE FROM ofertas WHERE codigo='".$consulta['codigoOferta']."';";
		$consultaDos=consultaBD("DELETE FROM ofertas WHERE codigo='".$consulta['codigoOferta']."';");
		if(!$consulta){
			$res=false;
		}
	}

	cierraBD();

	return $res;
}

function actualizaReferencia($codigo){
	conexionBD();
	
	$datos=arrayFormulario();
	if($datos['concepto']!='14'){
		$consultaReferencia=consultaBD("SELECT referencia FROM facturacion WHERE concepto!='14' ORDER BY referencia DESC LIMIT 1;",false,true);
	}else{
		if($datos['firma']=='3'){
			$consultaReferencia=consultaBD("SELECT referencia FROM facturacion WHERE concepto='".$datos['concepto']."' AND firma='".$datos['firma']."' ORDER BY referencia DESC LIMIT 1;",false,true);
		}else{
			$consultaReferencia=consultaBD("SELECT referencia FROM facturacion WHERE concepto='".$datos['concepto']."' AND firma!='3' ORDER BY referencia DESC LIMIT 1;",false,true);
		}
	}
	$consulta=consultaBD("UPDATE facturacion SET referencia='".$consultaReferencia['referencia']."' WHERE codigo='$codigo';");
	
	cierraBD();
}

function devuelveAnio(){
	return substr(date('Y'),2);
}


function emailsSistema(){
	$res='';

	conexionBD();
	$consultaC=consultaBD("SELECT mail FROM clientes;",false);
	$consultaA=consultaBD("SELECT mail FROM alumnos;",false);
	cierraBD();

	while($datos=mysql_fetch_assoc($consultaC)){
		if(trim($datos['mail'])!=''){
			$res.=$datos['mail'].', ';
		}
	}

	while($datos=mysql_fetch_assoc($consultaA)){
		if(trim($datos['mail'])!=''){
			$res.=$datos['mail'].', ';
		}
	}

	$res= substr_replace($res, '', strlen($res)-2, strlen($res));//Para quitar última 
	return $res;
}



//Parte de Encuestas (Jose Luis)/////////////////////////////////////////////////

function creaTablaSatisfaccion(){
	echo '
	<table class="table table-striped table-bordered mitadAncho">
	  <tbody>
		<tr>
	      <th> Apartado Analizado </th>
	      <th> Resultado </th>
	    </tr>';
}

function cierraTablaSatisfaccion(){
	echo '
	  </tbody>
	</table>
	';
}

function creaPreguntaTablaEncuesta($texto,$numero,$datos=false){		
	echo "
	<tr>
		<td class='justificado'>$texto</td>
		<td>
			<select name='pregunta$numero' id='pregunta$numero' class='selectpicker span2 show-tick'>";

			for($i=2;$i<=10;$i+=2){
				echo "<option value='$i'";
				if($datos!=false && $datos==$i){
					echo " selected='selected'";
				}
				echo ">$i</option>";
			}


		echo "
			</select>
		</td>
	</tr>";
}


//A partir de aqui hasta el final es nuevo de la actualización del 16/09/2014

function imprimeEncuestasSatisfaccion(){
	conexionBD();
	$codigoS=$_SESSION['codigoS'];
	$consulta=consultaBD("SELECT satisfaccion.codigo, satisfaccion.fecha, empresa FROM satisfaccion LEFT JOIN clientes ON satisfaccion.cliente=clientes.codigo ORDER BY fecha;");
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){

		$fecha=formateaFechaWeb($datos['fecha']);
		echo "
		<tr>        	
			<td> ".$datos['empresa']."</td>
        	<td> $fecha</td>
        	<td class='td-actions'>
        		<a href='detallesEncuesta.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}


function generaDatosGraficoEncuestasSatisfaccion(){
	$datos=array('1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0);

	conexionBD();

	$consulta=consultaBD("SELECT * FROM satisfaccion;");
	$datosConsulta=mysql_fetch_assoc($consulta);
	
	while($datosConsulta!=0){
		$datos[$datosConsulta['pregunta1']]++;
		$datos[$datosConsulta['pregunta2']]++;
		$datos[$datosConsulta['pregunta3']]++;
		$datos[$datosConsulta['pregunta4']]++;
		$datos[$datosConsulta['pregunta5']]++;
		$datos[$datosConsulta['pregunta6']]++;
		$datosConsulta=mysql_fetch_assoc($consulta);
	}

	cierraBD();

	return $datos;
}

function creaDatosEstadisticasSatisfaccion(){
	$datos=array();

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM satisfaccion;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];

	cierraBD();

	return $datos;
}


function creaEncuestaSatisfaccion(){
	$res=true;
	
	$datos=arrayFormulario();
	
	conexionBD();
	$consulta=consultaBD("INSERT INTO satisfaccion VALUES(NULL, '".$datos['cliente']."', '".$datos['fecha']."', '".$datos['puntuacion1']."', '".$datos['puntuacion2']."', '".$datos['puntuacion3']."', 
	'".$datos['puntuacion4']."', '".$datos['puntuacion5']."', '".$datos['puntuacion6']."', '".$datos['comentarios']."', '".$datos['nombreAlumno']."');");

	if(!$consulta){
		$res=false;
	}
	return $res;
}

function datosEncuesta($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM satisfaccion WHERE codigo='$codigo';");
	cierraBD();

	return mysql_fetch_assoc($consulta);
}

function actualizaEncuestaSatisfaccion(){
	$codigoU=$_SESSION['codigoS'];
	$res=true;
	
	$datos=arrayFormulario();
	
	conexionBD();
	$consulta=actualizaDatos('satisfaccion');

	if(!$consulta){
		$res=false;
	}
	return $res;
}

function obtieneURLCuestionario(){
	return "https://".$_SERVER['HTTP_HOST']."/grupqualia-pruebas/cuestionario-de-satisfaccion";
}

//Fin parte encuestas////////////////////////////////////////////


//Parte de Software

function imprimeTareasSoftware(){
	$consulta=consultaBD("SELECT software.*, clientes.empresa, usuarios.nombre, usuarios.apellidos FROM (software INNER JOIN clientes ON software.codigoCliente=clientes.codigo) INNER JOIN usuarios ON software.codigoUsuario=usuarios.codigo ORDER BY fechaSolicitud DESC",true);
	$tipos=array('CREACION'=>'Creación de Software','DEMO'=>'Personalización','MODIFICACION'=>'Modificaciones y/o mejoras','INCIDENCIA'=>'Incidencias');
	$realizado=array('1'=>'<span class="label label-danger"><i class="icon-remove-sign"></i> Pendiente</span>','2'=>'<span class="label label-warning"><i class="icon-time"></i> En desarrollo</span>','3'=>'<span class="label label-success"><i class="icon-ok-sign"></i> Finalizado</span>');
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
		<tr>
        	<td> ".$datos['empresa']." </td>
        	<td> ".$tipos[$datos['tipoTarea']]." </td>
        	<td> ".formateaFechaWeb($datos['fechaSolicitud'])." </td>
        	<td> ".$datos['nombre']." ".$datos['apellidos']."</td>
        	<td class='centro'> ".$realizado[$datos['realizado']]." </td>
        	<td class='centro'>
        		<a href='detallesSoftware.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver Tarea</i></a>
        	</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
	}
}


function imprimeTodo(){
	$consulta=consultaBD("SELECT software.*, clientes.empresa, usuarios.nombre, usuarios.apellidos FROM (software INNER JOIN clientes ON software.codigoCliente=clientes.codigo) INNER JOIN usuarios ON software.codigoUsuario=usuarios.codigo ORDER BY fechaSolicitud DESC",true);
	$tipos=array('CREACION'=>'Creación de Software','DEMO'=>'Personalización','MODIFICACION'=>'Modificaciones y/o mejoras','INCIDENCIA'=>'Incidencias');
	while($datos=mysql_fetch_assoc($consulta)){
		echo "<tr>";
		if($datos['realizado']=='1'){
			echo "<td><span class='label label-danger'><a href='detallesSoftware.php?codigo=".$datos["codigo"]."'>".$datos['empresa']." (".$tipos[$datos['tipoTarea']].")</a></span></td>
				 <td></td><td></td>";
		}
		elseif($datos['realizado']=='2'){
			echo "<td></td><td><span class='label label-warning'><a href='detallesSoftware.php?codigo=".$datos["codigo"]."'>".$datos['empresa']." (".$tipos[$datos['tipoTarea']].")</a></span></td>
				 <td></td>";
		}
		elseif($datos['realizado']=='3'){
			echo "<td></td><td></td><td><span class='label label-success'><a href='detallesSoftware.php?codigo=".$datos["codigo"]."'>".$datos['empresa']." (".$tipos[$datos['tipoTarea']].")</a></span></td>";
		}
        	
    	echo "</tr>";
	}
}


function creaTablaDesarrollo($codigoSoftware){
	echo "
	<div class='control-group'>                     
      <label class='control-label' for='tablaDesarrollo'>Evolución del desarrollo:</label>
      <div class='controls'>

		<table class='table table-striped table-bordered' id='tablaDesarrollo'>
	      <thead>
	        <tr>
	          <th> Actividad </th>
	          <th> Fecha de Inicio </th>
	          <th> Hora de Inicio </th>
	          <th> Fecha prevista de finalizción </th>
	          <th> Fecha de Fin </th>
	          <th> Hora de Fin </th>
	          <th> Tiempo empleado </th>
	        </tr>
	      </thead>
	      <tbody>";
	      
		  $consulta=consultaBD("SELECT * FROM tareasSoftware WHERE codigoSoftware='$codigoSoftware';",true);//TODO: editar consulta
	      if(mysql_num_rows($consulta)>0){
	      	$i=0;
	      	while($datos=mysql_fetch_assoc($consulta)){
	      		echo "<tr>";
		      		campoTextoTabla('actividad'.$i,$datos['actividad']);
		      		campoFechaTabla('fechaInicio'.$i,$datos['fechaInicio']);
		      		campoTextoTabla('horaInicio'.$i,formateaHoraWeb($datos['horaInicio']),'input-mini');
		      		campoFechaTabla('fechaPrevista'.$i,$datos['fechaPrevista']);
		      		campoFechaTabla('fechaFin'.$i,$datos['fechaFin']);
		      		campoTextoTabla('horaFin'.$i,formateaHoraWeb($datos['horaFin']),'input-mini');
					campoTextoTabla('tiempo'.$i,$datos['tiempo'],'input-small');
	      		echo "</tr>";
	      		$i++;
	      	}
	      }
	      else{
	      	echo "<tr>";
		  		campoTextoTabla('actividad0');
	      		campoFechaTabla('fechaInicio0');
	      		campoTextoTabla('horaInicio0','','input-mini');
	      		campoFechaTabla('fechaPrevista0');
	      		campoFechaTabla('fechaFin0');
	      		campoTextoTabla('horaFin0','','input-mini');
				campoTextoTabla('tiempo0','','input-small');
	  		echo "</tr>";
	      }

	    echo "</tbody>
	       </table>
		   <button type='button' class='btn btn-success' onclick='insertaFila(\"tablaDesarrollo\");'><i class='icon-plus'></i> Añadir fila</button> 
		   <button type='button' class='btn btn-danger' onclick='eliminaFila(\"tablaDesarrollo\");'><i class='icon-minus'></i> Eliminar fila</button>

       </div> <!-- /controls -->       
    </div> <!-- /control-group --><br />";
}

function actualizaSoftware(){
	$res=actualizaDatos('software');
	if($res){
		$datos=arrayFormulario();
		$codigoS=$datos['codigo'];

		conexionBD();
		$res=consultaBD("DELETE FROM tareasSoftware WHERE codigoSoftware='$codigoS';");
		for($i=0;isset($datos['actividad'.$i]);$i++){
			$res=$res && consultaBD("INSERT INTO tareasSoftware VALUES(NULL,'".$datos['actividad'.$i]."','".$datos['fechaInicio'.$i]."',
				'".$datos['horaInicio'.$i]."','".$datos['fechaPrevista'.$i]."', '".$datos['fechaFin'.$i]."','".$datos['horaFin'.$i]."',
				'".$datos['tiempo'.$i]."','$codigoS');");
		}
		cierraBD();
	}
	return $res;
}


function imprimeTiempos(){
	$tipos=array('CREACION'=>'Creación de Software','DEMO'=>'Personalización','MODIFICACION'=>'Modificaciones y/o mejoras','INCIDENCIA'=>'Incidencias');
	$consulta=consultaBD("SELECT software.codigo AS codigo, clientes.empresa, tipoTarea, usuarios.nombre, actividad, fechaInicio, horaInicio, tareasSoftware.fechaPrevista, fechaFin, horaFin, tiempo FROM ((software INNER JOIN clientes ON software.codigoCliente=clientes.codigo) INNER JOIN usuarios ON software.codigoUsuario=usuarios.codigo) INNER JOIN tareasSoftware ON software.codigo=tareasSoftware.codigoSoftware ORDER BY fechaFin, horaFin DESC",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "<tr>
				<td> ".$datos['empresa']." </td>
				<td> ".$tipos[$datos['tipoTarea']]." </td>
				<td> ".$datos['nombre']."</td>
				<td> ".$datos['actividad']." </td>
				<td> ".formateaFechaWeb($datos['fechaInicio'])." </td>
				<td> ".formateaHoraWeb($datos['horaInicio'])." </td>
				<td> ".formateaFechaWeb($datos['fechaPrevista'])." </td>
				<td> ".formateaFechaWeb($datos['fechaFin'])." </td>
				<td> ".formateaHoraWeb($datos['horaFin'])." </td>
				<td> ".$datos['tiempo']." </td>
				<td class='centro'>
	        		<a href='detallesSoftware.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver Tarea</i></a>
	        	</td>
			 </tr>";
	}
}

function creaEstadisticasAlumnosPendientes(){
	$datos=array();

	conexionBD();	
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION'){
		$where=compruebaPerfilParaWhere();
	}

	$consulta=consultaBD("SELECT COUNT(alumnos.codigo) AS codigo FROM alumnos
	$where AND alumnos.codigo NOT IN (SELECT alumnos.codigo FROM alumnos INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno);");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function imprimeAlumnosPendientes(){
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION'){
		$where=compruebaPerfilParaWhere('alumnos.codigoUsuario');
	}

	conexionBD();

	$consulta=consultaBD("SELECT alumnos.codigo, nombre, apellidos, alumnos.telefono, alumnos.mail, empresa, clientes.codigo AS codigoCliente FROM 
	(alumnos LEFT JOIN ventas ON alumnos.codigoVenta=ventas.codigo) 
	LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo 
	$where AND alumnos.codigo NOT IN (SELECT alumnos.codigo FROM alumnos INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno);;");
	$datos=mysql_fetch_assoc($consulta);


	while($datos!=0){
		$fecha = date('Y-m-d');
		$nuevafecha = date('Y-m-d',strtotime ( '+3 days' , strtotime ( $fecha ) ) );
		
		echo "
		<tr>
        	<td> ".$datos['apellidos'].", ".$datos['nombre']."</td>
        	<td> ".formateaTelefono($datos['telefono'])." </td>
        	<td> ".$datos['mail']." </td>
        	<td> <a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a> </td>
        	<td class='centro'>
        		<a href='detallesAlumno.php?codigo=".$datos['codigo']."&pendiente=1' class='btn btn-primary'><i class='icon-edit'></i> Datos</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function insertaAlumnoSinVenta(){
	$res=true;

	$datos=arrayFormulario();
	$datos['numSS']=$datos['numSS1'].$datos['numSS2'];

	if(isset($_SESSION["verifica"]) && $_SESSION["verifica"] == 1){
		conexionBD();
		$consulta=consultaBD("INSERT INTO alumnos VALUES (NULL, '".$datos['nombre']."', '".$datos['apellidos']."', '".$datos['dni']."', '".$datos['mail']."', '".$datos['telefono']."', '".$datos['sexo']."',
			'".$datos['fechaNac']."', '".$datos['numSS']."', '".$datos['discapacidad']."', '".$datos['vTerrorismo']."', '".$datos['vViolencia']."', '".$datos['movil']."', '".$datos['horario']."', 
			'".$datos['estudios']."', '".$datos['categoria']."',
			'".$datos['cotizacion']."','0000-00-00', NULL, '".$_SESSION['codigoS']."');");
			echo mysql_error();
			$codigoAlumno = mysql_insert_id();
		cierraBD();
		
		insertaObservaciones($codigoAlumno);

		if(!$consulta){
			$res=false;
			echo mysql_error();
		}
		unset($_SESSION['verifica']);
	}else{
		$res=false;
	}

	return $res;
}

function insertaObservaciones($codigoAlumno){
	$datos=arrayFormulario();

	eliminaObservaciones($codigoAlumno);

	$i=0;
	while(isset($datos['fecha'.$i])){
		$res=consultaBD("INSERT INTO observaciones_alumnos VALUES(NULL,'".$datos['fecha'.$i]."','".$datos['observacion'.$i]."', '$codigoAlumno');", true);
		$i++;
	}

}

function eliminaObservaciones($codigoAlumno){
	consultaBD("DELETE FROM observaciones_alumnos WHERE codigoAlumno='$codigoAlumno';", true);
}

function registraFichero($datos,$tipo){
	$res=true;
	$nombreFichero=subeDocumento('fichero',time(),'documentos/ficheros');
	if($nombreFichero=='NO'){
		$res=false;
	}
	else{
		$_POST['ruta']=$nombreFichero;
		$_POST['tipo']=$tipo;
		$_POST['codigoClienteAlumno']=$datos['codigo'];
		$res=insertaDatos('ficheros');
	}

	return $res;
}

//Parte de exportación  a Excel ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function compruebaOpcionExcel($seccion){//Se podría incluir dentro de la función compruebaOpcionEliminar(), pero de momento no lo hago por si la opción de exportar a Excel se amplía a otros usuarios.
	if($_SESSION['tipoUsuario']=='ADMIN'){
		echo '<a href="generaExcel.php?codigo='.$seccion.'" class="shortcut"><i class="shortcut-icon icon-download-alt"></i><span class="shortcut-label">Exportar a Excel</span> </a>';
		//echo '<a href="#" onclick="alert(\'Función en desarrollo\')" class="shortcut"><i class="shortcut-icon icon-download-alt"></i><span class="shortcut-label">Exportar a Excel</span> </a>';
	}
}


function exportarExcel($objPHPExcel,$seccion){
	$tablas=array('','clientes','clientes','ventas','cursos','alumnos','tareas','incidencias','correos','informes','usuarios','objetivos');
	$tam=array('clientes'=>19,'ventas'=>8,'cursos'=>5,'tareas'=>10,'incidencias'=>8,'correos'=>8,'usuarios'=>9,'objetivos'=>5);
	$tabla=$tablas[$seccion];

	$query=obtieneConsultaSeccion($seccion);
	

	conexionBD();
	$consulta=consultaBD($query);
	cierraBD();
	$letras=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

	encabezadoExcel($objPHPExcel,$tabla,$letras,4);

	$fila=5;

	$datos=mysql_fetch_array($consulta,MYSQL_NUM);//Le paso como parámetro MYSQL_NUM para que solo obtenga un array númerico (por defecto devuelve númerico y asociativo) y el count sea 21 (al final sustituí el count por $tam)
	while($datos!=false){
		if($tabla=="cursos"){
			$fila--;
			encabezadoExcel($objPHPExcel,$tabla,$letras,$fila);
			$fila++;
		}

		for($i=1,$j=0;$i<$tam[$tabla];$i++,$j++){//Empieza en 1 y acaba en $tam para no imprimir el código ni la clave ajena
			if (strpos($datos[$i],'-') !== false) {
				$datos[$i]=formateaFechaWeb($datos[$i]);
			}
			$objPHPExcel->getActiveSheet()->getCell($letras[$j].$fila)->setValue($datos[$i]);
		}

		if($tabla=="cursos"){
			/*$fila++;
			encabezadoExcel($objPHPExcel,$tabla,$letras,$fila);*/
			$fila++;
			$fila=datosAlumnoCursoExcel($objPHPExcel, $datos[0], $fila, $letras);
			$fila++;
		}

		$fila++;
		$datos=mysql_fetch_array($consulta,MYSQL_NUM);
	}

	$j--;//Porque ahora $j vale uno más que la posición de la última letra.
	$fila--;
	if($tabla=='cursos'){
		$fila-=2;
	}
	bordeCeldasExcel($objPHPExcel,$fila,$letras[$j]);
	
}


function obtieneConsultaSeccion($seccion){
	if($seccion==1){
		$consulta="SELECT clientes.codigo, clientes.empresa, clientes.cif, clientes.direccion, clientes.cp, clientes.localidad, clientes.provincia, clientes.telefono, clientes.movil, clientes.mail, clientes.fax, clientes.contacto, clientes.cargo, clientes.comercial, clientes.observaciones, clientes.sector, clientes.tipoEmpresa, clientes.fechaRegistro, CONCAT (usuarios.nombre, ' ', apellidos) AS nombreUsuario FROM clientes INNER JOIN usuarios ON clientes.codigoUsuario=usuarios.codigo WHERE activo='NO' ORDER BY fechaRegistro DESC;";
	}
	elseif($seccion==2){
		$consulta="SELECT clientes.codigo, clientes.empresa, clientes.cif, clientes.direccion, clientes.cp, clientes.localidad, clientes.provincia, clientes.telefono, clientes.movil, clientes.mail, clientes.fax, clientes.contacto, clientes.cargo, clientes.comercial, clientes.observaciones, clientes.sector, clientes.tipoEmpresa, clientes.fechaRegistro, CONCAT (usuarios.nombre, ' ', apellidos) AS nombreUsuario FROM clientes INNER JOIN usuarios ON clientes.codigoUsuario=usuarios.codigo WHERE activo='SI' ORDER BY fechaRegistro DESC;";
	}
	elseif($seccion==3){
		$consulta="SELECT ventas.codigo, empresa, tipo, concepto, precio, fecha, extras, ventas.observaciones, codigoCliente FROM ventas INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo;";
	}
	elseif($seccion==4){
		$consulta="SELECT cursos.codigo AS codigo, denominacion, CONCAT(apellidos,', ',nombre), fechaInicio, fechaFin, tutores.codigo FROM (accionFormativa INNER JOIN cursos ON accionFormativa.codigoInterno=cursos.codigoaccionFormativa) INNER JOIN tutores ON cursos.codigoTutor=tutores.codigo ORDER BY fechaInicio DESC;";
	}
	elseif($seccion==6){//La de alumnos (5) tiene su propia función (porque va dentro del listado de cursos).
		$consulta="SELECT tareas.codigo, clientes.empresa, clientes.contacto, clientes.telefono, clientes.localidad, tareas.tarea, tareas.fechaInicio, tareas.estado, tareas.prioridad, tareas.observaciones FROM clientes INNER JOIN tareas ON clientes.codigo=tareas.codigoCliente ORDER BY tareas.fechaInicio ASC;";
	}
	elseif($seccion==7){
		$consulta="SELECT incidencias.codigo, clientes.empresa, clientes.contacto, clientes.telefono, incidencias.departamento, incidencias.estado, incidencias.prioridad, incidencias.descripcion FROM clientes INNER JOIN incidencias ON clientes.codigo=incidencias.codigoCliente ORDER BY incidencias.prioridad;";
	}
	elseif($seccion==8){
		$consulta="SELECT correos.codigo AS codigo, CONCAT(apellidos,', ',nombre) AS remitente, destinatarios, fecha, hora, asunto, mensaje, adjunto FROM correos INNER JOIN usuarios ON correos.codigoUsuario=usuarios.codigo ORDER BY fecha, hora DESC;";
	}
	elseif($seccion==10){//Faltan los informes (9)
		$consulta="SELECT codigo, nombre, apellidos, dni, email, telefono, usuario, clave, tipo FROM usuarios;";
	}
	elseif($seccion==11){
		$consulta="SELECT objetivos.codigo, CONCAT(apellidos,', ',nombre), fechaInicio, fechaFin, valor FROM (objetivos INNER JOIN objetivos_asignados_comerciales ON objetivos.codigo=objetivos_asignados_comerciales.codigoObjetivo) INNER JOIN usuarios ON objetivos_asignados_comerciales.codigoUsuario=usuarios.codigo;";
	}

	return $consulta;
}

function encabezadoExcel($objPHPExcel,$tabla,$letras,$fila){
	if($tabla=='clientes'){
		$columnas=array('Empresa','CIF','Dirección','CP','Localidad','Provincia','Teléfono','Móvil','eMail','Fax','Contacto','Cargo','Comercial','Observaciones','Sector','Tipo de empresa','Fecha de registro','Trabajador asignado');
	}
	elseif($tabla=='ventas'){
		$columnas=array('Empresa','Tipo de venta','Concepto','Precio','Fecha','Extras','Observaciones');
	}
	elseif($tabla=='cursos'){
		$columnas=array('Acción Formativa','Tutor','Fecha de Inicio','Fecha de Fin');
	}
	elseif($tabla=='alumnos'){
		$columnas=array('Nombre','Apellidos','DNI','eMail','Teléfono','Sexo','Fecha de nacimiento','Número de SS','¿Discapacidad?','¿Vícitma de terrorismo?','¿Víctima de violencia de género?','Domicilio','CP','Localidad','Provincia','Móvil','Horario de trabajo','Nivel de estudios','Categoría','Grupo de cotización');
	}
	elseif($tabla=='tareas'){
		$columnas=array('Empresa','Localidad','Contacto','Teléfono','Tarea','Fecha de Inicio','Estado','Prioridad','Observaciones');
	}
	elseif($tabla=='incidencias'){
		$columnas=array('Empresa','Contacto','Teléfono','Departamento','Estado','Prioridad','Descripcion');
	}
	elseif($tabla=='correos'){
		$columnas=array('Usuario remitente','Destinatario/s','Fecha de envío','Hora','Asunto','Mensaje','Adjunto');
	}
	elseif($tabla=='usuarios'){
		$columnas=array('Nombre','Apellidos','DNI','eMail','Teléfono','Usuario','Clave','Tipo');
	}
	elseif($tabla=='objetivos'){
		$columnas=array('Comercial','Fecha de Inicio','Fecha de Fin','Ventas a conseguir');
	}

	for($i=0;$i<count($columnas);$i++){
		$objPHPExcel->getActiveSheet()->getCell($letras[$i].$fila)->setValue($columnas[$i]);
		$objPHPExcel->getActiveSheet()->getColumnDimension($letras[$i])->setAutoSize(true);//Ajusta el ancho de la columna al contenido
	}
	$i--;//Porque ahora $i=count($columnas).

	$rango=$letras[0].$fila.':'.$letras[$i].$fila;
	$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Pone el alineado centrado
	$objPHPExcel->getActiveSheet()->getStyle($rango)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CCC0DA');//Les pone el fondo primary
}


function bordeCeldasExcel($objPHPExcel,$fila,$letra,$inicio=false){
	$estiloBorde = array(
       'borders' => array(
            'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => '00000000'),
			),
		)
	);

	if(!$inicio){
		$objPHPExcel->getActiveSheet()->getStyle('A4:'.$letra.$fila)->applyFromArray($estiloBorde);
	}
	else{
		$objPHPExcel->getActiveSheet()->getStyle($inicio.':'.$letra.$fila)->applyFromArray($estiloBorde);		
	}
}



function datosAlumnoCursoExcel($objPHPExcel, $codigoCurso, $fila, $letras){
	conexionBD();
	$consultaA=consultaBD("SELECT alumnos.codigo, nombre, apellidos, dni, mail, telefono, sexo, fechaNac, numSS, discapacidad, vTerrorismo, vViolencia, alumnos.domicilio, alumnos.cp, localidad, provincia, movil, horario, estudios, categoria, cotizacion, codigoVenta FROM (alumnos INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno) INNER JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo WHERE codigoCurso='$codigoCurso' ORDER BY apellidos;");
	cierraBD();

	$datosA=mysql_fetch_array($consultaA,MYSQL_NUM);

	$inicio=$fila;
	encabezadoExcel($objPHPExcel,'alumnos',$letras,$fila);
	$fila++;

	while($datosA!=false){
		for($i=1,$j=0;$i<21;$i++,$j++){
			$objPHPExcel->getActiveSheet()->getCell($letras[$j].$fila)->setValue($datosA[$i]);
		}
		$fila++;
		$datosA=mysql_fetch_array($consultaA,MYSQL_NUM);
	}
	bordeCeldasExcel($objPHPExcel,$fila-1,'T','A'.$inicio);

	return $fila;
}
//Fin parte exportación a Excel ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function creaEstadisticasAccionesFormativas(){
	$datos=array();

	conexionBD();	
	//$codigoU=$_SESSION['codigoS'];
	$where='';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION'){
		$where=compruebaPerfilParaWhere();
	}

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM accionFormativa $where;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function imprimeAccionesFormativas(){
	//$codigoU=$_SESSION['codigoS'];
	$where='';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION'){
		$where=compruebaPerfilParaWhere(true);
	}

	conexionBD();

	$consulta=consultaBD("SELECT codigo, denominacion,  horas, modalidad, codigoInterno FROM accionFormativa $where ORDER BY denominacion;");
	echo mysql_error();
	$datos=mysql_fetch_assoc($consulta);
	
	$tipos=array("PRESENCIAL"=>"Presencial", "DISTANCIA"=>"A distancia", "MIXTA"=>"Mixta", "TELE"=>"Teleformación", "WEBINAR"=>"Webinar");

	while($datos!=0){

		echo "
		<tr>
			<td> ".$datos['codigoInterno']." </td>
        	<td> ".$datos['denominacion']." </td>
        	<td> ".$datos['horas']."</td>
			<td> ".$tipos[$datos['modalidad']]."</td>
        	<td class='centro'>
        		<a href='detallesAccionFormativa.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver detalles</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function creaEstadisticasTutores(){
	$datos=array();

	conexionBD();	
	//$codigoU=$_SESSION['codigoS'];
	$where='';
	if($_SESSION['tipoUsuario']!='ADMIN'&&$_SESSION['tipoUsuario']!='FORMACION'){
		$where=compruebaPerfilParaWhere();
	}

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM tutores $where;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function imprimeTutores(){
	//$codigoU=$_SESSION['codigoS'];

	conexionBD();

	$consulta=consultaBD("SELECT codigo, nombre, apellidos, dni, telefono, email FROM tutores ORDER BY apellidos, nombre;");
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){

		echo "
		<tr>
        	<td> ".$datos['apellidos'].", ".$datos['nombre']." </td>
        	<td> ".$datos['dni']." </td>
        	<td> ".$datos['telefono']."</td>
			<td> ".$datos['email']."</td>
        	<td class='centro'>
        		<a href='detallesTutor.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver detalles</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function datosTutor($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM tutores WHERE codigo='$codigo';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}

function actualizaTutor(){
	$res=true;

	$datos=arrayFormulario();
	
	conexionBD();
	
	$lunes=compruebaExistencia($datos, 'lunes');
	$martes=compruebaExistencia($datos, 'martes');
	$miercoles=compruebaExistencia($datos, 'miercoles');
	$jueves=compruebaExistencia($datos, 'jueves');
	$viernes=compruebaExistencia($datos, 'viernes');
	$sabado=compruebaExistencia($datos, 'sabado');
	$domingo=compruebaExistencia($datos, 'domingo');

	$consulta=consultaBD("UPDATE tutores SET nombre='".$datos['nombre']."', apellidos='".$datos['apellidos']."', dni='".$datos['dni']."', telefono='".$datos['telefono']."', email='".$datos['email']."', especialidad='".$datos['especialidad']."',
	lunes='$lunes', martes='$martes', miercoles='$miercoles', jueves='$jueves', viernes='$viernes', sabado='$sabado', domingo='$domingo',
	horaInicio='".$datos['horaInicio']."', horaFin='".$datos['horaFin']."', horaInicioTarde='".$datos['horaInicioTarde']."', horaFinTarde='".$datos['horaFinTarde']."', horasTutoria='".$datos['horasTutoria']."'
	WHERE codigo='".$datos['codigo']."';");

	if(!$consulta){
		$res=false;
	}
	
	cierraBD();

	return $res;
}

function eliminaAccionFormativa(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM accionFormativa WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
		}
	}

	cierraBD();

	return $res;	
}

function eliminaTutor(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$consulta=consultaBD("DELETE FROM tutores WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
		}
	}

	cierraBD();

	return $res;	
}


function datosaccionFormativa($codigo){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM accionFormativa WHERE codigo=$codigo;");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}

function creaInputsOcultos(){
	$where=compruebaPerfilParaWhere();
	conexionBD();
	$consulta=consultaBD("SELECT codigo, codigoInterno, denominacion, modalidad, horas FROM accionFormativa $where ORDER BY denominacion;");
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=false){
		$consultaDos=consultaBD("SELECT MAX(codigoInterno) AS numero FROM cursos WHERE codigoaccionFormativa='".$datos['codigoInterno']."';",false,true);
		$numeroNuevo=$consultaDos['numero']+1;
		divOculto($numeroNuevo,'grupo-'.$datos['codigoInterno']);
		divOculto($datos['horas'],'horas-'.$datos['codigoInterno']);
		$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function generaXMLParticipantesTodos(){
	$participantes='';

	conexionBD();
	$consulta=consultaBD("SELECT dni, nombre, apellidos, numSS, sexo, fechaNac, denominacion, cursos.codigo AS grupo FROM ((alumnos INNER JOIN alumnos_registrados_cursos ON 
		alumnos.codigo=alumnos_registrados_cursos.codigoAlumno)	INNER JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo) INNER JOIN 
		accionFormativa ON cursos.codigoaccionFormativa=accionFormativa.codigoInterno;");

	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	$fichero='FicheroParticipantes-'.time().'.xml';

	while($datos!=false){
		$nodoParticipante=file_get_contents('documentos/nodoParticipante.xml');

		$espacio=strpos($datos['apellidos'],' ');
		$datos['apellido1']=substr($datos['apellidos'],0,$espacio);
		$datos['apellido2']=substr($datos['apellidos'],$espacio+1);

		$nodoParticipante=str_replace('${dni}', $datos['dni'], $nodoParticipante);
		$nodoParticipante=str_replace('${apellido1}', $datos['apellido1'], $nodoParticipante);
		$nodoParticipante=str_replace('${apellido2}', $datos['apellido2'], $nodoParticipante);
		$nodoParticipante=str_replace('${nombre}', $datos['nombre'], $nodoParticipante);
		$nodoParticipante=str_replace('${numSS}', $datos['numSS'], $nodoParticipante);
		$nodoParticipante=str_replace('${sexo}', $datos['sexo'], $nodoParticipante);
		$nodoParticipante=str_replace('${fechaNac}', formateaFechaWeb($datos['fechaNac']), $nodoParticipante);

		$participantes.=$nodoParticipante;
		$datos=mysql_fetch_assoc($consulta);
	}

	unset($nodoParticipante);
	$xml=file_get_contents('documentos/plantillaParticipante.xml');
	$xml=str_replace('${participante}', $participantes, $xml);

	file_put_contents("documentos/$fichero",$xml);

	unset($participantes);
	unset($xml);//Borro las variables para liberar memoria.

	return $fichero;
}

function generaXMLGrupos(){
	$grupos='';

	conexionBD();
	$consulta=consultaBD("SELECT DISTINCT accionFormativa.*, fechaInicio, fechaFin, horaInicio, horaFin, titularidad, mediosPropios, mediosEntidad, mediosCentro, cif, centro, tlf, domicilio, cp, poblacion, horasTutoria, 
	lunes, martes, miercoles, jueves, viernes, sabado, domingo, tutores.nombre AS nombreTutor, tutores.dni, tutores.apellidos
	cifTutoria, centroTutoria, tlfTutoria, domicilioTutoria, cpTutoria, poblacionTutoria, horaInicioFormacion, horaFinFormacion, horaInicioFormacionTarde, horaFinFormacionTarde, horasFormacion,
	lunesFormacion, martesFormacion, miercolesFormacion, juevesFormacion, viernesFormacion, sabadoFormacion, domingoFormacion, cifDistancia, centroGestorDistancia, tlfDistancia, domicilioDistancia, 
	cpDistancia, poblacionDistancia, horaInicioFormacionDistancia, horaFinFormacionDistancia, horaInicioFormacionTardeDistancia, horaFinFormacionTardeDistancia, horasFormacionDistancia, lunesFormacionDistancia,
	martesFormacionDistancia, miercolesFormacionDistancia, juevesFormacionDistancia, viernesFormacionDistancia, sabadoFormacionDistancia, domingoFormacionDistancia, responsable, tlfResponsable, cursos.codigoInterno AS grupo, cursos.medios
	FROM cursos INNER JOIN accionFormativa ON cursos.codigoAccionFormativa=accionFormativa.codigoInterno
	INNER JOIN alumnos_registrados_cursos ON cursos.codigo=alumnos_registrados_cursos.codigoCurso
	INNER JOIN tutores ON tutores.codigo=cursos.codigoTutor;");
	echo mysql_error();
	
	$fichero='FicheroInicioGrupo'.time().'.xml';

	$datos=mysql_fetch_assoc($consulta);
	
	while(isset($datos['fechaInicio'])){
	
		$consultaDos=consultaBD("SELECT COUNT(codigoAlumno) AS numParticipantes FROM alumnos_registrados_cursos WHERE codigoCurso='$codigoCurso';");
		$numParticipantes=mysql_fetch_assoc($consultaDos);

		$nodoParticipante=file_get_contents('documentos/nodoGrupo.xml');

		$espacio=strpos($datos['apellidos'],' ');
		$datos['apellido1']=substr($datos['apellidos'],0,$espacio);
		$datos['apellido2']=substr($datos['apellidos'],$espacio+1);
		
		if($datos['modalidad']=="MIXTA"||$datos['modalidad']=="DISTANCIA"){
			$consultaTutor=consultaBD("SELECT tutores.nombre AS nombreTutor, tutores.dni, tutores.apellidos 
			FROM tutores INNER JOIN cursos ON cursos.tutorDistancia=tutores.codigo 
			WHERE cursos.codigo='$codigoCurso';");
			$consultaTutor=mysql_fetch_assoc($consultaTutor);
			$nombreTutorDistancia=$consultaTutor['nombreTutor'];
			$dniTutorDistancia=$consultaTutor['dni'];
			$espacio=strpos($consultaTutor['apellidos'],' ');
			$apellidoUno=substr($consultaTutor['apellidos'],0,$espacio);
			$apellidoDos=substr($consultaTutor['apellidos'],$espacio+1);
			$numHorasTutorDistancia=$datos['horasTutoria'];
		}else{
			$nombreTutorDistancia="";
			$dniTutorDistancia="";
			$apellidoUno="";
			$apellidoDos="";
			$numHorasTutorDistancia="";
		}
		
		
		if($datos['modalidad']=="TELE"){
			$nodoParticipante=str_replace('${idAccion}', $datos['codigoInterno'], $nodoParticipante);
			$nodoParticipante=str_replace('${idGrupo}', $datos['grupo'], $nodoParticipante);
			$nodoParticipante=str_replace('${descripcion}', $datos['contenidos'], $nodoParticipante);
			$titularidad=array("publico"=>"false", "privado"=>"true");
			$nodoParticipante=str_replace('${titularidad}', $titularidad[$datos['titularidad']], $nodoParticipante);
			$medios=array("SI"=>"true", "NO"=>"false");
			$nodoParticipante=str_replace('${mediosPropios}', $datos['medios'], $nodoParticipante);
			$nodoParticipante=str_replace('${numParticipantes}', $numParticipantes['numParticipantes'], $nodoParticipante);
			$nodoParticipante=str_replace('${fechaInicio}', formateaFechaWeb($datos['fechaInicio']), $nodoParticipante);
			$nodoParticipante=str_replace('${fechaFin}', formateaFechaWeb($datos['fechaFin']), $nodoParticipante);
			$nodoParticipante=str_replace('${responsable}', $datos['responsable'], $nodoParticipante);
			$nodoParticipante=str_replace('${telefono}', $datos['tlfResponsable'], $nodoParticipante);
			$nodoParticipante=str_replace('${cif}', "", $nodoParticipante);
			$nodoParticipante=str_replace('${nombreCentro}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${direccion}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${cp}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${localidad}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${horasFormacion}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${horaInicioFormacion}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${horaFinFormacion}', '', $nodoParticipante);	
			$nodoParticipante=str_replace('${cifTutoria}', "", $nodoParticipante);
			$nodoParticipante=str_replace('${nombreCentroTutoria}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${direccionTutoria}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${cpTutoria}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${localidadTutoria}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${horas}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${horaInicioManana}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${horaFinManana}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${dias}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${numHoras}', $datos['horasTutoria'], $nodoParticipante);
			$nodoParticipante=str_replace('${dni}', $datos['dni'], $nodoParticipante);
			$nodoParticipante=str_replace('${nombre}', $datos['nombreTutor'], $nodoParticipante);
			$nodoParticipante=str_replace('${apell1}', $datos['apellido1'], $nodoParticipante);
			$nodoParticipante=str_replace('${apell2}', $datos['apellido2'], $nodoParticipante);
			
			$nodoParticipante=str_replace('${numHorasTutorDistancia}', $numHorasTutorDistancia, $nodoParticipante);
			$nodoParticipante=str_replace('${dniTutorDistancia}', $dniTutorDistancia, $nodoParticipante);
			$nodoParticipante=str_replace('${nombreTutorDistancia}', $nombreTutorDistancia, $nodoParticipante);
			$nodoParticipante=str_replace('${apell1TutorDistancia}', $apellidoUno, $nodoParticipante);
			$nodoParticipante=str_replace('${apell2TutorDistancia}', $apellidoDos, $nodoParticipante);
			
			$nodoParticipante=str_replace('${cifTeleformacion}', $datos['cif'], $nodoParticipante);
			$nodoParticipante=str_replace('${nombreCentroTeleformacion}', $datos['centro'], $nodoParticipante);
			$nodoParticipante=str_replace('${direccionTeleformacion}', $datos['domicilio'], $nodoParticipante);
			$nodoParticipante=str_replace('${cpTeleformacion}', $datos['cp'], $nodoParticipante);
			$nodoParticipante=str_replace('${localidadTeleformacion}', $datos['poblacion'], $nodoParticipante);
			$nodoParticipante=str_replace('${horasFormacionDistancia}', $datos['horasTutoria'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicio'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaFinFormacionDistancia}', $datos['horaFin'], $nodoParticipante);
			$nodoParticipante=str_replace('${telefonoTeleformacion}', $datos['tlf'], $nodoParticipante);
			
			$nodoParticipante=str_replace('${cifDistancia}', $datos['cifDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${nombreCentroDistancia}', $datos['centroGestorDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${direccionDistancia}', $datos['domicilioDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${cpDistancia}', $datos['cpDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${localidadDistancia}', $datos['poblacionDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${telefonoDistancia}', $datos['tlfDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${horasFormacionDistancia}', $datos['horasFormacionDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicioFormacionDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaFinFormacionDistancia}', $datos['horaFinFormacionDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicioFormacionDistancia'], $nodoParticipante);
			
			$diasLunes=array("SI"=>"L", "NO"=>"");
			$diasMartes=array("SI"=>"M", "NO"=>"");
			$diasMiercoles=array("SI"=>"X", "NO"=>"");
			$diasJueves=array("SI"=>"J", "NO"=>"");
			$diasViernes=array("SI"=>"V", "NO"=>"");
			$diasSabado=array("SI"=>"S", "NO"=>"");
			$diasDomingo=array("SI"=>"D", "NO"=>"");
			
			$lunes=$diasLunes[$datos['lunesFormacion']];			
			$martes=$diasMartes[$datos['martesFormacion']];			
			$miercoles=$diasMiercoles[$datos['miercolesFormacion']];			
			$jueves=$diasJueves[$datos['juevesFormacion']];			
			$viernes=$diasViernes[$datos['viernesFormacion']];			
			$sabado=$diasSabado[$datos['sabadoFormacion']];			
			$domingo=$diasDomingo[$datos['domingoFormacion']];
			
			$total=$lunes."".$martes."".$miercoles."".$jueves."".$viernes;
			$nodoParticipante=str_replace('${diasFormacion}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${dias}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${diasFormacionDistancia}', $total, $nodoParticipante);
			
		}else{
			$nodoParticipante=str_replace('${idAccion}', $datos['codigoInterno'], $nodoParticipante);
			$nodoParticipante=str_replace('${idGrupo}', $datos['grupoAcciones'], $nodoParticipante);
			$nodoParticipante=str_replace('${descripcion}', $datos['contenidos'], $nodoParticipante);
			$titularidad=array("publico"=>"false", "privado"=>"true");
			$nodoParticipante=str_replace('${titularidad}', $titularidad[$datos['titularidad']], $nodoParticipante);
			$medios=array("SI"=>"true", "NO"=>"false");
			$nodoParticipante=str_replace('${mediosPropios}', $datos['medios'], $nodoParticipante);
			$nodoParticipante=str_replace('${numParticipantes}', $numParticipantes['numParticipantes'], $nodoParticipante);
			$nodoParticipante=str_replace('${fechaInicio}', formateaFechaWeb($datos['fechaInicio']), $nodoParticipante);
			$nodoParticipante=str_replace('${fechaFin}', formateaFechaWeb($datos['fechaFin']), $nodoParticipante);
			$nodoParticipante=str_replace('${responsable}', $datos['responsable'], $nodoParticipante);
			$nodoParticipante=str_replace('${telefono}', $datos['tlfResponsable'], $nodoParticipante);
			$nodoParticipante=str_replace('${cif}', $datos['cif'], $nodoParticipante);
			$nodoParticipante=str_replace('${nombreCentro}', $datos['centro'], $nodoParticipante);
			$nodoParticipante=str_replace('${direccion}', $datos['domicilio'], $nodoParticipante);
			$nodoParticipante=str_replace('${cp}', $datos['cp'], $nodoParticipante);
			$nodoParticipante=str_replace('${localidad}', $datos['poblacion'], $nodoParticipante);
			$nodoParticipante=str_replace('${horasFormacion}', $datos['horasFormacion'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaInicioFormacion}', $datos['horaInicioFormacion'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaFinFormacion}', $datos['horaFinFormacion'], $nodoParticipante);	
			$nodoParticipante=str_replace('${cifTutoria}', $datos['cifTutoria'], $nodoParticipante);
			$nodoParticipante=str_replace('${nombreCentroTutoria}', $datos['centroTutoria'], $nodoParticipante);
			$nodoParticipante=str_replace('${direccionTutoria}', $datos['domicilioTutoria'], $nodoParticipante);
			$nodoParticipante=str_replace('${cpTutoria}', $datos['cpTutoria'], $nodoParticipante);
			$nodoParticipante=str_replace('${localidadTutoria}', $datos['poblacionTutoria'], $nodoParticipante);
			$nodoParticipante=str_replace('${horas}', $datos['horasTutoria'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaInicioManana}', $datos['horaInicio'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaFinManana}', $datos['horaFin'], $nodoParticipante);
			$nodoParticipante=str_replace('${numHoras}', $datos['horasTutoria'], $nodoParticipante);
			$nodoParticipante=str_replace('${dni}', $datos['dni'], $nodoParticipante);
			$nodoParticipante=str_replace('${nombre}', $datos['nombreTutor'], $nodoParticipante);
			$nodoParticipante=str_replace('${apell1}', $datos['apellido1'], $nodoParticipante);
			$nodoParticipante=str_replace('${apell2}', $datos['apellido2'], $nodoParticipante);
			
			$nodoParticipante=str_replace('${numHorasTutorDistancia}', $numHorasTutorDistancia, $nodoParticipante);
			$nodoParticipante=str_replace('${dniTutorDistancia}', $dniTutorDistancia, $nodoParticipante);
			$nodoParticipante=str_replace('${nombreTutorDistancia}', $nombreTutorDistancia, $nodoParticipante);
			$nodoParticipante=str_replace('${apell1TutorDistancia}', $apellidoUno, $nodoParticipante);
			$nodoParticipante=str_replace('${apell2TutorDistancia}', $apellidoDos, $nodoParticipante);
			
			$nodoParticipante=str_replace('${cifTeleformacion}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${nombreCentroTeleformacion}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${direccionTeleformacion}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${cpTeleformacion}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${localidadTeleformacion}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${horasFormacionDistancia}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${horaInicioFormacionDistancia}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${horaFinFormacionDistancia}', '', $nodoParticipante);
			$nodoParticipante=str_replace('${telefonoTeleformacion}', '', $nodoParticipante);
			
			$nodoParticipante=str_replace('${cifDistancia}', $datos['cifDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${nombreCentroDistancia}', $datos['centroGestorDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${direccionDistancia}', $datos['domicilioDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${cpDistancia}', $datos['cpDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${localidadDistancia}', $datos['poblacionDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${telefonoDistancia}', $datos['tlfDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${horasFormacionDistancia}', $datos['horasFormacionDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicioFormacionDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaFinFormacionDistancia}', $datos['horaFinFormacionDistancia'], $nodoParticipante);
			$nodoParticipante=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicioFormacionDistancia'], $nodoParticipante);
			
			$diasLunes=array("SI"=>"L", "NO"=>"");
			$diasMartes=array("SI"=>"M", "NO"=>"");
			$diasMiercoles=array("SI"=>"X", "NO"=>"");
			$diasJueves=array("SI"=>"J", "NO"=>"");
			$diasViernes=array("SI"=>"V", "NO"=>"");
			$diasSabado=array("SI"=>"S", "NO"=>"");
			$diasDomingo=array("SI"=>"D", "NO"=>"");
			
			$lunes=$diasLunes[$datos['lunes']];			
			$martes=$diasMartes[$datos['martes']];			
			$miercoles=$diasMiercoles[$datos['miercoles']];			
			$jueves=$diasJueves[$datos['jueves']];			
			$viernes=$diasViernes[$datos['viernes']];			
			$sabado=$diasSabado[$datos['sabado']];			
			$domingo=$diasDomingo[$datos['domingo']];
			
			$totalTutoria=$lunes."".$martes."".$miercoles."".$jueves."".$viernes;
			
			$lunesFormacion=$diasLunes[$datos['lunesFormacion']];			
			$martesFormacion=$diasMartes[$datos['martesFormacion']];			
			$miercolesFormacion=$diasMiercoles[$datos['miercolesFormacion']];			
			$juevesFormacion=$diasJueves[$datos['juevesFormacion']];			
			$viernesFormacion=$diasViernes[$datos['viernesFormacion']];			
			$sabadoFormacion=$diasSabado[$datos['sabadoFormacion']];			
			$domingoFormacion=$diasDomingo[$datos['domingoFormacion']];
			
			$totalFormacion=$lunesFormacion."".$martesFormacion."".$miercolesFormacion."".$juevesFormacion."".$viernesFormacion;
			
			$lunesFormacionDistancia=$diasLunes[$datos['lunesFormacionDistancia']];			
			$martesFormacionDistancia=$diasMartes[$datos['martesFormacionDistancia']];			
			$miercolesFormacionDistancia=$diasMiercoles[$datos['miercolesFormacionDistancia']];			
			$juevesFormacionDistancia=$diasJueves[$datos['juevesFormacionDistancia']];			
			$viernesFormacionDistancia=$diasViernes[$datos['viernesFormacionDistancia']];			
			$sabadoFormacionDistancia=$diasSabado[$datos['sabadoFormacionDistancia']];			
			$domingoFormacionDistancia=$diasDomingo[$datos['domingoFormacionDistancia']];
			
			$totalFormacionDistancia=$lunesFormacionDistancia."".$martesFormacionDistancia."".$miercolesFormacionDistancia."".$juevesFormacionDistancia."".$viernesFormacionDistancia;			
			
			$nodoParticipante=str_replace('${dias}', $totalTutoria, $nodoParticipante);
			$nodoParticipante=str_replace('${diasFormacion}', $totalFormacion, $nodoParticipante);
			$nodoParticipante=str_replace('${diasFormacionDistancia}', $totalFormacionDistancia, $nodoParticipante);
			
		}

		cierraBD();

		$grupos.=$nodoParticipante;
		$datos=mysql_fetch_assoc($consulta);
	
	}

	unset($nodoParticipante);
	$xml=file_get_contents('documentos/plantillaGrupo.xml');
	$xml=str_replace('${grupo}', $grupos, $xml);

	file_put_contents("documentos/$fichero",$xml);

	unset($grupos);
	unset($xml);//Borro las variables para liberar memoria.

	return $fichero;
}

function selectAlumnosCostes($codigoCurso){
	echo "<select name='codigoEmpresa' class='selectpicker show-tick' data-live-search='true'>";
	conexionBD();
	$consulta=consultaBD("SELECT clientes.* FROM alumnos 
		INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno
		INNER JOIN ventas ON alumnos.codigoVenta=ventas.codigo
		INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo
		WHERE alumnos_registrados_cursos.codigoCurso='$codigoCurso' ORDER BY empresa;");
	$datos=mysql_fetch_assoc($consulta);
	
	while($datos!=0){
		echo "<option value='".$datos['codigo']."'>".$datos['empresa']."</option>";
		$datos=mysql_fetch_assoc($consulta);
	}
	echo "</select>
	";
	cierraBD();
}

function datosCostes($codigoAlumno,$codigoCurso){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM costes WHERE codigoAlumno='$codigoAlumno' AND codigoCurso='$codigoCurso';");
	cierraBD();

	$consulta=mysql_fetch_assoc($consulta);
	return $consulta;
}

function datosPeriodos($codigoCoste){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM periodos_costes WHERE codigoCoste='$codigoCoste';");
	cierraBD();

	return $consulta;
}

function selectUsuarios($codigo=false,$disabled=false,$texto='<strong>Trabajador asignado</strong>'){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre, apellidos FROM usuarios WHERE activoUsuario='SI' ORDER BY apellidos, nombre;");
	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	$deshabilitado='';
	if($disabled){
		$deshabilitado='disabled="disabled"';
	}
	echo '
	<div class="control-group">                     
		<label class="control-label" for="codigoUsuario">'.$texto.'</label>
        <div class="controls">
			<select name="codigoUsuario" id="codigoUsuario" class="selectpicker show-tick" data-live-search="true" '.$deshabilitado.'>';
		while($datos!=false){
			echo '<option value="'.$datos['codigo'].'"';
		
			if($codigo==$datos['codigo']){
				echo ' selected="selected"';
			}

			echo '>'.$datos['apellidos'].', '.$datos['nombre'].'</option>';
			$datos=mysql_fetch_assoc($consulta);
		}
	echo '</select></div></div>';
}

function selectTrabajador($codigo=false,$disabled=false,$texto='<strong>Trabajador asignado</strong>'){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre, apellidos FROM usuarios WHERE activoUsuario='SI' AND (tipo='COMERCIAL' OR tipo='ADMIN') AND apellidos != 'Fernandez Venegas' AND nombre != 'Director' ORDER BY apellidos, nombre;");
	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	$deshabilitado='';
	if($disabled){
		$deshabilitado='disabled="disabled"';
	}
	echo '
	<div class="control-group">                     
		<label class="control-label" for="codigoUsuario">'.$texto.'</label>
        <div class="controls">
			<select name="codigoUsuario" id="codigoUsuario" class="selectpicker show-tick" data-live-search="true" '.$deshabilitado.'>';
		while($datos!=false){
			echo '<option value="'.$datos['codigo'].'"';
		
			if($codigo==$datos['codigo']){
				echo ' selected="selected"';
			}

			echo '>'.$datos['apellidos'].', '.$datos['nombre'].'</option>';
			$datos=mysql_fetch_assoc($consulta);
		}
	echo '</select></div></div>';
}

function devuelveFecha(){
	return date('d')."/".date('m')."/".date('Y');
}

function generaXMLGrupo($codigoCurso){
	$grupos='';

	conexionBD();
	$consulta=consultaBD("SELECT COUNT(codigoAlumno) AS numParticipantes FROM alumnos_registrados_cursos WHERE codigoCurso='$codigoCurso';");
	$numParticipantes=mysql_fetch_assoc($consulta);
	$consulta=consultaBD("SELECT accionFormativa.*, fechaInicio, fechaFin, cursos.horaInicio, cursos.horaFin, titularidad, mediosPropios, mediosEntidad, mediosCentro, cif, centro, tlf, domicilio, cp, poblacion, cursos.horasTutoria, 
	cursos.lunes, cursos.martes, cursos.miercoles, cursos.jueves, cursos.viernes, cursos.sabado, cursos.domingo, tutores.nombre AS nombreTutor, tutores.dni, tutores.apellidos,
	cifTutoria, centroTutoria, tlfTutoria, domicilioTutoria, cpTutoria, poblacionTutoria, horaInicioFormacion, horaFinFormacion, horaInicioFormacionTarde, horaFinFormacionTarde, horasFormacion,
	lunesFormacion, martesFormacion, miercolesFormacion, juevesFormacion, viernesFormacion, sabadoFormacion, domingoFormacion, cifDistancia, centroGestorDistancia, tlfDistancia, domicilioDistancia, 
	cpDistancia, poblacionDistancia, horaInicioFormacionDistancia, horaFinFormacionDistancia, horaInicioFormacionTardeDistancia, horaFinFormacionTardeDistancia, horasFormacionDistancia, lunesFormacionDistancia,
	martesFormacionDistancia, miercolesFormacionDistancia, juevesFormacionDistancia, viernesFormacionDistancia, sabadoFormacionDistancia, domingoFormacionDistancia, responsable, tlfResponsable, titularidadDistancia, cursos.codigoInterno AS grupo, cursos.medios
	FROM cursos INNER JOIN tutores ON tutores.codigo=cursos.codigoTutor
	INNER JOIN accionFormativa ON cursos.codigoAccionFormativa=accionFormativa.codigoInterno
	INNER JOIN alumnos_registrados_cursos ON cursos.codigo=alumnos_registrados_cursos.codigoCurso
	WHERE cursos.codigo='$codigoCurso';");
	echo mysql_error();

	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	$fichero='FicheroInicioGrupo'.time().'.xml';

	$nodoParticipante=file_get_contents('documentos/nodoGrupo.xml');

	$espacio=strpos($datos['apellidos'],' ');
	$datos['apellido1']=substr($datos['apellidos'],0,$espacio);
	$datos['apellido2']=substr($datos['apellidos'],$espacio+1);
	
	$nodoParticipante=str_replace('${idAccion}', $datos['codigoInterno'], $nodoParticipante);
	$nodoParticipante=str_replace('${idGrupo}', $datos['grupo'], $nodoParticipante);
	$nodoParticipante=str_replace('${descripcion}', $datos['denominacion'], $nodoParticipante);
	$titularidad=array("publico"=>"false", "privado"=>"true");
	$nodoParticipante=str_replace('${titularidad}', "false", $nodoParticipante);
	$medios=array("SI"=>"true", "NO"=>"false");
	$nodoParticipante=str_replace('${mediosPropios}', $datos['medios'], $nodoParticipante);
	$nodoParticipante=str_replace('${numParticipantes}', $numParticipantes['numParticipantes'], $nodoParticipante);
	$nodoParticipante=str_replace('${fechaInicio}', formateaFechaWeb($datos['fechaInicio']), $nodoParticipante);
	$nodoParticipante=str_replace('${fechaFin}', formateaFechaWeb($datos['fechaFin']), $nodoParticipante);
	$nodoParticipante=str_replace('${responsable}', $datos['responsable'], $nodoParticipante);
	$nodoParticipante=str_replace('${telefono}', $datos['tlfResponsable'], $nodoParticipante);
	
	$diasLunes=array("SI"=>"L", "NO"=>"");
	$diasMartes=array("SI"=>"M", "NO"=>"");
	$diasMiercoles=array("SI"=>"X", "NO"=>"");
	$diasJueves=array("SI"=>"J", "NO"=>"");
	$diasViernes=array("SI"=>"V", "NO"=>"");
	$diasSabado=array("SI"=>"S", "NO"=>"");
	$diasDomingo=array("SI"=>"D", "NO"=>"");
	
	$lunes=$diasLunes[$datos['lunes']];			
	$martes=$diasMartes[$datos['martes']];			
	$miercoles=$diasMiercoles[$datos['miercoles']];			
	$jueves=$diasJueves[$datos['jueves']];			
	$viernes=$diasViernes[$datos['viernes']];			
	$sabado=$diasSabado[$datos['sabado']];			
	$domingo=$diasDomingo[$datos['domingo']];
	
	$totalTutoria=$lunes."".$martes."".$miercoles."".$jueves."".$viernes;
	
	$lunesFormacion=$diasLunes[$datos['lunesFormacion']];			
	$martesFormacion=$diasMartes[$datos['martesFormacion']];			
	$miercolesFormacion=$diasMiercoles[$datos['miercolesFormacion']];			
	$juevesFormacion=$diasJueves[$datos['juevesFormacion']];			
	$viernesFormacion=$diasViernes[$datos['viernesFormacion']];			
	$sabadoFormacion=$diasSabado[$datos['sabadoFormacion']];			
	$domingoFormacion=$diasDomingo[$datos['domingoFormacion']];
	
	$totalFormacion=$lunesFormacion."".$martesFormacion."".$miercolesFormacion."".$juevesFormacion."".$viernesFormacion;
	
	$lunesFormacionDistancia=$diasLunes[$datos['lunesFormacionDistancia']];			
	$martesFormacionDistancia=$diasMartes[$datos['martesFormacionDistancia']];			
	$miercolesFormacionDistancia=$diasMiercoles[$datos['miercolesFormacionDistancia']];			
	$juevesFormacionDistancia=$diasJueves[$datos['juevesFormacionDistancia']];			
	$viernesFormacionDistancia=$diasViernes[$datos['viernesFormacionDistancia']];			
	$sabadoFormacionDistancia=$diasSabado[$datos['sabadoFormacionDistancia']];			
	$domingoFormacionDistancia=$diasDomingo[$datos['domingoFormacionDistancia']];
	
	$totalFormacionDistancia=$lunesFormacionDistancia."".$martesFormacionDistancia."".$miercolesFormacionDistancia."".$juevesFormacionDistancia."".$viernesFormacionDistancia;			
	 
	if($datos['modalidad']=='PRESENCIAL'){
		$nodoPresencial=file_get_contents('documentos/nodoPresencial.xml');
		
		$nodoPresencial=str_replace('${cif}', $datos['cif'], $nodoPresencial);
		$nodoPresencial=str_replace('${nombreCentro}', $datos['centro'], $nodoPresencial);
		$nodoPresencial=str_replace('${direccion}', $datos['domicilio'], $nodoPresencial);
		$nodoPresencial=str_replace('${cp}', $datos['cp'], $nodoPresencial);
		$nodoPresencial=str_replace('${localidad}', $datos['poblacion'], $nodoPresencial);
		$nodoPresencial=str_replace('${horasFormacion}', $datos['horasTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${horaInicioFormacion}', $datos['horaInicioFormacion'], $nodoPresencial);
		$nodoPresencial=str_replace('${horaFinFormacion}', $datos['horaFinFormacion'], $nodoPresencial);	
		$nodoPresencial=str_replace('${cifTutoria}', $datos['cifTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${nombreCentroTutoria}', $datos['centroTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${direccionTutoria}', $datos['domicilioTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${cpTutoria}', $datos['cpTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${localidadTutoria}', $datos['poblacionTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${horas}', $datos['horasTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${horaInicioManana}', $datos['horaInicio'], $nodoPresencial);
		$nodoPresencial=str_replace('${horaFinManana}', $datos['horaFin'], $nodoPresencial);
		$nodoPresencial=str_replace('${numHoras}', $datos['horasTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${dni}', $datos['dni'], $nodoPresencial);
		$nodoPresencial=str_replace('${nombre}', $datos['nombreTutor'], $nodoPresencial);
		$nodoPresencial=str_replace('${apell1}', $datos['apellido1'], $nodoPresencial);
		$nodoPresencial=str_replace('${apell2}', $datos['apellido2'], $nodoPresencial);
		
		$nodoPresencial=str_replace('${dias}', $totalTutoria, $nodoPresencial);
		$nodoPresencial=str_replace('${diasFormacion}', $totalFormacion, $nodoPresencial);
		
		$nodoParticipante=str_replace('${presencial}', $nodoPresencial, $nodoParticipante);
		$nodoParticipante=str_replace('${distancia}', '', $nodoParticipante);
		$nodoParticipante=str_replace('${teleformacion}', '', $nodoParticipante);
		unset($nodoPresencial);
	}elseif($datos['modalidad']=='DISTANCIA'){	
		$nodoDistancia=file_get_contents('documentos/nodoDistancia.xml');
		
		$nodoDistancia=str_replace('${numHorasTutorDistancia}', $datos['horasTutoria'], $nodoDistancia);
		$nodoDistancia=str_replace('${dniTutorDistancia}', $datos['dni'], $nodoDistancia);
		$nodoDistancia=str_replace('${nombreTutorDistancia}', $datos['nombreTutor'], $nodoDistancia);
		$nodoDistancia=str_replace('${apell1TutorDistancia}', $datos['apellido1'], $nodoDistancia);
		$nodoDistancia=str_replace('${apell2TutorDistancia}', $datos['apellido2'], $nodoDistancia);
		
		$nodoDistancia=str_replace('${cifDistancia}', $datos['cifDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${nombreCentroDistancia}', $datos['centroGestorDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${direccionDistancia}', $datos['domicilioDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${cpDistancia}', $datos['cpDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${localidadDistancia}', $datos['poblacionDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${telefonoDistancia}', $datos['tlfDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${horasFormacionDistancia}', $datos['horasFormacionDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicioFormacionDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${horaFinFormacionDistancia}', $datos['horaFinFormacionDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicioFormacionDistancia'], $nodoDistancia);
		
		$nodoDistancia=str_replace('${diasFormacionDistancia}', $totalFormacionDistancia, $nodoDistancia); 
		
		$nodoParticipante=str_replace('${distancia}', $nodoDistancia, $nodoParticipante);
		$nodoParticipante=str_replace('${presencial}', '', $nodoParticipante);
		$nodoParticipante=str_replace('${teleformacion}', '', $nodoParticipante);
		unset($nodoDistancia);
	}elseif($datos['modalidad']=='TELE'){	
		$nodoTeleformacion=file_get_contents('documentos/nodoTeleformacion.xml');
		
		$nodoTeleformacion=str_replace('${cifTeleformacion}', $datos['cif'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${nombreCentroTeleformacion}', $datos['centro'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${direccionTeleformacion}', $datos['domicilio'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${cpTeleformacion}', $datos['cp'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${localidadTeleformacion}', $datos['poblacion'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${horasFormacionDistancia}', $datos['horasTutoria'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicio'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${horaFinFormacionDistancia}', $datos['horaFin'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${telefonoTeleformacion}', $datos['tlf'], $nodoTeleformacion);
		
		$nodoTeleformacion=str_replace('${diasFormacionDistancia}', $totalFormacion, $nodoTeleformacion); 
		$nodoTeleformacion=str_replace('${numHorasTutorDistancia}', $datos['horasTutoria'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${dniTutorDistancia}', $datos['dni'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${nombreTutorDistancia}', $datos['nombreTutor'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${apell1TutorDistancia}', $datos['apellido1'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${apell2TutorDistancia}', $datos['apellido2'], $nodoTeleformacion);
		
		$nodoParticipante=str_replace('${teleformacion}', $nodoTeleformacion, $nodoParticipante);
		$nodoParticipante=str_replace('${distancia}', '', $nodoParticipante);
		$nodoParticipante=str_replace('${presencial}', '', $nodoParticipante);
		unset($nodoTeleformacion);
	}else{
		$nodoPresencial=file_get_contents('documentos/nodoPresencial.xml');
		
		$nodoPresencial=str_replace('${cif}', $datos['cif'], $nodoPresencial);
		$nodoPresencial=str_replace('${nombreCentro}', $datos['centro'], $nodoPresencial);
		$nodoPresencial=str_replace('${direccion}', $datos['domicilio'], $nodoPresencial);
		$nodoPresencial=str_replace('${cp}', $datos['cp'], $nodoPresencial);
		$nodoPresencial=str_replace('${localidad}', $datos['poblacion'], $nodoPresencial);
		$nodoPresencial=str_replace('${horasFormacion}', $datos['horasTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${horaInicioFormacion}', $datos['horaInicioFormacion'], $nodoPresencial);
		$nodoPresencial=str_replace('${horaFinFormacion}', $datos['horaFinFormacion'], $nodoPresencial);	
		$nodoPresencial=str_replace('${cifTutoria}', $datos['cifTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${nombreCentroTutoria}', $datos['centroTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${direccionTutoria}', $datos['domicilioTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${cpTutoria}', $datos['cpTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${localidadTutoria}', $datos['poblacionTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${horas}', $datos['horasTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${horaInicioManana}', $datos['horaInicio'], $nodoPresencial);
		$nodoPresencial=str_replace('${horaFinManana}', $datos['horaFin'], $nodoPresencial);
		$nodoPresencial=str_replace('${numHoras}', $datos['horasTutoria'], $nodoPresencial);
		$nodoPresencial=str_replace('${dni}', $datos['dni'], $nodoPresencial);
		$nodoPresencial=str_replace('${nombre}', $datos['nombreTutor'], $nodoPresencial);
		$nodoPresencial=str_replace('${apell1}', $datos['apellido1'], $nodoPresencial);
		$nodoPresencial=str_replace('${apell2}', $datos['apellido2'], $nodoPresencial);
		
		$nodoPresencial=str_replace('${dias}', $totalTutoria, $nodoPresencial);
		$nodoPresencial=str_replace('${diasFormacion}', $totalFormacion, $nodoPresencial);
		
		$nodoParticipante=str_replace('${presencial}', $nodoPresencial, $nodoParticipante);
		unset($nodoPresencial);
		
		$nodoDistancia=file_get_contents('documentos/nodoDistancia.xml');
		
		$nodoDistancia=str_replace('${numHorasTutorDistancia}', $datos['horasTutoria'], $nodoDistancia);
		$nodoDistancia=str_replace('${dniTutorDistancia}', $datos['dni'], $nodoDistancia);
		$nodoDistancia=str_replace('${nombreTutorDistancia}', $datos['nombreTutor'], $nodoDistancia);
		$nodoDistancia=str_replace('${apell1TutorDistancia}', $datos['apellido1'], $nodoDistancia);
		$nodoDistancia=str_replace('${apell2TutorDistancia}', $datos['apellido2'], $nodoDistancia);
		
		$nodoDistancia=str_replace('${cifDistancia}', $datos['cifDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${nombreCentroDistancia}', $datos['centroGestorDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${direccionDistancia}', $datos['domicilioDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${cpDistancia}', $datos['cpDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${localidadDistancia}', $datos['poblacionDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${telefonoDistancia}', $datos['tlfDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${horasFormacionDistancia}', $datos['horasFormacionDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicioFormacionDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${horaFinFormacionDistancia}', $datos['horaFinFormacionDistancia'], $nodoDistancia);
		$nodoDistancia=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicioFormacionDistancia'], $nodoDistancia);
		
		$nodoDistancia=str_replace('${diasFormacionDistancia}', $totalFormacionDistancia, $nodoDistancia); 
		
		$nodoParticipante=str_replace('${distancia}', $nodoDistancia, $nodoParticipante);
		unset($nodoDistancia);
		
		$nodoTeleformacion=file_get_contents('documentos/nodoTeleformacion.xml');
		
		$nodoTeleformacion=str_replace('${cifTeleformacion}', $datos['cif'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${nombreCentroTeleformacion}', $datos['centro'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${direccionTeleformacion}', $datos['domicilio'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${cpTeleformacion}', $datos['cp'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${localidadTeleformacion}', $datos['poblacion'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${horasFormacionDistancia}', $datos['horasTutoria'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${horaInicioFormacionDistancia}', $datos['horaInicio'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${horaFinFormacionDistancia}', $datos['horaFin'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${telefonoTeleformacion}', $datos['tlf'], $nodoTeleformacion);
		
		$nodoTeleformacion=str_replace('${diasFormacionDistancia}', $totalFormacion, $nodoTeleformacion); 
		$nodoTeleformacion=str_replace('${numHorasTutorDistancia}', $datos['horasTutoria'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${dniTutorDistancia}', $datos['dni'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${nombreTutorDistancia}', $datos['nombreTutor'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${apell1TutorDistancia}', $datos['apellido1'], $nodoTeleformacion);
		$nodoTeleformacion=str_replace('${apell2TutorDistancia}', $datos['apellido2'], $nodoTeleformacion);
		
		$nodoParticipante=str_replace('${teleformacion}', $nodoTeleformacion, $nodoParticipante);
		unset($nodoTeleformacion);
	}
	
	

	//NUEVA PARTE EMPRESAS
	
	$consulta=consultaBD("SELECT clientes.cif FROM clientes WHERE codigo IN
	(SELECT clientes.codigo FROM clientes 
	INNER JOIN ventas ON clientes.codigo=ventas.codigoCliente 
	INNER JOIN alumnos ON ventas.codigo=alumnos.codigoVenta
	INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno
	WHERE alumnos_registrados_cursos.codigoCurso='$codigoCurso');",true);
	
	$empresas='';
	while($datosEmpresas=mysql_fetch_assoc($consulta)){
		$nodoEmpresa=file_get_contents('documentos/nodoEmpresa.xml');
		$nodoEmpresa=str_replace('${cifEmpresa}', $datosEmpresas['cif'], $nodoEmpresa);
		$nodoEmpresa=str_replace('${jornadaEmpresa}', '1', $nodoEmpresa);
		$empresas.=$nodoEmpresa;
		unset($nodoEmpresa);
	}	
	
	$nodoParticipante=str_replace('${empresas}', $empresas, $nodoParticipante);
	
	//
		

	$grupos.=$nodoParticipante;
	$datos=mysql_fetch_assoc($consulta);
	

	unset($nodoParticipante);
	$xml=file_get_contents('documentos/plantillaGrupo.xml');
	$xml=str_replace('${grupo}', $grupos, $xml);

	file_put_contents("documentos/$fichero",$xml);

	unset($grupos);
	unset($xml);//Borro las variables para liberar memoria.

	return $fichero;
}

function generaXMLAccion($codigoCurso){
	$participantes='';

	conexionBD();
	$consulta=consultaBD("SELECT cursos.informarlt, informerlt, fechaDiscrepancia, resuelto, clientes.cif, accionFormativa.denominacion AS nombreEmpresa, accionFormativa.codigoInterno, accionFormativa.denominacion, accionFormativa.grupoAcciones, accionFormativa.horas, accionFormativa.modalidad, accionFormativa.tipoAccion, accionFormativa.nivelAccion, accionFormativa.objetivos, accionFormativa.contenidos, cursos.codigoInterno AS codigoGrupo, accionFormativa.urlAccion, accionFormativa.usuarioAccion, accionFormativa.claveAccion FROM cursos 
	INNER JOIN accionFormativa ON accionFormativa.codigoInterno=cursos.codigoAccionFormativa
	INNER JOIN alumnos_registrados_cursos ON cursos.codigo=alumnos_registrados_cursos.codigoCurso
	INNER JOIN alumnos ON alumnos_registrados_cursos.codigoAlumno=alumnos.codigo
	INNER JOIN ventas ON alumnos.codigoVenta=ventas.codigo
	INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo
	WHERE cursos.codigo='$codigoCurso';");

	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	$fichero='AccionFormativa'.time().'.xml';
	
		$nodoParticipante=file_get_contents('documentos/nodoAccion.xml');

		$nodoParticipante=str_replace('${dni}', $datos['dni'], $nodoParticipante);
		$nodoParticipante=str_replace('${codigoAccion}', $datos['codigoInterno'], $nodoParticipante);
		$nodoParticipante=str_replace('${nombre}', $datos['denominacion'], $nodoParticipante);
		$nodoParticipante=str_replace('${url}', $datos['urlAccion'], $nodoParticipante);
		$nodoParticipante=str_replace('${usuario}', $datos['usuarioAccion'], $nodoParticipante);
		$nodoParticipante=str_replace('${clave}', $datos['claveAccion'], $nodoParticipante);
		if(strlen($datos['codigoInterno'])<3){
			while(strlen($datos['codigoInterno'])<3){
				$datos['codigoInterno']='0'.$datos['codigoInterno'];
			}
		}
		if(strlen($datos['codigoGrupo'])<2){
			while(strlen($datos['codigoGrupo'])<2){
				$datos['codigoGrupo']='0'.$datos['codigoGrupo'];
			}
		}
		$nodoParticipante=str_replace('${codigoGrupo}', $datos['grupoAcciones'], $nodoParticipante);
		$nodoParticipante=str_replace('${horas}', $datos['horas'], $nodoParticipante);
		$modalidad=array("PRESENCIAL"=>"7", "DISTANCIA"=>"8", "MIXTA"=>"9", "TELE"=>"10");
		$nodoParticipante=str_replace('${modalidad}', $modalidad[$datos['modalidad']], $nodoParticipante);
		$tipo=array("PROPIA"=>"0", "VINCULADA"=>"1");
		$nodoParticipante=str_replace('${tipoAccion}', $tipo[$datos['tipoAccion']], $nodoParticipante);
		$nivel=array("BASICO"=>"0", "SUPERIOR"=>"1");
		$nodoParticipante=str_replace('${nivelFormacion}', $nivel[$datos['nivelAccion']], $nodoParticipante);
		$nodoParticipante=str_replace('${objetivos}', $datos['objetivos'], $nodoParticipante);
		$nodoParticipante=str_replace('${contenidos}', $datos['contenidos'], $nodoParticipante);
		$nodoParticipante=str_replace('${cif}', $datos['cif'], $nodoParticipante);
		$informa=array("SI"=>"S", "NO"=>"N");
		if($datos['informarlt']=="SI"){
			$nodoParticipante=str_replace('${informaRlt}', $informa[$datos['informarlt']], $nodoParticipante);
			$informe=array("NOINF"=>"N", "FAV"=>"F", "DIS"=>"D");
			$nodoParticipante=str_replace('${valorinf}', $informe[$datos['informerlt']], $nodoParticipante);
			$nodoParticipante=str_replace('${fechaDis}', formateaFechaWeb($datos['fechaDiscrepancia']), $nodoParticipante);
			$nodoParticipante=str_replace('${resuelto}', $informa[$datos['resuelto']], $nodoParticipante);
		}else{
			$nodoParticipante=str_replace('${informaRlt}', $informa[$datos['informarlt']], $nodoParticipante);
			$nodoParticipante=str_replace('${valorinf}', "B", $nodoParticipante);
			$nodoParticipante=str_replace('${fechaDis}', "00/00/0000", $nodoParticipante);
			$nodoParticipante=str_replace('${resuelto}', "B", $nodoParticipante);
		}

		$participantes.=$nodoParticipante;
		
	unset($nodoParticipante);
	$xml=file_get_contents('documentos/plantillaAccion.xml');
	$xml=str_replace('${accionFormativa}', $participantes, $xml);

	file_put_contents("documentos/$fichero",$xml);

	unset($participantes);
	unset($xml);//Borro las variables para liberar memoria.

	return $fichero;
}

function generaXMLFin($codigoCurso){

	$nombresFicheros = array();

	conexionBD();
	$empresa=consultaBD("SELECT costesImparticion, costesOrganizacion, costesHora, costesElegibles, costesIndirectos, accionFormativa.codigoInterno, grupoAcciones, empresa, clientes.codigo AS codigoCliente, costes.codigo AS codigoCoste, cursos.codigoInterno AS grupo, clientes.cif FROM cursos 
	INNER JOIN accionFormativa ON cursos.codigoaccionFormativa=accionFormativa.codigoInterno
	INNER JOIN costes ON costes.codigoCurso=cursos.codigo
	INNER JOIN clientes ON costes.codigoAlumno=clientes.codigo WHERE cursos.codigo='$codigoCurso' AND costes.codigoCurso='$codigoCurso';");
	$datosEmpresa=mysql_fetch_assoc($empresa);
	$controles=array('SI'=>'true','NO'=>'false');
	$estudios=array('sin'=>'1','primarios'=>'2','3'=>'3','bachiller'=>'4','5'=>'5','6'=>'6','diplomatura'=>'7','licenciatura'=>'8','9'=>'9','10'=>'10');
	$categoria=array('directivo'=>'1','intermedio'=>'2','tecnico'=>'3','cualificado'=>'4','bajaCualificacion'=>'5');
	$i=0;
	while(isset($datosEmpresa['empresa'])){
		$participantes='';
		$costes='';
		
		$consulta=consultaBD("SELECT dni, nombre, apellidos, numSS, sexo, fechaNac, denominacion, clientes.cif, alumnos.mail, alumnos.telefono, discapacidad, vTerrorismo, vViolencia, alumnos.categoria, alumnos.cotizacion, alumnos.estudios, cursos.fechaInicio, cursos.fechaFin, clientes.ccc
		FROM ((alumnos INNER JOIN alumnos_registrados_cursos ON 
		alumnos.codigo=alumnos_registrados_cursos.codigoAlumno)	INNER JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo) INNER JOIN 
		accionFormativa ON cursos.codigoaccionFormativa=accionFormativa.codigoInterno INNER JOIN ventas ON ventas.codigo=alumnos.codigoVenta
		INNER JOIN clientes ON clientes.codigo=ventas.codigoCliente 
		WHERE clientes.codigo='".$datosEmpresa['codigoCliente']."' AND alumnos_registrados_cursos.codigoCurso='$codigoCurso';");

		$datos=mysql_fetch_assoc($consulta);
		$fichero='FicheroFinGrupo'.$i.time().'.xml';
		array_push($nombresFicheros, $fichero);

		while($datos!=false){
			$nodoParticipante=file_get_contents('documentos/nodoParticipanteFinalizacion.xml');

			$espacio=strpos($datos['apellidos'],' ');
			$datos['apellido1']=substr($datos['apellidos'],0,$espacio);
			$datos['apellido2']=substr($datos['apellidos'],$espacio+1);

			$nodoParticipante=str_replace('${dni}', $datos['dni'], $nodoParticipante);
			$nodoParticipante=str_replace('${apellido1}', $datos['apellido1'], $nodoParticipante);
			$nodoParticipante=str_replace('${apellido2}', $datos['apellido2'], $nodoParticipante);
			$nodoParticipante=str_replace('${nombre}', $datos['nombre'], $nodoParticipante);
			$nodoParticipante=str_replace('${numSS}', $datos['numSS'], $nodoParticipante);
			$nodoParticipante=str_replace('${sexo}', $datos['sexo'], $nodoParticipante);
			$nodoParticipante=str_replace('${fechaNac}', formateaFechaWeb($datos['fechaNac']), $nodoParticipante);
			$nodoParticipante=str_replace('${cifEmpresa}', $datos['cif'], $nodoParticipante);
			$nodoParticipante=str_replace('${cuentaCotizacion}', $datos['ccc'], $nodoParticipante);
			$nodoParticipante=str_replace('${correo}', $datos['mail'], $nodoParticipante);
			$nodoParticipante=str_replace('${telefono}', $datos['telefono'], $nodoParticipante);			
			$nodoParticipante=str_replace('${discapacitado}', $controles[$datos['discapacidad']], $nodoParticipante);
			$nodoParticipante=str_replace('${terrorismo}', $controles[$datos['vTerrorismo']], $nodoParticipante);
			$nodoParticipante=str_replace('${genero}', $controles[$datos['vViolencia']], $nodoParticipante);
			$nodoParticipante=str_replace('${categoria}', $categoria[$datos['categoria']], $nodoParticipante);
			$nodoParticipante=str_replace('${grupo}', $datos['cotizacion'], $nodoParticipante);
			$nodoParticipante=str_replace('${estudios}', $estudios[$datos['estudios']], $nodoParticipante);
			$nodoParticipante=str_replace('${fechaInicioFormacion}', formateaFechaWeb($datos['fechaInicio']), $nodoParticipante);
			$nodoParticipante=str_replace('${fechaFinFormacion}', formateaFechaWeb($datos['fechaFin']), $nodoParticipante);
			

			$participantes.=$nodoParticipante;
			$datos=mysql_fetch_assoc($consulta);
		}
		
		$consulta=consultaBD("SELECT mes, importe FROM periodos_costes WHERE codigoCoste='".$datosEmpresa['codigoCoste']."';");

		$datos=mysql_fetch_assoc($consulta);
		while($datos!=false){
			$nodoCoste=file_get_contents('documentos/nodoCoste.xml');

			$nodoCoste=str_replace('${mes}', $datos['mes'], $nodoCoste);
			$nodoCoste=str_replace('${importe}', $datos['importe'], $nodoCoste);

			$costes.=$nodoCoste;
			$datos=mysql_fetch_assoc($consulta);
		}

		unset($nodoParticipante);
		unset($nodoCoste);
		$xml=file_get_contents('documentos/nodoGrupoFin.xml');
		$xml=str_replace('${idAccion}',$datosEmpresa['codigoInterno'], $xml);
		$xml=str_replace('${idGrupo}',$datosEmpresa['grupo'], $xml);
		$xml=str_replace('${cifEmpresa}', $datosEmpresa['cif'], $xml);
		$xml=str_replace('${costesDirectos}',$datosEmpresa['costesImparticion'], $xml);
		$xml=str_replace('${costesAsociados}',$datosEmpresa['costesIndirectos'], $xml);
		$xml=str_replace('${organizacion}',$datosEmpresa['costesOrganizacion'], $xml);
		$xml=str_replace('${costesSalariales}',$datosEmpresa['costesHora'], $xml);
		$xml=str_replace('${participante}', $participantes, $xml);
		$xml=str_replace('${periodo}', $costes, $xml);

		file_put_contents("documentos/$fichero",$xml);

		unset($participantes);
		unset($costes);
		unset($xml);//Borro las variables para liberar memoria.
		$datosEmpresa=mysql_fetch_assoc($empresa);
		$i++;
	}
	cierraBD();
	

	return $nombresFicheros;
}

function generaXMLAccionTodas(){
	$participantes='';

	conexionBD();
	
	$consultaEmpresa=consultaBD("SELECT accionFormativa.*, cursos.codigo AS codigoCurso FROM accionFormativa 
							INNER JOIN cursos ON accionFormativa.codigoInterno=cursos.codigoaccionFormativa;");
							
	$datosEmpresa=mysql_fetch_assoc($consultaEmpresa);
	
	$fichero='AccionesFormativas'.time().'.xml';
	
	$acciones="";
	
	while(isset($datosEmpresa['codigoCurso'])){	
		$consulta=consultaBD("SELECT cursos.informarlt, informerlt, fechaDiscrepancia, resuelto, clientes.cif, accionFormativa.denominacion AS nombreEmpresa, 
		accionFormativa.codigoInterno, accionFormativa.denominacion, accionFormativa.grupoAcciones, accionFormativa.horas, accionFormativa.modalidad, 
		accionFormativa.tipoAccion, accionFormativa.nivelAccion, accionFormativa.objetivos, accionFormativa.contenidos FROM cursos 
		INNER JOIN accionFormativa ON accionFormativa.codigoInterno=cursos.codigoAccionFormativa
		INNER JOIN alumnos_registrados_cursos ON cursos.codigo=alumnos_registrados_cursos.codigoCurso
		INNER JOIN alumnos ON alumnos_registrados_cursos.codigoAlumno=alumnos.codigo
		INNER JOIN ventas ON alumnos.codigoVenta=ventas.codigo
		INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo
		WHERE cursos.codigo='".$datosEmpresa['codigoCurso']."';");

		$datos=mysql_fetch_assoc($consulta);
		
		$nodoAccion=file_get_contents('documentos/nodoAccion.xml');
		
		$nodoAccion=str_replace('${codigoAccion}', $datos['codigoInterno'], $nodoAccion);
		$nodoAccion=str_replace('${nombre}', $datos['denominacion'], $nodoAccion);
		$nodoAccion=str_replace('${codigoGrupo}', $datos['grupoAcciones'], $nodoAccion);
		$nodoAccion=str_replace('${horas}', $datos['horas'], $nodoAccion);
		$modalidad=array("PRESENCIAL"=>"7", "DISTANCIA"=>"8", "MIXTA"=>"9", "TELE"=>"10");
		$nodoAccion=str_replace('${modalidad}', $modalidad[$datos['modalidad']], $nodoAccion);
		$tipo=array("PROPIA"=>"0", "VINCULADA"=>"1");
		$nodoAccion=str_replace('${tipoAccion}', $tipo[$datos['tipoAccion']], $nodoAccion);
		$nivel=array("BASICO"=>"0", "SUPERIOR"=>"1");
		$nodoAccion=str_replace('${nivelFormacion}', $nivel[$datos['nivelAccion']], $nodoAccion);
		$nodoAccion=str_replace('${objetivos}', $datos['objetivos'], $nodoAccion);
		$nodoAccion=str_replace('${contenidos}', $datos['contenidos'], $nodoAccion);
		$nodoAccion=str_replace('${cif}', $datos['cif'], $nodoAccion);
		$informa=array("SI"=>"S", "NO"=>"N");
		if($datos['informarlt']=="SI"){
			$nodoAccion=str_replace('${informaRlt}', $informa[$datos['informarlt']], $nodoAccion);
			$informe=array("NOINF"=>"N", "FAV"=>"F", "DIS"=>"D");
			$nodoAccion=str_replace('${valorinf}', $informe[$datos['informerlt']], $nodoAccion);
			$nodoAccion=str_replace('${fechaDis}', formateaFechaWeb($datos['fechaDiscrepancia']), $nodoAccion);
			$nodoAccion=str_replace('${resuelto}', $informa[$datos['resuelto']], $nodoAccion);
		}else{
			$nodoAccion=str_replace('${informaRlt}', $informa[$datos['informarlt']], $nodoAccion);
			$nodoAccion=str_replace('${valorinf}', "B", $nodoAccion);
			$nodoAccion=str_replace('${fechaDis}', "", $nodoAccion);
			$nodoAccion=str_replace('${resuelto}', "B", $nodoAccion);
		}

		$acciones.=$nodoAccion;
		$datosEmpresa=mysql_fetch_assoc($consultaEmpresa);
	}
	
	
	cierraBD();
		
	unset($nodoAccion);
	$xml=file_get_contents('documentos/plantillaAccion.xml');
	$xml=str_replace('${accionFormativa}', $acciones, $xml);

	file_put_contents("documentos/$fichero",$xml);

	unset($acciones);
	unset($xml);//Borro las variables para liberar memoria.

	return $fichero;
}

function generaXMLFinTodos(){

	$nombresFicheros = array();

	conexionBD();
	$empresa=consultaBD("SELECT costesImparticion, costesOrganizacion, costesHora, costesElegibles, costesIndirectos, accionFormativa.codigoInterno, grupoAcciones, empresa, clientes.codigo AS codigoCliente, costes.codigo AS codigoCoste, clientes.cif FROM cursos 
	INNER JOIN accionFormativa ON cursos.codigoaccionFormativa=accionFormativa.codigoInterno
	INNER JOIN costes ON costes.codigoCurso=cursos.codigo
	INNER JOIN clientes ON costes.codigoAlumno=clientes.codigo;");
	$datosEmpresa=mysql_fetch_assoc($empresa);
	while(isset($datosEmpresa['codigoInterno'])){
		$participantes='';
		$costes='';
		
		$consulta=consultaBD("SELECT dni, nombre, apellidos, numSS1, numSS2, sexo, fechaNac, denominacion
		FROM ((alumnos INNER JOIN alumnos_registrados_cursos ON 
		alumnos.codigo=alumnos_registrados_cursos.codigoAlumno)	INNER JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo) INNER JOIN 
		accionFormativa ON cursos.codigoaccionFormativa=accionFormativa.codigoInterno INNER JOIN ventas ON ventas.codigo=alumnos.codigoVenta
		INNER JOIN clientes ON clientes.codigo=ventas.codigoCliente 
		WHERE clientes.codigo='".$datosEmpresa['codigoCliente']."';");

		$datos=mysql_fetch_assoc($consulta);
		$fichero='FicheroFinGrupo-'.str_replace(" ", "", $datosEmpresa['empresa']).'-'.time().'.xml';
		array_push($nombresFicheros, $fichero);

		while($datos!=false){
			$nodoParticipante=file_get_contents('documentos/nodoParticipante.xml');

			$espacio=strpos($datos['apellidos'],' ');
			$datos['apellido1']=substr($datos['apellidos'],0,$espacio);
			$datos['apellido2']=substr($datos['apellidos'],$espacio+1);

			$nodoParticipante=str_replace('${dni}', $datos['dni'], $nodoParticipante);
			$nodoParticipante=str_replace('${apellido1}', $datos['apellido1'], $nodoParticipante);
			$nodoParticipante=str_replace('${apellido2}', $datos['apellido2'], $nodoParticipante);
			$nodoParticipante=str_replace('${nombre}', $datos['nombre'], $nodoParticipante);
			$nodoParticipante=str_replace('${numSS}', $datos['numSS1'].$datos['numSS2'], $nodoParticipante);
			$nodoParticipante=str_replace('${sexo}', $datos['sexo'], $nodoParticipante);
			$nodoParticipante=str_replace('${fechaNac}', formateaFechaWeb($datos['fechaNac']), $nodoParticipante);

			$participantes.=$nodoParticipante;
			$datos=mysql_fetch_assoc($consulta);
		}
		
		$consulta=consultaBD("SELECT mes, importe FROM periodos_costes WHERE codigoCoste='".$datosEmpresa['codigoCoste']."';");

		$datos=mysql_fetch_assoc($consulta);
		while($datos!=false){
			$nodoCoste=file_get_contents('documentos/nodoCoste.xml');

			$nodoCoste=str_replace('${mes}', $datos['mes'], $nodoCoste);
			$nodoCoste=str_replace('${importe}', $datos['importe'], $nodoCoste);

			$costes.=$nodoCoste;
			$datos=mysql_fetch_assoc($consulta);
		}

		unset($nodoParticipante);
		unset($nodoCoste);
		$xml=file_get_contents('documentos/nodoGrupoFin.xml');
		$xml=str_replace('${idAccion}',$datosEmpresa['codigoInterno'], $xml);
		$xml=str_replace('${idGrupo}',$datosEmpresa['grupoAcciones'], $xml);
		$xml=str_replace('${cifEmpresa}',$datosEmpresa['cif'], $xml);
		$xml=str_replace('${costesDirectos}',$datosEmpresa['costesImparticion'], $xml);
		$xml=str_replace('${costesAsociados}',$datosEmpresa['costesIndirectos'], $xml);
		$xml=str_replace('${organizacion}',$datosEmpresa['costesOrganizacion'], $xml);
		$xml=str_replace('${costesSalariales}',$datosEmpresa['costesHora'], $xml);
		$xml=str_replace('${participante}', $participantes, $xml);
		$xml=str_replace('${periodo}', $costes, $xml);

		file_put_contents("documentos/$fichero",$xml);

		unset($participantes);
		unset($costes);
		unset($xml);//Borro las variables para liberar memoria.
		$datosEmpresa=mysql_fetch_assoc($empresa);
	}
	cierraBD();
	

	return $nombresFicheros;
}

function devuelveMes(){
	$mes=date('m');
	switch($mes){
		case 1:
			$mes= "Enero";
		break;
		case 2:
			$mes= "Febrero";
		break;
		case 3:
			$mes= "Marzo";
		break;
		case 4:
			$mes= "Abril";
		break;
		case 5:
			$mes= "Mayo";
		break;
		case 6:
			$mes= "Junio";
		break;
		case 7:
			$mes= "Julio";
		break;
		case 8:
			$mes= "Agosto";
		break;
		case 9:
			$mes= "Septiembre";
		break;
		case 10:
			$mes= "Octubre";
		break;
		case 11:
			$mes= "Noviembre";
		break;
		case 12:
			$mes= "Diciembre";
		break;
	}
	return $mes;
}

function areaTextoTabla($nombreCampo,$valor='',$clase='areaTexto'){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	echo "
	<td>
        <textarea name='$nombreCampo' class='$clase' id='$nombreCampo'>$valor</textarea>
    </td>";
}

function estadisticasGenericasWhere($tabla,$condicion=''){
	return consultaBD("SELECT COUNT(".$tabla.".codigo) AS total FROM $tabla $condicion;",true,true);
}

function estadisticasPreventas($tabla,$condicion='WHERE 1=1'){
	$res=array();
	$servicios=array('formacion','lssi','alergenos','prl','plataformaWeb','auditoriaLopd','consultoriaLopd','auditoriaPrl','webLegalizada','dominioCorreo','dominio','ecommerce');

	$consulta=consultaBD("SELECT COUNT(".$tabla.".codigo) AS total FROM $tabla $condicion;",true,true);
	$res['total']=$consulta['total'];

	$consulta=consultaBD("SELECT * FROM preventas $condicion;",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
    $condicion2=str_replace('SINEVALUAR', 'SI', $condicion);
    $consulta=consultaBD("SELECT * FROM preventas $condicion2;",true);
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
	$res['importe']=$total;

	$consulta=consultaBD("SELECT * FROM preventas $condicion AND aceptado='SINEVALUAR' AND documentos='NO';",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
	$res['sinevaluar']=$total;

	$consulta=consultaBD("SELECT * FROM preventas $condicion AND aceptado='NO';",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
	$res['importeCanceladas']=$total;

	$consulta=consultaBD("SELECT * FROM preventas $condicion AND aceptado='SI';",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
	$res['importeAceptadas']=$total;
	
	return $res;
}

function estadisticasPreventas2($where,$filtro=false){
	$res=array();
	$servicios=array('formacion','lssi','alergenos','prl','plataformaWeb','auditoriaLopd','consultoriaLopd','auditoriaPrl','webLegalizada','dominioCorreo','dominio','ecommerce');

	$fecha=date('Y-m-d');
	$fecha=explode('-', $fecha);
	if(!$filtro){
		$where.=" AND (fecha >= '".$fecha[0]."-".$fecha[1]."-01' OR aceptado='SINEVALUAR')";
	} else {
		$where.=" AND ".$filtro;
	}
	$consulta=consultaBD("SELECT COUNT(preventas.codigo) AS total FROM preventas $where;",true,true);
	$res['total']=$consulta['total'];

	$consulta=consultaBD("SELECT * FROM preventas $where;",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
    $res['totalImporte']=number_format((float)$total, 2, ',', '');

    $consulta=consultaBD("SELECT COUNT(preventas.codigo) AS total FROM preventas $where AND documentos='NO';",true,true);
	$res['noDocumentos']=$consulta['total'];

	$consulta=consultaBD("SELECT * FROM preventas $where AND documentos='NO';",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
    $res['noDocumentosImporte']=number_format((float)$total, 2, ',', '');

    $consulta=consultaBD("SELECT COUNT(preventas.codigo) AS total FROM preventas $where AND documentos='SI';",true,true);
	$res['siDocumentos']=$consulta['total'];

	$consulta=consultaBD("SELECT * FROM preventas $where AND documentos='SI';",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
    $res['siDocumentosImporte']=number_format((float)$total, 2, ',', '');

    $consulta=consultaBD("SELECT COUNT(preventas.codigo) AS total FROM preventas $where AND aceptado='NO';",true,true);
	$res['rechazadas']=$consulta['total'];

	$consulta=consultaBD("SELECT * FROM preventas $where AND aceptado='NO';",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
    $res['rechazadasImporte']=number_format((float)$total, 2, ',', '');

    $consulta=consultaBD("SELECT COUNT(preventas.codigo) AS total FROM preventas $where AND documentos='SI' AND verificada='NO' AND aceptado!='NO';",true,true);
	$res['noVerificadas']=$consulta['total'];

	$consulta=consultaBD("SELECT * FROM preventas $where AND documentos='SI' AND verificada='NO' AND aceptado!='NO';",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
    $res['noVerificadasImporte']=number_format((float)$total, 2, ',', '');

    $consulta=consultaBD("SELECT COUNT(preventas.codigo) AS total FROM preventas $where AND documentos='SI' AND verificada='SI';",true,true);
	$res['siVerificadas']=$consulta['total'];

	$consulta=consultaBD("SELECT * FROM preventas $where AND documentos='SI' AND verificada='SI';",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
    $res['siVerificadasImporte']=number_format((float)$total, 2, ',', '');

    $consulta=consultaBD("SELECT COUNT(preventas.codigo) AS total FROM preventas $where AND documentos='SI' AND verificada='SI' AND aceptado='SINEVALUAR';",true,true);
	$res['noFacturado']=$consulta['total'];

	$consulta=consultaBD("SELECT * FROM preventas $where AND documentos='SI' AND verificada='SI' AND aceptado='SINEVALUAR';",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
    $res['noFacturadoImporte']=number_format((float)$total, 2, ',', '');

    $consulta=consultaBD("SELECT COUNT(preventas.codigo) AS total FROM preventas $where AND documentos='SI' AND verificada='SI' AND aceptado='SI';",true,true);
	$res['facturado']=$consulta['total'];

	$consulta=consultaBD("SELECT * FROM preventas $where AND documentos='SI' AND verificada='SI' AND aceptado='SI';",true);
	$total=0;
    while($preventa=mysql_fetch_assoc($consulta)){
    	for($i=0;$i<count($servicios);$i++){
        	if($preventa[$servicios[$i]]=='SI'){
            	  	$total=$total+$preventa[$servicios[$i].'Precio'];
            }
        }
    }
    $res['facturadoImporte']=number_format((float)$total, 2, ',', '');

    return $res;
}

function imprimeColaboradores(){
	conexionBD();
	$consulta=consultaBD("SELECT colaboradores.*, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS comercial FROM colaboradores LEFT JOIN usuarios ON colaboradores.codigoUsuario=usuarios.codigo ORDER BY empresa DESC;");
	
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		echo "
		<tr>
			<td> ".$datos['referencia']." </td>
        	<td> ".$datos['empresa']." </td>
			<td> ".$datos['telefono']." </td>
			<td> ".$datos['mail']." </td>
			<td> ".$datos['contacto']." </td>
			<td> ".$datos['comercial']." </td>
			<td> ".formateaFechaWeb($datos['fechaFirma'])." </td>
			<td> ".$datos['localidad']." </td>";
			if($_SESSION['tipoUsuario']!='TELECONCERTADOR' && $_SESSION['tipoUsuario']!='MARKETING'){			
				echo "<td>
					<div class='btn-group'>
						<button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'><i class='icon-download'></i> Opciones <span class='caret'></span></button>
							<ul class='dropdown-menu inverse' role='menu'>
								<li><a href='detallesColaborador.php?codigo=".$datos['codigo']."'><span class='label label-primary'><i class='icon-zoom-in'></i> Ver datos</a></span></li>
								<li><a href='posiblesClientesColaborador.php?codigo=".$datos['codigo']."'><span class='label label-warning'><i class='icon-exclamation-sign'></i> Posibles Clientes</i></a></span></li>
								<li><a href='cuentasColaborador.php?codigo=".$datos['codigo']."'><span class='label label-success'><i class='icon-ok-sign'></i> Cuentas</i></a></span></li>
							</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
				</td>";
			}
			echo "
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}


function insertaHistorico($codigoCliente){
	$res=true;
	
	$datos=arrayFormulario();
	if($datos['observaciones']!=''){
		$consulta=consultaBD("INSERT INTO historico_clientes VALUES(NULL, '".$datos['observaciones']."', '".formateaFechaBD(fecha())."', '".date('H:i:s')."', '$codigoCliente', '".$_SESSION['codigoS']."');");
		if(!$consulta){
			$res=false;
			echo mysql_error();
		}
	}
	
	return $res;
}

function insertaHistoricoFacturas($codigoFactura){
	$res=true;
	
	$datos=arrayFormulario();
	$i=0;
	conexionBD();
	if($datos['observaciones']!=''){
		$consulta=consultaBD("INSERT INTO historico_facturas VALUES(NULL, '".$datos['observaciones']."', '".formateaFechaBD(fecha())."', '".date('H:i:s')."', '$codigoFactura');");
		if(!$consulta){
			$res=false;
		}
	}
	cierraBD();
	
	return $res;
}

function insertaVtoFacturas($codigoFactura){
	$res=true;
	
	$datos=arrayFormulario();
	$i=0;
	$factura=datosRegistro('facturacion',$codigoFactura);
	while(isset($datos['fecha'.$i])){
		if($datos['importe'.$i] != ''){
			if(!isset($datos['codigoExiste'.$i]) || $datos['codigoExiste'.$i]==''){
				$cobrada=$factura['cobrada']=='SI'?'SI':$datos['cobrada'.$i];
				$enviada=$factura['enviada']=='SI'?'SI':$datos['enviada'.$i];
				$res=consultaBD("INSERT INTO facturas_vto VALUES(NULL, '".$codigoFactura."','".$datos['fecha'.$i]."', '".$datos['importe'.$i]."','".$enviada."', '".$cobrada."');",true);
			} else {
				$res=consultaBD("UPDATE facturas_vto SET fecha='".$datos['fecha'.$i]."',importe='".$datos['importe'.$i]."',enviada='".$datos['enviada'.$i]."',cobrada='".$datos['cobrada'.$i]."' WHERE codigo=".$datos['codigoExiste'.$i],true);
			}
		} else {
			if($datos['codigoExiste'.$i]!=''){
				$res=consultaBD('DELETE FROM facturas_vto WHERE codigo='.$datos['codigoExiste'.$i],true);
			}
		}
		$i++;
	}
	
	return $res;
}

function insertaDevolucionesFacturas($codigoFactura){
	$res=true;
	
	$datos=arrayFormulario();
	$res=consultaBD('DELETE FROM devoluciones_facturas WHERE codigoFactura='.$codigoFactura,true);
	$i=0;
	while(isset($datos['fechaDevolucion'.$i])){
		if($datos['motivo'.$i] != ''){
			$res=consultaBD("INSERT INTO devoluciones_facturas VALUES(NULL, '".$codigoFactura."','".$datos['fechaDevolucion'.$i]."', '".$datos['motivo'.$i]."','".$datos['resolucionFactura'.$i]."');",true);
		}
		$i++;
	}
	
	return $res;
}

function enviaFactura($codigo,$remesa){
	$res=true;
	$consulta=consultaBD("UPDATE facturas_vto SET enviada='SI', cobrada='SI' WHERE codigo='$codigo';",true);
	if(!$consulta){
		$res=false;
	} else {
		$vto=datosRegistro('facturas_vto',$codigo);
		$factura=datosRegistro('facturacion',$vto['codigoFactura']);
		$res= $consulta=consultaBD("UPDATE facturacion SET devuelta='NO' WHERE codigo='".$factura['codigo']."';",true);
		$consulta=consultaBD("SELECT * FROM facturas_vto WHERE enviada='NO' AND codigoFactura=".$factura['codigo'],true);
		if(mysql_num_rows($consulta) == 0){
			$res= $consulta=consultaBD("UPDATE facturacion SET enviada='SI', cobrada='SI' WHERE codigo='".$factura['codigo']."';",true);
		}
		$res=consultaBD("INSERT INTO remesas_recibos VALUES(NULL,".$remesa.",".$codigo.");",true);
	}
	return $res;
}

function compruebaObjetivosImporte(){
	if($_SESSION['tipoUsuario']=='COMERCIAL' || $_SESSION['tipoUsuario']=='ADMIN'){//Añadido ADMIN en la actualización del 20/10/2014
		$codigo=$_SESSION['codigoS'];

		conexionBD();
		//POR HACER: mejorar la consulta para que solo haga falta ejecutar 1
		$consulta=consultaBD("SELECT objetivos.importe, objetivos.fechaFin FROM objetivos INNER JOIN objetivos_asignados_comerciales ON objetivos.codigo=objetivos_asignados_comerciales.codigoObjetivo WHERE objetivos.fechaInicio<=CURDATE() AND objetivos.fechaFin>=CURDATE() AND objetivos_asignados_comerciales.codigoUsuario='$codigo';");

		if(mysql_num_rows($consulta)==0){
			echo '<h6 class="bigstats marginAbPeque">Sin objetivo asignado</h6>';
		}
		else{
			$consulta=mysql_fetch_assoc($consulta);
			$valorObjetivo=$consulta['importe'];
			$fechaObjetivo=$consulta['fechaFin'];

			$consulta=consultaBD("SELECT SUM(ventas.precio) AS codigo, objetivos.importe, objetivos.fechaFin FROM (((ventas INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo) INNER JOIN usuarios ON clientes.codigoUsuario=usuarios.codigo) INNER JOIN objetivos_asignados_comerciales ON usuarios.codigo=objetivos_asignados_comerciales.codigoUsuario) INNER JOIN objetivos ON objetivos_asignados_comerciales.codigoObjetivo=objetivos.codigo WHERE ventas.fecha>=objetivos.fechaInicio AND ventas.fecha<=objetivos.fechaFin AND objetivos.fechaInicio<=CURDATE() AND objetivos.fechaFin>=CURDATE() AND objetivos_asignados_comerciales.codigoUsuario='$codigo';");
			$reg=mysql_fetch_assoc($consulta);

			$tipoBarra='';
			while($reg!=false){
				if($reg['importe']!=NULL){
					$porcen=floor(($reg['codigo']*100)/$reg['importe']);//Calculo el porcentaje de ventas realizadas con respecto al total, y se lo paso a la función floor, que reondea los decimales a la baja.
					
					if($porcen<16){
						$tipoBarra='progress-danger';
					}
					elseif($porcen<51){
						$tipoBarra='progress-warning';
					}
					elseif($porcen<76){
						$tipoBarra='progress-primary';
					}
					elseif($porcen>=76){
						$tipoBarra='progress-success';
					}
					$valorObjetivo=$reg['importe'];
					$fechaObjetivo=$reg['fechaFin'];
				}
				else{
					$porcen=0;
					$tipoBarra='';
				}
				$reg=mysql_fetch_assoc($consulta);
				echo '
				<div class="cajaObjetivo">
			 		<h6><i class="icon icon-flag"></i> Objetivo vigente: conseguir '.$valorObjetivo.' € antes del '.formateaFechaWeb($fechaObjetivo).' (<strong>'.$porcen.'% completado</strong>)</h6>
					<div class="progress '.$tipoBarra.' progress-striped active">
	                    <div class="bar" aria-valuetransitiongoal="'.$porcen.'"></div>
					</div>
				</div>';
			}
		}
		cierraBD();
	}

}

function creaTarea($preventa=false){
	$res=true;
	if(!isset($_POST['horaInicio']) || !isset($_POST['horaFin'])){
		$_POST['horaInicio']='';
		$_POST['horaFin']='';
	}elseif(!isset($_POST['todoDia'])){
		$_POST['todoDia']='NO';
	}
	$horaRealizacion='';
	if($_POST['estado']=='realizada'){
		$horaRealizacion=date('H:i:s');
	}
    $i=0;
	while(isset($_POST['codigoLista'][$i])){
		$_POST['codigoCliente']=$_POST['codigoLista'][$i];
		$res=insertaDatos('tareas');
		$codigoTarea=$res;
		$res=$res && insertaHistoricoTareas($codigoTarea);
		$i++;
	}
	if($preventa){
		$res=$codigoTarea;
	}
	return $res;
}

function actualizaTarea(){
	$res=true;
	if(!isset($_POST['horaInicio']) || !isset($_POST['horaFin'])){
		$_POST['horaInicio']='';
		$_POST['horaFin']='';
	}elseif(!isset($_POST['todoDia'])){
		$_POST['todoDia']='NO';
	}

	if($_POST['prioridad']!='ALTA'){
		$preventa=datosRegistro('preventas',$_POST['codigo'],'codigoTarea');
		$res=consultaBD('DELETE FROM preventas WHERE codigo='.$preventa['codigo'],true);
	}
    $res=actualizaDatos('tareas');
	$res=$res && insertaHistoricoTareas($_POST['codigo']);
	return $res;
}

function insertaHistoricoTareas($codigoTarea){
	$res=true;
	
	$datosHistorico=arrayFormulario();
	conexionBD();
	if($datosHistorico['observaciones']!=''){
		$consulta=consultaBD("INSERT INTO historico_tareas VALUES(NULL, '".$datosHistorico['observaciones']."', '".formateaFechaBD(fecha())."', '".date('H:i:s')."', '$codigoTarea');");
		if(!$consulta){
			$res=false;
			echo mysql_error();
		}
	}
	cierraBD();
	
	return $res;
}


function datosRegistroMultiple($tabla){

	return consultaBD("SELECT * FROM $tabla;",true,false);

}

function actualizaFirmas(){
	$datos=arrayFormulario();
	$res=true;
	$i=1;
	while(isset($datos['firma'.$i])){
		$val=consultaBD("INSERT INTO firmas VALUES('$i','".$datos['firma'.$i]."','".$datos['cif'.$i]."','".$datos['direccion'.$i]."','".$datos['cp'.$i]."','".$datos['localidad'.$i]."','".$datos['telefono'.$i]."');",true);
		if(!$val){
			$res=$res && consultaBD("UPDATE firmas SET firma='".$datos['firma'.$i]."', cif='".$datos['cif'.$i]."', direccion='".$datos['direccion'.$i]."', cp='".$datos['cp'.$i]."', localidad='".$datos['localidad'.$i]."', telefono='".$datos['telefono'.$i]."' WHERE codigo='$i';",true);
		}
		$i++;
	}
	return $res;
}

function selectClientesMultiple($codigo=false){
	
	//$codigoU=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	echo "<select name='clientes' id='clientes' class='selectpicker span4 show-tick' data-live-search='true' multiple data-selected-text-format='count' multiple title='Seleccione a los que desee enviar mail...'>";
	conexionBD();
	$consulta=consultaBD("SELECT codigo, empresa FROM clientes $where;");
	cierraBD();
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=false){
		echo "<option value='".$datos['codigo']."'";

		if($codigo!=false && $codigo==$datos['codigo']){
			echo " selected='selected'";
		}

		echo ">".$datos['empresa']."</option>";
		$datos=mysql_fetch_assoc($consulta);
	}
	echo "</select>";
}

function imprimeFacturasDescarga($fechaUno, $fechaDos, $firma, $sepa){
	//$codigoS=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	conexionBD();

	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.referencia, productos.nombreProducto AS concepto, coste, empresa, fechaEmision, fechaVencimiento, clientes.cp, clientes.numCuenta 
	FROM facturacion LEFT JOIN clientes ON facturacion.codigoCliente=clientes.codigo 
	LEFT JOIN productos ON facturacion.concepto=productos.codigo
	$where AND fechaVencimiento>='$fechaUno' AND fechaVencimiento<='$fechaDos' AND firma='$firma' AND enviada='NO' AND facturacion.formaPago='domiciliacion' AND clientes.tipoRemesa='$sepa' AND coste > 0 ORDER BY fechaEmision DESC;");
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		$clase='';
		$mensaje='';
		$iconoAviso='';
		$titulo=$datos['empresa'];
		if(strlen($datos['numCuenta'])<24 || strlen($datos['cp'])<5 || strlen($datos['numCuenta'])>24){
			$clase='mensajeAviso';
			$iconoAviso=' <span class="label label-danger"><i class="icon-exclamation-sign"></i></span>';
			if(strlen($datos['numCuenta'])<24 || strlen($datos['numCuenta'])>24){
				$mensaje='Número de cuenta erróneo.';
			}
			if(strlen($datos['cp'])<5){
				$mensaje='Código postal erróneo.';
			}
			if(strlen($datos['cp'])<5 && (strlen($datos['numCuenta'])<24 || strlen($datos['numCuenta'])>24)){
				$mensaje='Número de cuenta erróneo.<br>Código postal erróneo.';
			}
		}
		echo "
		<tr>
        	<td> ".$datos['referencia']." </td>
        	<td class='$clase' mensaje='$mensaje' titulo='$titulo'> ".$datos['empresa']." $iconoAviso</td>
        	<td> ".$datos['concepto']." </td>
			<td> ".formateaFechaWeb($datos['fechaEmision'])." </td>
			<td> ".formateaFechaWeb($datos['fechaVencimiento'])." </td>
        	<td> ".formateaNumero($datos['coste'])." </td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' checked='checked'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function imprimeFacturasVtoDescarga($fechaUno, $fechaDos, $firma, $sepa){
	//$codigoS=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	conexionBD();

	$consulta=consultaBD("SELECT facturas_vto.codigo, facturacion.referencia, productos.nombreProducto AS concepto, facturas_vto.importe AS coste, empresa, fechaEmision, fechaVencimiento, facturas_vto.fecha AS fechaVto, clientes.cp, clientes.numCuenta 
	FROM facturacion LEFT JOIN clientes ON facturacion.codigoCliente=clientes.codigo 
	LEFT JOIN productos ON facturacion.concepto=productos.codigo
	INNER JOIN facturas_vto ON facturacion.codigo=facturas_vto.codigoFactura
	$where AND facturas_vto.fecha>='$fechaUno' AND facturas_vto.fecha<='$fechaDos' AND firma='$firma' AND (facturas_vto.enviada='NO' OR (facturas_vto.enviada='SI' AND facturacion.devuelta='SI')) AND facturacion.formaPago='domiciliacion' AND clientes.tipoRemesa='$sepa' AND coste > 0 ORDER BY facturas_vto.fecha DESC;");
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		$clase='';
		$mensaje='';
		$iconoAviso='';
		$titulo=$datos['empresa'];
		if(strlen($datos['numCuenta'])<24 || strlen($datos['cp'])<5 || strlen($datos['numCuenta'])>24){
			$clase='mensajeAviso';
			$iconoAviso=' <span class="label label-danger"><i class="icon-exclamation-sign"></i></span>';
			if(strlen($datos['numCuenta'])<24 || strlen($datos['numCuenta'])>24){
				$mensaje='Número de cuenta erróneo.';
			}
			if(strlen($datos['cp'])<5){
				$mensaje='Código postal erróneo.';
			}
			if(strlen($datos['cp'])<5 && (strlen($datos['numCuenta'])<24 || strlen($datos['numCuenta'])>24)){
				$mensaje='Número de cuenta erróneo.<br>Código postal erróneo.';
			}
		}
		echo "
		<tr>
        	<td> ".$datos['referencia']." </td>
        	<td class='$clase' mensaje='$mensaje' titulo='$titulo'> ".$datos['empresa']." $iconoAviso</td>
        	<td> ".$datos['concepto']." </td>
			<td> ".formateaFechaWeb($datos['fechaEmision'])." </td>
			<td> ".formateaFechaWeb($datos['fechaVto'])." </td>
        	<td> ".formateaNumero($datos['coste'])." </td>
			<td>
				<input type='checkbox' importe='".$datos['coste']."' name='codigoLista[]' value='".$datos['codigo']."' checked='checked'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function imprimeFacturasVtoDescarga2($codigo){
	//$codigoS=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	$consulta=consultaBD("SELECT * FROM remesas_recibos WHERE codigoRemesa=".$codigo,true);
	

	while($datos=mysql_fetch_assoc($consulta)){
		$datos=consultaBD("SELECT facturas_vto.codigo, facturacion.referencia, productos.nombreProducto AS concepto, facturas_vto.importe AS coste, empresa, fechaEmision, fechaVencimiento, facturas_vto.fecha AS fechaVto, clientes.cp, clientes.numCuenta 
	FROM facturacion LEFT JOIN clientes ON facturacion.codigoCliente=clientes.codigo 
	LEFT JOIN productos ON facturacion.concepto=productos.codigo
	INNER JOIN facturas_vto ON facturacion.codigo=facturas_vto.codigoFactura
	WHERE facturas_vto.codigo=".$datos['codigoRecibo'],true,true);

		$clase='';
		$mensaje='';
		$iconoAviso='';
		$titulo=$datos['empresa'];
		if(strlen($datos['numCuenta'])<24 || strlen($datos['cp'])<5 || strlen($datos['numCuenta'])>24){
			$clase='mensajeAviso';
			$iconoAviso=' <span class="label label-danger"><i class="icon-exclamation-sign"></i></span>';
			if(strlen($datos['numCuenta'])<24 || strlen($datos['numCuenta'])>24){
				$mensaje='Número de cuenta erróneo.';
			}
			if(strlen($datos['cp'])<5){
				$mensaje='Código postal erróneo.';
			}
			if(strlen($datos['cp'])<5 && (strlen($datos['numCuenta'])<24 || strlen($datos['numCuenta'])>24)){
				$mensaje='Número de cuenta erróneo.<br>Código postal erróneo.';
			}
		}
		echo "
		<tr>
        	<td> ".$datos['referencia']." </td>
        	<td class='$clase' mensaje='$mensaje' titulo='$titulo'> ".$datos['empresa']." $iconoAviso</td>
        	<td> ".$datos['concepto']." </td>
			<td> ".formateaFechaWeb($datos['fechaEmision'])." </td>
			<td> ".formateaFechaWeb($datos['fechaVto'])." </td>
        	<td> ".formateaNumero($datos['coste'])." </td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' checked='checked' disabled>
        	</td>
    	</tr>";
	}
}

function enviaInformeFacturacion(){
	$res=true;
	$headers="From: programacion@qmaconsultores.com\r\n";
	$headers.= "MIME-Version: 1.0\r\n";
		$headers.= "Content-Type: text/html; charset=UTF-8";
		if(date('d')=='01'){
			$dia=date('d', strtotime('-1 day'));
			$mes=date('m', strtotime('-1 month'));
			$anio=date('Y');
		}else{
			$dia=date('d', strtotime('-1 day'));
			$mes=date('m');
			$anio=date('Y');
		}
		$mensaje='Informe de facturación diaria '.$dia.'-'.$mes.'-'.$anio;
		$mensaje.='<br><br>Totales facturados por usuarios<br><br>';
		conexionBD();
		$consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS nombre FROM usuarios;");
		while($datosUsuarios=mysql_fetch_assoc($consulta)){
			$consultaAux=consultaBD("SELECT SUM(coste) AS total FROM facturacion WHERE codigoUsuario='".$datosUsuarios['codigo']."' AND insercion LIKE'".$anio."-".$mes."-".$dia."%';");
			$total=mysql_fetch_assoc($consultaAux);
			if($total['total']!='' && $total['total']!=NULL){
				$mensaje.=$datosUsuarios['nombre'].': '.$total['total'].'<br>';
			}
		}
		$mensaje.='<br>Totales facturados por departamentos<br><br>';
		$consulta=consultaBD("SELECT DISTINCT tipo FROM usuarios;");
		$tipo=array('ADMIN'=>'Administradores','ADMINISTRACION'=>'Administración','COMERCIAL'=>'Comercial','CONSULTORIA'=>'Consultoría','FORMACION'=>'Formación','ATENCION'=>'Atención al cliente');
		
		while($datosUsuarios=mysql_fetch_assoc($consulta)){
			$consultaAux=consultaBD("SELECT SUM(coste) AS total FROM facturacion INNER JOIN usuarios ON facturacion.codigoUsuario=usuarios.codigo WHERE usuarios.tipo='".$datosUsuarios['tipo']."' AND insercion LIKE'".$anio."-".$mes."-".$dia."%';");
			$total=mysql_fetch_assoc($consultaAux);
			if($total['total']!='' && $total['total']!=NULL){
				$mensaje.=$tipo[$datosUsuarios['tipo']].': '.$total['total'].'<br>';
			}
		}
		cierraBD();
	if($_SESSION['codigoS'] != 'daniel'){
		if (!mail('miquel.lopez@grupqualia.es ', 'Resumen facturación diaria '.$dia.'-'.$mes.'-'.$anio, $mensaje ,$headers)){
				$res=false;
			}
	}
	return $res;
}

function insertaHistoricoColaborador($codigoColaborador){
	$res=true;
	
	$datos=arrayFormulario();
	if($datos['observaciones']!=''){
		$consulta=consultaBD("INSERT INTO historico_colaboradores VALUES(NULL, '".$datos['observaciones']."', '".formateaFechaBD(fecha())."', '".date('H:i:s')."', '$codigoColaborador');",true);
		if(!$consulta){
			$res=false;
			echo mysql_error();
		}
	}
	
	return $res;
}

function programaTarea(){
	$res=true;
	if(!isset($_POST['horaInicio']) || !isset($_POST['horaFin'])){
		$_POST['horaInicio']='';
		$_POST['horaFin']='';
	}elseif(!isset($_POST['todoDia'])){
		$_POST['todoDia']='NO';
	}
	$horaRealizacion='';
	if($_POST['estado']=='realizada'){
		$horaRealizacion=date('H:i:s');
	}
	$_POST['horaRealizacion']='';
	$res=insertaDatos('tareas');
	$codigoTarea=$res;
	
	$datos=arrayFormulario();
	
	$res=$res && insertaHistoricoTareas($codigoTarea);
	conexionBD();	
	$consulta=consultaBD("SELECT * FROM historico_tareas WHERE codigoTarea='".$datos['tareaAnterior']."';");
	$datosTareas=mysql_fetch_assoc($consulta);
	while(isset($datosTareas['codigo'])){
		$consultaDos=consultaBD("INSERT INTO historico_tareas VALUES(NULL, '".$datosTareas['observaciones']."','".$datosTareas['fecha']."','".$datosTareas['hora']."','$codigoTarea');");
		$datosTareas=mysql_fetch_assoc($consulta);
	}
	cierraBD();
	
	return $res;
}

function enviaFacturaCliente($codigo){
	$res=true;
	$consulta=consultaBD("UPDATE facturacion SET enviadaCliente='SI' WHERE codigo='$codigo';",true);
	if(!$consulta){
		$res=false;
	}
	return $res;
}

function correos($tabla,$campo='mail'){
	$res='';

	conexionBD();
	$consultaC=consultaBD("SELECT $campo FROM $tabla;",false);
	cierraBD();

	while($datos=mysql_fetch_assoc($consultaC)){
		if(trim($datos[$campo])!=''){
			$res.=$datos[$campo].', ';
		}
	}

	$res= substr_replace($res, '', strlen($res)-2, strlen($res));//Para quitar última 
	return $res;
}

function compruebaPerfilParaWhereTareas($campo=false){
	$where='';
	if($_SESSION['tipoUsuario']=='ADMIN'||$_SESSION['codigoS']=='57'){
		$where="WHERE 1=1";
	}
	else{
		$codigoU=$_SESSION['codigoS'];
		$where="WHERE ($campo='$codigoU' OR $campo IN (SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU'))";
	}
	return $where;
}

function compruebaPerfilParaHavingTareas($campo=false){
	$where='';
	if($_SESSION['tipoUsuario']=='ADMIN'||$_SESSION['codigoS']=='57'){
		$where="HAVING 1=1";
	}
	else{
		$codigoU=$_SESSION['codigoS'];
		$where="HAVING ($campo='$codigoU' OR $campo IN (SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU'))";
	}
	return $where;
}

function creaEstadisticasVentasGeneral($and=''){
	$datos=array();
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='SUPERVISOR'){
		$where=compruebaPerfilParaWhere('ventas.codigoUsuario');
	}else{
		$where="WHERE fecha>='2015-03-01' AND fecha<='2015-12-31'";
	}

	conexionBD();	

	$consulta=consultaBD("SELECT COUNT(ventas.codigo) AS codigo FROM ventas LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo $where $and AND clientes.baja='NO';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=$consulta['codigo'];

	$consulta=consultaBD("SELECT SUM(CAST(REPLACE(ventas.precio, ',', '.') as DECIMAL(10,2))) AS codigo FROM ventas LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo $where $and AND clientes.baja='NO';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['preciototal']=$consulta['codigo'];


	$consulta=consultaBD("SELECT SUM(CAST(REPLACE(ventas.precio, ',', '.') as DECIMAL(10,2))) AS precioNueva FROM ventas LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo $where $and AND tipoVentaCarteraNueva='NUEVA' AND clientes.baja='NO';");
	$consulta=mysql_fetch_assoc($consulta);

	if($consulta['precioNueva']==''){
		$datos['precioNueva']='0';
	}
	else{
		$datos['precioNueva']=$consulta['precioNueva'];
	}
	
	$consulta=consultaBD("SELECT SUM(CAST(REPLACE(ventas.precio, ',', '.') as DECIMAL(10,2))) AS precioCartera FROM ventas LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo $where $and AND tipoVentaCarteraNueva='CARTERA' AND clientes.baja='NO';");
	$consulta=mysql_fetch_assoc($consulta);

	if($consulta['precioCartera']==''){
		$datos['precioCartera']='0';
	}
	else{
		$datos['precioCartera']=$consulta['precioCartera'];
	}

	cierraBD();

	return $datos;
}

function imprimeVentasGeneral($and=''){
	conexionBD();
	$where="WHERE 1=1";
	if($_SESSION['tipoUsuario']!='SUPERVISOR'){
		$where=compruebaPerfilParaWhere('ventas.codigoUsuario');
	}
	$consulta=consultaBD("SELECT ventas.codigo, ventas.tipo, concepto, precio, fecha, observaciones, empresa, activo, clientes.codigo AS codigoCliente, CONCAT(nombre, ' ', apellidos) AS comercial, productos.nombreProducto, tipoVentaCarteraNueva, referencia FROM ventas
	INNER JOIN clientes ON clientes.codigo=ventas.codigoCliente LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo
	LEFT JOIN productos ON ventas.concepto=productos.codigo	$where $and AND clientes.baja='NO' ORDER BY empresa, fecha DESC;");
	echo mysql_error();
	$datos=mysql_fetch_assoc($consulta);

	$destino=array('SI'=>'detallesCuenta.php', 'NO'=>'detallesPosibleCliente.php');

	while($datos!=0){
		$fecha=formateaFechaWeb($datos['fecha']);
		
		echo "
		<tr>
			<td>".$datos['referencia']."</td>
			<td> <a href='".$destino[$datos['activo']]."?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a> </td>
        	<td> ".ucfirst($datos['tipo'])." </td>
			<td> ".$datos['comercial']." </td>
        	<td> ".$datos['nombreProducto']." </td>
			<td> $fecha </td>
			<td> ".$datos['precio']." € </td>
			<td> ".ucfirst($datos['observaciones'])." </td>
			<td> ".$datos['tipoVentaCarteraNueva']." </td>
        	<td class='centro'>
        		<a href='detallesVentaGeneral.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
        	</td>
        	<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function compruebaOpcionEliminarAlumnos(){
	if($_SESSION['usuario']=='ylopez' || $_SESSION['usuario']=='mlopez'){
		echo '<a href="#" id="eliminarAlumno" class="shortcut"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>';
	}
}


function registraVentaAuxi(){
	$verifica = 1;  
  	$_SESSION["verifica"] = $verifica;  
	$datos=arrayFormulario();
	
	conexionBD();

	if($datos['activa']==1){
		$cliente=datosRegistro('clientes',$datos['codigoC']);
		conexionBD();
		if($cliente['activo']!='SI'){
			$anio=date('Y')+1;
			$fecha=$anio.'-'.date('m').'-'.date('d');
			$consultaAux=consultaBD("SELECT referencia FROM clientes WHERE activo='SI' ORDER BY referencia DESC LIMIT 1;");
			$referencia=mysql_fetch_assoc($consultaAux);
			$referenciaNueva=$referencia['referencia']+1;
		} else {
			$referenciaNueva=$cliente['referencia'];
		}
		$consulta=consultaBD("UPDATE clientes SET activo='SI', fechaVenta='".$datos['fechaVenta']."', referencia='$referenciaNueva'
				WHERE codigo='".$datos['codigoC']."';");
	}
	
	
	$consulta=consultaBD("SELECT tipoServicio FROM clientes WHERE codigo='".$datos['codigoC']."';");
	$tipoServicio=mysql_fetch_assoc($consulta);
	$consulta=consultaBD("INSERT INTO ofertas VALUES(NULL, '', '', '".formateaFechaBD(fecha())."', '', 'ACEPTADA', '".$tipoServicio['tipoServicio']."', '".$datos['codigoC']."', '".$_SESSION['codigoS']."');");
	$codigoOferta=mysql_insert_id();
	$fecha = date_create(date('Y-m-d'));
	$intervalo=compruebaIntervalo($datos['concepto']);
	date_add($fecha, date_interval_create_from_date_string($intervalo));
	$fechaInsertar=date_format($fecha, 'Y-m-d');
	if($datos['concepto']=='14'){
			$formacion=$datos['formacionCurso'];
		} else {
			if($datos['codigoVentaRelacionada'] == 'NULL'){
				$formacion='NO';
			} else {
				cierraBD();
				$ventaRelacionada=datosRegistro('ventas',$datos['codigoVentaRelacionada']);
				$ofertaRelacionada=datosRegistro('ofertas',$ventaRelacionada['codigo'],'codigoOferta');
				$trabajo=datosRegistro('trabajos',$ofertaRelacionada['codigo'],'codigoOferta');
				$consulta=consultaBD("INSERT INTO ventas_relacionadas VALUES(NULL,".$ventaRelacionada['codigo'].",".$codigoVenta.")",true);
				$alumno=datosRegistro('alumnos',$ventaRelacionada['codigo'],'codigoVenta');
				$alumno=datosRegistro('alumnos_registrados_cursos',$alumno['codigo'],'codigoAlumno');
				if($alumno['resolucion']=='SI'){
					$formacion='FINALIZADO';
				} else {
					$formacion='HECHO';
				}
				conexionBD();
			}
		}
	$consulta=consultaBD("INSERT INTO trabajos VALUES(NULL, '$codigoOferta', '".$datos['codigoC']."', '".$tipoServicio['tipoServicio']."', '', '".$datos['proyecto']."', '', 'PROCESO',
	'$fechaInsertar','".$formacion."','".$datos['prl']."','".$datos['lopd']."','".$datos['lssi']."','".$datos['aler']."','".$datos['web']."','".$datos['auditoria1']."','".$datos['auditoria2']."',NULL);");
	$codigoTrabajo=mysql_insert_id();
	$consulta=consultaBD("INSERT INTO hitos VALUES(NULL, '$codigoTrabajo', '".$datos['actividad0']."', '".$datos['fechaPrevista0']."', '', '".$datos['observaciones0']."');");
	$consulta=consultaBD("INSERT INTO hitos VALUES(NULL, '$codigoTrabajo', '".$datos['actividad1']."', '".$datos['fechaPrevista1']."', '', '".$datos['observaciones1']."');");
	$consulta=consultaBD("INSERT INTO hitos VALUES(NULL, '$codigoTrabajo', '".$datos['actividad2']."', '".$datos['fechaPrevista2']."', '', '".$datos['observaciones2']."');");
		
	if($datos['concepto']!='14'){
		$_POST['codigoCurso']='NULL';		
	}
	
	$consulta=consultaBD("UPDATE clientes SET fechaUltimaVenta='".$datos['fechaVenta']."' WHERE codigo='".$datos['codigoC']."';");	

	$consulta=consultaBD("INSERT INTO ventas VALUES(NULL, '".$datos['tipo']."', '".$datos['concepto']."', '".$datos['precio']."', '".$datos['fechaVenta']."',
		'".$datos['observaciones']."','".$datos['participaColaborador']."','".$datos['comision']."','".$datos['tipoVentaCarteraNueva']."','".$datos['codigoC']."','".$datos['codigoUsuario']."');");
		$codigoVenta=mysql_insert_id();

	if(!$consulta){
	}
	elseif($datos['concepto']=='14'){
		$consulta=consultaBD("INSERT INTO historico_clientes VALUES(NULL, '".$datos['observaciones']."', '".date('Y-m-d')."', '".date('H:i:s')."', '".$datos['codigoC']."', '".$_SESSION['codigoS']."');");
		$i=0;
		//NUEVO PARA VENTA DIRECTA DE CURSO
		$arrayAlumnos=array();
		while(isset($datos['nombre'.$i])){
			$consulta=consultaBD("INSERT INTO alumnos(codigo,nombre,apellidos,dni,mail,telefono,codigoVenta,codigoUsuario) VALUES(NULL,'".$datos['nombre'.$i]."','".$datos['apellidos'.$i]."','".$datos['dni'.$i]."','".$datos['mail'.$i]."','".$datos['tlf'.$i]."','".$codigoVenta."','".$_SESSION['codigoS']."');");
			$codigoAlumno=mysql_insert_id();
			array_push($arrayAlumnos, $codigoAlumno);
			$i++;
		}
		if(isset($datos['alumnosPrevios'])){
			foreach($datos['alumnosPrevios'] as $alumno){ 
				array_push($arrayAlumnos, $alumno);
			}
		}
		$codigoCurso=registraCursoNuevo($arrayAlumnos,$datos);
		$_POST['codigoCurso']=$codigoCurso;
	}
	$_POST['codigoVenta']=$codigoVenta;
		if(isset($_POST['codigoPreventa'])){
			$res=consultaBD('UPDATE preventas SET aceptado="SI" WHERE codigo='.$_POST['codigoPreventa'],true);
			$res=consultaBD('INSERT INTO ventas_preventas VALUES(NULL,'.$codigoVenta.','.$_POST['codigoPreventa'].');',true);
		}

	$codigoFactura=insertaDatos('facturacion');
	$_POST['cursos']=array();
	if(!is_null($codigoCurso)){
		array_push($_POST['cursos'],$codigoCurso);
		insertaCursosFactura($codigoFactura);
	}
	insertaVtoFacturas($codigoFactura);


	return $codigoVenta;
}

function completaAlumnos(){
	$res=true;
	$datos=arrayFormulario();
	$i=0;
	conexionBD();
	while(isset($datos['nombre'.$i])){
		$datos['numSS']=$datos['numSS1'.$i].$datos['numSS2'.$i];
		$consulta=consultaBD("UPDATE alumnos SET nombre='".$datos['nombre'.$i]."', apellidos='".$datos['apellidos'.$i]."', dni='".$datos['dni'.$i]."', sexo='".$datos['sexo'.$i]."', 
		fechaNac='".$datos['fechaNac'.$i]."', telefono='".$datos['telefono'.$i]."', movil='".$datos['movil'.$i]."', mail='".$datos['mail'.$i]."',
		numSS='".$datos['numSS']."', estudios='".$datos['estudios'.$i]."', 
		categoria='".$datos['categoria'.$i]."', cotizacion='".$datos['cotizacion'.$i]."', horario='".$datos['horario'.$i]."', discapacidad='".$datos['discapacidad'.$i]."', 
		vTerrorismo='".$datos['vTerrorismo'.$i]."', vViolencia='".$datos['vViolencia'.$i]."' WHERE codigo='".$datos['codigo'.$i]."';");
		if(!$consulta){
			$res=false;
		}
		$i++;
	}
	cierraBD();
	return $res;
}

function exportarExcelCurso($objPHPExcel,$codigoCurso){
	$consulta=consultaBD("SELECT cursos.*, tutores.nombre AS nombreTutor, tutores.apellidos AS apellidosTutor, usuarios.nombre AS nombreComercial, usuarios.apellidos AS apellidoComercial, clientes.empresa, clientes.referencia, accionFormativa.modalidad, accionFormativa.codigoInterno AS codigoInternoAccionFormativa, accionFormativa.denominacion, alumnos.nombre, alumnos.apellidos, alumnos.telefono 
	FROM cursos LEFT JOIN accionFormativa ON cursos.codigoaccionFormativa=accionFormativa.codigoInterno
	LEFT JOIN tutores ON cursos.codigoTutor=tutores.codigo 
	LEFT JOIN usuarios ON cursos.comercial=usuarios.codigo
	LEFT JOIN alumnos_registrados_cursos ON cursos.codigo=alumnos_registrados_cursos.codigoCurso
	LEFT JOIN alumnos ON alumnos_registrados_cursos.codigoAlumno=alumnos.codigo
	LEFT JOIN ventas ON alumnos.codigoVenta=ventas.codigo
	LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo
	WHERE cursos.codigo='$codigoCurso';",true);
	$i=2;
	while($datos=mysql_fetch_assoc($consulta)){
		$objPHPExcel->getActiveSheet()->getCell('A'.$i)->setValue($datos['referencia']);
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($datos['nombreComercial'].' '.$datos['apellidoComercial']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($datos['empresa']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($datos['modalidad']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue('QUALIA');
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($datos['nombreTutor'].' '.$datos['apellidosTutor']);
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue($datos['codigoInternoAccionFormativa'].'/'.$datos['codigoInterno']);
		$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue($datos['nombre'].' '.$datos['apellidos']);
		$objPHPExcel->getActiveSheet()->getCell('I'.$i)->setValue($datos['denominacion']);
		$objPHPExcel->getActiveSheet()->getCell('J'.$i)->setValue($datos['telefono']);
		$objPHPExcel->getActiveSheet()->getCell('K'.$i)->setValue(formateaFechaWeb($datos['fechaInicio']));
		$objPHPExcel->getActiveSheet()->getCell('L'.$i)->setValue(formateaFechaWeb($datos['fechaFin']));
		$porcentaje='10%';
		if($datos['llamadaBienvenida']=='SI'){
			$porcentaje='40%';
		}
		if($datos['llamadaSeguimiento']=='SI'){
			$porcentaje='70%';
		}
		if($datos['llamadaFinalizacion']=='SI'){
			$porcentaje='90%';
		}
		$fechaFin=strtotime($datos['fechaFin']);
		$fechaActual=strtotime(date('Y').'-'.date('m').'-'.date('d'));
		if($fechaActual>$fechaFin){
			$porcentaje='100%';
		}
		$objPHPExcel->getActiveSheet()->getCell('M'.$i)->setValue($porcentaje);
		$objPHPExcel->getActiveSheet()->getCell('N'.$i)->setValue($datos['llamadaBienvenida']);
		$objPHPExcel->getActiveSheet()->getCell('O'.$i)->setValue($datos['llamadaSeguimiento']);
		$objPHPExcel->getActiveSheet()->getCell('P'.$i)->setValue($datos['llamadaFinalizacion']);
		$objPHPExcel->getActiveSheet()->getCell('Q'.$i)->setValue('');
		$objPHPExcel->getActiveSheet()->getCell('R'.$i)->setValue($datos['bonificado']);
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':R'.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('95B3D7');
		$i++;
	}
}

function generaExcelCurso($codigoCurso){
	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos/plantillaExcelCurso.xlsx");
	

	exportarExcelCurso($objPHPExcel,$codigoCurso);//Llamada a la función que rellena el Excel

	$tiempo=time();
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('documentos/excelCurso'.$tiempo.'.xlsx');
	
	return 'excelCurso'.$tiempo.'.xlsx';
}

function imprimeAlumnosNuevo($condicion="AND cursos.fechaFin>=CURDATE()"){
	$codigoU=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION' && $_SESSION['tipoUsuario']!='MARKETING' && $_SESSION['tipoUsuario']!='CONSULTORIA'){
		$where=compruebaPerfilParaWhere('alumnos.codigoUsuario');
	}
	
	if($_SESSION['tipoUsuario']=='COMERCIAL'){
		$where="WHERE alumnos.codigoVenta IN(SELECT codigo FROM ventas WHERE codigoCliente IN(
		SELECT codigo FROM clientes WHERE codigoUsuario='$codigoU' OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador='$codigoU')))";
	}

	conexionBD();

	$consulta=consultaBD("SELECT alumnos.codigo, nombre, apellidos, alumnos.telefono, alumnos.mail, empresa, fechaFin, clientes.codigo AS codigoCliente, alumnos_registrados_cursos.codigoCurso AS codigoCurso, alumnos_registrados_cursos.llamadaBienvenida, alumnos.fechaUltimaLlamada, alumnos_registrados_cursos.fechaBienvenida, alumnos_registrados_cursos.horaBienvenida, alumnos_registrados_cursos.resolucion FROM 
	(alumnos LEFT JOIN ventas ON alumnos.codigoVenta=ventas.codigo) 
	LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo 
	INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno
	INNER JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo
	$where $condicion ORDER BY apellidos, nombre;");
	$datos=mysql_fetch_assoc($consulta);

	$llamada=array('SI'=>'Si', 'NO'=>'No');
	$tipos=array("PRESENCIAL"=>"Presencial", "DISTANCIA"=>"A distancia", "MIXTA"=>"Mixta", "TELE"=>"Teleformación", "WEBINAR"=>"Webinar");

	$resoluciones = array("SI"=>"<span class='label label-success'>Curso acabado</span>","NO"=>"<span class='label label-danger'>Curso no hecho</span>","MAL"=>"<span class='label label-amarillo'>El alumno va muy mal<br/> y termina el plazo</span>","NOLLAMAR"=>"<span class='label label-azul-claro'>No llamar al alumno</span>","BONIFICADO"=>"<span class='label label-rosa'>Curso bonificado</span>","TARDE"=>"<span class='label label-primary'>Llamar por la tarde</span>","CANCELADO"=>"<span class='label label-inverse'>Cancelado</span>","COMERCIAL"=>"<span class='label label-naranja'>Hablar con comercial</span>");
	
	while($datos!=0){
	
		$consultaAux=consultaBD("SELECT clientes.empresa, accionFormativa.denominacion, accionFormativa.codigoInterno AS codigoAccion, cursos.codigoInterno, cursos.fechaInicio, cursos.fechaFin, accionFormativa.modalidad, cursos.llamadaBienvenida, clientes.codigo AS codigoCliente, accionFormativa.grupoAcciones, cursos.codigo AS codigoCurso
		FROM alumnos LEFT JOIN ventas ON alumnos.codigoVenta=ventas.codigo
		LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo
		INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno
		LEFT JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo
		LEFT JOIN accionFormativa ON cursos.codigoAccionFormativa=accionFormativa.codigoInterno WHERE alumnos.codigo='".$datos['codigo']."' AND cursos.codigo='".$datos['codigoCurso']."';");
		
		$datosAux=mysql_fetch_assoc($consultaAux);
		
		$fecha = date('Y-m-d');
		$nuevafecha = date('Y-m-d',strtotime ( '+3 days' , strtotime ( $fecha ) ) );
		if($datosAux['fechaFin']<=$nuevafecha){
			$icono=' <span class="label label-danger"><i class="icon-flag"></i></span>';
		}
		else{
			$icono='';
		}
	
		$bienvenida = $llamada[$datos['llamadaBienvenida']];
		if($bienvenida == 'Si'){
			$bienvenida = formateaFechaWeb($datos['fechaBienvenida']).' '.$datos['horaBienvenida'];
			$bienvenida = $bienvenida == ' ' ? 'Si' : $bienvenida;
		} else {
			$bienvenida = "<button codigo='".$datos['codigo']."' codigoCurso='".$datosAux['codigoCurso']."' class='btn btn-propio marcarFecha'>NO</button>";
		}
		$resolucionesSelect = array("<span class='label label-success'>Curso acabado</span>","<span class='label label-danger'>Curso no hecho</span>","<span class='label label-amarillo'>El alumno va muy mal y termina el plazo</span>","<span class='label label-azul-claro'>No llamar al alumno</span>","<span class='label label-rosa'>Curso bonificado</span>","<span class='label label-primary'>Llamar por la tarde</span>","<span class='label label-inverse'>Cancelado</span>","<span class='label label-naranja'>Hablar con comercial</span>");
		echo "
		<tr>
			<td> ".$datosAux['codigoAccion']." </td>
			<td> ".$datosAux['codigoInterno']." </td>
			<td> <a href='detallesCuenta.php?codigo=".$datosAux['codigoCliente']."'>".$datosAux['empresa']."</a> </td>
			<td> ".$datos['nombre']." ".$datos['apellidos']." $icono </td>
			<td> ".formateaFechaWeb($datosAux['fechaInicio'])." </td>
			<td> ".formateaFechaWeb($datosAux['fechaFin'])." </td>
        	<td> ".formateaTelefono($datos['telefono'])." </td>
        	<td> ".$bienvenida." </td>";
			campoSelectHTMLFormacion('resolucion','',$resolucionesSelect,array('SI','NO','MAL','NOLLAMAR','BONIFICADO','TARDE','CANCELADO','COMERCIAL'),$datos,'selectpicker span3 resolucion');
		echo"	<td> ".$tipos[$datosAux['modalidad']]." </td>
        	<td class='centro'>";
				if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='CONSULTORIA' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){
					echo"<a href='detallesAlumno.php?codigo=".$datos['codigo']."&codigoCurso=".$datosAux['codigoCurso']."' class='btn btn-primary'><i class='icon-edit'></i></i></a>";
				}
				echo "<a href='generaDocAlumno.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-download'></i></i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function imprimeDocumentosCliente($codigoCliente,$tipo='CONTRATOS'){
	$consulta=consultaBD("SELECT * FROM documentosClientes WHERE codigoCliente='$codigoCliente' AND tipo='$tipo' ORDER BY fechaFactura DESC;",true);

	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		echo "
		<tr>
        	<td> ".$datos['nombreFactura']."</td>
        	<td> ".formateaFechaWeb($datos['fechaFactura'])." </td>
        	<td class='centro'>";
				if(file_exists("documentos/".$datos['ficheroFactura'])){
					echo "<a href='documentos/".$datos['ficheroFactura']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>";
				}else{
					echo "<a href='https://softwareparapymes.net/grupqualia/documentos/".$datos['ficheroFactura']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>";
				}        		
				if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='MARKETING'){
					echo "<a href='eliminaFactura.php?codigo=".$datos['codigo']."&eliminar=1&codigoCliente=$codigoCliente' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>";
				}
			echo "
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function sanear_string($string){

    $string = trim($string);

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array("\\", "¨", "º", "-", "~",
             "#", "@", "|", "!", "\"",
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "`", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             ".","'","&nbsp;"),
        '',
        $string
    );


	 return $string;
}

function generaEventosCalendarioVentas(){
	//$codigoU=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere('tareas.codigoUsuario');

	conexionBD();
	$consulta=consultaBD("SELECT ventas.codigo, clientes.empresa, ventas.fecha, usuarios.nombre, usuarios.apellidos FROM ventas LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo INNER JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo ORDER BY fecha, usuarios.nombre, usuarios.apellidos, clientes.empresa;");

	$datos=mysql_fetch_assoc($consulta);
	$tareas="";
	$clases=array('normal'=>"backgroundColor: '#5e96ea', borderColor: '#3f85f5'");

	while($datos!=false){
		$fechaInicio=explode('-',$datos['fecha']);
		$fechaFin=explode('-',$datos['fecha']);
		$fechaInicio[1]--;
		$fechaFin[1]--;

		$tareas.="
			{
				id: ".$datos['codigo'].",
		    	title: '".addslashes($datos['nombre'])." ".addslashes($datos['apellidos']).": ".addslashes($datos['empresa'])."',
			    start: new Date(".$fechaInicio[0].", ".$fechaInicio[1].", ".$fechaInicio[2]."),
		    	end: new Date(".$fechaFin[0].", ".$fechaFin[1].", ".$fechaFin[2]."),
		    	allDay: true,
		    	url: 'detallesVentaGeneral.php?codigo=".$datos['codigo']."',
		    	".$clases['normal']."
		    	
	    	},";

		$datos=mysql_fetch_assoc($consulta);
  	}
  	$tareas=substr_replace($tareas, '', strlen($tareas)-1, strlen($tareas));//Para quitar última coma
  	echo $tareas;
}

function creaEstadisticasVentasInicio(){
	$datos=array();

	conexionBD();	

	$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha='".date('Y-m-d')."';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totalHoy']=$consulta['codigo'];
	
	# Obtenemos el día de la semana de la fecha dada
	$diaSemana=date("w",mktime(0,0,0,date('m'),date('d'),date('Y')));

	# el 0 equivale al domingo...
	if($diaSemana==0){
		$diaSemana=7;
	}

	# A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
	$primerDia=date("Y-m-d",mktime(0,0,0,date('m'),date('d')-$diaSemana+1,date('Y')));

	# A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
	$ultimoDia=date("Y-m-d",mktime(0,0,0,date('m'),date('d')+(7-$diaSemana),date('Y')));


	$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha>='$primerDia' AND fecha<='$ultimoDia';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totalSemana']=$consulta['codigo'];
	
	$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE fecha>='".date('Y-m-1')."' AND fecha<='".date("d",(mktime(0,0,0,date('m')+1,1,date('Y'))-1))."';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totalMes']=$consulta['codigo'];

	$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE tipoVentaCarteraNueva ='NUEVA' AND fecha>='".date('Y-m-1')."' AND fecha<='".date("d",(mktime(0,0,0,date('m')+1,1,date('Y'))-1))."';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totalNueva']=$consulta['codigo'];

	$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas WHERE tipoVentaCarteraNueva ='Cartera' AND fecha>='".date('Y-m-1')."' AND fecha<='".date("d",(mktime(0,0,0,date('m')+1,1,date('Y'))-1))."';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totalCartera']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function imprimeVentasGeneralInicio(){
	conexionBD();
	$where=compruebaPerfilParaWhere('ventas.codigoUsuario');
	$consulta=consultaBD("SELECT ventas.codigo, ventas.tipo, concepto, precio, fecha, observaciones, empresa, activo, clientes.codigo AS codigoCliente, CONCAT(nombre, ' ', apellidos) AS comercial, productos.nombreProducto FROM ventas
	INNER JOIN clientes ON clientes.codigo=ventas.codigoCliente LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo
	LEFT JOIN productos ON ventas.concepto=productos.codigo $where AND clientes.baja='NO' ORDER BY empresa, fecha DESC;");
	echo mysql_error();
	$datos=mysql_fetch_assoc($consulta);

	$destino=array('SI'=>'detallesCuenta.php', 'NO'=>'detallesPosibleCliente.php');

	while($datos!=0){
		$fecha=formateaFechaWeb($datos['fecha']);
		
		echo "
		<tr>
			<td> <a href='".$destino[$datos['activo']]."?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a> </td>
        	<td> ".ucfirst($datos['tipo'])." </td>
			<td> ".$datos['comercial']." </td>
        	<td> ".$datos['nombreProducto']." </td>
			<td> $fecha </td>
			<td> ".$datos['precio']." € </td>
			<td> ".ucfirst($datos['observaciones'])." </td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function obtieneMes($mesActual){
	$mes=array('01'=>'Enero', '02'=>'Febrero', '03'=>'Marzo', '04'=>'Abril', '05'=>'Mayo', '06'=>'Junio', '07'=>'Julio', '08'=>'Agosto', '09'=>'Septiembre', '10'=>'Octubre', '11'=>'Noviembre', '12'=>'Diciembre');
	return $mes[$mesActual];
}

function imprimeCarteras(){
	conexionBD();
	
	$where=compruebaPerfilParaWhere('carteras.codigoUsuario');
	
	$consulta=consultaBD("SELECT carteras_comerciales.*, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) AS comercial, carteras_comerciales.tieneColaborador, colaboradores.empresa FROM carteras_comerciales
	LEFT JOIN usuarios ON carteras_comerciales.codigoUsuario=usuarios.codigo
	LEFT JOIN colaboradores ON carteras_comerciales.colaborador=colaboradores.codigo $where ORDER BY nombre, apellidos, fechaSubida");
	echo mysql_error();
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		echo "
		<tr>
			<td> ".$datos['comercial']." </td>
        	<td> ".$datos['fechaSubida']." </td>
			<td>";
			if($datos['tieneColaborador']=='NO'){
				echo 'No';
			}else{
				echo $datos['empresa'];
			}
        	echo"</td>
			<td class='centro'>
				<a href='documentos/".$datos['fichero']."' class='btn btn-success'><i class='icon-download-alt'></i> Descargar</i></a>
        	</td>
        	<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function creaCartera(){
	$res=true;
	$datos=arrayFormulario();
	$tiempo=time();
	$res=insertaDatos('carteras_comerciales','cartera-'.$datos['fechaSubida'].''.$tiempo,'documentos');
	
	insertaCartera($datos,'cartera-'.$datos['fechaSubida'].''.$tiempo);
	
	return $res;
}

function insertaCartera($datos, $cartera){
	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos/".$cartera.".xlsx");
	
	conexionBD();
	$i=2;
	
	$empresa=addslashes($objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue());
	
	$consultaInsert="INSERT INTO clientes(codigo, empresa, cif,direccion,cp,localidad,provincia,telefono,movil,mail,fax,contacto,cargo,comercial,sector,
			tipoEmpresa,fechaRegistro,tipoServicio,fechaVenta,activo,ccc,dniRepresentante,representacion,nuevacreacion,fechaCreacion,pyme,repreLegal,numCuenta,telefonoDos,movilDos,tieneColaborador,creditoFormativo,
			formaPago,fechaUltimaVenta,bic,referencia,baja,fechaBaja,numTrabajadores,tipoRemesa,nombreGestoria,mailGestoria,telefonoGestoria,estado,direccionVisita,firmado,pendiente,codigoUsuario) VALUES";
	
	$referenciaAnterior=consultaBD("SELECT referencia FROM clientes WHERE activo='".$datos['activo']."' ORDER BY referencia DESC LIMIT 1;");
	$referenciaAnterior=mysql_fetch_assoc($referenciaAnterior);
	$referencia=$referenciaAnterior['referencia']+1;
	
	while($empresa!=''){
		$empresa=addslashes($objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue());
		$cif=addslashes($objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue());
		$nss=addslashes($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue());
		$numCuenta=addslashes($objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue());
		$bic=addslashes($objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue());
		$creditoFormativo=addslashes($objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue());
		$direccion=addslashes($objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue());
		$cp=addslashes($objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue());
		$localidad=addslashes($objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue());
		$provincia=addslashes($objPHPExcel->getActiveSheet()->getCell('K'.$i)->getValue());
		$telefono=addslashes($objPHPExcel->getActiveSheet()->getCell('L'.$i)->getValue());
		$mail=addslashes($objPHPExcel->getActiveSheet()->getCell('M'.$i)->getValue());
		$formaPago=addslashes($objPHPExcel->getActiveSheet()->getCell('N'.$i)->getValue());
		$fechaInsercion=addslashes($objPHPExcel->getActiveSheet()->getCell('O'.$i)->getValue());
		$repreLegal=addslashes($objPHPExcel->getActiveSheet()->getCell('P'.$i)->getValue());
		$dniRepre=addslashes($objPHPExcel->getActiveSheet()->getCell('Q'.$i)->getValue());
		$contacto=addslashes($objPHPExcel->getActiveSheet()->getCell('R'.$i)->getValue());
		$representacion=addslashes($objPHPExcel->getActiveSheet()->getCell('S'.$i)->getValue());
		
		$timestamp = PHPExcel_Shared_Date::ExcelToPHP($fechaInsercion);
		$fecha_php = date("Y-m-d",$timestamp);
		
		if($datos['tieneColaborador']=='SI'){
			$colaborador=$datos['colaborador'];
			$tieneColaborador='SI';
		}else{
			$colaborador='NULL';
			$tieneColaborador='NO';
		}
		
		if($empresa!=''){
			$consultaInsert.="(NULL, '$empresa','$cif','$direccion','$cp','$localidad','$provincia','$telefono','','$mail','','$contacto','',$colaborador,'',
			'EMPRESA','".$fecha_php."',NULL,'','".$datos['activo']."','$nss','$dniRepre','$representacion','NO','','NO','$repreLegal','$numCuenta','','','$tieneColaborador','$creditoFormativo',
			'$formaPago','','$bic','$referencia','NO','','','core','','','','Sin contactar','','NO','Datos','".$datos['codigoUsuario']."'),";
			/*$consulta=consultaBD("INSERT INTO clientes VALUES(NULL, '$empresa','$cif','$direccion','$cp','$localidad','$provincia','$telefono','','$mail','','$contacto','',$colaborador,'',
			'EMPRESA','".$fecha_php."',NULL,'','NO','$nss','$dniRepre','$representacion','NO','','NO','$repreLegal','$numCuenta','','','$tieneColaborador','$creditoFormativo',
			'$formaPago','','$bic','$referencia','NO','','','core','".$datos['codigoUsuario']."');");*/
			$referencia++;
		}
		
		$i++;
	}
	
	$consultaInsert=substr ($consultaInsert, 0, strlen($consultaInsert) - 1);
	$consulta=consultaBD($consultaInsert);
	echo mysql_error();
	
	cierraBD();
}

//PARTE NUEVA PAR PODER CREAR CURSO DIRECTO

function registraCursoNuevo($arrayAlumnos,$datos){
	$res=true;

	$codigoU=$_SESSION['codigoS'];
	
	$mediosPropios=compruebaExistencia($datos, 'mediosPropios');
	$mediosEntidad=compruebaExistencia($datos, 'mediosEntidad');
	$mediosCentro=compruebaExistencia($datos, 'mediosCentro');

	$lunes=compruebaExistencia($datos, 'lunes');
	$martes=compruebaExistencia($datos, 'martes');
	$miercoles=compruebaExistencia($datos, 'miercoles');
	$jueves=compruebaExistencia($datos, 'jueves');
	$viernes=compruebaExistencia($datos, 'viernes');
	$sabado=compruebaExistencia($datos, 'sabado');
	$domingo=compruebaExistencia($datos, 'domingo');
	
	$lunesFormacion=compruebaExistencia($datos, 'lunesFormacion');
	$martesFormacion=compruebaExistencia($datos, 'martesFormacion');
	$miercolesFormacion=compruebaExistencia($datos, 'miercolesFormacion');
	$juevesFormacion=compruebaExistencia($datos, 'juevesFormacion');
	$viernesFormacion=compruebaExistencia($datos, 'viernesFormacion');
	$sabadoFormacion=compruebaExistencia($datos, 'sabadoFormacion');
	$domingoFormacion=compruebaExistencia($datos, 'domingoFormacion');
	
	$lunesFormacionDistancia=compruebaExistencia($datos, 'lunesFormacionDistancia');
	$martesFormacionDistancia=compruebaExistencia($datos, 'martesFormacionDistancia');
	$miercolesFormacionDistancia=compruebaExistencia($datos, 'miercolesFormacionDistancia');
	$juevesFormacionDistancia=compruebaExistencia($datos, 'juevesFormacionDistancia');
	$viernesFormacionDistancia=compruebaExistencia($datos, 'viernesFormacionDistancia');
	$sabadoFormacionDistancia=compruebaExistencia($datos, 'sabadoFormacionDistancia');
	$domingoFormacionDistancia=compruebaExistencia($datos, 'domingoFormacionDistancia');
	

	if($datos['tipoFormacion']=='PRESENCIAL'||$datos['tipoFormacion']=='TELEFORMA'||$datos['tipoFormacion']=='WEBINAR'){
			$consulta=consultaBD("INSERT INTO cursos VALUES(NULL, '".$datos['accionFormativa']."', '".$datos['codigoInterno']."', '".$datos['tutor']."', '".$datos['fechaInicio']."',
		'".$datos['fechaFin']."', '$mediosPropios', '$mediosCentro', '$mediosEntidad', '".$datos['horaInicio']."', '".$datos['horaFin']."', '".$datos['horaInicioTarde']."', 
		'".$datos['horaFinTarde']."', '".$datos['horasTutoria']."', '".$lunes."', '".$martes."', '".$miercoles."', '".$jueves."', '".$viernes."', 
		'".$sabado."', '".$domingo."', '".$datos['cif']."', '".$datos['centro']."', '".$datos['tlf']."', '".$datos['domicilio']."', '".$datos['cp']."', 
		'".$datos['poblacion']."', '".$datos['titularidad']."', '".$datos['informarlt']."', '".$datos['informerlt']."', '".$datos['fechaDiscrepancia']."',
		'".$datos['resuelto']."', '".$datos['cifTutoria']."', '".$datos['centroTutoria']."', '".$datos['tlfTutoria']."', '".$datos['domicilioTutoria']."', '".$datos['cpTutoria']."',
		'".$datos['poblacionTutoria']."', '".$datos['horaInicioFormacion']."', '".$datos['horaFinFormacion']."', '".$datos['horaInicioFormacionTarde']."', '".$datos['horaFinFormacionTarde']."',
		'".$datos['horasFormacion']."', '$lunesFormacion', '$martesFormacion', '$miercolesFormacion', '$juevesFormacion', 
		'$viernesFormacion', '$sabadoFormacion', '$domingoFormacion', null, '', '', '', '', '', '', '', '', '', '', '', '', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 
		'".$datos['responsable']."', '".$datos['tlfResponsable']."','".$datos['llamadaBienvenida']."','".$datos['llamadaSeguimiento']."','".$datos['llamadaFinalizacion']."',
		'".$datos['bonificado']."','".$datos['plataforma']."', '".$datos['finalizado']."', '".$datos['medios']."', '".$datos['formacionCurso']."', '".$datos['comercial']."');");
	}elseif($datos['tipoFormacion']=='DISTANCIA'){
			$consulta=consultaBD("INSERT INTO cursos VALUES(NULL, '".$datos['accionFormativa']."', '".$datos['codigoInterno']."', '".$datos['tutor']."', '".$datos['fechaInicio']."',
		'".$datos['fechaFin']."','$mediosPropios', '$mediosCentro', '$mediosEntidad', '".$datos['horaInicio']."', '".$datos['horaFin']."', '".$datos['horaInicioTarde']."', 
		'".$datos['horaFinTarde']."', '".$datos['horasTutoria']."', '".$lunes."', '".$martes."', '".$miercoles."', '".$jueves."', '".$viernes."', 
		'".$sabado."', '".$domingo."', '', '', '', '', '', 
		'', '', '".$datos['informarlt']."', '".$datos['informerlt']."', '".$datos['fechaDiscrepancia']."', 
		'".$datos['resuelto']."', '".$datos['cifTutoria']."', '".$datos['centroTutoria']."', '".$datos['tlfTutoria']."', '".$datos['domicilioTutoria']."', '".$datos['cpTutoria']."',
		'".$datos['poblacionTutoria']."', '', '', '', '',
		'', 'NO', 'NO', 'NO', 'NO', 
		'NO', 'NO', 'NO', '".$datos['tutorDistancia']."', '".$datos['cifDistancia']."', '".$datos['centroGestorDistancia']."', 
		'".$datos['tlfDistancia']."', '".$datos['domicilioDistancia']."', '".$datos['cpDistancia']."', '".$datos['poblacionDistancia']."', '".$datos['titularidadDistancia']."',
		'".$datos['horaInicioFormacionDistancia']."', '".$datos['horaFinFormacionDistancia']."', '".$datos['horaInicioFormacionTardeDistancia']."', '".$datos['horaFinFormacionTardeDistancia']."',
		'".$datos['horasFormacionDistancia']."', '$lunesFormacionDistancia', '$martesFormacionDistancia', '$miercolesFormacionDistancia', '$juevesFormacionDistancia',
		'$viernesFormacionDistancia','$sabadoFormacionDistancia', '$domingoFormacionDistancia', '".$datos['responsable']."', '".$datos['tlfResponsable']."', '".$datos['llamadaBienvenida']."',
		'".$datos['llamadaSeguimiento']."','".$datos['llamadaFinalizacion']."','".$datos['bonificado']."', '".$datos['plataforma']."', '".$datos['finalizado']."', '".$datos['medios']."', '".$datos['formacionCurso']."', '".$datos['comercial']."');");
	}elseif($datos['tipoFormacion']=='MIXTA'){
			$consulta=consultaBD("INSERT INTO cursos VALUES(NULL, '".$datos['accionFormativa']."', '".$datos['codigoInterno']."', '".$datos['tutor']."', '".$datos['fechaInicio']."',
		'".$datos['fechaFin']."','$mediosPropios', '$mediosCentro', '$mediosEntidad', '".$datos['horaInicio']."', '".$datos['horaFin']."', '".$datos['horaInicioTarde']."', 
		'".$datos['horaFinTarde']."', '".$datos['horasTutoria']."', '".$lunes."', '".$martes."', '".$miercoles."', '".$jueves."', '".$viernes."', 
		'".$sabado."', '".$domingo."', '".$datos['cif']."', '".$datos['centro']."', '".$datos['tlf']."', '".$datos['domicilio']."', '".$datos['cp']."', 
		'".$datos['poblacion']."', '".$datos['titularidad']."', '".$datos['informarlt']."', '".$datos['informerlt']."', '".$datos['fechaDiscrepancia']."',
		'".$datos['resuelto']."', '".$datos['cifTutoria']."', '".$datos['centroTutoria']."', '".$datos['tlfTutoria']."', '".$datos['domicilioTutoria']."', '".$datos['cpTutoria']."',
		'".$datos['poblacionTutoria']."', '".$datos['horaInicioFormacion']."', '".$datos['horaFinFormacion']."', '".$datos['horaInicioFormacionTarde']."', '".$datos['horaFinFormacionTarde']."',
		'".$datos['horasFormacion']."', '$lunesFormacion', '$martesFormacion', '$miercolesFormacion', '$juevesFormacion', 
		'$viernesFormacion', '$sabadoFormacion', '$domingoFormacion', '".$datos['tutorDistancia']."', '".$datos['cifDistancia']."', '".$datos['centroGestorDistancia']."', 
		'".$datos['tlfDistancia']."', '".$datos['domicilioDistancia']."', '".$datos['cpDistancia']."', '".$datos['poblacionDistancia']."', '".$datos['titularidadDistancia']."',
		'".$datos['horaInicioFormacionDistancia']."', '".$datos['horaFinFormacionDistancia']."', '".$datos['horaInicioFormacionTardeDistancia']."', '".$datos['horaFinFormacionTardeDistancia']."',
		'".$datos['horasFormacionDistancia']."', '$lunesFormacionDistancia', '$martesFormacionDistancia', '$miercolesFormacionDistancia', '$juevesFormacionDistancia', '$viernesFormacionDistancia',
		'$sabadoFormacionDistancia', '$domingoFormacionDistancia', '".$datos['responsable']."', '".$datos['tlfResponsable']."','".$datos['llamadaBienvenida']."','".$datos['llamadaSeguimiento']."',
		'".$datos['llamadaFinalizacion']."','".$datos['bonificado']."', '".$datos['plataforma']."', '".$datos['finalizado']."', '".$datos['medios']."', '".$datos['formacionCurso']."', '".$datos['comercial']."');");
	}

	
	$codigoCurso=mysql_insert_id();
	
	if(!$consulta){
		$res=false;
		echo mysql_error();
	}
	else{
		foreach ($arrayAlumnos as $codigoAlumno){ 
	   		$consulta=consultaBD("INSERT INTO alumnos_registrados_cursos VALUES('$codigoCurso', '$codigoAlumno', 'NO','','', 'NO', 'NO','NO');");			
			$consulta=consultaBD("SELECT clientes.codigo FROM alumnos INNER JOIN ventas ON ventas.codigo=alumnos.codigoVenta INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo WHERE alumnos.codigo='$codigoAlumno';");
			$datosConsulta=mysql_fetch_assoc($consulta);
			if(isset($datosConsulta['codigo'])){
				$anterior=consultaBD("SELECT * FROM costes WHERE codigoAlumno='".$datosConsulta['codigo']."' AND codigoCurso='$codigoCurso';",false,true);
				if(!isset($anterior['codigo'])){
					$consulta=consultaBD("INSERT INTO accionformativa_cliente VALUES('".$datos['accionFormativa']."', '".$datosConsulta['codigo']."');");			
					$consulta=consultaBD("INSERT INTO costes VALUES(NULL, '".$datosConsulta['codigo']."', '$codigoCurso', '0','0','0','0','0');");
					$codigoCoste=mysql_insert_id();
					$consulta=consultaBD("INSERT INTO periodos_costes VALUES(NULL, '', '', '$codigoCoste');");
				}
			}
	   		if(!$consulta){
	   			$res=false;
	   		}
		}
	}
	
	/*$consulta2=consultaBD("SELECT clientes.empresa, clientes.codigo FROM ((clientes INNER JOIN ventas ON ventas.codigoCliente=clientes.codigo) 
	INNER JOIN alumnos ON alumnos.codigoVenta=ventas.codigo) 
	INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno 
	WHERE alumnos_registrados_cursos.codigoCurso=".$codigoCurso." GROUP BY clientes.codigo;");
	$datos2=mysql_fetch_assoc($consulta2);
	
	while($datos2!=false){
		
		$consulta3=consultaBD("INSERT INTO costes VALUES(null,".$datos2['codigo'].", $codigoCurso, 0, 0, 0);");
		
		$datos2=mysql_fetch_assoc($consulta2);
	}*/

	return $codigoCurso;
}

//FIN PARTE NUEVA

function cargaVariables(){
  if(isset($_POST['fechaUno'])){
	$_SESSION['fechaUno']=$_POST['fechaUno'];
	$_SESSION['fechaDos']=$_POST['fechaDos'];
	$_SESSION['devuelta']=$_POST['devuelta'];
	$_SESSION['pagada']=$_POST['pagada'];
	$_SESSION['servicio']=$_POST['servicio'];
  }
}


function campoDestinatarios($valor=false){

	$valoresCampos=array('todos');
	$textosCampos=array('Todos');

	$where=compruebaPerfilParaWhere('codigo');

	conexionBD();
    $consulta=consultaBD("SELECT codigo, CONCAT(apellidos,', ',nombre) AS texto FROM usuarios;");

    $i=0;
    while($datos=mysql_fetch_assoc($consulta)){
    	if($datos['codigo']!=$_SESSION['codigoS']){
    		array_push($valoresCampos,$datos['codigo']);
    		array_push($textosCampos,$datos['texto']);
    		$i++;
    	}
    }

	echo '<div id="cajaDestinatarios">';
    campoCheck('destinatarios','Destinatario/s', $textosCampos,$valoresCampos,true,$valor);
    echo '</div>';
    campoOculto($i,'numDestinatarios');

    cierraBD();

}

function creaConversacion(){
	$_POST['hora']=hora();
	$_POST['fecha']=fecha();

	$res=insertaDatos('conversaciones');
	if($res){
		$codigoC=$res;
		$datos=arrayFormulario();

		conexionBD();
		for($i=1;$i<=$datos['numDestinatarios'];$i++){//Empieza en 1 porque el 0 es "todos"
			if(isset($datos['destinatarios'.$i])){
				$res=$res && consultaBD("INSERT INTO destinatarios_conversacion VALUES(NULL,'$codigoC','".$datos['destinatarios'.$i]."');");
			}
		}
		cierraBD();

		$_POST['codigoConversacion']=$codigoC;
		$res=$res && insertaDatos('mensajes',time(),'documentos/comunicacionInterna');
	}

	return $res;
}

function imprimeConversaciones(){
	$codigoU=$_SESSION['codigoS'];

	conexionBD();
	$consulta=consultaBD("SELECT conversaciones.codigo AS codigo, fecha, hora, asunto, nombre, apellidos FROM (conversaciones INNER JOIN usuarios ON conversaciones.codigoUsuario=usuarios.codigo) INNER JOIN destinatarios_conversacion ON conversaciones.codigo=destinatarios_conversacion.codigoConversacion WHERE conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversacion.codigoUsuario='$codigoU' GROUP BY conversaciones.codigo;");
	while($datos=mysql_fetch_assoc($consulta)){
		$sinLeer=compruebaMensajesSinLeerConversacion($codigoU,$datos['codigo']);

		echo "
		<tr>
			<td> ".$datos['asunto']." $sinLeer</td>
        	<td> ".formateaFechaWeb($datos['fecha'])." </td>
        	<td> ".formateaHoraWeb($datos['hora'])." </td>
        	<td> ".$datos['apellidos'].", ".$datos['nombre']." </td>
        	<td class='centro'>
        		<a href='detallesConversacion.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver mensajes</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
	}
	cierraBD();
}

function compruebaMensajesSinLeerConversacion($codigoU,$codigoC){
	$sinLeer='';
	$numSinLeer=consultaBD("SELECT COUNT(conversaciones.codigo) AS sinLeer FROM (conversaciones INNER JOIN destinatarios_conversacion ON conversaciones.codigo=destinatarios_conversacion.codigoConversacion) INNER JOIN mensajes ON conversaciones.codigo=mensajes.codigoConversacion WHERE (conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversacion.codigoUsuario='$codigoU') AND leido='NO' AND conversaciones.codigo='$codigoC' AND mensajes.codigoUsuario!='$codigoU';",false,true);
	if($numSinLeer['sinLeer']>0){
		$sinLeer="<span class='badge'>".$numSinLeer['sinLeer']."</span>";
	}

	return $sinLeer;
}

function imprimeMensajesConversacion($codigoConversacion){
	$sinLeer=0;
	echo "<ul class='messages_layout'>";
	
	conexionBD();
	$consulta=consultaBD("SELECT mensajes.*, CONCAT(apellidos,', ',nombre) AS autor FROM mensajes INNER JOIN usuarios ON mensajes.codigoUsuario=usuarios.codigo WHERE codigoConversacion='$codigoConversacion' ORDER BY codigo;");
	while($datos=mysql_fetch_assoc($consulta)){
		$aviso='';
		$opciones='';
		$clase='from_user left';

		if($datos['codigoUsuario']==$_SESSION['codigoS']){
			$clase='by_myself right';
			$opciones="<div class='options_arrow'>
		                <div class='dropdown pull-right'> <a class='dropdown-toggle ' id='dLabel' role='button' data-toggle='dropdown' data-target='#' href='#'> <i class=' icon-caret-down'></i> </a>
		                  <ul class='dropdown-menu ' role='menu' aria-labelledby='dLabel'>
		                    <li><a href='detallesConversacion.php?codigo=".$codigoConversacion."&elimina=".$datos['codigo']."'><i class=' icon-trash icon-large'></i> Eliminar</a></li>
		                  </ul>
		                </div>
		              </div>";
		}
		if($datos['ficheroAdjunto']!='NO'){
			$datos['mensaje'].="<br /><br /><a class='btn btn-primary' href='documentos/comunicacionInterna/".$datos['ficheroAdjunto']."' target='_blank'><i class='icon-download'></i> Descargar Adjunto</a>";
		}
		if($datos['leido']=='NO' && $datos['codigoUsuario']!=$_SESSION['codigoS']){
			$aviso="<span class='label label-danger'><i class='icon-flag'></i></span>";
			consultaBD("UPDATE mensajes SET leido='SI' WHERE codigo='".$datos['codigo']."';");
			$sinLeer++;
		}

		echo "
			<li class='sinFlotar ".$clase."'> <a class='avatar'><img src='img/logo3.png'/></a>
	          <div class='message_wrap'> <span class='arrow'></span>
	            <div class='info'> <a class='name'>".$datos['autor']." |</a> <span class='time'>El ".formateaFechaWeb($datos['fecha'])." a las ".formateaHoraWeb($datos['hora'])." ".$aviso."</span>
	              ".$opciones."
	            </div>
	            <div class='text'> ".$datos['mensaje']." </div>
	          </div>
	        </li>";
    }
    cierraBD();
    $_SESSION['mensajesSinLeer']=$_SESSION['mensajesSinLeer']-$sinLeer;

	echo "</ul>";
}


function creaMensajeConversacion($codigoC){
	$_POST['hora']=hora();
	$_POST['fecha']=fecha();
	$_POST['codigoConversacion']=$codigoC;
	$_POST['codigoUsuario']=$_SESSION['codigoS'];
	$_POST['leido']='NO';

	return insertaDatos('mensajes',time(),'documentos/comunicacionInterna');
}

function eliminaMensajeConversacion($codigoMensaje){
	return consultaBD("DELETE FROM mensajes WHERE codigo='$codigoMensaje';",true);
}

function creaEstadisticasComunicacionInterna(){
	$codigoU=$_SESSION['codigoS'];
	return consultaBD("SELECT COUNT(DISTINCT conversaciones.codigo) AS total FROM conversaciones INNER JOIN destinatarios_conversacion ON conversaciones.codigo=destinatarios_conversacion.codigoConversacion WHERE conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversacion.codigoUsuario='$codigoU';",true,true);
}


function compruebaMensajesPorLeer(){
	$inicio=false;
	date_default_timezone_set('Europe/Madrid');//Necesario para que el servidor no dé error.
	
	if(isset($_SESSION['ultimaComprobacion'])){//Si ya se ha comprobado anteriormente
		$ultimaComprobacion=$_SESSION['ultimaComprobacion'];
		$sinLeer=$_SESSION['mensajesSinLeer'];
	}
	else{//Si es la primera vez que se comprueba
		$ultimaComprobacion=new DateTime();
		$sinLeer=0;
		$inicio=true;
	}

	$ahora=new DateTime();

	$diferencia=date_diff($ultimaComprobacion,$ahora);
	$diferencia=date_interval_format($diferencia,'%i');//Transformación del intervalo en minutos

	if($diferencia>5 || $inicio){//Comprueba que haya mensajes nuevos cada 5 minutos
		$codigoU=$_SESSION['codigoS'];
		//La siguiente consulta contabiliza todos los mensajes no leídos de las conversaciones a los que el usuario está asociado, sin tener en cuenta sus propios mensajes
		$datos=consultaBD("SELECT COUNT(conversaciones.codigo) AS sinLeer FROM (conversaciones INNER JOIN destinatarios_conversacion ON conversaciones.codigo=destinatarios_conversacion.codigoConversacion) INNER JOIN mensajes ON conversaciones.codigo=mensajes.codigoConversacion WHERE (conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversacion.codigoUsuario='$codigoU') AND leido='NO' AND mensajes.codigoUsuario!='$codigoU';",true,true);
		$sinLeer=$datos['sinLeer'];
	}

	$_SESSION['ultimaComprobacion']=$ahora;//Actualizo la variable temporal de referencia
	$_SESSION['mensajesSinLeer']=$sinLeer;//Actualizo la variable de mensajes nuevos

	echo $sinLeer;
}

function campoColor($nombreCampo,$texto,$valor='',$clase='input-large',$disabled=false){
	$des='';
	if($disabled){
		$des='disabled="disabled"';
	}
	$valor=compruebaValorCampo($valor,$nombreCampo);

	echo "
	<div class='control-group'>                     
      <label class='control-label' for='$nombreCampo'>$texto:</label>
      <div class='controls'>
        <input type='text' value='$valor' id='$nombreCampo' name='$nombreCampo' class='form-control $clase'/>
      </div> <!-- /controls -->     
    </div> <!-- /control-group -->";
}

function eliminaDocumento($tabla, $codigo){
	$res=true;

	conexionBD();

	$consulta=consultaBD("UPDATE $tabla SET papelera='SI' WHERE codigo='$codigo';");
	cierraBD();	

	if(!$consulta){
		$res=false;
	}

	return $res;
}

function eliminaDocumentoDefinitivo($tabla, $codigo){
	$res=true;

	conexionBD();

	$consulta=consultaBD("DELETE FROM $tabla WHERE codigo='$codigo';");
	cierraBD();	

	if(!$consulta){
		$res=false;
	}

	return $res;
}

function imprimeDocumentosInternos($anio,$papelera='NO'){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM documentosInternos WHERE fecha LIKE'%$anio-%' AND papelera='$papelera' ORDER BY codigo DESC;");
	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		$dir='documentos';
		if(!file_exists('documentos/internos/'.$datos['ficheroInterno']))
		{
			$dir='documentos2';
		}
		$fecha=formateaFechaWeb($datos['fecha']);
		$revision=formateaFechaWeb($datos['fechaRevision']);
		echo "
		<tr>
        	<td> ".$datos['codigoDocumento']." </td>
        	<td> ".$datos['nombreDocumento']." </td>
        	<td> $revision </td>
        	<td> $fecha </td>
        	<td class='centro'>
        		<a href='".$dir."/internos/".$datos['ficheroInterno']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>";
				if($papelera=='NO' && ($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19' || $_SESSION['tipoUsuario']=='MARKETING')){ 
					echo "<a href='documentacion.php?codigo=".$datos['codigo']."&eliminarInterno' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>";
				}	
				if($papelera=='SI' && ($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19')){
					echo "<a href='papelera.php?codigo=".$datos['codigo']."&eliminarInterno' class='btn btn-danger'><i class='icon-trash'></i> Eliminar definitivamente</i></a>";
				}
        	echo "</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function imprimeDocumentosExternos($anio,$papelera='NO'){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM documentosExternos WHERE fechaSubida LIKE'%$anio-%' AND papelera='$papelera' ORDER BY codigo DESC;");
	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		$dir='documentos';
		if(!file_exists('documentos/externos/'.$datos['ficheroInterno']))
		{
			$dir='documentos2';
		}
		echo "
		<tr>
        	<td> ".$datos['codigoDocumento']." </td>
        	<td> ".$datos['nombreDocumento']." </td>
        	<td> ".$datos['ubicacion']." </td>
			<td> ".formateaFechaWeb($datos['fechaSubida'])." </td>
        	<td class='centro'>
        		<a href='".$dir."/externos/".$datos['ficheroExterno']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>";
				if($papelera=='NO' && ($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19' || $_SESSION['tipoUsuario']=='MARKETING')){ 
					echo "<a href='documentacion.php?codigo=".$datos['codigo']."&eliminarExterno' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>";
				}	
				if($papelera=='SI' && ($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19')){
					echo "<a href='papelera.php?codigo=".$datos['codigo']."&eliminarExterno' class='btn btn-danger'><i class='icon-trash'></i> Eliminar definitivamente</i></a>";
				}
        	echo "</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function imprimeDocumentosRegistros($anio,$papelera='NO'){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM documentosRegistros WHERE fechaSubida LIKE'%$anio-%' AND papelera='$papelera' ORDER BY codigo DESC;");
	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		$dir='documentos';
		if(!file_exists('documentos/registros/'.$datos['ficheroInterno']))
		{
			$dir='documentos2';
		}
		echo "
		<tr>
        	<td> ".$datos['codigoDocumento']." </td>
        	<td> ".$datos['nombreDocumento']." </td>
        	<td> ".$datos['responsable']." </td>
        	<td> ".$datos['ubicacion']." </td>
			<td> ".formateaFechaWeb($datos['fechaSubida'])." </td>
        	<td> ".$datos['tiempoConservacion']." </td>
        	<td class='centro'>
        		<a href='".$dir."/registros/".$datos['ficheroRegistro']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>";
				if($papelera=='NO' && ($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19' || $_SESSION['tipoUsuario']=='MARKETING')){ 
					echo "<a href='documentacion.php?codigo=".$datos['codigo']."&eliminarRegistro' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>";
				}	
				if($papelera=='SI' && ($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19')){
					echo "<a href='papelera.php?codigo=".$datos['codigo']."&eliminarRegistro' class='btn btn-danger'><i class='icon-trash'></i> Eliminar definitivamente</i></a>";
				}
        	echo "</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function imprimeDocumentosColaboradores($anio,$papelera='NO'){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM documentosDistribucion WHERE fechaSubida LIKE'%$anio-%' AND papelera='$papelera' ORDER BY codigo DESC;");
	cierraBD();

	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		$dir='documentos';
		if(!file_exists('documentos/distribucion/'.$datos['ficheroInterno']))
		{
			$dir='documentos2';
		}
		echo file_exists('documentos/distribucion/'.$datos['ficheroInterno']);
		echo "
		<tr>
        	<td> ".$datos['codigoDocumento']." </td>
        	<td> ".$datos['nombreDocumento']." </td>
        	<td> ".$datos['responsable']." </td>
        	<td> ".$datos['ubicacion']." </td>
			<td> ".formateaFechaWeb($datos['fechaSubida'])." </td>
        	<td> ".$datos['tiempoConservacion']." </td>
        	<td class='centro'>
        		<a href='".$dir."/distribucion/".$datos['ficheroDistribucion']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>";
				if($papelera=='NO' && ($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19' || $_SESSION['tipoUsuario']=='MARKETING')){ 
					echo "<a href='documentacion.php?codigo=".$datos['codigo']."&eliminarRegistro' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>";
				}	
				if($papelera=='SI' && ($_SESSION['codigoS']=='21' || $_SESSION['codigoS']=='19')){
					echo "<a href='papelera.php?codigo=".$datos['codigo']."&eliminarRegistro' class='btn btn-danger'><i class='icon-trash'></i> Eliminar definitivamente</i></a>";
				}
        	echo "</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function compruebaPerfilParaWhereHaving($campo=false){
	$where='';
	if($_SESSION['tipoUsuario']=='ADMIN'||$_SESSION['tipoUsuario']=='ADMINISTRACION'||$_SESSION['tipoUsuario']=='ATENCION'||$_SESSION['codigoS']=='57'){
		$where="HAVING 1=1";//Condición irrelevante, pero necesaria para encajar bien la función en las consultas que tengan más de una condición
	}
	else{
		$codigoU=$_SESSION['codigoS'];
		if($campo!=false){
			$where="HAVING ($campo='$codigoU' OR $campo IN (SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU'))";
		}
		else{
			$where="HAVING (codigoUsuario='$codigoU' OR codigoUsuario IN (SELECT codigo FROM usuarios WHERE directorAsociado =  '$codigoU'))";
		}
	}
	return $where;
}

function generaFactura($codigo){
	//$_GET['codigo'] viene en la URL a través de un botón "Descargar Contrato"
	$datos=datosRegistro('facturacion',$codigo);
	$datosCliente=datosRegistro('clientes',$datos['codigoCliente']);
	$datosFirma=datosRegistro('firmas',$datos['firma']);
	
	//Carga de la librería PHPWord
	require_once 'phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$plantilla='';
	if($datos['concepto']=='14'){
		$plantilla='plantillaFacturaCursos.docx';
	}else{
		if($datos['tipoFactura']=='mantenimiento'){
			$plantilla='plantillaFacturaLopd3.docx';
		}elseif($datos['tipoFactura']=='auditoria'){
			$plantilla='plantillaFacturaLopd.docx';
		}elseif($datos['tipoFactura']=='consultoria'){
			$plantilla='plantillaFacturaLopd2.docx';
		}elseif($datos['tipoFactura']=='lssi'){
			$plantilla='plantillaFacturaBlanqueo.docx';
		}elseif($datos['tipoFactura']=='prl'){
			$plantilla='plantillaFacturaPRL.docx';
		}elseif($datos['tipoFactura']=='alergenos'){
			$plantilla='plantillaFacturaAlergenos.docx';
		}
	}
	if($plantilla!=''){
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('documentos/'.$plantilla);

	/*
	Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
	En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
	*/
	
	if($datos['tipoFactura']=='lssi'){																//GENERACIÓN DEL LSSI//
	
		$datosCliente['empresa']=str_replace('&','&amp;',$datosCliente['empresa']);
		$formaPago=array('transferencia'=>'Transferencia','efectivo'=>'Efectivo','cheque'=>'Cheque','domiciliacion'=>'Domiciliación','tarjeta'=>'Tarjeta');
		$document->setValue("fecha",utf8_decode(formateaFechaWeb($datos['fechaEmision'])));
		$document->setValue("vencimiento",utf8_decode(formateaFechaWeb($datos['fechaVencimiento'])));
		$document->setValue("numFactura",utf8_decode($datos['referencia']));
		$document->setValue("anio",date('Y'));
		$document->setValue("nombreCliente",utf8_decode($datosCliente['empresa']));
		$document->setValue("cif",utf8_decode($datosCliente['cif']));
		$document->setValue("dir",utf8_decode($datosCliente['direccion']));
		$document->setValue("cp",utf8_decode($datosCliente['cp']));
		$document->setValue("localidad",utf8_decode($datosCliente['localidad']));
		$document->setValue("provincia",utf8_decode($datosCliente['provincia']));
		$document->setValue("formaPago",utf8_decode($formaPago[$datos['formaPago']]));
		$document->setValue("numCuenta",utf8_decode(substr($datosCliente['numCuenta'],0,4).'-'.substr($datosCliente['numCuenta'],4,4).'-'.substr($datosCliente['numCuenta'],8,2).'-'.substr($datosCliente['numCuenta'],10,6).'-'.substr($datosCliente['numCuenta'],-8,-4).'-****'));
		$document->setValue("firma",utf8_decode($datosFirma['firma']));
		$document->setValue("cifFirma",utf8_decode($datosFirma['cif']));
		$document->setValue("subtotal",utf8_decode(number_format((float)$datos['coste'], 2, ',', '')));
		$iva=0.21*$datos['coste'];
		$total=$datos['coste']+$iva;
		$document->setValue("iva",utf8_decode(number_format((float)$iva, 2, ',', '')));
		$document->setValue("total",utf8_decode(number_format((float)$total, 2, ',', '')));
		
	}else{																							//GENERACIÓN DEL RESTO DE FACTURAS//
	
		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$modalidades=array('PRESENCIAL'=>'Presencial','DISTANCIA'=>'A distancia','MIXTA'=>'Mixta','TELE'=>'Teleformación','WEBINAR'=>'Webinar');
		$datosCliente['empresa']=str_replace('&','&amp;',$datosCliente['empresa']);
		
		$banco=array('ABNAESMMXXX'=>'THE ROYAL BANK OF SCOTLAND PLC, SUCURSAL EN ESPAÑA','AHCFESMMXXX'=>'AHORRO CORPORACION FINANCIERA, S.A., SOCIEDAD DE VALORES','ALCLESMMXXX'=>'BANCO ALCALA, S.A.','AREBESMMXXX'=>'ARESBANK, S.A.',
		'BAPUES22XXX'=>'BANCA PUEYO, S.A.','BASKES2BXXX'=>'KUTXABANK, S.A.','BBPIESMMXXX'=>'BANCO BPI, S.A., SUCURSAL EN ESPAÑA','BBRUESMXXXX'=>'ING BELGIUM, S.A., SUCURSAL EN ESPAÑA','BBVAESMMXXX'=>'BANCO BILBAO VIZCAYA ARGENTARIA, S.A.',
		'BCCAESMMXXX'=>'BANCO DE CRÉDITO SOCIAL COOPERATIVO S.A.','BCOEESMM081'=>'CAJA RURAL DE CASTILLA-LA MANCHA, S.C.C.','BCOEESMMXXX'=>'BANCO COOPERATIVO ESPAÑOL, S.A.','BESMESMMXXX'=>'NOVO BANCO, S.A., SUCURSAL EN ESPAÑA',
		'BFIVESBBXXX'=>'BANCO MEDIOLANUM, S.A.','BKBKESMMXXX'=>'BANKINTER, S.A.','BKOAES22XXX'=>'BANKOA, S.A.','BMARES2MXXX'=>'BANCA MARCH, S.A.','BMCEESMMXXX'=>'BANQUE MAROCAINE COMMERCE EXTERIEUR INTERNATIONAL, S.A.',
		'BOTKESMXXXX'=>'THE BANK OF TOKYO-MITSUBISHI UFJ, LTD, SUCURSAL EN ESPAÑA','BRASESMMXXX'=>'BANCO DO BRASIL AG, SUCURSAL EN ESPAÑA','BSABESBBXXX'=>'BANCO DE SABADELL, S.A.',
		'BSCHESMMXXX'=>'BANCO SANTANDER, S.A.','BSUIESMMXXX'=>'CREDIT AGRICOLE CORPORATE AND INVESTMENT BANK, SUCURSAL EN ESPAÑA','BVALESMMXXX'=>'RBC INVESTOR SERVICES ESPAÑA, S.A.',
		'CAGLESMMVIG'=>'ABANCA CORPORACIÓN BANCARIA, S.A.','CAHMESMMXXX'=>'BANKIA, S.A.','CAIXESBBXXX'=>'CAIXABANK, S.A.','CAPIESMMXXX'=>'CM CAPITAL MARKETS BOLSA, SOCIEDAD DE VALORES, S.A.','CASDESBBXXX'=>'CAJA DE ARQUITECTOS, S.C.C.',
		'CAZRES2ZXXX'=>'IBERCAJA BANCO, S.A.','CCOCESMMXXX'=>'BANCO CAMINOS, S.A.','CCRIES2AXXX'=>'CAJAS RURALES UNIDAS, S.C.C.','CDENESBBXXX'=>'CAIXA DE CREDIT DELS ENGINYERS - CAJA DE CREDITO DE LOS INGENIEROS, S.C.C.',
		'CECAESMM045'=>'CAJA DE AHORROS Y M.P. DE ONTINYENT','CECAESMM048'=>'LIBERBANK, S.A.','CECAESMM056'=>'COLONYA - CAIXA DESTALVIS DE POLLENSA','CECAESMM105'=>'BANCO DE CASTILLA-LA MANCHA, S.A. /CAJA DE AHORROS DE CASTILLA-LA MANCHA',
		'CECAESMMXXX'=>'CECABANK, S.A.','CESCESBBXXX'=>'CATALUNYA BANC, S.A.','CGDIESMMXXX'=>'BANCO CAIXA GERAL, S.A.','CITIES2XXXX'=>'CITIBANK ESPAÑA, S.A.','CLPEES2MXXX'=>'CAJA LABORAL POPULAR, C.C.','CRESESMMXXX'=>'CREDIT SUISSE AG, SUCURSAL EN ESPAÑA',
		'CSPAES2L108'=>'BANCO DE CAJA ESPAÑA DE INVERSIONES, SALAMANCA Y SORIA, S.A.','CSURES2CXXX'=>'CAJASUR BANCO, S.A.','DSBLESMMXXX'=>'DEXIA SABADELL, S.A.','ESPBESMMXXX'=>'BANCO DE ESPAÑA','ESSIESMMXXX'=>'BANCO ESPIRITO SANTO DE INVESTIMENTO, S.A., SUCURSAL EN ESPAÑA',
		'EVOBESMMXXX'=>'EVO BANCO S.A.U.','FIOFESM1XXX'=>'BANCO FINANTIA SOFINLOC, S.A.','GBMNESMMXXX'=>'BANCO MARE NOSTRUM, S.A.','GVCBESBBETB'=>'GVC GAESCO VALORES, SOCIEDAD DE VALORES, S.A.',
		'IBRCESMMXXX'=>'SOCIEDAD DE GESTION DE LOS SISTEMAS DE REGISTRO, COMPENSACION Y LIQUIDACION DE VALORES, S.A.U.','ICROESMMXXX'=>'INSTITUTO DE CREDITO OFICIAL',
		'INGDESMMXXX'=>'ING BANK, N.V., SUCURSAL EN ESPAÑA','INSGESMMXXX'=>'INVERSEGUROS, SOCIEDAD DE VALORES, S.A.','INVLESMMXXX'=>'BANCO INVERSIS, S.A.','IPAYESMMXXX'=>'SOCIEDAD ESPAÑOLA DE SISTEMAS DE PAGO, S.A.',
		'IVALESMMXXX'=>'INTERMONEY VALORES, SOCIEDAD DE VALORES, S.A.','LISEESMMXXX'=>'LINK SECURITIES, SOCIEDAD DE VALORES, S.A.','MADRESMMXXX'=>'BANCO DE MADRID, S.A.','MEFFESBBXXX'=>'BME CLEARING, S.A.',
		'MISVESMMXXX'=>'MAPFRE INVERSION, SOCIEDAD DE VALORES, S.A.','MLCEESMMXXX'=>'MERRILL LYNCH CAPITAL MARKETS ESPAÑA, S.A., SOCIEDAD DE VALORES','NACNESMMXXX'=>'BANCO DE LA NACION ARGENTINA, SUCURSAL EN ESPAÑA',
		'NATXESMMXXX'=>'NATIXIS, S.A., SUCURSAL EN ESPAÑA','POHIESMMXXX'=>'TARGOBANK, S.A.','POPIESMMXXX'=>'POPULAR BANCA PRIVADA, S.A.','POPLESMMXXX'=>'BANCOPOPULAR-E, S.A.','POPUESMMXXX'=>'BANCO POPULAR ESPAÑOL, S.A.',
		'PRABESMMXXX'=>'COOPERATIEVE CENTRALE RAIFFEISEN- BOERENLEENBANK B.A. (RABOBANK NEDERLAND), SUCURSAL EN ESPAÑA','PROAESMMXXX'=>'EBN BANCO DE NEGOCIOS, S.A.',
		'PSTRESMMXXX'=>'BANCO PASTOR, S.A.','RENBESMMXXX'=>'RENTA 4 BANCO, S.A.','RENTESMMXXX'=>'RENTA 4 SOCIEDAD DE VALORES, S.A.','UBIBESMMXXX'=>'UBI BANCA INTERNATIONAL, S.A., SUCURSAL EN ESPAÑA','UCJAES2MXXX'=>'UNICAJA BANCO, S.A.',
		'XBCNESBBXXX'=>'SOCIEDAD RECTORA BOLSA VALORES DE BARCELONA, S.A., S.R.B.V.','XRBVES2BXXX'=>'SOCIEDAD RECTORA BOLSA DE VALORES DE BILBAO, S.A., S.R.B.V.','XRVVESVVXXX'=>'SOCIEDAD RECTORA BOLSA VALORES DE VALENCIA, S.A., S.R.B.V.','XXXXESBBXXX'=>'');
		
		if($datos['formaPago']!='transferencia'){
			$document->setValue("banco",utf8_decode($banco[$datosCliente['bic']]));
			$document->setValue("cuenta",utf8_decode(substr($datosCliente['numCuenta'],0,4).'-****-**-******-'.substr($datosCliente['numCuenta'],-8,-4).'-'.substr($datosCliente['numCuenta'],-4)));
		}else{
			$document->setValue("banco",'BBVA');
			$document->setValue("cuenta",'ES76. 0182.4853.54.0201560273');
		}
		if($datos['concepto']=='14'){    //PLANTILLA DE CURSOS
		
			$datosCurso=datosRegistro('cursos',$datos['codigoCurso']);
			$datosAccion=datosRegistro('accionFormativa',$datosCurso['codigoaccionFormativa'],'codigoInterno');
			
			$consulta=consultaBD("SELECT COUNT(codigoAlumno) AS total FROM alumnos_registrados_cursos WHERE codigoCurso='".$datos['codigoCurso']."';",true);
			$total=mysql_fetch_assoc($consulta);
			
			if(strlen($datosAccion['codigoInterno'])<3){
				while(strlen($datosAccion['codigoInterno'])<3){
					$datosAccion['codigoInterno']='0'.$datosAccion['codigoInterno'];
				}
			}
			if(strlen($datosCurso['codigoInterno'])<2){
				while(strlen($datosCurso['codigoInterno'])<2){
					$datosCurso['codigoInterno']='0'.$datosCurso['codigoInterno'];
				}
			}
			$document->setValue("grupo",utf8_decode($datosCurso['codigoInterno']));
			
			$document->setValue("grupo",utf8_decode($datosAccion['codigoInterno'].'-'.$datosCurso['codigoInterno']));
			$document->setValue("accion",utf8_decode($datosAccion['codigoInterno']));
			$document->setValue("curso",utf8_decode($datosAccion['denominacion']));
			$document->setValue("modalidad",utf8_decode($modalidades[$datosAccion['modalidad']]));
			$document->setValue("empresa",utf8_decode($datosCliente['empresa']));
			$document->setValue("horas",utf8_decode($datosAccion['horas']));
			$document->setValue("numAlumnos",utf8_decode($total['total']));
			$document->setValue("fechaInicio",utf8_decode(formateaFechaWeb($datosCurso['fechaInicio'])));
			$document->setValue("fechaFin",utf8_decode(formateaFechaWeb($datosCurso['fechaFin'])));
			
		}
		
			$document->setValue("firma",utf8_decode($datosFirma['firma']));
			$document->setValue("cifFirma",utf8_decode($datosFirma['cif']));
			$document->setValue("fecha",utf8_decode($fecha));
			if($datos['concepto']=='14' && $datosFirma['codigo']=='3'){
				$letra='S/';
			}elseif($datos['concepto']=='14' && $datosFirma['codigo']!='3'){
				$letra='F/';
			}else{
				$letra='';
			}
			$document->setValue("referencia",utf8_decode($letra.$datos['referencia']));
			$document->setValue("fechaVencimiento",utf8_decode(formateaFechaWeb($datos['fechaVencimiento'])));
			$document->setValue("cliente",utf8_decode($datosCliente['empresa']));
			$document->setValue("direccion",utf8_decode($datosCliente['direccion']));
			$document->setValue("cp",utf8_decode($datosCliente['cp']));
			$document->setValue("localidad",utf8_decode($datosCliente['localidad']));
			$document->setValue("provincia",utf8_decode($datosCliente['provincia']));
			$document->setValue("telefono",utf8_decode($datosCliente['telefono']));
			$document->setValue("cif",utf8_decode($datosCliente['cif']));
			$document->setValue("idCliente",utf8_decode($datosCliente['referencia']));
			$document->setValue("coste",utf8_decode(number_format((float)$datos['coste'], 2, ',', '')));
			$document->setValue("subtotal",utf8_decode(number_format((float)$datos['coste'], 2, ',', '').' &#8364;'));
			
			$iva=0.21*$datos['coste'];
			$total=$datos['coste']+$iva;
			
			
			if($datos['concepto']!='14'){
				$document->setValue("total",utf8_decode(number_format((float)$total, 2, ',', '')).' &#8364;');
				$document->setValue("totalIva",utf8_decode(number_format((float)$iva, 2, ',', '')).' &#8364;');
			}else{
				$document->setValue("gastosOrga",utf8_decode(number_format((float)$datos['gastosOrganizacion'], 2, ',', '')).' &#8364;');
				$document->setValue("gastosImpar",utf8_decode(number_format((float)$datos['gastosImparticion'], 2, ',', '')).' &#8364;');
				if($datosFirma['codigo']!=3){
					$document->setValue("total",utf8_decode(number_format((float)$total, 2, ',', '')).' &#8364;');
					$document->setValue("totalIva",utf8_decode(number_format((float)$iva, 2, ',', '')).' &#8364;');
					$document->setValue("iva",utf8_decode('21%'));
				}else{
					$document->setValue("total",utf8_decode(number_format((float)$datos['coste'], 2, ',', '').' &#8364;'));
					$document->setValue("totalIva",'');
					$document->setValue("iva",'');
				}
			}
	}

	/*
	Cuando ya has sustituido todas las etiquetas por su valores, el método save
	guarda el fichero resultane en la ruta que le indiques con el nombre que le
	indiques.
	*/
	$tiempo=time();
	$document->save('documentos/Factura-'.$tiempo.'.docx');
	return 'documentos/Factura-'.$tiempo.'.docx';
	} else {
		return '';
	}
}

function generaContrato($codigoCliente){
	$datos=datosRegistro('clientes',$codigoCliente);
	$datosUsuario=datosRegistro('usuarios',$datos['codigoUsuario']);
	
	//Carga de la librería PHPWord
	require_once 'phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('documentos/plantillaContrato.docx');

	/*
	Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
	En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
	*/
	
	
	// PARTE CLIENTE
	$document->setValue("representante",utf8_decode($datos['repreLegal']));
	$document->setValue("dni",utf8_decode($datos['dniRepresentante']));
	$document->setValue("empresa",utf8_decode($datos['empresa']));
	$document->setValue("nif",utf8_decode($datos['cif']));
	$document->setValue("domicilio",utf8_decode($datos['direccion']));
	$document->setValue("poblacion",utf8_decode($datos['localidad']));
	$document->setValue("provincia",utf8_decode($datos['provincia']));
	$document->setValue("cp",utf8_decode($datos['cp']));
	$document->setValue("telefono",utf8_decode($datos['telefono']));
	$document->setValue("mail",utf8_decode($datos['mail']));
	$document->setValue("contacto",utf8_decode($datos['contacto']));
	$document->setValue("numCuenta",utf8_decode($datos['numCuenta']));
	/****************************************/
	
	// PARTE USUARIO
	$document->setValue("usuario",utf8_decode($datosUsuario['nombre'].' '.$datosUsuario['apellidos']));
	$document->setValue("dniUsuario",utf8_decode($datosUsuario['dni']));
	/****************************************/
	
	// FECHA
	$document->setValue("dia",utf8_decode(date('d')));
	$document->setValue("mes",utf8_decode(devuelveMes(date('m'))));
	$document->setValue("anio",utf8_decode(date('Y')));
	/****************************************/

	/*
	Cuando ya has sustituido todas las etiquetas por su valores, el método save
	guarda el fichero resultane en la ruta que le indiques con el nombre que le
	indiques.
	*/
	$tiempo=time();
	$document->save('documentos/Contrato-'.$tiempo.'.docx');
	
	return 'documentos/Contrato-'.$tiempo.'.docx';
}

function enviaFacturas(){	
	/*$res=true;
	$hoy=date('Y-m-d');
	$nuevafecha = strtotime ( '-6 day' , strtotime ( $hoy ) ) ;
	$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
	conexionBD();
	$consulta=consultaBD("SELECT * FROM facturacion WHERE insercion LIKE '$nuevafecha%';");
	cierraBD();
	while($datosFacturas=mysql_fetch_assoc($consulta)){
		$aleatorio=md5(time());
		$limiteMime="==TecniBoundary_x{$aleatorio}x";
		$headers="From: atencion.cliente@grupqualia.es\r\n";
		$headers.= "MIME-Version: 1.0\r\n";
		$headers.="Content-Type: multipart/mixed;" . "boundary=\"{$limiteMime}\"";
		
		$mensajeBienvenida='';
		switch($datosFacturas['concepto']){
			case '14':
				$mensajeBienvenida="Apreciado cliente, <br><br>

				En nombre de <strong>GRUPO QUALIA</strong> agradecemos la confianza depositada en nuestra empresa para gestionar la formación continua de su empresa.  <br><br>

				Tal y como hemos acordado , adjuntamos el contrato firmado y la factura por los servicios a recibir. <br><br>
				 
				Le recordamos que el documento para descontar la bonificación <strong>no es la Factura</strong>, recibirán un email con el Informe de Bonificacion adjunto, una vez llegada la fecha de finalización, que podrán reenviar a su gestor. <br><br>
				 
				Reciba un cordial saludo, <br><br>
				";
			break;
			case '22':
				$mensajeBienvenida="Apreciado cliente,  <br><br>

				En nombre de <strong>GRUPO QUALIA</strong> agraecemos la confianza depositada en nuestra empresa para su asesoramiento en los aspectos legales contratados.  <br><br>

				Tal y como hemos acordado , adjuntamos el contrato firmado y la factura por los servicios a recibir. <br><br>
				 
				Reciba un cordial saludo, <br><br>";
			break;
			case 'blanqueo':
				$mensajeBienvenida="Apreciado cliente,  <br><br>

				En nombre de <strong>GRUPO QUALIA</strong> agraecemos la confianza depositada en nuestra empresa para su asesoramiento en los aspectos legales contratados.  <br><br>

				Tal y como hemos acordado , adjuntamos el contrato firmado y la factura por los servicios a recibir. <br><br>
				 
				Reciba un cordial saludo, <br><br>";
			break;
			case '20': 
				$mensajeBienvenida="Apreciado cliente, <br><br>

				En nombre de <strong>GRUPO QUALIA</strong> agraecemos la confianza depositada en nuestra empresa para su asesoramiento en los aspectos legales contratados. <br><br>

				Tal y como hemos acordado , adjuntamos el contrato firmado y la factura por los servicios a recibir. <br><br>
				 
				Reciba un cordial saludo, <br><br>";
			break;
			case '19': 
				$mensajeBienvenida="Apreciado cliente, <br><br>

				En nombre de <strong>GRUPO QUALIA</strong> agraecemos la confianza depositada en nuestra empresa para su asesoramiento en los aspectos legales contratados. <br><br>

				Tal y como hemos acordado , adjuntamos el contrato firmado y la factura por los servicios a recibir. <br><br>
				 
				Reciba un cordial saludo, <br><br>";
			break;
		}

		$mensaje="--{$limiteMime}\r\n"."Content-Type: text/html; charset=\"utf-8\"\r\n"."Content-Transfer-Encoding: 7bit\n\n$mensajeBienvenida<img src='http://www.crmparapymes.com.es/grupqualia/img/imagenCorreos.png'></img>\r\n\r\n
				<div style='font:12px Sans-Serif; color: #616161;'><strong>AVISO DE CONFIDENCIALIDAD:</strong> Este mensaje y sus archivos van dirigidos exclusivamente a su destinatario, pudiendo contener información confidencial sometida a secreto profesional. No está permitida su reproducción o distribución sin la autorización expresa de <strong>GRUPO QUALIA</strong>. Si usted no es el destinatario final por favor elimínelo e infórmenos por esta vía.<br>
				De acuerdo con lo establecido por la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (LOPD), le informamos que sus datos están incorporados en un fichero del que es titular <strong>GRUPO QUALIA</strong> con la finalidad de realizar la gestión administrativa, contable y fiscal, así como enviarle comunicaciones comerciales sobre nuestros productos y/o servicios.<br>
				Asimismo, le informamos de la posibilidad de ejercer los derechos de acceso, rectificación, cancelación y oposición de sus datos en el domicilio de <strong>GRUPO QUALIA</strong>, Oficinas Centrales sito en <strong>C/ Pablo Iglesias, 58, 1º-1ª.</strong> Código Postal: <strong>08302</strong>. Localidad: <strong>Mataró</strong>. Provincia: <strong>Barcelona</strong>.<br>
				Si usted no desea recibir nuestra información, póngase en contacto con nosotros enviando un correo electrónico a la siguiente dirección: <strong>info@grupqualia.es</strong><br><br>
				 
				<strong>CONFIDENTIALITY NOTICE:</strong> This message and any files are intended exclusively for its addressee and may contain confidential information subject to professional secrecy. Reproduction or distribution without the express permission of <strong>GRUPO QUALIA</strong> is not allowed. If you are not the intended recipient please delete it and inform us in this way.<br>
				In accordance with the provisions of Organic Law 15/1999 of December 13, Protection of Personal Data (LOPD), we inform you that your details are incorporated into a file that holds <strong>GRUPO QUALIA, S.L.</strong> in order to perform administrative, accounting and fiscal management, as well as send you communications about our products and / or services.<br>
				Also advised of the possibility of exercising rights of access, rectification, cancellation and opposition of their data at the home of <strong>GRUPO QUALIA</strong>, located in <strong>C / Pablo Iglesias, 58, 1º-1ª.</strong> Postal Code: 08302. Location: <strong>Mataró.</strong> Province: <strong>Barcelona.</strong><br>
				If you do not wish to receive information, please contact us by sending an email to the following address: <strong>info@grupqualia.es</strong>.</div>
				 \n\n";
		
		$factura=generaFactura($datosFacturas['codigo']);
		if($factura =! ''){
			$fp=fopen($factura, "r");
			$tam=filesize($factura);
			$fichero=fread($fp,$tam);
			$ficheroAdjunto=chunk_split(base64_encode($fichero));
			fclose($fp);
			
			$mensaje.="--{$limiteMime}\r\n";
			$mensaje.="Content-Type: application/octet-stream; name=\"".basename($factura)."\"\r\n"."Content-Description:".basename($factura)."\r\n"."Content-Disposition: attachment;filename=\"".basename($factura)."\";size=".$tam."\r\n"."Content-Transfer-Encoding:base64\r\n\r\n".$ficheroAdjunto."\r\n\r\n";
			
			$datosCliente=datosRegistro('clientes',$datosFacturas['codigoCliente']);
			$contrato=generaContrato($datosFacturas['codigoCliente']);
			
			$fp=fopen($contrato, "r");
			$tam=filesize($contrato);
			$fichero=fread($fp,$tam);
			$ficheroAdjunto=chunk_split(base64_encode($fichero));
			fclose($fp);
			
			$mensaje.="--{$limiteMime}\r\n";
			$mensaje.="Content-Type: application/octet-stream; name=\"".basename($contrato)."\"\r\n"."Content-Description:".basename($contrato)."\r\n"."Content-Disposition: attachment;filename=\"".basename($contrato)."\";size=".$tam."\r\n"."Content-Transfer-Encoding:base64\r\n\r\n".$ficheroAdjunto."\r\n\r\n";
			
			$mensaje .= "--{$limiteMime}--";

			}
			
			if (!mail($datosCliente['mail'].", atencion.cliente2@grupqualia.es", 'Correo de bienvenida', $mensaje ,$headers)){
				$res=false;
			}
		}

		if($res){
			$headers="From: programacion@qmaconsultores.com\r\n";
			$headers.= "MIME-Version: 1.0\r\n";
			$mensaje= "Se ha procedido al envío de las facturas a los clientes del día: ".formateaFechaWeb($nuevafecha).". Para cualquier incidencia contacte con: programacion@qmaconsultores.com";
			$mensaje=stripcslashes($mensaje);
			//mail('atencion.cliente2@grupqualia.es', 'Envío de facturas completado', $mensaje, $headers);
		}*/

	//Y el return??
}

function selectCursos($codigo=false){
	conexionBD();
	$consulta=consultaBD("SELECT cursos.codigo, CONCAT(cursos.codigoInterno, ' ',accionFormativa.denominacion) AS texto FROM cursos INNER JOIN accionFormativa ON cursos.codigoaccionFormativa=accionFormativa.codigoInterno GROUP BY cursos.codigo");
	cierraBD();
	if($codigo!=false){
		$comerciales=cursosFactura($codigo);
	}

	$datos=mysql_fetch_assoc($consulta);
	echo '<select name="cursos[]" class="selectpicker span4 show-tick" data-live-search="true" multiple data-selected-text-format="count" multiple title="Seleccione los cursos...">';
	while($datos!=false){
		echo '<option value="'.$datos['codigo'].'"';
		
		if($codigo!=false && in_array($datos['codigo'],$comerciales)){
			echo ' selected="selected"';
		}

		echo '>'.$datos['texto'].'</option>';
		$datos=mysql_fetch_assoc($consulta);
	}
	echo '</select>';
}

function cursosFactura($codigo){
	$datos=array();

	conexionBD();
	$consulta=consultaBD("SELECT codigoCurso FROM facturas_cursos WHERE codigoFactura='$codigo';");
	cierraBD();

	$reg=mysql_fetch_assoc($consulta);
	while($reg!=false){
		array_push($datos,$reg['codigoCurso']);
		$reg=mysql_fetch_assoc($consulta);
	}
	return $datos;
}

function insertaCursosFactura($codigoFactura){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
	consultaBD("DELETE FROM facturas_cursos WHERE codigoFactura='".$codigoFactura."';");

	foreach($datos['cursos'] as $curso){ 
		$consulta=consultaBD("INSERT INTO facturas_cursos VALUES('".$codigoFactura."', '$curso');");
		if(!$consulta){
			$res=false;
		}
	}
	cierraBD();
	return $res;
}

function generaDatosGraficoCuentasFiltrado(){
	$datos=array();
	//$codigoS=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
		$where='WHERE (codigoUsuario="'.$_SESSION['codigoS'].'" OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'"))';
	}
	elseif($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION'){
		$where=compruebaPerfilParaWhere();
	}

	conexionBD();
	
	$datosFormu=arrayFormulario();
	
	$filtrado=cargaVariablesFiltrado();
	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes $where $filtrado;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totales']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes $where AND activo='SI' $filtrado;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['confirmados']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function generaDatosGraficoPosiblesClientesFiltrado($activo='NO'){
	$datos=array();
	//$codigoS=$_SESSION['codigoS'];
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
		$where='WHERE codigoUsuario="'.$_SESSION['codigoS'].'" OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'")';
	}
	elseif($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ATENCION'){
		$where=compruebaPerfilParaWhere();
	}
	
	conexionBD();
	
	$datosFormu=arrayFormulario();
	
	$filtrado=cargaVariablesFiltrado();

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes $where $filtrado;");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['totales']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM clientes $where AND activo='".$activo."' $filtrado");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['pendientes']=$consulta['codigo'];

	cierraBD();

	return $datos;
}

function cargaVariablesFiltrado($tipo='POST'){
	($tipo=='POST') ? $datosFormu=arrayFormulario() : $datosFormu=arrayFormularioGet();
	$cadena='';
	
	if(isset($datosFormu['servicio']) && $datosFormu['servicio']!='NULL'){
		$cadena.=" AND clientes.codigo IN(SELECT codigoCliente FROM productos_clientes WHERE codigoProducto='".$datosFormu['servicio']."')";
	}
	if(isset($datosFormu['comercial']) && $datosFormu['comercial']!='NULL'){
		$cadena.=" AND clientes.codigoUsuario='".$datosFormu['comercial']."'";
	}
	if($datosFormu['estado']!='TODOS'){
		$cadena.=" AND clientes.estado='".$datosFormu['estado']."'";
	}
	if(isset($datosFormu['colaborador']) && $datosFormu['colaborador']!='NULL'){
		$cadena.=" AND clientes.comercial='".$datosFormu['colaborador']."'";
	}
	if($datosFormu['comprado']=='SI'){
		$cadena.=" AND clientes.codigo IN(SELECT codigoCliente FROM ventas WHERE fecha LIKE'".date('Y')."-%')";
	}
	if($datosFormu['tieneWeb']=='SI'){
		$cadena.=" AND clientes.web!=''";
	}
	if(isset($datosFormu['fechaUno']) && $datosFormu['fechaUno']!='' && isset($datosFormu['fechaDos']) && $datosFormu['fechaDos']!=''){
		$cadena.=" AND clientes.fechaUltimaVenta>='".$datosFormu['fechaUno']."' AND clientes.fechaUltimaVenta<='".$datosFormu['fechaDos']."'";
	}

	if(isset($datosFormu['compra']) && $datosFormu['compra'] != 'NULL'){
        if($datosFormu['compra'] == 'SI'){
        	$cadena .= ' AND clientes.codigo IN (SELECT codigoCliente FROM ventas)';
       	} else {
       		$cadena .= ' AND clientes.codigo NOT IN (SELECT codigoCliente FROM ventas)';
       	}
    }

    if(isset($datosFormu['baja'])){
    	$cadena .= " AND baja = '".$datosFormu['baja']."'";
    	if($datosFormu['baja'] == 'SI' && $datosFormu['motivoBaja'] != 'NULL'){
    		$cadena .= " AND motivoBaja = '".$datosFormu['motivoBaja']."'";
    	}
	}

	$cadena.=" AND localidad LIKE'%".$datosFormu['poblacion']."%' AND clientes.cp LIKE'%".$datosFormu['cp']."%' AND clientes.actividad LIKE'%".$datosFormu['actividad']."%' AND clientes.trabajadores='".$datosFormu['tieneTrabajadores']."'";

	return $cadena;
}

function selectConsultaMultiple($codigo=false){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombreProducto AS texto FROM productos ORDER BY codigoProducto;");//Añadido el administrador en la actualización del 20/10/2014
	cierraBD();
	if($codigo!=false){
		$productos=productosCliente($codigo);
	}

	$datos=mysql_fetch_assoc($consulta);
	echo '
	<div class="control-group">                     
	    <label class="control-label" for="estado">Tipo servicio a ofrecer:</label>
	    <div class="controls">
			<select name="productos[]" class="selectpicker span3 show-tick" data-live-search="true" multiple data-selected-text-format="count" multiple title="Seleccione los servicios...">';
	while($datos!=false){
		echo '<option value="'.$datos['codigo'].'"';
		
		if($codigo!=false && in_array($datos['codigo'],$productos)){
			echo ' selected="selected"';
		}

		echo '>'.$datos['texto'].'</option>';
		$datos=mysql_fetch_assoc($consulta);
	}
	echo '</select>
		</div> <!-- /controls -->       
    </div> <!-- /control-group -->';
}

function productosCliente($codigo){
	$datos=array();

	conexionBD();
	$consulta=consultaBD("SELECT codigoProducto FROM productos_clientes WHERE codigoCliente='$codigo';");
	cierraBD();

	$reg=mysql_fetch_assoc($consulta);
	while($reg!=false){
		array_push($datos,$reg['codigoProducto']);
		$reg=mysql_fetch_assoc($consulta);
	}
	return $datos;
}

function insertaServicios($codigoCliente){
	$res=true;
	$datos=arrayFormulario();	
	$i=0;	
	
	$consulta=consultaBD("DELETE FROM productos_clientes WHERE codigoCliente='$codigoCliente';");
	if(isset($datos['productos'])){
		foreach($datos['productos'] as $producto){ 
			$consulta=consultaBD("INSERT INTO productos_clientes VALUES('".$producto."', '".$codigoCliente."');");
			if(!$consulta){
				$res=false;
			}
			$i++;
		}
	}
	
	return $res;
}

function insertaEmpresasVinculadas($codigoCliente){
	$res=true;
	$datos=arrayFormulario();	
	$i=0;	
	
	$consulta=consultaBD("DELETE FROM empresas_vinculadas WHERE empresa1='$codigoCliente' OR empresa2='$codigoCliente';");
	if(isset($datos['empresasVinculadas'])){
		foreach($datos['empresasVinculadas'] as $empresa){ 
			$consulta=consultaBD("INSERT INTO empresas_vinculadas VALUES(NULL,'".$codigoCliente."', '".$empresa."');");
			if(!$consulta){
				$res=false;
			}
		}
	}
	
	return $res;
}

function campoSelectConsultaNulo($nombreCampo,$texto,$consulta,$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$disabled='',$tipo=0,$conexion=true,$multiple=''){//MODIFICACIÓN 16/06/2015: añadido el parámetro $multiple || MODIFICACIÓN 20/11/2014: creado el parámetro $conexion para evitar múltiples conexiones al usar la función en un bucle
	$valor=compruebaValorCampo($valor,$nombreCampo);
	if($disabled){
		$disabled="disabled='disabled'";
	}

	if($tipo==0){
		echo "
		<div class='control-group'>                     
			<label class='control-label' for='$nombreCampo'>$texto:</label>
			<div class='controls'>";
	}
	elseif($tipo==1){
		echo "<td>";
	}

	echo "<select name='$nombreCampo' class='$clase' id='$nombreCampo' $busqueda $disabled $multiple>
			<option value='NULL'>Sin asignación</option>";
		
		$consulta=consultaBD($consulta,$conexion);
		$datos=mysql_fetch_assoc($consulta);
		while($datos!=false){
			echo "<option value='".$datos['codigo']."'";

			if($valor!=false && $valor==$datos['codigo']){
				echo " selected='selected'";
			}

			echo ">".$datos['texto']."</option>";
			$datos=mysql_fetch_assoc($consulta);
		}
		
	echo "</select>";

	if($tipo==0){
		echo "
			</div> <!-- /controls -->       
		</div> <!-- /control-group -->";
	}
	elseif($tipo==1){
		echo "</td>";
	}
}


function imprimeCursosDetallesCuenta($codigoCuenta){
	$codigoU=$_SESSION['codigoS'];
	
	$where='';
	$where="WHERE cursos.codigo IN(SELECT codigoCurso FROM alumnos_registrados_cursos WHERE codigoAlumno IN(
	SELECT codigo FROM alumnos WHERE codigoVenta IN(SELECT codigo FROM ventas WHERE codigoCliente='$codigoCuenta')))";
	
	conexionBD();

	$consulta=consultaBD("SELECT cursos.codigo AS codigo, denominacion, nombre, apellidos, fechaInicio, fechaFin, accionFormativa.codigoInterno AS codigoAccion, cursos.codigoInterno AS grupo FROM (accionFormativa INNER JOIN cursos ON accionFormativa.codigoInterno=cursos.codigoAccionFormativa) INNER JOIN tutores ON cursos.codigoTutor=tutores.codigo $where ORDER BY denominacion, apellidos;");

	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		$fechaInicio=formateaFechaWeb($datos['fechaInicio']);
		$fechaFin=formateaFechaWeb($datos['fechaFin']);

		echo "
		<tr>
			<td> ".$datos['codigoAccion']." </td>
			<td> ".$datos['grupo']." </td>
        	<td> ".$datos['denominacion']." </td>
        	<td> ".$datos['apellidos'].", ".$datos['nombre']."</td>
        	<td> $fechaInicio </td>
        	<td> $fechaFin </td>
        	<td class='centro'>";
				if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='CONSULTORIA'){
					echo "<a href='detallesCurso.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
					<a href='notificaciones.php?codigo=".$datos['codigo']."' class='btn btn-warning'><i class='icon-bell-alt'></i> Notificaciones</i></a>
					<a href='costes.php?codigo=".$datos['codigo']."' class='btn btn-info'><i class='icon-euro'></i> Costes</i></a>";
				}
        		echo "
				<!--a href='generaXMLParticipantes.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-download'></i> XML Participantes</i></a-->
				<a href='generaDocumentacion.php?codigo=".$datos['codigo']."' class='btn btn-success'><i class='icon-download'></i> Documentación</i></a>
			</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function selectTeleconcertadores($codigo=false){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, CONCAT(nombre, ' ',apellidos) AS texto FROM usuarios WHERE tipo='TELECONCERTADOR' AND activoUsuario='SI';");//Añadido el administrador en la actualización del 20/10/2014
	cierraBD();
	if($codigo!=false){
		$teleconcertadores=teleconcertadoresSeleccionados($codigo);
	}

	$datos=mysql_fetch_assoc($consulta);
	echo '<select name="teleconcertadores[]" class="selectpicker span4 show-tick" data-live-search="true" multiple data-selected-text-format="count" multiple title="Seleccione a los teleconcertadores...">';
	while($datos!=false){
		echo '<option value="'.$datos['codigo'].'"';
		
		if($codigo!=false && in_array($datos['codigo'],$teleconcertadores)){
			echo ' selected="selected"';
		}

		echo '>'.$datos['texto'].'</option>';
		$datos=mysql_fetch_assoc($consulta);
	}
	echo '</select>';
}

function teleconcertadoresSeleccionados($codigo){
	$datos=array();

	conexionBD();
	$consulta=consultaBD("SELECT codigoTeleconcertador FROM usuarios_teleconcertadores WHERE codigoUsuario='$codigo';");
	cierraBD();

	$reg=mysql_fetch_assoc($consulta);
	while($reg!=false){
		array_push($datos,$reg['codigoTeleconcertador']);
		$reg=mysql_fetch_assoc($consulta);
	}
	return $datos;
}

function imprimeTareasDetallesCuenta($codigoCliente){
	$consulta=consultaBD("SELECT tareas.codigo, clientes.empresa, clientes.contacto, clientes.telefono, clientes.provincia, tareas.tarea, tareas.fechaInicio, tareas.estado, tareas.prioridad, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) AS usuarioAsignado, clientes.codigo AS codigoCliente, horaInicio, clientes.movil, tareas.pendiente, tareas.firmado 
		FROM clientes INNER JOIN tareas ON clientes.codigo=tareas.codigoCliente INNER JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo WHERE tareas.codigoCliente='$codigoCliente' AND tareas.estado='pendiente' ORDER BY tareas.fechaInicio ASC;",true);
	
	$colores=array('sincontactar'=>'','proceso'=>"class='registroAzul'",'reconcertar'=>"class='registroNegro'",'alta'=>"class='registroVerde'",
	'normal'=>"class='registroAmarillo'",'baja'=>"class='registroRojo'",'concertada'=>"class='registroVerdeClaro'",'visitado'=>"class='registroAzulClaro'");
	
	$prioridad=array('normal'=>"Pendiente",'alta'=>"Vendido",'baja'=>"Baja",'sincontactar'=>"Sin contactar",
	'proceso'=>"En proceso",'reconcertar'=>"Reconcertar",'concertada'=>'Concertada','visitado'=>'Visitado');
	$estado=array('pendiente'=>"<span class='label label-warning'>Pendiente</span>",'realizada'=>"<span class='label label-success'>Realizada</span>");
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		$fecha=formateaFechaWeb($datos['fechaInicio']);

		$textoConcatenado='';
		if($datos['prioridad']=='normal'){
			$textoConcatenado=". Firmado: ".$datos['firmado'].". Pendiente de: ".$datos['pendiente'];
		}
		
		echo "
		<tr>
        	<td> <a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a>  </td>
			<td> ".$datos['usuarioAsignado']." </td>
        	<td> ".$datos['provincia']." </td>
        	<td> ".$datos['contacto']." </td>
        	<td class='nowrap'> ".formateaTelefono($datos['telefono'])." </td>
			<td class='nowrap'> ".formateaTelefono($datos['movil'])." </td>
        	<td> ".$datos['tarea']." </td>
        	<td> $fecha </td>
			<td> ".formateaHoraWeb($datos['horaInicio'])." </td>
        	<td> <div ".$colores[$datos['prioridad']].">".$prioridad[$datos['prioridad']].$textoConcatenado."</div> </td>
	        <td class='centro'>
	        	<a href='detallesTarea.php?codigo=".$datos['codigo']."' class='btn btn-primary' target='_blank'><i class='icon-zoom-in'></i> Ver datos</i></a>
	        	<a href='tareas.php?realizaTarea=".$datos['codigo']."' class='btn btn-success' target='_blank'><i class='icon-ok-sign'></i> Realizada</i></a>
			</td>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

//SERVICIOS PARA LOS COLABORADORES
function selectConsultaMultipleColaboradores($codigo=false){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombreProducto AS texto FROM productos ORDER BY codigoProducto;");//Añadido el administrador en la actualización del 20/10/2014
	cierraBD();
	if($codigo!=false){
		$productos=productosColaborador($codigo);
	}

	$datos=mysql_fetch_assoc($consulta);
	echo '
	<div class="control-group">                     
	    <label class="control-label" for="estado">Servicios contratados:</label>
	    <div class="controls">
			<select name="productos[]" class="selectpicker span3 show-tick" data-live-search="true" multiple data-selected-text-format="count" multiple title="Seleccione los servicios...">';
	while($datos!=false){
		echo '<option value="'.$datos['codigo'].'"';
		
		if($codigo!=false && in_array($datos['codigo'],$productos)){
			echo ' selected="selected"';
		}

		echo '>'.$datos['texto'].'</option>';
		$datos=mysql_fetch_assoc($consulta);
	}
	echo '</select>
		</div> <!-- /controls -->       
    </div> <!-- /control-group -->';
}

function productosColaborador($codigo){
	$datos=array();

	conexionBD();
	$consulta=consultaBD("SELECT codigoProducto FROM productos_colaboradores WHERE codigoColaborador='$codigo';");
	cierraBD();

	$reg=mysql_fetch_assoc($consulta);
	while($reg!=false){
		array_push($datos,$reg['codigoProducto']);
		$reg=mysql_fetch_assoc($consulta);
	}
	return $datos;
}

function insertaServiciosColaborador($codigoColaborador){
	$res=true;
	$datos=arrayFormulario();	
	$i=0;	
	
	conexionBD();
	$consulta=consultaBD("DELETE FROM productos_colaboradores WHERE codigoColaborador='$codigoColaborador';");
	
	foreach($datos['productos'] as $producto){ 
		$consulta=consultaBD("INSERT INTO productos_colaboradores VALUES('".$producto."', '".$codigoColaborador."');");
		if(!$consulta){
			$res=false;
		}
		$i++;
	}
	cierraBD();
	
	return $res;
}
//FIN SERVICIOS PARA COLABORADORES

function reasignaTareas(){
	$res=true;
	$datos=arrayFormulario();
	$consulta=consultaBD("UPDATE tareas SET codigoUsuario='".$datos['codigoUsuarioFinal']."' WHERE codigoUsuario='".$datos['codigoUsuarioPartida']."';",true);
	if(!$consulta){
		$res=false;
	}
	return $res;
}
//Fin parte de Software

function filtroTrabajos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'ID Cliente');
	campoTexto(1,'Empresa');
	campoFecha(2,'Fecha máx. Entrega','0000-00-00');
	
	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelect(3,'Formación',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(4,'PRL',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(5,'LOPD',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(6,'LSSI',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(7,'ALER',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(8,'WEB',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(9,'Auditoría LOPD',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(10,'Auditoría PRL',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function filtroTrabajosArchivos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'ID Cliente');
	campoTexto(1,'Empresa');
	campoFecha(2,'Fecha máx. Entrega','0000-00-00');
	
	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelect(5,'Formación',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(6,'PRL',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(7,'LOPD',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(8,'LSSI',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(9,'ALER',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(10,'WEB',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(11,'Auditoría LOPD',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));
	campoSelect(12,'Auditoría PRL',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function filtroCursos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Nº Acción');
	campoTexto(1,'Grupo');
	campoTexto(2,'Acción formativa');
	
	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(3,'Tutor');
	campoFecha(4,'Fecha inicio','0000-00-00');
	campoFecha(5,'Fecha fin','0000-00-00');
	campoSelect(6,'Formación',array('Nada seleccionado','No tiene','No está hecho','En proceso','Finalizado'),array('','NO','HECHO','PROCESO','FINALIZADO'));

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function abreCajaBusqueda($id="cajaFiltros"){
   echo '<div class="cajaFiltros form-horizontal hide" id="'.$id.'">
           <h3 class="apartadoFormulario">Filtrar por:</h3>';
}

function cierraCajaBusqueda(){
   echo '</div>';
}

function abreColumnaCampos($clase='span3'){
	echo "<fieldset class='$clase'>";
}

function cierraColumnaCampos($salto=false){
	echo "</fieldset>";
	if($salto){
		echo "<fieldset class='sinFlotar'></fieldset>";
	}
}

function imprimeAlumnosDetallesCurso($codigoCurso){
	//$codigoS=$_SESSION['codigoS'];

	conexionBD();

	$consulta=consultaBD("SELECT codigo, nombre, apellidos, dni, mail, telefono, codigoVenta FROM alumnos WHERE codigo IN(SELECT codigoAlumno FROM alumnos_registrados_cursos
	WHERE codigoCurso='$codigoCurso') ORDER BY nombre;");
	$datos=mysql_fetch_assoc($consulta);

	while($datos!=0){
		$consulta2=consultaBD("SELECT ventas.codigoCliente, clientes.empresa FROM (alumnos INNER JOIN ventas ON alumnos.codigoVenta=ventas.codigo) INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo WHERE alumnos.codigoVenta='".$datos['codigoVenta']."';");
		$datos2=mysql_fetch_assoc($consulta2);
		echo "
		<tr>
        	<td> ".$datos['apellidos'].", ".$datos['nombre']." </td>
        	<td> ".$datos['dni']." </td>
        	<td> ".$datos['mail']." </td>
			<td> ".$datos['telefono']." </td>
			<td> ".$datos2['empresa']." </td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function imprimeDocumentosColaborador($codigoCliente){
	$consulta=consultaBD("SELECT * FROM documentos_colaboradores WHERE codigoCliente='$codigoCliente' ORDER BY fechaFactura DESC;",true);

	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		echo "
		<tr>
        	<td> ".$datos['nombreFactura']."</td>
        	<td> ".formateaFechaWeb($datos['fechaFactura'])." </td>
        	<td class='centro'>";
				if(file_exists("documentos/".$datos['ficheroFactura'])){
					echo "<a href='documentos/".$datos['ficheroFactura']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>";
				}else{
					echo "<a href='https://softwareparapymes.net/grupqualia/documentos/".$datos['ficheroFactura']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>";
				}        		
				if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='MARKETING'){
					echo "<a href='eliminaDocumentoColaborador.php?codigo=".$datos['codigo']."&eliminar=1&codigoCliente=$codigoCliente' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>";
				}
			echo "
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function conexionBDLOPD(){
	$conexion=mysql_connect("localhost","lopdQualia","Writemaster7");//PARA CONFIG
	if(!$conexion){ 
		echo "Error estableciendo la conexi&oacute;n a la BBDD.<br />";
	}
	else{
		if(!mysql_select_db("lopdQualia")){
			echo "Error seleccionando base de datos QMA.<br />";
		}
	}

}

//Inserción automática de los datos de un formulario en una tabla de la BDD
function insertaDatosLOPD($tabla,$nombreFichero='',$ruta='ficheros'){
	$res=true;
	$datos=arrayFormulario();

	conexionBDLOPD();
	$campos=camposTabla($tabla);

	$consulta="INSERT INTO $tabla VALUES(NULL";
	foreach($campos as $campo){
		if(substr_count($campo,'check')==1 && !isset($datos[$campo])){//Comprueba si el campo es un check y si NO se ha marcado
			$datos[$campo]='NO';
		}
		elseif(substr_count($campo,'fichero')==1 && $_FILES[$campo]['name']==''){//Comprueba si el campo es un tipe="file" y no se ha subido ningún fichero
			$datos[$campo]='NO';
		}
		elseif(substr_count($campo,'fichero')==1){//Comprueba si el campo es un tipe="file" y se ha subido un fichero
			$datos[$campo]=subeDocumento($campo,$nombreFichero,$ruta);
		}

		if(!isset($datos[$campo])){
			$res=false;
		}
		elseif($datos[$campo]=='NULL'){//MODIFICACIÓN 23/07/2014 (para pasar a Jose Luis): evita que el valor NULL se inserte como una cadena
			$consulta.=", ".$datos[$campo];
		}
		else{
			$consulta.=",'".$datos[$campo]."'";
		}
	}
	$consulta.=");";
	
	$res=consultaBD($consulta);
	if($res){
		$res=mysql_insert_id();//Si la consulta se ha realizado correctamente devuelve el código de la inserción
	}
	else{
		echo mysql_error();
		echo $consulta;
	}

	cierraBD();

	return $res;
}

function pasaLOPD($codigo){
	$url_actual = explode('/',$_SERVER["REQUEST_URI"]);
  	if($url_actual[1] == 'grupqualia'){
	$res=true;
	$datos=datosRegistro('clientes',$codigo);
	if(!isset($datos['comercial'])){
		$datos['comercial']='NULL';
	}
	conexionBDLOPD();
	$consultaAux=consultaBD("SELECT codigo FROM clientes WHERE num_cliente='".$datos['referencia']."';",false,true);
	if(!isset($consultaAux['codigo'])){
		$sql='INSERT INTO clientes(codigo, num_cliente, fecha, razon_s, cif_nif, nombre, apellido1, apellido2, nif, cargo, dir_postal, provincia, localidad, postal, telefono, fax, email, empleado, colaborador, iban)
		VALUES(NULL, "'.$datos['referencia'].'", "'.$datos['fechaRegistro'].'", "'.$datos['empresa'].'", 
		"'.$datos['cif'].'", "'.$datos['contacto'].'", "", "", "'.$datos['dniRepresentante'].'", "'.$datos['cargo'].'", "'.$datos['direccion'].'",
		"'.$datos['provincia'].'", "'.$datos['localidad'].'", "'.$datos['cp'].'", "'.$datos['telefono'].'", "'.$datos['fax'].'", "'.$datos['mail'].'", 
		NULL, '.$datos['comercial'].', "'.$datos['numCuenta'].'");';
		$consulta=consultaBD($sql);
		if(!$consulta){
			$res=false;
			$mensaje = 'Consulta fallida '.$sql;
			mail('programacion2@qmaconsultores.com','Error al pasar a lopd',$mensaje);
		}
	}else{
		$res=false;
	}
	cierraBD();
	return $res;
	}
}

function mensajeResultadoGet($indice,$res,$campo,$texto='Cliente traspasado correctamente.'){
	if(isset($_GET[$indice])){
		if($res){
		  mensajeOk($texto); 
		}
		else{
		  mensajeError("se ha producido un error al gestionar los datos. Compruebe la información introducida."); 
		}
	}
}

//Función que requiere demasiados recursos y deja el sistema pillado
/*function exportarExcelClientes($objPHPExcel,$activo){
	conexionBD();
	$query=consultaBD("SELECT clientes.empresa, clientes.cif, clientes.direccion, clientes.cp, clientes.localidad, clientes.provincia, clientes.telefono, clientes.movil, clientes.mail, clientes.fax, clientes.contacto, clientes.cargo, colaboradores.empresa AS colaborador, clientes.sector, 
	clientes.tipoEmpresa, clientes.fechaRegistro, clientes.fechaVenta, clientes.ccc, clientes.dniRepresentante, clientes.representacion, clientes.nuevacreacion, clientes.fechaCreacion, clientes.pyme, clientes.repreLegal, clientes.numCuenta, clientes.telefonoDos, clientes.movilDos, 
	clientes.tieneColaborador, clientes.creditoFormativo, clientes.formaPago, clientes.fechaUltimaVenta, clientes.bic, clientes.referencia, clientes.baja, clientes.fechaBaja, clientes.numTrabajadores, clientes.tipoRemesa, clientes.nombreGestoria, clientes.mailGestoria, clientes.telefonoGestoria,
	clientes.estado, clientes.direccionVisita, clientes.firmado, clientes.pendiente, clientes.responsableSeguridad, clientes.nifResponsable, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS usuario, clientes.codigo AS codigoCliente
	FROM clientes LEFT JOIN usuarios ON clientes.codigoUsuario=usuarios.codigo
	LEFT JOIN colaboradores ON clientes.comercial=colaboradores.codigo
	WHERE activo='$activo' ORDER BY empresa LIMIT 10;");

	$letras=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV');
	$fila=2;
	while($datosClientes=mysql_fetch_array($query,MYSQL_NUM)){
		for($j=0;$j<=15;$j++){
			$objPHPExcel->getActiveSheet()->getCell($letras[$j].$fila)->setValue($datosClientes[$j]);
		}
		$servicios=consultaBD("SELECT productos.nombreProducto FROM productos_clientes 
		LEFT JOIN productos ON productos_clientes.codigoProducto=productos.codigo WHERE productos_clientes.codigoCliente='".$datosClientes[47]."';");
		$serviciosContratados="";
		while($datosServicios=mysql_fetch_assoc($servicios)){
			$serviciosContratados.=$datosServicios['nombreProducto'].", ";
		}
		$serviciosContratados=substr($serviciosContratados,0,-2);
		$objPHPExcel->getActiveSheet()->getCell('Q'.$fila)->setValue($serviciosContratados);
		for($j=17,$k=16;$j<=47;$j++,$k++){
			$objPHPExcel->getActiveSheet()->getCell($letras[$j].$fila)->setValue($datosClientes[$k]);
		}
		$fila++;
	}
	cierraBD();	
	$objPHPExcel->getActiveSheet()->getStyle('A1:AV1')->getFill()->getStartColor()->setRGB('9BC2E6');
}*/

//Exportar los clientes en CSV para evitar errores de tiempos de carga
function exportarCSVClientes($activo){
	conexionBD();
	$query=consultaBD("SELECT clientes.empresa, clientes.cif, clientes.direccion, clientes.cp, clientes.localidad, clientes.provincia, clientes.telefono, clientes.movil, clientes.mail, clientes.fax, clientes.contacto, clientes.cargo, colaboradores.empresa AS colaborador, clientes.sector, 
	clientes.tipoEmpresa, clientes.fechaRegistro, clientes.fechaVenta, clientes.ccc, clientes.dniRepresentante, clientes.representacion, clientes.nuevacreacion, clientes.fechaCreacion, clientes.pyme, clientes.repreLegal, clientes.numCuenta, clientes.telefonoDos, clientes.movilDos, 
	clientes.tieneColaborador, clientes.creditoFormativo, clientes.formaPago, clientes.fechaUltimaVenta, clientes.bic, clientes.referencia, clientes.baja, clientes.fechaBaja, clientes.numTrabajadores, clientes.tipoRemesa, clientes.nombreGestoria, clientes.mailGestoria, clientes.telefonoGestoria,
	clientes.estado, clientes.direccionVisita, clientes.firmado, clientes.pendiente, clientes.responsableSeguridad, clientes.nifResponsable, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS usuario, clientes.codigo AS codigoCliente
	FROM clientes LEFT JOIN usuarios ON clientes.codigoUsuario=usuarios.codigo
	LEFT JOIN colaboradores ON clientes.comercial=colaboradores.codigo
	WHERE activo='$activo' ORDER BY empresa;");
	
	$fichero=fopen("documentos/Listado_clientes.csv", "w");
	fwrite($fichero, pack("CCC",0xef,0xbb,0xbf)); 

	fwrite($fichero, "Empresa;CIF;Dirección;CP;Localidad;Provincia;Teléfono;Móvil;Mail;Fax;Contacto;Cargo;Colaborador;Sector;Tipo empres;Fecha registro;Servicios contratados;Resolución;Fecha venta;C.C.C.;DNI Representante;Represtanción Legal Trabajadores;Nueva creación;Fecha Creación;PYME;Representante legal;Num. Cuenta;Teléfono 2;Móvil 2;Tiene colaborador;Crédito formativo;Forma de pago;Fecha última venta;BIC;Referencia;Baja;Fecha baja;Num. Trabajadores;Tipo remesa;Nombre gestoría;Mail gestoría;Teléfono gestoría ;Estado;Dirección visita;Firmado;Pendiente;Responsable Seguridad;NIF Responsable;Usuario asignado\r\n");
	$textoTarea=array(''=>'','sincontactar'=>'Sin contactar','proceso'=>"En proceso",'reconcertar'=>"Reconcertar",'alta'=>"Cerrada",
	'normal'=>"Pendiente",'pendiente'=>"Pendiente",'baja'=>"Baja",'concertada'=>'Concertada','visitado'=>'Visitado');
	while($datosClientes=mysql_fetch_array($query,MYSQL_NUM)){
		for($j=0;$j<=15;$j++){
			$datosClientes[$j] = str_replace(';', ',', $datosClientes[$j]);
			fwrite($fichero, $datosClientes[$j].";");
		}
		$servicios=consultaBD("SELECT productos.nombreProducto FROM productos_clientes 
		LEFT JOIN productos ON productos_clientes.codigoProducto=productos.codigo WHERE productos_clientes.codigoCliente='".$datosClientes[47]."';");
		$serviciosContratados="";
		while($datosServicios=mysql_fetch_assoc($servicios)){
			$serviciosContratados.=$datosServicios['nombreProducto'].", ";
		}
		$serviciosContratados=substr($serviciosContratados,0,-2);
		fwrite($fichero, $serviciosContratados.";");
		$tarea=consultaBD("SELECT prioridad FROM tareas WHERE codigoCliente='".$datosClientes[47]."' ORDER BY codigo DESC LIMIT 1;");
		$tarea=mysql_fetch_assoc($tarea);
		fwrite($fichero, $textoTarea[$tarea['prioridad']].";");
		for($j=16;$j<=46;$j++){
			fwrite($fichero, $datosClientes[$j].";");
		}
		fwrite($fichero, "\r\n");
	}
	cierraBD();	
}

function selectColaboradores(){
	echo "<select name='codigoColaborador' class='selectpicker show-tick' data-live-search='true'>";
	conexionBD();
	$consulta=consultaBD("SELECT codigo, empresa FROM colaboradores ORDER BY empresa;");
	cierraBD();
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=false){
		echo "<option value='".$datos['codigo']."'";

		echo ">".$datos['empresa']."</option>";
		$datos=mysql_fetch_assoc($consulta);
	}
	echo "</select>";
}

function creaEstadisticasFacturasColaborador($codigoColaborador, $where='WHERE 1=1'){
	$datos=array();
	
	$datosColaborador=datosRegistro('colaboradores',$codigoColaborador);

	conexionBD();

	$consulta=consultaBD("SELECT SUM(coste) AS codigo FROM facturacion $where AND codigoCliente IN(SELECT codigo FROM clientes WHERE tieneColaborador='SI' AND comercial='$codigoColaborador') AND (facturacion.devuelta='NO' OR (facturacion.devuelta='SI' AND facturacion.resolucionFactura!='Anulada'));");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['comision']=number_format((float) $consulta['codigo']*($datosColaborador['comision']/100), 2, ',', '');

	cierraBD();

	return $datos;
}

function imprimeFacturasComisionColaborador($codigoColaborador,$where=''){
	$datosColaborador=datosRegistro('colaboradores',$codigoColaborador);
	conexionBD();
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.fechaEmision, facturacion.cobrada AS facturacionCobrada, facturacion.fechaVencimiento, facturacion.referencia, facturacion.coste, clientes.empresa, facturacion.enviada, facturacion.concepto, facturacion.enviadaCliente, clientes.codigo AS codigoCliente, insercion, facturacion.firma, facturacion.devuelta, facturacion.pagadoColaborador, productos.nombreProducto 
		FROM facturacion LEFT JOIN clientes ON clientes.codigo=facturacion.codigoCliente
		LEFT JOIN productos ON facturacion.concepto=productos.codigo $where AND codigoCliente IN(SELECT codigo FROM clientes WHERE tieneColaborador='SI' AND comercial='$codigoColaborador') AND (facturacion.devuelta='NO' OR (facturacion.devuelta='SI' AND facturacion.resolucionFactura!='Anulada'))
		ORDER BY insercion DESC;");
	
	$datos=mysql_fetch_assoc($consulta);
	$iconoC=array('SI'=>'<i class="icon-ok-sign iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove-sign iconoFactura icon-danger"></i>');
	while($datos!=0){

		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$fechaVencimiento=formateaFechaWeb($datos['fechaVencimiento']);
		echo "
		<tr>
			<td> ";
				if($datos['concepto']=='14'){ 
					if($datos['firma']!='3'){
						echo"F";
					}else{
						echo"S";
					}
				}else{
					if($datos['firma']!='3'){
						echo"C";
					}else{
						echo"S";
					}
				}
			echo "-".$datos['referencia']." </td>
			<td> <a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a>  </td>
        	<td> ".$datos['nombreProducto']." </td>
        	<td> $fecha </td>
			<td> $fechaVencimiento </td>
			<td> ".formateaNumero($datos['coste'])." €</td>
			<td> ".formateaNumero($datos['coste']*($datosColaborador['comision']/100))." €</td>
			<td> ".$iconoC[$datos['pagadoColaborador']]." </td>
			<td> ".$iconoC[$datos['facturacionCobrada']]." </td>
			<td class='centro'>
				<div class='btn-group'>
                    <button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
                    <ul class='dropdown-menu warning' role='menu'>
						<li><a href='detallesComisiones.php?codigo=".$datos['codigo']."' class='pull-left'><span class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles comisiones</i></a></span></li>
					</ul>
				</div>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function selectComercialesComisiones(){
	echo "<select name='codigoComercial' class='selectpicker show-tick' data-live-search='true'>";
	conexionBD();
	$consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS comercial FROM usuarios WHERE activoUsuario='SI' AND tipo='COMERCIAL' ORDER BY nombre, apellidos;");
	cierraBD();
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=false){
		echo "<option value='".$datos['codigo']."'";

		echo ">".$datos['comercial']."</option>";
		$datos=mysql_fetch_assoc($consulta);
	}
	echo "</select>";
}

function creaEstadisticasFacturasComercial($codigoComercial, $where='WHERE 1=1', $whereVentas=''){
	$datos=array();

	$datosComercial=datosRegistro('usuarios',$codigoComercial);
	$datosComercial['objetivo'];
	conexionBD();

	$consulta=consultaBD("SELECT SUM(precio) AS codigo FROM ventas $whereVentas AND codigoUsuario='$codigoComercial';");
	//$consulta=consultaBD("SELECT SUM(coste) AS codigo FROM facturacion $where AND codigoVenta IN(SELECT codigo FROM ventas WHERE codigoUsuario='$codigoComercial') AND (facturacion.devuelta='NO' OR (facturacion.devuelta='SI' AND facturacion.resolucionFactura!='Anulada'));");
	$consulta=mysql_fetch_assoc($consulta);
	if($consulta['codigo']>=$datosComercial['objetivo']){
		$datos['comisiona']='SI';
		
		$comisionNueva=consultaBD("SELECT SUM(facturacion.coste) AS codigo FROM facturacion INNER JOIN facturas_vto ON facturacion.codigo=facturas_vto.codigoFactura $where AND codigoVenta IN(SELECT codigo FROM ventas WHERE codigoUsuario='$codigoComercial') AND tipoFacturaComercial='NUEVA' AND (facturacion.devuelta='NO' OR (facturacion.devuelta='SI' AND facturacion.resolucionFactura!='Anulada'));",false,true);
		$comisionNueva=$comisionNueva['codigo']*($datosComercial['porcentajeNueva']/100);
		
		$comisionCartera=consultaBD("SELECT SUM(facturacion.coste) AS codigo FROM facturacion INNER JOIN facturas_vto ON facturacion.codigo=facturas_vto.codigoFactura $where AND codigoVenta IN(SELECT codigo FROM ventas WHERE codigoUsuario='$codigoComercial') AND tipoFacturaComercial='CARTERA' AND (facturacion.devuelta='NO' OR (facturacion.devuelta='SI' AND facturacion.resolucionFactura!='Anulada'));",false,true);;
		$comisionCartera=$comisionCartera['codigo']*($datosComercial['porcentajeCartera']/100);
		
		$comisionAuditoria=consultaBD("SELECT SUM(facturacion.coste) AS codigo FROM facturacion INNER JOIN facturas_vto ON facturacion.codigo=facturas_vto.codigoFactura $where AND codigoVenta IN(SELECT codigo FROM ventas WHERE codigoUsuario='$codigoComercial') AND tipoFacturaComercial='AUDITORIA' AND (facturacion.devuelta='NO' OR (facturacion.devuelta='SI' AND facturacion.resolucionFactura!='Anulada'));",false,true);;
		$comisionAuditoria=$comisionAuditoria['codigo']*($datosComercial['porcentajeAuditoria']/100);
		
		$comisionTeleventa=consultaBD("SELECT SUM(facturacion.coste) AS codigo FROM facturacion INNER JOIN facturas_vto ON facturacion.codigo=facturas_vto.codigoFactura $where AND codigoVenta IN(SELECT codigo FROM ventas WHERE codigoUsuario='$codigoComercial') AND tipoFacturaComercial='TELEVENTA' AND (facturacion.devuelta='NO' OR (facturacion.devuelta='SI' AND facturacion.resolucionFactura!='Anulada'));",false,true);;
		$comisionTeleventa=$comisionTeleventa['codigo']*($datosComercial['porcentajeTeleventa']/100);
		
		$total=$comisionNueva+$comisionCartera+$comisionAuditoria+$comisionTeleventa;
		$datos['comision']=number_format((float) $total, 2, ',', '');
	}else{
		$datos['comisiona']='NO';
		$datos['comision']=number_format((float) '0', 2, ',', '');
	}

	cierraBD();

	return $datos;
}

function imprimeFacturasComisionComercial($codigoComercial,$where='',$comisiona){
	$datosComercial=datosRegistro('usuarios',$codigoComercial);
	conexionBD();
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.fechaEmision, facturas_vto.cobrada AS facturacionCobrada, facturacion.fechaVencimiento, facturacion.referencia, facturacion.coste, clientes.empresa, facturacion.enviada, facturacion.concepto, facturacion.enviadaCliente, clientes.codigo AS codigoCliente, insercion, facturacion.firma, facturacion.devuelta, facturacion.tipoFacturaComercial, productos.nombreProducto, facturacion.pagadoColaborador 
		FROM facturacion INNER JOIN facturas_vto ON facturacion.codigo=facturas_vto.codigoFactura LEFT JOIN clientes ON clientes.codigo=facturacion.codigoCliente
		LEFT JOIN productos ON facturacion.concepto=productos.codigo $where AND codigoVenta IN(SELECT codigo FROM ventas WHERE codigoUsuario='$codigoComercial') AND (facturacion.devuelta='NO' OR (facturacion.devuelta='SI' AND facturacion.resolucionFactura!='Anulada'))
		ORDER BY insercion DESC;");
	
	$datos=mysql_fetch_assoc($consulta);
	$tipoVenta=array('NUEVA'=>'porcentajeNueva','CARTERA'=>'porcentajeCartera','TELEVENTA'=>'porcentajeTeleventa','AUDITORIA'=>'porcentajeAuditoria');
	$iconoC=array('SI'=>'<i class="icon-ok-sign iconoFactura icon-success"></i>','NO'=>'<i class="icon-remove-sign iconoFactura icon-danger"></i>');
	while($datos!=0){

		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$fechaVencimiento=formateaFechaWeb($datos['fechaVencimiento']);
		echo "
		<tr>
			<td> ";
				if($datos['concepto']=='14'){ 
					if($datos['firma']!='3'){
						echo"F";
					}else{
						echo"S";
					}
				}else{
					if($datos['firma']!='3'){
						echo"C";
					}else{
						echo"S";
					}
				}
			echo "-".$datos['referencia']." </td>
			<td> <a href='detallesCuenta.php?codigo=".$datos['codigoCliente']."'>".$datos['empresa']."</a>  </td>
        	<td> ".$datos['nombreProducto']." </td>
        	<td> $fecha </td>
			<td> $fechaVencimiento </td>
			<td> ".formateaNumero($datos['coste'])." €</td>";
			if($comisiona=='SI'){
				echo "<td> ".formateaNumero($datos['coste']*($datosComercial[$tipoVenta[$datos['tipoFacturaComercial']]]/100))." €</td>";
			}else{
				echo "<td> ".formateaNumero('0')." €</td>";
			}
			echo "<td> ".$iconoC[$datos['pagadoColaborador']]." </td>
			<td> ".$iconoC[$datos['facturacionCobrada']]." </td>
			<td class='centro'>
				<div class='btn-group'>
                    <button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
                    <ul class='dropdown-menu warning' role='menu'>
						<li><a href='detallesComisiones.php?codigo=".$datos['codigo']."&comercial=SI' class='pull-left'><span class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles comisiones</i></a></span></li>
					</ul>
				</div>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}

function imprimeDocumentosUsuario($codigoCliente){
	$consulta=consultaBD("SELECT * FROM documentos_usuarios WHERE codigoCliente='$codigoCliente' ORDER BY fechaFactura DESC;",true);

	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){
		echo "
		<tr>
        	<td> ".$datos['nombreFactura']."</td>
        	<td> ".formateaFechaWeb($datos['fechaFactura'])." </td>
        	<td class='centro'>";
				if(file_exists("documentos/".$datos['ficheroFactura'])){
					echo "<a href='documentos/".$datos['ficheroFactura']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>";
				}else{
					echo "<a href='https://softwareparapymes.net/grupqualia/documentos/".$datos['ficheroFactura']."' target='_blank' class='btn btn-primary'><i class='icon-download'></i> Descargar</i></a>";
				}        		
				if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='MARKETING'){
					echo "<a href='eliminaDocumentoUsuario.php?codigo=".$datos['codigo']."&eliminar=1&codigoCliente=$codigoCliente' class='btn btn-danger'><i class='icon-trash'></i> Eliminar</i></a>";
				}
			echo "
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
}

function reasignaClientes($activo='NO'){
	$res=true;
	$datos=arrayFormulario();
	if($datos['reasignacionDirecta']=='Directa'){
		$consulta=consultaBD("UPDATE clientes SET codigoUsuario='".$datos['codigoUsuarioFinal']."' WHERE codigoUsuario='".$datos['codigoUsuarioPartida']."' AND activo='$activo';",true);
	}else{
		$consulta=consultaBD("UPDATE clientes SET codigoUsuario='".$datos['codigoUsuarioAsignacion']."' WHERE cp='".$datos['cp']."' AND localidad='".$datos['poblacion']."' AND activo='$activo';",true);
	}
	if(!$consulta){
		$res=false;
	}
	return $res;
}

function actualizaMotivos(){
	$datos=arrayFormulario();
	$res=true;
	$i=1;
	while(isset($datos['motivo'.$i])){
		$val=consultaBD("INSERT INTO motivos VALUES('$i','".$datos['motivo'.$i]."');",true);
		if(!$val){
			$res=$res && consultaBD("UPDATE motivos SET motivo='".$datos['motivo'.$i]."' WHERE codigo='$i';",true);
		}
		$i++;
	}
	return $res;
}

function creaEstadisticasFacturasSupervisor($usuario){
	$datos=array();

	$firmas=array('supervisor'=>1,'supervisor2'=>2,'supervisor3'=>3);

	conexionBD();
	$consulta=consultaBD("SELECT COUNT(codigo) AS codigo FROM facturacion WHERE firma=".$firmas[$usuario]." AND fechaEmision>='2015-03-01' AND fechaEmision<='2015-12-31';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['facturacion']=$consulta['codigo'];
	
	$consulta=consultaBD("SELECT SUM(coste) AS total FROM facturacion WHERE firma=".$firmas[$usuario]." AND fechaEmision>='2015-03-01' AND fechaEmision<='2015-12-31';");
	$consulta=mysql_fetch_assoc($consulta);

	$datos['total']=number_format((float)$consulta['total'], 2, ',', '');

	cierraBD();

	return $datos;
}

function imprimefacturasSupervisor($usuario){
	conexionBD();
	$firmas=array('supervisor'=>1,'supervisor2'=>2,'supervisor3'=>3);
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.fechaEmision, facturacion.cobrada, facturacion.fechaVencimiento, facturacion.referencia, facturacion.coste, 
	clientes.empresa, facturacion.enviada, facturacion.concepto, facturacion.enviadaCliente, clientes.codigo AS codigoCliente, insercion, facturacion.firma, 
	facturacion.devuelta, productos.nombreProducto, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS comercial, tipoVentaCarteraNueva
		FROM facturacion LEFT JOIN clientes ON clientes.codigo=facturacion.codigoCliente 
		LEFT JOIN productos ON facturacion.concepto=productos.codigo
		LEFT JOIN ventas ON facturacion.codigoVenta=ventas.codigo
		LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo WHERE firma=".$firmas[$usuario]." AND fechaEmision>='2015-03-01' AND fechaEmision<='2015-12-31'
		ORDER BY insercion DESC;");
	
	$datos=mysql_fetch_assoc($consulta);
	while($datos!=0){

		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$fechaVencimiento=formateaFechaWeb($datos['fechaVencimiento']);
		echo "
		<tr>
			<td> ";
				if($datos['concepto']=='14'){ 
					if($datos['firma']!='3'){
						echo"F";
					}else{
						echo"S";
					}
				}else{
					if($datos['firma']!='3'){
						echo"C";
					}else{
						echo"S";
					}
				}
			echo "-".$datos['referencia']." </td>
			<td> ".$datos['empresa']." </td>
			<td> ".$datos['comercial']." </td>
        	<td> ".$datos['nombreProducto']." </td>
        	<td> ".$datos['tipoVentaCarteraNueva']." </td>
        	<td> $fecha </td>
			<td> $fechaVencimiento </td>
			<td> ".formateaNumero($datos['coste'])." €</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
    	$datos=mysql_fetch_assoc($consulta);
	}
	cierraBD();
}


//Para cargar select por ajax y agilizar las tareas
function campoSelectConsultaAjax($nombreCampo,$texto,$consulta,$valor=false,$enlace='',$clase='selectpicker selectAjax span2 show-tick',$disabled='',$tipo=0,$conexion=true){
    global $_CONFIG;

    $valor=compruebaValorCampo($valor,$nombreCampo);
    if($disabled){
        $disabled="disabled='disabled'";
    }

    if($tipo==0){
        echo "
        <div class='control-group'>                     
            <label class='control-label' for='$nombreCampo'>$texto:</label>
            <div class='controls nowrap'>";
    }
    elseif($tipo==1){
        echo "<td>";
    }

    echo "<select name='$nombreCampo' consulta=\"$consulta\" class='$clase' id='$nombreCampo' $disabled data-live-search='true'>
            <option value='NULL'></option>";
        
    $consulta=consultaBD($consulta,$conexion);
    while($datos=mysql_fetch_assoc($consulta)){

        if($valor!=false && $valor==$datos['codigo']){//Por defecto solo se muestra el valor seleccionado, si existiese
            echo "<option value='".$datos['codigo']."' selected='selected' >".$datos['texto']."</option>";
        }
    }
        
    echo "</select> <a enlace='$enlace' href='#' class='btn btn-primary btn-small botonSelectAjax noAjax' target='_blank' id='boton-$nombreCampo'><i class='icon-external-link'></i></a>";

    if($tipo==0){
        echo "
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "</td>";
    }
}

function cargaBusquedaSelectAjax(){
    $consulta=$_POST['consulta'];
    $busqueda=$_POST['busqueda'];

    $partesConsulta=explode('ORDER BY',$consulta);
	$consultaConHaving=explode('HAVING',$consulta);
	(isset($consultaConHaving[1])) ? $having='AND' : $having='HAVING';
	
    if(isset($partesConsulta[1])){//La consulta está ordenada...
        $consulta=$partesConsulta[0]." $having texto LIKE '%$busqueda%' ORDER BY ".$partesConsulta[1];
    }
    else{
        $consulta=$partesConsulta[0]." $having texto LIKE '%$busqueda%'";//Uso HAVING para utilizar el alias del campo a filtrar
    }
    
    $res="<option value='NULL'></option>";
    $consulta=consultaBD($consulta,true);
    while($datos=mysql_fetch_assoc($consulta)){
        $res.="<option value='".$datos['codigo']."'>".$datos['texto']."</option>";
    }

    echo $res;
}

//Función para áreas de texto con filtrado por años
function areaTextoFiltrar($nombreCampo,$texto,$valor='',$clase='areaTexto',$disabled='',$idCampo='anioFiltrado',$idBoton='activarFiltrado',$valorCalendario=''){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	$disabledArea='';
	($valorCalendario=='') ? $valorCalendario=date('Y') : $valorCalendario=$valorCalendario;
	if($disabled){
		$disabledArea='disabled="disabled"';
	}
	echo "
	<div class='control-group'>                     
      <label class='control-label espacioArriba' for='$nombreCampo'>$texto:</label>
      <div class='controls'>
		<div class='anchoContenedorFiltros'>
			<input id='$idCampo' name='$idCampo' class='input-mini inputFiltrado' value='$valorCalendario'></input>
			<button class='btn btn-primary icon-calendar iconoFiltrado' type='button' id='$idBoton'></button>
		</div>
        <textarea name='$nombreCampo' class='$clase' id='$nombreCampo' $disabledArea>$valor</textarea>
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function arrayFormularioGet(){
	$datos=array();
	foreach($_GET as $nombre=>$valor){
		if(substr_count($nombre,'fecha')==1){//Para formatear la fecha a almacenar
			$valor=formateaFechaBD($valor);
		}
		elseif(!is_array($valor)){
			$valor=addslashes($valor);
		}
		$datos[$nombre]=$valor;
	}
	return $datos;
}

function exportarCSVVentas($codigoCliente){
	conexionBD();
	$whereCliente='';
	if($codigoCliente){
		$whereCliente="AND codigoCliente='$codigoCliente'";
	}
	$query=consultaBD("SELECT ventas.tipo, CONCAT(nombre, ' ', apellidos) AS comercial, productos.nombreProducto, fecha, precio, observaciones, tipoVentaCarteraNueva, empresa FROM ventas
	INNER JOIN clientes ON clientes.codigo=ventas.codigoCliente LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo
	LEFT JOIN productos ON ventas.concepto=productos.codigo WHERE clientes.baja='NO' $whereCliente");
	
	$fichero=fopen("documentos/Listado_ventas.csv", "w");
	fwrite($fichero, pack("CCC",0xef,0xbb,0xbf)); 

	fwrite($fichero, "Tipo de venta;Comercial;Concepto;Fecha;Importe;Observaciones;Tipo de venta;Cliente;\r\n");
	while($datosVentas=mysql_fetch_array($query,MYSQL_NUM)){
		for($j=0;$j<=7;$j++){
			fwrite($fichero, $datosVentas[$j].";");
		}
		fwrite($fichero, "\r\n");
	}
	cierraBD();	
}

//Ránking comercial para mostrar al inicio del administrador

function generaDatosRanking(){
	conexionBD();
	$textoDeRelleno="";
	$textoGrafico="";
	
	$consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS comercial, colorTareas FROM usuarios WHERE tipo='COMERCIAL' AND codigo IN(SELECT codigoUsuario FROM ventas WHERE fecha LIKE'".date('Y')."-%') AND activoUsuario='SI' ORDER BY codigo");
	echo "var pieData = [";
	$i=1;
	while($datosComerciales=mysql_fetch_assoc($consulta)){
		$datosVenta=consultaBD("SELECT SUM(precio) AS total FROM ventas WHERE codigoUsuario='".$datosComerciales['codigo']."' AND fecha LIKE'".date('Y')."-%' AND precio>0;",false,true);
		$textoGrafico.="{
            value: ".$datosVenta['total'].",
            color: '".$datosComerciales['colorTareas']."',
			label: '".$datosComerciales['comercial']."'
        },";
		$textoDeRelleno.='$("#valor'.$i.'").text('.$datosVenta['total'].');';
		$i++;
	}
	$textoGrafico=substr($textoGrafico,0,-1);
	echo $textoGrafico."];";
	cierraBD();
	
	return $textoDeRelleno;
}

function cargaTotalValores(){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS comercial FROM usuarios WHERE tipo='COMERCIAL' AND codigo IN(SELECT codigoUsuario FROM ventas WHERE fecha LIKE'".date('Y')."-%') AND activoUsuario='SI' ORDER BY codigo");
	$i=1;
	while($datosComerciales=mysql_fetch_assoc($consulta)){
		echo '<span class="grafico grafico2"></span>'.$datosComerciales['comercial'].': <span id="valor'.$i.'"></span><br>';
		$i++;
	}
	cierraBD();
}

function campoActividades($nombreCampo='actividad',$texto='Actividad',$datos=false){
	$i=0;
	foreach (generaArrayActividades() as $key => $value) {
		$nombres[$i] = $value;
		$valores[$i] = $key;
		$i++;
	}
	campoSelect($nombreCampo,$texto,$nombres,$valores,$datos);
}

function generaArrayActividades(){
	$actividades=array();
	$valores = array('','0111','0112','0113','0114','0115','0116','0119','0121','0122','0123','0124','0125','0126','0127','0128','0129','0130','0141','0142','0143','0144','0145','0146','0147','0149','0150','0161','0162','0163','0164','0170','0210','0220','0230','0240','0311','0312','0321','0322','0510','0520','0610','0620','0710','0721','0729','0811','0812','0891','0892','0893','0899','0910','0990','1011','1012','1013','1021','1022','1031','1032','1039','1042','1043','1044','1052','1053','1054','1061','1062','1071','1072','1073','1081','1082','1083','1084','1085','1086','1089','1091','1092','1101','1102','1103','1104','1105','1106','1107','1200','1310','1320','1330','1391','1392','1393','1394','1395','1396','1399','1411','1412','1413','1414','1419','1420','1431','1439','1511','1512','1520','1610','1621','1622','1623','1624','1629','1711','1712','1721','1722','1723','1724','1729','1811','1812','1813','1814','1820','1910','1920','2011','2012','2013','2014','2015','2016','2017','2020','2030','2041','2042','2051','2052','2053','2059','2060','2110','2120','2211','2219','2221','2222','2223','2229','2311','2312','2313','2314','2319','2320','2331','2332','2341','2342','2343','2344','2349','2351','2352','2361','2362','2363','2364','2365','2369','2370','2391','2399','2410','2420','2431','2432','2433','2434','2441','2442','2443','2444','2445','2446','2451','2452','2453','2454','2511','2512','2521','2529','2530','2540','2550','2561','2562','2571','2572','2573','2591','2592','2593','2594','2599','2611','2612','2620','2630','2640','2651','2652','2660','2670','2680','2711','2712','2720','2731','2732','2733','2740','2751','2752','2790','2811','2812','2813','2814','2815','2821','2822','2823','2824','2825','2829','2830','2841','2849','2891','2892','2893','2894','2895','2896','2899','2910','2920','2931','2932','3011','3012','3020','3030','3040','3091','3092','3099','3101','3102','3103','3109','3211','3212','3213','3220','3230','3240','3250','3291','3299','3311','3312','3313','3314','3315','3316','3317','3319','3320','3512','3513','3514','3515','3516','3517','3518','3519','3521','3522','3523','3530','3600','3700','3811','3812','3821','3822','3831','3832','3900','4110','4121','4122','4211','4212','4213','4221','4222','4291','4299','4311','4312','4313','4321','4322','4329','4331','4332','4333','4334','4339','4391','4399','4511','4519','4520','4531','4532','4540','4611','4612','4613','4614','4615','4616','4617','4618','4619','4621','4622','4623','4624','4631','4632','4633','4634','4635','4636','4637','4638','4639','4641','4642','4643','4644','4645','4646','4647','4648','4649','4651','4652','4661','4662','4663','4664','4665','4666','4669','4671','4672','4673','4674','4675','4676','4677','4690','4711','4719','4721','4722','4723','4724','4725','4726','4729','4730','4741','4742','4743','4751','4752','4753','4754','4759','4761','4762','4763','4764','4765','4771','4772','4773','4774','4775','4776','4777','4778','4779','4781','4782','4789','4791','4799','4910','4920','4931','4932','4939','4941','4942','4950','5010','5020','5030','5040','5110','5121','5122','5210','5221','5222','5223','5224','5229','5310','5320','5510','5520','5530','5590','5610','5621','5629','5630','5811','5812','5813','5814','5819','5821','5829','5912','5914','5915','5916','5917','5918','5920','6010','6020','6110','6120','6130','6190','6201','6202','6203','6209','6311','6312','6391','6399','6411','6419','6420','6430','6491','6492','6499','6511','6512','6520','6530','6611','6612','6619','6621','6622','6629','6630','6810','6820','6831','6832','6910','6920','7010','7021','7022','7111','7112','7120','7211','7219','7220','7311','7312','7320','7410','7420','7430','7490','7500','7711','7712','7721','7722','7729','7731','7732','7733','7734','7735','7739','7740','7810','7820','7830','7911','7912','7990','8010','8020','8030','8110','8121','8122','8129','8130','8211','8219','8220','8230','8291','8292','8299','8411','8412','8413','8421','8422','8423','8424','8425','8430','8510','8520','8531','8532','8541','8543','8544','8551','8552','8553','8559','8560','8610','8621','8622','8623','8690','8710','8720','8731','8732','8790','8811','8812','8891','8899','9001','9002','9003','9004','9102','9103','9104','9105','9106','9200','9311','9312','9313','9319','9321','9329','9411','9412','9420','9491','9492','9499','9511','9512','9521','9522','9523','9524','9525','9529','9601','9602','9603','9604','9609','9700','9810','9820','9900');
	$nombres = array('',' Cultivo de cereales (excepto arroz), leguminosas y semillas oleaginosas ',' Cultivo de arroz ',' Cultivo de hortalizas, raíces y tubérculos ',' Cultivo de caña de azúcar ',' Cultivo de tabaco ',' Cultivo de plantas para fibras textiles ',' Otros cultivos no perennes ',' Cultivo de la vid ',' Cultivo de frutos tropicales y subtropicales ',' Cultivo de cítricos, ',' Cultivo de frutos con hueso y pepitas ',' Cultivo de otros árboles y arbustos frutales y frutos secos ',' Cultivo de frutos oleaginosos ',' Cultivo de plantas para bebidas ',' Cultivo de especias, plantas aromáticas, medicinales y farmacéuticas ',' Otros cultivos perennes ',' Propagación de plantas ',' Explotación de ganado bovino para la producción de leche ',' Explotación de otro ganado bovino y búfalos ',' Explotación de caballos y otros equinos ',' Explotación de camellos y otros camélidos ',' Explotación de ganado ovino y caprino ',' Explotación de ganado porcino ',' Avicultura ',' Otras explotaciones de ganado ',' Producción agrícola combinada con la producción ganadera ',' Actividades de apoyo a la agricultura ',' Actividades de apoyo a la ganadería ',' Actividades de preparación posterior a la cosecha ',' Tratamiento de semillas para reproducción ',' Caza, captura de animales y servicios relacionados con las mismas ',' Silvicultura y otras actividades forestales ',' Explotación de la madera ',' Recolección de productos silvestres, excepto madera ',' Servicios de apoyo a la silvicultura ',' Pesca marina ',' Pesca en agua dulce ',' Acuicultura marina ',' Acuicultura en agua dulce ',' Extracción de antracita y hulla ',' Extracción de lignito ',' Extracción de crudo de petróleo ',' Extracción de gas natural ',' Extracción de minerales de hierro ',' Extracción de minerales de uranio y torio ',' Extracción de otros minerales metálicos no férreos ',' Extracción de piedra ornamental y para la construcción, piedra caliza, yeso, creta y pizarra ',' Extracción de gravas y arenas; extracción de arcilla y caolín ',' Extracción de minerales para productos químicos y fertilizantes ',' Extracción de turba ',' Extracción de sal ',' Otras industrias extractivas n.c.o.p. ',' Actividades de apoyo a la extracción de petróleo y gas natural ',' Actividades de apoyo a otras industrias extractivas ',' Procesado y conservación de carne ',' Procesado y conservación de volatería ',' Elaboración de productos cárnicos y de volatería ',' Procesado de pescados, crustáceos y moluscos ',' Fabricación de conservas de pescado ',' Procesado y conservación de patatas ',' Elaboración de zumos de frutas y hortalizas ',' Otro procesado y conservación de frutas y hortalizas ',' Fabricación de margarina y grasas comestibles similares ',' Fabricación de aceite de oliva ',' Fabricación de otros aceites y grasas ',' Elaboración de helados ',' Fabricación de quesos ',' Preparación de leche y otros productos lácteos ',' Fabricación de productos de molinería ',' Fabricación de almidones y productos amiláceos ',' Fabricación de pan y de productos frescos de panadería y pastelería ',' Fabricación de galletas y productos de panadería y pastelería de larga duración ',' Fabricación de pastas alimenticias, cuscús y productos similares ',' Fabricación de azúcar ',' Fabricación de cacao, chocolate y productos de confitería ',' Elaboración de café, té e infusiones ',' Elaboración de especias, salsas y condimentos ',' Elaboración de platos y comidas preparados ',' Elaboración de preparados alimenticios homogeneizados y alimentos dietéticos ',' Elaboración de otros productos alimenticios n.c.o.p. ',' Fabricación de productos para la alimentación de animales de granja ',' Fabricación de productos para la alimentación de animales de compañía ',' Destilación, rectificación y mezcla de bebidas alcohólicas ',' Elaboración de vinos ',' Elaboración de sidra y otras bebidas fermentadas a partir de frutas ',' Elaboración de otras bebidas no destiladas, procedentes de la fermentación ',' Fabricación de cerveza ',' Fabricación de malta ',' Fabricación de bebidas no alcohólicas; producción de aguas minerales y otras aguas embotelladas ',' Industria del tabaco ',' Preparación e hilado de fibras textiles ',' Fabricación de tejidos textiles ',' Acabado de textiles ',' Fabricación de tejidos de punto ',' Fabricación de artículos confeccionados con textiles, excepto prendas de vestir ',' Fabricación de alfombras y moquetas ',' Fabricación de cuerdas, cordeles, bramantes y redes ',' Fabricación de telas no tejidas y artículos confeccionados con ellas, excepto prendas de vestir ',' Fabricación de otros productos textiles de uso técnico e industrial ',' Fabricación de otros productos textiles n.c.o.p. ',' Confección de prendas de vestir de cuero ',' Confección de ropa de trabajo ',' Confección de otras prendas de vestir exteriores ',' Confección de ropa interior ',' Confección de otras prendas de vestir y accesorios ',' Fabricación de artículos de peletería ',' Confección de calcetería ',' Confección de otras prendas de vestir de punto ',' Preparación, curtido y acabado del cuero; preparación y teñido de pieles ',' Fabricación de artículos de marroquinería, viaje y de guarnicionería y talabartería ',' Fabricación de calzado ',' Aserrado y cepillado de la madera ',' Fabricación de chapas y tableros de madera ',' Fabricación de suelos de madera ensamblados ',' Fabricación de otras estructuras de madera y piezas de carpintería y ebanistería para la construcción ',' Fabricación de envases y embalajes de madera ',' Fabricación de otros productos de madera; artículos de corcho, cestería y espartería ',' Fabricación de pasta papelera ',' Fabricación de papel y cartón ',' Fabricación de papel y cartón ondulados; fabricación de envases y embalajes de papel y cartón ',' Fabricación de artículos de papel y cartón para uso doméstico, sanitario e higiénico ',' Fabricación de artículos de papelería ',' Fabricación de papeles pintados ',' Fabricación de otros artículos de papel y cartón ',' Artes gráficas y servicios relacionados con las mismas ',' Otras actividades de impresión y artes gráficas ',' Servicios de preimpresión y preparación de soportes ',' Encuadernación y servicios relacionados con la misma ',' Reproducción de soportes grabados ',' Coquerías ',' Refino de petróleo ',' Fabricación de gases industriales ',' Fabricación de colorantes y pigmentos ',' Fabricación de otros productos básicos de química inorgánica ',' Fabricación de otros productos básicos de química orgánica ',' Fabricación de fertilizantes y compuestos nitrogenados ',' Fabricación de plásticos en formas primarias ',' Fabricación de caucho sintético en formas primarias ',' Fabricación de pesticidas y otros productos agroquímicos ',' Fabricación de pinturas, barnices y revestimientos similares; tintas de imprenta y masillas ',' Fabricación de jabones, detergentes y otros artículos de limpieza y abrillantamiento ',' Fabricación de perfumes y cosméticos ',' Fabricación de explosivos ',' Fabricación de colas ',' Fabricación de aceites esenciales ',' Fabricación de otros productos químicos n.c.o.p. ',' Fabricación de fibras artificiales y sintéticas ',' Fabricación de productos farmacéuticos de base ',' Fabricación de especialidades farmacéuticas ',' Fabricación de neumáticos y cámaras de caucho; reconstrucción y recauchutado de neumáticos ',' Fabricación de otros productos de caucho ',' Fabricación de placas, hojas, tubos y perfiles de plástico ',' Fabricación de envases y embalajes de plástico ',' Fabricación de productos de plástico para la construcción ',' Fabricación de otros productos de plástico ',' Fabricación de vidrio plano ',' Manipulado y transformación de vidrio plano ',' Fabricación de vidrio hueco ',' Fabricación de fibra de vidrio ',' Fabricación y manipulado de otro vidrio, incluido el vidrio técnico ',' Fabricación de productos cerámicos refractarios ',' Fabricación de azulejos y baldosas de cerámica ',' Fabricación de ladrillos, tejas y productos de tierras cocidas para la construcción ',' Fabricación de artículos cerámicos de uso doméstico y ornamental ',' Fabricación de aparatos sanitarios cerámicos ',' Fabricación de aisladores y piezas aislantes de material cerámico ',' Fabricación de otros productos cerámicos de uso técnico ',' Fabricación de otros productos cerámicos ',' Fabricación de cemento ',' Fabricación de cal y yeso ',' Fabricación de elementos de hormigón para la construcción ',' Fabricación de elementos de yeso para la construcción ',' Fabricación de hormigón fresco ',' Fabricación de mortero ',' Fabricación de fibrocemento ',' Fabricación de otros productos de hormigón, yeso y cemento ',' Corte, tallado y acabado de la piedra ',' Fabricación de productos abrasivos ',' Fabricación de otros productos minerales no metálicos n.c.o.p. ',' Fabricación de productos básicos de hierro, acero y ferroaleaciones ',' Fabricación de tubos, tuberías, perfiles huecos y sus accesorios, de acero ',' Estirado en frío ',' Laminación en frío ',' Producción de perfiles en frío por conformación con plegado ',' Trefilado en frío ',' Producción de metales preciosos ',' Producción de aluminio ',' Producción de plomo, zinc y estaño ',' Producción de cobre ',' Producción de otros metales no férreos ',' Procesamiento de combustibles nucleares ',' Fundición de hierro ',' Fundición de acero ',' Fundición de metales ligeros ',' Fundición de otros metales no férreos ',' Fabricación de estructuras metálicas y sus componentes ',' Fabricación de carpintería metálica ',' Fabricación de radiadores y calderas para calefacción central ',' Fabricación de otras cisternas, grandes depósitos y contenedores de metal ',' Fabricación de generadores de vapor, excepto calderas de calefacción central ',' Fabricación de armas y municiones ',' Forja, estampación y embutición de metales; metalurgia de polvos ',' Tratamiento y revestimiento de metales ',' Ingeniería mecánica por cuenta de terceros ',' Fabricación de artículos de cuchillería y cubertería ',' Fabricación de cerraduras y herrajes ',' Fabricación de herramientas ',' Fabricación de bidones y toneles de hierro o acero ',' Fabricación de envases y embalajes metálicos ligeros ',' Fabricación de productos de alambre, cadenas y muelles ',' Fabricación de pernos y productos de tornillería ',' Fabricación de otros productos metálicos n.c.o.p. ',' Fabricación de componentes electrónicos ',' Fabricación de circuitos impresos ensamblados ',' Fabricación de ordenadores y equipos periféricos ',' Fabricación de equipos de telecomunicaciones ',' Fabricación de productos electrónicos de consumo ',' Fabricación de instrumentos y aparatos de medida, verificación y navegación ',' Fabricación de relojes ',' Fabricación de equipos de radiación, electromédicos y electroterapéuticos ',' Fabricación de instrumentos de óptica y equipo fotográfico ',' Fabricación de soportes magnéticos y ópticos ',' Fabricación de motores, generadores y transformadores eléctricos ',' Fabricación de aparatos de distribución y control eléctrico ',' Fabricación de pilas y acumuladores eléctricos ',' Fabricación de cables de fibra óptica ',' Fabricación de otros hilos y cables electrónicos y eléctricos ',' Fabricación de dispositivos de cableado ',' Fabricación de lámparas y aparatos eléctricos de iluminación ',' Fabricación de electrodomésticos ',' Fabricación de aparatos domésticos no eléctricos ',' Fabricación de otro material y equipo eléctrico ',' Fabricación de motores y turbinas, excepto los destinados a aeronaves, vehículos automóviles y ciclomotores ',' Fabricación de equipos de transmisión hidráulica y neumática ',' Fabricación de otras bombas y compresores ',' Fabricación de otra grifería y válvulas ',' Fabricación de cojinetes, engranajes y órganos mecánicos de transmisión ',' Fabricación de hornos y quemadores ',' Fabricación de maquinaria de elevación y manipulación ',' Fabricación de máquinas y equipos de oficina, excepto equipos informáticos ',' Fabricación de herramientas eléctricas manuales ',' Fabricación de maquinaria de ventilación y refrigeración no doméstica ',' Fabricación de otra maquinaria de uso general n.c.o.p. ',' Fabricación de maquinaria agraria y forestal ',' Fabricación de máquinas herramienta para trabajar el metal ',' Fabricación de otras máquinas herramienta ',' Fabricación de maquinaria para la industria metalúrgica ',' Fabricación de maquinaria para las industrias extractivas y de la construcción ',' Fabricación de maquinaria para la industria de la alimentación, bebidas y tabaco ',' Fabricación de maquinaria para las industrias textil, de la confección y del cuero ',' Fabricación de maquinaria para la industria del papel y del cartón ',' Fabricación de maquinaria para la industria del plástico y el caucho ',' Fabricación de otra maquinaria para usos específicos n.c.o.p. ',' Fabricación de vehículos de motor ',' Fabricación de carrocerías para vehículos de motor; fabricación de remolques y semirremolques ',' Fabricación de equipos eléctricos y electrónicos para vehículos de motor ',' Fabricación de otros componentes, piezas y accesorios para vehículos de motor ',' Construcción de barcos y estructuras flotantes ',' Construcción de embarcaciones de recreo y deporte ',' Fabricación de locomotoras y material ferroviario ',' Construcción aeronáutica y espacial y su maquinaria ',' Fabricación de vehículos militares de combate ',' Fabricación de motocicletas ',' Fabricación de bicicletas y de vehículos para personas con discapacidad ',' Fabricación de otro material de transporte n.c.o.p. ',' Fabricación de muebles de oficina y de establecimientos comerciales ',' Fabricación de muebles de cocina ',' Fabricación de colchones ',' Fabricación de otros muebles ',' Fabricación de monedas ',' Fabricación de artículos de joyería y artículos similares ',' Fabricación de artículos de bisutería y artículos similares ',' Fabricación de instrumentos musicales ',' Fabricación de artículos de deporte ',' Fabricación de juegos y juguetes ',' Fabricación de instrumentos y suministros médicos y odontológicos ',' Fabricación de escobas, brochas y cepillos ',' Otras industrias manufactureras n.c.o.p. ',' Reparación de productos metálicos ',' Reparación de maquinaria ',' Reparación de equipos electrónicos y ópticos ',' Reparación de equipos eléctricos ',' Reparación y mantenimiento naval ',' Reparación y mantenimiento aeronáutico y espacial ',' Reparación y mantenimiento de otro material de transporte ',' Reparación de otros equipos ',' Instalación de máquinas y equipos industriales ',' Transporte de energía eléctrica ',' Distribución de energía eléctrica ',' Comercio de energía eléctrica ',' Producción de energía hidroeléctrica ',' Producción de energía eléctrica de origen térmico convencional ',' Producción de energía eléctrica de origen nuclear ',' Producción de energía eléctrica de origen eólico ',' Producción de energía eléctrica de otros tipos ',' Producción de gas ',' Distribución por tubería de combustibles gaseosos ',' Comercio de gas por tubería ',' Suministro de vapor y aire acondicionado ',' Captación, depuración y distribución de agua ',' Recogida y tratamiento de aguas residuales ',' Recogida de residuos no peligrosos ',' Recogida de residuos peligrosos ',' Tratamiento y eliminación de residuos no peligrosos ',' Tratamiento y eliminación de residuos peligrosos ',' Separación y clasificación de materiales ',' Valorización de materiales ya clasificados ',' Actividades de descontaminación y otros servicios de gestión de residuos ',' Promoción inmobiliaria ',' Construcción de edificios residenciales ',' Construcción de edificios no residenciales ',' Construcción de carreteras y autopistas ',' Construcción de vías férreas de superficie y subterráneas ',' Construcción de puentes y túneles ',' Construcción de redes para fluidos ',' Construcción de redes eléctricas y de telecomunicaciones ',' Obras hidráulicas ',' Construcción de otros proyectos de ingeniería civil n.c.o.p. ',' Demolición ',' Preparación de terrenos ',' Perforaciones y sondeos ',' Instalaciones eléctricas ',' Fontanería, instalaciones de sistemas de calefacción y aire acondicionado ',' Otras instalaciones en obras de construcción ',' Revocamiento ',' Instalación de carpintería ',' Revestimiento de suelos y paredes ',' Pintura y acristalamiento ',' Otro acabado de edificios ',' Construcción de cubiertas ',' Otras actividades de construcción especializada n.c.o.p. ',' Venta de automóviles y vehículos de motor ligeros ',' Venta de otros vehículos de motor ',' Mantenimiento y reparación de vehículos de motor ',' Comercio al por mayor de repuestos y accesorios de vehículos de motor ',' Comercio al por menor de repuestos y accesorios de vehículos de motor ',' Venta, mantenimiento y reparación de motocicletas y de sus repuestos y accesorios ',' Intermediarios del comercio de materias primas agrarias, animales vivos, materias primas textiles y productos semielaborados ',' Intermediarios del comercio de combustibles, minerales, metales y productos químicos industriales ',' Intermediarios del comercio de la madera y materiales de construcción ',' Intermediarios del comercio de maquinaria, equipo industrial, embarcaciones y aeronaves ',' Intermediarios del comercio de muebles, artículos para el hogar y ferretería ',' Intermediarios del comercio de textiles, prendas de vestir, peletería, calzado y artículos de cuero ',' Intermediarios del comercio de productos alimenticios, bebidas y tabaco ',' Intermediarios del comercio especializados en la venta de otros productos específicos ',' Intermediarios del comercio de productos diversos ',' Comercio al por mayor de cereales, tabaco en rama, simientes y alimentos para animales ',' Comercio al por mayor de flores y plantas ',' Comercio al por mayor de animales vivos ',' Comercio al por mayor de cueros y pieles ',' Comercio al por mayor de frutas y hortalizas ',' Comercio al por mayor de carne y productos cárnicos ',' Comercio al por mayor de productos lácteos, huevos, aceites y grasas comestibles ',' Comercio al por mayor de bebidas ',' Comercio al por mayor de productos del tabaco ',' Comercio al por mayor de azúcar, chocolate y confitería ',' Comercio al por mayor de café, té, cacao y especias ',' Comercio al por mayor de pescados y mariscos y otros productos alimenticios ',' Comercio al por mayor, no especializado, de productos alimenticios, bebidas y tabaco ',' Comercio al por mayor de textiles ',' Comercio al por mayor de prendas de vestir y calzado ',' Comercio al por mayor de aparatos electrodomésticos ',' Comercio al por mayor de porcelana, cristalería y artículos de limpieza ',' Comercio al por mayor de productos perfumería y cosmética ',' Comercio al por mayor de productos farmacéuticos ',' Comercio al por mayor de muebles, alfombras y aparatos de iluminación ',' Comercio al por mayor de artículos de relojería y joyería ',' Comercio al por mayor de otros artículos de uso doméstico ',' Comercio al por mayor de ordenadores, equipos periféricos y programas informáticos ',' Comercio al por mayor de equipos electrónicos y de telecomunicaciones y sus componentes ',' Comercio al por mayor de maquinaria, equipos y suministros agrícolas ',' Comercio al por mayor de máquinas herramienta ',' Comercio al por mayor de maquinaria para la minería, la construcción y la ingeniería civil ',' Comercio al por mayor de maquinaria para la industria textil y de máquinas de coser y tricotar ',' Comercio al por mayor de muebles de oficina ',' Comercio al por mayor de otra maquinaria y equipo de oficina ',' Comercio al por mayor de otra maquinaria y equipo ',' Comercio al por mayor de combustibles sólidos, líquidos y gaseosos, y productos similares ',' Comercio al por mayor de metales y minerales metálicos ',' Comercio al por mayor de madera, materiales de construcción y aparatos sanitarios ',' Comercio al por mayor de ferretería, fontanería y calefacción ',' Comercio al por mayor de productos químicos ',' Comercio al por mayor de otros productos semielaborados ',' Comercio al por mayor de chatarra y productos de desecho ',' Comercio al por mayor no especializado ',' Comercio al por menor en establecimientos no especializados, con predominio en productos alimenticios, bebidas y tabaco ',' Otro comercio al por menor en establecimientos no especializados ',' Comercio al por menor de frutas y hortalizas en establecimientos especializados ',' Comercio al por menor de carne y productos cárnicos en establecimientos especializados ',' Comercio al por menor de pescados y mariscos en establecimientos especializados ',' Comercio al por menor de pan y productos de panadería, confitería y pastelería en establecimientos especializados ',' Comercio al por menor de bebidas en establecimientos especializados ',' Comercio al por menor de productos de tabaco en establecimientos especializados ',' Otro comercio al por menor de productos alimenticios en establecimientos especializados ',' Comercio al por menor de combustible para la automoción en establecimientos especializados ',' Comercio al por menor de ordenadores, equipos periféricos y programas informáticos en establecimientos especializados ',' Comercio al por menor de equipos de telecomunicaciones en establecimientos especializados ',' Comercio al por menor de equipos de audio y vídeo en establecimientos especializados ',' Comercio al por menor de textiles en establecimientos especializados ',' Comercio al por menor de ferretería, pintura y vidrio en establecimientos especializados ',' Comercio al por menor de alfombras, moquetas y revestimientos de paredes y suelos en establecimientos especializados ',' Comercio al por menor de aparatos electrodomésticos en establecimientos especializados ',' Comercio al por menor de muebles, aparatos de iluminación y otros artículos de uso doméstico en establecimientos especializados ',' Comercio al por menor de libros en establecimientos especializados ',' Comercio al por menor de periódicos y artículos de papelería en establecimientos especializados ',' Comercio al por menor de grabaciones de música y vídeo en establecimientos especializados ',' Comercio al por menor de artículos deportivos en establecimientos especializados ',' Comercio al por menor de juegos y juguetes en establecimientos especializados ',' Comercio al por menor de prendas de vestir en establecimientos especializados ',' Comercio al por menor de calzado y artículos de cuero en establecimientos especializados ',' Comercio al por menor de productos farmacéuticos en establecimientos especializados ',' Comercio al por menor de artículos médicos y ortopédicos en establecimientos especializados ',' Comercio al por menor de productos cosméticos e higiénicos en establecimientos especializados ',' Comercio al por menor de flores, plantas, semillas, fertilizantes, animales de compañía y alimentos para los mismos en establecimientos especializados ',' Comercio al por menor de artículos de relojería y joyería en establecimientos especializados ',' Otro comercio al por menor de artículos nuevos en establecimientos especializados ',' Comercio al por menor de artículos de segunda mano en establecimientos ',' Comercio al por menor de productos alimenticios, bebidas y tabaco en puestos de venta y en mercadillos ',' Comercio al por menor de productos textiles, prendas de vestir y calzado en puestos de venta y en mercadillos ',' Comercio al por menor de otros productos en puestos de venta y en mercadillos ',' Comercio al por menor por correspondencia o Internet ',' Otro comercio al por menor no realizado ni en establecimientos, ni en puestos de venta ni en mercadillos ',' Transporte interurbano de pasajeros por ferrocarril ',' Transporte de mercancías por ferrocarril ',' Transporte terrestre urbano y suburbano de pasajeros ',' Transporte por taxi ',' tipos de transporte terrestre de pasajeros n.c.o.p. ',' Transporte de mercancías por carretera ',' Servicios de mudanza ',' Transporte por tubería ',' Transporte marítimo de pasajeros ',' Transporte marítimo de mercancías ',' Transporte de pasajeros por vías navegables interiores ',' Transporte de mercancías por vías navegables interiores ',' Transporte aéreo de pasajeros ',' Transporte aéreo de mercancías ',' Transporte espacial ',' Depósito y almacenamiento ',' Actividades anexas al transporte terrestre ',' Actividades anexas al transporte marítimo y por vías navegables interiores ',' Actividades anexas al transporte aéreo ',' Manipulación de mercancías ',' Otras actividades anexas al transporte ',' Actividades postales sometidas a la obligación del servicio universal ',' Otras actividades postales y de correos ',' Hoteles y alojamientos similares ',' Alojamientos turísticos y otros alojamientos de corta estancia ',' Campings y aparcamientos para caravanas ',' Otros alojamientos ',' Restaurantes y puestos de comidas ',' Provisión de comidas preparadas para eventos ',' Otros servicios de comidas ',' Establecimientos de bebidas ',' Edición de libros ',' Edición de directorios y guías de direcciones postales ',' Edición de periódicos ',' Edición de revistas ',' Otras actividades editoriales ',' Edición de videojuegos ',' Edición de otros programas informáticos ',' Actividades de postproducción cinematográfica, de vídeo y de programas de televisión ',' Actividades de exhibición cinematográfica ',' Actividades de producción cinematográfica y de vídeo ',' Actividades de producciones de programas de televisión ',' Actividades de distribución cinematográfica y de vídeo ',' Actividades de distribución de programas de televisión ',' Actividades de grabación de sonido y edición musical ',' Actividades de radiodifusión ',' Actividades de programación y emisión de televisión ',' Telecomunicaciones por cable ',' Telecomunicaciones inalámbricas ',' Telecomunicaciones por satélite ',' Otras actividades de telecomunicaciones ',' Actividades de programación informática ',' Actividades de consultoría informática ',' Gestión de recursos informáticos ',' Otros servicios relacionados con las tecnologías de la información y la informática ',' Proceso de datos, hosting y actividades relacionadas ',' Portales web ',' Actividades de las agencias de noticias ',' Otros servicios de información n.c.o.p. ',' Banco central ',' Otra intermediación monetaria ',' Actividades de las sociedades holding ',' Inversión colectiva, fondos y entidades financieras similares ',' Arrendamiento financiero ',' Otras actividades crediticias ',' Otros servicios financieros, excepto seguros y fondos de pensiones n.c.o.p. ',' Seguros de vida ',' Seguros distintos de los seguros de vida ',' Reaseguros ',' Fondos de pensiones ',' Administración de mercados financieros ',' Actividades de intermediación en operaciones con valores y otros activos ',' Otras actividades auxiliares a los servicios financieros, excepto seguros y fondos de pensiones ',' Evaluación de riesgos y daños ',' Actividades de agentes y corredores de seguros ',' Otras actividades auxiliares a seguros y fondos de pensiones ',' Actividades de gestión de fondos ',' Compraventa de bienes inmobiliarios por cuenta propia ',' Alquiler de bienes inmobiliarios por cuenta propia ',' Agentes de la propiedad inmobiliaria ',' Gestión y administración de la propiedad inmobiliaria ',' Actividades jurídicas ',' Actividades de contabilidad, teneduría de libros, auditoría y asesoría fiscal ',' Actividades de las sedes centrales ',' Relaciones públicas y comunicación ',' Otras actividades de consultoría de gestión empresarial ',' Servicios técnicos de arquitectura ',' Servicios técnicos de ingeniería y otras actividades relacionadas con el asesoramiento técnico ',' Ensayos y análisis técnicos ',' Investigación y desarrollo experimental en biotecnología ',' Otra investigación y desarrollo experimental en ciencias naturales y técnicas ',' Investigación y desarrollo experimental en ciencias sociales y humanidades ',' Agencias de publicidad ',' Servicios de representación de medios de comunicación ',' Estudio de mercado y realización de encuestas de opinión pública ',' Actividades de diseño especializado ',' Actividades de fotografía ',' Actividades de traducción e interpretación ',' Otras actividades profesionales, científicas y técnicas n.c.o.p. ',' Actividades veterinarias ',' Alquiler de automóviles y vehículos de motor ligeros ',' Alquiler de camiones ',' Alquiler de artículos de ocio y deportivos ',' Alquiler de cintas de vídeo y discos ',' Alquiler de otros efectos personales y artículos de uso doméstico ',' Alquiler de maquinaria y equipo de uso agrícola ',' Alquiler de maquinaria y equipo para la construcción e ingeniería civil ',' Alquiler de maquinaria y equipo de oficina, incluidos ordenadores ',' Alquiler de medios de navegación ',' Alquiler de medios de transporte aéreo ',' Alquiler de otra maquinaria, equipos y bienes tangibles n.c.o.p. ',' Arrendamiento de la propiedad intelectual y productos similares, excepto trabajos protegidos por los derechos de autor ',' Actividades de las agencias de colocación ',' Actividades de las empresas de trabajo temporal ',' Otra provisión de recursos humanos ',' Actividades de las agencias de viajes ',' Actividades de los operadores turísticos ',' Otros servicios de reservas y actividades relacionadas con los mismos ',' Actividades de seguridad privada ',' Servicios de sistemas de seguridad ',' Actividades de investigación ',' Servicios integrales a edificios e instalaciones ',' Limpieza general de edificios ',' Otras actividades de limpieza industrial y de edificios ',' Otras actividades de limpieza ',' Actividades de jardinería ',' Servicios administrativos combinados ',' Actividades de fotocopiado, preparación de documentos y otras actividades especializadas de oficina ',' Actividades de los centros de llamadas ',' Organización de convenciones y ferias de muestras ',' Actividades de las agencias de cobros y de información comercial ',' Actividades de envasado y empaquetado ',' Otras actividades de apoyo a las empresas n.c.o.p. ',' Actividades generales de la Administración Pública ',' Regulación de las actividades sanitarias, educativas y culturales y otros servicios sociales, excepto Seguridad Social ',' Regulación de la actividad económica y contribución a su mayor eficiencia ',' Asuntos exteriores ',' Defensa ',' Justicia ',' Orden público y seguridad ',' Protección civil ',' Seguridad Social obligatoria ',' Educación preprimaria ',' Educación primaria ',' Educación secundaria general ',' Educación secundaria técnica y profesional ',' Educación postsecundaria no terciaria ',' Educación universitaria ',' Educación terciaria no universitaria ',' Educación deportiva y recreativa ',' Educación cultural ',' Actividades de las escuelas de conducción y pilotaje ',' Otra educación n.c.o.p. ',' Actividades auxiliares a la educación ',' Actividades hospitalarias ',' Actividades de medicina general ',' Actividades de medicina especializada ',' Actividades odontológicas ',' Otras actividades sanitarias ',' Asistencia en establecimientos residenciales con cuidados sanitarios ',' Asistencia en establecimientos residenciales para personas con discapacidad intelectual, enfermedad mental y drogodependencia ',' Asistencia en establecimientos residenciales para personas mayores ',' Asistencia en establecimientos residenciales para personas con discapacidad física ',' Otras actividades de asistencia en establecimientos residenciales ',' Actividades de servicios sociales sin alojamiento para personas mayores ',' Actividades de servicios sociales sin alojamiento para personas con discapacidad ',' Actividades de cuidado diurno de niños ',' Otros actividades de servicios sociales sin alojamiento n.c.o.p. ',' Artes escénicas ',' Actividades auxiliares a las artes escénicas ',' Creación artística y literaria ',' Gestión de salas de espectáculos ',' Actividades de museos ',' Gestión de lugares y edificios históricos ',' Actividades de los jardines botánicos, parques zoológicos y reservas naturales ',' Actividades de bibliotecas ',' Actividades de archivos ',' Actividades de juegos de azar y apuestas ',' Gestión de instalaciones deportivas ',' Actividades de los clubes deportivos ',' Actividades de los gimnasios ',' Otras actividades deportivas ',' Actividades de los parques de atracciones y los parques temáticos ',' Otras actividades recreativas y de entretenimiento ',' Actividades de organizaciones empresariales y patronales ',' Actividades de organizaciones profesionales ',' Actividades sindicales ',' Actividades de organizaciones religiosas ',' Actividades de organizaciones políticas ',' Otras actividades asociativas n.c.o.p. ',' Reparación de ordenadores y equipos periféricos ',' Reparación de equipos de comunicación ',' Reparación de aparatos electrónicos de audio y vídeo de uso doméstico ',' Reparación de aparatos electrodomésticos y de equipos para el hogar y el jardín ',' Reparación de calzado y artículos de cuero ',' Reparación de muebles y artículos de menaje ',' Reparación de relojes y joyería ',' Reparación de otros efectos personales y artículos de uso doméstico ',' Lavado y limpieza de prendas textiles y de piel ',' Peluquería y otros tratamientos de belleza ',' Pompas fúnebres y actividades relacionadas ',' Actividades de mantenimiento físico ',' Otras servicios personales n.c.o.p. ',' Actividades de los hogares como empleadores de personal doméstico ',' Actividades de los hogares como productores de bienes para uso propio ',' Actividades de los hogares como productores de servicios para uso propio ',' Actividades de organizaciones y organismos extraterritoriales', 
		);
		for($i=0;$i<count($nombres);$i++){
			$actividades[$valores[$i]] = $nombres[$i];
		}
		return $actividades;
}

function imprimeDevoluciones($comercial){

	conexionBD();

	$consulta=consultaBD("SELECT codigo, referencia, fechaVencimiento, cobrada, devuelta, codigoCliente, coste, concepto, firma FROM facturacion WHERE cobrada = 'NO' AND devuelta = 'SI' AND codigoUsuario = '".$comercial."';");
	while($datos=mysql_fetch_assoc($consulta)){
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		if($datos['concepto']=='14'){ 
			if($datos['firma']!='3'){
				$letra="F";
			}else{
				$letra="S";
			}
		}else{
			if($datos['firma']!='3'){
				$letra="C";
			}else{
				$letra="S";
			}
		}
		echo "
		<tr>
			<td> ".$letra."-".$datos['referencia']." </td>
        	<td> ".$cliente['empresa']." </td>
        	<td> ".formateaNumero($datos['coste'])." € </td>
        	<td> ".formateaFechaWeb($datos['fechaVencimiento'])." </td>
        </tr>";
	}
	cierraBD();
}

function imprimeRanking($mes='',$anio=''){
	conexionBD();
	$consulta=consultaBD("SELECT COUNT(ventas.codigo) AS numeros, SUM(ventas.precio) AS total, CONCAT(nombre,' ',apellidos) AS comercial, codigoUsuario FROM ventas INNER JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo WHERE fecha >= '".$anio."-".$mes."-1' AND fecha <= '".$anio."-".$mes."-31' GROUP BY codigoUsuario ORDER BY total DESC");

	while($datos=mysql_fetch_assoc($consulta)){
		$nuevas = consultaBD("SELECT COUNT(ventas.codigo) AS numeros, SUM(ventas.precio) AS total, CONCAT(nombre,' ',apellidos) AS comercial FROM ventas INNER JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo WHERE fecha >= '".$anio."-".$mes."-1' AND fecha <= '".$anio."-".$mes."-31' AND codigoUsuario=".$datos['codigoUsuario']." AND tipoVentaCarteraNueva LIKE 'NUEVA';",false,true);
		$cartera = consultaBD("SELECT COUNT(ventas.codigo) AS numeros, SUM(ventas.precio) AS total, CONCAT(nombre,' ',apellidos) AS comercial FROM ventas INNER JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo WHERE fecha >= '".$anio."-".$mes."-1' AND fecha <= '".$anio."-".$mes."-31' AND codigoUsuario=".$datos['codigoUsuario']." AND tipoVentaCarteraNueva LIKE 'CARTERA';",false,true);
		$nuevastotal = $nuevas['total'] != '' ? formateaNumero($nuevas['total']) : '0';
		$carteratotal = $cartera['total'] != '' ? formateaNumero($cartera['total']) : '0';
		$total = $datos['total'] != '' ? formateaNumero($datos['total']) : '0';
		echo "
		<tr>
			<td> ".$datos['comercial']." </td>
			<td> ".$nuevas['numeros']." </td>
        	<td> ".$nuevastotal." € </td>
        	<td> ".$cartera['numeros']." </td>
        	<td> ".$carteratotal." € </td>
        	<td> ".$datos['numeros']." </td>
        	<td> ".$total." € </td>
        </tr>";
	}

	cierraBD();
}

function actualizaFechaFormacion(){
	$codigoAlumno = $_POST['codigo'];
	$codigoCurso = $_POST['codigoCurso'];
	$fecha = date('Y-m-d');
	$hora = date('H:i');
	$sql = "UPDATE alumnos_registrados_cursos SET llamadaBienvenida='SI', fechaBienvenida='".$fecha."', horaBienvenida='".$hora."' WHERE codigoAlumno=".$codigoAlumno." AND codigoCurso=".$codigoCurso;
	$res=consultaBD($sql,true);
}

function actualizaResolucion(){
	$codigoAlumno = $_POST['codigo'];
	$codigoCurso = $_POST['codigoCurso'];
	$resolucion = $_POST['resolucion'];
	$fecha = date('Y-m-d');
	$hora = date('H:i');
	$sql = "UPDATE alumnos_registrados_cursos SET resolucion='".$resolucion."' WHERE codigoAlumno=".$codigoAlumno." AND codigoCurso=".$codigoCurso;
	$res=consultaBD($sql,true);
	if($res){
		$alumno=datosRegistro('alumnos',$codigoAlumno);
		$venta=datosRegistro('ventas',$alumno['codigoVenta']);
		$ventaRelacionada=datosRegistro('ventas_relacionadas',$venta['codigo'],'codigoFormacion');
		$oferta=datosRegistro('ofertas',$ventaRelacionada['codigoVenta'],'codigoOferta');
		$trabajo=datosRegistro('trabajos',$oferta['codigo'],'codigoOferta');
		if($trabajo['formacion']!='NO'){
			if($resolucion=='SI'){
				$resolucion='FINALIZADO';
			} else {
				$resolucion='HECHO';
			}
			$sql = "UPDATE trabajos SET formacion='".$resolucion."' WHERE codigo=".$trabajo['codigo'];
			$res=consultaBD($sql,true);
		}
	}
}

/**
* Función campoSelectHTML: similar a campoSelect(), con la particularidad de que los textos para los option son HTML 
* (debe usarse en combinación con la librería JS bootstrap-selectpicker).
* @param $nombreCampo string. El atributo name del select
* @param $texto string. El texto del label que acompaña al campo.
* @param $nombres array. Contiene el código HTML que será usado como texto de los option
* @param $valores array. Contiene los valores de los option, tiendo que ser el número de íncides igual al de $nombres.
* @param $busqueda string. Cadena que se coloca como atributo para determinar si el select tendrá o no un campo de búsqueda. A diferencia de campoSelect(), por defecto no lo tendrá.
* @param $tipo int. 0 = campo en control-group (formulario normal). 1 = campo encerrado entre <td></td>. 2 = campo sin envoltura (directamente la etiqueta select)
* @return void
*/
function campoSelectHTML($nombreCampo,$texto,$nombres,$valores,$valor=false,$clase='selectpicker span3',$busqueda="",$tipo=0){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	if($tipo==0){
		echo "
		<div class='control-group'>                     
			<label class='control-label' for='$nombreCampo'>$texto:</label>
			<div class='controls'>";
	}
	elseif($tipo==1){
		echo "<td>";
	}

	echo "<select name='$nombreCampo' id='$nombreCampo' class='$clase' $busqueda>";
		
	for($i=0;$i<count($nombres);$i++){
		echo "<option value='".$valores[$i]."'";

		if($valor!=false && $valor==$valores[$i]){
			echo " selected='selected'";
		}

		echo " data-content=\"".$nombres[$i]."\"></option>";
	}
		
	echo "</select>";

	if($tipo==0){
		echo "
			</div> <!-- /controls -->       
		</div> <!-- /control-group -->";
	}
	elseif($tipo==1){
		echo "</td>";
	}
}

function campoSelectHTMLFormacion($nombreCampo,$texto,$nombres,$valores,$valor=false,$clase='selectpicker span3',$busqueda=""){
	$codigo=$valor['codigo'];
	$codigoCurso=$valor['codigoCurso'];
	$valor=compruebaValorCampo($valor,$nombreCampo);
	echo "<td>";

	echo "<select name='$nombreCampo' id='$nombreCampo' class='$clase' codigo='$codigo' codigoCurso='$codigoCurso' $busqueda>";
		
	for($i=0;$i<count($nombres);$i++){
		echo "<option value='".$valores[$i]."'";

		if($valor!=false && $valor==$valores[$i]){
			echo " selected='selected'";
		}

		echo " data-content=\"".$nombres[$i]."\"></option>";
	}
		
	echo "</select>";

	echo "</td>";
}

function compruebaFechasObjetivos(){
	conexionBD();
		$objetivos=consultaBD("SELECT * FROM objetivos LIMIT 1",false,true);
		$mes=date('m');
		$anio=date('Y');
		$ultimoDia=date("d",(mktime(0,0,0,$mes+1,1,$anio)-1));
		$mes=date('m');
		$fechaInicio=explode('-', $objetivos['fechaInicio']);
		if($mes != $fechaInicio['1']){
			$sql = 'UPDATE objetivos SET fechaInicio = "'.$anio.'-'.$mes.'-01", fechaFin="'.$anio.'-'.$mes.'-'.$ultimoDia.'";';
			$res=consultaBD($sql);
		}
	cierraBD();
}

function campoSelectTieneFiltro($nombreCampo,$texto){
    campoSelect($nombreCampo,$texto,array('','Si','No'),array('','NOTNUL','ISNUL'),false,'selectpicker span1 show-tick');//Uso "NUL" porque datatables quita la cadena "NULL" de los valores
}

function botonVinculadas($codigoEmpresa=0){
	echo "<td class='centro'>
				<div class='margenPeque'>
					<a href='detallesPosibleCliente.php?codigo=".$codigoEmpresa."' class='btn btn-primary'><i class='icon-zoom-in'></i> Ver datos</i></a>
					<a href='creaTarea.php?codigo0=".$codigoEmpresa."' class='btn btn-danger'><i class='icon-remove'></i> Desvincular</i></a>
				</div>
			</td>";
}

function selectEmpresasVinculadas($codigo=false){
	conexionBD();
	$consulta=consultaBD("SELECT codigo, empresa AS texto FROM clientes WHERE empresa <> '' AND baja='NO' ORDER BY empresa;");//Añadido el administrador en la actualización del 20/10/2014
	cierraBD();
	if($codigo!=false){
		$empresas=obtieneEmpresasVinculadas($codigo);
	}

	echo '
	<div class="control-group">                     
	    <label class="control-label" for="estado">Empresas vinculadas:</label>
	    <div class="controls">
			<select name="empresasVinculadas[]" class="selectpicker span3 show-tick" data-live-search="true" multiple data-selected-text-format="count" multiple title="Seleccione">';
	while($datos=mysql_fetch_assoc($consulta)){
		echo '<option value="'.$datos['codigo'].'"';
		
		if($codigo!=false && in_array($datos['codigo'],$empresas)){
			echo ' selected="selected"';
		}

		echo '>'.$datos['texto'].'</option>';
	}
	echo '</select>
		</div> <!-- /controls -->       
    </div> <!-- /control-group -->';
}

function obtieneEmpresasVinculadas($codigo,$texto=false){
	conexionBD();
	$consulta=consultaBD('SELECT * FROM empresas_vinculadas WHERE empresa1='.$codigo.' OR empresa2='.$codigo);
	cierraBD();
	$listado=array();
	while($datos=mysql_fetch_assoc($consulta)){
		if($datos['empresa1']!=$codigo){
			$empresa=$datos['empresa1'];
		} else {
			$empresa=$datos['empresa2'];
		}
		if($texto){
			$empresa=datosRegistro('clientes',$empresa);
			$url='detallesCuenta.php';
			if($empresa['baja'] == 'SI'){
				$url='detallesClienteBaja.php';
			} elseif ($empresa['activo'] == 'NO'){
				$url='detallesPosibleCliente';
			}
			$empresa="<a href='".$url."?codigo=".$empresa['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i></i></a> ".$empresa['empresa'];
		}
		array_push($listado,$empresa);
	}
	return $listado;
}

function campoSelectConsultaConBlanco($nombreCampo,$texto,$consulta,$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$disabled='',$tipo=0){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	if($disabled){
		$disabled="disabled='disabled'";
	}

	if($tipo==0){
		echo "
		<div class='control-group'>                     
			<label class='control-label' for='$nombreCampo'>$texto:</label>
			<div class='controls'>";
	}
	elseif($tipo==1){
		echo "<td>";
	}

	echo "<select name='$nombreCampo' id='$nombreCampo' class='$clase' $busqueda $disabled>";
		echo "<option value='NULL'> </option>";
		$consulta=consultaBD($consulta,true);
		$datos=mysql_fetch_assoc($consulta);
		while($datos!=false){
			echo "<option value='".$datos['codigo']."'";

			if($valor!=false && $valor==$datos['codigo']){
				echo " selected='selected'";
			}

			echo ">".$datos['texto']."</option>";
			$datos=mysql_fetch_assoc($consulta);
		}
		
	echo "</select>";

	if($tipo==0){
		echo "
			</div> <!-- /controls -->       
		</div> <!-- /control-group -->";
	}
	elseif($tipo==1){
		echo "</td>";
	}
}

function eliminaTareas($tabla){
	$res=true;
	$datos=arrayFormulario();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$preventa=datosRegistro('preventas',$datos['codigo'.$i],'codigoTarea');
		if($preventa){
			if($preventa['aceptado']='SI'){
				$ventas=consultaBD("SELECT * FROM ventas_preventas WHERE codigoPreventa=".$preventa['codigo'],true);
				while($venta=mysql_fetch_assoc($ventas)){
					$oferta=datosRegistro('ofertas',$venta['codigoVenta'],'codigoOferta');
					$eliminar=consultaBD('DELETE FROM trabajos WHERE codigoOferta='.$oferta['codigo'],true);
					$eliminar=consultaBD('DELETE FROM ofertas WHERE codigo='.$oferta['codigo'],true);
					$eliminar=consultaBD('DELETE FROM facturacion WHERE codigoVenta='.$venta['codigoVenta'],true);
					$eliminar=consultaBD('DELETE FROM ventas WHERE codigo='.$venta['codigoVenta'],true);
				}
			}
			$consulta=consultaBD("DELETE FROM preventas WHERE codigo='".$preventa['codigo']."';",true);
		}
		$consulta=consultaBD("DELETE FROM $tabla WHERE codigo='".$datos['codigo'.$i]."';",true);
		if(!$consulta){
			$res=false;
			echo mysql_error();
		}
	}

	return $res;
}

function actualizaReferencias(){
	$anio=date('Y');
    if(date("Y-m-d") >= $anio.'-12-21' && date("Y-m-d") < ($anio+1).'-01-01'){
        $anio++;
    }
  	//F
  	$consulta=consultaBD("SELECT MAX(referencia) AS referencia FROM facturacion WHERE concepto='14' AND firma!='3' AND anio = '".$anio."';",true);
  	$referencia=mysql_fetch_assoc($consulta);
  	if($referencia['referencia']==''){
  		$referencia['referencia']=0;
  	}
  	$referenciaNueva=$referencia['referencia']+1;
  
  	$consulta=consultaBD("SELECT MAX(referencia) AS referencia FROM facturacion WHERE concepto!='14' AND anio = '".$anio."' ORDER BY referencia DESC LIMIT 1;",true);
  	$referencia=mysql_fetch_assoc($consulta);
  	if($referencia['referencia']==''){
  		$referencia['referencia']=0;
  	}
  	$referenciaNuevaConsultoria=$referencia['referencia']+1;
  
  	//S
  	$consulta=consultaBD("SELECT MAX(referencia) AS referencia FROM facturacion WHERE concepto='14' AND firma='3' AND anio = '".$anio."' ORDER BY referencia DESC LIMIT 1;",true);
  	$referencia=mysql_fetch_assoc($consulta);
  	if($referencia['referencia']==''){
  		$referencia['referencia']=0;
  	}
  	$referenciaNuevaFirmaTres=$referencia['referencia']+1;

  	$consulta=consultaBD("SELECT MAX(referencia) AS referencia FROM facturacion WHERE firma='4' AND anio = '".$anio."' ORDER BY referencia DESC LIMIT 1;",true);
  $referencia=mysql_fetch_assoc($consulta);
  if($referencia['referencia']==''){
  $referencia['referencia']=9999;
  }
  $referenciaEducativa=$referencia['referencia']+1;
							  
	if($referenciaNueva<10){
		$referenciaNueva='0000'.$referenciaNueva;
	 }elseif($referenciaNueva<100){	
		$referenciaNueva='000'.$referenciaNueva;
	}elseif($referenciaNueva<1000){	
		$referenciaNueva='00'.$referenciaNueva;
	}elseif($referenciaNueva<10000){	
		$referenciaNueva='0'.$referenciaNueva;
	}
							  
	if($referenciaNuevaFirmaTres<10){
		$referenciaNuevaFirmaTres='0000'.$referenciaNuevaFirmaTres;
	}elseif($referenciaNuevaFirmaTres<100){	
		$referenciaNuevaFirmaTres='000'.$referenciaNuevaFirmaTres;
	}elseif($referenciaNuevaFirmaTres<1000){	
		$referenciaNuevaFirmaTres='00'.$referenciaNuevaFirmaTres;
	}elseif($referenciaNuevaFirmaTres<10000){	
		$referenciaNuevaFirmaTres='0'.$referenciaNuevaFirmaTres;
	}


	$res['referenciaNueva']=$referenciaNueva;
	$res['referenciaNuevaConsultoria']=$referenciaNuevaConsultoria;
	$res['referenciaNuevaFirmaTres']=$referenciaNuevaFirmaTres;
	$res['referenciaEducativa']=$referenciaEducativa;

	$codigo=$_POST['codigo'];
	$ventas=consultaBD('SELECT codigo, fecha, precio FROM ventas WHERE codigoCliente='.$codigo.' AND concepto=14',true);
	$res['select']="<option value='NULL'></option>";
	while($venta=mysql_fetch_assoc($ventas)){
		$res['select'].="<option value='".$venta['codigo']."'>".formateaFechaWeb($venta['fecha']).' - '.$venta['precio'].' €'."</option>";
	}

	echo json_encode($res);
}


function campoSelectClientePreventa($codigoCliente,$empresa){
	if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='ATENCION'){
		$where=compruebaPerfilParaWhere();
		campoSelectConsulta('codigoCliente','Cliente',"SELECT codigo, empresa AS texto FROM clientes $where ORDER BY empresa;",$codigoCliente);
	}
	else{
		campoOculto($codigoCliente,'codigoCliente');
		campoTexto('empresa','Cliente',$empresa,'input-large',true);
	}
}


function actualizaTareaPreventa(){
	$datos=arrayFormulario();
	extract($datos);

	return consultaBD("UPDATE tareas SET fechaInicio='$fecha' WHERE codigo='$codigoTarea';",true);
}

function modificarVentas(){
	$res = true;
	$ventas=consultaBD('SELECT * FROM ventas_preventas INNER JOIN ventas ON ventas_preventas.codigoVenta=ventas.codigo WHERE codigoPreventa='.$_POST['codigo'],true);
	$productos=array(14=>'formacionPrecio',18=>'lssiPrecio',19=>'alergenosPrecio',20=>'prlPrecio',21=>'plataformaWebPrecio',22=>'auditoriaLopdPrecio',23=>'consultoriaLopdPrecio',24=>'auditoriaPrlPrecio',25=>'webLegalizadaPrecio',26=>'dominioCorreoPrecio',27=>'dominioPrecio',28=>'ecommercePrecio');
	while($venta=mysql_fetch_assoc($ventas)){
		$sql='UPDATE ventas SET precio="'.$_POST[$productos[$venta['concepto']]].'" WHERE codigo='.$venta['codigoVenta'];
		$res=consultaBD($sql,true);
	}
	return $res;
}

function imprimeHistorial(){

	$consulta=consultaBD("SELECT * FROM historial;",true);

	while($datos=mysql_fetch_assoc($consulta)){
		$partes=explode(' ', $datos['consulta']);
		if($partes[0]=='UPDATE'){
			$tabla=$partes[1];
			$accion = 'Modificación del registro <b>'.array_pop($partes).'</b>';
		} else if ($partes[0]=='DELETE'){
			$tabla=$partes[2];
			$accion = 'Eliminación del registro <b>'.array_pop($partes).'</b>';
		} else {
			$tabla=$partes[2];
			$accion= 'Inserción de un registro';
		}
		$usuario=datosRegistro('usuarios',$datos['codigoUsuario']);
		echo "
		<tr>
        	<td> ".formateaFechaWeb($datos['fecha'])."</td>
        	<td> ".$datos['hora']." </td>
        	<td> ".$partes[0]." </td>
        	<td> ".$tabla." </td>
        	<td> ".$accion." </td>
        	<td> ".$usuario['nombre']." ".$usuario['apellidos']."</td>
        	<td class='centro'>
        		<a href='detallesHistorial.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-zoom-in'></i> Detalles</i></a>
			</td>
    	</tr>";
	}
}

function exportarExcelDevoluciones($objPHPExcel){
	$where='';
	if($_GET['motivo']!='todos'){
		$where='WHERE motivo = "'.$_GET['motivo'].'"';
	}
	if($_GET['resolucion']!='todas'){
		if($where==''){
			$where='WHERE devoluciones_facturas.resolucionFactura = "'.$_GET['resolucion'].'"';
		} else {
			$where.=' AND devoluciones_facturas.resolucionFactura = "'.$_GET['resolucion'].'"';
		}	
	}
	
	if($_GET['anio']=='0'){
		$fecha=date('Y');
		$whereFecha=' OR fecha LIKE "0000-00-00"';
	}else{
		$fecha=$_GET['anio'];
		$whereFecha='';
	}
	
	if($_GET['mes']=='0'){
		$fecha=$fecha.'-%';
	}else{
		$fecha=$fecha.'-'.$_GET['mes'].'-%';
	}
	

	if($where==''){
		$where.="WHERE fecha LIKE '".$fecha."'".$whereFecha;
	} else {
		$where.=" AND fecha LIKE '".$fecha."'".$whereFecha;
	}	

	$consulta=consultaBD("SELECT facturacion.codigo,facturacion.referencia,facturacion.coste,facturacion.codigoCliente, devoluciones_facturas.fecha, devoluciones_facturas.motivo, devoluciones_facturas.resolucionFactura, facturacion.firma, facturacion.concepto, facturacion.anio, facturacion.fechaVencimiento FROM facturacion INNER JOIN devoluciones_facturas ON facturacion.codigo=devoluciones_facturas.codigoFactura $where GROUP BY facturacion.codigo ORDER BY facturacion.referencia,fecha DESC;",true);

	$i=4;
	$referenciaAnt='';
	$arrayDevoluciones=array('F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','V','W','X','Y','Z','AA');
	$final=0;
	while($item=mysql_fetch_assoc($consulta)){
		$cliente=datosRegistro('clientes',$item['codigoCliente']);
		if($item['concepto']=='14'){ 
			if($item['firma']!='3'){
				$letra="F";
			}else{
				$letra="S";
			}
		}else{
			if($item['firma']!='3'){
				$letra="C";
			}else{
				$letra="S";
			}
		}
		$referencia=$letra."-".$item['referencia'].'/'.substr($item['anio'], 2);
		if($referencia==$referenciaAnt){
			$referencia='';
		} else {
			$referenciaAnt=$referencia;
		}
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($referencia);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($cliente['empresa']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($item['fechaVencimiento']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($item['coste'].' €');
		$devoluciones=consultaBD('SELECT * FROM devoluciones_facturas WHERE codigoFactura='.$item['codigo'].' ORDER BY fecha ASC',true);
		$j=0;
		while($devolucion=mysql_fetch_assoc($devoluciones)){
			$fecha=formateaFechaWeb($devolucion['fecha']);
			$motivo=datosRegistro('motivos',$devolucion['motivo']);
			$objPHPExcel->getActiveSheet()->getCell($arrayDevoluciones[$j].$i)->setValue($fecha);
			$j++;
			$objPHPExcel->getActiveSheet()->getCell($arrayDevoluciones[$j].$i)->setValue($motivo['motivo']);
			$j++;
			$objPHPExcel->getActiveSheet()->getCell($arrayDevoluciones[$j].$i)->setValue($item['resolucionFactura']);
			if($j>$final){
				$final=$j;
			}
			$j++;
		}
		$i++;
	}
	foreach(range('B',$arrayDevoluciones[$final]) as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
	}

	$styleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF')
    ),
    'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '000000')
    ));
    $objPHPExcel->getActiveSheet()->getStyle('B3:'.$arrayDevoluciones[$final].'3')->applyFromArray($styleArray);
}