<?php
  $seccionActiva=5;
  include_once('cabecera.php');
  
  if(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaCurso();
  }
  elseif(isset($_POST['eliminaAlumno']) && $_POST['eliminaAlumno']=='SI'){
    $res=eliminaDatos('alumnos');
  }

  $estadisticas=creaEstadisticasCursos("AND cursos.fechaInicio LIKE'".$_POST['anio']."-%'");
  $estadisticasAlumnos=creaEstadisticasAlumnos("AND cursos.fechaInicio LIKE'".$_POST['anio']."-%'");
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para formación</h6>

                   <div id="big_stats" class="cf">

                     <div class="stat"> <i class="icon-book"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Cursos registrados</div>
					 <div class="stat"> <i class="icon-pencil"></i> <span class="value"><?php echo $estadisticasAlumnos['total']?></span> <br>Alumnos registrados en cursos</div>
                      <!-- .stat -->

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span12 -->
		
		<?php  if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='CONSULTORIA'){ ?>
		
        <div class="span12">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de formación</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<h2 class="arribaFormacion">Cursos</h2>
                <a href="formacion.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
                <br>
                <?php 
                compruebaOpcionExcel('4');
                compruebaOpcionEliminar();
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		
		<?php } ?>

      <div class="span12">
        
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Cursos registrados</h3>
			  <div class="pull-right">
               <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
             </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php filtroCursos(); ?>
              <table class="table table-striped table-bordered datatable" id="tablaCursos">
                <thead>
                  <tr>
					<th> Num. Acción </th>
					<th> Grupo </th>
                    <th> Acción formativa </th>
                    <th> Tutor </th>
                    <th> Fecha de inicio </th>
                    <th> Fecha de finalización </th>
					<th> Formación </th>
                    <th class="centro"> </th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					//imprimeCursos();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		 
		<div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Alumnos registrados en cursos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable" id="tablaAlumnos">
                <thead>
                  <tr>
					<th> Acción formativa </th>
					<th> Empresa </th>
					<th> Alumno </th>
					<th> eMail </th>
					<th> Curso </th>
					<th> Fecha inicio </th>
					<th> Fecha fin </th>
					<th> Teléfono </th>
					<th> Llamada de inicio </th>
					<th> Última llamada </th>
					<th> Plataforma </th>
					<th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                    <!--th> Nombre </th>
                    <th> Teléfono </th>
                    <th> eMail </th>
                    <th> Cliente </th>
                    <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th-->
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeAlumnosNuevo("AND cursos.FechaInicio LIKE'".$_POST['anio']."-%'");
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<script type="text/javascript" src="js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  
	$(document).ready(function() {
		$('.cajaFiltros select').selectpicker();
		oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaCursos');
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');realizaBusquedaFiltrada('#cajaFiltros','#tablaCursos');});
	});
  
  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un registro.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('formacion.php',valoresChecks,'post');
    }

  });
  
   $('#eliminarAlumno').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un registro.');
    }
    else{
      valoresChecks['eliminaAlumno']='SI';
      creaFormulario('formacion.php',valoresChecks,'post');
    }

  });
  
  $('#email').click(function(){
      var valoresChecks=recorreChecks();
      creaFormulario('enviarEmail.php?seccion=5',valoresChecks,'post');
    });
	
	var tabla=$('#tablaCursos').DataTable({
	  'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoCursos.php?action=getMembersAjx&archivo=<?php echo $_POST['anio']; ?>',
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          $('td:eq(7)', nRow).addClass( "centro" );
       },
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });
	
  var tabla=$('#tablaAlumnos').DataTable({
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave":true,
		"iDisplayLength":25,
		"oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
		  'sZeroRecords':'No se han encontrado coincidencias'
    }});

	

</script>
