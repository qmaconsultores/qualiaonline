SELECT 
		CONCAT('C-',facturacion.referencia) AS Referencia,
		CONVERT(CAST(CONVERT(clientes.empresa USING latin1) AS binary) USING utf8) AS Cliente,
		CONVERT(CAST(CONVERT(CONCAT(usuarios.nombre,' ',usuarios.apellidos) USING latin1) AS binary) USING utf8) AS Comercial,
		CONVERT(CAST(CONVERT(productos.nombreProducto USING latin1) AS binary) USING utf8) AS Concepto,
		tipoVentaCarteraNueva AS 'Cartera/Nueva',
		DATE_FORMAT(fecha,'%d/%m/%Y') AS Emision,
		DATE_FORMAT(fechaVencimiento,'%d/%m/%Y') AS Vencimiento,
		coste AS Importe		 

FROM facturacion LEFT JOIN clientes ON clientes.codigo=facturacion.codigoCliente 
LEFT JOIN productos ON facturacion.concepto=productos.codigo 
LEFT JOIN ventas ON facturacion.codigoVenta=ventas.codigo 
LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo 

WHERE firma=1 AND fechaEmision>='2015-03-01' AND fechaEmision<='2015-12-31' 

ORDER BY referencia;