<?php
  $seccionActiva=16;
  include_once('cabecera.php');
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12 margenAb">
		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-columns"></i> 
              <h3>Esquema de planificación <i>To Do, Doing, Done</i></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th> Pendiente </th>
                    <th> En desarrollo </th>
                    <th> Finalizado </th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeTodo();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  
        <br />
        <center><a href='software.php'>Volver</a></center>


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>