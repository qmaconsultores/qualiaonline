<?php
  $seccionActiva=$_GET['seccion'];
  include_once('cabecera.php');

  if($seccionActiva==3 || $seccionActiva==0){
    $destinatarios=obtieneMailsClientesTareas();
  }
  elseif($seccionActiva==4){
    $destinatarios=obtieneMailsClientesIncidencias();
  }
  elseif($seccionActiva==8){
    $destinatarios=obtieneMailsUsuarios();
  }
  elseif($seccionActiva==5){
    $destinatarios=obtieneMailsAlumnos();
  }
  else{
    $destinatarios=obtieneMailsClientes();
  }
  
  if(isset($_GET['codigoFactura'])){
	$_SESSION['codigoFactura']=$_GET['codigoFactura'];
	$datosFactura=datosRegistro('facturacion',$_GET['codigoFactura']);
	$datosEmpresa=consultaBD("SELECT clientes.* FROM facturacion 
	INNER JOIN clientes ON facturacion.codigoCliente=clientes.codigo WHERE facturacion.codigo='".$_GET['codigoFactura']."';",true);
	$datosEmpresa=mysql_fetch_assoc($datosEmpresa);
  }
  
  if(isset($_GET['codigoEnvio'])){
	$datosCliente=datosRegistro('clientes',$_GET['codigoEnvio']);
	$destinatarios=$datosCliente['mail'];
  }

  $destinoFormu=destinoFormulario($_GET['seccion']);
  $firma=firmaUsuario();
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-envelope"></i>
              <h3>Nuevo mensaje de correo electrónico</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="<?php echo $destinoFormu; ?>" method="post" enctype="multipart/form-data">
                  <fieldset>
                    
                    <div class="control-group">                     
                      <label class="control-label" for="detinatarios">Destinatario/s:</label>
                      <div class="controls">
                        <input type="text" class="span5" id="destinatarios" name="destinatarios" value="<?php echo $destinatarios; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="ocultos">CCO:</label>
                      <div class="controls">
                        <input type="text" class="span5" id="ocultos" name="ocultos" value="<?php echo $destinatarios; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoRadio('oculto','En oculto','NO');
						campoSelect('colectivos','Colectivos',array('Ninguno','Posibles clientes','Cuentas','Alumnos','Colaboradores','Tutores','Usuarios'),array('ninguno','posibles','cuentas','alumnos','colaboradores','tutores','usuarios'),false,'selectpicker span4 show-tick');
					?>
				
					
					<div class="control-group" id='parent-div'>                     
                      <label class="control-label" for="ocultos">Clientes:</label>
                      <div class="controls">
                        <?php
							selectClientesMultiple();
						?>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <?php
					$posibles=correos('clientes WHERE activo="NO"');
					$cuentas=correos('clientes WHERE activo="SI"');
					$alumnos=correos('alumnos');
					$colaboradores=correos('colaboradores');
					$tutores=correos('tutores','email');
					$usuarios=correos('usuarios','email');
					
					campoOculto($posibles,'mailsPosibles');
					campoOculto($cuentas,'mailsCuentas');
					campoOculto($alumnos,'mailsAlumnos');
					campoOculto($colaboradores,'mailsColaboradores');
					campoOculto($tutores,'mailsTutores');
					campoOculto($usuarios,'mailsUsuarios');
					
					$consulta=consultaBD("SELECT codigo, mail FROM clientes;",true);
					$datosCliente=mysql_fetch_assoc($consulta);
					while(isset($datosCliente['codigo'])){
						divOculto($datosCliente['mail'],'email'.$datosCliente['codigo']);
						$datosCliente=mysql_fetch_assoc($consulta);
					}
                    ?>

                    <div class="control-group">                     
                      <label class="control-label" for="asunto">Asunto:</label>
                      <div class="controls">
                        <input type="text" class="span5" id="asunto" name="asunto">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
                     

                    <div class="control-group">                     
                      <label class="control-label" for="mensajeCorreo"></label>
                      <div class="controls">
                        <textarea name="mensajeCorreo" id="mensajeCorreo">
						<?php if(isset($_GET['codigoFactura'])){
							echo"Envío de factura <br><br> Número de factura: ".$datosFactura['referencia']."<br>Empresa: ".$datosEmpresa['empresa']."<br>Importe: ".$datosFactura['coste']." €<br>N. Cuenta Bancaria: ".$datosEmpresa['numCuenta'];
						}elseif(isset($_GET['codigoEnvio'])){
							echo "Buenos días.<br><br>A continuación les facilitamos el enlace para que puedan evaluar el grado de satisfacción con nuestra organización: <br>http://".$_SERVER['HTTP_HOST']."/grupqualia/cuestionario-de-satisfaccion/index.php?codigoCliente=".$_GET['codigoEnvio']."<br><br>Reciban un cordial saludo.";
						}?>
						<br><br><br><?php echo $firma; ?></textarea>
                      </div>
                    </div>


                    <div class="control-group">                     
                      <label class="control-label" for="adjunto">Adjunto:</label>
                      <div class="controls">
                        <input type="file" name="adjunto" id="adjunto"><div class="tip">Solo se admiten ficheros de 20 MB como máximo</div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                  
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-share-alt"></i> Enviar correo</button> 
                      <a href="<?php echo $destinoFormu; ?>" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                  
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script src="js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.es-ES.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/bootstrap-filestyle.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
	$('.selectpicker').selectpicker();
    $('#mensajeCorreo').wysihtml5({locale: "es-ES"});
    $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

	
	$('#tablaClientes input[type=checkbox]').click(function(){
		$('#destinatarios').val($('#destinatarios').val()+', '+$(this).val());
	});
	
	$('#parent-div').focusout(function(){
		var clientes='';
		$("#clientes option:selected").each(function(){
			var email=$("#email"+$(this).val()).text();
			if(email!=''){
				clientes=clientes+email+', ';
			}
		});
		if($("input[type=radio][name=oculto]:checked").val()=='SI'){
			$("#ocultos").val(clientes);
		}else{
			$("#destinatarios").val(clientes);
		}
	});
	
	$( "#colectivos" ).change(function() {
	  switch($( "#colectivos" ).val()) {
		case 'posibles':
			if($("input[type=radio][name=oculto]:checked").val()=='SI'){
				$("#ocultos").val($("#mailsPosibles").val());
			}else{
				$("#destinatarios").val($("#mailsPosibles").val());
			}
			break;
		case 'cuentas':
			if($("input[type=radio][name=oculto]:checked").val()=='SI'){
				$("#ocultos").val($("#mailsCuentas").val());
			}else{
				$("#destinatarios").val($("#mailsCuentas").val());
			}
			break;
		case 'alumnos':
			if($("input[type=radio][name=oculto]:checked").val()=='SI'){
				$("#ocultos").val($("#mailsAlumnos").val());
			}else{
				$("#destinatarios").val($("#mailsAlumnos").val());
			}
			break;
		case 'colaboradores':
			if($("input[type=radio][name=oculto]:checked").val()=='SI'){
				$("#ocultos").val($("#mailsColaboradores").val());
			}else{
				$("#destinatarios").val($("#mailsColaboradores").val());
			}
			break;
		case 'tutores':
			if($("input[type=radio][name=oculto]:checked").val()=='SI'){
				$("#ocultos").val($("#mailsTutores").val());
			}else{
				$("#destinatarios").val($("#mailsTutores").val());
			}
			break;
		case 'usuarios':
			if($("input[type=radio][name=oculto]:checked").val()=='SI'){
				$("#ocultos").val($("#mailsUsuarios").val());
			}else{
				$("#destinatarios").val($("#mailsUsuarios").val());
			}
			break;	
		default:
			if($("input[type=radio][name=oculto]:checked").val()=='SI'){
				$("#ocultos").val('');
			}else{
				$("#destinatarios").val('');
			}
		}
	});

  });
</script>