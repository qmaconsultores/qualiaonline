<?php
  $seccionActiva=7;
  include_once('cabecera.php');
  $res=false;
  
  if(isset($_POST['codigo'])){
	$res=actualizaDatos('facturacion');  
  }

  if(isset($_POST['codigoComercial']) && isset($_POST['quincena']) && isset($_POST['mes'])){
	 $_SESSION['codigoComercial']=$_POST['codigoComercial'];
	 $_SESSION['quincena']=$_POST['quincena'];
	 $_SESSION['mes']=$_POST['mes'];
   $_SESSION['anio']=$_POST['anio'];
  }
  $datosComercial=datosRegistro('usuarios',$_SESSION['codigoComercial']);
  $fechaUno='';
  $fechaDos='';
  $anio=$_POST['anio'];
  if($_SESSION['mes']=='13'){
		$anioUno=date('Y');
		$anioDos=date('Y');
		$mesUno='01';
		$mesDos='12';
		$diaInicio='01';
		$diaFin='31';
		$whereVentas='WHERE fecha LIKE"'.$anio.'-%"';
  }else{
	  $quincena=$_SESSION['quincena'];
    $anioUno=$_SESSION['anio'];
    $anioDos=$_SESSION['anio'];
    $mesUno=$_SESSION['mes'];
    $mesDos=$_SESSION['mes'];
	  $diaInicio='';
	  $diaFin='';
	  
	  switch($quincena){
		case 'una':
			$diaInicio='24';
      $diaFin='24';
		break;
		case 'dos':
      $diaInicio='09';
      $diaFin='09';
			if($_SESSION['mes']=='12'){
				$mesUno='01';
				$mesDos='01';
				$anioUno=$anioUno+1;
				$anioDos=$anioDos+1;
			}else{
				$mesUno=$mesUno+1;
				$mesDos=$mesDos+1;
			}
		break;
		case 'mes':
			$diaInicio='24';
			$diaFin='09';
			if($_SESSION['mes']=='12'){
				$mesUno='12';
				$mesDos='01';
				$anioUno=$anioUno;
				$anioDos=$anioDos+1;
			}else{
				$mesDos=$mesDos+1;
			}
		break;
	  }
	  $whereVentas='WHERE fecha LIKE"'.$anio.'-'.$_SESSION['mes'].'-%"';
  }
  $fechaUno=$anioUno.'-'.$mesUno.'-'.$diaInicio;
  $fechaDos=$anioDos.'-'.$mesDos.'-'.$diaFin;
  //$estadisticas=creaEstadisticasFacturasComercial($_SESSION['codigoComercial'], 'WHERE fechaVencimiento>="'.$fechaUno.'" AND fechaVencimiento<="'.$fechaDos.'" AND cobrada="SI"');
  $estadisticas=creaEstadisticasFacturasComercial($_SESSION['codigoComercial'], 'WHERE facturas_vto.fecha >= "'.$fechaUno.'" AND facturas_vto.fecha <= "'.$fechaDos.'" AND facturas_vto.cobrada="SI"', $whereVentas);
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-tasks"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Comisiones al comercial <?php echo $datosComercial['nombre'].' '.$datosComercial['apellidos']; ?> desde el: <?php echo formateaFechaWeb($fechaUno); ?> al: <?php echo formateaFechaWeb($fechaDos); ?></h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo $estadisticas['comision']?></span> <br>Total en comisiones</div>
                    <!-- .stat -->
                    <!-- .stat -->
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
		<div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de comisiones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<!--a href="javascript:void(0);" onclick='alert("Función en desarrollo");' class="shortcut"><i class="shortcut-icon icon-download-alt"></i><span class="shortcut-label">Descargar excel</span> </a-->
				<a href="generaExcelComisionesComerciales.php?comercial=<?php echo $_SESSION['codigoComercial']; ?>&fechaUno=<?php echo $fechaUno; ?>&fechaDos=<?php echo $fechaDos; ?>" class="shortcut"><i class="shortcut-icon icon-download-alt"></i><span class="shortcut-label">Descargar excel</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
        <?php 
          mensajeResultado('codigo',$res,'comisiones');
        ?>
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Facturas emitidas durante el periodo seleccionado</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
					<th> Referencia </th>
					<th> Cliente </th>
                    <th> Concepto </th>
                    <th> Fecha de emisión </th>
					<th> Fecha de vencimiento </th>
					<th> Importe </th>
					<th> Comisión </th>
					<th> Pagado </th>
          <th> Cobrada </th>
					<th class="centro"> </th>
					<th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeFacturasComisionComercial($_SESSION['codigoComercial'], 'WHERE facturas_vto.fecha >= "'.$fechaUno.'" AND facturas_vto.fecha <= "'.$fechaDos.'" AND facturas_vto.cobrada="SI"',$estadisticas['comisiona']);

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">	
	var tabla=$('.datatable').DataTable({
		"aaSorting": [],
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave":false,
		"iDisplayLength":25,
		"oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
		  'sZeroRecords':'No se han encontrado coincidencias'
    }});

</script>