<?php
  include_once('funciones.php');
  
  $fichero=generaXMLGrupo($_GET['codigo']);

  // Definir headers
  header("Content-Type: application/xml");
  header("Content-Disposition: attachment; filename=$fichero");
  header("Content-Transfer-Encoding: binary");

  // Descargar archivo
  readfile("documentos/$fichero");

?>