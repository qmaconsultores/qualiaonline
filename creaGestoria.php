<?php
  $seccionActiva=22;
  include_once('cabecera.php');
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nueva Gestoría</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="gestorias.php" method="post">
                  <fieldset class="span5">

                    <?php
						campoTexto('nombreGestoria','Nombre');
						campoSelectConsulta('codigoCliente','Cliente',"SELECT codigo, empresa AS texto FROM clientes ORDER BY empresa;");
						campoTexto('mailGestoria','Correo electrónico');
						campoTexto('telefonoGestoria','Teléfono');
						campoOculto($_SESSION['codigoS'],'codigoUsuario');
					?>

                  </fieldset>
                  
				  <fieldset class="sinFlotar">
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Registrar Gestoría</button> 
                      <a href="gestorias.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/iban.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
	$('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
  });
</script>
