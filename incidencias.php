<?php
  $seccionActiva=4;
  include_once('cabecera.php');
  
  $empresa='';
  if(isset($_GET['codigoCliente'])){
	$codigoCliente=$_GET['codigoCliente'];
	$consulta=consultaBD("SELECT empresa FROM clientes WHERE codigo='$codigoCliente';",true);
	$datosCliente=mysql_fetch_assoc($consulta);
	$empresa=$datosCliente['empresa'];
  }
  
  if(isset($_POST['codigo'])){
    $res=actualizaIncidencia();
  }
  elseif(isset($_POST['codigoCliente'])){
   $res=registraIncidencia();
  }
  elseif(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaIncidencia();
  }
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para las incidencias</h6>

                   <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
              
                   <div class="leyenda">
                    <span class="grafico labelAmarilloIncidencias"></span>En proceso: <span id="valor1"></span><br>
                    <span class="grafico labelRojoIncidencias"></span>Pendientes: <span id="valor2"></span><br>
                    <span class="grafico labelVerdeIncidencias"></span>Resueltas: <span id="valor3"></span><br>
                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de incidencias</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="creaIncidencia.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Crear Incidencia</span> </a>
                <a href="#" id='email' class="shortcut"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Enviar eMail</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Incidencia actualizada correctamente."); 
            }
            else{
              mensajeError("no se han podido actualizar la incidencia. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['codigoCliente'])){
  		      if($res){
              mensajeOk("Incidencia registrada correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar la incidencia. Compruebe los datos introducidos."); 
            }
		      }
          elseif(isset($_POST['destinatarios'])){
            if($res){
              mensajeOk("Mensaje enviado correctamente."); 
            }
            else{
              mensajeError("no se ha podido enviar el mensaje. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el posible cliente. Contacte con el webmaster."); 
            }
          }
        ?>
		
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Incidencias registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable" id="tablaIncidencias">
                <thead>
                  <tr>
                    <th> Empresa </th>
					<th> Tipo </th>
                    <th> Contacto </th>
                    <th> Teléfono </th>
                    <th> Departamento </th>
                    <th> Prioridad </th>
                    <th> Estado </th>
                    <th class="centro"> </th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeIncidencias();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/excanvas.min.js" type="text/javascript"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
  <?php
    $datos=generaDatosGraficoIndicencias();
	echo "var marcado='".$empresa."';";
  ?>
  var pieData = [
      {
          value: <?php echo $datos['enproceso']; ?>,
          color: "#f89406"
      },
      {
          value: <?php echo $datos['pendientes']; ?>,
          color: "#d9534f"
      },
      {
          value: <?php echo $datos['resueltas']; ?>,
          color: "#468847"
      }
    ];

  var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
  
  $("#valor1").text("<?php echo $datos['enproceso']; ?>");
  $("#valor2").text("<?php echo $datos['pendientes']; ?>");
  $("#valor3").text("<?php echo $datos['resueltas']; ?>");

  $('#email').click(function(){
    var valoresChecks=recorreChecks();
    creaFormulario('enviarEmail.php?seccion=4',valoresChecks,'post');
  });

  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('incidencias.php',valoresChecks,'post');
    }

  });
  
  var tabla=$('.datatable').DataTable({
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave":true,
		"iDisplayLength":25,
		"oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
		  'sZeroRecords':'No se han encontrado coincidencias'
    }});

    var tabla=$('#tablaIncidencias').dataTable();
    tabla.fnFilter(marcado);

</script>
