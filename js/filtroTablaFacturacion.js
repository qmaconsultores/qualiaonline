$('.datatable').dataTable({
    "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap",
    "bStateSave":true,
    "iDisplayLength":1000000,
    "oLanguage": {
      "sLengthMenu": "_MENU_ registros por página",
      "sSearch":"Búsqueda:",
      "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
      "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
      "sEmptyTable":"Aún no hay datos que mostrar",
      "sInfoEmpty":"",
      'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
      'sZeroRecords':'No se han encontrado coincidencias'
    }
});