var temporizador;//Variable global

$(document).ready(function(){
  temporizadorInactividad();//Inicio el temporizador que envía al cierre de sesión cuando se carga la página

  //Las siguientes 2 líneas reinician el temporizador si se pulsa una tecla o el ratón
  $('body').attr('onkeypress','reiniciaTemporizador()');
  $('body').attr('onclick','reiniciaTemporizador()');
});

function reiniciaTemporizador() {
  clearTimeout(temporizador);//Borra el temporizador
  temporizadorInactividad();//Lo reestablece
}

function temporizadorInactividad(){
  temporizador=setTimeout('location="cerrarSesion.php"',900000);//Envía a la página de cierre de sesión a los 30 minutos de inactividad (es decir, cuando no se detectan pulsaciones de ratón y teclado)
}