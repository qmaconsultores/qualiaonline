function obtieneProvincia(cp){
	var res;
	if(parseFloat(cp)<1000){
		res=false;
	}else if(cp.length<5){
		res=false;
	}else{
		var digito=cp.substr(0,2);
		
		switch(digito){
			case '01':
				res='Álava';
			break;
			case '02':
				res='Albacete';
			break;
			case '03':
				res='Alicante';
			break;
			case '04':
				res='Almería';
			break;
			case '05':
				res='Ávila';
			break;
			case '06':
				res='Badajoz';
			break;
			case '07':
				res='Baleares (Palma de Mallorca)';
			break;
			case '08':
				res='Barcelona';
			break;
			case '09':
				res='Burgos';
			break;
			case '10':
				res='Cáceres';
			break;
			case '11':
				res='Cádiz';
			break;
			case '12':
				res='Castellón';
			break;
			case '13':
				res='Ciudad Real';
			break;
			case '14':
				res='Córdoba';
			break;
			case '15':
				res='Coruña';
			break;
			case '16':
				res='Cuenca';
			break;
			case '17':
				res='Gerona';
			break;
			case '18':
				res='Granada';
			break;
			case '19':
				res='Guadalajara';
			break;
			case '20':
				res='Guipúzcoa (San Sebastián)';
			break;
			case '21':
				res='Huelva';
			break;
			case '22':
				res='Huesca';
			break;
			case '23':
				res='Jaén';
			break;
			case '24':
				res='León';
			break;
			case '25':
				res='Lérida';
			break;
			case '26':
				res='La Rioja (Logroño)';
			break;
			case '27':
				res='Lugo';
			break;
			case '28':
				res='Madrid';
			break;
			case '29':
				res='Málaga';
			break;
			case '30':
				res='Murcia';
			break;
			case '31':
				res='Navarra (Pamplona)';
			break;
			case '32':
				res='Orense';
			break;
			case '33':
				res='Asturias (Oviedo)';
			break;
			case '34':
				res='Palencia';
			break;
			case '35':
				res='Las Palmas';
			break;
			case '36':
				res='Pontevedra';
			break;
			case '37':
				res='Salamanca';
			break;
			case '38':
				res='Santa Cruz de Tenerife';
			break;
			case '39':
				res='Cantabria (Santander)';
			break;
			case '40':
				res='Segovia';
			break;
			case '41':
				res='Sevilla';
			break;
			case '42':
				res='Soria';
			break;
			case '43':
				res='Tarragona';
			break;
			case '44':
				res='Teruel';
			break;
			case '45':
				res='Toledo';
			break;
			case '46':
				res='Valencia';
			break;
			case '47':
				res='Valladolid';
			break;
			case '48':
				res='Vizcaya (Bilbao)';
			break;
			case '49':
				res='Zamora';
			break;
			case '50':
				res='Zaragoza';
			break;
			case '51':
				res='Ceuta';
			break;
			case '52':
				res='Melilla';
			break;
			default:
				res=false;
		}
	}
	return res;
}