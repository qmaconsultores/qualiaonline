function insertaFila(tabla){//Función para la tabla de "Planificación"
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody tr:last").clone();
    //Obtengo el atributo name para los inputs y selects
    $tr.find("input,select,textarea").attr("name", function()
    {
     //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
     var parts = this.id.match(/(\D+)(\d*)$/);
     //Creo un nombre nuevo incrementando el número de fila (++parts[2])
     return parts[1] + ++parts[2];
    //Hago lo mismo con los IDs
    }).attr("id", function(){
     var parts = this.id.match(/(\D+)(\d*)$/);
     return parts[1] + ++parts[2];
    });

    $tr.find("button").attr("id", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input").attr("value","");//Quito los valores que tuvieran las casillas de esa fila
    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody tr:last").after($tr);
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
}

function insertaFilaAlumno(tabla){//Función para la tabla de "Planificación"
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody tr:last").clone();
    //Obtengo el atributo name para los inputs y selects
    $tr.find("input,select,textarea").attr("name", function()
    {
     //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
     var parts = this.id.match(/(\D+)(\d*)$/);
     //Creo un nombre nuevo incrementando el número de fila (++parts[2])
     return parts[1] + ++parts[2];
    //Hago lo mismo con los IDs
    }).attr("id", function(){
     var parts = this.id.match(/(\D+)(\d*)$/);
     return parts[1] + ++parts[2];
    });

    $tr.find("button").attr("id", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input").attr("value","");//Quito los valores que tuvieran las casillas de esa fila
    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody tr:last").after($tr);
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
}

function eliminaFila(tabla){//Función común para las 3 tablas
  if($('#'+tabla).find("tbody tr").length>1){//Si la tabla tiene más de 1 fila...
    $('#'+tabla).find("tbody tr:last").remove();
  }
}


function insertaFilaArticulo(tabla){//Función para la tabla de "Planificación"
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody tr:last").clone();
    //Obtengo el atributo name para los inputs y selects
    $tr.find("input,select").attr("name", function()
    {
     //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
     var parts = this.id.match(/(\D+)(\d*)$/);
     //Creo un nombre nuevo incrementando el número de fila (++parts[2])
     return parts[1] + ++parts[2];
    //Hago lo mismo con los IDs
    }).attr("id", function(){
     var parts = this.id.match(/(\D+)(\d*)$/);
     return parts[1] + ++parts[2];
    });

    $tr.find("button").attr("id", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input").attr("value","");//Quito los valores que tuvieran las casillas de esa fila
    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody tr:last").after($tr);
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    cambiaPrecios($tr.find('select'));
}