var leyenda = document.getElementById('leyenda');
leyenda.innerHTML=grafico.generateLegend();
  
helpers = Chart.helpers;

helpers.each(leyenda.firstChild.childNodes, function(leyenda, index){
  helpers.addEvent(leyenda, 'mouseover', function(){
    var activeSegment = grafico.segments[index];
    activeSegment.save();
    activeSegment.fillColor = activeSegment.highlightColor;
    grafico.showTooltip([activeSegment]);
    activeSegment.restore();
  });
});
helpers.addEvent(leyenda.firstChild, 'mouseout', function(){
  grafico.draw();
});