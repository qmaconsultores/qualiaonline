//Función para mostrar/ocultar clave
$('#mostrarClave').click(function(){
  var estado=$(this).attr('estado');
  if(estado=='ver'){
    var campo=$(this).parent().parent().find('input[type=password]');
    campo.get(0).type='text';
    $(this).html('Ocultar clave');
    $(this).attr('estado','oculto');
  }
  else{
    var campo=$(this).parent().parent().find('input[type=text]');
    campo.get(0).type='password';
    $(this).html('Mostrar clave');
    $(this).attr('estado','ver');
  }
});