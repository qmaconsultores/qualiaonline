//Esta función crea un fomulario oculto y lo envía. Para su uso en combinación con los checks de los listados y los botones

function creaFormulario(destino, campos, metodo) {
  metodo = metodo || "post";

  var form = document.createElement("form");
  form.setAttribute("method", metodo);
  form.setAttribute("action", destino);

  for(var clave in campos) {
      if(campos.hasOwnProperty(clave)) {
          var camposOcultos = document.createElement("input");
          camposOcultos.setAttribute("type", "hidden");
          camposOcultos.setAttribute("name", clave);
          camposOcultos.setAttribute("value", campos[clave]);

          form.appendChild(camposOcultos);
       }
  }

  document.body.appendChild(form);
  form.submit();
}


//Esta otra función recorre los checkbox marcados en los listados
function recorreChecks(){
  var valoresChecks=[];
  var i=0;
  $('input[name="codigoLista[]"]:checked').each(function() {
    valoresChecks['codigo'+i]=$(this).val();
    i++;
  });

  return valoresChecks;
}