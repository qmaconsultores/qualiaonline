<?php
  $seccionActiva=5;
  include_once('cabecera.php');

  $datos=datosCompletosAlumno($_GET['codigo']);
  $consultaObservaciones=consultaBD("SELECT * FROM observaciones_alumnos WHERE codigoAlumno='".$_GET['codigo']."';", true);
  $datosObservaciones=mysql_fetch_assoc($consultaObservaciones);
  
  $destino="formacion.php";
  if(isset($_GET['pendiente']) && $_GET['pendiente']=='1'){
	$destino="alumnosPendientes.php";
  }
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Datos del alumno</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="<?php echo $destino; ?>" method="post">
                  <div class="tabbable">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#pagina1" data-toggle="tab">Datos generales</a></li>
                      <li><a href="#pagina2" data-toggle="tab">Observaciones</a></li>
                    </ul>
                  
                    <br>

                    <div class="tab-content" id="pestanas">

                    <div class="tab-pane active" id="pagina1">
                  <fieldset class="span3">

                    <input type="hidden" name="codigo" value="<?php echo $datos['codigo']; ?>">
					<?php campoOculto('SI','esAlumno'); ?>

                    <div class="control-group">                     
                      <label class="control-label" for="nombre">Nombre:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="nombre" name="nombre" value="<?php echo $datos['nombre']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    
                    <div class="control-group">                     
                      <label class="control-label" for="apellidos">Apellidos:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="apellidos" name="apellidos" value="<?php echo $datos['apellidos']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="dni">DNI:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="dni" name="dni" value="<?php echo $datos['dni']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="sexo">Sexo:</label>
                      <div class="controls">
                        
                        <label class="radio inline">
                          <input type="radio" name="sexo" value="M" <?php if($datos['sexo']=='M'){ echo 'checked="checked"'; } ?> > Hombre
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="sexo" value="F" <?php if($datos['sexo']=='F'){ echo 'checked="checked"'; } ?> > Mujer
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->





                    <div class="control-group">                     
                      <label class="control-label" for="fechaNac">Fecha de nacimiento:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaNac" name="fechaNac" value="<?php echo formateaFechaWeb($datos['fechaNac']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->





                    <div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono" value="<?php echo $datos['telefono']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="movil">Móvil:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="movil" name="movil" value="<?php echo $datos['movil']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="mail">eMail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="mail" name="mail" value="<?php echo $datos['mail']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                  </fieldset>
                  <fieldset class="span3">


                    <div class="control-group">                     
                      <label class="control-label" for="numSS1">Nº Seg. Social:</label>
                      <div class="controls numSS">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-supermini" id="numSS1" name="numSS1" maxlength="2" value="<?php echo $datos['numSS1']; ?>">
                          <span class="add-on">/</span>
                          <input type="text" class="input-small" id="numSS2" name="numSS2" maxlength="10" value="<?php echo $datos['numSS2']; ?>">
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php
						campoSelect('estudios','Nivel de estudios:',array('1. Menos que primaria','2. Educación primaria','3. ESO, EGB, Cert. Prof. (1y2)',
						'4. Bachillerato, BUP, FP (I y II)','5. Cert. Prof. de Nivel 3','6. Técnico Superior','7. -1º Ciclo Uni. (Diplomatura-Grados)',
						'8. -2º Ciclo Uni. (Licenciatura-Master)','9. -3º Ciclo Uni. (Doctorado)','10. Otras Titulaciones'),array('sin','primarios','3',
						'bachiller','5','6','diplomatura','licenciatura','9','10'),$datos);
					?>



                    <div class="control-group">                     
                      <label class="control-label" for="categoria">Categoría:</label>
                      <div class="controls">
                        
                        <select name="categoria" class='selectpicker show-tick'>
                          <option value="directivo" <?php if($datos['categoria']=='directivo'){ echo 'selected="selected"'; } ?> >Directivo</option>
                          <option value="intermedio" <?php if($datos['categoria']=='intermedio'){ echo 'selected="selected"'; } ?> >Mando Intermedio</option>
                          <option value="tecnico" <?php if($datos['categoria']=='tecnico'){ echo 'selected="selected"'; } ?> >Técnico</option>
                          <option value="cualificado" <?php if($datos['categoria']=='cualificado'){ echo 'selected="selected"'; } ?> >Trabajador Cualificado</option>
                          <option value="bajaCualificacion" <?php if($datos['categoria']=='bajaCualificacion'){ echo 'selected="selected"'; } ?> >Trabajador no Cualificado</option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="cotizacion">Grupo de Cotización:</label>
                      <div class="controls">
                        
                        <select name="cotizacion" class='selectpicker show-tick'>
                          <option value="1" <?php if($datos['cotizacion']=='1'){ echo 'selected="selected"'; } ?> >1. Ingenieros y Licenciados</option>
                          <option value="2" <?php if($datos['cotizacion']=='2'){ echo 'selected="selected"'; } ?> >2. Ingenieros Técnicos</option>
                          <option value="3" <?php if($datos['cotizacion']=='3'){ echo 'selected="selected"'; } ?> >3. Jefes Administrativos y de taller</option>
                          <option value="4" <?php if($datos['cotizacion']=='4'){ echo 'selected="selected"'; } ?> >4. Ayudantes no Titulados</option>
                          <option value="5" <?php if($datos['cotizacion']=='5'){ echo 'selected="selected"'; } ?> >5. Oficiales Administrativos</option>
                          <option value="6" <?php if($datos['cotizacion']=='6'){ echo 'selected="selected"'; } ?> >6. Subalternos</option>
                          <option value="7" <?php if($datos['cotizacion']=='7'){ echo 'selected="selected"'; } ?> >7. Auxiliares Administrativos</option>
                          <option value="8" <?php if($datos['cotizacion']=='8'){ echo 'selected="selected"'; } ?> >8. Oficiales de Primera y Segunda</option>
                          <option value="9" <?php if($datos['cotizacion']=='9'){ echo 'selected="selected"'; } ?> >9. Oficiales de Tercera y Especialistas</option>
                          <option value="10" <?php if($datos['cotizacion']=='10'){ echo 'selected="selected"'; } ?> >10. Peones</option>
                          <option value="11" <?php if($datos['cotizacion']=='11'){ echo 'selected="selected"'; } ?> >11. Trabajadores menores de 18 años</option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="horario">Horario de trabajo:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="horario" name="horario" value="<?php echo $datos['horario']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="discapacidad">Discapacidad:</label>
                      <div class="controls">
                        
                        <label class="radio inline">
                          <input type="radio" name="discapacidad" value="SI" <?php if($datos['discapacidad']=='SI'){ echo 'checked="checked"'; } ?> > Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="discapacidad" value="NO" <?php if($datos['discapacidad']=='NO'){ echo 'checked="checked"'; } ?> > No
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="discapacidad">Víctima de terrorismo:</label>
                      <div class="controls">
                        
                        <label class="radio inline">
                          <input type="radio" name="vTerrorismo" value="SI" <?php if($datos['vTerrorismo']=='SI'){ echo 'checked="checked"'; } ?> > Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="vTerrorismo" value="NO" <?php if($datos['vTerrorismo']=='NO'){ echo 'checked="checked"'; } ?> > No
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->




                    <div class="control-group">                     
                      <label class="control-label" for="vViolencia">Víctima de violencia de género:</label>
                      <div class="controls">
                        
                        <label class="radio inline">
                          <input type="radio" name="vViolencia" value="SI" <?php if($datos['vViolencia']=='SI'){ echo 'checked="checked"'; } ?> > Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="vViolencia" value="NO" <?php if($datos['vViolencia']=='NO'){ echo 'checked="checked"'; } ?> > No
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                  </fieldset>
                  </div>


                  <div class="tab-pane" id="pagina2">
                    <table class="table table-striped table-bordered datatable" id="tablaObservaciones">
                      <thead>
                        <tr>
                          <th> Fecha </th>
                          <th> Observaciones </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=0;
                        while($datosObservaciones!=0){
                        ?>    
                        <tr>
                          <?php
                            campoFechaTabla('fecha'.$i, $datosObservaciones['fecha']);
                            $observaciones='observacion'.$i;
                          ?>
                          <td>
                            <div class='control-group'>                     
                              <div class='controls'>
                                <textarea name='<?php echo $observaciones;?>' class='areaTexto' id='<?php echo $observaciones;?>'><?php echo $datosObservaciones['observaciones'];?></textarea>
                              </div> <!-- /controls -->       
                            </div> <!-- /control-group -->
                          </td>
                        </tr>
                        <?php
                          $i++;
                          $datosObservaciones=mysql_fetch_assoc($consultaObservaciones);
                        }

                        if($i==0){
                        ?>
                        <tr>
                          <?php
                            campoFechaTabla('fecha0');
                          ?>
                          <td>
                            <div class='control-group'>                     
                              <div class='controls'>
                                <textarea name='observacion0' class='areaTexto' id='observacion0'></textarea>
                              </div> <!-- /controls -->       
                            </div> <!-- /control-group -->
                          </td>
                        </tr>
                        <?php
                        }
                        ?>

                      </tbody>
                    </table>
					
                      
                    <center>
                      <button type="button" class="btn btn-success" onclick="insertaFila('tablaObservaciones');"><i class="icon-plus"></i> Añadir Observación</button> 
                      <button type="button" class="btn btn-danger" onclick="eliminaFila('tablaObservaciones');"><i class="icon-minus"></i> Eliminar Observación</button> 
                    </center>
					
					<?php if(isset($_GET['codigoCurso'])){ 
						$consulta=consultaBD("SELECT llamadaBienvenida, llamadaSeguimiento, llamadaFinalizacion, fechaBienvenida, horaBienvenida, resolucion FROM alumnos_registrados_cursos WHERE codigoAlumno='".$_GET['codigo']."' AND codigoCurso='".$_GET['codigoCurso']."';",true);
						$datosCurso=mysql_fetch_assoc($consulta);
						echo "<h3 class='apartadoFormulario'>Datos de seguimiento</h3>";
						$opciones=array('NO', 'SI');
            $resoluciones = array("<span class='label label-success'>Curso acabado</span>","<span class='label label-danger'>Curso no hecho</span>","<span class='label label-amarillo'>El alumno va muy mal y termina el plazo</span>","<span class='label label-azul-claro'>No llamar al alumno</span>","<span class='label label-rosa'>Curso bonificado</span>","<span class='label label-primary'>Llamar por la tarde</span>","<span class='label label-inverse'>Cancelado</span>","<span class='label label-naranja'>Hablar con comercial</span>");
            campoSelectHTML('resolucion','Resolucion',$resoluciones,array('SI','NO','MAL','NOLLAMAR','BONIFICADO','TARDE','CANCELADO','COMERCIAL'),$datosCurso);

						campoSelect('llamadaBienvenida', 'LLamada de Bienvenida', $opciones, $opciones, $datosCurso, 'selectpicker show-tick anchoAuto');
            campoFecha('fechaBienvenida','Fecha',$datosCurso);
            campoTexto('horaBienvenida','Hora',$datosCurso,'input-small');
						campoSelect('llamadaSeguimiento', 'LLamada de Seguimiento', $opciones, $opciones, $datosCurso, 'selectpicker show-tick anchoAuto');
						campoSelect('llamadaFinalizacion', 'LLamada de Finalización', $opciones, $opciones, $datosCurso, 'selectpicker show-tick anchoAuto');
						campoFecha('fechaUltimaLlamada','Fecha última llamada',$datos);

						campoOculto($_GET['codigoCurso'],'codigoCurso');
					} ?>

                    </div><!-- /pagina2 -->

                  <fieldset class="sinFlotar">
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar datos</button> 
                      <a href="<?php echo $destino; ?>" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
</div>
</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
  });
</script>
