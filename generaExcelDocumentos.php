<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

  	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos/plantillaDocumentacion.xlsx");
	
	$objPHPExcel->setActiveSheetIndex(3);

	$objPHPExcel->getActiveSheet()->getCell('C10')->setValue(utf8_encode(utf8_decode("CÓDIGO")));
	$objPHPExcel->getActiveSheet()->getCell('D10')->setValue(utf8_encode(utf8_decode("COLABORADOR")));
	$objPHPExcel->getActiveSheet()->getCell('E10')->setValue(utf8_encode(utf8_decode("RESPONSABLE")));
	$objPHPExcel->getActiveSheet()->getCell('F10')->setValue(utf8_encode(utf8_decode("UBICACIÓN")));
	$objPHPExcel->getActiveSheet()->getCell('G10')->setValue(utf8_encode(utf8_decode("TIEMPO CONSERVACIÓN")));
	$objPHPExcel->getActiveSheet()->getStyle('C10:G10')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

  	$consulta=consultaBD("SELECT * FROM documentosDistribucion ORDER BY codigoDocumento;",true);
	$datos=mysql_fetch_assoc($consulta);
	$i=11;
	while($datos!=0){

		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue(utf8_encode(utf8_decode($datos['codigoDocumento'])));
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue(utf8_encode(utf8_decode($datos['nombreDocumento'])));
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue(utf8_encode(utf8_decode($datos['responsable'])));
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue(utf8_encode(utf8_decode($datos['ubicacion'])));
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue(utf8_encode(utf8_decode($datos['tiempoConservacion'])));

		$datos=mysql_fetch_assoc($consulta);
		$i++;
	}

	$objPHPExcel->setActiveSheetIndex(2);

	$objPHPExcel->getActiveSheet()->getCell('C10')->setValue(utf8_encode(utf8_decode("CÓDIGO")));
	$objPHPExcel->getActiveSheet()->getCell('D10')->setValue(utf8_encode(utf8_decode("NOMBRE")));
	$objPHPExcel->getActiveSheet()->getCell('E10')->setValue(utf8_encode(utf8_decode("RESPONSABLE")));
	$objPHPExcel->getActiveSheet()->getCell('F10')->setValue(utf8_encode(utf8_decode("UBICACIÓN")));
	$objPHPExcel->getActiveSheet()->getCell('G10')->setValue(utf8_encode(utf8_decode("TIEMPO CONSERVACIÓN")));
	$objPHPExcel->getActiveSheet()->getStyle('C10:G10')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

  	$consulta=consultaBD("SELECT * FROM documentosRegistros ORDER BY codigoDocumento;",true);
	$datos=mysql_fetch_assoc($consulta);
	$i=11;
	while($datos!=0){

		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue(utf8_encode(utf8_decode($datos['codigoDocumento'])));
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue(utf8_encode(utf8_decode($datos['nombreDocumento'])));
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue(utf8_encode(utf8_decode($datos['responsable'])));
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue(utf8_encode(utf8_decode($datos['ubicacion'])));
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue(utf8_encode(utf8_decode($datos['tiempoConservacion'])));

		$datos=mysql_fetch_assoc($consulta);
		$i++;
	}


	$objPHPExcel->setActiveSheetIndex(1);

	$objPHPExcel->getActiveSheet()->getCell('C10')->setValue(utf8_encode(utf8_decode("CÓDIGO")));
	$objPHPExcel->getActiveSheet()->getCell('D10')->setValue(utf8_encode(utf8_decode("NOMBRE")));
	$objPHPExcel->getActiveSheet()->getCell('E10')->setValue(utf8_encode(utf8_decode("UBICACIÓN")));
	$objPHPExcel->getActiveSheet()->getStyle('C10:E10')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

  	$consulta=consultaBD("SELECT * FROM documentosExternos ORDER BY codigoDocumento;",true);
	$datos=mysql_fetch_assoc($consulta);
	$i=11;
	while($datos!=0){

		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue(utf8_encode(utf8_decode($datos['codigoDocumento'])));
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue(utf8_encode(utf8_decode($datos['nombreDocumento'])));
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue(utf8_encode(utf8_decode($datos['ubicacion'])));

		$datos=mysql_fetch_assoc($consulta);
		$i++;
	}

	$objPHPExcel->setActiveSheetIndex(0);

	$objPHPExcel->getActiveSheet()->getCell('C10')->setValue(utf8_encode(utf8_decode("CÓDIGO")));
	$objPHPExcel->getActiveSheet()->getCell('D10')->setValue(utf8_encode(utf8_decode("NOMBRE")));
	$objPHPExcel->getActiveSheet()->getCell('E10')->setValue(utf8_encode(utf8_decode("FECHA REVISIÓN")));
	$objPHPExcel->getActiveSheet()->getCell('F10')->setValue(utf8_encode(utf8_decode("FECHA")));
	$objPHPExcel->getActiveSheet()->getStyle('C10:F10')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

  	$consulta=consultaBD("SELECT * FROM documentosInternos ORDER BY codigoDocumento;",true);
	$datos=mysql_fetch_assoc($consulta);
	$i=11;
	while($datos!=0){

		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue(utf8_encode(utf8_decode($datos['codigoDocumento'])));
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue(utf8_encode(utf8_decode($datos['nombreDocumento'])));
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue(utf8_encode(utf8_decode(formateaFechaWeb($datos['fechaRevision']))));
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue(utf8_encode(utf8_decode(formateaFechaWeb($datos['fecha']))));

		$datos=mysql_fetch_assoc($consulta);
		$i++;
	}

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('documentos/Documentacion.xlsx');	

	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=Documentacion.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/Documentacion.xlsx');

?>