<?php
  $seccionActiva=24;
  include_once('cabecera.php');
  
  $res=false;
  if(isset($_POST['codigo'])){
	if(isset($_POST['iban'])){
		$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
	}
	if($_POST['tieneColaborador']=='NO'){
		$_POST['comercial']='NULL';
	}
    $res=actualizaCliente();
  }
  elseif(isset($_POST['empresa'])){
	$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
	if($_POST['tieneColaborador']=='NO'){
		$_POST['comercial']='NULL';
	}
    $res=altaCliente('PROPIO');
  }
  elseif(isset($_POST['reasignacion'])){
	$res=reasignaClientes();
  }
  elseif(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_GET['codigoPasar'])){
    $res=consultaBD('UPDATE clientes SET activo="PROPIO" WHERE codigo='.$_GET['codigoPasar'],true);
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaCliente();
  }
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema sobre clientes</h6>

                   <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
              
                   <div class="leyenda">
                    <span class="grafico grafico1"></span>Totales: <span id="valor1"></span><br>
                    <span class="grafico grafico2"></span>Por confirmar: <span id="valor2"></span><br>
                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión comercial</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="creaPosibleClientePropio.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Agregar cliente</span> </a>
				<a href="#" id='email' class="shortcut"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Enviar eMail</span> </a>
                <a href="#" id='tareas' class="shortcut"><i class="shortcut-icon icon-list-ol"></i><span class="shortcut-label">Añadir Tarea</span> </a>
				<a href='filtrarClientes.php?posibles=PROPIO&seccion=1' class="shortcut"><i class="shortcut-icon icon-filter"></i><span class="shortcut-label">Filtrar</span> </a>
				<br>
				<a href='carteras.php' class="shortcut"><i class="shortcut-icon icon-briefcase"></i><span class="shortcut-label">Carteras</span> </a>
				<?php if($_SESSION['tipoUsuario']=='ADMIN'){ ?>
					<a href="#" id="reasignarClientes" class="shortcut"><i class="shortcut-icon icon-refresh"></i><span class="shortcut-label">Reasignar</span> </a>
					<a href="generaExcelCompleto.php?cliente=PROPIO" class="shortcut"><i class="shortcut-icon icon-download-alt"></i><span class="shortcut-label">Excel completo</span> </a>
                <?php } compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Datos de posible cliente actualizados."); 
            }
            else{
              mensajeError("no se ha podido actualizar el posible cliente. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['empresa'])){
            if($res){
              mensajeOk("Posible cliente registrado correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar el posible cliente. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['destinatarios'])){
            if($res){
              mensajeOk("Mensaje enviado correctamente."); 
            }
            else{
              mensajeError("no se ha podido enviar el mensaje. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el posible cliente. Contacte con el webmaster."); 
            }
          }
		   mensajeResultado('reasignacion',$res,'clientes');
        ?>
		    
        <div class="widget widget-table action-table cajaSelect">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Posibles clientes registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
				<table class='table table-striped table-bordered datatable' id='tablaClientes'>
                <thead>
                  <tr>
					<th> ID </th>
                    <th> Nombre del cliente </th>
					<th> Estado </th>
					<th> Resolución </th>
					<th> CIF </th>
					<th> Población </th>
					<th> CP </th>
          <th> Provincia </th>
					<th> Comercial asignado </th>
                    <th> Servicio ofrecido </th>
                    <th class='td-actions'></th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>
					<?php //imprimeClientes(); ?>
				</tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  
	  <!--Popup -->
      <div class="span6 hide" id="ventanaFlotante">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-shopping-cart"></i><i class="icon-chevron-right"></i><i class="icon-refresh"></i>
              <h3>Reasignación de clientes</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" method="post" action="posiblesClientes.php">
                  <fieldset>
                    
                    <?php
                      campoRadio('reasignacionDirecta','Tipo resignación','Directa',array('Directa','Indirecta'),array('Directa','Indirecta'));
                      echo "<div id='asignarPorComercial'>";
						campoSelectConsulta('codigoUsuarioPartida','Usuario actual',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios ORDER BY nombre, apellidos;");
						campoSelectConsulta('codigoUsuarioFinal','Reasignar a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios ORDER BY nombre, apellidos;");
					  echo "</div>
					  <div id='asignarPorPoblacion'>";
						campoTexto('cp','CP','','input-small');
						campoTexto('poblacion','Población');
						campoSelectConsulta('codigoUsuarioAsignacion','Asignar a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios ORDER BY nombre, apellidos;");
					  echo "</div>";
					  campoOculto('SI','reasignacion');
                    ?>


                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Reasignar clientes</button> 
                      <button type="button" class="btn" id="cancelar"><i class="icon-remove"></i> Cancelar</button> 
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>
      </div>
      <!--Fin Popup -->
	  
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<!--script src="js/filtroTabla.js"></script-->
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/excanvas.min.js" type="text/javascript"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
    <?php
      $datos=generaDatosGraficoPosiblesClientes('PROPIO');
    ?>
	$('#asignarPorPoblacion').css('display','none');
	$('input[type=radio][name=reasignacionDirecta]').change(function() {
        if($(this).val()=='Directa'){
			$('#asignarPorPoblacion').hide(300);
			$('#asignarPorComercial').show(300);
		}else{
			$('#asignarPorPoblacion').show(300);
			$('#asignarPorComercial').hide(300);
		}
    });
	$('#tablaClientes').dataTable({	
	  "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 3 ] }
       ],
      'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoPosiblesClientesPropios.php?action=getMembersAjx',
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          $('td:eq(10)', nRow).addClass( "centro" );
		  $('td:eq(9)', nRow).addClass( "input-large" );
       },
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });
    var pieData = [
        {
            value: <?php echo $datos['totales']; ?>,
            color: "#428bca"
        },
        {
            value: <?php echo $datos['pendientes']; ?>,
            color: "#B02B2C"
        }
      ];

    var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
    
    $("#valor1").text("<?php echo $datos['totales']; ?>");
    $("#valor2").text("<?php echo $datos['pendientes']; ?>");

    $('#email').click(function(){
      var valoresChecks=recorreChecks();
      creaFormulario('enviarEmail.php?seccion=1',valoresChecks,'post');
    });

    $('#tareas').click(function(){
      var valoresChecks=recorreChecks();
      creaFormulario('creaTarea.php',valoresChecks,'post');
    });
	
	$('#reasignarClientes').click(function(){
		$('#ventanaFlotante').removeClass('hide');
	});
	
	$('#cancelar').click(function(){
		$('#ventanaFlotante').addClass('hide');
	});

    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes un cliente.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('posiblesClientesPropios.php',valoresChecks,'post');
      }

    });

</script>