<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común


  	//$_GET['codigo'] viene en la URL a través de un botón "Descargar Contrato"
	$datos=datosRegistro('clientes',$_GET['codigo']);
	$datosUsuario=datosRegistro('usuarios',$datos['codigoUsuario']);
	
	//Carga de la librería PHPWord
	require_once 'phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('documentos/plantillaContrato.docx');

	/*
	Las siguientes dos líneas son un ejemplo de como se le dice a PHPWord el valor de una etiqueta.
	En este caso, las etiquetas serían ${precio} y ${turnos}, pero se ponen sin ${}.
	*/
	
	
	// PARTE CLIENTE
	$document->setValue("representante",utf8_decode($datos['repreLegal']));
	$document->setValue("dni",utf8_decode($datos['dniRepresentante']));
	$document->setValue("empresa",utf8_decode($datos['empresa']));
	$document->setValue("nif",utf8_decode($datos['cif']));
	$document->setValue("domicilio",utf8_decode($datos['direccion']));
	$document->setValue("poblacion",utf8_decode($datos['localidad']));
	$document->setValue("provincia",utf8_decode($datos['provincia']));
	$document->setValue("cp",utf8_decode($datos['cp']));
	$document->setValue("telefono",utf8_decode($datos['telefono']));
	$document->setValue("mail",utf8_decode($datos['mail']));
	$document->setValue("contacto",utf8_decode($datos['contacto']));
	$document->setValue("numCuenta",utf8_decode($datos['numCuenta']));
	/****************************************/
	
	// PARTE USUARIO
	$document->setValue("usuario",utf8_decode($datosUsuario['nombre'].' '.$datosUsuario['apellidos']));
	$document->setValue("dniUsuario",utf8_decode($datosUsuario['dni']));
	/****************************************/
	
	// FECHA
	$document->setValue("dia",utf8_decode(date('d')));
	$document->setValue("mes",utf8_decode(devuelveMes(date('m'))));
	$document->setValue("anio",utf8_decode(date('Y')));
	/****************************************/

	/*
	Cuando ya has sustituido todas las etiquetas por su valores, el método save
	guarda el fichero resultane en la ruta que le indiques con el nombre que le
	indiques.
	*/
	$tiempo=time();
	$document->save('documentos/Contrato.docx');


	/*
	Definir headers.
	Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
	que lo que va a recibir es un fichero que debe descargar.
	*/
	header("Content-Type: application/vnd.ms-docx");
	header("Content-Disposition: attachment; filename=Contrato.docx");
	header("Content-Transfer-Encoding: binary");

	/* 
	Descargar archivo.
	Por último, le decirmos a PHP que lea y transfiera el documento que
	antes con save.
	*/
	readfile('documentos/Contrato.docx');


?>