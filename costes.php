<?php
  $seccionActiva=5;
  include_once("cabecera.php");
  
  $codigo=$_GET['codigo'];
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  

?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-eur"></i>
              <h3>Costes de curso</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              Seleccione la empresa para la que quiere gestionar los costes:<br><br>
			       <form action="costesCurso.php" method="post">
					     <fieldset>
                  <input type="hidden" value="<?php echo $codigo; ?>" name="codigoCurso">
						      <?php 
								    selectAlumnosCostes($codigo);
						      ?>
                  <br>
				          <button type="submit" class="btn btn-primary"> Ver costes <i class="icon-circle-arrow-right"></i></button>
                </fieldset>
			        </form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
	</div>
	</div>
	</div>
</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
  });
</script>


