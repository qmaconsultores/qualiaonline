<?php
  $seccionActiva=17;
  include_once('cabecera.php');
  $consulta=consultaBD("SELECT referencia FROM colaboradores ORDER BY referencia DESC LIMIT 1;",true);
  $referencia=mysql_fetch_assoc($consulta);
  $referenciaNueva=$referencia['referencia']+1;
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nuevo Colaborador</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="colaboradores.php" method="post">
                  <fieldset class="span5">
				  
					<?php
						campoOculto($referenciaNueva,'referencia');
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="empresa">Empresa:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="empresa" name="empresa">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="cif">CIF:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="cif" name="cif" maxlength="9">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					          <div class="control-group">                     
                      <label class="control-label" for="ccc">NºSS (C.C.C):</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="ccc" name="ccc" maxlength="12">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTextoSimbolo('comision','Comisión','%');
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="numSS1">Nº Cuenta Bancaria:</label>
                      <div class="controls numSS">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini" id="iban" name="iban" maxlength="4" size="4">
                          <span class="add-on">/</span>
                          <input type="text" class="input-medium" id="numCuenta" name="numCuenta" maxlength="20" size="20">
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="nuevacreacion">Empresa de Nueva Creación:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="nuevacreacion" value="SI"> Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="nuevacreacion" value="NO" checked="checked"> No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php 
						campoFecha('fechaCreacion','Fecha de creación',' ');
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="direccion">Dirección:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="direccion" name="direccion">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="cp">CP:</label>
                      <div class="controls">
                        <input type="text" class="input-mini" id="cp" name="cp">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="localidad">Localidad:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="localidad" name="localidad">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="provincia">Provincia:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="provincia" name="provincia">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="movil">Móvil:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="movil" name="movil">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('telefonoDos','Teléfono 2','','input-small');
						campoTexto('movilDos','Móvil 2','','input-small');
					?>


                    <div class="control-group">                     
                      <label class="control-label" for="mail">Mail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="mail" name="mail">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="fax">Fax:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="fax" name="fax">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="pyme">PYME:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="pyme" value="SI" checked="checked"> Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="pyme" value="NO"> No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                  </fieldset>
                  <fieldset class="span3">
				  
					<?php
						campoSelectConsulta('codigoUsuario','Comercial',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' ORDER BY nombre, apellidos;");
						selectConsultaMultipleColaboradores();
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="sector">Sector:</label>
                      <div class="controls">
                        
                        <select name="sector" class='selectpicker show-tick' data-live-search="true">
                          <option value="HOSTELERIA">Hostelería</option>
                          <option value="COMERCIO">Comercio</option>
                          <option value="TRANSPORTE">Transporte</option>
                          <option value="ESTETICA">Estética</option>
                          <option value="SANIDAD">Sanidad</option>
                          <option value="VETERINARIA">Veterinaria</option>
                          <option value="DEPORTES">Deportes</option>
                          <option value="ASOCIACIONES">Asociaciones</option>
                          <option value="FORMACION">Formación</option>
                          <option value="INDUSTRIA">Industria</option>
                          <option value="CONSTRUCCION">Construcción</option>
                          <option value="INFORMATICA">Informática</option>
                          <option value="SERVICIOS">Servicios</option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fechaRegistro">Fecha de registro:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaRegistro" name="fechaRegistro" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					
					<?php
						campoTexto('repreLegal','Representante legal');
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="dniRepresentante">DNI Representante:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="dniRepresentante" name="dniRepresentante">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
                    
					           <div class="control-group">                     
                      <label class="control-label" for="contacto">Persona de contacto:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="contacto" name="contacto">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					


                    <div class="control-group">                     
                      <label class="control-label" for="cargo">Cargo:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="cargo" name="cargo">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					                

                    <div class="control-group">                     
                      <label class="control-label" for="representacion">R. Legal de los Trabajadores:</label>
                      <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="representacion" value="SI"> Si
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="representacion" value="NO" checked="checked"> No
                        </label>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						areaTexto('observaciones','Observaciones');
					?>



                  </fieldset>
				  
				  <fieldset class="span10">
				  <h3 class="apartadoFormulario sinFlotar">Convenio colectivo</h3>
				  
				  <?php
          campoFecha('fechaFirma','Fecha firma');
					campoTexto('convenio','Convenio colectivo');
					areaTexto('desConvenio','Descripción Convenio colectivo');
				  ?>
				  
				  </fieldset>

				  <fieldset class="span10">
				  <h3 class="apartadoFormulario sinFlotar">C.N.A.E.</h3>
				  
				  <?php
					campoTexto('cnae','C.N.A.E.');
					areaTexto('desCnae','Descripción C.N.A.E.');
				  ?>
				  </fieldset>
				  <fieldset class="sinFlotar">
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Registrar Colaborador</button> 
                      <a href="colaboradores.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/iban.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
	$('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$("#numCuenta").focusout(function() {
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
	  }).blur(function(){
		$("#iban").val(CalcularIBAN($("#numCuenta").val(),'ES'));
	});
  });
</script>
