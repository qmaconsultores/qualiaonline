<?php
  $seccionActiva=8;
  include_once('cabecera.php');
    
  $estadisticas=creaEstadisticasInicio();

  if(isset($_POST['codigo'])){
    $res=actualizaUsuario();
  }
  elseif(isset($_POST['nombre'])){
    $res=registraUsuario();
  }
  elseif(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaUsuario();
  }
  $datosUsuario=datosRegistro('usuarios',$_SESSION['codigoS']);

?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de accesos al sistema durante el día de hoy</h6>

                  <canvas id="area-chart" class="chart-holder" width="538" height="250"></canvas>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de usuarios</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="creaUsuario.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Crear Usuario</span> </a>
                <a href="#" id='email' class="shortcut"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Enviar eMail</span> </a>
				<?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      </div><!-- /row -->


      <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Usuario actualizado correctamente."); 
            }
            else{
              mensajeError("no se han podido actualizar el usuario. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['nombre'])){
            if($res){
              mensajeOk("Usuario registrado correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar el usuario. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['destinatarios'])){
            if($res){
              mensajeOk("Mensaje enviado correctamente."); 
            }
            else{
              mensajeError("no se ha podido enviar el mensaje. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el usuario. Contacte con el webmaster."); 
            }
          }
        ?>


      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-th-list"></i>
          <h3>Usuarios registrados</h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
          <table class='table table-striped table-bordered datatable' id='tablaUsuarios'>
            <thead>
              <tr>
                <th> Nombre </th>
                <th> Teléfono </th>
                <th> eMail </th>
                <th> Usuario </th>
                <th> Clave </th>
                <th> Tipo </th>
                <th class='centro'></th>
                <th><input type='checkbox' id='todo'></th>
              </tr>
            </thead>
            <tbody>
			<?php imprimeUsuarios(); ?>
			</tbody>
       </table>
        </div>
        <!-- /widget-content --> 
      </div>




      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-signin"></i>
          <h3>Accesos al sistema</h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable" id='tablaAccesos'>
            <thead>
              <tr>
                <th> Usuario </th>
                <th> Fecha </th>
                <th> Hora </th>
                <th> Dirección IP </th>
                <th> Navegador </th>
                <th> Sistema Operativo </th>
              </tr>
            </thead>
            <tbody>

              <?php
                
                //imprimeAccesos();

              ?>
            
            </tbody>
          </table>
        </div>
        <!-- /widget-content --> 
      </div>


    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/excanvas.min.js" type="text/javascript"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#email').click(function(){
    var valoresChecks=recorreChecks();
    creaFormulario('enviarEmail.php?seccion=8',valoresChecks,'post');
  });

  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('usuarios.php',valoresChecks,'post');
    }

  });
  
  /*var tabla=$('#tablaUsuarios').DataTable({
		"aaSorting": [],
      'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoUsuarios.php?action=getMembersAjx',
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		  $('td:eq(6)', nRow).addClass( "centro" );
		  $('td:eq(7)', nRow).addClass( "centro" );
		  $('td:eq(8)', nRow).addClass( "centro" );
		  $('td:eq(9)', nRow).addClass( "centro" );
	   },
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });*/
	
	$('#tablaUsuarios').dataTable({
		"sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave":true,
		"iDisplayLength":25,
		"oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
		  'sZeroRecords':'No se han encontrado coincidencias'
		}
	});
	
   $('#tablaAccesos').DataTable({
		"aaSorting": [],
      'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoAccesos.php?action=getMembersAjx',
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });


  <?php $datos=generaDatosGraficoAccesos(); ?>
  var lineChartData = {
            labels: <?php echo $datos['etiquetas']; ?>,
            datasets: [
        {
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            data: <?php echo $datos['accesos']; ?>
        }
      ]

  }

  var opciones={
    scaleOverride : true,
    scaleSteps : <?php echo $datos['max']; ?>,
    scaleStepWidth : 1, //Si el número de incidencias es menor que 10, la escala del gráfico va de 1 en 1, sino de 5 en 5.
    scaleStartValue : 0,
    barValueSpacing:60,
    barDatasetSpacing:20
  }

  var myLine = new Chart(document.getElementById("area-chart").getContext("2d")).Line(lineChartData,opciones);
  
  
</script>
