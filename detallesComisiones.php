<?php
  $seccionActiva=7;
  include_once('cabecera.php');

  $codigo=$_GET['codigo'];

  $datos=datosRegistro('facturacion',$codigo);
  $cliente=datosRegistro('clientes',$datos['codigoCliente']);
  $destino="informesComisionesColaboradores.php";
  if(isset($_GET['comercial']) && $_GET['comercial']=='SI'){
	$destino="informesComisionesComerciales.php";
  }
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Comisiones de la factura <?php echo $datos['referencia']; ?> para el cliente <?php echo "<a href='detallesCuenta.php?codigo=".$cliente['codigo']."'>".$cliente['empresa']."</a>";?></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="<?php echo $destino; ?>" method="post">
                  <fieldset>
				  
					<?php
						campoOculto($datos['codigo'],'codigo');
						campoFecha('fechaPago','Fecha pago comisión',$datos);
						if(!isset($_GET['comercial'])){
							campoFecha('fechaEnvioDocumentacion','Fecha envío doc.',$datos);
							campoFecha('fechaRecepcion','Fecha recepción factura',$datos);
						}
						campoRadio('pagadoColaborador','Pagado',$datos);
					?>
					
                      
                     <br>
                    
                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Guardar</button> 
                      <a href="<?php echo $destino; ?>" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#fechaEmision').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('#fechaVencimiento').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();	
  });
</script>