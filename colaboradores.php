<?php
  $seccionActiva=17;
  include_once('cabecera.php');
  
  $res=false;
  if(isset($_POST['codigo'])){
	if(isset($_POST['iban'])){
		$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
	}
    $res=actualizaDatos('colaboradores');
	$res=$res && insertaHistoricoColaborador($_POST['codigo']);
	insertaServiciosColaborador($_POST['codigo']);
  }
  elseif(isset($_POST['empresa'])){
	$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
    $res=insertaDatos('colaboradores');
	$res=insertaDatosLOPD('colaboradores');
	$codigoColaborador=$res;
	$res=$res && insertaHistoricoColaborador($codigoColaborador);
	insertaServiciosColaborador($codigoColaborador);
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('colaboradores');
  }

  $estadisticas=estadisticasGenericas('colaboradores');
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de colaboradores</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Colaboradores registrados</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
		<?php if($_SESSION['tipoUsuario']!='TELECONCERTADOR' && $_SESSION['tipoUsuario']!='MARKETING'){	?>
			<div class="span6">
			  <div class="widget" id="target-2">
				<div class="widget-header"> <i class="icon-cog"></i>
				  <h3>Gestión de colaboradores</h3>
				</div>
				<!-- /widget-header -->
				<div class="widget-content">
				  <div class="shortcuts">
							<a href="creaColaborador.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Añadir</span> </a>
					<?php compruebaOpcionEliminar(); ?>
				  </div>
				  <!-- /shortcuts --> 
				</div>
				<!-- /widget-content --> 
			  </div>
			</div>
		<?php }?>
		

      <div class="span12">
        
        <?php
          mensajeResultado('empresa',$res,'colaborador');//Fusionamos el mensaje de inserción y actualización
          mensajeResultado('elimina',$res,'colaborador', true);
        ?>
		    
        <div class="widget widget-table action-table cajaSelect">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Colaboradores registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class='table table-striped table-bordered datatable'>
                <thead>
                  <tr>
					<th> ID </th>
                    <th> Denominación </th>
                    <th> Teléfono </th>
					<th> eMail </th>
					<th> Persona contacto </th>
					<th> Comercial asignado </th>
          <th> Fecha firma del convenio </th>
          <th> Población </th>
					<?php if($_SESSION['tipoUsuario']!='TELECONCERTADOR' && $_SESSION['tipoUsuario']!='MARKETING'){	?>
						<th class='centro'></th>
						<th><input type='checkbox' id='todo'></th>
					<?php } ?>
                  </tr>
                </thead>
                <tbody>
					<?php imprimeColaboradores(); ?>
					
				</tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes un cliente.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('colaboradores.php',valoresChecks,'post');
      }

    });
</script>