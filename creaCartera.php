<?php
  $get='';
  if(isset($_GET['cuentas']) && $_GET['cuentas']=='SI'){
	$seccionActiva=2;
	$get='?cuentas=SI';
  }else{
	$seccionActiva=1;
  }
  include_once('cabecera.php');
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  

?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nueva cartera comercial</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="carteras.php<?php echo $get; ?>" method="post" enctype="multipart/form-data">
                  <fieldset>
                    
                    <?php
						campoFecha('fechaSubida','Fecha de subida');
						$consulta="SELECT codigo, CONCAT(nombre, ' ', apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' ORDER BY nombre, apellidos;";
						campoSelectConsulta('codigoUsuario','Comercial',$consulta);
						campoRadio('tieneColaborador','¿Tiene colaborador?','NO');
						echo "<div id='colaboradorOculto'>";
							$consulta="SELECT codigo, empresa AS texto FROM colaboradores ORDER BY empresa;";
							campoSelectConsulta('colaborador','Colaborador',$consulta);
						echo "</div>";
						campoFichero('fichero','Cartera');
						if(isset($_GET['cuentas']) && $_GET['cuentas']=='SI'){
							campoOculto('SI','activo');
						}else{
							campoOculto('NO','activo');
						}
					?>


                     <br>
                    
                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary" id="registrar"><i class="icon-ok"></i> Registrar cartera</button> 
                      <a href="carteras.php<?php echo $get; ?>" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/filasVentas.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/bootstrap-filestyle.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
	$("#fichero").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$("#colaboradorOculto").css('display','none');
	$("input[name='tieneColaborador']").change(function(){
		if($("input[name='tieneColaborador']:checked").val()=='SI'){	
			$("#colaboradorOculto").css('display','block');
		}else{
			$("#colaboradorOculto").css('display','none');
		}
	});
 });
</script>
