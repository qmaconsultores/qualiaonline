<?php
  $seccionActiva=13;
  include_once('cabecera.php');

  $anio=$_POST['anio'];
  $where="WHERE fechaValidez LIKE'$anio%'";
  $estadisticas=estadisticasGenericasWhere('trabajos',$where);
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para consultoría</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-cogs"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Trabajos registrados</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
		<?php  if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){ ?>

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Consultoría</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
			    <a href="trabajos.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		<?php } ?>

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Trabajos registrados</h3>
			  <div class="pull-right">
               <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
             </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php filtroTrabajosArchivos(); ?>
              <table class="table table-striped table-bordered datatable" id="tablaTrabajos">
                <thead>
                  <tr>
                    <th> ID Cliente </th>
                    <th> Proyecto </th>
                    <th> Cliente </th>
                    <th> Estado </th>
					<th> Fecha Máx. Entrega </th>
					<th> Formación </th>
					<th> PRL </th>
					<th> LOPD </th>
					<th> LSSI </th>
					<th> ALER </th>
					<th> WEB </th>
          <th> A. LOPD </th>
          <th> A. PRL </th>
                  </tr>
				  <!--tr class="segundaFila">
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				  </tr-->
                </thead>
                <tbody>

          				<?php
          					//imprimeTrabajos($where);
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<script type="text/javascript" src="js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.cajaFiltros select').selectpicker();
		oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaTrabajos');
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');realizaBusquedaFiltrada('#cajaFiltros','#tablaTrabajos');});
	});

	var tabla=$('.datatable').DataTable({
		'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoTrabajosArchivo.php?action=getMembersAjx&anio=<?php echo $_POST['anio']; ?>',
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          $('td:eq(11)', nRow).addClass( "centro" );
       },
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });

	var tabla=$('#tablaTrabajos').dataTable();

	
</script>