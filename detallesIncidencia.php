<?php
  $seccionActiva=4;
  include_once('cabecera.php');

  $datos=datosIncidencias($_GET['codigo']);
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Datos de la incidencia</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="incidencias.php" method="post">
                  <fieldset>
                    

                    <input type="hidden" name="codigo" value="<?php echo $datos['codigo']; ?>">

                    <div class="control-group">                     
                      <label class="control-label" for="codigoCliente">Empresa:</label>
                      <div class="controls">
                        <?php selectClientes($datos['codigoCliente']); ?>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


					<?php
						campoRadio('tipoIncidencia','Tipo',$datos,array('Incidencia','Falta datos'),array('Incidencia','Falta'));
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="descripcion">Descripción:</label>
                      <div class="controls">
                        <textarea name="descripcion" class="areaInforme" id="descripcion"><?php echo $datos['descripcion']; ?></textarea>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="departamento">Departamento:</label>
                      <div class="controls">
                        
                        <select name="departamento" class='selectpicker show-tick'>
                          <option value="ADMINISTRACION" <?php if($datos['departamento']=='ADMINISTRACION'){ echo 'selected="selected"'; } ?> >Administración</option>
                          <option value="TUTORES" <?php if($datos['departamento']=='TUTORES'){ echo 'selected="selected"'; } ?> >Tutores</option>
                          <option value="COMERCIAL" <?php if($datos['departamento']=='COMERCIAL'){ echo 'selected="selected"'; } ?> >Comercial</option>
                          <option value="IMPLANTACION" <?php if($datos['departamento']=='IMPLANTACION'){ echo 'selected="selected"'; } ?> >Implantación</option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->




                    <div class="control-group">                     
                      <label class="control-label" for="prioridad">Prioridad:</label>
                      <div class="controls">
                        
                        <select name="prioridad" class='selectpicker show-tick'>
                          <option value="ALTA" data-content="<span class='label label-danger'>Alta</span>" <?php if($datos['prioridad']=='ALTA'){ echo 'selected="selected"'; } ?> ></option>
                          <option value="NORMAL" data-content="<span class='label label-info'>Normal</span>" <?php if($datos['prioridad']=='NORMAL'){ echo 'selected="selected"'; } ?> ></option>
                          <option value="BAJA" data-content="<span class='label'>Baja</span>" <?php if($datos['prioridad']=='BAJA'){ echo 'selected="selected"'; } ?> ></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="estado">Estado:</label>
                      <div class="controls">
                        
                        <select name="estado" class='selectpicker show-tick'>
                          <option value="PENDIENTE" data-content="<span class='label label-danger'>Pendiente</span>" <?php if($datos['estado']=='PENDIENTE'){ echo 'selected="selected"'; } ?> ></option>
                          <option value="ENPROCESO" data-content="<span class='label label-warning'>En proceso</span>" <?php if($datos['estado']=='ENPROCESO'){ echo 'selected="selected"'; } ?> ></option>
                          <option value="RESUELTA" data-content="<span class='label label-success'>Resuelta</span>" <?php if($datos['estado']=='RESUELTA'){ echo 'selected="selected"'; } ?> ></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar incidencia</button> 
                      <a href="incidencias.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});

    $('#todoDia').change(function(){
      
      if($(this).prop('checked')){
        $('#horaInicio').prop('disabled', true);
        $('#horaFin').prop('disabled', true);
      }
      else{
        $('#horaInicio').prop('disabled', false);
        $('#horaFin').prop('disabled', false);
      }

    });
  });
</script>
