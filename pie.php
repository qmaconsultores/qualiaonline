<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; 2014 <a href="http://www.grupqualia.es/es/inicio" target="_blank">Grupqualia</a>.
          Software desarrollado por <a href="http://www.qmaconsultores.com" target="_blank">Q&amp;MA Consultores</a>.</div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 

<!-- Javascript
================================================== -->
<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script src="js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
	var numIncidencias=<?php echo $_SESSION['numIncidencias']; ?>;
	if(numIncidencias>0){
		$( "#aviso" ).addClass( "badge" );
		$( "#aviso" ).html( numIncidencias );
	}else{
		$( "#aviso" ).removeClass( "badge" );
		$( "#aviso" ).html('');
	}
	
	$('input').keypress(function(e){//SOLO PARA INNOFFICES: PREVIENE QUE CUALQUIER FORMULARIO SE ENVÍE AL PULSAR ENTER EN LOS CAMPOS
		if(e.which==13){
		  e.preventDefault();
		}
	  });

	  var pendientes=<?php compruebaMensajesPorLeer(); ?>;
	  if(pendientes>0){
		$('.avisoMensajes').addClass('badge').text(pendientes);
	  }
</script>
</body>
</html>
