<?php
  $seccionActiva=14;
  include_once("cabecera.php");
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-euro"></i><i class="icon-chevron-right"></i><i class="icon-filter"></i>
              <h3>Filtrado de facturas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              <h3>Seleccione las opciones de filtrado de facturas:</h3><br><br>
				<form action='facturacionFiltrado.php' method='post'>
					Servicio: 
						<?php 
							echo "<select name='servicio' id='servicio' class='span3 show-tick'>
								<option value='todos'>Todos</option>";
		
								$consulta=consultaBD("SELECT codigo, nombreProducto AS texto FROM productos ORDER BY codigo;",true);
								while($datos=mysql_fetch_assoc($consulta)){
									echo "<option value='".$datos['codigo']."'";

									echo ">".$datos['texto']."</option>";
								}
								
							echo "</select>";
						?>
					<br>
					<div class='control-group'>                     
					  <div class='controls'>
						Vencimiento del: &nbsp;<input type='text' class='input-small datepicker hasDatepicker' id='fechaUno' name='fechaUno' value='<?php echo imprimeFecha(); ?>'> 
						Al: <input type='text' class='input-small datepicker hasDatepicker' id='fechaDos' name='fechaDos' value='<?php echo imprimeFecha(); ?>'>
					  </div> <!-- /controls -->       
					</div> <!-- /control-group -->
					                   
					<div class='control-group'>                     
					  <div class='controls'>
						Devuelta: &nbsp;&nbsp;
						<label class='radio inline'>
							<input type='radio' name='devuelta' id='devuelta' value='NO' checked='checked'>No
						</label>
						<label class='radio inline'>
							<input type='radio' name='devuelta' id='devuelta' value='SI'>Si
						</label>
					  </div>
					</div>
					
					<div class='control-group'>                     
					  <div class='controls'>
						Pagada: &nbsp;&nbsp;
						<label class='radio inline'>
							<input type='radio' name='pagada' id='pagada' value='NO' checked='checked'>No
						</label>
						<label class='radio inline'>
							<input type='radio' name='pagada' id='pagada' value='SI'>Si
						</label>
					  </div>
					</div>
					<br>
					
					<button type="submit" class="btn btn-primary">Seleccionar <i class="icon-circle-arrow-right"></i></button>
				</form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();

  });
</script>