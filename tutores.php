<?php
  $seccionActiva=5;
  include_once('cabecera.php');
  
  if(isset($_POST['codigo'])){
    $res=actualizaTutor();
  }
  elseif(isset($_POST['apellidos'])){
    $res=registraTutor();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaTutor();
  }

  $estadisticas=creaEstadisticasTutores();
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para los tutores</h6>

                   <div id="big_stats" class="cf">

                     <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Tutores registrados</div>
                      <!-- .stat -->

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de tutores</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="formacion.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
                <a href="creaTutor.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Añadir Tutor</span> </a>
                <?php 
                compruebaOpcionEliminar();
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Tutor actualizado correctamente."); 
            }
            else{
              mensajeError("no se ha podido actualizar el tutor. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['apellidos'])){
            if($res){
              mensajeOk("Tutor registrado correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar el tutor. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el tutor. Contacte con el webmaster."); 
            }
          }
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Tutores registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Apellidos, nombre </th>
                    <th> Dni </th>
                    <th> Teléfono </th>
                    <th> Mail </th>
                    <th class="centro"> </th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeTutores();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('tutores.php',valoresChecks,'post');
    }

  });

</script>
