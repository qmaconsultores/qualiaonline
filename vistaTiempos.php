<?php
  $seccionActiva=16;
  include_once('cabecera.php');
  
  $estadisticas=estadisticasGenericas('tareasSoftware');
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas sobre Software</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-time"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Actividades realizadas</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Actividades</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="software.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Activiades para cada Proyecto registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Cliente </th>
                    <th> Tipo Tarea </th>
                    <th> Programador </th>
                    <th> Actividad </th>
                    <th> Fecha de Inicio </th>
                    <th> Hora de Inicio </th>
                    <th> Fecha prevista de finalizción </th>
                    <th> Fecha de Fin </th>
                    <th> Hora de Fin </th>
                    <th> Tiempo empleado </th>
                    <th class="centro"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeTiempos();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes una tarea del listado.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('software.php',valoresChecks,'post');
      }

    });
</script>