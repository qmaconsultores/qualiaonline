<?php
  $seccionActiva=$_GET['seccion'];
  include_once('cabecera.php');

  $codigo=$_GET['codigo'];

  $datos=datosRegistro('facturacion',$codigo);

  $cliente=datosRegistro('clientes',$datos['codigoCliente']);
  $colaborador=datosRegistro('colaboradores',$cliente['comercial']);
  
  $destino='facturacion.php';
  if($_GET['seccion']=='7'){
	$destino='informesFacturacion.php';
  }

?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Detalles de la factura <?php echo $datos['referencia'].'/'.substr($datos['anio'], 2); ?> para el cliente <?php echo "<a href='detallesCuenta.php?codigo=".$codigo."'>".$cliente['empresa']."</a>";?></h3>
			  <?php 
					echo "<a href='enviarEmail.php?codigoFactura=$codigo&seccion=14'><span class='label label-success' id='avisoCorreo'><i class='icon-envelope blanco'></i> Enviar</span></a>";
			  ?>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="<?php echo $destino; ?>" method="post">
                  <fieldset>
				  
					<div class="control-group">                     
                      <label class="control-label" for="empresa">Empresa:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="empresa" name="empresa" value="<?php echo $cliente['empresa']; ?>" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php if($cliente['tieneColaborador']=='SI'){ ?>
					<div class="control-group">                     
                      <label class="control-label" for="empresa">Colaborador:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="empresa" name="empresa" value="<?php echo $colaborador['empresa']; ?>" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					<?php } ?>
					
					<div class="control-group">                     
                      <label class="control-label" for="contacto">Persona de contacto:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="contacto" name="contacto" value="<?php echo $cliente['contacto']; ?>" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono" value="<?php echo $cliente['telefono']; ?>" maxlength="9" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="mail">Mail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="mail" name="mail" value="<?php echo $cliente['mail']; ?>" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
                    
                    <div class="control-group" style="float:left;">                    
                      <label class="control-label" for="referencia">Referencia:</label>
                      <div class="controls">
                        <input type="hidden" name="codigo" value="<?php echo $datos['codigo']; ?>">
                        <input type="text" class="input-medium" readonly id="referencia" name="referencia" value="<?php echo $datos['referencia']; ?>">/<?php echo substr($datos['anio'],2); ?>
                      </div> <!-- /controls -->   
                    </div> <!-- /control-group -->
                    <?php
                    if($_SESSION['codigoS']==19 || $_SESSION['codigoS']==11 || $_SESSION['codigoS']==14){?>
                    <button style="margin-left: 10px;" class="btn btn-primary" id="bloqueoReferencia"><i class='icon-unlock'></i></button>  
                    <?php } ?> 
            		<br clear='both'>


                    <div class="control-group">                     
                      <label class="control-label" for="referencia">Año:</label>
                      <div class="controls">
                        <input type="text" class="input-medium" id="anio" name="anio" value="<?php echo $datos['anio']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="fechaEmision">Fecha emisión:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaEmision" name="fechaEmision" value="<?php echo formateaFechaWeb($datos['fechaEmision']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php

						campoSelect('tipoFacturaComercial','Tipo de factura',array('Cartera','Venta nueva','Auditoría','Televenta'),array('CARTERA','NUEVA','AUDITORIA','TELEVENTA'),$datos);				
					?>

					<h3 class="apartadoFormulario sinFlotar">Vencimientos</h3>
						Se guardarán aquellos vencimientos con importes definidos
						  <center>
						  <table class="table table-striped table-bordered datatable" id="tablaVencimientos">
							<thead>
							  <tr>
								<th> Fecha </th>
								<th> Importe € </th>
								<th> Enviada </th>
								<th> Cobrada </th>
							  </tr>
							</thead>
							<tbody>
								<?php
								  $vtos=consultaBD('SELECT * FROM facturas_vto WHERE codigoFactura='.$datos['codigo'],true);
								  $i=0;
								  while($vto=mysql_fetch_assoc($vtos)){
								  echo "<tr>";
								  campoFechaTabla('fecha'.$i,$vto['fecha']);
								  campoTextoTabla('importe'.$i,$vto['importe'],'input-small pagination-right');
								  campoSelect('enviada'.$i,'',array('No','Si'),array('NO','SI'),$vto['enviada'],'span2','data-live-search="true"',1);
								  campoSelect('cobrada'.$i,'',array('No','Si'),array('NO','SI'),$vto['cobrada'],'span2','data-live-search="true"',1);
								  campoOculto($vto['codigo'],'codigoExiste'.$i);
								  echo "</tr>";
								  $i++;
									}
								if($i==0){
										echo "<tr>";
								  campoFechaTabla('fecha'.$i);
								  campoTextoTabla('importe'.$i,'','input-small pagination-right');
								  campoSelect('enviada'.$i,'',array('No','Si'),array('NO','SI'),'','span2','data-live-search="true"',1);
								  campoSelect('cobrada'.$i,'',array('No','Si'),array('NO','SI'),'','span2','data-live-search="true"',1);
								  campoOculto('','codigoExiste'.$i);
								  echo "</tr>";
									}
								?>
							</tbody>
						  </table>
						  <br/>
						  </center>
						  <center>
                      		<button type="button" class="btn btn-success" onclick="insertaFila('tablaVencimientos');"><i class="icon-plus"></i> Añadir Vencimiento</button> 
                    		</center>
                    		<br/>
					
					<?php
						$consulta="SELECT codigo, firma AS texto FROM firmas ORDER BY codigo;";
						campoSelectConsulta('firma','Empresa (Firma)',$consulta,$datos);
						campoSelectConsulta('concepto','Concepto',"SELECT codigo, nombreProducto AS texto FROM productos ORDER BY codigo;",$datos);
						echo "<div id='cursoAsociado'>";
							$consulta="SELECT cursos.codigo, CONCAT(cursos.codigoInterno, ' ',accionFormativa.denominacion) AS texto FROM cursos INNER JOIN accionFormativa ON cursos.codigoaccionFormativa=accionFormativa.codigoInterno";
							echo '<div class="control-group">                     
							  <label class="control-label" for="estado">Curso/s:</label>
							  <div class="controls">';
								 selectCursos($datos['codigo']);
							  echo '</div> <!-- /controls -->       
							</div> <!-- /control-group -->';
							campoTextoSimbolo('gastosOrganizacion','Gastos de organización','€',$datos);
							campoTextoSimbolo('gastosImparticion','Gastos de impartición','€',$datos);
						echo "</div>";
						echo "<div id='tipoFacturaDescarga'>";
							campoSelect('tipoFactura','Tipo de documento',array('Mantenimiento','Auditoría','Consultoría','LSSI','PRL','APPCC-Alérgenos','Web legalizada','Dominio y correo electrónico','Dominio','E-commerce'), array('mantenimiento','auditoria','consultoria','lssi','prl','alergenos','web','dominiocorreo','dominio','commerce'),$datos);
						echo "</div>";
					?>



                    <div class="control-group">                     
                      <label class="control-label" for="coste">Coste:</label>
                      <div class="controls">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini pagination-right" id="coste" name="coste" value="<?php echo formateaNumero($datos['coste']); ?>">
                          <span class="add-on">€</span>
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    
                    <?php
						campoSelect('formaPago','Forma de pago',array('Transferencia','Efectivo','Cheque','Domiciliación bancaria','Tarjeta'),array('transferencia','efectivo','cheque','domiciliacion','tarjeta'),$datos);
						campoOculto(formateaFechaWeb($datos['fechaVencimiento']),'fechaVencimiento');
					?>



                    <div class="control-group">                     
                      <label class="control-label">¿Cobrada?</label>  
                        <div class="controls">
                        <label class="radio inline">
                          <input type="radio" name="cobrada" value="SI" <?php if($datos['cobrada']=='SI'){echo 'checked="checked"';}?> > Si
                        </label>

                        <label class="radio inline">
                          <input type="radio" name="cobrada" value="NO" <?php if($datos['cobrada']=='NO'){echo 'checked="checked"';}?> > No
                        </label>

                      </div>  
                    </div> <!-- /control-group -->
					
					<?php
						campoRadio('enviada','¿Enviada banco?',$datos['enviada']);
						campoRadio('enviadaCliente','¿Enviada cliente?',$datos['enviadaCliente']);
						campoRadio('devuelta','¿Devuelta?',$datos['devuelta']);
						$vtos=consultaBD('SELECT * FROM devoluciones_facturas WHERE codigoFactura='.$datos['codigo'],true);
						if(mysql_num_rows($vtos)>0){
							echo "<div id='devolucion'>";
						} else {
							echo "<div id='devolucionOculta'>";
						}
						?>
							Se guardarán aquella devoluciones con fecha definida
						  <center>
						  <table class="table table-striped table-bordered datatable" id="tablaDevoluciones">
							<thead>
							  <tr>
								<th> Fecha </th>
								<th> Motivo </th>
								<th> Resolución </th>
							  </tr>
							</thead>
							<tbody>
								<?php
								  $motivos=consultaBD("SELECT codigo, motivo AS texto FROM motivos ORDER BY motivo",true);
								  $nombres=array('');
								  $valores=array('');
								  while($motivo=mysql_fetch_assoc($motivos)){
								  	array_push($nombres, $motivo['texto']);
								  	array_push($valores, $motivo['codigo']);
								  }
								  $i=0;
								  while($vto=mysql_fetch_assoc($vtos)){
								  echo "<tr>";
								  campoFechaTabla('fechaDevolucion'.$i,$vto['fecha']);
								  campoSelect('motivo'.$i,'',$nombres,$valores,$vto['motivo'],'span3','data-live-search="true"',1);
								  campoSelect('resolucionFactura'.$i,'',array('','Remesada','Transferencia','Efectivo','Anulada'),array('','Remesada','Transferencia','Efectivo','Anulada'),$vto['resolucionFactura'],'span3','data-live-search="true"',1);
								  echo "</tr>";
								  $i++;
									}
									if($i==0){
										 echo "<tr>";
								  campoFechaTabla('fechaDevolucion'.$i,'0000-00-00');
								  campoSelect('motivo'.$i,'',$nombres,$valores,'','span3','data-live-search="true"',1);
								  campoSelect('resolucionFactura'.$i,'',array('','Remesada','Transferencia','Efectivo','Anulada'),array('','Remesada','Transferencia','Efectivo','Anulada'),false,'span3','data-live-search="true"',1);
								  echo "</tr>";
									}
								?>
							</tbody>
						  </table>
						  <br/>
						  </center>
						  <center>
                      		<button type="button" class="btn btn-success" onclick="insertaFila('tablaDevoluciones');"><i class="icon-plus"></i> Añadir Devolución</button> 
                    		</center>
                    		<br/>
                    		<?php
							/*campoSelectConsulta('motivoDevolucion','Motivo devolución',"SELECT codigo, motivo AS texto FROM motivos ORDER BY motivo;",$datos);
							campoSelect('resolucionFactura','Resolución',array('','Remesada','Transferencia','Efectivo','Anulada'),array('','Remesada','Transferencia','Efectivo','Anulada'),$datos);*/

							campoOculto('','motivoDevolucion');
							campoOculto('','resolucionFactura');
						echo "</div>";
						areaTexto('observaciones','Observaciones','','areaInforme');
						$consulta=consultaBD("SELECT * FROM historico_facturas WHERE codigoFactura='$codigo' ORDER BY fecha;",true);
						$datosHistorico=mysql_fetch_assoc($consulta);
						$historico='';
						if(isset($datosHistorico['codigo'])){
							while(isset($datosHistorico['codigo'])){
								$historico=$historico.formateaFechaWeb($datosHistorico['fecha'])." ".$datosHistorico['hora']." ".$datosHistorico['observaciones']." \n\n";
								$datosHistorico=mysql_fetch_assoc($consulta);
							}
						}
						areaTexto('historicoObservaciones','Histórico Observaciones',$historico,'areaInforme',true);
					?>
					
                      
                     <br>
                    
                      
                    <div class="form-actions">
                      <button type="submit" id='registrar' class="btn btn-primary"><i class="icon-refresh"></i> Guardar cambios</button> 
                      <a href="<?php echo $destino; ?>" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#fechaEmision').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('#fechaVencimiento').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();
	var valor=$('input[name=devuelta]:checked').val();
	compruebaDevuelta(valor);
	
	$('#registrar').click(function(e){
		e.preventDefault();
		var coste=parseFloat($('#coste').val());
		var i=0;
		var recibos=0;
		while($("#importe"+i).length){
			recibos=recibos+parseFloat($("#importe"+i).val());
			i++;
		}
		if(coste == recibos){
			$('form').submit();
		} else {
			alert('El coste y la suma de los importes de los recibos no coincide');
			alert('Coste: '+coste+' - Recibos: '+recibos)
		}
	})
	<?php if($datos['concepto']=='14'){ 
			echo "$('#cursoAsociado').css('display','block');
			$('#tipoFacturaDescarga').css('display','none');";
		}else{
			echo "$('#cursoAsociado').css('display','none');
			$('#tipoFacturaDescarga').css('display','block');";
		}
	?>

	$('#bloqueoReferencia').click(function(e){
		e.preventDefault();
		var estado=$('#referencia').attr('readonly');
		if(estado=='readonly'){
			$('#referencia').attr('readonly',false);
			$('#bloqueoReferencia i').removeClass('icon-unlock');
			$('#bloqueoReferencia i').addClass('icon-lock');
		} else {
			$('#referencia').attr('readonly',true);
			$('#bloqueoReferencia i').removeClass('icon-lock');
			$('#bloqueoReferencia i').addClass('icon-unlock');
		}
	});
	
	$('select[name=concepto]').change(function() {
        if (this.value == '14') {
            $("#cursoAsociado").css('display','block');
			$("#tipoFacturaDescarga").css('display','none');
        }
        else {
            $("#cursoAsociado").css('display','none');
			$("#tipoFacturaDescarga").css('display','block');
			if(this.value == 'prl'){
				$('#tipoFactura').val('prl');
				$('.selectpicker').selectpicker('refresh');
			}
			if(this.value == 'alergenos'){
				$('#tipoFactura').val('alergenos');
				$('.selectpicker').selectpicker('refresh');
			}
			if(this.value == 'web'){
				$('#tipoFactura').val('web');
				$('.selectpicker').selectpicker('refresh');
			}
			if(this.value == 'lopd'){
				$('#tipoFactura').val('commerce');
				$('.selectpicker').selectpicker('refresh');
			}
        }
    });
	$('input[type=radio][name=devuelta]').change(function() {
        if (this.value == 'SI') {
			$('input:radio[name=cobrada][value=NO]').click();
        }
    });
	
	$('#totalUno').focusout(function(){
		validacion('totalUno');
	});
	
	$('#totalDos').focusout(function(){
		validacion('totalDos');
	});
	
	$('#totalTres').focusout(function(){
		validacion('totalTres');
	});

	$('#importe0').focusout(function(){
		if(confirm('¿Quieres cambiar además el importe de la factura?')){
			$('#coste').val($('#importe0').val());
		}
	});

	$('#fecha0').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
		if(confirm('¿Quieres cambiar además la fecha de la factura?')){
			$('#fechaVencimiento').val($(this).val());
		}
    });

	
	
	$('input[type=radio][name=devuelta]').on('change', function() {
		var valor=$(this).val();
		compruebaDevuelta(valor);
	});
	
  });
  
  function validacion(id){
	var coste=parseFloat($('#coste').val().replace(',','.'));
	if(isNaN(coste)){
		coste=0;
	}
	var totalUno=parseFloat($('#totalUno').val().replace(',','.'));
	if(isNaN(totalUno)){
		totalUno=0;
	}
	var totalDos=parseFloat($('#totalDos').val().replace(',','.'));
	if(isNaN(totalDos)){
		totalDos=0;
	}
	var totalTres=parseFloat($('#totalTres').val().replace(',','.'));
	if(isNaN(totalTres)){
		totalTres=0;
	}
	if((totalUno+totalDos+totalTres)>coste){
		alert("La suma de los importes no puede ser mayor al total");
		if(id=='totalUno'){
			$('#'+id).val((coste-totalDos-totalTres).toFixed(2));
		}else if(id=='totalDos'){
			$('#'+id).val((coste-totalUno-totalTres).toFixed(2));
		}else if(id=='totalTres'){
			$('#'+id).val((coste-totalUno-totalDos).toFixed(2));
		}
	}	
  }
  
  function compruebaDevuelta(valor){
	if(valor=='SI'){
		$('#devolucionOculta').css('display','block');
	}else{
		$('#devolucionOculta').css('display','none');
	}
  }
</script>