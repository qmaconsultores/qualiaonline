<?php
  $seccionActiva=16;
  include_once("cabecera.php");
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nueva tarea de Software</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="software.php" method="post">
                  <fieldset>

                    <?php
                      $consulta="SELECT codigo, empresa AS texto FROM clientes";
                      campoSelectConsulta('codigoCliente','Cliente',$consulta);
                      campoSelect('tipoTarea','Tipo de tarea',array('Creación de Software','Personalización','Modificaciones y/o mejoras','Incidencias'),array('CREACION','DEMO','MODIFICACION','INCIDENCIA'));
                      campoFecha('fechaSolicitud','Fecha de solicitud');
                      campoFecha('fechaPrevista','Fecha de prevista de finalización');
                      $consulta="SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' AND tipo='PROGRAMADOR';";
                      campoSelectConsulta('codigoUsuario','Programador asignado',$consulta);
                      campoRadio('realizado','Estado','1',array('Pendiente','En desarrollo','Finalizado'),array('1','2','3'));
                      
                      echo "<div id='cajaRealizado'>";
                        campoFecha('fechaFinalizacion','Fecha de finalización');
                        campoTexto('tiempoEmpleado','Tiempo empleado','','input-small');
                      echo "</div>";

                      areaTexto('observaciones','Observaciones');

                    ?>
                    
                    <br />                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Registrar Tarea</button> 
                      <a href="software.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    $('.selectpicker').selectpicker();
    $('#cajaRealizado').css('display','none');

    $('input[name=realizado]').change(function(){
      if($(this).val()=='3'){
        $('#cajaRealizado').css('display','block');
      }
      else{
        $('#cajaRealizado').css('display','none');
      }

    })

  });
</script>