<?php
  $seccionActiva=2;
  include_once('cabecera.php');
  
  if(isset($_POST['codigo'])){
	if(isset($_POST['iban'])){
		$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
	}
	if($_POST['tieneColaborador']=='NO'){
		$_POST['comercial']='NULL';
	}
    $res=actualizaCliente();
  }
  elseif(isset($_POST['completaAlumnos'])){
    $res=completaAlumnos();
  }
  elseif(isset($_POST['codigoC'])){
   $res=registraVenta();
  }
  elseif(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaCliente();
  }
  
  $where="WHERE 1=1";
  if($_SESSION['tipoUsuario']!='FORMACION'){
	$where=compruebaPerfilParaWhere();
  }
  $estadisticas=estadisticasGenericasWhere('clientes',$where.' AND baja="SI"');
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema sobre clientes</h6>

                    <div id="big_stats" class="cf">

                     <div class="stat"> <i class="icon-exclamation"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Clientes dados de baja</div>
                      <!-- .stat -->

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de cuentas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="#" id='email' class="shortcut"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Enviar eMail</span> </a>
                <a href="#" id='tareas' class="shortcut"><i class="shortcut-icon icon-list-ol"></i><span class="shortcut-label">Añadir Tarea</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Datos del cliente actualizados."); 
            }
            else{
              mensajeError("no se han podido actualizar los datos del cliente. Compruebe los datos introducidos."); 
            }
          }
		  elseif(isset($_POST['completaAlumnos'])){
            if($res){
              mensajeOk("Venta registrada y alumnos completados."); 
            }
            else{
              mensajeError("no se ha podido completar los datos de los alumnos. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['codigoC'])){
  		      if($res){
              mensajeOk("Venta registrada y cliente establecido."); 
            }
            else{
              mensajeError("no se ha podido registrar la venta. Compruebe los datos introducidos."); 
            }
		      }
          elseif(isset($_POST['destinatarios'])){
            if($res){
              mensajeOk("Mensaje enviado correctamente."); 
            }
            else{
              mensajeError("no se ha podido enviar el mensaje. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el posible cliente. Contacte con el webmaster."); 
            }
          }
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Cuentas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
				 <table class='table table-striped table-bordered datatable' id='tablaCuentas'>
					<thead>
					  <tr>
						<th> ID </th>
						<th> Nombre del cliente </th>
						<th> CIF </th>
						<th> Población </th>
            <th> Provincia </th>
						<th> Comercial asignado </th>
						<th> Servicio Contratado </th>
						<th> Fecha baja </th>
						<th> Motivo de baja </th>
						<th class='td-actions'> </th>
						<th><input type='checkbox' id='todo'></th>
					  </tr>
					</thead>
					<tbody>
						<?php //imprimeCuentas(); ?>
					</tbody>
              </table>
                
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<!--script src="js/filtroTabla.js"></script-->
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/excanvas.min.js" type="text/javascript"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $('#tablaCuentas').dataTable({
      'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoClientesBaja.php?action=getMembersAjx',
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          $('td:eq(9)', nRow).addClass( "centro" );
       },
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });

  $('#email').click(function(){
    var valoresChecks=recorreChecks();
    creaFormulario('enviarEmail.php?seccion=2',valoresChecks,'post');
  });


  $('#tareas').click(function(){
      var valoresChecks=recorreChecks();
      creaFormulario('creaTarea.php',valoresChecks,'post');
  });


  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('clientesBaja.php',valoresChecks,'post');
    }

  });
</script>
