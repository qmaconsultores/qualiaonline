<?php
  $seccionActiva=14;
  include_once('cabecera.php');
  $res=false;
  
  $empresa='';
  if(isset($_GET['codigoCliente'])){
	$codigoCliente=$_GET['codigoCliente'];
	$consulta=consultaBD("SELECT empresa FROM clientes WHERE codigo='$codigoCliente';",true);
	$datosCliente=mysql_fetch_assoc($consulta);
	$empresa=$datosCliente['empresa'];
  }
  
  if(isset($_POST['codigo'])){
	$_POST['coste']=formateaNumero($_POST['coste'],true);
    $res=actualizaDatos('facturacion',$_POST['codigo']);
	$res=$res && insertaHistoricoFacturas($_POST['codigo']);
	$res=$res && insertaCursosFactura($_POST['codigo']);
  $res=$res && insertaVtoFacturas($_POST['codigo']);
  $res=$res && insertaDevolucionesFacturas($_POST['codigo']);
  }
  elseif(isset($_POST['referencia'])){
	$_POST['coste']=formateaNumero($_POST['coste'],true);
    $res=insertaDatos('facturacion');
	$codigoFactura=$res;
	//actualizaReferencia($codigoFactura);
	$res=$res && insertaHistoricoFacturas($codigoFactura);
	$res=$res && insertaCursosFactura($codigoFactura);
  $res=$res && insertaVtoFacturas($codigoFactura);
  }
  elseif(isset($_POST['firma1'])){
    $res=actualizaFirmas();
  }
  elseif(isset($_POST['motivo1'])){
    $res=actualizaMotivos();
  }
  elseif(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
	$res=enviaFacturaCliente($_SESSION['codigoFactura']);
  }
  elseif(isset($_POST['descarga'])){
	$i=0;
    $res=insertaDatos('remesas');
    $remesa=$res;
    while(isset($_POST['codigoLista'][$i])){
		$res=enviaFactura($_POST['codigoLista'][$i],$remesa);
		$i++;
	}
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('facturacion');
  }

  $estadisticas=creaEstadisticasFacturas();
  $pendientes=$estadisticas['facturacion']-$estadisticas['cobradas'];

  /*$facturas=consultaBD("SELECT * FROM facturacion WHERE motivoDevolucion != 'NULL'",true);
  while($factura=mysql_fetch_assoc($facturas)){
   //echo $factura['codigo'].'<br/>';
    echo 'INSERT INTO devoluciones_facturas VALUES(NULL,"'.$factura['codigo'].'","","'.$factura['motivoDevolucion'].'","'.$factura['resolucionFactura'].'");<br/>';
  }*/
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-tasks"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de facturación:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-ticket"></i> <span class="value"><?php echo $estadisticas['facturacion']?></span> <br>Facturas emitidas</div>
                    <!-- .stat -->
                    <div class="stat"> <i class="icon-credit-card"></i> <span class="value"><?php echo $estadisticas['cobradas']?></span> <br>Facturas percibidas</div>
                    <!-- .stat --> 
                     <div class="stat"> <i class="icon-time"></i> <span class="value"><?php echo $pendientes?></span> <br>Facturas pendientes de cobro</div>
                    <!-- .stat -->
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
		<?php  if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='CONSULTORIA' && $_SESSION['tipoUsuario']!='FORMACION' && $_SESSION['tipoUsuario']!='ADMINISTRACION'){ ?>
		
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de facturación</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="creaFactura.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Nueva Factura</span> </a>
						<a href="firmas.php" class="shortcut"><i class="shortcut-icon icon-pencil"></i><span class="shortcut-label">Firmas</span> </a>
						<a href="seleccionaFechas.php" class="shortcut"><i class="shortcut-icon icon-download-alt"></i><span class="shortcut-label">Documentación</span> </a>
						<a href="motivosDevolucion.php" class="shortcut"><i class="shortcut-icon icon-refresh"></i><span class="shortcut-label">Motivos devolución</span> </a>
						<br>
						<a href="filtrarFacturas.php" class="shortcut"><i class="shortcut-icon icon-filter"></i><span class="shortcut-label">Filtrar</span> </a>
						<a href="seleccionaFechasColaboradores.php" class="shortcut"><i class="shortcut-icon icon-group"></i><span class="shortcut-label">Colaboradores</span> </a>
						<a href="seleccionaFirma.php" class="shortcut"><i class="shortcut-icon icon-download"></i><span class="shortcut-label">Excel</span> </a>
						<?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		<?php } ?>

      <div class="span12">
        
        <?php 
          mensajeResultado('referencia',$res,'factura');//Fusionamos el mensaje de inserción y actualización
		  mensajeResultado('firma1',$res,'firmas');//Fusionamos el mensaje de inserción y actualización
		  mensajeResultado('motivo1',$res,'motivos');//Fusionamos el mensaje de inserción y actualización
          mensajeResultado('elimina',$res,'factura', true);
        ?>
        
        <div class="widget widget-table action-table cajaSelect">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Facturas emitidas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable" id="tablaFacturacion">
                <thead>
                  <tr>
					<th> Referencia </th>
					<th> Cliente </th>
                    <th> Concepto </th>
                    <th> Fecha de emisión </th>
					<th> Fecha de vencimiento </th>
					<th> Importe </th>
					<th> ¿Cobrada? </th>
					<th> ¿Enviada banco? </th>
					<th> ¿Enviada cliente? </th>
					<th> Devolución y resolución </th>
                    <th class="centro"> </th>
					<th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    //imprimeFacturas();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
	<?php
	echo "var marcado='".$empresa."';";
  ?>
    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes una factura.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('facturacion.php',valoresChecks,'post');
      }

    });
	
	var tabla=$('.datatable').DataTable({
		"aaSorting": [],
      'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoFacturacion.php?action=getMembersAjx',
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		  $('td:eq(7)', nRow).addClass( "centro" );
		  $('td:eq(8)', nRow).addClass( "centro" );
		  $('td:eq(9)', nRow).addClass( "centro" );
		  $('td:eq(10)', nRow).addClass( "centro" );
	   },
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });

    var tabla=$('#tablaFacturacion').dataTable();
    tabla.fnFilter(marcado);
</script>