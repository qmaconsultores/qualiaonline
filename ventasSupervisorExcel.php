<?php
	session_start();
	include_once('funciones.php');
	compruebaSesion();
	
	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');
	require_once('phpexcel/PHPExcel/IOFactory.php');

	//global $_CONFIG;
	
	//Carga de la plantilla
	$objReader=new PHPExcel_Reader_Excel2007();
	$objPHPExcel=$objReader->load("documentos/plantillaVentasSupervisor.xlsx");
	$sheet=0;
	$objPHPExcel->setActiveSheetIndex($sheet);//Selección hoja
	$objPHPExcel->getActiveSheet()->setTitle('Ventas');//Cambio nombre de hoja


	conexionBD();
	$firmas=array('supervisor'=>1,'supervisor2'=>2,'supervisor3'=>3);
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.fechaEmision, facturacion.cobrada, facturacion.fechaVencimiento, facturacion.referencia, facturacion.coste, 
	clientes.empresa, facturacion.enviada, facturacion.concepto, facturacion.enviadaCliente, clientes.codigo AS codigoCliente, insercion, facturacion.firma, 
	facturacion.devuelta, productos.nombreProducto, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS comercial
		FROM facturacion LEFT JOIN clientes ON clientes.codigo=facturacion.codigoCliente 
		LEFT JOIN productos ON facturacion.concepto=productos.codigo
		LEFT JOIN ventas ON facturacion.codigoVenta=ventas.codigo
		LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo WHERE firma=".$firmas[$_GET['codigo']]." AND fechaEmision>='2015-03-01' AND fechaEmision<='2015-12-31'
		ORDER BY insercion DESC;");
	cierraBD();
	$fila=4;
	while($datos=mysql_fetch_assoc($consulta)){
		$fecha=formateaFechaWeb($datos['fechaEmision']);
		$fechaVencimiento=formateaFechaWeb($datos['fechaVencimiento']);
		if($datos['concepto']=='14'){ 
				if($datos['firma']!='3'){
					$concepto="F";
				}else{
					$concepto="S";
				}
			}else{
				if($datos['firma']!='3'){
					$concepto="C";
				}else{
					$concepto="S";
				}
			}
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$fila,$concepto.'-'.$datos['referencia']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$fila,$datos['empresa']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$fila,$datos['comercial']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$fila,$datos['nombreProducto']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$fila,$fecha);
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$fila,$fechaVencimiento);
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$fila,formateaNumero($datos['coste'])." €");
		$fila++;
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	

	$objWriter=new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('documentos/Ventas.xlsx');

	// Definir headers
	header("Content-Type: application/ms-xlsx");
	header("Content-Disposition: attachment; filename=Ventas.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/Ventas.xlsx');
	
?>