<?php
  $seccionActiva=3;
  include_once('cabecera.php');

  $empresa='';
  if(isset($_GET['codigoCliente'])){
	$codigoCliente=$_GET['codigoCliente'];
	$consulta=consultaBD("SELECT empresa FROM clientes WHERE codigo='$codigoCliente';",true);
	$datosCliente=mysql_fetch_assoc($consulta);
	$empresa=$datosCliente['empresa'];
  }
  
  $res=false;
  if(isset($_GET['realizaTarea'])){
    $res=realizaTarea($_GET['realizaTarea']);
  }
  
  if(isset($_POST['codigo'])){
	$res=actualizaTarea();
  }
  elseif(isset($_POST['tareaAnterior'])){
	$res=programaTarea();
  }
  elseif(isset($_POST['fechaInicio'])){
	$res=creaTarea();
  }
  elseif(isset($_POST['reasignacion'])){
	$res=reasignaTareas();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaTareas('tareas');
  }
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      
      <div class="row">
        
        <?php
		  if(isset($_GET['realizaTarea'])){
			if($res){
				mensajeOk("Tarea realizada correctamente");
			}else{
				mensajeError("Se ha producido un error. Contacte con el webmaster");
			}
		  }
          mensajeResultado('fechaInicio',$res,'tarea');
		  mensajeResultado('reasignacion',$res,'tarea');
          mensajeResultado('elimina',$res,'tarea', true);
        ?>


        <div class="span12">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-calendar"></i>
               <h3>Agenda de Tareas</h3>
            </div>
              <!-- /widget-header -->
            <div class="widget-content">
             
              <div id='calendar'></div>
              <!-- /widget-content --> 
            </div>
          </div>
        </div>

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container tareas">
                <div class="widget-content">
                  <h6 class="bigstats tareas">Estadísticas de tareas</h6>

                   <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
              
                   <div class="leyenda" id='leyenda'>
					<span class="grafico grafico3Borde"></span>Pendientes: <span id="valor1"></span><br>
                    <span class="grafico grafico9Borde"></span>Realizadas: <span id="valor2"></span><br>
				   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
        </div>


        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Tareas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="creaTarea.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Nueva tarea</span> </a>
				<a href="filtrarTareas.php" class="shortcut"><i class="shortcut-icon icon-filter"></i><span class="shortcut-label">Filtrar</span> </a>
				<a href="seleccionaAnioTareas.php" class="shortcut"><i class="shortcut-icon icon-archive"></i><span class="shortcut-label">Histórico</span> </a>
                <a href="#" id="reasignarTareas" class="shortcut"><i class="shortcut-icon icon-refresh"></i><span class="shortcut-label">Reasignar</span> </a>
				<br>
				<?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      </div>


       <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-th-list"></i>
          <h3>Tareas pendientes</h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable" id="tablaUno">
            <thead>
              <tr>
                <th> Empresa </th>
				<th> Usuario </th>
                <th> Localidad </th>
				<th> Contacto </th>
                <th> Teléfono </th>
				<th> Móvil </th>
                <th> Tarea </th>
                <th> Fecha de Inicio </th>
				<th> Hora </th>
                <th> Resolución </th>
                <th class="centro"> </th>
                <th><input type='checkbox' id="todo"></th>
              </tr>
            </thead>
            <tbody>

              <?php
                //imprimeTareas(true);
              ?>
            
            </tbody>
          </table>
        </div>
        <!-- /widget-content-->
      </div>



      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-th-list"></i>
          <h3>Tareas realizadas</h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable" id="tablaDos">
            <thead>
              <tr>
                <th> Empresa </th>
				<th> Usuario </th>
                <th> Localidad </th>
				<th> Contacto </th>
                <th> Teléfono </th>
				<th> Móvil </th>
                <th> Tarea </th>
                <th> Fecha de Inicio </th>
				<th> Hora </th>
                <th> Resolución </th>
                <th class="centro"> </th>
                <th><input type='checkbox' id="todo"></th>
              </tr>
            </thead>
            <tbody>

              <?php
                //imprimeTareas();
              ?>
            
            </tbody>
          </table>
        </div>
        <!-- /widget-content-->
      </div>

      <!--Popup -->
      <div class="span6 hide" id="ventanaFlotante">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-share-alt icon-calendar"></i><i class="icon-chevron-right"></i><i class="icon-plus-sign"></i>
              <h3>Nueva entrada en la Agenda</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" method="post">
                  <fieldset>
                    
                    <?php
                      campoTexto('tarea','Tarea','','input-large');
                      campoSelect('prioridad','Resolución',array('Sin contactar','En proceso','Reconcertar','Concertada','Vendido','Visitado','Pendiente','Baja'),array('sincontactar','proceso','reconcertar','concertada','alta','visitado','normal','baja'),'normal','selectpicker span2 show-tick',"");
					  echo "<div id='pendienteOculto'>";
							campoRadio('firmado','Firmado');
							campoRadio('pendiente','Pendiente de','Datos',array('Datos','Formación'),array('Datos','Formacion'));
						echo "</div>";
					  echo "<div id='vendidoOculto'>";
							campoTextoSimbolo('importe','Importe','€');
					  echo "</div>";
					  $where='';
					  campoOculto($_SESSION['codigoS'],'codigoUsuario');
					  if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
						$where='WHERE codigoUsuario="'.$_SESSION['codigoS'].'" OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador="'.$_SESSION['codigoS'].'")';
						$consulta="SELECT codigo, CONCAT(nombre, ' ', apellidos) AS texto FROM usuarios WHERE (codigo='".$_SESSION['codigoS']."' OR codigo IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador='".$_SESSION['codigoS']."'));";
						campoSelectConsulta('codigoUsuario','Usuario asignado',$consulta,$_SESSION['codigoS']);
					  }
					  ($_SESSION['tipoUsuario']!='TELECONCERTADOR') ? campoSelectConsultaAjax('codigoCliente','Cliente',"SELECT codigo, empresa AS texto FROM clientes",false,'detallesCuenta.php?codigo=') : campoSelectConsultaAjax('codigoCliente','Cliente','SELECT codigo, empresa AS texto, codigoUsuario FROM clientes HAVING 1=1 AND (codigoUsuario='.$_SESSION['codigoS'].' OR codigoUsuario IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador='.$_SESSION['codigoS'].'))',false,'detallesCuenta.php?codigo=');

            campoTexto('direccionVisita','Dirección de visita');
                    ?>


                    <div class="form-actions">
                      <button type="button" class="btn btn-primary" id="registrar"><i class="icon-ok"></i> Registrar tarea</button> 
                      <button type="button" class="btn" id="cancelar"><i class="icon-remove"></i> Cancelar</button> 
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>
      </div>
      <!--Fin Popup -->
	  
	   <!--Popup -->
      <div class="span6 hide" id="ventanaFlotanteDos">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-share-alt icon-calendar"></i><i class="icon-chevron-right"></i><i class="icon-plus-sign"></i>
              <h3>Reasignación de tareas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" method="post" action="tareas.php">
                  <fieldset>
                    
                    <?php
                      campoSelectConsulta('codigoUsuarioPartida','Usuario actual',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios ORDER BY nombre, apellidos;");
					  campoSelectConsulta('codigoUsuarioFinal','Reasignar a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios ORDER BY nombre, apellidos;");
					  campoOculto('SI','reasignacion');
                    ?>


                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Reasignar tareas</button> 
                      <button type="button" class="btn" id="cancelarDos"><i class="icon-remove"></i> Cancelar</button> 
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>
      </div>
      <!--Fin Popup -->

      </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/full-calendar/jquery-ui.custom.min.js"></script><!-- Habilita el drag y el resize -->
<script type="text/javascript" src="js/full-calendar/fullcalendar.min.js"></script>
<script src="js/excanvas.min.js" type="text/javascript"></script>
<script src="js/chart.js" type="text/javascript"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/selectAjax.js"></script>

<script type="text/javascript">

<?php
echo "var marcado='".$empresa."';";
?>

$(document).ready(function() {
  $('#ventanaFlotante').draggable();
  
  /*var tabla=$('#tablaUno').DataTable({
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave":true,
		"iDisplayLength":25,
		"oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
		  'sZeroRecords':'No se han encontrado coincidencias'
    }});*/
	
	/*var tablaDos=$('#tablaDos').DataTable({
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave":true,
		"iDisplayLength":25,
		"oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
		  'sZeroRecords':'No se han encontrado coincidencias'
    }});*/
	
	var tablaUno=$('#tablaUno').DataTable({
		"aaSorting": [],
      'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoTareas.php?action=getMembersAjx&inicio=SI',
	   "iDisplayLength":25,
	   "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		  $('td:eq(9)', nRow).addClass( "centro" );
		  $('td:eq(10)', nRow).addClass( "centro" );
		  $('td:eq(11)', nRow).addClass( "centro" );
	   },
	   "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 9, 10, 11 ] }
       ],
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });
	
	
	var tablaDos=$('#tablaDos').DataTable({
		"aaSorting": [],
      'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoTareas.php?action=getMembersAjx&inicio=NO',
	   "iDisplayLength":25,
	   "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		  $('td:eq(9)', nRow).addClass( "centro" );
		  $('td:eq(10)', nRow).addClass( "centro" );
		  $('td:eq(11)', nRow).addClass( "centro" );
	   },
	   "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 9, 10, 11 ] }
       ],
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });

	/*var tabla=$('#tablaUno').dataTable();
    tabla.fnFilter(marcado);*/
	var tablaDos=$('#tablaDos').dataTable();
    tablaDos.fnFilter(marcado);
  
  var eventosCreados=0;//Para corregir un BUG que se produce con el callback select de Fullcalendar y el resourceView: se crean en un mismo resource tantos eventos como se han creado anteriormente sin recargar la página.
  var eventoActual=1;

  var colorFondo={'normal':"#7ba9ee",'alta':"#ea807e",'baja':"#7acc76"};
  var colorBorde={'normal':"#3f85f5",'alta':"#b94a48",'baja':"#00a100"};

  var f=new Date();
  var ultimaFecha=[f.getDay()+1,f.getMonth(),f.getFullYear()];
  
  var calendario = $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultView: 'agendaDay',
    selectable:true,
    selectHelper: true,
    editable:true,
    select: function(start, end, allDay, event) {
      abreVentana();
      $('#registrar').click(function(){
          cierraVentana();

          var tarea=$('#tarea').val();
          var prioridad=$('select[name=prioridad]').val();
		  var cliente=$('select[name=codigoCliente]').val();
		  var estado='pendiente';
		  <?php if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){ ?>
			var usuario=$('select[name=codigoUsuario]').val();
		  <?php }else{ ?>
			var usuario=$('#codigoUsuario').val();  
		  <?php } ?>
		  var firmado=$('input[name=firmado]').val();
		  var pendiente=$('input[name=pendiente]').val();
		  var direccionVisita=$('#direccionVisita').val();
		  var importe=$('#importe').val();

          var fecha = $.fullCalendar.formatDate(start, 'dd/MM/yyyy');
          var horaI = $.fullCalendar.formatDate(start, 'HH:mm');
          var horaF = $.fullCalendar.formatDate(end, 'HH:mm');
          var todoDia='SI';
          if(!allDay){
            todoDia='NO';
          }
          
          eventosCreados++;
          if(eventosCreados==eventoActual){
            eventosCreados=0;
            eventoActual++;

            //Creación-renderización del evento
            var creacion=$.post("creaEvento.php", {tarea: tarea, prioridad: prioridad, fecha:fecha, horaI: horaI, horaF: horaF, todoDia: todoDia, cliente: cliente, estado: estado, usuario: usuario, firmado: firmado, pendiente: pendiente, direccionVisita: direccionVisita, importe: importe});
            creacion.done(function(datos){
              calendario.fullCalendar('renderEvent',
              {
                  id: datos,
                  title: tarea,
                  start: start,
                  end: end,
                  allDay: allDay,
                  backgroundColor: colorFondo[prioridad],
                  borderColor: colorBorde[prioridad]
                },
                true
              );
              calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
            });
            //Fin creación-renderización
          }

        });
      calendario.fullCalendar('unselect');
    },

    eventResize: function( event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {//Actualización del evento cuando se modifica tu tamaño (duración)
      var fecha = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var horaI = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var fechaF = $.fullCalendar.formatDate(event.end, 'dd/MM/yyyy');
      var horaF = $.fullCalendar.formatDate(event.end, 'HH:mm');
      var todoDia='SI';
      if(!event.allDay){
        todoDia='NO';
      }

      var creacion=$.post("actualizaEvento.php", {codigo: event._id, fecha:fecha, fechaF: fechaF, horaI: horaI, horaF: horaF, todoDia: todoDia});
      creacion.done(function(datos){
        calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
      });
    },
    eventDrop: function (event, dayDelta, minuteDelta) {//Actualización del evento cuando se mueve de hora
      var fecha = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var horaI = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var fechaF = $.fullCalendar.formatDate(event.end, 'dd/MM/yyyy');
      var horaF = $.fullCalendar.formatDate(event.end, 'HH:mm');
      var todoDia='SI';
      if(!event.allDay){
        todoDia='NO';
      }
      
      var creacion=$.post("actualizaEvento.php", {codigo: event._id, fecha:fecha, fechaF: fechaF, horaI: horaI, horaF: horaF, todoDia: todoDia});
      creacion.done(function(datos){
        calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
      });
    },

    firstDay:1,//Añadido por mi
    titleFormat: {
      month: 'MMMM yyyy',
      week: "d MMM [ yyyy]{ '&#8212;' d [ MMM] yyyy}",
      day: 'dddd, d MMM, yyyy'
    },
    columnFormat: {
      month: 'ddd',
      week: 'ddd d/M',
      day: 'dddd d/M'
    },
    axisFormat: 'H:mm',
    minTime:7,
    maxTime:22,
    firstHour:7,
    slotMinutes:15,
    timeFormat: 'H:mm { - H:mm}',//Fin añadido por mi
    events:[
      <?php
        generaEventosCalendarioTareas();
      ?>]
  });

  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('tareas.php',valoresChecks,'post');
    }

  });

  $('#cancelar').click(function(){
    $('#ventanaFlotante').addClass('hide');
	$('#registrar').unbind();
    var codigoEntrenador=$('select[name=codigoEntrenador]').val();
    $('select[name=codigoEntrenador] option[value='+codigoEntrenador+']').attr('selected',false);
  });
  
  $('#vendidoOculto').css('display','none');
	$('#prioridad').change(function(){
		if($(this).val()=='normal'){
			$('#pendienteOculto').show(300);
		}else{
			$('#pendienteOculto').hide(300);
		}
		if($(this).val()=='alta'){
			$('#vendidoOculto').show(300);
		}else{
			$('#vendidoOculto').hide(300);
		}
	});
	
  $('#cancelarDos').click(function(){
	$('#ventanaFlotanteDos').addClass('hide');
  });
  
  $('#reasignarTareas').click(function(){
	$('#ventanaFlotanteDos').removeClass('hide');
  });

});

  <?php
    $datos=generaDatosGraficoTareas();
  ?>
  var pieData = [
      {
          value: <?php echo $datos['pendientes']; ?>,
          color: "#428bca",
          label: 'Pendientes'
      },
      {
          value: <?php echo $datos['realizadas']; ?>,
          color: "#ACACAC",
          label: 'Realizadas'
      }
    ];
	
	$("#valor1").text("<?php echo $datos['pendientes']; ?>");
	$("#valor2").text("<?php echo $datos['realizadas']; ?>");

    var grafico = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);


  function abreVentana(codigoEntrenador){
    if(codigoEntrenador!=undefined){
      $('select[name=codigoEntrenador] option[value='+codigoEntrenador+']').attr('selected',true);
    }
    $('#ventanaFlotante').removeClass('hide');
  }

  function cierraVentana(){
    $('#ventanaFlotante').addClass('hide');
  }
  
  

</script><!-- /Calendar -->