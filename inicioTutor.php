<?php
   /* La sección activa y la cabecera se especifican en el archivo padre inicio.php
  $seccionActiva=0;
  include_once('cabecera.php');
  */
    
  $estadisticas=creaEstadisticasInicioTutor();

  if(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaTarea();
  }
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-home"></i>
              <h3>Inicio</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Bienvenido de nuevo <?php echo $usuario; ?>. A continuación se muestra el estado del sistema:</h6>

                   <div id="big_stats" class="cf">

                     <div class="stat"> <i class="icon-group"></i> <span class="value"><?php echo $estadisticas['alumnos']?></span> <br>Alumnos</div>
                      <!-- .stat -->
                      <div class="stat"> <i class="icon-pencil"></i> <span class="value"><?php echo $estadisticas['cursos']?></span> <br>Cursos registrados</div>
                      <!-- .stat -->

                      <div class="stat"> <i class="icon-calendar"></i> <span class="value"><?php echo $estadisticas['tareas']?></span> <br>Tareas pendientes</div>
                      <!-- .stat -->

					  
                      <div class="stat"> <i class="icon-exclamation-sign"></i> <span class="value"><?php echo $estadisticas['incidencias']?></span> <br>Incidencias</div>
                      <!-- .stat -->

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-th"></i>
              <h3>Opciones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="javascript:void;" id='guia' class="shortcut"><i class="shortcut-icon icon-book"></i><span class="shortcut-label">Guía rápida</span> </a>
                <a href="cerrarSesion.php" class="shortcut"><i class="shortcut-icon icon-off"></i><span class="shortcut-label">Cerrar sesión</span> </a>
                <br>
                <a href="creaTarea.php" class="shortcut"><i class="shortcut-icon icon-list-ol"></i><span class="shortcut-label">Añadir Tarea</span> </a>
                <a href="#" id="email" class="shortcut"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Enviar eMail</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      </div><!-- /row -->


      <?php
          if(isset($_POST['destinatarios'])){
            if($res){
              mensajeOk("Mensaje enviado correctamente."); 
            }
            else{
              mensajeError("no se ha podido enviar el mensaje. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el posible cliente. Contacte con el webmaster."); 
            }
          }
        ?>

      <?php compruebaObjetivos(); ?>

      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-th-list"></i>
          <h3>Tareas pendientes para hoy</h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable" id="target-3">
            <thead>
              <tr>
                <th> Empresa </th>
                <th> Contacto </th>
                <th> Teléfono </th>
                <th> Tarea </th>
                <th> Fecha </th>
                <th> Prioridad </th>
                <th class="centro"> </th>
                <th><input type='checkbox' id="todo"></th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeTareas(true,'FALSO','',true);
              ?>
            
            </tbody>
          </table>
        </div>
        <!-- /widget-content-->
      </div>




    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/guidely/guidely.min.js"></script>

<script type="text/javascript">

$(function () {
  
  guidely.add ({
    attachTo: '#target-1'
    , anchor: 'top-left'
    , title: 'Panel de Estadísticas'
    , text: 'En el lado izquierdo de su pantalla encontrará el Panel de Estadísticas, que le servirá para ver de forma rápida y visual el estado del sistema.'
  });
  
  guidely.add ({
    attachTo: '#target-2'
    , anchor: 'top-right'
    , title: 'Panel de Acciones'
    , text: 'En este panel se agrupan las distintas opciones disponibles en cada sección (crear un nuevo elemento, acceder a una subsección, generar un fichero, ...).<br>Para acceder a una opción solo tiene que pulsar el botón correspondiente.'
  });
  
  guidely.add ({
    attachTo: '#target-3'
    , anchor: 'middle-middle'
    , title: 'Listado de Elementos'
    , text: 'Esta tabla muestra un listado en el que podrá consultar los elementos creados en cada sección. Utilice la caja de <b>Búsqueda</b> para filtrar los registros a visualizar.'
  });
  
  guidely.add ({
    attachTo: '#target-4'
    , anchor: 'top-right'
    , title: 'Menú principal'
    , text: 'Una vez que accede al sistema, el Menú Principal está siempre disponible para permitirle navegar entre los distintos módulos de la herramienta. Solo tiene que pulsar en la sección correspondiente para acceder a ella.'
  });

    guidely.add ({
    attachTo: '#target-5'
    , anchor: 'bottom-right'
    , title: 'Menú de Usuario'
    , text: 'Por último, el Menú de Usuario le recuerda en cada momento con que cuenta está trabajando. Además, haciendo click en él se le proporciona un acceso directo para cerrar su sesión.'
  });
  
  $('#guia').click(function(){
    guidely.init ({welcome: true, startTrigger: false, welcomeTitle: 'Guía de uso básico', welcomeText: 'Bienvenido a la Guía de uso del sistema. Pulsando en el botón Iniciar, la plataforma le mostrará unas sencillas explicaciones sobre los distintos elementos que encontrará en la mayoría de las secciones.' });
  });


});

$('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('inicio.php',valoresChecks,'post');
    }
 });

$('#email').click(function(){
  var valoresChecks=recorreChecks();
  creaFormulario('enviarEmail.php?seccion=0',valoresChecks,'post');
});

</script>
