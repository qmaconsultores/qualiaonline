<?php
  $seccionActiva=7;
  include_once('cabecera.php');
  $res=false;
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
		
		<div class="span12">
		  <div class="widget widget-nopad" id="target-1">
			<div class="widget-header"> <i class="icon-tasks"></i>
			  <h3>Control consultoría</h3>
			</div>
			<!-- /widget-header -->
			
			<div class="widget-content">
			  <div class="widget big-stats-container">
				<div class="widget-content">
					<center>
						<div class='seleccionComercial'>
						<?php
						campoSelectConsultaConBlanco('comercial','Comercial',"SELECT codigo, CONCAT(nombre, ' ', apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' ORDER BY nombre, apellidos;");
						?>
							<strong>Mes: </strong>
							<?php 
								echo "<select name='mes' id='mes' class='input-large'>";
								echo "<option value='0'></option>";
								echo "<option value='01'>Enero</option>";
								echo "<option value='02'>Febrero</option>";
								echo "<option value='03'>Marzo</option>";
								echo "<option value='04'>Abril</option>";
								echo "<option value='05'>Mayo</option>";
								echo "<option value='06'>Junio</option>";
								echo "<option value='07'>Julio</option>";
								echo "<option value='08'>Agosto</option>";
								echo "<option value='09'>Septiembre</option>";
								echo "<option value='10'>Octubre</option>";
								echo "<option value='11'>Noviembre</option>";
								echo "<option value='12'>Diciembre</option>";
								echo "</select>";
							?>
						</div>
					</center>
				  <h6 class="bigstats">Trabajos realizados:</h6>


					<span id='resultadoDos'></span>

				</div> <!-- /widget-content -->
				<!-- /widget-content --> 
				
			  </div>
			</div>
		  </div>
		 
		</div>
		
		
	</div><!-- /row -->

    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/guidely/guidely.min.js"></script>
<script type="text/javascript" src="js/bootstrap-progressbar.js"></script>

<script type="text/javascript" src="js/full-calendar/fullcalendar.min.js"></script>

<script type="text/javascript">

	$('#mes').change(function(){
		var mes=$('#mes').val();
		var comercial=$('#comercial').val();
		muestraCantidades(mes,comercial);
	});	

	$('#comercial').change(function(){
		var mes=$('#mes').val();
		var comercial=$('#comercial').val();
		muestraCantidades(mes,comercial);
	});	
	  
	var mes=$('#mes').val();
	var comercial=$('#comercial').val();
	$(document).ready(muestraCantidades(mes,comercial));
	  
	function muestraCantidades(mes,comercial){
		var parametros = {
                "mes" 		: mes,
                "comercial" : comercial
        };
		$.ajax({
			 type: "POST",
			 url: "listadosajax/muestraCantidadesConsultoria.php",
			 data: parametros,
			  beforeSend: function () {
				   $("#resultadoDos").html('<center><i class="icon-refresh icon-spin"></i></center>');
			  },
			 success: function(response){
				   $("#resultadoDos").html(response);
			 }
		});
	}
</script>