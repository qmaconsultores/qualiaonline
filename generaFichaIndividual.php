<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común

	//Carga de la librería PHPWord
	require_once 'phpword/PHPWord.php';
	
	$PHPWord = new PHPWord();
	
	$codigo=$_GET['codigo'];
	
	conexionBD();
	$consulta=consultaBD("SELECT cursos.fechaInicio, cursos.fechaFin, cursos.centro, cursos.cp AS cpCentro, cursos.domicilio AS domicilioCentro, cursos.poblacion AS localidadCentro, cursos.titularidad, accionformativa.denominacion, accionformativa.modalidad, accionformativa.horas, alumnos.*,
	clientes.empresa, clientes.cif, clientes.direccion, clientes.localidad AS localidadEmpresa, clientes.cp AS cpEmpresa, clientes.provincia AS provinciaEmpresa,
	clientes.ccc, clientes.telefono AS telefonoEmpresa, clientes.fax, clientes.mail AS mailEmpresa, clientes.contacto, clientes.dniRepresentante, clientes.sector 
	FROM cursos INNER JOIN accionformativa ON cursos.codigoAccionFormativa=accionformativa.codigo
	INNER JOIN alumnos_registrados_cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo
	INNER JOIN alumnos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno
	INNER JOIN ventas ON alumnos.codigoVenta=ventas.codigo
	INNER JOIN clientes ON ventas.codigoCliente=clientes.codigo
	WHERE cursos.codigo='$codigo';");
	echo mysql_error();
	
	$datos=mysql_fetch_assoc($consulta);
	
	$i=0;
	
	while($datos!=0){
	
		/*----------------------------------------------------------------------------PRIMER DOCUMENTO------------------------------------------------------*/
		$document = $PHPWord->loadTemplate('documentos/plantilla.docx');
		$document->setValue("curso",utf8_decode($datos['denominacion']));
		$document->setValue("fechaInicio",utf8_decode(formateaFechaWeb($datos['fechaInicio'])));
		$document->setValue("fechaFin",utf8_decode(formateaFechaWeb($datos['fechaFin'])));
		$document->setValue("horas",utf8_decode($datos['horas']));
		$document->setValue("nomApell",utf8_decode($datos['nombre']." ".$datos['apellidos']));
		$document->setValue("dni",utf8_decode($datos['dni']));
		$document->setValue("numSS",utf8_decode($datos['numSS1'].$datos['numSS2']));
		$document->setValue("fechaNac",utf8_decode(formateaFechaWeb($datos['fechaNac'])));
		$document->setValue("domicilio",utf8_decode($datos['domicilio']));
		$document->setValue("localidad",utf8_decode($datos['localidad']));
		$document->setValue("cp",utf8_decode($datos['cp']));
		$document->setValue("provincia",utf8_decode($datos['provincia']));
		$document->setValue("telefono",utf8_decode($datos['telefono']));
		$document->setValue("mail",utf8_decode($datos['mail']));
		$document->setValue("nombreEmpresa",utf8_decode($datos['empresa']));
		$document->setValue("cif",utf8_decode($datos['cif']));
		$document->setValue("domicilioEmpresa",utf8_decode($datos['direccion']));
		$document->setValue("localidadEmpresa",utf8_decode($datos['localidadEmpresa']));
		$document->setValue("cpEmpresa",utf8_decode($datos['cpEmpresa']));
		$document->setValue("provinciaEmpresa",utf8_decode($datos['provinciaEmpresa']));
		$document->setValue("ccc",utf8_decode($datos['ccc']));
		$document->setValue("telefonoEmpresa",utf8_decode($datos['telefonoEmpresa']));
		$document->setValue("fax",utf8_decode($datos['fax']));
		$document->setValue("mailEmpresa",utf8_decode($datos['mailEmpresa']));
		$document->setValue("representante",utf8_decode($datos['contacto']));
		$document->setValue("dniRepresentante",utf8_decode($datos['dniRepresentante']));
		$document->setValue("actividadEmpresa",utf8_decode($datos['sector']));
		switch($datos['modalidad']){
			case "PRESENCIAL":
				$document->setValue("pre",utf8_decode("X"));
				$document->setValue("tel",utf8_decode(""));
				$document->setValue("dis",utf8_decode(""));
			break;
			case "DISTANCIA":
				$document->setValue("pre",utf8_decode(""));
				$document->setValue("tel",utf8_decode(""));
				$document->setValue("dis",utf8_decode("X"));
			break;
			case "MIXTA":
				$document->setValue("pre",utf8_decode("X"));
				$document->setValue("tel",utf8_decode(""));
				$document->setValue("dis",utf8_decode("X"));
			break;
			case "TELE":
				$document->setValue("pre",utf8_decode(""));
				$document->setValue("tel",utf8_decode("X"));
				$document->setValue("dis",utf8_decode(""));
			break;
		}
		
		switch($datos['sexo']){
			case "M":
				$document->setValue("var",utf8_decode("X"));
				$document->setValue("muj",utf8_decode(""));
			break;
			case "F":
				$document->setValue("var",utf8_decode(""));
				$document->setValue("muj",utf8_decode("X"));
			break;
		}
		
		switch($datos['estudios']){
			case "sin":
				$document->setValue("e1",utf8_decode("X"));
				$document->setValue("e2",utf8_decode(""));
				$document->setValue("e3",utf8_decode(""));
				$document->setValue("e4",utf8_decode(""));
				$document->setValue("e5",utf8_decode(""));
				$document->setValue("e6",utf8_decode(""));
				$document->setValue("e7",utf8_decode(""));
			break;
			case "primarios":
				$document->setValue("e1",utf8_decode(""));
				$document->setValue("e2",utf8_decode("X"));
				$document->setValue("e3",utf8_decode(""));
				$document->setValue("e4",utf8_decode(""));
				$document->setValue("e5",utf8_decode(""));
				$document->setValue("e6",utf8_decode(""));
				$document->setValue("e7",utf8_decode(""));
			break;
			case "bachiller":
				$document->setValue("e1",utf8_decode(""));
				$document->setValue("e2",utf8_decode(""));
				$document->setValue("e3",utf8_decode("X"));
				$document->setValue("e4",utf8_decode(""));
				$document->setValue("e5",utf8_decode(""));
				$document->setValue("e6",utf8_decode(""));
				$document->setValue("e7",utf8_decode(""));
			break;
			case "diplomatura":
				$document->setValue("e1",utf8_decode(""));
				$document->setValue("e2",utf8_decode(""));
				$document->setValue("e3",utf8_decode(""));
				$document->setValue("e4",utf8_decode("X"));
				$document->setValue("e5",utf8_decode(""));
				$document->setValue("e6",utf8_decode(""));
				$document->setValue("e7",utf8_decode(""));
			break;
			case "licenciatura":
				$document->setValue("e1",utf8_decode(""));
				$document->setValue("e2",utf8_decode(""));
				$document->setValue("e3",utf8_decode(""));
				$document->setValue("e4",utf8_decode(""));
				$document->setValue("e5",utf8_decode("X"));
				$document->setValue("e6",utf8_decode(""));
				$document->setValue("e7",utf8_decode(""));
			break;
		}
		
		switch($datos['puesto']){
			case "direccion":
				$document->setValue("p1",utf8_decode("X"));
				$document->setValue("p2",utf8_decode(""));
				$document->setValue("p3",utf8_decode(""));
				$document->setValue("p4",utf8_decode(""));
				$document->setValue("p5",utf8_decode(""));
			break;
			case "administracion":				
				$document->setValue("p1",utf8_decode(""));
				$document->setValue("p2",utf8_decode("X"));
				$document->setValue("p3",utf8_decode(""));
				$document->setValue("p4",utf8_decode(""));
				$document->setValue("p5",utf8_decode(""));
			break;
			case "comercial":				
				$document->setValue("p1",utf8_decode(""));
				$document->setValue("p2",utf8_decode(""));
				$document->setValue("p3",utf8_decode("X"));
				$document->setValue("p4",utf8_decode(""));
				$document->setValue("p5",utf8_decode(""));
			break;
			case "mantenimiento":				
				$document->setValue("p1",utf8_decode(""));
				$document->setValue("p2",utf8_decode(""));
				$document->setValue("p3",utf8_decode(""));
				$document->setValue("p4",utf8_decode("X"));
				$document->setValue("p5",utf8_decode(""));
			break;
			case "produccion":				
				$document->setValue("p1",utf8_decode(""));
				$document->setValue("p2",utf8_decode(""));
				$document->setValue("p3",utf8_decode(""));
				$document->setValue("p4",utf8_decode(""));
				$document->setValue("p5",utf8_decode("X"));
			break;
		}
		
		switch($datos['categoria']){
			case "directivo":
				$document->setValue("c1",utf8_decode("X"));
				$document->setValue("c2",utf8_decode(""));
				$document->setValue("c3",utf8_decode(""));
				$document->setValue("c4",utf8_decode(""));
				$document->setValue("c5",utf8_decode(""));
			break;
			case "intermedio":
				$document->setValue("c1",utf8_decode(""));
				$document->setValue("c2",utf8_decode("X"));
				$document->setValue("c3",utf8_decode(""));
				$document->setValue("c4",utf8_decode(""));
				$document->setValue("c5",utf8_decode(""));
			break;
			case "tecnico":
				$document->setValue("c1",utf8_decode(""));
				$document->setValue("c2",utf8_decode(""));
				$document->setValue("c3",utf8_decode("X"));
				$document->setValue("c4",utf8_decode(""));
				$document->setValue("c5",utf8_decode(""));
			break;
			case "cualificado":
				$document->setValue("c1",utf8_decode(""));
				$document->setValue("c2",utf8_decode(""));
				$document->setValue("c3",utf8_decode(""));
				$document->setValue("c4",utf8_decode("X"));
				$document->setValue("c5",utf8_decode(""));
			break;
			case "bajaCualificacion":
				$document->setValue("c1",utf8_decode(""));
				$document->setValue("c2",utf8_decode(""));
				$document->setValue("c3",utf8_decode(""));
				$document->setValue("c4",utf8_decode(""));
				$document->setValue("c5",utf8_decode("X"));
			break;
		}
		
		switch($datos['cotizacion']){
			case "1":
				$document->setValue("g1",utf8_decode("X"));
				$document->setValue("g2",utf8_decode(""));
				$document->setValue("g3",utf8_decode(""));
				$document->setValue("g4",utf8_decode(""));
				$document->setValue("g5",utf8_decode(""));
				$document->setValue("g6",utf8_decode(""));
				$document->setValue("g7",utf8_decode(""));
				$document->setValue("g8",utf8_decode(""));
				$document->setValue("g9",utf8_decode(""));
				$document->setValue("g10",utf8_decode(""));
				$document->setValue("g11",utf8_decode(""));
			break;
			case "2":
				$document->setValue("g1",utf8_decode(""));
				$document->setValue("g2",utf8_decode("X"));
				$document->setValue("g3",utf8_decode(""));
				$document->setValue("g4",utf8_decode(""));
				$document->setValue("g5",utf8_decode(""));
				$document->setValue("g6",utf8_decode(""));
				$document->setValue("g7",utf8_decode(""));
				$document->setValue("g8",utf8_decode(""));
				$document->setValue("g9",utf8_decode(""));
				$document->setValue("g10",utf8_decode(""));
				$document->setValue("g11",utf8_decode(""));
			break;
			case "3":
				$document->setValue("g1",utf8_decode(""));
				$document->setValue("g2",utf8_decode(""));
				$document->setValue("g3",utf8_decode("X"));
				$document->setValue("g4",utf8_decode(""));
				$document->setValue("g5",utf8_decode(""));
				$document->setValue("g6",utf8_decode(""));
				$document->setValue("g7",utf8_decode(""));
				$document->setValue("g8",utf8_decode(""));
				$document->setValue("g9",utf8_decode(""));
				$document->setValue("g10",utf8_decode(""));
				$document->setValue("g11",utf8_decode(""));
			break;
			case "4":
				$document->setValue("g1",utf8_decode(""));
				$document->setValue("g2",utf8_decode(""));
				$document->setValue("g3",utf8_decode(""));
				$document->setValue("g4",utf8_decode("X"));
				$document->setValue("g5",utf8_decode(""));
				$document->setValue("g6",utf8_decode(""));
				$document->setValue("g7",utf8_decode(""));
				$document->setValue("g8",utf8_decode(""));
				$document->setValue("g9",utf8_decode(""));
				$document->setValue("g10",utf8_decode(""));
				$document->setValue("g11",utf8_decode(""));
			break;
			case "5":
				$document->setValue("g1",utf8_decode(""));
				$document->setValue("g2",utf8_decode(""));
				$document->setValue("g3",utf8_decode(""));
				$document->setValue("g4",utf8_decode(""));
				$document->setValue("g5",utf8_decode("X"));
				$document->setValue("g6",utf8_decode(""));
				$document->setValue("g7",utf8_decode(""));
				$document->setValue("g8",utf8_decode(""));
				$document->setValue("g9",utf8_decode(""));
				$document->setValue("g10",utf8_decode(""));
				$document->setValue("g11",utf8_decode(""));
			break;
			case "6":
				$document->setValue("g1",utf8_decode(""));
				$document->setValue("g2",utf8_decode(""));
				$document->setValue("g3",utf8_decode(""));
				$document->setValue("g4",utf8_decode(""));
				$document->setValue("g5",utf8_decode(""));
				$document->setValue("g6",utf8_decode("X"));
				$document->setValue("g7",utf8_decode(""));
				$document->setValue("g8",utf8_decode(""));
				$document->setValue("g9",utf8_decode(""));
				$document->setValue("g10",utf8_decode(""));
				$document->setValue("g11",utf8_decode(""));
			break;
			case "7":
				$document->setValue("g1",utf8_decode(""));
				$document->setValue("g2",utf8_decode(""));
				$document->setValue("g3",utf8_decode(""));
				$document->setValue("g4",utf8_decode(""));
				$document->setValue("g5",utf8_decode(""));
				$document->setValue("g6",utf8_decode(""));
				$document->setValue("g7",utf8_decode("X"));
				$document->setValue("g8",utf8_decode(""));
				$document->setValue("g9",utf8_decode(""));
				$document->setValue("g10",utf8_decode(""));
				$document->setValue("g11",utf8_decode(""));
			break;
			case "8":
				$document->setValue("g1",utf8_decode(""));
				$document->setValue("g2",utf8_decode(""));
				$document->setValue("g3",utf8_decode(""));
				$document->setValue("g4",utf8_decode(""));
				$document->setValue("g5",utf8_decode(""));
				$document->setValue("g6",utf8_decode(""));
				$document->setValue("g7",utf8_decode(""));
				$document->setValue("g8",utf8_decode("X"));
				$document->setValue("g9",utf8_decode(""));
				$document->setValue("g10",utf8_decode(""));
				$document->setValue("g11",utf8_decode(""));
			break;
			case "9":
				$document->setValue("g1",utf8_decode(""));
				$document->setValue("g2",utf8_decode(""));
				$document->setValue("g3",utf8_decode(""));
				$document->setValue("g4",utf8_decode(""));
				$document->setValue("g5",utf8_decode(""));
				$document->setValue("g6",utf8_decode(""));
				$document->setValue("g7",utf8_decode(""));
				$document->setValue("g8",utf8_decode(""));
				$document->setValue("g9",utf8_decode("X"));
				$document->setValue("g10",utf8_decode(""));
				$document->setValue("g11",utf8_decode(""));
			break;
			case "10":
				$document->setValue("g1",utf8_decode(""));
				$document->setValue("g2",utf8_decode(""));
				$document->setValue("g3",utf8_decode(""));
				$document->setValue("g4",utf8_decode(""));
				$document->setValue("g5",utf8_decode(""));
				$document->setValue("g6",utf8_decode(""));
				$document->setValue("g7",utf8_decode(""));
				$document->setValue("g8",utf8_decode(""));
				$document->setValue("g9",utf8_decode(""));
				$document->setValue("g10",utf8_decode("X"));
				$document->setValue("g11",utf8_decode(""));
			break;
			case "11":
				$document->setValue("g1",utf8_decode(""));
				$document->setValue("g2",utf8_decode(""));
				$document->setValue("g3",utf8_decode(""));
				$document->setValue("g4",utf8_decode(""));
				$document->setValue("g5",utf8_decode(""));
				$document->setValue("g6",utf8_decode(""));
				$document->setValue("g7",utf8_decode(""));
				$document->setValue("g8",utf8_decode(""));
				$document->setValue("g9",utf8_decode(""));
				$document->setValue("g10",utf8_decode(""));
				$document->setValue("g11",utf8_decode("X"));
			break;
		}
		
		$document->setValue("dia",utf8_decode(devuelveDia()));
		$document->setValue("mes",utf8_decode(devuelveMes()));
		$document->setValue("anio",utf8_decode(devuelveAnio()));
		
		
		$document->save('documentos/ficha-'.utf8_decode($datos['nombre']).','.utf8_decode($datos['apellidos']).'.docx');
		
		/*------------------------------------------------------------------------FIN PRIMER DOCUMENTO------------------------------------------------------*/
		
		/*---------------------------------------------------------------------------SEGUNDO DOCUMENTO------------------------------------------------------*/
		$document = $PHPWord->loadTemplate('documentos/plantillatres.docx');
		$document->setValue("empresa",utf8_decode($datos['empresa']));
		$document->setValue("cif",utf8_decode($datos['cif']));
		$document->setValue("fechaNac",utf8_decode(formateaFechaWeb($datos['fechaNac'])));
		$document->setValue("dniT",utf8_decode($datos['dni']));
		$document->setValue("nss",utf8_decode($datos['numSS1'].$datos['numSS2']));
		$document->setValue("nombre",utf8_decode($datos['nombre']));
		$apellidos=explode(" ",$datos['apellidos']);
		$document->setValue("apell1",utf8_decode($apellidos[0]));
		$document->setValue("apell2",utf8_decode($apellidos[1]));
		switch($datos['sexo']){
			case "M":
				$document->setValue("sexo",utf8_decode("Masculino"));
			break;
			case "F":
				$document->setValue("sexo",utf8_decode("Femenino"));
			break;
		}	
		
		$document->setValue("direccion",utf8_decode($datos['domicilio']));		
		$document->setValue("localidad",utf8_decode($datos['localidad']));
		$document->setValue("cp",utf8_decode($datos['cp']));
		$document->setValue("provincia",utf8_decode($datos['provincia']));
		$document->setValue("estudios",utf8_decode($datos['estudios']));
		$document->setValue("discapacidad",utf8_decode($datos['discapacidad']));
		$document->setValue("tipo",utf8_decode($datos['categoria']));
		$document->setValue("cotizacion",utf8_decode($datos['cotizacion']));
		$document->setValue("puesto",utf8_decode($datos['puesto']));
		$document->setValue("denominacion",utf8_decode($datos['denominacion']));
		$document->setValue("duracion",utf8_decode($datos['horas']));
		$document->setValue("centroFormacion",utf8_decode($datos['centro']));
		$document->setValue("direccionCentro",utf8_decode($datos['domicilioCentro']));
		$document->setValue("cpCen",utf8_decode($datos['cpCentro']));
		$document->setValue("localidadCentro",utf8_decode($datos['localidadCentro']));
		$document->setValue("representante",utf8_decode($datos['contacto']));
		$document->setValue("dniRepresentante",utf8_decode($datos['dniRepresentante']));
		$document->setValue("actividadEmpresa",utf8_decode($datos['sector']));
		$document->setValue("salario",utf8_decode($datos['salarioAnual']));
		$document->setValue("horas",utf8_decode($datos['horasAnuales']));
		
		switch($datos['jornada']){
			case "completa":
				$document->setValue("j1","X");
				$document->setValue("j2","");
				$document->setValue("j3","");
				$document->setValue("j4","");
			break;
			case "media":
				$document->setValue("j1","");
				$document->setValue("j2","X");
				$document->setValue("j3","");
				$document->setValue("j4","");
			break;
			case "reducida":
				$document->setValue("j1","");
				$document->setValue("j2","");
				$document->setValue("j3","X");
				$document->setValue("j4","");
			break;
			case "otros":
				$document->setValue("j1","");
				$document->setValue("j2","");
				$document->setValue("j3","");
				$document->setValue("j4","X");
			break;
		}
		
		switch($datos['horario']){
			case "mañana":
				$document->setValue("h1","X");
				$document->setValue("h2","");
				$document->setValue("h3","");
				$document->setValue("h4","");
				$document->setValue("h5","");
			break;
			case "partida":
				$document->setValue("h1","");
				$document->setValue("h2","X");
				$document->setValue("h3","");
				$document->setValue("h4","");
				$document->setValue("h5","");
			break;
			case "tardes":
				$document->setValue("h1","");
				$document->setValue("h2","");
				$document->setValue("h3","X");
				$document->setValue("h4","");
				$document->setValue("h5","");
			break;
			case "noches":
				$document->setValue("h1","");
				$document->setValue("h2","");
				$document->setValue("h3","");
				$document->setValue("h4","X");
				$document->setValue("h5","");
			break;
			case "turnos":
				$document->setValue("h1","");
				$document->setValue("h2","");
				$document->setValue("h3","");
				$document->setValue("h4","");
				$document->setValue("h5","X");
			break;
		}
		
		switch($datos['modalidad']){
			case "PRESENCIAL":
				$document->setValue("pre",utf8_decode("X"));
				$document->setValue("el",utf8_decode(""));
				$document->setValue("dis",utf8_decode(""));
				$document->setValue("mix",utf8_decode(""));
			break;
			case "DISTANCIA":
				$document->setValue("pre",utf8_decode(""));
				$document->setValue("el",utf8_decode(""));
				$document->setValue("dis",utf8_decode("X"));
				$document->setValue("mix",utf8_decode(""));
			break;
			case "MIXTA":
				$document->setValue("pre",utf8_decode(""));
				$document->setValue("el",utf8_decode(""));
				$document->setValue("dis",utf8_decode(""));
				$document->setValue("mix",utf8_decode("X"));
			break;
			case "TELE":
				$document->setValue("pre",utf8_decode(""));
				$document->setValue("el",utf8_decode("X"));
				$document->setValue("dis",utf8_decode(""));
				$document->setValue("mix",utf8_decode(""));
			break;
		}
		switch($datos['titularidad']){
			case "publico":
				$document->setValue("pub",utf8_decode("X"));
				$document->setValue("priv",utf8_decode(""));
			break;
			case "privado":
				$document->setValue("pub",utf8_decode(""));
				$document->setValue("priv",utf8_decode("X"));
			break;
		}
		$document->setValue("dia",utf8_decode(devuelveDia()));
		$document->setValue("mes",utf8_decode(devuelveMes()));
		$document->setValue("anio",utf8_decode(devuelveAnio()));
		
		$document->save('documentos/permiso-'.utf8_decode($datos['nombre']).','.utf8_decode($datos['apellidos']).'.docx');
		
		$datos=mysql_fetch_assoc($consulta);
	}
	
	/*
		Cuando ya has sustituido todas las etiquetas por su valores, el método save
		guarda el fichero resultane en la ruta que le indiques con el nombre que le
		indiques.
		*/
		/*$document->save('documentos/listadoAlumnosCurso.docx');*/


		/*
		Definir headers.
		Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
		que lo que va a recibir es un fichero que debe descargar.
		
		header("Content-Type: application/vnd.ms-docx");
		header("Content-Disposition: attachment; filename=listadoAlumnosCurso.docx");
		header("Content-Transfer-Encoding: binary");
		
		Descargar archivo.
		Por último, le decirmos a PHP que lea y transfiera el documento que
		antes con save.
		
		readfile('documentos/listadoAlumnosCurso.docx');
		*/
	
	cierraBD();
	
	header("Location: generaHojaAdhesion.php?codigo=$codigo"); 
	exit;

?>