<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos/plantillaFacturaFirma.xlsx");
	
	conexionBD();
	$tiempo=time();
	$i=1;
	$j=4;
	$costeTotal=0;
	
	//COMIENZO DE TODOS LAS FACTURAS COMPRENDIDAS
	conexionBD();
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.referencia, concepto, coste, empresa, fechaEmision, fechaVencimiento, numCuenta, clientes.referencia AS refCliente, clientes.cif AS cifCliente, clientes.empresa, clientes.direccion, clientes.cp, clientes.localidad, clientes.empresa, clientes.provincia, clientes.bic, facturacion.firma, usuarios.nombre, usuarios.apellidos, facturacion.codigoCliente, facturacion.cobrada 
		FROM facturacion LEFT JOIN clientes ON facturacion.codigoCliente=clientes.codigo
		LEFT JOIN usuarios ON usuarios.codigo=facturacion.codigoUsuario WHERE firma='3' AND fechaEmision>='2015-01-01' AND fechaEmision<='2015-03-31' ORDER BY fechaEmision;");
		$datos=mysql_fetch_assoc($consulta);
	
	$concepto=array('14'=>'Formación', '22'=>'Protección de datos', 'blanqueo'=>'Blanqueo de capitales');
	while(isset($datos['codigo'])){		
		
		$consultaAux=consultaBD("SELECT COUNT(ventas.codigo) AS total, usuarios.nombre, usuarios.apellidos FROM ventas LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo WHERE ventas.codigoCliente='".$datos['codigoCliente']."' AND (ventas.precio='".$datos['coste']."' OR ventas.precio='".$datos['coste']."'*(-1) OR ventas.precio='".$datos['coste']."'*2);");
		$datosComercial=mysql_fetch_assoc($consultaAux);
		
		$datos['coste']=number_format((float)$datos['coste'], 2, ',', '');
		
		$objPHPExcel->getActiveSheet()->getCell('B'.$j)->setValue($datos['refCliente']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$j)->setValue($datos['referencia']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$j)->setValue($datos['empresa']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$j)->setValue($concepto[$datos['concepto']]);
		$objPHPExcel->getActiveSheet()->getCell('F'.$j)->setValue(formateaFechaWeb($datos['fechaEmision']));
		$objPHPExcel->getActiveSheet()->getCell('G'.$j)->setValue(formateaFechaWeb($datos['fechaVencimiento']));
		$objPHPExcel->getActiveSheet()->getCell('H'.$j)->setValue($datos['coste'].' €');
		$objPHPExcel->getActiveSheet()->getCell('I'.$j)->setValue($datosComercial['nombre'].' '.$datosComercial['apellidos']);
		$objPHPExcel->getActiveSheet()->getCell('J'.$j)->setValue($datos['cobrada']);
		$objPHPExcel->getActiveSheet()->getCell('K'.$j)->setValue($datos['cifCliente']);
		$objPHPExcel->getActiveSheet()->getCell('L'.$j)->setValue($datos['provincia']);
		$objPHPExcel->getActiveSheet()->getCell('M'.$j)->setValue($datos['localidad']);
		$objPHPExcel->getActiveSheet()->getCell('N'.$j)->setValue($datos['direccion']);
		
		
		$datos=mysql_fetch_assoc($consulta);
		$i++;
		$j++;
	}
	cierraBD();

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	
	$objWriter->save('documentos/facturas.xlsx');

	/*
	// Definir headers
	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=Facturacion-".$tiempo.".zip");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/Facturacion-'.$tiempo.'.zip');*/

	
	
	// Definir headers
	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=facturas.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/facturas.xlsx');
?>