<?php
  $seccionActiva=7;
  include_once("cabecera.php");
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Informe de comisiones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              Seleccione el colaborador y el trimestre para ver sus comisiones:<br><br>
              <?php
              $anios=array();
              for($i=2015;$i<=date('Y');$i++){
                array_push($anios, $i);
              }
				echo'<form action="informesComisionesColaboradores.php" method="post">';
					selectColaboradores();
					echo "&nbsp;";
					campoSelect('periodo','Periodo',array('Enero - Marzo','Abril - Junio','Julio - Septiembre','Octubre - Diciembre','Año completo'),array('01','02','03','04','05'),false,'selectpicker show-tick',"data-live-search='true'",1);
          echo "&nbsp;";
          campoSelect('anio','Año',$anios,$anios,date('Y'),'selectpicker show-tick',"data-live-search='true'",1);
					echo '<br>
					<button type="submit" class="btn btn-primary">Seleccionar <i class="icon-circle-arrow-right"></i></button>
				</form>';
              ?>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#fechaEmision').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('#fechaVencimiento').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();
    

  });
</script>