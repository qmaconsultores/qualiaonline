<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	
  	$datosEmpresa=datosCliente($_GET['codigo']);

  	require_once 'phpword/PHPWord.php';

	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('documentos/plantillacuatro.docx');
			
			$document->setValue("representante",utf8_decode($datosEmpresa['contacto']));
			$document->setValue("dniRepresentante",utf8_decode($datosEmpresa['dniRepresentante']));
			$document->setValue("empresa",utf8_decode($datosEmpresa['empresa']));
			$document->setValue("cif",utf8_decode($datosEmpresa['cif']));
			$document->setValue("actividad",utf8_decode($datosEmpresa['sector']));
			$document->setValue("tlf",utf8_decode($datosEmpresa['telefono']));
			$document->setValue("sede",utf8_decode($datosEmpresa['direccion']));
			$document->setValue("poblacion",utf8_decode($datosEmpresa['localidad']));
			$document->setValue("cp",utf8_decode($datosEmpresa['cp']));
			$document->setValue("provincia",utf8_decode($datosEmpresa['provincia']));
			$document->setValue("ccc",utf8_decode($datosEmpresa['ccc']));
			$document->setValue("mail",utf8_decode($datosEmpresa['mail']));
			
			switch($datosEmpresa['representacion']){
				case "SI":
					$document->setValue("rl",utf8_decode("X"));
					$document->setValue("norl",utf8_decode(""));
				break;
				case "NO":
					$document->setValue("rl",utf8_decode(""));
					$document->setValue("norl",utf8_decode("X"));
				break;
			}
			
			switch($datosEmpresa['nuevacreacion']){
				case "SI":
					$document->setValue("nu",utf8_decode("X"));
					$document->setValue("nonu",utf8_decode(""));
				break;
				case "NO":
					$document->setValue("nu",utf8_decode(""));
					$document->setValue("nonu",utf8_decode("X"));
				break;
			}
			
			$document->setValue("fecha",devuelveFecha());
			
			$document->save('documentos/Adhesion-'.$datosEmpresa['empresa'].'.docx');

	/*
	Definir headers.
	Las 3 siguientes líneas definen las cabeceras HTTP de modo que el navegador entienda
	que lo que va a recibir es un fichero que debe descargar.
	*/
	header("Content-Type: application/vnd.ms-docx");
	header('Content-Disposition: attachment; filename=Adhesion-'.$datosEmpresa['empresa'].'.docx');
	header("Content-Transfer-Encoding: binary");

	/* 
	Descargar archivo.
	Por último, le decirmos a PHP que lea y transfiera el documento que
	antes con save.
	*/
	readfile('documentos/Adhesion-'.$datosEmpresa['empresa'].'.docx');


?>