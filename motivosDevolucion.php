<?php
  $seccionActiva=14;
  include_once('cabecera.php');

  $consultaF=datosRegistroMultiple('motivos');
  $i=1;
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Motivos de devolución registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="facturacion.php" method="post">

                  <fieldset class="span11">
                    <div class="control-group">                     
                      <label class="control-label" for="formasPago"></label>
                      <div class="controls sinMargenIz">
						<center>
                        <table class="table table-striped table-bordered mitadAncho" id="formasPago">
                          <thead>
                            <tr>
                              <th class="centro encabezadoTablaContabilidad">Motivos</th>
                            </tr>
                            <tr>
                              <th>Motivo</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $datos=mysql_fetch_assoc($consultaF);
							if(isset($datos['codigo'])){
								while($datos!=false){
								  echo "
								  <tr>
									<td>
										<input type='text' name='motivo$i' id='motivo$i' class='span4' value='".$datos['motivo']."' />
									</td>
								  </tr>";
								  $datos=mysql_fetch_assoc($consultaF);
								  $i++;
								}
							}else{
								echo "
								  <tr>
									<td>
										<input type='text' name='motivo$i' id='motivo$i' class='span5' />
									</td>
								  </tr>";
							}
                            ?>
                          </tbody>
                        </table>
						</center>

                        <div class="centro">
                          <button type="button" class="btn btn-success" onclick="insertaFila('formasPago');" ><i class="icon-plus"></i> Insertar</button>
                        </div>

                      </div>
                    </div>

                  </fieldset>
                  <fieldset class="span3">


                    
                    <br>

                  </fieldset>

                  <fieldset class="sinFlotar">
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Guardar Motivos</button> 
                      <a href="facturacion.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/filasTabla.js"></script>
