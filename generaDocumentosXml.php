<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común

	//Carga de la librería PHPWord
	require_once 'phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$PHPWord = new PHPWord();
	
	
	conexionBD();
	
	$consulta=consultaBD("SELECT * FROM cursos;");
	
	$zip = new ZipArchive();
	
	$fichero = 'documentos/DocumentacionCursos.zip';
	
	if($zip->open($fichero,ZIPARCHIVE::CREATE)===true) {
							
		$datosEmpresa=mysql_fetch_assoc($consulta);
	
		while($datosEmpresa!=0){
			$codigo=$datosEmpresa['codigo'];
		
			$ficheroXML=generaXMLParticipantesTodos();
			$zip->addFile("documentos/$ficheroXML",$ficheroXML);
			
			
			$ficheroXMLGrupo=generaXMLGrupos();
			$zip->addFile("documentos/$ficheroXMLGrupo",$ficheroXMLGrupo);
			
			$ficheroXMLAccion=generaXMLAccionTodas();
			$zip->addFile("documentos/$ficheroXMLAccion",$ficheroXMLAccion);
			
			
			$ficheroXMLFin=generaXMLFinTodos();
			$i=0;
			while(isset($ficheroXMLFin[$i])){
				$zip->addFile("documentos/".$ficheroXMLFin[$i],$ficheroXMLFin[$i]);
				$i++;
			}
			
			$datosEmpresa=mysql_fetch_assoc($consulta);
			
		}

			if(!$zip->close()){
				echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
				echo $zip->getStatusString();
			}
		
	}

	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=DocumentacionCursos.zip");
	header("Content-Transfer-Encoding: binary");

		// Descargar archivo
	readfile('documentos/DocumentacionCursos.zip');

?>