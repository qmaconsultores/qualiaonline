<?php
	ini_set('memory_limit', '32M');
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
	
	if(isset($_GET['codigoCliente'])){
		exportarCSVVentas($_GET['codigoCliente']);
	}else{
		exportarCSVVentas(false);
	}

	// Definir headers
	header("Content-Type: application/csv");
	header("Content-Disposition: attachment; filename=Listado_ventas.csv");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/Listado_ventas.csv');
?>