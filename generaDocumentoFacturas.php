<?php
	session_start();
	include_once("funciones.php");
	require_once("../api/sepassd/SEPASDD.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');
	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos/plantillaFacturas.xlsx");
	
	$tipoSepa=$_POST['tipoSepa'];//Las remesas SEPA pueden ser CORE o B2B, que llevan los códigos asociados 19143 y 19445 respectivamente.
	$banco=$_POST['banco'];
	if($banco=='SABADELL'){
		$bancoRemesa='0081';
	} elseif($banco=='CAIXA'){
		$bancoRemesa='2100';//Dígitos 5,6,7 y 8
	} else{
		$bancoRemesa='0182';
	}
	$facturas=explode(',',$_POST['facturas']);
	conexionBD();
	$tiempo=time();
	$file=fopen("documentos/remesa".$tiempo.".dat","a") or die("Problemas");
	$i=1;
	$j=5;
	$costeTotal=0;
	$controlador=0;
	
	//COMIENZO DE TODOS LAS FACTURAS COMPRENDIDAS
	while(isset($facturas[$i])){		
		$consulta=consultaBD("SELECT facturas_vto.codigo, facturacion.referencia, productos.nombreProducto AS concepto, facturas_vto.importe AS coste, empresa, fechaEmision, facturas_vto.fecha AS fechaVencimiento, numCuenta, clientes.referencia AS refCliente, clientes.cif AS cifCliente, clientes.empresa, clientes.direccion, clientes.cp, clientes.localidad, clientes.empresa, clientes.provincia, clientes.bic, facturacion.firma, facturas_vto.fecha 
		FROM facturacion LEFT JOIN clientes ON facturacion.codigoCliente=clientes.codigo
		LEFT JOIN productos ON facturacion.concepto=productos.codigo
		INNER JOIN facturas_vto ON facturas_vto.codigoFactura=facturacion.codigo WHERE facturas_vto.codigo='".$facturas[$i]."';");


		$datos=mysql_fetch_assoc($consulta);
		
		if($datos['firma']!='3'){
			$datos['coste']=$datos['coste']*0.21+$datos['coste'];
		}
		
		$datos['coste']=number_format((float)$datos['coste'], 2, '.', '');
		
		$objPHPExcel->getActiveSheet()->getCell('B'.$j)->setValue($datos['referencia']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$j)->setValue($datos['empresa']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$j)->setValue($datos['concepto']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$j)->setValue($datos['coste']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$j)->setValue($datos['numCuenta']);
		
		$costeTotal+=$datos['coste'];
		
		$datos['empresa']=sanear_string($datos['empresa']);
		$datos['direccion']=substr(sanear_string($datos['direccion']), 0, 50);
		$datos['cp']=sanear_string($datos['cp']);
		$datos['localidad']=sanear_string($datos['localidad']);
		$datos['provincia']=sanear_string($datos['provincia']);
		
		$datos['coste']=str_replace('.','',$datos['coste']);
		$fechaEmision=str_replace('-','',$datos['fechaEmision']);
		$fechaVencimiento=str_replace('-','',$datos['fechaVencimiento']);
		
		if($i==1){
			switch($datos['firma']){
				case '1':
					$nombre='GRUP QUALIA ASSOCIATS SL                                              ';
					$cif='B65775850';
					if($banco=='SABADELL'){
						$numCuenta='ES6400810454460001193421';
						$oficina='4199';
						$identificador='ES08001';
					} else if($banco=='CAIXA'){
						$numCuenta='ES9621000815550200939598';
						$oficina='0815';
						$identificador='ES08001B65775850';
					} else{
						$numCuenta='ES5720136153670200223955';
						$oficina='6153';
						$identificador='ES08001';
					}
				break;
				case '2':
					$nombre='ASESORES Y CONSULTORES GRUP QUALIA SL                                 ';
					$cif='B66453978';
					if($banco=='SABADELL'){
						$numCuenta='ES0500814199210001986703';
						$oficina='4853';
						$identificador='ES78001';
					} else if($banco=='CAIXA'){
						$numCuenta='ES6221000815500200939485'; //IBAN de la cuenta
						$oficina='0815'; //Oficina de la cuenta https://es.ibancalculator.com/iban_validieren.html
						$identificador='ES78001B66453978'; //Identificador de acreedor
					} else{
						$numCuenta='ES7101824853510201572829';
						$oficina='4853';
						$identificador='ES78001';
					}
				break;
				case '3':
					$nombre='QUALIA FORMACION SL                                                   ';
					$cif='B66473943';
					if($banco=='SABADELL'){
						$numCuenta='ES2400814199200001986604';
						$oficina='4853';
						$identificador='ES52001';
					} else if($banco=='CAIXA'){
						$numCuenta='ES7921000815590200939611';
						$oficina='0815';
						$identificador='ES52001B66473943';
					} else{
						$numCuenta='ES5001824853510201573013';
						$oficina='4853';
						$identificador='ES08001';
					}
				break;
				case '4':
					$nombre='QUALIA EDUCATIVA SL                                                   ';
					$cif='B66823865';
					if($banco=='SABADELL'){
						$numCuenta='ES2400814199200001986604';
						$oficina='4853';
						$identificador='ES52001';
					}else{
						$numCuenta='ES5001824853510201573013';
						$oficina='4853';
						$identificador='ES08001';
					}
				break;
			}
			
			fputs($file,'01'.$tipoSepa.'001'.$identificador.$cif.'                   '.$nombre.date('Ymd').'PRE'.date('Ymd').'09080500000             '.$bancoRemesa.$oficina);
			
			$m=168;
			while($m<=601){
				fputs($file,' ');
				$m++;
			}
			
			fputs($file,chr(13).chr(10));
			while(strlen($nombre)<212){
				$nombre=$nombre.' ';
			}
			fputs($file,'02'.$tipoSepa.'002'.$identificador.$cif.'                   '.date('Ymd').$nombre.$numCuenta);
			
			$m=291;
			while($m<=601){
				fputs($file,' ');
				$m++;
			}
			
			fputs($file,chr(13).chr(10));
			
			//XML CONFIG
			
			$pagos[]=array();	
			$fecha = date('Y-m-d');
			$nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
			$nuevafecha = date ( 'Y-m-d' , $nuevafecha );
			$nuevafechaData = str_replace('-','',$nuevafecha);
			$controlSepa=array('19143'=>'CORE','19445'=>'B2B');
			
			$nombreXml=array('1'=>'GRUP QUALIA ASSOCIATS SL','2'=>'ASESORES Y CONSULTORES GRUP QUALIA SL','3'=>'QUALIA FORMACION SL','4'=>'QUALIA EDUCATIVA SL');
			if($banco=='SABADELL'){
				$ibanXml=array('1'=>'ES6400810454460001193421','2'=>'ES0500814199210001986703','3'=>'ES2400814199200001986604','4'=>'ES2400814199200001986604');
				$bicXml=array('1'=>'ES6400810454460001193421','2'=>'ES0500814199210001986703','3'=>'ES2400814199200001986604','4'=>'ES2400814199200001986604');
				$bicSenalado='BSABESBBXXX';
			} else if($banco=='CAIXA'){
				$ibanXml=array('1'=>'ES9621000815550200939598','2'=>'ES6221000815500200939485','3'=>'ES7921000815590200939611','4'=>'ES0801824775550200313563');// IBAN de las cuentas
				$bicXml=array('1'=>'ES9621000815550200939598','2'=>'ES6221000815500200939485','3'=>'ES7921000815590200939611','4'=>'ES0801824775550200313563');// IBAN de las cuentas
				$bicSenalado='CAIXESBBXXX';//BIC del Banco
			} else{
				$ibanXml=array('1'=>'ES5720136153670200223955','2'=>'ES7101824853510201572829','3'=>'ES5001824853510201573013','4'=>'ES0801824775550200313563');
				$bicXml=array('1'=>'ES5720136153670200223955','2'=>'ES7101824853510201572829','3'=>'ES5001824853510201573013','4'=>'ES5001824853510201573013');
				$bicSenalado='BBVAESMM';
			}
			$idXml=array('1'=>'ES08001B65775850','2'=>'ES78001B66453978','3'=>'ES52001B66473943','4'=>'ES55000B66823865');
			$config = array("name" => $nombreXml[$datos['firma']],
                "IBAN" => $ibanXml[$datos['firma']],
                "BIC" => $bicSenalado,
                "batch" => true,
                "creditor_id" => $idXml[$datos['firma']],
                "currency" => "EUR",
				"tipoRemesa" => $controlSepa[$tipoSepa],
				"id_cabecera" => $idXml[$datos['firma']]
                );
			//FIN XML CONFIG
		}
		
		if(strlen($datos['coste'])<11){
			switch(strlen($datos['coste'])){
				case 0:
					$datos['coste']='00000000000'.$datos['coste'];
				break;
				case 1:
					$datos['coste']='0000000000'.$datos['coste'];
				break;
				case 2:
					$datos['coste']='000000000'.$datos['coste'];
				break;
				case 3:
					$datos['coste']='00000000'.$datos['coste'];
				break;
				case 4:
					$datos['coste']='0000000'.$datos['coste'];
				break;
				case 5:
					$datos['coste']='000000'.$datos['coste'];
				break;
				case 6:
					$datos['coste']='00000'.$datos['coste'];
				break;
				case 7:
					$datos['coste']='0000'.$datos['coste'];
				break;
				case 8:
					$datos['coste']='000'.$datos['coste'];
				break;
				case 9:
					$datos['coste']='00'.$datos['coste'];
				break;
				case 10:
					$datos['coste']='0'.$datos['coste'];
				break;
			}
		}
		if(strlen($datos['refCliente'])<5){
			switch(strlen($datos['refCliente'])){
				case 0:
					$datos['refCliente']='00000'.$datos['refCliente'];
				break;
				case 1:
					$datos['refCliente']='0000'.$datos['refCliente'];
				break;
				case 2:
					$datos['refCliente']='000'.$datos['refCliente'];
				break;
				case 3:
					$datos['refCliente']='00'.$datos['refCliente'];
				break;
				case 4:
					$datos['refCliente']='0'.$datos['refCliente'];
				break;
			}
		}
		if(mb_strlen($datos['empresa'], 'UTF-8')<70){
			while(mb_strlen($datos['empresa'], 'UTF-8')<70){
				$datos['empresa']=$datos['empresa'].' ';
			}
		}
		
		if($datos['direccion']==""){
			$datos['direccion']=$datos['direccion'].' ';
		}
		if(mb_strlen($datos['direccion'], 'UTF-8')<50){
			while(mb_strlen($datos['direccion'], 'UTF-8')<50){
				$datos['direccion']=$datos['direccion'].' ';
			}
		}
		
		if($datos['localidad']==""){
			$datos['localidad']=$datos['localidad'].' ';
		}
		if(mb_strlen($datos['localidad'], 'UTF-8')<44){
			while(mb_strlen($datos['localidad'], 'UTF-8')<44){
				$datos['localidad']=$datos['localidad'].' ';
			}
		}
		
		if($datos['provincia']==""){
			$datos['provincia']=$datos['provincia'].' ';
		}
		if(mb_strlen($datos['provincia'], 'UTF-8')<39){
			while(mb_strlen($datos['provincia'], 'UTF-8')<39){
				$datos['provincia']=$datos['provincia'].' ';
			}
		}
		
		if($datos['bic']==""){
			$datos['bic']=$datos['bic'].'0';
		}
		if(mb_strlen($datos['bic'], 'UTF-8')<11){
			while(mb_strlen($datos['provincia'], 'UTF-8')<11){
				$datos['provincia']=$datos['provincia'].'0';
			}
		}
	    //vamos añadiendo el contenido
	    fputs($file,'03'.$tipoSepa.'003F/'.$datos['referencia']."                            ".$datos['refCliente']." ".$datos['cifCliente']."".date('Y')."                RCUR    ".$datos['coste']."".$nuevafechaData."".$datos['bic']."".$datos['empresa']."".$datos['direccion']."".$datos['cp']." ".$datos['localidad']."".$datos['provincia']." ES                                                                        A".$datos['numCuenta']."              N/FACTURA F/".$datos['referencia']." DE FECHA ".formateaFechaWeb($datos['fechaEmision']));
	    
		$k=480;
		while($k<=601){
			fputs($file,' ');
			$k++;
		}
		
		fputs($file,chr(13).chr(10));
		$i++;
		$j++;
		
		//Líneas de XML Remesas
		$pagos[$controlador]=array("name" => sanear_string($datos['empresa']),
                 "IBAN" => $datos['numCuenta'],
                 "BIC" => $datos['bic'],
                 "amount" => $datos['coste'],
                 "type" => "RCUR",
                 "collection_date" => $datos['fechaVencimiento'],
                 "mandate_id" => $datos['referencia'],
                 "mandate_date" => $nuevafecha,
                 "description" => "N FACTURA F ".$datos['referencia']." DE FECHA ".$datos['fechaEmision']
                );
		//Fin Líneas de XML Remesas
		$controlador++;
	}
	
	//FINALIZACIÓN DE LA REMESA
	$costeTotal=number_format((float)$costeTotal, 2, '.', '');
	$costeTotal=str_replace('.','',$costeTotal);
	if(strlen($costeTotal)<17){
		while(strlen($costeTotal)<17){
			$costeTotal='0'.$costeTotal;
		}
	}
	$i--;
	$k=$i+2;
	if(strlen($k)<10){
		while(strlen($k)<10){
			$k='0'.$k;
		}
	}
	if(strlen($i)<8){
		while(strlen($i)<8){
			$i='0'.$i;
		}
	}
	
	fputs($file,"04$identificador$cif                   ".date('Ymd')."".$costeTotal."".$i."".$k);
	$j=$i;
	
	$i=82;
	while($i<=601){
		fputs($file,' ');
		$i++;
	}
	
	fputs($file,chr(13).chr(10));
	$k++;
	if(strlen($k)<10){
		while(strlen($k)<10){
			$k='0'.$k;
		}
	}
	fputs($file,"05$identificador$cif                   ".$costeTotal."".$j."".$k);
	
	$i=74;
	while($i<=601){
		fputs($file,' ');
		$i++;
	}
	
	fputs($file,chr(13).chr(10));
	$k+=2;
	if(strlen($k)<10){
		while(strlen($k)<10){
			$k='0'.$k;
		}
	}
	fputs($file,"99".$costeTotal."".$j."".$k." ");
	
	$i=40;
	while($i<=601){
		fputs($file,' ');
		$i++;
	}
	fputs($file,chr(13).chr(10));
	cierraBD();
	
	fclose($file);

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	
	$objWriter->save('documentos/facturas'.$tiempo.'.xlsx');
	
	//Creación del fichero XML Remesa
	$SEPASDD = new SEPASDD($config);
	$i=1;
	foreach ($pagos as &$valor) {
		$SEPASDD->addPayment($valor,$i);
		$i++;
	}
    $xml=$SEPASDD->save();
	$myfile = fopen("documentos/remesa".$tiempo.".xml", "w") or die("Imposible abrir el fichero");
	fwrite($myfile, $xml);
	fclose($myfile);
	//Fin Creación del fichero XML Remesa
	
	$zip = new ZipArchive();
	
	$fichero = 'documentos/Facturacion.zip';
	
	unlink($fichero);
	
	if($zip->open($fichero,ZIPARCHIVE::CREATE)===true) {
		$zip->addFile('documentos/facturas'.$tiempo.'.xlsx' , 'facturas'.$tiempo.'.xlsx');
		$zip->addFile('documentos/remesa'.$tiempo.'.dat' , 'remesa'.$tiempo.'.dat');
		$zip->addFile('documentos/remesa'.$tiempo.'.xml' , 'remesa'.$tiempo.'.xml');
		if(!$zip->close()){
			echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
			echo $zip->getStatusString();
		}
	}

	
	// Definir headers
	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=Facturacion.zip");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/Facturacion.zip');

	
	/*
	// Definir headers
	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=facturas".$tiempo.".xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/facturas'.$tiempo.'.xlsx');*/
?>