<?php
  include_once('funciones.php');
  
  $codigoCurso=$_GET['codigo'];
  $fichero=generaXMLParticipantes($codigoCurso);

  // Definir headers
  header("Content-Type: application/xml");
  header("Content-Disposition: attachment; filename=$fichero");
  header("Content-Transfer-Encoding: binary");

  // Descargar archivo
  readfile("documentos/$fichero");

?>