<?php
  $seccionActiva=2;
  include_once('cabecera.php');
  
  if(isset($_POST['codigo'])){
	if(isset($_POST['iban'])){
		$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
	}
	if($_POST['tieneColaborador']=='NO'){
		$_POST['comercial']='NULL';
	}
    $res=actualizaCliente();
  }
  elseif(isset($_POST['empresa'])){
	$_POST['numCuenta']=$_POST['iban'].$_POST['numCuenta'];
	if($_POST['tieneColaborador']=='NO'){
		$_POST['comercial']='NULL';
	}
    $res=altaCliente('SI');
  }
  elseif(isset($_POST['completaAlumnos'])){
    $res=completaAlumnos();
  }
  elseif(isset($_POST['codigoC'])){
   $res=registraVenta();
  }
  elseif(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaCliente();
  }

  if(isset($_GET['action'])){
    foreach($_GET as $nombre => $valor){
        $_POST[$nombre]=$valor;
    } 
  }
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema sobre clientes</h6>

                   <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
              
                   <div class="leyenda">
                    <span class="grafico grafico2"></span>Totales: <span id="valor1"></span><br>
                    <span class="grafico grafico1"></span>Confirmados: <span id="valor2"></span><br>
                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de cuentas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				      <a href="cuentas.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
              <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Datos del cliente actualizados."); 
            }
            else{
              mensajeError("no se han podido actualizar los datos del cliente. Compruebe los datos introducidos."); 
            }
          }
		  elseif(isset($_POST['empresa'])){
            if($res){
              mensajeOk("Cliente registrado correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar el cliente. Compruebe los datos introducidos."); 
            } 
          }
		  elseif(isset($_POST['completaAlumnos'])){
            if($res){
              mensajeOk("Venta registrada y alumnos completados."); 
            }
            else{
              mensajeError("no se ha podido completar los datos de los alumnos. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['codigoC'])){
  		      if($res){
              mensajeOk("Venta registrada y cliente establecido."); 
            }
            else{
              mensajeError("no se ha podido registrar la venta. Compruebe los datos introducidos."); 
            }
		      }
          elseif(isset($_POST['destinatarios'])){
            if($res){
              mensajeOk("Mensaje enviado correctamente."); 
            }
            else{
              mensajeError("no se ha podido enviar el mensaje. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el posible cliente. Contacte con el webmaster."); 
            }
          }

        ?>
		
        <div class="widget widget-table action-table cajaSelect">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Cuentas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
				 <table class='table table-striped table-bordered datatable' id='tablaCuentas'>
					<thead>
					  <tr>
						<th> ID </th>
						<th> Nombre del cliente </th>
						<th> Estado </th>
						<th> Resolución </th>
						<th> CIF </th>
						<th> Población </th>
						<th> Comercial asignado </th>
						<th> Actividad </th>
						<th> Servicio Contratado </th>
						<th> Fecha ult. venta </th>
						<th class='td-actions'> </th>
						<th><input type='checkbox' id='todo'></th>
					  </tr>
					</thead>
					<tbody>
						<?php //imprimeCuentas(); ?>
					</tbody>
              </table>
                
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<!--script src="js/filtroTabla.js"></script-->
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/excanvas.min.js" type="text/javascript"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
  <?php
    $datos=generaDatosGraficoCuentasFiltrado();
  ?>
  $('#tablaCuentas').dataTable({
      'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoCuentasFiltrado.php?action=getMembersAjx&comercial=<?php echo $_POST['comercial']; ?>&servicio=<?php echo $_POST['servicio']; ?>&poblacion=<?php echo $_POST['poblacion']; ?>&estado=<?php echo $_POST['estado']; ?>&cp=<?php echo $_POST['cp']; ?>&colaborador=<?php echo $_POST['colaborador']; ?>&actividad=<?php echo $_POST['actividad']; ?>&comprado=<?php echo $_POST['comprado']; ?>&tieneTrabajadores=<?php echo $_POST['tieneTrabajadores']; ?>&tieneWeb=<?php echo $_POST['tieneWeb']; ?>&fechaUno=<?php echo $_POST['fechaUno']; ?>&fechaDos=<?php echo $_POST['fechaDos']; ?>&compra=<?php echo $_POST['compra']; ?>&baja=<?php echo $_POST['baja']; ?>&motivoBaja=<?php echo $_POST['motivoBaja']; ?>&provincia=<?php echo $_POST['provincia']; ?>',
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          $('td:eq(8)', nRow).addClass( "centro" );
       },
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });
  var pieData = [
      {
          value: <?php echo $datos['totales']; ?>,
          color: "#B02B2C"
      },
      {
          value: <?php echo $datos['confirmados']; ?>,
          color: "#428bca"
      }
    ];

  var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
  
  $("#valor1").text("<?php echo $datos['totales']; ?>");
  $("#valor2").text("<?php echo $datos['confirmados']; ?>");

  $('#email').click(function(){
    var valoresChecks=recorreChecks();
    creaFormulario('enviarEmail.php?seccion=2',valoresChecks,'post');
  });


  $('#tareas').click(function(){
      var valoresChecks=recorreChecks();
      creaFormulario('creaTarea.php',valoresChecks,'post');
  });


  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('cuentasFiltrado.php?action=getMembersAjx&comercial=<?php echo $_POST['comercial']; ?>&servicio=<?php echo $_POST['servicio']; ?>&poblacion=<?php echo $_POST['poblacion']; ?>&estado=<?php echo $_POST['estado']; ?>&cp=<?php echo $_POST['cp']; ?>&colaborador=<?php echo $_POST['colaborador']; ?>&actividad=<?php echo $_POST['actividad']; ?>&comprado=<?php echo $_POST['comprado']; ?>&tieneTrabajadores=<?php echo $_POST['tieneTrabajadores']; ?>&tieneWeb=<?php echo $_POST['tieneWeb']; ?>&fechaUno=<?php echo $_POST['fechaUno']; ?>&fechaDos=<?php echo $_POST['fechaDos']; ?>&compra=<?php echo $_POST['compra']; ?>&baja=<?php echo $_POST['baja']; ?>&motivoBaja=<?php echo $_POST['motivoBaja']; ?>&provincia=<?php echo $_POST['provincia']; ?>',valoresChecks,'post');
    }

  });
</script>
