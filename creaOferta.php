<?php
  $seccionActiva=12;
  include_once('cabecera.php');
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nueva Oferta</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="ofertas.php" method="post">
                  <fieldset>

                    <?php
					  campoOculto($_SESSION['codigoS'],'codigoUsuario');
                      $consulta="SELECT codigo, empresa AS texto FROM clientes;";
                      campoSelectConsulta('codigoCliente','Cliente',$consulta,false,'selectpicker span4 show-tick');
                    
                      $consulta="SELECT codigo, CONCAT(codigoProducto,' - ',nombreProducto) AS texto FROM productos;";
                      campoSelectConsulta('codigoProducto','Producto',$consulta,false,'selectpicker span4 show-tick');

                      campoOculto('','codigoOferta');
                      campoOculto('CURSO','estado');
                      campoTextoSimbolo('precio','Precio de la oferta','€');
                      campoFecha('fechaOferta','Fecha de emisión');
                      areaTexto('observaciones','Observaciones');
                    ?>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Registrar Oferta</button> 
                      <a href="ofertas.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
  });
</script>