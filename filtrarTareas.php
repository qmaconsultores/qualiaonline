<?php
  $seccionActiva=3;
  include_once("cabecera.php");
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-calendar"></i><i class="icon-chevron-right"></i><i class="icon-filter"></i>
              <h3>Filtrado de tareas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              <h3>Seleccione el comercial para filtrar tareas:</h3><br><br>
				<form action='tareasFiltrado.php' method='post'>
					<?php 
						if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
							$consulta="SELECT codigo, CONCAT(nombre, ' ', apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' AND (codigo='".$_SESSION['codigoS']."' OR codigo IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador='".$_SESSION['codigoS']."'));";
						}elseif($_SESSION['tipoUsuario']!='ADMIN'){
							$consulta="SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' AND (codigo='".$_SESSION['codigoS']."' OR codigo IN (SELECT codigo FROM usuarios WHERE directorAsociado =  '".$_SESSION['codigoS']."')) ORDER BY nombre, apellidos;";
						}else{
							$consulta="SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' ORDER BY nombre, apellidos;";
						}
						campoSelectConsulta('comercial','',$consulta,false,'selectpicker span3 show-tick',"data-live-search='true'",'',1);
					?>
					<br>
					
					<button type="submit" class="btn btn-primary">Seleccionar <i class="icon-circle-arrow-right"></i></button>
				</form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();

  });
</script>