<?php
  $seccionActiva=5;
  include_once('cabecera.php');
  include_once('matrizAcciones.php');
  $codigo=$_GET['codigo'];
  $datos=datosAccionFormativa($codigo);
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Detalles acción formativa</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="accionesFormativas.php" method="post">
                  <fieldset>
					
					          <div class="control-group">                     
                      <label class="control-label" for="codigoInterno">Acción:</label>
                      <div class="controls">
                        <input type="text" class="input-mini" id="codigoInterno" name="codigoInterno" value="<?php echo $datos['codigoInterno']; ?>">
						<input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="denominacion">Denominación:</label>
                      <div class="controls">
                        <input type="text" class="span4" id="denominacion" name="denominacion" value="<?php echo $datos['denominacion']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


					<?php 
						campoTexto('urlAccion','URL',$datos);
						campoTexto('usuarioAccion','Usuario',$datos);
						campoTexto('claveAccion','Contraseña',$datos);
						campoSelect('grupoAcciones','Grupo de acciones',devuelveArrayTextos(),devuelveValores(),$datos); 
					?>



                    <div class="control-group">                     
                      <label class="control-label" for="tipoAccion">Tipo de acción:</label>
                      <div class="controls">
                        <select name="tipoAccion" class='selectpicker span4'>
                          <option value="PROPIA" <?php if($datos['tipoAccion']=="PROPIA"){ echo "selected='selected'"; } ?>>Propia</option>
                          <option value="VINCULADA" <?php if($datos['tipoAccion']=="VINCULADA"){ echo "selected='selected'"; } ?>>Vinculada a la obtención de un Cert. de Profesionalidad</option>
                        </select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="nivelAccion">Nivel de acción:</label>
                      <div class="controls">
                        <select name="nivelAccion" class='selectpicker span4'>
                          <option value="BASICO" <?php if($datos['nivelAccion']=="BASICO"){ echo "selected='selected'"; } ?>>Básico</option>
                          <option value="SUPERIOR" <?php if($datos['nivelAccion']=="SUPERIOR"){ echo "selected='selected'"; } ?>>Superior</option>
                        </select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="contenidos">Contenidos:</label>
                      <div class="controls">
						            <textarea name="contenidos" class="areaTextoAmplia"><?php echo $datos['contenidos']; ?></textarea>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="objetivos">Objetivos:</label>
                      <div class="controls">
                        <textarea name="objetivos" class="areaTextoAmplia"><?php echo $datos['objetivos']; ?></textarea>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="horas">Número de Horas:</label>
                      <div class="controls">
                        <input type="text" class="input-mini pagination-right" id="horas" name="horas" value="<?php echo $datos['horas']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="modalidad">Modalidad:</label>
                      <div class="controls">
                        
                        <select name="modalidad" class='selectpicker show-tick'>
                          <option value="PRESENCIAL" <?php if($datos['modalidad']=="PRESENCIAL"){ echo "selected='selected'"; } ?>>Presencial</option>
                          <option value="DISTANCIA" <?php if($datos['modalidad']=="DISTANCIA"){ echo "selected='selected'"; } ?>>A distancia</option>
                          <option value="MIXTA" <?php if($datos['modalidad']=="MIXTA"){ echo "selected='selected'"; } ?>>Mixta</option>
                          <option value="TELE" <?php if($datos['modalidad']=="TELE"){ echo "selected='selected'"; } ?>>Teleformación</option>
                          <option value="WEBINAR" <?php if($datos['modalidad']=="WEBINAR"){ echo "selected='selected'"; } ?>>Webinar</option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->




                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar acción formativa</button> 
                      <a href="accionesFormativas.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
  });
</script>
