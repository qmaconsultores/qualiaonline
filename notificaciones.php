<?php
  $seccionActiva=5;
  include_once('cabecera.php');

  $datos=datosCurso($_GET['codigo']);
  $codigosAlumnos=datosAlumnosCurso($_GET['codigo']);
  $datosNotificacion=datosNotificacion($_GET['codigo']);

  $firma=firmaUsuario();
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-bell-alt"></i>
              <h3>Notificaciones para el curso</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="formacion.php" method="post">
                  <fieldset>

                    <input type="hidden" name="codigoCursoNotificacion" value="<?php echo $datos['codigo']; ?>">

                    <div class="control-group">                     
                      <label class="control-label" for="accionFormativa">Acción formativa:</label>
                      <div class="controls">
                       <input type="text" class="span5" id="accionFormativa" name="accionFormativa" disabled="disabled" value="<?php echo $datos['denominacion']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    
                    <div class="control-group">                     
                      <label class="control-label" for="tutor">Tutor:</label>
                      <div class="controls">
                        <input type="text" class="input-medium" id="tutor" name="tutor" disabled="disabled" value="<?php echo $datos['apellidos'].', '.$datos['nombre']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fechaInicioCurso">Fecha de inicio del curso:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaInicioCurso" name="fechaInicioCurso" value="<?php echo formateaFechaWeb($datos['fechaInicio']); ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="fechaFinCurso">Fecha de finalización:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaFinCurso" name="fechaFinCurso" value="<?php echo formateaFechaWeb($datos['fechaFin']); ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="email">Alumnos:</label>
                      <div class="controls conBorde">
                       
                        <table class="table table-striped table-bordered datatable">
                          <thead>
                            <tr>
                              <th> Nombre </th>
                              <th> DNI </th>
                              <th> eMail </th>
                              <th> Teléfono </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              imprimeAlumnosNotificaciones($codigosAlumnos);
                            ?>
                          
                          </tbody>
                        </table>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="fechaInicio">Inicio de notificación:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaInicio" name="fechaInicio" value="<?php echo formateaFechaWeb($datosNotificacion['fechaInicio']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fechaFin">Fin de notificación:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaFin" name="fechaFin" value="<?php echo formateaFechaWeb($datosNotificacion['fechaFin']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="hora">Hora:</label>
                      <div class="controls">
                        <input type="text" class="input-mini" id="hora" name="hora" value="<?php echo $datosNotificacion['hora']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="repetir">Repetir todos los:</label>
                      <div class="controls">
                        <select name="repetir" class='selectpicker show-tick'>
                          <option value="LUNES" <?php if($datosNotificacion['repetir']=='LUNES'){ echo 'checked="checked"';} ?> >Lunes</option>
                          <option value="MARTES" <?php if($datosNotificacion['repetir']=='MARTES'){ echo 'checked="checked"';} ?> >Martes</option>
                          <option value="MIERCOLES" <?php if($datosNotificacion['repetir']=='MIERCOLES'){ echo 'checked="checked"';} ?> >Miércoles</option>
                          <option value="JUEVES" <?php if($datosNotificacion['repetir']=='JUEVES'){ echo 'checked="checked"';} ?> >Jueves</option>
                          <option value="VIERNES" <?php if($datosNotificacion['repetir']=='VIERNES'){ echo 'checked="checked"';} ?> >Viernes</option>
                          <option value="SABADOS" <?php if($datosNotificacion['repetir']=='SABADOS'){ echo 'checked="checked"';} ?> >Sábados</option>
                          <option value="DOMINGOS" <?php if($datosNotificacion['repetir']=='DOMINGOS'){ echo 'checked="checked"';} ?> >Domingos</option>
                        </select>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="mensajeCorreo"></label>
                      <div class="controls">
                        <textarea name="mensajeCorreo" id="mensajeCorreo"><?php if($datosNotificacion['mensaje']!=''){ echo $datosNotificacion['mensaje']; }else{ echo "Mensaje...<br>$firma"; } ?></textarea>
                      </div>
                    </div>


                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-bell-alt"></i> Establecer notificación</button> 
                      <a href="formacion.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script src="js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});

    $('#mensajeCorreo').wysihtml5({locale: "es-ES"});
  });
</script>