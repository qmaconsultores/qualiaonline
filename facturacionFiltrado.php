<?php
  $seccionActiva=14;
  include_once('cabecera.php');
  $res=false;
  
  cargaVariables();
  
  $fechaUno=formateaFechaBD($_SESSION['fechaUno']);
  $fechaDos=formateaFechaBD($_SESSION['fechaDos']);
  $devuelta=$_SESSION['devuelta'];
  $pagada=$_SESSION['pagada'];
  $servicio=$_SESSION['servicio'];
  $condicionServicio='';
  if($servicio!='todos'){
	$condicionServicio="AND facturacion.concepto='$servicio'";
  }
  
  $empresa='';
  if(isset($_GET['codigoCliente'])){
	$codigoCliente=$_GET['codigoCliente'];
	$consulta=consultaBD("SELECT empresa FROM clientes WHERE codigo='$codigoCliente';",true);
	$datosCliente=mysql_fetch_assoc($consulta);
	$empresa=$datosCliente['empresa'];
  }
  
  if(isset($_POST['codigo'])){
	$_POST['coste']=formateaNumero($_POST['coste'],true);
    $res=actualizaDatos('facturacion',$_POST['codigo']);
	$res=$res && insertaHistoricoFacturas($_POST['codigo']);
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('facturacion');
  }

  $estadisticas=creaEstadisticasFacturasDevoluciones('WHERE fechaVencimiento>="'.$fechaUno.'" AND fechaVencimiento<="'.$fechaDos.'" AND devuelta="'.$devuelta.'" AND cobrada="'.$pagada.'" '.$condicionServicio,'WHERE fecha>="'.$fechaUno.'" AND fecha<="'.$fechaDos.'" AND cobrada="'.$pagada.'"');
  $pendientes=$estadisticas['facturacion']-$estadisticas['cobradas'];
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-tasks"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de facturación:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-ticket"></i> <span class="value"><?php echo $estadisticas['facturacion']?></span> <br>Facturas emitidas</div>
                    <!-- .stat -->
                    <div class="stat"> <i class="icon-credit-card"></i> <span class="value"><?php echo $estadisticas['cobradas']?></span> <br>Facturas percibidas</div>
                    <!-- .stat --> 
                     <div class="stat"> <i class="icon-time"></i> <span class="value"><?php echo $pendientes?></span> <br>Facturas pendientes de cobro</div>
                    <!-- .stat -->
					<div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Total facturado</div>
                    <!-- .stat -->
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
		<?php  if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='CONSULTORIA' && $_SESSION['tipoUsuario']!='FORMACION'){ ?>
		
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de facturación</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<a href="facturacion.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
						<?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		<?php } ?>

      <div class="span12">
        
        <?php 
          mensajeResultado('referencia',$res,'factura');//Fusionamos el mensaje de inserción y actualización
		  mensajeResultado('firma1',$res,'firmas');//Fusionamos el mensaje de inserción y actualización
          mensajeResultado('elimina',$res,'factura', true);
        ?>
        
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Facturas emitidas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
					<th> Referencia </th>
					<th> Cliente </th>
                    <th> Concepto </th>
                    <th> Fecha de emisión </th>
					<th> Fecha de vencimiento </th>
					<th> Importe </th>
					<th> ¿Cobrada? </th>
					<th> ¿Enviada banco? </th>
					<th> ¿Enviada cliente? </th>
					<th> Devolución y resolución </th>
                    <th class="centro"> </th>
					<th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeFacturasDevolucion('WHERE fechaVencimiento>="'.$fechaUno.'" AND fechaVencimiento<="'.$fechaDos.'" AND devuelta="'.$devuelta.'" AND cobrada="'.$pagada.'" '.$condicionServicio,'WHERE fecha>="'.$fechaUno.'" AND fecha<="'.$fechaDos.'" AND cobrada="'.$pagada.'"',false,true);

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
	<?php
	echo "var marcado='".$empresa."';";
  ?>
    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes una factura.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('facturacionFiltrado.php',valoresChecks,'post');
      }

    });
	
	var tabla=$('.datatable').DataTable({
		"aaSorting": [],
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave":false,
		"iDisplayLength":25,
		"oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
		  'sZeroRecords':'No se han encontrado coincidencias'
    }});

    tabla.fnFilter(marcado);
</script>