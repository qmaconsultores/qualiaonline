<?php
  $seccionActiva=16;
  include_once('cabecera.php');
  
  $res=false;
  if(isset($_POST['codigo'])){
    //$res=actualizaDatos('software');
    $res=actualizaSoftware();
  }
  elseif(isset($_POST['tipoTarea'])){
    $res=insertaDatos('software');
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('software');
  }

  $estadisticas=estadisticasGenericas('software');
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas sobre Software</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-laptop"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Tareas registradas</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Tareas de Software</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="creaSoftware.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Nueva tarea</span> </a>
                <a href="tododoindone.php" class="shortcut"><i class="shortcut-icon icon-columns"></i><span class="shortcut-label">ToDo-Doing-Done</span> </a>
                <a href="vistaTiempos.php" class="shortcut"><i class="shortcut-icon icon-time"></i><span class="shortcut-label">Vista de tiempos</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        
        <?php
          mensajeResultado('tipoTarea',$res,'tarea');
          mensajeResultado('elimina',$res,'tarea', true);
        ?>
		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Tareas de Software registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Cliente </th>
                    <th> Tipo de Tarea </th>
                    <th> Fecha de Solicitud </th>
                    <th> Programador asignado </th>
                    <th> ¿Realizado? </th>
                    <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeTareasSoftware();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes una tarea del listado.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('software.php',valoresChecks,'post');
      }

    });
</script>