<?php
  $get='';
  if(isset($_GET['cuentas']) && $_GET['cuentas']=='SI'){
	$seccionActiva=2;
	$get='?cuentas=SI';
  }else{
	$seccionActiva=1;
  }
  include_once('cabecera.php');
  $res=true;
  
  if(isset($_POST['codigoUsuario'])){
    $res=creaCartera();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('carteras_comerciales');
  }
  
  $where=compruebaPerfilParaWhere();
  $estadisticas=estadisticasGenericasWhere('carteras_comerciales',$where);

?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de carteras comerciales</h6>

                   <div id="big_stats" class="cf">

                    <div class="stat"> <i class="icon-briefcase"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Carteras registradas</div>
                    <!-- .stat -->

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de carteras</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content prueba">
              <div class="shortcuts">
				        <a href="creaCartera.php<?php echo $get; ?>" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Nueva cartera</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>	


      <div class="span12">
        
        <?php
          mensajeResultado('codigoUsuario',$res,'cartera');
          mensajeResultado('elimina',$res,'cartera', true);
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Ventas realizadas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			   <table class='table table-striped table-bordered datatable'>
                <thead>
                  <tr>					
                    <th> Comercial </th>
					<th> Fecha de subida </th>
					<th> Colaborador </th>
                    <th class='centro'></th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>
				
				<?php imprimeCarteras(); ?>
				
				</tbody>
              </table>
                
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
$('#eliminar').click(function(){
  var valoresChecks=recorreChecks();
  if(valoresChecks['codigo0']==undefined){
    alert('Por favor, seleccione antes una cartera.');
  }
  else{
    valoresChecks['elimina']='SI';
    creaFormulario('carteras.php',valoresChecks,'post');
  }

});
</script>
