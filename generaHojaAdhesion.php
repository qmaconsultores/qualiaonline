<?php
	/*
	Parte común.
	En este caso no figura $seccionActiva, porque este fichero PHP nunca se va a mostrar en el navegador.
	Al pulsar en el botón que lo enlaza, el fichero debe descargarse, pero la página no cambiará.
	Esto es así porque al final modificamos las cabeceras HTTP del navegador para decirle que lo que se le va
	a enviar es un documento Word. Para que funcione, no se debe imprimir NADA por pantalla (ningún echo).
	*/
	session_start();
	include_once("funciones.php");
  	compruebaSesion();
  	//Fin parte común

	//Carga de la librería PHPWord
	require_once 'phpword/PHPWord.php';

	/*
	Carga de la plantilla (pon la ruta y la extensión que tenga tu fichero). El fichero con las etiquetas
	debes ponerlo en alguna subcarpeta dentro de la del proyecto.
	*/
	$codigo=$_GET['codigo'];
	$PHPWord = new PHPWord();
	
	
	conexionBD();
	
	$consulta=consultaBD("SELECT DISTINCT clientes.*
							FROM clientes
							INNER JOIN ventas ON clientes.codigo = ventas.codigoCliente
							INNER JOIN alumnos ON alumnos.codigoVenta = ventas.codigo
							INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno
							WHERE alumnos_registrados_cursos.codigoCurso=$codigo;");
							
	$datosEmpresa=mysql_fetch_assoc($consulta);
	
	while($datosEmpresa!=0){
		$document = $PHPWord->loadTemplate('documentos/plantillacuatro.docx');
		
		$document->setValue("representante",utf8_decode($datosEmpresa['contacto']));
		$document->setValue("dniRepresentante",utf8_decode($datosEmpresa['dniRepresentante']));
		$document->setValue("empresa",utf8_decode($datosEmpresa['empresa']));
		$document->setValue("cif",utf8_decode($datosEmpresa['cif']));
		$document->setValue("actividad",utf8_decode($datosEmpresa['sector']));
		$document->setValue("tlf",utf8_decode($datosEmpresa['telefono']));
		$document->setValue("sede",utf8_decode($datosEmpresa['direccion']));
		$document->setValue("poblacion",utf8_decode($datosEmpresa['localidad']));
		$document->setValue("cp",utf8_decode($datosEmpresa['cp']));
		$document->setValue("provincia",utf8_decode($datosEmpresa['provincia']));
		$document->setValue("ccc",utf8_decode($datosEmpresa['ccc']));
		$document->setValue("mail",utf8_decode($datosEmpresa['mail']));
		
		switch($datosEmpresa['representacion']){
			case "SI":
				$document->setValue("rl",utf8_decode("X"));
				$document->setValue("norl",utf8_decode(""));
			break;
			case "NO":
				$document->setValue("rl",utf8_decode(""));
				$document->setValue("norl",utf8_decode("X"));
			break;
		}
		
		switch($datosEmpresa['nuevacreacion']){
			case "SI":
				$document->setValue("nu",utf8_decode("X"));
				$document->setValue("nonu",utf8_decode(""));
			break;
			case "NO":
				$document->setValue("nu",utf8_decode(""));
				$document->setValue("nonu",utf8_decode("X"));
			break;
		}
		
		$document->setValue("fecha",devuelveFecha());
		
		$document->save('documentos/Adhesion-'.$datosEmpresa['empresa'].'.docx');
		
		$datosEmpresa=mysql_fetch_assoc($consulta);
		
	}
	


		/*
		header("Content-Type: application/vnd.ms-docx");
		header("Content-Disposition: attachment; filename=listadoAlumnosCurso.docx");
		header("Content-Transfer-Encoding: binary");

		readfile('documentos/listadoAlumnosCurso.docx');*/
	
	cierraBD();  

?>