<?php
  $seccionActiva=5;
  include_once('cabecera.php');
  
  $empresa='';
  if(isset($_GET['codigoCliente'])){
	$codigoCliente=$_GET['codigoCliente'];
	$consulta=consultaBD("SELECT empresa FROM clientes WHERE codigo='$codigoCliente';",true);
	$datosCliente=mysql_fetch_assoc($consulta);
	$empresa=$datosCliente['empresa'];
  }
  
  if(isset($_POST['codigo'])){
	if(isset($_POST['esAlumno'])){
		$res=actualizaAlumno(true);
	}else{
		if(!isset($_POST['tutorDistancia'])){
			$_POST['tutorDistancia']='NULL';
		}
		if(!isset($_POST['titularidadDistancia'])){
			$_POST['titularidadDistancia']='';
		}
		$res=actualizaCurso();
	}
  }
  elseif(isset($_POST['accionFormativa'])){
    $res=registraCurso();
  }
  elseif(isset($_POST['destinatarios'])){
    $res=enviaCorreos();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaCurso();
  }
  elseif(isset($_POST['eliminaAlumno']) && $_POST['eliminaAlumno']=='SI'){
    $res=eliminaDatos('alumnos');
  }
  elseif(isset($_POST['codigoCursoNotificacion'])){
    $res=registraNotificacion();
  }
  elseif(isset($_POST['codigoCurso'])){
	$res=actualizaCostes();
  }

  $estadisticas=creaEstadisticasCursos();
  $estadisticasAlumnos=creaEstadisticasAlumnos();

  $consulta=consultaBD("SELECT alumnos.codigo, nombre, apellidos, alumnos.telefono, alumnos.mail, empresa, fechaFin, clientes.codigo AS codigoCliente, alumnos_registrados_cursos.codigoCurso AS codigoCurso, alumnos_registrados_cursos.llamadaBienvenida, alumnos.fechaUltimaLlamada, alumnos_registrados_cursos.fechaBienvenida, alumnos_registrados_cursos.horaBienvenida, alumnos_registrados_cursos.resolucion, alumnos.codigoVenta FROM (alumnos LEFT JOIN ventas ON alumnos.codigoVenta=ventas.codigo) LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo INNER JOIN alumnos_registrados_cursos ON alumnos.codigo=alumnos_registrados_cursos.codigoAlumno INNER JOIN cursos ON alumnos_registrados_cursos.codigoCurso=cursos.codigo WHERE 1=1 AND cursos.fechaFin>=CURDATE() ORDER BY apellidos, nombre; ",true);

  /*$i=1;
  while($alumno=mysql_fetch_assoc($consulta)){
    $trabajo=datosRegistro('trabajos',$alumno['codigoVenta'],'codigoOferta');
    echo $i.' - '.$alumno['nombre'].' '.$alumno['apellidos'].'. Venta: '.$alumno['codigoVenta'].'. Trabajo: '.$trabajo['codigo'].'<br/>';
    $i++;
  }*/
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para formación</h6>

                   <div id="big_stats" class="cf">

                     <div class="stat"> <i class="icon-book"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Cursos registrados</div>
					 <div class="stat"> <i class="icon-pencil"></i> <span class="value"><?php echo $estadisticasAlumnos['total']?></span> <br>Alumnos registrados en cursos</div>
                      <!-- .stat -->

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span12 -->
		
		<?php  if($_SESSION['tipoUsuario']!='COMERCIAL' && $_SESSION['tipoUsuario']!='CONSULTORIA'){ ?>
		
        <div class="span12">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de formación</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				<h2 class="arribaFormacion">Cursos</h2>
                <a href="accionesFormativas.php" class="shortcut"><i class="shortcut-icon icon-book"></i><span class="shortcut-label">Acciones Form.</span> </a>
                <a href="tutores.php" class="shortcut"><i class="shortcut-icon icon-user"></i><span class="shortcut-label">Tutores</span> </a>
                <a href="creaCurso.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Crear Curso</span> </a>
                <br>
                <?php 
                compruebaOpcionExcel('4');
        				?>
        				<a href="generaDocumentosXml.php" class="shortcut"><i class="shortcut-icon icon-download"></i><span class="shortcut-label">Ficheros XML</span> </a>
						<a href="seleccionaAnioCursos.php" class="shortcut"><i class="shortcut-icon icon-archive"></i><span class="shortcut-label">Archivo</span> </a>
        				<?php
                compruebaOpcionEliminar();
                ?>
				<h2 class="apartadoFormacion">Alumnos</h2>
				<a href="alumnosPendientes.php" class="shortcut"><i class="shortcut-icon icon-exclamation"></i><span class="shortcut-label">Pendientes</span> </a>
				<a href="alumnosFinalizados.php" class="shortcut"><i class="shortcut-icon icon-archive"></i><span class="shortcut-label">Finalizados</span> </a>
                <a href="#" id='email' class="shortcut"><i class="shortcut-icon icon-envelope"></i><span class="shortcut-label">Enviar eMail</span> </a>
                <?php compruebaOpcionEliminarAlumnos(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		
		<?php } ?>

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
			if(isset($_POST['esAlumno'])){
				if($res){
				  mensajeOk("Alumno actualizado correctamente."); 
				}
				else{
				  mensajeError("no se ha podido actualizar el alumno. Compruebe los datos introducidos."); 
				} 
			}else{
				if($res){
				  mensajeOk("Curso actualizado correctamente."); 
				}
				else{
				  mensajeError("no se han podido actualizar el curso. Compruebe los datos introducidos."); 
				}
			}
          }
          elseif(isset($_POST['accionFormativa'])){
            if($res){
              mensajeOk("Curso registrado correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar el curso. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar. Contacte con el webmaster."); 
            }
          }
          elseif(isset($_POST['codigoCursoNotificacion'])){
            if($res){
              mensajeOk("Notificación establecida correctamente."); 
            }
            else{
              mensajeError("no se ha podido establecer la notificación. Compruebe los datos introducidos."); 
            } 
          }
		  elseif(isset($_POST['codigoCurso'])){
            if($res){
              mensajeOk("Costes actualizados correctamente."); 
            }
            else{
              mensajeError("no se ha podido actualizar los costes. Compruebe los datos introducidos."); 
            } 
          }
        ?>

        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Alumnos registrados en cursos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable" id="tablaAlumnos">
                <thead>
                  <tr>
                  <th> Acción formativa </th>
                  <th> Curso </th>
          <th> Empresa </th>
          <th> Alumno </th>
          <th> Fecha inicio </th>
          <th> Fecha fin </th>
          <th> Teléfono </th>
          <th> Llamada de inicio </th>
          <th> Resolución </th>
          <th> Plataforma </th>
          <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                    <!--th> Nombre </th>
                    <th> Teléfono </th>
                    <th> eMail </th>
                    <th> Cliente </th>
                    <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th-->
                  </tr>
                </thead>
                <tbody>

                  <?php
                    imprimeAlumnosNuevo();
                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Cursos registrados</h3>
			  <div class="pull-right">
               <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
             </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php filtroCursos(); ?>
              <table class="table table-striped table-bordered datatable" id="tablaCursos">
                <thead>
                  <tr>
					<th> Num. Acción </th>
					<th> Grupo </th>
                    <th> Acción formativa </th>
                    <th> Tutor </th>
                    <th> Fecha de inicio </th>
                    <th> Fecha de finalización </th>
					<th> Formación </th>
                    <th class="centro"> <a href="generaExcelCursosRegistrados.php" class="btn btn-propio noAjax" target="_blank"><i class=" icon-download-alt"></i><span class="shortcut-label">Descargar excel</span> </a> </th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					//imprimeCursos();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		 
      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<script type="text/javascript" src="js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  <?php
	echo "var marcado='".$empresa."';";
  ?>
  
	$(document).ready(function() {
    $('.selectpicker').selectpicker();
		$('.cajaFiltros select').selectpicker();
		oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaCursos');
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');realizaBusquedaFiltrada('#cajaFiltros','#tablaCursos');});
    $('.resolucion').change(function(){
      var resolucion = $(this).val();
      var codigo = $(this).attr('codigo');
      var codigoCurso = $(this).attr('codigoCurso');
      actualizaResolucion(resolucion, codigo,codigoCurso);
    });

    $('.marcarFecha').click(function(){
      var d = new Date();
      var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getYear()+' '+d.getHours()+':'+d.getMinutes();
      $(this).html(fecha);
      $(this).removeClass();
      $(this).addClass('llamadaHecha');
      var codigo = $(this).attr('codigo');
      var codigoCurso = $(this).attr('codigoCurso');
      actualizaFecha(codigo,codigoCurso);
    });
	});

  function actualizaFecha(codigo, codigoCurso){
    $.post('gestionesAjax.php?funcion=actualizaFechaFormacion();',{'codigo':codigo,'codigoCurso':codigoCurso});
  }
  
  function actualizaResolucion(resolucion, codigo, codigoCurso){
    $.post('gestionesAjax.php?funcion=actualizaResolucion();',{'codigo':codigo,'codigoCurso':codigoCurso,'resolucion':resolucion});
  }

  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un registro.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('formacion.php',valoresChecks,'post');
    }

  });
  
   $('#eliminarAlumno').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un registro.');
    }
    else{
      valoresChecks['eliminaAlumno']='SI';
      creaFormulario('formacion.php',valoresChecks,'post');
    }

  });

  
  $('#email').click(function(){
      var valoresChecks=recorreChecks();
      creaFormulario('enviarEmail.php?seccion=5',valoresChecks,'post');
    });
	
	var tabla=$('#tablaCursos').DataTable({
	  'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoCursos.php?action=getMembersAjx',
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          $('td:eq(7)', nRow).addClass( "centro" );
       },
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });
	
  var tabla=$('#tablaAlumnos').DataTable({
      "sDom": "<'row-fluid arriba'<'span6'l><'span6'f>r>t<'row-fluid abajo'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"bStateSave":true,
		"iDisplayLength":25,
		"oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
		  'sZeroRecords':'No se han encontrado coincidencias'
    },
    "fnDrawCallback": function (oSettings) {
      $('.selectpicker').selectpicker();
      $('.resolucion').change(function(){
        var resolucion = $(this).val();
        var codigo = $(this).attr('codigo');
        var codigoCurso = $(this).attr('codigoCurso');
        actualizaResolucion(resolucion, codigo,codigoCurso);
      });
      $('.marcarFecha').click(function(){
        var d = new Date();
        var fecha = d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getYear()+' '+d.getHours()+':'+d.getMinutes();
        $(this).html(fecha);
        $(this).removeClass();
        $(this).addClass('llamadaHecha');
        var codigo = $(this).attr('codigo');
        var codigoCurso = $(this).attr('codigoCurso');
        actualizaFecha(codigo,codigoCurso);
      });
    }});

    var tabla=$('#tablaAlumnos').dataTable();
    tabla.fnFilter(marcado);
	

</script>
