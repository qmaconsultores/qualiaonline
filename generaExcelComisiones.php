<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos/plantillaFacturaFirma.xlsx");
	
	conexionBD();
	$tiempo=time();
	$i=1;
	$j=4;
	$costeTotal=0;
	
	//COMIENZO DE TODOS LAS FACTURAS COMPRENDIDAS
	conexionBD();
	
	$datosFormulario=arrayFormulario();
	
	$consulta=consultaBD("SELECT facturacion.codigo, facturacion.referencia, productos.nombreProducto AS concepto, coste, empresa, fechaEmision, fechaVencimiento, numCuenta, clientes.referencia AS refCliente, clientes.cif AS cifCliente, clientes.empresa, clientes.direccion, clientes.cp, clientes.localidad, clientes.empresa, clientes.provincia, clientes.bic, facturacion.firma, usuarios.nombre, usuarios.apellidos, facturacion.codigoCliente, facturacion.cobrada, facturacion.codigoVenta, facturacion.formaPago FROM facturacion INNER JOIN facturas_vto ON facturacion.codigo=facturas_vto.codigoFactura  
		LEFT JOIN ventas ON facturacion.codigoVenta=ventas.codigo
		LEFT JOIN clientes ON ventas.codigoCliente=clientes.codigo
		LEFT JOIN productos ON facturacion.concepto=productos.codigo
		LEFT JOIN usuarios ON usuarios.codigo=ventas.codigoUsuario WHERE firma='".$datosFormulario['firma']."' AND facturacion.fechaEmision >='".$datosFormulario['fechaUno']."' AND facturacion.fechaEmision <='".$datosFormulario['fechaDos']."' AND (facturacion.devuelta='NO' OR (facturacion.devuelta='SI' AND facturacion.resolucionFactura!='Anulada')) GROUP BY facturacion.codigo;");
		$datos=mysql_fetch_assoc($consulta);
		
	$concepto=array('14'=>'Formación', '22'=>'Protección de datos', 'blanqueo'=>'Blanqueo de capitales');
	$formaPago=array("transferencia"=>"Transferencia", "efectivo"=>"Efectivo", "cheque"=>"Cheque", "domiciliacion"=>"Domiciliación bancaria", "tarjeta"=>"Tarjeta");
	while(isset($datos['codigo'])){		
		
		$datos['coste']=number_format((float)$datos['coste'], 2, ',', '');
		
		$objPHPExcel->getActiveSheet()->getCell('B'.$j)->setValue($datos['refCliente']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$j)->setValue($datos['referencia']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$j)->setValue($datos['empresa']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$j)->setValue($datos['concepto']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$j)->setValue(formateaFechaWeb($datos['fechaEmision']));
		$objPHPExcel->getActiveSheet()->getCell('G'.$j)->setValue(formateaFechaWeb($datos['fechaVencimiento']));
		$objPHPExcel->getActiveSheet()->getCell('H'.$j)->setValue($datos['coste'].' €');
		$objPHPExcel->getActiveSheet()->getCell('I'.$j)->setValue($datos['nombre'].' '.$datos['apellidos']);
		$objPHPExcel->getActiveSheet()->getCell('J'.$j)->setValue($datos['cobrada']);
		$objPHPExcel->getActiveSheet()->getCell('K'.$j)->setValue($datos['cifCliente']);
		$objPHPExcel->getActiveSheet()->getCell('L'.$j)->setValue($datos['provincia']);
		$objPHPExcel->getActiveSheet()->getCell('M'.$j)->setValue($datos['localidad']);
		$objPHPExcel->getActiveSheet()->getCell('N'.$j)->setValue($datos['direccion']);
		$objPHPExcel->getActiveSheet()->getCell('O'.$j)->setValue($datos['cp']);
		$objPHPExcel->getActiveSheet()->getCell('P'.$j)->setValue($formaPago[$datos['formaPago']]);
		
		if(!isset($datos['refCliente'])){
			$datosCliente=datosRegistro('clientes',$datos['codigoCliente']);
			$consultaAux=consultaBD("SELECT usuarios.nombre, usuarios.apellidos FROM ventas LEFT JOIN usuarios ON ventas.codigoUsuario=usuarios.codigo WHERE ventas.codigoCliente='".$datos['codigoCliente']."' AND (ventas.precio='".$datos['coste']."' OR ventas.precio='-".$datos['coste']."');",true);

			$datosComercial=mysql_fetch_assoc($consultaAux);
			$objPHPExcel->getActiveSheet()->getCell('B'.$j)->setValue($datosCliente['referencia']);
			$objPHPExcel->getActiveSheet()->getCell('D'.$j)->setValue($datosCliente['empresa']);
			$objPHPExcel->getActiveSheet()->getCell('I'.$j)->setValue($datosComercial['nombre'].' '.$datosComercial['apellidos']);
			$objPHPExcel->getActiveSheet()->getCell('K'.$j)->setValue($datosCliente['cif']);
			$objPHPExcel->getActiveSheet()->getCell('L'.$j)->setValue($datosCliente['provincia']);
			$objPHPExcel->getActiveSheet()->getCell('M'.$j)->setValue($datosCliente['localidad']);
			$objPHPExcel->getActiveSheet()->getCell('N'.$j)->setValue($datosCliente['direccion']);
			$objPHPExcel->getActiveSheet()->getCell('O'.$j)->setValue($datosCliente['cp']);
		}		
		
		$datos=mysql_fetch_assoc($consulta);
		$i++;
		$j++;
	}
	//cierraBD();

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	
	$objWriter->save('documentos/facturas.xlsx');

	/*
	// Definir headers
	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=Facturacion-".$tiempo.".zip");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/Facturacion-'.$tiempo.'.zip');*/

	
	
	// Definir headers
	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=facturas.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/facturas.xlsx');
?>