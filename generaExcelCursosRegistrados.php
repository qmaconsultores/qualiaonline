<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('phpexcel/PHPExcel.php');
	require_once('phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos2/plantillaExcelCursosRegistrados.xlsx");
	
	conexionBD();
	$i=7;
	
	$datosFormulario=arrayFormulario();
	
	$consulta=consultaBD($_SESSION['consultaCursos']);
		
		
	$estado=array('NO'=>"No tiene",'HECHO'=>"No hecho",'PROCESO'=>"En proceso",'FINALIZADO'=>"Finalizado");

	while($datos=mysql_fetch_assoc($consulta)){		
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($datos['codigoAccion']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($datos['grupo']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($datos['denominacion']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($datos['apellidos'].", ".$datos['nombre']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue(formateaFechaWeb($datos['fechaInicio']));
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue(formateaFechaWeb($datos['fechaFin']));
		$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue($estado[$datos['formacionCurso']]);
		$i++;
	}
	cierraBD();

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	
	$objWriter->save('documentos2/cursos_registrados.xlsx');

	/*
	// Definir headers
	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=Facturacion-".$tiempo.".zip");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos/Facturacion-'.$tiempo.'.zip');*/

	
	
	// Definir headers
	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=cursos_registrados.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('documentos2/cursos_registrados.xlsx');
?>