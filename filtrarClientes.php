<?php
  $seccionActiva=$_GET['seccion'];
  include_once("cabecera.php");
  if($_GET['posibles']=='SI'){
	$destino="posiblesClientesFiltrado.php";
  }elseif($_GET['posibles']=='PROPIO'){
	$destino="posiblesClientesPropiosFiltrado.php";
  }else{
	$destino="cuentasFiltrado.php";
  }
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-group"></i><i class="icon-chevron-right"></i><i class="icon-filter"></i>
              <h3>Filtrado de clientes</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
				<form id='edit-profile' class='form-horizontal' action='<?php echo $destino; ?>' method='post'>
					<fieldset class='span5'>
						<?php
							campoSelectConsultaNulo('comercial','Comercial',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE (tipo='COMERCIAL' AND activoUsuario='SI') OR codigo=19 OR codigo=21 OR 120 ORDER BY nombre, apellidos;");
							campoSelectConsultaNulo('servicio','Servicio',"SELECT codigo, nombreProducto AS texto FROM productos ORDER BY codigoProducto;");
							campoTexto('poblacion','Población');
							campoTexto('provincia','Provincia');
							campoSelect('estado','Estado',array('Todos','Sin contactar','En proceso','Reconcertar','Cerrada','Pendiente','Baja'),array('TODOS','Sin contactar','En proceso','Reconcertar','Cerrada','Pendiente','Baja'));
							campoTexto('cp','CP','','input-small');
							campoSelect('compra','¿Ha hecho alguna compra?',array('','Si','No'),array('NULL','SI','NO'));
						?>
					</fieldset>
					<fieldset class='span3'>
						<?php
							campoSelectConsultaNulo('colaborador','Colaborador',"SELECT codigo, empresa AS texto FROM colaboradores ORDER BY empresa;");
							campoActividades();
							campoRadio('comprado','¿Compró este año?','NO');
							campoRadio('tieneTrabajadores','¿Tiene trabajadores?','NO');
							campoRadio('tieneWeb','¿Tiene página web?','NO');
							campoRadio('baja','¿Dado de baja?','NO');
							echo '<div class="motivoBaja" style="display:none;">';
							campoSelect('motivoBaja','Motivo de baja',array('','Cierre empresa','Descontento','Alta otra empresa','Fin de contrato'),array('NULL','Cierre empresa','Descontento','Alta otra empresa','Fin de contrato'));
							echo '</div>';
							if($_GET['posibles']=='SI'){
								campoOculto('','fechaUno');
								campoOculto('','fechaDos');
							}else{
								echo "<div class='control-group'>                     
								  <label class='control-label' for='fechas'>Fecha de venta del:</label>
									<div class='controls'>
										<input type='text' class='input-small hasDatepicker' id='fechaUno' name='fechaUno'> &nbsp;&nbsp; al: &nbsp;&nbsp;  <input type='text' class='input-small hasDatepicker' id='fechaDos' name='fechaDos'>
									</div> <!-- /controls -->       
								</div> <!-- /control-group -->";
							}
						?>
					</fieldset>
					<br>
					<fieldset class='sinFlotar'>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Seleccionar <i class="icon-circle-arrow-right"></i></button>
						</div>
					</fieldset>
				</form>				
				</div>
				
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$('.selectpicker').selectpicker();
	$('input[name=baja]').change(function(){
		if($(this).val() == 'SI'){
			$('.motivoBaja').css('display','block');
		} else {
			$('.motivoBaja').css('display','none');
		}
	})
  });
</script>