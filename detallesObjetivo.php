<?php
  $seccionActiva=10;
  include_once('cabecera.php');

  $datos=datosObjetivo($_GET['codigo']);
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Detalles de objetivo: número de ventas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="objetivos.php" method="post">
                  <fieldset>

                    <input type="hidden" name="codigo" value="<?php echo $datos['codigo']; ?>">
                    
                    <div class="control-group">                     
                      <label class="control-label" for="fechaInicio">Fecha de inicio:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaInicio" name="fechaInicio" value="<?php echo formateaFechaWeb($datos['fechaInicio']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="fechaFin">Fecha de fin:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaFin" name="fechaFin" value="<?php echo formateaFechaWeb($datos['fechaFin']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="valor">Valor objetivo:</label>
                      <div class="controls">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini pagination-right" id="valor" name="valor" value="<?php echo $datos['valor']; ?>">
                          <span class="add-on"> ventas</span>
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="importe">Importe objetivo:</label>
                      <div class="controls">
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini pagination-right" id="importe" name="importe" value="<?php echo $datos['importe']; ?>">
                          <span class="add-on"> €</span>
                        </div>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="estado">Comerciales:</label>
                      <div class="controls">
                         <?php selectComerciales($_GET['codigo']); ?>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualiza objetivo</button> 
                      <a href="objetivos.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
  });
</script>
