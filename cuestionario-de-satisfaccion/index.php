<?php
  $seccionActiva=0;
  include_once("cabecera.php");

  if(isset($_POST['pregunta1'])){
    $res=insertaDatos('satisfaccion');
    conexionBDLOPD();
      $actuaciones = consultaBD("SELECT * FROM actuaciones_cliente WHERE codigoCliente=".$_POST['cliente'],false,true);
      if($actuaciones){
        $res=consultaBD("UPDATE actuaciones_cliente SET satisfaccion_recibida = true WHERE codigoCliente='".$_POST['cliente']."';");
      } else {
        $res=consultaBD("INSERT INTO actuaciones_cliente VALUES(NULL,".$_POST['cliente'].",0,0,0,0,0,0,1,NULL,NULL,NULL,NULL);");
      }
    cierraBD();
  }
  if(isset($_GET['codigoCliente'])){
	$_SESSION['codigoClienteEncuesta']=$_GET['codigoCliente'];
  }
?>
<br><br>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">

        <?php
          if(isset($_POST['pregunta1'])){
            if($res){
              mensajeOk("Encuesta enviada. Gracias por su colaboración.");
            }
            else{
              mensajeError("no se han podido enviar la encuesta. Compruebe los datos introducidos.");
            }
          }
        ?>

        <div class="widget">
            
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="?codigoCliente=<?php echo $_SESSION['codigoClienteEncuesta']; ?>" method="post">
                  <fieldset>
					
        					  <p class="justificado parrafo-margenes" > <strong>Estimado Cliente.</strong> En Grupqualia Consultores nos preocupa mucho su satisfacción con los servicios

        						contratados con nuestra entidad y la calidad final de los proyectos ejecutados. Por ello, le agradecemos que pueda 

        						hacernos llegar la presente encuesta de satisfacción, indicando además en el apartado observaciones cualquier 

        						comentario que crea que pueda ayudarnos a mejorar la calidad de nuestros servicios o a ajustar los mismos a sus 

        						necesidades. Sin duda, creemos firmemente que en sus opiniones y sugerencias se encuentran nuestras mayores 

        						oportunidades de mejorar. Gracias por anticipado.<br>
        						</p>

                    <br />			

                    <?php
            if(isset($_GET['MyXCCgt'])){
              conexionBDLOPD();
                $cliente=consultaBD("SELECT * FROM clientes WHERE codigoCuestionario=".$_GET['MyXCCgt'],false,true);
                campoOculto($cliente['codigo'],'cliente');
              cierraBD();
            } else {
						  campoOculto($_SESSION['codigoClienteEncuesta'],'cliente');
            }
					?>
                    

                    <div class="control-group">                     
                      <label class="control-label" for="fecha">Fecha:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fecha" name="fecha" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group --> <br />

                    <br>
                    
                    <strong>En general, qué satisfacción tiene de los servicios de GRUPO QUALIA:</strong><br><br>

                  <center>
          					<?php 
          						creaTablaSatisfaccion(); 
          						creaPreguntaTablaEncuesta("Atención comercial:",1,10);
          						creaPreguntaTablaEncuesta("La atención del Formador:",2,10);
          						creaPreguntaTablaEncuesta("Profesionalidad del Formador:",3,10);
								cierraTablaSatisfaccion();
          					?>
			      </center>
				  
				  <br><br>
				  
					<strong>Valore su satisfaccion de los siguientes apartados:</strong><br><br>
					
					De los servicios contratados<br><br>

                  <center>
          					<?php 
          						creaTablaSatisfaccion(); 
          						creaPreguntaTablaEncuesta("Información y atención antes de ser cliente:",4,10);
          						creaPreguntaTablaEncuesta("Información sobre el servicio contratado:",5,10);
          						creaPreguntaTablaEncuesta("Información sobre el procedimiento de cobro:",6,10);
								creaPreguntaTablaEncuesta("Tiempo de entrega del servicio contratado:",7,10);
								creaPreguntaTablaEncuesta("Implantación del servicio en su empresa:",8,10);
								creaPreguntaTablaEncuesta("Resolución de incidencias desde que es cliente:",9,10);
								cierraTablaSatisfaccion();
          					?>
			      </center>
				  
				  <br>
				    De los cursos de formación realizados<br><br>

                  <center>
          					<?php 
          						creaTablaSatisfaccion(); 
          						creaPreguntaTablaEncuesta("Ha obtenido la formación que usted esperaba:",10,10);
          						creaPreguntaTablaEncuesta("Funcionalidad de la plataforma de formación:",11,10);
          						creaPreguntaTablaEncuesta("Pedagogía y contenidos del curso:",12,10);
								creaPreguntaTablaEncuesta("Tiempo asignado para su realización:",13,10);
								creaPreguntaTablaEncuesta("Tiempo de espera del diploma y del informe de bonificación:",14,10);
          					?>
				  
          						<td colspan="2">Le agradecemos que nos transmita sus comentarios o sugerencias:</td>	
          						<tr>
          							<td colspan="2"><textarea rows="10" class="textarea-amplia" name="comentarios" id="comentarios"></textarea></td>	
          						</tr>
          					<?php 
          						cierraTablaSatisfaccion();
          					?>
					        </center>
							
							<p class="justificado parrafo-margenes">
								En cumplimineto de la Ley Orgánica 15/1999, de 13 de Diciembre, de Protección de Datos de Carácter Personal, le informamos que  sus datos forman parte de un fichero titularidad de <strong>GRUP QUALIA ASOCIADOS SL.</strong> Usted puede ejercer los derechos de acceso, rectificación, cancelación y oposición en <strong>Cl. Pablo Iglesias 58 1º.. 08302 MATARÓ (BARCELONA)</strong> o bien mediante el correo electrònico : <strong>info@grupqualia.es</strong> 
							</p>

                     <br />
                    
                      
                    <div class="form-actions">
                      <button type="button" class="btn btn-primary" id="enviar"><i class="icon-share"></i> Enviar cuestionario</button> 
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="../js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('#fecha').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$('#enviar').click(function(){
		if($('#cliente').val()!='' && $('#nombreAlumno').val()!=''){
			$('form').submit();
		}else{
			alert("Debe rellenar todos los campos para enviar la encuesta");
		}
	});
  });

</script>


