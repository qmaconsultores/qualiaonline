<?php
  session_start();// Es necesario ponerlo ANTES DE IMPRIMIR CONTENIDO EN EL NAVEGADOR.
  include_once("../funciones.php");


  $titulos=array('Cuestionario de Satisfacción de Clientes');
?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<title><?php echo $titulos[$seccionActiva]; ?> - GRUPQUALIA</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/bootstrap-responsive.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
<link href="../css/font-awesome.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/pages/dashboard.css" rel="stylesheet">

<link href="../css/datepicker.css" rel="stylesheet">
<link href="../css/bootstrap-select.css" rel="stylesheet">

<?php
if(isset($planificacion)){
  echo '<link href="../css/bootstrap-switch.min.css" rel="stylesheet">';
}
?>

<!-- Script HTML5, para el soporte del mismo en IE6-8 -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<meta name="google-translate-customization" content="4fcce03dad044864-7521a80c17218173-g403e83fe756dea77-1a"></meta>
</head>
<body>
<div class="navbar navbar-fixed-top cabeceraCuestionario">
  <div class="navbar-inner">
    <div class="container"> 
      
      <span class="logo2">
        <img src="../img/logo2.png" alt="GRUPQUALIA"> - Cuestionario de Satisfacción de Clientes
      </span>

    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  
</div>
<!-- /subnavbar -->