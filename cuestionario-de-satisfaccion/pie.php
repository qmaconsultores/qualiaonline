<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
         <div class="span12"> &copy; 2014 <a href="http://www.grupqualia.es/es/inicio" target="_blank">Grupqualia</a>.
          Software desarrollado por <a href="http://www.qmaconsultores.com" target="_blank">Q&amp;MA Consultores</a>.</div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 

<!-- Javascript
================================================== -->
<script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>

<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/base.js" type="text/javascript"></script>
<script src="../js/bootstrap-datepicker.js" type="text/javascript"></script>

</body>
</html>
