<?php
  $seccionActiva=17;
  include_once('cabecera.php');
  
  $codigoColaborador=$_GET['codigo'];
  $datosColaborador=datosRegistro('colaboradores',$codigoColaborador);
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

	   <div class="span12">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Volver a colaboradores</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				  <a href="colaboradores.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
	  
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Cuentas registradas para el colaborador <a href='detallesColaborador.php?codigo=<?php echo $codigoColaborador; ?>'><?php echo $datosColaborador['empresa']; ?></a></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
				 <table class='table table-striped table-bordered datatable' id='tablaCuentas'>
					<thead>
					  <tr>
						<th> ID </th>
						<th> Nombre del cliente </th>
						<th> CIF </th>
						<th> Población </th>
						<th> Comercial asignado </th>
						<th> Sector </th>
						<th> Servicio Contratado </th>
						<th> Fecha ult. venta </th>
						<th class='td-actions'> </th>
					  </tr>
					</thead>
					<tbody>
						<?php //imprimeCuentas(); ?>
					</tbody>
              </table>
                
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<!--script src="js/filtroTabla.js"></script-->
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script src="js/excanvas.min.js" type="text/javascript"></script>
<script src="js/chart.min.js" type="text/javascript"></script>
<script type="text/javascript">
  <?php
    $datos=generaDatosGraficoCuentas();
  ?>
  $('#tablaCuentas').dataTable({
      'bProcessing': true, 
	  'bServerSide': true, 
	  'sAjaxSource': 'listadosajax/listadoCuentasColaborador.php?action=getMembersAjx&codigoColaborador=<?php echo $codigoColaborador;?>',
	  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          $('td:eq(8)', nRow).addClass( "centro" );
       },
	   "iDisplayLength":25,
	  "oLanguage": {
		  "sLengthMenu": "_MENU_ registros por página",
		  "sSearch":"Búsqueda:",
		  "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
		  "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
		  "sEmptyTable":"Aún no hay datos que mostrar",
		  "sInfoEmpty":"",
		  'sInfoFiltered':"",
		  'sZeroRecords':'No se han encontrado coincidencias',
		  'sProcessing':'Procesando...'
		}
    });
  var pieData = [
      {
          value: <?php echo $datos['totales']; ?>,
          color: "#B02B2C"
      },
      {
          value: <?php echo $datos['confirmados']; ?>,
          color: "#428bca"
      }
    ];

  var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
  
  $("#valor1").text("<?php echo $datos['totales']; ?>");
  $("#valor2").text("<?php echo $datos['confirmados']; ?>");

  $('#email').click(function(){
    var valoresChecks=recorreChecks();
    creaFormulario('enviarEmail.php?seccion=2',valoresChecks,'post');
  });


  $('#tareas').click(function(){
      var valoresChecks=recorreChecks();
      creaFormulario('creaTarea.php',valoresChecks,'post');
  });


  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('cuentas.php',valoresChecks,'post');
    }

  });
</script>
