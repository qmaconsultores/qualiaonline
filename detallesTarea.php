<?php
  $seccionActiva=3;
  include_once('cabecera.php');

  $datos=datosTarea($_GET['codigo']);
  $codigo=$_GET['codigo'];
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Datos de la tarea</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="" method="post">
                  <fieldset>

                    <input type="hidden" name="codigo" value="<?php echo $datos['codigo']; ?>">
                    
                    <?php
                        $preventa=datosRegistro('preventas',$datos['codigo'],'codigoTarea');
                        if($preventa && $preventa['aceptado']=='NO'){
                          echo '<span style="color:red;margin-left:70px;">* La preventa ha sido rechazada por el siguiente motivo: <b>'.$preventa['observaciones'].'</b></span><br/><br/>';
                        }
                        campoSelect('descripcion','Descripción de tarea',array('Primera visita','Visita seguimiento','Auditoría LOPD','Auditoría PRL','Visita formación','Llamada Seguimiento','Mail'),array('PRIMERA','SEGUIMIENTO','LOPD','PRL','FORMACION','LLAMADA','MAIL'),$datos);
                      ?>

                    <div class="control-group">                     
                      <label class="control-label" for="tarea">Tarea:</label>
                      <div class="controls">
                        <input type="text" class="span6" id="tarea" name="tarea" value="<?php echo $datos['tarea']; ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
                    
                    <div class="control-group">                     
                      <a href='detallesCuenta.php?codigo=<?php echo $datos['codigoCliente']; ?>'><label class="control-label" for="empresa">Cliente:</label></a>
                      <div class="controls">
                          <a href='detallesCuenta.php?codigo=<?php echo $datos['codigoCliente']; ?>'><input type="text" class="input-large" id="empresa" name="empresa" value="<?php echo $datos['empresa']; ?>" disabled="disabled"></a>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoTexto('sector','Sector',$datos['sector'],'input-large',true);
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="direccion">Dirección:</label>
                      <div class="controls">
						<?php 
							if($datos['donde']=='empresa'){
						?>
								<input type="text" class="input-large" id="direccion" name="direccion" value="<?php echo $datos['direccion']; ?>" disabled="disabled">
						<?php 
							}else{	
						?>
								<input type="text" class="input-large" id="direccion" name="direccion" value="<?php echo $datos['direccionVisita']; ?>" disabled="disabled">	
						<?php } ?>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="poblacion">Población:</label>
                      <div class="controls">
                          <input type="text" class="input-large" id="poblacion" name="poblacion" value="<?php echo $datos['localidad']; ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="colaborador">Colaborador:</label>
                      <div class="controls">
                          <input type="text" class="input-large" id="colaborador" name="colaborador" value="<?php echo $datos['colaborador']; ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="contacto">Contacto:</label>
                      <div class="controls">
                          <input type="text" class="input-large" id="contacto" name="contacto" value="<?php echo $datos['contacto']; ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono" value="<?php echo formateaTelefono($datos['telefono']); ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="movil">Móvil:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="movil" name="movil" value="<?php echo formateaTelefono($datos['movil']); ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="mail">eMail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="mail" name="mail" value="<?php echo $datos['mail']; ?>" disabled="disabled">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



                    <div class="control-group">                     
                      <label class="control-label" for="fechaInicio">Fecha de inicio:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaInicio" name="fechaInicio"  value="<?php echo formateaFechaWeb($datos['fechaInicio']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fechaFin">Fecha de fin:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaFin" name="fechaFin" value="<?php echo formateaFechaWeb($datos['fechaFin']); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="horaInicio">Hora de inicio:</label>
                      <div class="controls">
                        <input type="text" class="input-mini timepicker" id="horaInicio" name="horaInicio" data-minute-step='1' <?php if($datos['todoDia']=='NO'){ echo 'value="'.formateaHoraWeb($datos['horaInicio']).'"'; } ?> >
                        &nbsp;
                        <label class="checkbox inline">
                          <input type="checkbox" name="todoDia" id="todoDia" value="SI" <?php if($datos['todoDia']=='SI'){ echo 'checked="checked"'; } ?> > Todo el día
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="horaFin">Hora de fin:</label>
                      <div class="controls">
                        <input type="text" class="input-mini timepicker" id="horaFin" name="horaFin" data-minute-step='1' <?php if($datos['todoDia']=='NO'){ echo 'value="'.formateaHoraWeb($datos['horaFin']).'"'; } ?> >
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

          <?php 
          $preventa=datosRegistro('preventas',$datos['codigo'],'codigoTarea');
          if($preventa['aceptado']!='SI'){
          ?>
                    <div class="control-group">                     
                      <label class="control-label" for="estado">Estado:</label>
                      <div class="controls">
                        
                        <label class="radio inline">
                          <input type="radio" name="estado" value="pendiente" <?php if($datos['estado']=='pendiente'){ echo 'checked="checked"'; } ?> > Pendiente
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="estado" value="realizada" <?php if($datos['estado']=='realizada'){ echo 'checked="checked"'; } ?> > Realizada
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php } 
          if($datos['estado']=='realizada'){ ?>
					
					<div class="control-group">                     
					  <label class="control-label" for="horaRealizacion">Hora de realización:</label>
					  <div class="controls">
						<input type="text" class="input-mini" id="horaRealizacion" name="horaRealizacion" <?php if($datos['estado']=='realizada'){ echo 'value="'.formateaHoraWeb($datos['horaRealizacion']).'"'; } ?> disabled='disabled'>
					  </div> <!-- /controls -->       
					</div> <!-- /control-group -->

					<?php }
          if($preventa['aceptado']=='SI'){
            campoOculto('ALTA','prioridad');
          } else {
           ?>

                    <div class="control-group">                     
                      <label class="control-label" for="prioridad">Resolución:</label>
                      <div class="controls">
                        
                        <select name="prioridad" class='selectpicker show-tick' id='prioridad'>
			
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php
          }
            campoOculto($datos['prioridad'],'textoPrioridad');
						echo "<div id='pendienteOculto'>";
							campoRadio('firmado','Firmado',$datos);
							campoRadio('pendiente','Pendiente de','Datos',array('Datos','Formación'),array('Datos','Formacion'),$datos);
						echo "</div>";
						echo "<div id='vendidoOculto'>";
						
              if($preventa){
                if($preventa['aceptado']=='NO'){
                  echo '<button id="vuelvePreventa" class="btn btn-success" style="margin-left:220px;margin-bottom:20px;">Volver a enviar la preventa</a>';
                  campoOculto($preventa['codigo'],'codigoPreventa');
                } else {
                  echo '<a href="detallesPreventa.php?codigo='.$preventa['codigo'].'" class="btn btn-success"   style="margin-left:220px;margin-bottom:20px;">Ver preventa</a>';
                }
              } else {
                /*echo '<button id="creaPreventa" class="btn btn-success" style="margin-left:220px;margin-bottom:20px;">Crear preventa</a>';*/
                if($datos['importe']!=''){
                  campoTextoSimbolo('importe','Importe','€',$datos,'input-mini pagination-right',true);
                } else {
                  campoOculto($datos,'importe');
                }
              }
						echo "</div>";
						if($_SESSION['tipoUsuario']=='ADMIN'||$_SESSION['tipoUsuario']=='ADMINISTRACION'||$_SESSION['tipoUsuario']=='TELECONCERTADOR'){
							$where='WHERE 1=1';
							if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
								$codigoU=$_SESSION['codigoS'];
								$where="WHERE (codigo='$codigoU' OR codigo IN(SELECT codigoUsuario FROM usuarios_teleconcertadores WHERE codigoTeleconcertador='$codigoU'))";
							}
							$consulta="SELECT codigo, CONCAT(nombre, ' ', apellidos) AS texto FROM usuarios $where AND activoUsuario='SI';";
							campoSelectConsulta('codigoUsuario','Usuario asignado',$consulta,$datos['codigoUsuario']);
						}else{
							campoOculto($_SESSION['codigoS'],'codigoUsuario');
						}
					?>

                    <div class="control-group">                     
                      <label class="control-label" for="observaciones">Observaciones:</label>
                      <div class="controls">
                        <textarea name="observaciones" class="areaInforme" id="observaciones"></textarea>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						$consulta=consultaBD("SELECT * FROM historico_tareas WHERE codigoTarea='$codigo' ORDER BY fecha DESC, hora DESC;",true);
						$datosHistorico=mysql_fetch_assoc($consulta);
						$historico='';
						if(isset($datosHistorico['codigo'])){
							while(isset($datosHistorico['codigo'])){
								$historico=$historico.formateaFechaWeb($datosHistorico['fecha'])." ".$datosHistorico['hora']." ".$datosHistorico['observaciones']." \n\n";
								$datosHistorico=mysql_fetch_assoc($consulta);
							}
						}
						areaTexto('historicoObservaciones','Histórico Observaciones',$historico,'areaInforme',true);
					?>


                    <div class="form-actions">
                      <button type="button" class="btn btn-primary" id="actualizar"><i class="icon-refresh"></i> Actualizar tarea</button> 
					  <button type="button" class="btn btn-success" id="programar"><i class="icon-time"></i> Programar nueva</button>
                      <a href="tareas.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script src="js/jquery.inputmask.js" type="text/javascript"></script>
<script type="text/javascript" src="js/bootstrap-timepicker.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$('.timepicker').timepicker({showMeridian: false, defaultTime: false});	
	$('.hasDatepicker').inputmask({"mask": "99/99/9999"});	
	$('.timepicker').inputmask({"mask": "99:99"});
	$("#fechaInicio").on("changeDate", function() { $('#fechaFin').val($(this).val()); $(this).datepicker("hide"); });
	$("#fechaFin").on("changeDate", function() { $(this).datepicker("hide"); });
    <?php
      if($datos['todoDia']=='SI'){
        echo "
          $('#horaInicio').prop('disabled', true);
          $('#horaFin').prop('disabled', true);";
      }
	  if($datos['estado']=='realizada'){
        echo "
          $('#oculto').css({display: 'block'});";
      }else{
		echo "
          $('#oculto').css({display: 'none'});";
	  }
    ?>
  cambiarSelect($('input[name=estado]:checked').val());
  $('input[name=estado]').change(function(){
    cambiarSelect($(this).val());
    modificaBoton($('#prioridad').val());
  });

    $('#todoDia').change(function(){
      
      if($(this).prop('checked')){
        $('#horaInicio').prop('disabled', true);
        $('#horaFin').prop('disabled', true);
      }
      else{
        $('#horaInicio').prop('disabled', false);
        $('#horaFin').prop('disabled', false);
      }

    });
	$('#horaInicio').blur(function(){
      
      var hora=$('#horaInicio').val().substr(0,2);
	  var minutos=$('#horaInicio').val().slice(3);
	  var minutosSumados=parseFloat(minutos)+15;
	  if(minutosSumados<10){
		minutosSumados='0'+minutosSumados;
	  }
	  $('#horaFin').val(hora+minutosSumados);
	  
    });
	$('#actualizar').click(function(){
			actualizar();
  });
	$('#programar').click(function(){
		programar();
    });
	
	<?php if($datos['prioridad']!='PENDIENTE'){
		echo "$('#pendienteOculto').css('display','none');";
	}
	if($datos['prioridad']!='ALTA'){
		echo "$('#vendidoOculto').css('display','none');";
	}?>	
  
  mostrarDivs($('#prioridad').val());
  modificaBoton($('#prioridad').val());
  $('#prioridad').change(function(){
    var val=$(this).val();
    mostrarDivs(val);
    modificaBoton(val);
  });

  $('button#creaPreventa').click(function(e){
    creaPreventa();
  })

  $('#vuelvePreventa').click(function(e){
    e.preventDefault();
    if($('#observaciones').val()!=''){
      $('form').attr('action','detallesPreventa.php').submit();
    } else {
      alert("El campo observaciones debe ser rellenado");
    }
  })
});
   function programar(){		
		$('form').attr('action', "programaTarea.php").submit();
	}
	
	function actualizar(){		
    if($('#observaciones').val()!=''){
      $('form').attr('action', "tareas.php").submit();
    }else{
      alert("El campo observaciones debe ser rellenado");
    }
	}

  function creaPreventa(){    
    if($('#observaciones').val()!=''){
      $('form').attr('action','creaPreventa.php').submit();
    } else {
      alert("El campo observaciones debe ser rellenado");
    }
  }

  function cambiarSelect(val){
  var select='';
  $("#prioridad option").remove();
  if(val == 'realizada'){
    $("#prioridad").append("<option value='ALTA' data-content='<span class=\"label label-success\">Venta</span>'></option>");
    $("#prioridad").append("<option value='VISITADO' data-content='<span class=\"label label-azul-claro\">Visitada</span>'></option>"); 
    $("#prioridad").append("<option value='BAJA' data-content='<span class=\"label label-danger\">Baja</span>'></option>");
  } else {
    $("#prioridad").append("<option value='PENDIENTE' data-content='<span class=\"label\">Pendiente</span>'></option>");
    $("#prioridad").append("<option value='PROCESO' data-content='<span class=\"label label-primary\">En proceso</span>'></option>");
    $("#prioridad").append("<option value='RECONCERTAR' data-content='<span class=\"label label-inverse\">Reconcertar</span>'></option>");
    $("#prioridad").append("<option value='CONCERTADA' data-content='<span class=\"label label-verde-claro\">Concertada</span>'></option>"); 
  }
  $("#prioridad option[value="+ $('#textoPrioridad').val().toUpperCase() +"]").attr("selected",true);
  $('#prioridad').selectpicker('refresh');
  mostrarDivs($('#prioridad').val());
}

function mostrarDivs(val){
  if(val=='PENDIENTE'){
      $('#pendienteOculto').show(300);
    }else{
      $('#pendienteOculto').hide(300);
    }
    if(val=='ALTA'){
      $('#vendidoOculto').show(300);
    }else{
      $('#vendidoOculto').hide(300);
    }
}

function modificaBoton(val){
  if(val=='ALTA' && $("#codigoPreventa").length == 0){
      $('button#actualizar').html('<i class="icon-tags"></i> Crear preventa');
      $('button#actualizar').attr('id','creaPreventa');
      $('button#creaPreventa').unbind();
      $('button#creaPreventa').click(function(){
        creaPreventa();
      })
    } else {
      $('button#creaPreventa').html('<i class="icon-refresh"></i> Actualizar tarea');
      $('button#creaPreventa').attr('id','actualizar');
      $('#actualizar').unbind();
      $('#actualizar').click(function(){
        actualizar();
      });
    }
}
</script>
