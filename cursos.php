<?php
  $seccionActiva=5;
  include_once('cabecera.php');
  
  if(isset($_POST['codigo'])){
	if(!isset($_POST['tutorDistancia'])){
		$_POST['tutorDistancia']='NULL';
	}
	if(!isset($_POST['titularidadDistancia'])){
		$_POST['titularidadDistancia']='';
	}
    $res=actualizaCurso();
  }
  elseif(isset($_POST['denominacion'])){
   $res=registraAccionFormativa();
  }
  elseif(isset($_POST['apellidos'])){
    $res=registraTutor();
  }
  elseif(isset($_POST['accionFormativa'])){
    $res=registraCurso();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaCurso();
  }
  elseif(isset($_POST['codigoCursoNotificacion'])){
    $res=registraNotificacion();
  }
  elseif(isset($_POST['codigoCurso'])){
	$res=actualizaCostes();
  }

  $estadisticas=creaEstadisticasCursos();
  
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para los cursos</h6>

                   <div id="big_stats" class="cf">

                     <div class="stat"> <i class="icon-book"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Cursos registrados</div>
                      <!-- .stat -->

                   </div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
		
        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de cursos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="accionesFormativas.php" class="shortcut"><i class="shortcut-icon icon-book"></i><span class="shortcut-label">Acciones Form.</span> </a>
                <a href="tutores.php" class="shortcut"><i class="shortcut-icon icon-user"></i><span class="shortcut-label">Tutores</span> </a>
                <a href="creaCurso.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Crear Curso</span> </a>
                <br>
                <?php 
                compruebaOpcionExcel('4');
        				?>
        				<a href="generaDocumentosXml.php" class="shortcut"><i class="shortcut-icon icon-download"></i><span class="shortcut-label">Ficheros XML</span> </a>
        				<?php
                compruebaOpcionEliminar();
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
        
        <?php
          if(isset($_POST['codigo'])){
            if($res){
              mensajeOk("Curso actualizado correctamente."); 
            }
            else{
              mensajeError("no se han podido actualizar el curso. Compruebe los datos introducidos."); 
            }
          }
          elseif(isset($_POST['denominacion'])){
  		      if($res){
              mensajeOk("Acción formativa registrada correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar la acción formativa. Compruebe los datos introducidos."); 
            }
		      }
          elseif(isset($_POST['apellidos'])){
            if($res){
              mensajeOk("Tutor registrado correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar el tutor. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['accionFormativa'])){
            if($res){
              mensajeOk("Curso registrado correctamente."); 
            }
            else{
              mensajeError("no se ha podido registrar el curso. Compruebe los datos introducidos."); 
            } 
          }
          elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
            if($res){
              mensajeOk("Eliminación realizada correctamente."); 
            }
            else{
              mensajeError("no se ha podido eliminar el posible cliente. Contacte con el webmaster."); 
            }
          }
          elseif(isset($_POST['codigoCursoNotificacion'])){
            if($res){
              mensajeOk("Notificación establecida correctamente."); 
            }
            else{
              mensajeError("no se ha podido establecer la notificación. Compruebe los datos introducidos."); 
            } 
          }
		  elseif(isset($_POST['codigoCurso'])){
            if($res){
              mensajeOk("Costes actualizados correctamente."); 
            }
            else{
              mensajeError("no se ha podido actualizar los costes. Compruebe los datos introducidos."); 
            } 
          }
        ?>
		
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Cursos registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Acción formativa </th>
                    <th> Tutor </th>
                    <th> Fecha de inicio </th>
                    <th> Fecha de finalización </th>
                    <th class="centro"> </th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeCursos();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>
<script type="text/javascript">


  $('#eliminar').click(function(){
    var valoresChecks=recorreChecks();
    if(valoresChecks['codigo0']==undefined){
      alert('Por favor, seleccione antes un cliente.');
    }
    else{
      valoresChecks['elimina']='SI';
      creaFormulario('cursos.php',valoresChecks,'post');
    }

  });

</script>
