<?php  
  $seccionActiva=7;
  include_once('cabecera.php');
  $_SESSION["verifica"]=1;
  $codigo=$_GET['codigo'];
  $datos=datosRegistro('remesas',$codigo);
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-download-alt"></i>
              <h3>Recibos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="" method="post" id="formulario">
                  <fieldset>
				  
					<?php

						campoOculto('1','descarga');
						campoOculto('','facturas');
						campoOculto($datos['tipoSepa'],'tipoSepa');
						campoOculto($datos['banco'],'banco');
					?>
					
					<div id="big_stats" class="cf">
						<div class="stat"> <i class="icon-euro"></i> <span id='tiulosFacturado' class="value"><?php echo number_format((float)$datos['importe'], 2, ',', '');?></span> <br>Total remesado</div>
					</div>

								  
                    
                    <div class="widget widget-table action-table">
						<div class="widget-header"> <i class="icon-th-list"></i>
						  <h3>Factura/s</h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
						  <table class="table table-striped table-bordered datatable">
							<thead>
							  <tr>
								<th> Referencia </th>
								<th> Cliente </th>
								<th> Concepto </th>
								<th> Fecha de emisión </th>
								<th> Fecha de vencimiento </th>
								<th> Importe </th>
								<th></th>
							  </tr>
							</thead>
							<tbody>

							  <?php
								
								
								imprimeFacturasVtoDescarga2($codigo);

							  ?>
							
							</tbody>
						  </table>
						</div>
						<!-- /widget-content --> 
					  </div>

                  
                    <div class="form-actions">
					  <button type="button" class="btn btn-primary" id="descargar"><i class="icon-download-alt"></i> Descargar</button> 
                      <a href="informeRemesas.php" class="btn"><i class="icon-remove"></i> Volver</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                  
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTablaFacturacion.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
	$('.mensajeAviso').hover(function(){
		var titulo=$(this).attr("titulo");
		var contenido=$(this).attr("mensaje");
		$(this).popover({
			 title: titulo,
			 content: contenido,
			 placement:'top'
		});
	});
	$('input[id!=todo]').change(function(){
		val=$(this).attr('checked');
		importe=parseFloat($(this).attr('importe'));
		total=parseFloat($('#importe').val());
		if(val == 'checked'){
			total=total+importe;
		} else {
			total=total-importe;
		}
		$('#titulosRemesado').html(total);
		$('#importe').val(total)
	});

	$('#todo').change(function(){
		val=$(this).attr('checked');
		importe=parseFloat($('#importeFacturado').val());
		if(val == 'checked'){
			total=importe;
		} else {
			total=0;
		}
		$('#titulosRemesado').html(total);
		$('#importe').val(total)
	});

	$('#descargar').click(function(){
		descarga();
    });
	$('#actualizar').click(function(){
		actualizar();
    });
  });
  function descarga(){		
		var valoresChecks=[];
		var i=0;
		$('input[name="codigoLista[]"]:checked').each(function() {
			valoresChecks['codigo'+i]=$(this).val();
			$('#facturas').val($('#facturas').val()+','+valoresChecks['codigo'+i]);
			i++;
		});
		$('form').attr('action', "generaDocumentoFacturas.php").submit();
	}
	
	function actualizar(){		
		$('form').attr('action', "facturacion.php").submit();
	}
  
</script>