<?php
  $seccionActiva=14;
  include_once("cabecera.php");
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-euro"></i><i class="icon-chevron-right"></i><i class="icon-download-alt"></i>
              <h3>Descarga de documentación de facturas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
				<form action='descargaFacturas.php' method='post'>
					
					Firma: <br />
					<?php 
						echo "<select name='firma' id='firma' class='selectpicker span3 show-tick'>";
	
							$consulta=consultaBD("SELECT codigo, firma AS texto FROM firmas ORDER BY codigo;",true);
							while($datos=mysql_fetch_assoc($consulta)){
								echo "<option value='".$datos['codigo']."'";

								echo ">".$datos['texto']."</option>";
							}
							
						echo "</select>";
					?>
					<br /><br />

					Vencimiento del: <input type='text' class='input-small datepicker hasDatepicker' id='fechaUno' name='fechaUno' value='<?php echo imprimeFecha(); ?>'> 
					al: <input type='text' class='input-small datepicker hasDatepicker' id='fechaDos' name='fechaDos' value='<?php echo imprimeFecha(); ?>'>
					
					<br /><br />
					Tipo de remesa:<br />
					<?php
						//Las remesas SEPA pueden ser CORE o B2B, que llevan los códigos asociados 19143 y 19445 respectivamente.
						campoSelect('tipoSepa','',array('CORE','B2B'),array('19143','19445'),false,'selectpicker span3 show-tick','',2);
					?>
					<br /><br />
					Banco receptor:<br />
					<?php
						//Las remesas SEPA pueden ser CORE o B2B, que llevan los códigos asociados 19143 y 19445 respectivamente.
						campoSelect('banco','',array('SABADELL','BBVA','LA CAIXA'),array('SABADELL','BBVA','CAIXA'),false,'selectpicker span3 show-tick','',2);
					?>
					

					<br /><br />
					<a href="facturacion.php" class="btn"><i class="icon-arrow-left"></i> Volver</a>
					<button type="submit" class="btn btn-primary">Continuar <i class="icon-circle-arrow-right"></i></button>
				</form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();

  });
</script>