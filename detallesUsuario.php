<?php
  $seccionActiva=8;
  include_once('cabecera.php');
  
  if(isset($_POST['codigo'])){
    $res=actualizaUsuario();
	if(isset($_FILES['ficheroDocumento']) && $_FILES['ficheroDocumento']['name']!=''){	
		$_POST['codigoCliente']=$_POST['codigo'];
		$_FILES['ficheroFactura']=$_FILES['ficheroDocumento'];
    	$res=$res && insertaDatos('documentos_usuarios',time(),'documentos');
    }
  }

  if(isset($_GET['codigo'])){
	  $codigo=$_GET['codigo'];
  }else{
	  $codigo=$_POST['codigo'];
  }
  $datos=datosUsuario($codigo);
  $datosUsuario=datosRegistro('usuarios',$_SESSION['codigoS']);

  $claves=true;
  if($datos['tipo']=='ADMIN' || $datos['tipo']=='ATENCION' || $datos['tipo']=='MARKETING'){
		if($_SESSION['codigoS']!=19 && $_SESSION['codigoS']!=21){
			$claves=false;
		}
	}
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget  cajaSelect">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Modificar usuario</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="?" method="post" enctype="multipart/form-data">
					<div class="tabbable">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#pagina1" data-toggle="tab">Datos generales</a></li>
                      <li><a href="#pagina2" data-toggle="tab">Comisiones</a></li>
					  <li><a href="#pagina3" data-toggle="tab">Mail</a></li>
                    </ul>
                  
                    <br>

                    <div class="tab-content" id="pestanas">

                    <div class="tab-pane active" id="pagina1">
						<fieldset>

						<input type="hidden" name="codigo" value="<?php echo $datos['codigo']; ?>">

					


						<div class="control-group">                     
						  <label class="control-label" for="nombre">Nombre:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="nombre" name="nombre" value="<?php echo $datos['nombre']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="apellidos">Apellidos:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="apellidos" name="apellidos" value="<?php echo $datos['apellidos']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="dni">DNI:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="dni" name="dni" value="<?php echo $datos['dni']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->




						<div class="control-group">                     
						  <label class="control-label" for="email">eMail:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="email" name="email" value="<?php echo $datos['email']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="telefono">Teléfono:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="telefono" name="telefono" value="<?php echo $datos['telefono']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<?php if($claves){ ?>
						<div class="control-group">                     
						  <label class="control-label" for="usuario">Usuario:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="usuario" name="usuario" value="<?php echo $datos['usuario']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->

						
						<div class="control-group">                     
						  <label class="control-label" for="clave">Clave:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="clave" name="clave" value="<?php echo $datos['clave']; ?>">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->

						<?php
						}
							campoRadio('director','¿Director?',$datos);
							echo "<div id='directorOculto'>";
								$consulta="SELECT codigo, CONCAT(nombre, ' ',apellidos) AS texto FROM usuarios WHERE director='SI' AND activoUsuario='SI';";
								campoSelectConsulta('directorAsociado','Director asociado',$consulta,$datos);
							echo "</div>";
							campoRadio('habilitado','Modificación habilitada',$datos);
							campoRadio('activoUsuario','Activo',$datos);
							campoColor('colorTareas','Color para tareas',$datos,'input-mini');
						?>


						<div class="control-group">                     
						  <label class="control-label" for="tipo">Tipo:</label>
						  <div class="controls">
							
							<select name="tipo" class='selectpicker show-tick'>
							  <option value="ADMIN" <?php if($datos['tipo']=='ADMIN'){ echo 'selected="selected"';} ?> >Administrador</option>
										  <option value="ADMINISTRACION" <?php if($datos['tipo']=='ADMINISTRACION'){ echo 'selected="selected"';} ?> >Administración</option>
							  <option value="ATENCION" <?php if($datos['tipo']=='ATENCION'){ echo 'selected="selected"';} ?> >Atención al Cliente</option>
							  <option value="COMERCIAL" <?php if($datos['tipo']=='COMERCIAL'){ echo 'selected="selected"';} ?> >Comercial</option>
							  <option value="CONSULTORIA" <?php if($datos['tipo']=='CONSULTORIA'){ echo 'selected="selected"';} ?> >Técnico Consultoría</option>
							  <option value="FORMACION"  <?php if($datos['tipo']=='FORMACION'){ echo 'selected="selected"';} ?> >Técnico Formación</option>
							  <option value="TELECONCERTADOR" <?php if($datos['tipo']=='TELECONCERTADOR'){ echo 'selected="selected"';} ?>>Teleconcertador</option>
							  <option value="MARKETING" <?php if($datos['tipo']=='MARKETING'){ echo 'selected="selected"';} ?>>Marketing</option>
							  <option value="SUPERVISOR" <?php if($datos['tipo']=='SUPERVISOR'){ echo 'selected="selected"';} ?>>Supervisor</option>
							</select>

						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->

						<div id='teleconcertadorOculto'>
							<?php
								$consulta="SELECT codigo, CONCAT(nombre, ' ',apellidos) AS texto FROM usuarios WHERE tipo='TELECONCERTADOR' AND activoUsuario='SI';";
								echo '<div class="control-group">                     
								  <label class="control-label" for="estado">Teleconcertador:</label>
								  <div class="controls">';
									selectTeleconcertadores($datos['codigo']);
								echo '</div> <!-- /controls -->       
								</div> <!-- /control-group -->';
								//campoSelectConsultaNulo('teleconcertador','Teleconcertador asociado',$consulta,$datos);
							?>
						</div>
					
					</div>

					<div class="tab-pane" id="pagina2">
						<?php
							campoSelect('tipoProfesional','Tipo de comisión',array('Nómina','Profesional'),array('Nomina','Profesional'),$datos);
							campoTextoSimbolo('objetivo','Objetivo mes','€',$datos);
							campoTextoSimbolo('porcentajeNueva','Comisión v. nueva','%',$datos);
							campoTextoSimbolo('porcentajeCartera','Comisión v. cartera','%',$datos);
							campoTextoSimbolo('porcentajeAuditoria','Comisión auditoría','%',$datos);
							campoTextoSimbolo('porcentajeTeleventa','Comisión televenta','%',$datos);
						?>
					</div>
					
					<div class="tab-pane" id="pagina3">
						<center>
                        <table class="table table-striped table-bordered datatable mitadAncho" id="tablaDocumentos">
                          <thead>
                            <tr>
                              <th> Documento </th>
                              <th> Fecha de documento </th>
                              <th class="centro"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              
                              imprimeDocumentosUsuario($codigo);

                            ?>
                          
                          </tbody>
                        </table>
                      
                        <a href="javascript:void" id="botonDocumento" class="btn btn-success"><i class="icon-plus"></i> Añadir documento</a>  Solo se permiten documentos de 5 MB como máximo.
                        <input type="file" class="hide" name="ficheroDocumento" id="ficheroDocumento" onchange="nuevaFila();"/>
                      </center>
					</div>
					
					</div>


                    <div class="form-actions">
					  <?php if($_SESSION['tipoUsuario']!='MARKETING' || ($_SESSION['tipoUsuario']=='MARKETING' && $datosUsuario['habilitado']=='SI')){ ?>
						<button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar usuario</button> 
					  <?php } ?>
                      <a href="usuarios.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-colorpicker.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	<?php
		if($datos['director']=='SI'){
			echo '$("#directorOculto").css("display","none");';
		}
		if($datos['tipo']!='COMERCIAL'){
			echo '$("#teleconcertadorOculto").css("display","none");';
		}
	?>
	
	$('#colorTareas').colorpicker();
	
    $('#todoDia').change(function(){
      
      if($(this).prop('checked')){
        $('#horaInicio').prop('disabled', true);
        $('#horaFin').prop('disabled', true);
      }
      else{
        $('#horaInicio').prop('disabled', false);
        $('#horaFin').prop('disabled', false);
      }

    });
	
	$("input[type=radio][name=director]").change(function(){
		if(this.value=='SI'){
			$("#directorOculto").css('display','none');
		}else{
			$("#directorOculto").css('display','block');
		}
	});
	
	$("select[name=tipo]").change(function(){
		if(this.value=='COMERCIAL'){
			$("#teleconcertadorOculto").css('display','block');
		}else{
			$("#teleconcertadorOculto").css('display','none');
		}
	});
	
	$('#botonDocumento').click(function(){
      if($('#ficheroDocumento').val()==''){
        $('#ficheroDocumento').click();
      }
    });
	
  });
  
  function nuevaFila(){
    $('#botonDocumento').attr('disabled',true);
    $('#tablaDocumentos').find("tr:last").after("<tr><td><input type='text' name='nombreFactura' class='input-large' /></td><td><input type='text' name='fechaFactura' class='input-small datepicker hasDatepicker' /></td><td class='centro'><a href='javascript:void' class='btn btn-primary' onclick=\"$('form').submit();\"><i class='icon-upload'></i> Subir fichero</a> </td></tr>");
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
    });
  }
</script>
