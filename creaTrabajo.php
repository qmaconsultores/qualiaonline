<?php
  $seccionActiva=13;
  include_once("cabecera.php");

  $datos=datosRegistro('ofertas',$_GET['codigo']);
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Alta de Trabajo</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="trabajos.php" method="post">
                  <fieldset>

                    <?php
                      $consulta="SELECT codigo, empresa AS texto FROM clientes;";
					  campoOculto('NULL','tecnico');
                      campoSelectConsulta('codigoCliente','Cliente',$consulta,$datos,'selectpicker span4 show-tick','',true);
                      
                      $consulta="SELECT codigo, CONCAT(codigoProducto,' - ',nombreProducto) AS texto FROM productos;";
                      campoSelectConsulta('codigoProducto','Producto',$consulta,$datos,'selectpicker span4 show-tick','',true);

                      campoOculto($datos['codigo'],'codigoOferta');
                      campoOculto($datos,'codigoCliente');
                      campoOculto($datos,'codigoProducto');
                      campoTexto('ordenTrabajo','Orden de Trabajo',$datos['codigoOferta'],'input-small');
                      campoTexto('proyecto','Denominación del Proyecto');
                      areaTexto('descripcion','Descripción');
                      campoOculto('PROCESO','estado');
                    ?>


                    <h3 class="apartadoFormulario">Planificación de Hitos</h3>
                    <table class="table table-striped table-bordered" id="tablaHitos">
                      <thead>
                        <tr>
                          <th> Actividad </th>
                          <th> Fecha de finalización prevista </th>
                          <th> Fecha de finalización real </th>
                          <th> Observaciones </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><?php campoTextoSolo('actividad0'); ?></td>
                          <td><?php campoFecha('fechaPrevista0','',false,true); ?></td>
                          <td><?php campoFecha('fechaReal0','',false,true); ?></td>
                           <?php areaTextoTabla("observaciones0");?>
                        </tr>
                      </tbody>
                    </table>

                    <center>
                      <button type="button" class="btn btn-success" onclick="insertaFila('tablaHitos');"><i class="icon-plus"></i> Añadir Partida</button> 
                      <button type="button" class="btn btn-danger" onclick="eliminaFila('tablaHitos');"><i class="icon-minus"></i> Eliminar Partida</button> 
                    </center>


                     <br />
                    
                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Registrar Trabajo</button> 
                      <a href="ofertas.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-slider.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $(".slider").slider({
      formater: function(value) {
        return value + '%';
      }

    });

  });

</script>


