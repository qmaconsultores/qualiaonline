<?php
  $seccionActiva=3;
  include_once('cabecera.php');

  $codigoCliente=$_GET['codigo'];
  $datosCliente=datosRegistro('clientes',$codigoCliente);
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Nueva tarea</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="tareas.php" method="post">
                  <fieldset>
                    <?php
                        campoSelect('descripcion','Descripción de tarea',array('Primera visita','Visita seguimiento','Auditoría LOPD','Auditoría PRL','Visita formación','Llamada Seguimiento','Mail'),array('PRIMERA','SEGUIMIENTO','LOPD','PRL','FORMACION','LLAMADA','MAIL'));
                      ?>

                    <div class="control-group">                     
                      <label class="control-label" for="tarea">Tarea:</label>
                      <div class="controls">
                        <input type="text" class="span6" id="tarea" name="tarea">
						<?php
							campoOculto('','horaRealizacion');
						?>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->



					<?php
						//Te lo comento porque aún no está implantado David. Cuando lo termines pues aquí está el campo. campoSelect('tipoTarea','Tipo de tarea',array('Primera visita','Visita seguimiento','Auditoria LOPD','Auditoria PRL','Visita formación','Llamada seguimiento','Mail'),array('Primera visita','Visita seguimiento','Auditoria LOPD','Auditoria PRL','Visita formacion','Llamada seguimiento','Mail'));
					
						campoOculto($codigoCliente,'codigoLista[0]');
						campoTexto('empresa','Empresa',$datosCliente['empresa'],'input-large',true);
						campoSelect('direccionVisita','Dirección de visita',array('Dirección de empresa','Dirección de visita'),array('empresa','visita'));
					?>
         


                    <div class="control-group">                     
                      <label class="control-label" for="fechaInicio">Fecha de inicio:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaInicio" name="fechaInicio" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fechaFin">Fecha de fin:</label>
                      <div class="controls">
                        <input type="text" class="input-small datepicker hasDatepicker" id="fechaFin" name="fechaFin" value="<?php imprimeFecha(); ?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="horaInicio">Hora de inicio:</label>
                      <div class="controls">
                        <input type="text" class="input-mini timepicker" id="horaInicio" name="horaInicio" data-minute-step='1' value="<?php imprimeHora(); ?>">
                        &nbsp;
                        <label class="checkbox inline">
                          <input type="checkbox" name="todoDia" id="todoDia" value="SI"> Todo el día
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

					<?php
						campoHora('horaFin','Hora de fin');
					?>


                    <div class="control-group">                     
                      <label class="control-label" for="estado">Estado:</label>
                      <div class="controls">
                        
                        <label class="radio inline">
                          <input type="radio" name="estado" value="pendiente" checked="checked"> Pendiente
                        </label>
                        
                        <label class="radio inline">
                          <input type="radio" name="estado" value="realizada"> Realizada
                        </label>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="prioridad">Resolución:</label>
                      <div class="controls">
                        
                        <select name="prioridad" id="prioridad" class='selectpicker show-tick'>
						
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						echo "<div id='pendienteOculto'>";
							campoRadio('firmado','Firmado');
							campoRadio('pendiente','Pendiente de','Datos',array('Datos','Formación'),array('Datos','Formacion'));
						echo "</div>";
						echo "<div id='vendidoOculto'>";
							campoOculto('','importe');
						echo "</div>";
						if($_SESSION['tipoUsuario']=='ADMIN'||$_SESSION['tipoUsuario']=='ADMINISTRACION'||$_SESSION['tipoUsuario']=='TELECONCERTADOR'){
							$where='WHERE 1=1';
							if($_SESSION['tipoUsuario']=='TELECONCERTADOR'){
								$codigoU=$_SESSION['codigoS'];
								$where="WHERE (codigo='$codigoU' OR codigo IN (SELECT codigo FROM usuarios WHERE teleconcertador =  '$codigoU'))";
							}
							$consulta="SELECT codigo, CONCAT(nombre, ' ', apellidos) AS texto FROM usuarios $where AND activoUsuario='SI';";
							campoSelectConsulta('codigoUsuario','Usuario asignado',$consulta,$_SESSION['codigoS']);
						}else{
							campoOculto($_SESSION['codigoS'],'codigoUsuario');
						}
					?>


                    <div class="control-group">                     
                      <label class="control-label" for="observaciones">Observaciones:</label>
                      <div class="controls">
                        <textarea name="observaciones" class="areaInforme" id="observaciones"></textarea>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="form-actions">
                      <button id="creaTarea" type="button" class="btn btn-primary"><i class="icon-ok"></i> Registrar tarea</button> 
                      <a href="tareas.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script src="js/jquery.inputmask.js" type="text/javascript"></script>
<script type="text/javascript" src="js/bootstrap-timepicker.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$('.timepicker').timepicker({showMeridian: false, defaultTime: false});	
	$('.hasDatepicker').inputmask({"mask": "99/99/9999"});	
	$('.timepicker').inputmask({"mask": "99:99"});
	$("#fechaInicio").on("changeDate", function() { $('#fechaFin').val($(this).val()); $(this).datepicker("hide"); });
	$("#fechaFin").on("changeDate", function() { $(this).datepicker("hide"); });
    $('#todoDia').change(function(){
      
      if($(this).prop('checked')){
        $('#horaInicio').prop('disabled', true);
        $('#horaFin').prop('disabled', true);
      }
      else{
        $('#horaInicio').prop('disabled', false);
        $('#horaFin').prop('disabled', false);
      }

    });
  
  cambiarSelect($('input[name=estado]:checked').val());
  $('input[name=estado]').change(function(){
    cambiarSelect($(this).val());
    modificaBoton($('#prioridad').val());
  });
  modificaBoton($('#prioridad').val());
  $('#prioridad').change(function(){
    var val=$(this).val();
    mostrarDivs(val);
    modificaBoton(val);
  });
	$('button#creaPreventa').click(function(e){
    creaPreventa();
  })


  $('#horaInicio').blur(function(){
      
      var hora=$('#horaInicio').val().substr(0,2);
	  var minutos=$('#horaInicio').val().slice(3);
	  var minutosSumados=parseFloat(minutos)+15;
	  if(minutosSumados<10){
		minutosSumados='0'+minutosSumados;
	  }
	  $('#horaFin').val(hora+minutosSumados);
	  
    });
	
  cambiarSelect($('input[name=estado]:checked').val());
  $('input[name=estado]').change(function(){
    cambiarSelect($(this).val());
  });

	$('#vendidoOculto').css('display','none');
	$('#prioridad').change(function(){
		mostrarDivs($(this).val());
	});
  });

function cambiarSelect(val){
  var select='';
  $("#prioridad option").remove();
  if(val == 'realizada'){
    $("#prioridad").append("<option value='ALTA' data-content='<span class=\"label label-success\">Venta</span>'></option>");
    $("#prioridad").append("<option value='VISITADO' data-content='<span class=\"label label-azul-claro\">Visitada</span>'></option>"); 
    $("#prioridad").append("<option value='BAJA' data-content='<span class=\"label label-danger\">Baja</span>'></option>");
  } else {
    $("#prioridad").append("<option value='PENDIENTE' data-content='<span class=\"label\">Pendiente</span>'></option>");
    $("#prioridad").append("<option value='PROCESO' data-content='<span class=\"label label-primary\">En proceso</span>'></option>");
    $("#prioridad").append("<option value='RECONCERTAR' data-content='<span class=\"label label-inverse\">Reconcertar</span>'></option>");
    $("#prioridad").append("<option value='CONCERTADA' data-content='<span class=\"label label-verde-claro\">Concertada</span>'></option>"); 
  }
  $('#prioridad').selectpicker('refresh');
  mostrarDivs($('#prioridad').val());
}

function mostrarDivs(val){
  if(val=='PENDIENTE'){
      $('#pendienteOculto').show(300);
    }else{
      $('#pendienteOculto').hide(300);
    }
    if(val=='ALTA'){
      $('#vendidoOculto').show(300);
    }else{
      $('#vendidoOculto').hide(300);
    }
}

function modificaBoton(val){
  if(val=='ALTA'){
      $('button#creaTarea').html('<i class="icon-tags"></i> Crear preventa');
      $('button#creaTarea').attr('id','creaPreventa');
      $('button#creaPreventa').unbind();
      $('button#creaPreventa').click(function(){
        creaPreventa();
      });
    } else {
      $('button#creaPreventa').html('<i class="icon-ok"></i> Registrar tarea');
      $('button#creaPreventa').attr('id','creaTarea');
      $('#creaTarea').unbind();
      $('#creaTarea').click(function(){
        actualizar();
      });
    }
}

function actualizar(){    
    if($('#observaciones').val()!=''){
      $('form').attr('action', "tareas.php").submit();
    }else{
      alert("El campo observaciones debe ser rellenado");
    }
  }

  function creaPreventa(){    
    if($('#observaciones').val()!=''){
      $('form').attr('action','creaPreventa.php').submit();
    } else {
      alert("El campo observaciones debe ser rellenado");
    }
  }
</script>
