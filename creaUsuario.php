<?php
  $seccionActiva=8;
  include_once('cabecera.php');
  $verifica = 1;  
  $_SESSION["verifica"] = $verifica;  
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget  cajaSelect">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Usuario nuevo</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="usuarios.php" method="post">
					<div class="tabbable">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#pagina1" data-toggle="tab">Datos generales</a></li>
                      <li><a href="#pagina2" data-toggle="tab">Comisiones</a></li>
                    </ul>
                  
                    <br>

                    <div class="tab-content" id="pestanas">

                    <div class="tab-pane active" id="pagina1">
					  <fieldset>


						<div class="control-group">                     
						  <label class="control-label" for="nombre">Nombre:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="nombre" name="nombre">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="apellidos">Apellidos:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="apellidos" name="apellidos">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="dni">DNI:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="dni" name="dni">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->




						<div class="control-group">                     
						  <label class="control-label" for="email">eMail:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="email" name="email">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="telefono">Teléfono:</label>
						  <div class="controls">
							<input type="text" class="input-small" id="telefono" name="telefono">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->



						<div class="control-group">                     
						  <label class="control-label" for="usuario">Usuario:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="usuario" name="usuario">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->

						
						<div class="control-group">                     
						  <label class="control-label" for="clave">Clave:</label>
						  <div class="controls">
							<input type="text" class="input-large" id="clave" name="clave">
						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->


						<?php
							campoRadio('director','¿Director?','SI');
							echo "<div id='directorOculto'>";
								$consulta="SELECT codigo, CONCAT(nombre, ' ',apellidos) AS texto FROM usuarios WHERE director='SI' AND activoUsuario='SI';";
								campoSelectConsulta('directorAsociado','Director asociado',$consulta);
							echo "</div>";
							campoRadio('habilitado','Modificación habilitada');
							campoRadio('activoUsuario','Activo');
							campoColor('colorTareas','Color para tareas','','input-mini');
						?>

						<div class="control-group">                     
						  <label class="control-label" for="tipo">Tipo:</label>
						  <div class="controls">
							
							<select name="tipo" class='selectpicker show-tick' id='tipo'>
							  <option value="ADMIN">Administrador</option>
							  <option value="ADMINISTRACION">Administración</option>
							  <option value="ATENCION">Atención al Cliente</option>
							  <option value="COMERCIAL">Comercial</option>
							  <option value="CONSULTORIA">Técnico Consultoría</option>
							  <option value="FORMACION">Técnico Formación</option>
							  <option value="TELECONCERTADOR">Teleconcertador</option>
							  <option value="MARKETING">Marketing</option>
							  <option value="SUPERVISOR">Supervisor</option>
							</select>

						  </div> <!-- /controls -->       
						</div> <!-- /control-group -->

						<div id='teleconcertadorOculto'>
							<?php
								$consulta="SELECT codigo, CONCAT(nombre, ' ',apellidos) AS texto FROM usuarios WHERE tipo='TELECONCERTADOR' AND activoUsuario='SI';";
								campoOculto('NULL','teleconcertador');
								echo '<div class="control-group">                     
								  <label class="control-label" for="estado">Teleconcertador:</label>
								  <div class="controls">';
									selectTeleconcertadores();
								echo '</div> <!-- /controls -->       
								</div> <!-- /control-group -->';
								//campoSelectConsultaNulo('teleconcertador','Teleconcertador asociado',$consulta);
							?>
						</div>
					</div>

					<div class="tab-pane" id="pagina2">
						<?php
							campoSelect('tipoProfesional','Tipo de comisión',array('Nómina','Profesional'),array('Nomina','Profesional'));
							campoTextoSimbolo('objetivo','Objetivo mes','€');
							campoTextoSimbolo('porcentajeNueva','Comisión v. nueva','%');
							campoTextoSimbolo('porcentajeCartera','Comisión v. cartera','%');
							campoTextoSimbolo('porcentajeAuditoria','Comisión auditoría','%');
							campoTextoSimbolo('porcentajeTeleventa','Comisión televenta','%');
						?>
					</div>
					
					</div>


                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Registrar usuario</button> 
                      <a href="usuarios.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-colorpicker.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
	$("#directorOculto").css('display','none');
	$("#teleconcertadorOculto").css('display','none');
	
	$('#colorTareas').colorpicker();

    $('#todoDia').change(function(){
      
      if($(this).prop('checked')){
        $('#horaInicio').prop('disabled', true);
        $('#horaFin').prop('disabled', true);
      }
      else{
        $('#horaInicio').prop('disabled', false);
        $('#horaFin').prop('disabled', false);
      }

    });
	
	$("input[type=radio][name=director]").change(function(){
		if(this.value=='SI'){
			$("#directorOculto").css('display','none');
		}else{
			$("#directorOculto").css('display','block');
		}
	});
	
	$("select[name=tipo]").change(function(){
		if(this.value=='COMERCIAL'){
			$("#teleconcertadorOculto").css('display','block');
		}else{
			$("#teleconcertadorOculto").css('display','none');
		}
	});
	
  });
</script>
