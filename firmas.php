<?php
  $seccionActiva=14;
  include_once('cabecera.php');

  $consultaF=datosRegistroMultiple('firmas');
  $i=1;
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12 margenAb">
        <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Firmas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="facturacion.php" method="post">

                  <fieldset class="span11">
                    <div class="control-group">                     
                      <label class="control-label" for="formasPago"></label>
                      <div class="controls sinMargenIz">
						<center>
                        <table class="table table-striped table-bordered mitadAncho" id="formasPago">
                          <thead>
                            <tr>
                              <th colspan="7" class="centro encabezadoTablaContabilidad">Firmas</th>
                            </tr>
                            <tr>
                              <th>Firma</th>
                              <th>CIF</th>
							  <th>Dirección</th>
							  <th>CP</th>
							  <th>Localidad</th>
							  <th>Teléfono</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $datos=mysql_fetch_assoc($consultaF);
                            while($datos!=false){
                              echo "
                              <tr>
                                <td><input type='text' name='firma$i' id='firma$i' class='input-medium' value='".$datos['firma']."' /></td>
                                <td>
                                  <input type='text' class='input-medium' id='cif$i' name='cif$i' value='".$datos['cif']."' />
                                </td>
								<td>
                                  <input type='text' class='input-medium' id='direccion$i' name='direccion$i' value='".$datos['direccion']."' />
                                </td>
								<td>
                                  <input type='text' class='input-mini' id='cp$i' name='cp$i' value='".$datos['cp']."' />
                                </td>
								<td>
                                  <input type='text' class='input-medium' id='localidad$i' name='localidad$i' value='".$datos['localidad']."' />
                                </td>
								<td>
                                  <input type='text' class='input-small' id='telefono$i' name='telefono$i' value='".$datos['telefono']."' />
                                </td>
                              </tr>";
                              $datos=mysql_fetch_assoc($consultaF);
                              $i++;
                            }
                            ?>
                          </tbody>
                        </table>
						</center>

                        <div class="centro">
                          <button type="button" class="btn btn-success" onclick="insertaFila('formasPago');" ><i class="icon-plus"></i> Insertar</button>
                        </div>

                      </div>
                    </div>

                  </fieldset>
                  <fieldset class="span3">


                    
                    <br>

                  </fieldset>

                  <fieldset class="sinFlotar">
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-save"></i> Guardar Firmas</button> 
                      <a href="facturacion.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/filasTabla.js"></script>
