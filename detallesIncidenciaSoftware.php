<?php
  $seccionActiva=18;
  include_once("cabecera.php");
  include_once("funcionesIncidencia.php");
  $datos=datosRegistroIncidencia('software',$_GET['codigo']);
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Datos incidencia de software</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="incidenciasSoftware.php" method="post">
                  <fieldset>

                    <?php
                      campoOculto($datos);
                      campoFecha('fechaSolicitud','Fecha de solicitud',$datos);
                      areaTexto('observaciones','Observaciones',$datos);

                    ?>
                    
                    <br />                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar Incidencia</button> 
                      <a href="incidenciasSoftware.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

  });
</script>