<?php  
  $seccionActiva=14;
  include_once('cabecera.php');
  $_SESSION["verifica"]=1;
  $fechaUno=formateaFechaBD($_POST['fechaUno']);
  $fechaDos=formateaFechaBD($_POST['fechaDos']);
  $tipo=array('19143'=>'core','19445'=>'b2b');
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-download-alt"></i>
              <h3>Descarga de facturas comprendidas entre el <?php echo formateaFechaWeb($fechaUno); ?> y el <?php echo formateaFechaWeb($fechaDos); ?></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="" method="post" id="formulario">
                  <fieldset>
				  
					<?php
						campoOculto('1','descarga');
						campoOculto('','facturas');
						campoOculto($_POST['tipoSepa'],'tipoSepa');
						campoOculto($_POST['banco'],'banco');
						campoOculto($_POST['firma'],'codigoFirma');
						
						$where=compruebaPerfilParaWhere();
						$consulta=consultaBD("SELECT SUM(facturas_vto.importe) AS facturacion
						FROM facturacion LEFT JOIN clientes ON facturacion.codigoCliente=clientes.codigo 
						LEFT JOIN productos ON facturacion.concepto=productos.codigo
						INNER JOIN facturas_vto ON facturacion.codigo=facturas_vto.codigoFactura
						$where AND facturas_vto.fecha>='$fechaUno' AND facturas_vto.fecha<='$fechaDos' AND firma='".$_POST['firma']."' AND (facturas_vto.enviada='NO' OR (facturas_vto.enviada='SI' AND facturacion.devuelta='SI')) AND facturacion.formaPago='domiciliacion' AND clientes.tipoRemesa='".$tipo[$_POST['tipoSepa']]."' AND coste > 0 ORDER BY facturas_vto.fecha DESC;",true);
						$estadisticas=mysql_fetch_assoc($consulta);
						if($estadisticas['facturacion']==''){
							$estadisticas['facturacion']='0';
						}
						campoOculto($estadisticas['facturacion'],'importe');
						campoOculto($estadisticas['facturacion'],'importeFacturado');
						campoOculto(fecha(),'fecha');
					?>
					
					<div id="big_stats" class="cf">
						<div class="stat" style="width: 50%"> <i class="icon-euro"></i> <span id='tiulosFacturado' class="value"><?php echo number_format((float)$estadisticas['facturacion'], 2, ',', '');?></span> <br>Total facturado</div>
						<div class="stat" style="width: 50%"> <i class="icon-euro"></i> <span id='titulosRemesado' class="value"><?php echo number_format((float)$estadisticas['facturacion'], 2, ',', '');?></span> <br>Total a remesar</div>
					</div>

								  
                    
                    <div class="widget widget-table action-table">
						<div class="widget-header"> <i class="icon-th-list"></i>
						  <h3>Factura/s</h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
						  <table class="table table-striped table-bordered datatable">
							<thead>
							  <tr>
								<th> Referencia </th>
								<th> Cliente </th>
								<th> Concepto </th>
								<th> Fecha de emisión </th>
								<th> Fecha de vencimiento </th>
								<th> Importe </th>
								<th><input type='checkbox' id="todo" checked='checked'></th>
							  </tr>
							</thead>
							<tbody>

							  <?php
								
								
								imprimeFacturasVtoDescarga($fechaUno,$fechaDos,$_POST['firma'],$tipo[$_POST['tipoSepa']]);

							  ?>
							
							</tbody>
						  </table>
						</div>
						<!-- /widget-content --> 
					  </div>

                  
                    <div class="form-actions">
					  <button type="button" class="btn btn-primary" id="descargar"><i class="icon-download-alt"></i> Descargar</button> 
                      <button type="button" class="btn btn-success" id="actualizar"><i class="icon-ok"></i> Confirmado</button> 
                      <a href="facturacion.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                  
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTablaFacturacion.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
	$('.mensajeAviso').hover(function(){
		var titulo=$(this).attr("titulo");
		var contenido=$(this).attr("mensaje");
		$(this).popover({
			 title: titulo,
			 content: contenido,
			 placement:'top'
		});
	});
	$('input[id!=todo]').change(function(){
		val=$(this).attr('checked');
		importe=parseFloat($(this).attr('importe'));
		total=parseFloat($('#importe').val());
		if(val == 'checked'){
			total=total+importe;
		} else {
			total=total-importe;
		}
		$('#titulosRemesado').html(total);
		$('#importe').val(total)
	});

	$('#todo').change(function(){
		val=$(this).attr('checked');
		importe=parseFloat($('#importeFacturado').val());
		if(val == 'checked'){
			total=importe;
		} else {
			total=0;
		}
		$('#titulosRemesado').html(total);
		$('#importe').val(total)
	});

	$('#descargar').click(function(){
		descarga();
    });
	$('#actualizar').click(function(){
		actualizar();
    });
  });
  function descarga(){		
		var valoresChecks=[];
		var i=0;
		$('input[name="codigoLista[]"]:checked').each(function() {
			valoresChecks['codigo'+i]=$(this).val();
			$('#facturas').val($('#facturas').val()+','+valoresChecks['codigo'+i]);
			i++;
		});
		$('form').attr('action', "generaDocumentoFacturas.php").submit();
	}
	
	function actualizar(){		
		$('form').attr('action', "facturacion.php").submit();
	}
  
</script>