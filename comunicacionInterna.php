<?php
  $seccionActiva=20;
  include_once('cabecera.php');
  $res=false;

  if(isset($_POST['codigo'])){
    $res=actualizaDatos('proveedores');
  }
  elseif(isset($_POST['asunto'])){
    $res=creaConversacion();
  }
  elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('conversaciones');
  }

  $estadisticas=creaEstadisticasComunicacionInterna();
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-tasks"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de comunicación interna:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-comments-o"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Conversaciones registradas</div>
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Comunicación Interna</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="creaConversacion.php" class="shortcut"><i class="shortcut-icon icon-plus-sign"></i><span class="shortcut-label">Nueva Conversación</span> </a>
                <?php compruebaOpcionEliminar(); ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      <div class="span12">
        
        <?php 
          mensajeResultado('asunto',$res,'mensaje');
          mensajeResultado('elimina',$res,'mensaje', true);
        ?>
        
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-comments-o"></i>
              <h3>Conversaciones registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Asunto </th>
                    <th> Fecha </th>
					          <th> Hora </th>
                    <th> Autor </th>
                    <th class="centro"> </th>
				          	<th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeConversaciones();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>
<script src="js/jquery.dataTables.js"></script>
<script src="js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="js/filtroTabla.js"></script>
<script type="text/javascript" src="js/checkTabla.js"></script>
<script type="text/javascript" src="js/creaFormulario.js"></script>

<script type="text/javascript">
    $('#eliminar').click(function(){
      var valoresChecks=recorreChecks();
      if(valoresChecks['codigo0']==undefined){
        alert('Por favor, seleccione antes un mensaje del listado.');
      }
      else{
        valoresChecks['elimina']='SI';
        creaFormulario('comunicacionInterna.php',valoresChecks,'post');
      }

    });
</script>