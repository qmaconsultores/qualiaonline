<?php
  $seccionActiva=13;
  include_once("cabecera.php");
  $datosTrabajo=datosRegistro('trabajos',$_GET['codigo']);
  $datos=datosRegistro('ofertas',$datosTrabajo['codigoOferta']);
  $datosHitos=datosHitos($_GET['codigo']);
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-zoom-in"></i>
              <h3>Detalles de Trabajo</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="trabajos.php" method="post">
                  <fieldset class='span3'>

                    <?php
                      $consulta="SELECT codigo, empresa AS texto FROM clientes;";
                      campoSelectConsulta('codigoCliente','Cliente',$consulta,$datosTrabajo,'selectpicker span4 show-tick','',true);
					  
					  
						$consulta="SELECT codigo, CONCAT(nombre, ' ', apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' AND(tipo='CONSULTORIA' OR tipo='FORMACION');";
						campoSelectConsulta('tecnico','Técnico',$consulta,$datosTrabajo);
					  
                      
                      $consulta="SELECT codigo, CONCAT(codigoProducto,' - ',nombreProducto) AS texto FROM productos;";
                      campoSelectConsulta('codigoProducto','Producto',$consulta,$datosTrabajo,'selectpicker span4 show-tick');

					  campoOculto($datos['codigo'],'codigoOferta');
                      campoOculto($datosTrabajo['codigo'],'codigo');
                      campoOculto($datos,'codigoCliente');
                      campoTexto('ordenTrabajo','Orden de Trabajo',$datos['codigoOferta'],'input-small');
                      campoTexto('proyecto','Denominación del Proyecto',$datosTrabajo['proyecto']);
                      $estado=array('REDACCION', 'PROCESO', 'ENTREGADO');
                      campoSelect('estado', 'Estado', $estado, $estado, $datosTrabajo['estado']);
					  campoFecha('fechaValidez','Fecha máx. entrega',$datosTrabajo);
                      areaTexto('descripcion','Descripción',$datosTrabajo['descripcion']);
					  
                    ?>
				  </fieldset>
				  
				  <fieldset class='span3'>

                    <div class="control-group">                     
                      <label class="control-label" for="formacionCurso">Formación:</label>
                      <div class="controls">
                        
                        <select name="formacionCurso" id="formacionCurso" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>" <?php if($datosTrabajo['formacion']=='NO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>" <?php if($datosTrabajo['formacion']=='HECHO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>" <?php if($datosTrabajo['formacion']=='PROCESO'){ echo 'selected="selected"'; } ?>></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>" <?php if($datosTrabajo['formacion']=='FINALIZADO'){ echo 'selected="selected"'; } ?>></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="prl">PRL:</label>
                      <div class="controls">
                        
                        <select name="prl" id="prl" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>" <?php if($datosTrabajo['prl']=='NO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>" <?php if($datosTrabajo['prl']=='HECHO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>" <?php if($datosTrabajo['prl']=='PROCESO'){ echo 'selected="selected"'; } ?>></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>" <?php if($datosTrabajo['prl']=='FINALIZADO'){ echo 'selected="selected"'; } ?>></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="lopd">LOPD:</label>
                      <div class="controls">
                        
                        <select name="lopd" id="lopd" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>" <?php if($datosTrabajo['lopd']=='NO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>" <?php if($datosTrabajo['lopd']=='HECHO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>" <?php if($datosTrabajo['lopd']=='PROCESO'){ echo 'selected="selected"'; } ?>></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>" <?php if($datosTrabajo['lopd']=='FINALIZADO'){ echo 'selected="selected"'; } ?>></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="lssi">LSSI:</label>
                      <div class="controls">
                        
                        <select name="lssi" id="lssi" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>" <?php if($datosTrabajo['lssi']=='NO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>" <?php if($datosTrabajo['lssi']=='HECHO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>" <?php if($datosTrabajo['lssi']=='PROCESO'){ echo 'selected="selected"'; } ?>></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>" <?php if($datosTrabajo['lssi']=='FINALIZADO'){ echo 'selected="selected"'; } ?>></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="aler">APPCC-Alérgenos:</label>
                      <div class="controls">
                        
                        <select name="aler" id="aler" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>" <?php if($datosTrabajo['aler']=='NO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>" <?php if($datosTrabajo['aler']=='HECHO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>" <?php if($datosTrabajo['aler']=='PROCESO'){ echo 'selected="selected"'; } ?>></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>" <?php if($datosTrabajo['aler']=='FINALIZADO'){ echo 'selected="selected"'; } ?>></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="web">Web:</label>
                      <div class="controls">
                        
                        <select name="web" id="web" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>" <?php if($datosTrabajo['web']=='NO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>" <?php if($datosTrabajo['web']=='HECHO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>" <?php if($datosTrabajo['web']=='PROCESO'){ echo 'selected="selected"'; } ?>></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>" <?php if($datosTrabajo['web']=='FINALIZADO'){ echo 'selected="selected"'; } ?>></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="auditoria1">Auditoría LOPD:</label>
                      <div class="controls">
                        
                        <select name="auditoria1" id="auditoria1" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>" <?php if($datosTrabajo['auditoria1']=='NO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>" <?php if($datosTrabajo['auditoria1']=='HECHO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>" <?php if($datosTrabajo['auditoria1']=='PROCESO'){ echo 'selected="selected"'; } ?>></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>" <?php if($datosTrabajo['auditoria1']=='FINALIZADO'){ echo 'selected="selected"'; } ?>></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="auditoria2">Auditoría PRL:</label>
                      <div class="controls">
                        
                        <select name="auditoria2" id="auditoria2" class='selectpicker show-tick'>
                          <option value="NO" data-content="<span class='label'>No tiene</span>" <?php if($datosTrabajo['auditoria2']=='NO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="HECHO" data-content="<span class='label label-danger'>No está hecho</span>" <?php if($datosTrabajo['auditoria2']=='HECHO'){ echo 'selected="selected"'; } ?>></option>
                          <option value="PROCESO" data-content="<span class='label label-warning'>En proceso</span>" <?php if($datosTrabajo['auditoria2']=='PROCESO'){ echo 'selected="selected"'; } ?>></option>
						  <option value="FINALIZADO" data-content="<span class='label label-success'>Finalizado</span>" <?php if($datosTrabajo['auditoria2']=='FINALIZADO'){ echo 'selected="selected"'; } ?>></option>
                        </select>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
				  </fieldset>

				  <fieldset class='span11 sinFlotar'>

                    <h3 class="apartadoFormulario">Planificación de Hitos</h3>
                    <table class="table table-striped table-bordered" id="tablaHitos">
                      <thead>
                        <tr>
                          <th> Actividad </th>
                          <th> Fecha de finalización prevista </th>
                          <th> Fecha de finalización real </th>
						  <th> Observaciones </th>
                        </tr>
                      </thead>
                      <tbody>
						<?php $datosHitosTrabajo=mysql_fetch_assoc($datosHitos);
							$i=0;
							while($datosHitosTrabajo!=0){
							echo "
								<tr>";
								  campoTextoTabla("actividad$i",$datosHitosTrabajo['actividad']);
								  campoFechaTabla("fechaPrevista$i",$datosHitosTrabajo['fechaPrevista'],false,true);
								  campoFechaTabla("fechaReal$i",$datosHitosTrabajo['fechaReal'],false,true);
								  areaTextoTabla("observaciones$i",$datosHitosTrabajo['observaciones']);
							echo "
								</tr>
								";
								$datosHitosTrabajo=mysql_fetch_assoc($datosHitos);
								$i++;
							}
						?>
                      </tbody>
                    </table>

                    <center>
                      <button type="button" class="btn btn-success" onclick="insertaFila('tablaHitos');"><i class="icon-plus"></i> Añadir Partida</button> 
                      <button type="button" class="btn btn-danger" onclick="eliminaFila('tablaHitos');"><i class="icon-minus"></i> Eliminar Partida</button> 
                    </center>


                     <br />
					</fieldset>
                    
                      
				  <fieldset class='sinFlotar'>
                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar Trabajo</button> 
                      <a href="trabajos.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>
<?php include_once('pie.php'); ?>
<script type="text/javascript" src="js/bootstrap-slider.js"></script>
<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $(".slider").slider({
      formater: function(value) {
        return value + '%';
      }

    });

  });

</script>


