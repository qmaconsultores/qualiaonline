<?php
  $seccionActiva=19;
  include_once('cabecera.php');
  $codigo=$_GET['codigo'];

  if(isset($_SESSION['codigoClienteVenta'])){
	$nombreCliente=nombreCliente($_SESSION['codigoClienteVenta']);
  }else{
	
  }
  $datos=datosVenta($codigo);
  $datosCliente=datosRegistro('clientes',$datos['codigoCliente']);
  $nombreCliente=$datosCliente['empresa'];
?>

<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
        <div class="widget">
            <div class="widget-header"> <i class="icon-plus-sign"></i>
              <h3>Datos de la venta realizada al cliente <a href="detallesCuenta.php?codigo=<?php echo $datosCliente['codigo'];?>"><?php echo $nombreCliente; ?></a></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="ventasGeneral.php" method="post">
                  <fieldset>
                    
                    <input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
					
					<?php
						campoOculto('servicio','tipo');
					?>
					
					<div class="control-group">                     
                      <label class="control-label" for="empresa">Empresa:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="empresa" name="empresa" value="<?php echo $datosCliente['empresa']; ?>" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="contacto">Persona de contacto:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="contacto" name="contacto" value="<?php echo $datosCliente['contacto']; ?>" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="telefono">Teléfono:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="telefono" name="telefono" value="<?php echo $datosCliente['telefono']; ?>" maxlength="9" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<div class="control-group">                     
                      <label class="control-label" for="mail">Mail:</label>
                      <div class="controls">
                        <input type="text" class="input-large" id="mail" name="mail" value="<?php echo $datosCliente['mail']; ?>" disabled='disabled'>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					
					<?php
						campoSelect('tipoVentaCarteraNueva','Tipo de venta',array('Cartera','Nueva'),array('CARTERA','NUEVA'),$datos);
						$consulta=consultaBD("SELECT * FROM historico_clientes WHERE codigoCliente='".$datosCliente['codigo']."' ORDER BY fecha;",true);
						$datosHistorico=mysql_fetch_assoc($consulta);
						$historico='';
						if(isset($datosHistorico['codigo'])){
							while(isset($datosHistorico['codigo'])){
								$historico=$historico.formateaFechaWeb($datosHistorico['fecha'])." ".$datosHistorico['hora']." ".$datosHistorico['observaciones']." \n\n";
								$datosHistorico=mysql_fetch_assoc($consulta);
							}
						}
						areaTexto('historicoObservaciones','Histórico Observaciones',$historico,'areaTexto',true);
						$consulta=consultaBD("SELECT historico_tareas.* FROM historico_tareas INNER JOIN tareas ON historico_tareas.codigoTarea=tareas.codigo INNER JOIN clientes ON clientes.codigo=tareas.codigoCliente WHERE tareas.codigoCliente='".$datosCliente['codigo']."' ORDER BY fecha;",true);
						$datosHistorico=mysql_fetch_assoc($consulta);
						$historico='';
						if(isset($datosHistorico['codigo'])){
							while(isset($datosHistorico['codigo'])){
								$historico=$historico.formateaFechaWeb($datosHistorico['fecha'])." ".$datosHistorico['hora']." ".$datosHistorico['observaciones']." \n\n";
								$datosHistorico=mysql_fetch_assoc($consulta);
							}
						}
						areaTexto('historicoTareas','Histórico Tareas',$historico,'areaTexto',true);
						$consulta="SELECT codigo, CONCAT(nombre, ' ', apellidos) AS texto FROM usuarios WHERE activoUsuario='SI' ORDER BY nombre, apellidos;";
						campoSelectConsulta('codigoUsuario','Comercial',$consulta,$datos);
						campoSelectConsulta('concepto','Concepto',"SELECT codigo, nombreProducto AS texto FROM productos ORDER BY codigo;",$datos);
					?>


                    <!--div class="control-group" id="servicio">                     
                      <label class="control-label" for="selectServicio">Concepto:</label>
                      <div class="controls">
                      
                        <select name="concepto" id="selectServicio" class='selectpicker show-tick' data-live-search="true">						  
                          <option value="formacion" <?php if($datos['concepto']=='formacion'){ echo 'selected="selected";'; } ?> >Formación</option>
                          <option value="lopd" <?php if($datos['concepto']=='lopd'){ echo 'selected="selected";'; } ?> >Protección de datos</option>
                          <option value="blanqueo" <?php if($datos['concepto']=='blanqueo'){ echo 'selected="selected";'; } ?> >Blanqueo de capitales</option>
						  <option value="prl" <?php if($datos['concepto']=='prl'){ echo 'selected="selected";'; } ?> >PRL</option>
						  <option value="alergenos" <?php if($datos['concepto']=='alergenos'){ echo 'selected="selected";'; } ?> >ALÉRGENOS</option>
						  <option value="web" <?php if($datos['concepto']=='web'){ echo 'selected="selected";'; } ?> >Plataforma WEB</option>
                        </select>
                      
                      </div--> <!-- /controls -->       
                    <!--/div--> <!-- /control-group -->
                    



                    <div class="control-group">                     
                      <label class="control-label" for="precio">Precio:</label>
                      <div class="controls">
                        
                        <div class="input-prepend input-append">
                          <input type="text" class="input-mini pagination-right" id="precio" name="precio" value="<?php echo $datos['precio']; ?>">
                          <span class="add-on">€</span>
                        </div>

                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->


                    <div class="control-group">                     
                      <label class="control-label" for="fecha">Fecha venta:</label>
                      <div class="controls">
                        <input type="text" class="input-small" id="fecha" name="fecha" value="<?php echo formateaFechaWeb($datos['fecha']);?>">
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->

                    <div class="control-group">                     
                      <label class="control-label" for="observaciones">Observaciones:</label>
                      <div class="controls">
                        <textarea name="observaciones" class="areaTexto" id="observaciones"><?php echo $datos['observaciones']; ?></textarea>
                      </div> <!-- /controls -->       
                    </div> <!-- /control-group -->
					


                    <div id='cajaAlumnos'>
                     <h4 class="apartadoFormulario">Alumnos a inscribir</h4>
                      <table class="table table-striped table-bordered anchoAuto" id="tablaAlumnos">
                        <thead>
                          <tr>
                            <th> Nombre </th>
                            <th> Apellidos </th>
                            <th> DNI </th>
                            <th> Mail </th>
                            <th> Teléfono </th>
                          </tr>
                        </thead>
                        <tbody>
                        
                        <?php

                          $i=0;
                          if($datos['concepto']=='formacion' || $datos['concepto']=='formacionBonificada' || $datos['concepto']=='pif' || $datos['concepto']=='14'){
                            $consultaAlumnos=datosAlumnos($codigo);
                            $datosAlumnos=mysql_fetch_assoc($consultaAlumnos);
                            while($datosAlumnos!=false){
  							              campoOculto($datosAlumnos['codigo'],'codigoAlumno'.$i);
                              echo "
                              <tr>
                                <td><input type='text' class='input-large' id='nombre$i' name='nombre$i' value='".$datosAlumnos['nombre']."'></td>
                                <td><input type='text' class='input-large' id='apellidos$i' name='apellidos$i' value='".$datosAlumnos['apellidos']."'></td>
                                <td><input type='text' class='input-small' id='dni$i' name='dni$i' value='".$datosAlumnos['dni']."'></td>
                                <td><input type='text' class='input-large' id='mail$i' name='mail$i' value='".$datosAlumnos['mail']."'></td>
                                <td><input type='text' class='input-small' id='tlf$i' name='tlf$i' value='".$datosAlumnos['telefono']."'></td>
                              </tr>";

                              $datosAlumnos=mysql_fetch_assoc($consultaAlumnos);
                              $i++;
                            }
                          }
                          
                          if($i==0){
                            echo '
                              <tr>
                                <td><input type="text" class="input-large" id="nombre0" name="nombre0"></td>
                                <td><input type="text" class="input-large" id="apellidos0" name="apellidos0"></td>
                                <td> <input type="text" class="input-small" id="dni0" name="dni0"></td>
                                <td><input type="text" class="input-small" id="mail0" name="mail0"></td>
                                <td><input type="text" class="input-large" id="tlf0" name="tlf0"></td>
                              </tr>
                            ';
                          }

                        ?>
                        </tbody>
                      </table>
                      <br>
                      <center>
                        <button type="button" class="btn btn-success" onclick="insertaFila('tablaAlumnos');"><i class="icon-plus"></i> Añadir alumno</button> 
                        <button type="button" class="btn btn-danger" onclick="eliminaFila('tablaAlumnos');"><i class="icon-minus"></i> Eliminar alumno</button> 
                      </center>
                    </div>


                     <br>
                    
                      
                    <div class="form-actions">
					  <?php if($_SESSION['tipoUsuario']!='SUPERVISOR'){ ?>
						<button type="submit" class="btn btn-primary"><i class="icon-refresh"></i> Actualizar datos</button> 
					  <?php } ?>
                      <a href="ventasGeneral.php" class="btn"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('pie.php'); ?>

<script type="text/javascript" src="js/filasTabla.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('#fecha').datepicker({format:'dd/mm/yyyy',weekStart:1});

	<?php
		if($datos['concepto']=='14' || $datos['concepto']=='formacionBonificada' || $datos['concepto']=='pif'){
			echo "$('#cajaAlumnos').css('display','block');";
		}else{	
			echo "$('#cajaAlumnos').css('display','none');";
		}
	?>

    //Para tabla alumnos
    $('select[name=concepto]').change(function(){//Lo mismo pero para el select de servicios
      if($(this).val()=='14'){
        $('#cajaAlumnos').css('display','block');
		$('#cajaGestoria').css('display','none');
      }
      else{
        $('#cajaAlumnos').css('display','none'); 
		$('#cajaGestoria').css('display','block');
      }
    });
    //Fin tabla alumnos

  });
</script>
